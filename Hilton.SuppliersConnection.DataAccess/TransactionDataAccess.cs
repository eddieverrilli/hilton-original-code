﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class TransactionDataAccess
    {
        /// <summary>
        /// Get the transaction details
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public static PaymentTransactionDetails GetTransactionDetails(int paymentRecordId)
        {
            PaymentTransactionDetails transactionDetails = null;
            IDataReader drTransactionDetails = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetTransactionDetails))
                {
                    //set the input parameters
                    db.AddInParameter(dbCommand, "@paymentRecordId", DbType.Int32, paymentRecordId);

                    drTransactionDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drTransactionDetails.Read())
                    {
                        transactionDetails = new PaymentTransactionDetails()
                        {
                            PartnerCompany = Convert.ToString(drTransactionDetails["CompanyName"], CultureInfo.InvariantCulture),
                            PartnerType = Convert.ToString(drTransactionDetails["PartnershipTypeDesc"], CultureInfo.InvariantCulture),
                            PartnerStatus = Convert.ToString(drTransactionDetails["PartnerStatus"], CultureInfo.InvariantCulture),
                        };
                    }
                    if (transactionDetails == null)
                    {
                        transactionDetails = new PaymentTransactionDetails();
                    }
                    drTransactionDetails.NextResult();
                    transactionDetails.Contacts = new List<Contact>();
                    while (drTransactionDetails.Read())
                    {
                        transactionDetails.CompanyName = Convert.ToString(drTransactionDetails["CompanyName"], CultureInfo.InvariantCulture);
                        transactionDetails.CompanyAddress1 = Convert.ToString(drTransactionDetails["CompanyAddress1"], CultureInfo.InvariantCulture);
                        transactionDetails.CompanyAddress2 = Convert.ToString(drTransactionDetails["CompanyAddress2"], CultureInfo.InvariantCulture);
                        transactionDetails.Country = Convert.ToString(drTransactionDetails["CountryName"], CultureInfo.InvariantCulture);
                        transactionDetails.City = Convert.ToString(drTransactionDetails["CityName"], CultureInfo.InvariantCulture);
                        transactionDetails.State = Convert.ToString(drTransactionDetails["State"], CultureInfo.InvariantCulture);
                        transactionDetails.ZipCode = Convert.ToString(drTransactionDetails["Zip"], CultureInfo.InvariantCulture);
                        transactionDetails.Contacts.Add(new Contact()
                        {
                            FirstName = Convert.ToString(drTransactionDetails["PartnerFirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drTransactionDetails["PartnerLastName"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drTransactionDetails["Email"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drTransactionDetails["Fax"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drTransactionDetails["Phone"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drTransactionDetails["Title"], CultureInfo.InvariantCulture),
                            ContactTypeId = Convert.ToInt32(drTransactionDetails["ContactTypeId"], CultureInfo.InvariantCulture)
                        });
                    }

                    drTransactionDetails.NextResult();
                    while (drTransactionDetails.Read())
                    {
                        transactionDetails.TransactionDateTime = Convert.ToDateTime(drTransactionDetails["TransactionDate"], CultureInfo.InvariantCulture);
                        transactionDetails.TransactionId = Convert.ToString(drTransactionDetails["TransactionId"], CultureInfo.InvariantCulture);
                        transactionDetails.TransactionStatus = Convert.ToString(drTransactionDetails["TransactionStatus"], CultureInfo.InvariantCulture);
                        transactionDetails.PaymentAmount = drTransactionDetails["AmountPaid"] != DBNull.Value ? (decimal?)Convert.ToDecimal(drTransactionDetails["AmountPaid"], CultureInfo.InvariantCulture) : null;
                        transactionDetails.ErrorMessage = Convert.ToString(drTransactionDetails["ErrorMessage"], CultureInfo.InvariantCulture);
                    }
                    drTransactionDetails.NextResult();
                    transactionDetails.TransComment = new List<TranComment>();
                    while (drTransactionDetails.Read())
                    {
                        transactionDetails.TransComment.Add(new TranComment()
                        {
                            UserName = Convert.ToString(drTransactionDetails["UserName"]),
                            RecordedDatetime = Convert.ToString(drTransactionDetails["RecordedDateTime"]),
                            Comment = Convert.ToString(drTransactionDetails["Comments"]),
                        }

                            );
                    }
                    drTransactionDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drTransactionDetails != null && !drTransactionDetails.IsClosed)
                {
                    drTransactionDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return transactionDetails;
        }

        /// <summary>
        /// Gets Transaction List
        /// </summary>
        /// <param name="transactionSearch"></param>
        /// <returns></returns>
        public static Transaction GetTransactionList(TransactionSearch transactionSearch)
        {
            Transaction transaction = null;
            IDataReader drTransactionDetails = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetTransactionList);
                //set the input parameters
                if (transactionSearch.DateFrom == null)
                    db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, transactionSearch.DateFrom);

                if (transactionSearch.DateTo == null)
                    db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, transactionSearch.DateTo);

                if (string.IsNullOrWhiteSpace(transactionSearch.PartnerId))
                    db.AddInParameter(dbCommand, "@partnerId", DbType.String, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@partnerId", DbType.String, transactionSearch.PartnerId);

                if (string.IsNullOrWhiteSpace(transactionSearch.TransactionStatusId))
                    db.AddInParameter(dbCommand, "@transactionStatusId", DbType.String, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@transactionStatusId", DbType.String, transactionSearch.TransactionStatusId);

                if (string.IsNullOrWhiteSpace(transactionSearch.PartnerStatusId))
                    db.AddInParameter(dbCommand, "@partnerStatusId", DbType.String, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@partnerStatusId", DbType.String, transactionSearch.PartnerStatusId);

                if (string.IsNullOrWhiteSpace(transactionSearch.PartnerShipTypeId))
                    db.AddInParameter(dbCommand, "@partnerShipTypeId", DbType.String, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@partnerShipTypeId", DbType.String, transactionSearch.PartnerShipTypeId);

                db.AddInParameter(dbCommand, "@userId", DbType.Int32, transactionSearch.UserId);
                db.AddInParameter(dbCommand, "@searchExpression", DbType.String, transactionSearch.QuickSearchExpression);
                db.AddInParameter(dbCommand, "@searchType", DbType.String, transactionSearch.SearchFlag);
                db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, transactionSearch.PageIndex);
                db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, transactionSearch.PageSize);
                db.AddInParameter(dbCommand, "@sortExpression", DbType.String, ((transactionSearch.SortExpression != null && String.IsNullOrEmpty(transactionSearch.SortExpression))) ? (object)DBNull.Value : transactionSearch.SortExpression);
                db.AddInParameter(dbCommand, "@sortDirection", DbType.String, transactionSearch.SortDirection);
                drTransactionDetails = (IDataReader)db.ExecuteReader(dbCommand);

                if (drTransactionDetails.Read())
                {
                    transaction = new Transaction()
                    {
                        FromDate = Convert.ToDateTime(drTransactionDetails["FromDate"], CultureInfo.InvariantCulture),
                        ToDate = Convert.ToDateTime(drTransactionDetails["ToDate"], CultureInfo.InvariantCulture),
                        Total = drTransactionDetails["TotalAmount"] != DBNull.Value ?
                            (decimal?)Convert.ToDecimal(drTransactionDetails["TotalAmount"], CultureInfo.InvariantCulture) : null,
                        GoldTransactionsNo = Convert.ToInt32(drTransactionDetails["GoldTransactionsNo"], CultureInfo.InvariantCulture),
                        RegularTransactionsNo = Convert.ToInt32(drTransactionDetails["RegularTransactionsNo"], CultureInfo.InvariantCulture),
                        GoldTransactionsAmount = drTransactionDetails["GoldTransactionsAmount"] != DBNull.Value ?
                            (decimal?)Convert.ToDecimal(drTransactionDetails["GoldTransactionsAmount"], CultureInfo.InvariantCulture) : null,
                        RegularTransactionsAmount = drTransactionDetails["RegularTransactionsAmount"] != DBNull.Value ?
                            (decimal?)Convert.ToDecimal(drTransactionDetails["RegularTransactionsAmount"], CultureInfo.InvariantCulture) : null
                    };
                }

                if (transaction != null)
                {
                    drTransactionDetails.NextResult();
                    if (drTransactionDetails.Read())
                    {
                        transaction.TotalRecords = Convert.ToInt32(drTransactionDetails["TotalRecords"], CultureInfo.InvariantCulture);
                    }
                    drTransactionDetails.NextResult();
                    transaction.Transactions = new List<TransactionLineItem>();
                    while (drTransactionDetails.Read())
                    {
                        transaction.Transactions.Add(new TransactionLineItem()
                        {
                            TransactionDateTime = Convert.IsDBNull(drTransactionDetails["TransactionDate"]) ? null : (DateTime?)Convert.ToDateTime(drTransactionDetails["TransactionDate"], CultureInfo.InvariantCulture),
                            PartnerCompany = Convert.ToString(drTransactionDetails["CompanyName"], CultureInfo.InvariantCulture),
                            ContactLastName = Convert.ToString(drTransactionDetails["PartnerLastName"], CultureInfo.InvariantCulture),
                            ContactFirstName = Convert.ToString(drTransactionDetails["PartnerFirstName"], CultureInfo.InvariantCulture),
                            TransactionId = Convert.ToString(drTransactionDetails["TransactionId"], CultureInfo.InvariantCulture),
                            PartnerType = Convert.ToString(drTransactionDetails["PartnershipTypeDesc"], CultureInfo.InvariantCulture),
                            PaymentAmount = drTransactionDetails["AmountPaid"] != DBNull.Value ?
                            (decimal?)Convert.ToDecimal(drTransactionDetails["AmountPaid"], CultureInfo.InvariantCulture) : null,
                            TransactionStatus = Convert.ToString(drTransactionDetails["TransactionStatus"], CultureInfo.InvariantCulture),
                            ContactName = Convert.ToString(drTransactionDetails["PartnerFirstName"], CultureInfo.InvariantCulture) + " " +
                                            Convert.ToString(drTransactionDetails["PartnerLastName"], CultureInfo.InvariantCulture),
                            PaymentRecordId = drTransactionDetails["PaymentRecordId"] == DBNull.Value ? 0 : Convert.ToInt32(drTransactionDetails["PaymentRecordId"], CultureInfo.InvariantCulture)
                        });
                    }
                }

                drTransactionDetails.Close();
            }
            catch (Exception exception)
            {
                if (drTransactionDetails != null && !drTransactionDetails.IsClosed)
                {
                    drTransactionDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return transaction;
        }
        /// <summary>
        /// Gets Transaction List
        /// </summary>
        /// <param name="transactionSearch"></param>
        /// <returns></returns>
        public static DataSet ExportTransactions(TransactionSearch transactionSearch)
        {
            DataSet transaction = null;
            IDataReader drTransactionDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetTransactionList);
                //set the input parameters
                if (transactionSearch.DateFrom == null)
                    db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, transactionSearch.DateFrom);

                if (transactionSearch.DateTo == null)
                    db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, transactionSearch.DateTo);

                if (string.IsNullOrWhiteSpace(transactionSearch.PartnerId))
                    db.AddInParameter(dbCommand, "@partnerId", DbType.String, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@partnerId", DbType.String, transactionSearch.PartnerId);

                if (string.IsNullOrWhiteSpace(transactionSearch.TransactionStatusId))
                    db.AddInParameter(dbCommand, "@transactionStatusId", DbType.String, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@transactionStatusId", DbType.String, transactionSearch.TransactionStatusId);

                if (string.IsNullOrWhiteSpace(transactionSearch.PartnerStatusId))
                    db.AddInParameter(dbCommand, "@partnerStatusId", DbType.String, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@partnerStatusId", DbType.String, transactionSearch.PartnerStatusId);

                if (string.IsNullOrWhiteSpace(transactionSearch.PartnerShipTypeId))
                    db.AddInParameter(dbCommand, "@partnerShipTypeId", DbType.String, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@partnerShipTypeId", DbType.String, transactionSearch.PartnerShipTypeId);

                db.AddInParameter(dbCommand, "@userId", DbType.Int32, transactionSearch.UserId);
                db.AddInParameter(dbCommand, "@searchExpression", DbType.String, transactionSearch.QuickSearchExpression);
                db.AddInParameter(dbCommand, "@searchType", DbType.String, transactionSearch.SearchFlag);
                db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, transactionSearch.PageIndex);
                db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, transactionSearch.PageSize);
                db.AddInParameter(dbCommand, "@sortExpression", DbType.String, ((transactionSearch.SortExpression != null && String.IsNullOrEmpty(transactionSearch.SortExpression))) ? (object)DBNull.Value : transactionSearch.SortExpression);
                db.AddInParameter(dbCommand, "@sortDirection", DbType.String, transactionSearch.SortDirection);
                transaction = db.ExecuteDataSet(dbCommand);

            }
            catch (Exception exception)
            {
                if (drTransactionDetails != null && !drTransactionDetails.IsClosed)
                {
                    drTransactionDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return transaction;
        }

        /// <summary>
        /// Update the transaction status
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int UpdateTransactionStatus(int transactionId, string transactionStatusId, TranComment transComment)        
        {
            int result = 0;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateTransactionStatus))
                {
                    db.AddInParameter(dbCommand, "@paymentRecordId", DbType.Int32, transactionId);
                    db.AddInParameter(dbCommand, "@transactionStatusId", DbType.String, transactionStatusId);
                    if (transComment != null)
                    {
                        db.AddInParameter(dbCommand, "@userId", DbType.Int32, transComment.Userid);
                        db.AddInParameter(dbCommand, "@comment", DbType.String, transComment.Comment);
                    }
                    result = db.ExecuteNonQuery(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}