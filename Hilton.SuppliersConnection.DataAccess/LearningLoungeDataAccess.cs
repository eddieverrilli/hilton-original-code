﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql; 

namespace Hilton.SuppliersConnection.DataAccess
{
    public class LearningLoungeDataAccess
    {
        /// <summary>
        /// Get the details for the gallery control on the learning lounge homepage.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeGallery</returns>
        public static List<LearningLoungeGallery> GetGalleryDetails(int userTypeId)
        { 
            LearningLoungeGallery gallery = null;
            List<LearningLoungeGallery> galleryList = new List<LearningLoungeGallery>();
            IDataReader drGalleryDetails = null;

            try
            {
            SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetGallery"))
                {
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, userTypeId);
                    drGalleryDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drGalleryDetails.Read())
                    {
                        gallery = new LearningLoungeGallery()
                        {
                            GalleryImageId = Convert.ToInt32(drGalleryDetails["GalleryImageId"], CultureInfo.InvariantCulture),
                            Image = Convert.IsDBNull(drGalleryDetails["Image"]) ? null : Convert.ToBase64String((byte[])(drGalleryDetails["Image"])),
                            ClickActionTypeId = Convert.ToInt32(drGalleryDetails["ClickActionTypeId"], CultureInfo.InvariantCulture),
                            ClickActionTypeName = Convert.ToString(drGalleryDetails["ClickActionTypeName"], CultureInfo.InvariantCulture),
                            ClickActionURL = Convert.IsDBNull(drGalleryDetails["ClickActionURL"]) ? null : Convert.ToString(drGalleryDetails["ClickActionURL"], CultureInfo.InvariantCulture),
                            ClickActionTarget = Convert.IsDBNull(drGalleryDetails["ClickActionTarget"]) ? null : Convert.ToString(drGalleryDetails["ClickActionTarget"], CultureInfo.InvariantCulture),
                            Enabled = Convert.ToBoolean(drGalleryDetails["Enabled"], CultureInfo.InvariantCulture),
                            TemplateId = Convert.IsDBNull(drGalleryDetails["TemplateId"]) ? -1 : Convert.ToInt32(drGalleryDetails["TemplateId"]),
                            AllowDelete = Convert.ToBoolean(drGalleryDetails["AllowDelete"], CultureInfo.InvariantCulture),
                            ImageURL = Convert.IsDBNull(drGalleryDetails["ImageURL"]) ? null : Convert.ToString(drGalleryDetails["ImageURL"], CultureInfo.InvariantCulture),
                            ImageType = Convert.IsDBNull(drGalleryDetails["ImageType"]) ? null : Convert.ToString(drGalleryDetails["ImageType"], CultureInfo.InvariantCulture),
                        };

                        galleryList.Add(gallery);
                    }


                    drGalleryDetails.Close();
                }

                return galleryList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeGallery>();
                }
            }
            
        }

        /// <summary>
        /// Get the details for the tile grid control on the learning lounge homepage.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeTile</returns>
        public static List<LearningLoungeTile> GetTileGridDetails(int userTypeId)
        {
            LearningLoungeTile tile = null;
            List<LearningLoungeTile> tileList = new List<LearningLoungeTile>();
            IDataReader drTileGridDetails = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());

                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetTiles"))
                {
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, userTypeId);
                    drTileGridDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drTileGridDetails.Read())
                    {
                        tile = new LearningLoungeTile()
                        {
                            TileImageId = Convert.ToInt32(drTileGridDetails["TileImageId"], CultureInfo.InvariantCulture),
                            Image = Convert.IsDBNull(drTileGridDetails["Image"]) ? null : Convert.ToBase64String((byte[])(drTileGridDetails["Image"])),
                            ClickActionTypeId = Convert.ToInt32(drTileGridDetails["ClickActionTypeId"], CultureInfo.InvariantCulture),
                            ClickActionTypeName = Convert.ToString(drTileGridDetails["ClickActionTypeName"], CultureInfo.InvariantCulture),
                            ClickActionURL = Convert.IsDBNull(drTileGridDetails["ClickActionURL"]) ? null : Convert.ToString(drTileGridDetails["ClickActionURL"], CultureInfo.InvariantCulture),
                            ClickActionTarget = Convert.IsDBNull(drTileGridDetails["ClickActionTarget"]) ? null : Convert.ToString(drTileGridDetails["ClickActionTarget"], CultureInfo.InvariantCulture),
                            HasRollover = Convert.ToBoolean(drTileGridDetails["HasRollover"], CultureInfo.InvariantCulture),
                            Enabled = Convert.ToBoolean(drTileGridDetails["Enabled"], CultureInfo.InvariantCulture),
                            RestrictToUserTypeId = Convert.ToInt32(drTileGridDetails["RestrictToUserTypeId"], CultureInfo.InvariantCulture),
                            TemplateId = Convert.IsDBNull(drTileGridDetails["TemplateId"]) ? -1 : Convert.ToInt32(drTileGridDetails["TemplateId"]),
                            AllowDelete = Convert.ToBoolean(drTileGridDetails["AllowDelete"], CultureInfo.InvariantCulture)
                        };

                        tileList.Add(tile);
                    }


                    drTileGridDetails.Close();
                }

                return tileList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeTile>();
                }
            }

        }

        /// <summary>
        /// Get the details for the FAQs on the learning lounge support center page.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeFAQ</returns>
        public static List<LearningLoungeFAQ> GetSupportCenterFAQs(int userTypeId)
        {
            LearningLoungeFAQ faq = null;
            List<LearningLoungeFAQ> faqList = new List<LearningLoungeFAQ>();
            IDataReader drFAQDetails = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());

                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetFAQs"))
                {
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, userTypeId);
                    drFAQDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drFAQDetails.Read())
                    {
                        faq = new LearningLoungeFAQ()
                        {
                            FAQId = Convert.ToInt32(drFAQDetails["FAQId"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drFAQDetails["Title"], CultureInfo.InvariantCulture),
                            Description = Convert.ToString(drFAQDetails["Description"], CultureInfo.InvariantCulture),
                            TitleColor = Convert.ToString(drFAQDetails["TitleColor"], CultureInfo.InvariantCulture),
                            DisplayOrder = Convert.IsDBNull(drFAQDetails["DisplayOrder"]) ? 0 : Convert.ToInt32(drFAQDetails["DisplayOrder"], CultureInfo.InvariantCulture),                        
                            Enabled = Convert.ToBoolean(drFAQDetails["Enabled"], CultureInfo.InvariantCulture),
                        };

                        faqList.Add(faq);
                    }


                    drFAQDetails.Close();
                }

                return faqList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeFAQ>();
                }
            }

        }

        /// <summary>
        /// Get the calendar events for the learning lounge calendar page.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeCalendarEvent</returns>
        public static List<LearningLoungeCalendarEvent> GetCalendarEvents(int userTypeId) 
        {
            LearningLoungeCalendarEvent evt = null;
            List<LearningLoungeCalendarEvent> eventList = new List<LearningLoungeCalendarEvent>();
            IDataReader drEvents = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetCalendarEvents"))
                {
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, userTypeId);
                    drEvents = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drEvents.Read())
                    {
                        evt = new LearningLoungeCalendarEvent()
                        {
                            EventId = Convert.ToInt32(drEvents["EventId"], CultureInfo.InvariantCulture),
                            name = Convert.ToString(drEvents["Name"], CultureInfo.InvariantCulture),
                            image = Convert.IsDBNull(drEvents["Image"]) ? null : Convert.ToBase64String((byte[])(drEvents["Image"])),
                            StartDate = Convert.IsDBNull(drEvents["StartDate"]) ? DateTime.MinValue: Convert.ToDateTime(drEvents["StartDate"], CultureInfo.InvariantCulture),
                            EndDate = Convert.IsDBNull(drEvents["EndDate"]) ? DateTime.MinValue : Convert.ToDateTime(drEvents["EndDate"], CultureInfo.InvariantCulture),
                            //day = Convert.ToInt32(drEvents["Day"], CultureInfo.InvariantCulture),
                            //month = Convert.ToInt32(drEvents["Month"], CultureInfo.InvariantCulture),
                            //year = Convert.ToInt32(drEvents["Year"], CultureInfo.InvariantCulture),
                            //time = Convert.ToString(drEvents["Time"], CultureInfo.InvariantCulture),
                            //duration = Convert.ToInt32(drEvents["Duration"], CultureInfo.InvariantCulture),
                            color = Convert.ToInt32(drEvents["Color"], CultureInfo.InvariantCulture),
                            location = Convert.ToString(drEvents["Location"], CultureInfo.InvariantCulture),
                            description = Convert.ToString(drEvents["Description"], CultureInfo.InvariantCulture),
                            RestrictToUserTypes = Convert.ToString(drEvents["RestrictToUserTypes"], CultureInfo.InvariantCulture),
                            Enabled = Convert.ToBoolean(drEvents["Enabled"], CultureInfo.InvariantCulture),
                            URL = Convert.ToString(drEvents["URL"], CultureInfo.InvariantCulture),
                        };

                        eventList.Add(evt);
                    }


                    drEvents.Close();
                }

                //Loop through the events and populate each CategoryIds and UserTypeIds properties
                foreach (LearningLoungeCalendarEvent ev in eventList)
                {
                    ev.CategoryIds = GetEventCategories(ev.EventId);
                    ev.UserTypeIds = GetEventUserTypes(ev.EventId);
                }

                return eventList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeCalendarEvent>();
                }
            }

        }

        private static int[] GetEventCategories(int eventId)
        {
            List<int> catList = new List<int>();
            IDataReader drCats = null;

            SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
            using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetCalendarEventCategories"))
            {
                db.AddInParameter(dbCommand, "@eventId", DbType.Int32, eventId);
                drCats = (IDataReader)db.ExecuteReader(dbCommand);

                while (drCats.Read())
                {
                    catList.Add(Convert.ToInt32(drCats["CategoryId"], CultureInfo.InvariantCulture));
                }

                drCats.Close();
            }

            return catList.ToArray();
        }

        private static int[] GetEventUserTypes(int eventId)
        {
            List<int> userTypeList = new List<int>();
            IDataReader drUserTypes = null;

            SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
            using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetCalendarEventUserTypes"))
            {
                db.AddInParameter(dbCommand, "@eventId", DbType.Int32, eventId);
                drUserTypes = (IDataReader)db.ExecuteReader(dbCommand);

                while (drUserTypes.Read())
                {
                    userTypeList.Add(Convert.ToInt32(drUserTypes["UserTypeId"], CultureInfo.InvariantCulture));
                }

                drUserTypes.Close();
            }

            return userTypeList.ToArray();
        }
        
        /// <summary>
        /// Add or edit a calendar event.
        /// </summary>
        /// <returns></returns>
        public static String UpsertCalendarEvent(LearningLoungeCalendarEvent newEvent)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpsertCalendarEvent"))
                {
                    db.AddInParameter(dbCommand, "@eventId", DbType.Int32, newEvent.EventId);
                    db.AddInParameter(dbCommand, "@name", DbType.String, newEvent.name);

                    if (!String.IsNullOrEmpty(newEvent.image))
                    {
                        db.AddInParameter(dbCommand, "@image", DbType.Binary, Convert.FromBase64String(newEvent.image));
                    }

                    db.AddInParameter(dbCommand, "@startDate", DbType.DateTime, newEvent.StartDate);

                    if (newEvent.EndDate != DateTime.MinValue) {
                        db.AddInParameter(dbCommand, "@endDate", DbType.DateTime, newEvent.EndDate);
                    }
                    db.AddInParameter(dbCommand, "@day", DbType.Int32, newEvent.day);
                    db.AddInParameter(dbCommand, "@month", DbType.Int32, newEvent.month);
                    db.AddInParameter(dbCommand, "@year", DbType.Int32, newEvent.year);
                    db.AddInParameter(dbCommand, "@time", DbType.String, newEvent.time);
                    db.AddInParameter(dbCommand, "@duration", DbType.Int32, newEvent.duration);
                    db.AddInParameter(dbCommand, "@color", DbType.String, newEvent.color);
                    db.AddInParameter(dbCommand, "@location", DbType.String, newEvent.location);
                    db.AddInParameter(dbCommand, "@description", DbType.String, newEvent.description);
                    db.AddInParameter(dbCommand, "@restrictToUserTypes", DbType.String, newEvent.RestrictToUserTypes);
                    db.AddInParameter(dbCommand, "@url", DbType.String, newEvent.URL);
                    db.AddInParameter(dbCommand, "@enabled", DbType.Boolean, newEvent.Enabled);
                    db.AddInParameter(dbCommand, "@duplicatingEventId", DbType.Int32, newEvent.DuplicatedEventId);

                    SqlParameter specParam = new SqlParameter("@eventCategories", SqlDbType.Structured);
                    specParam.TypeName = "dbo.LearningLoungeCalendarEventCategoriesType";

                    if (newEvent.CategoryIds.Length == 0)
                    {
                        specParam.Value = null;
                    }
                    else
                    {
                        specParam.Value = CreateSqlDataRecords(newEvent.CategoryIds, "CategoryId");
                    }
                    dbCommand.Parameters.Add(specParam);

                    SqlParameter specParam2 = new SqlParameter("@userTypes", SqlDbType.Structured);
                    specParam2.TypeName = "dbo.LearningLoungeCalendarEventUsersType";

                    if (newEvent.UserTypeIds.Length == 0)
                    {
                        specParam2.Value = null;
                    }
                    else
                    {
                        specParam2.Value = CreateSqlDataRecords(newEvent.UserTypeIds, "UserTypeId");
                    }                    
                    dbCommand.Parameters.Add(specParam2);

                    db.ExecuteNonQuery(dbCommand);

                    return "SUCCESS";
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Creates Sql Data Records
        /// </summary>
        /// <param name="partnerCategories"></param>
        /// <returns></returns>
        private static IEnumerable<SqlDataRecord> CreateSqlDataRecords(int[] eventCategories, String fieldName)
        {
            SqlMetaData[] metaData = new SqlMetaData[1];
            metaData[0] = new SqlMetaData(fieldName, SqlDbType.Int);

            SqlDataRecord resultRecord = new SqlDataRecord(metaData);

            foreach (int eventCategory in eventCategories)
            {
                resultRecord.SetInt32(0, eventCategory);
                yield return resultRecord;
            }
        }

        /// <summary>
        /// To retrieve the data for the design info page of the LearningLounge.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeDesignInfo</returns>
        public static List<LearningLoungeDesignInfo> GetDesignInfo(int designInfoId)
        {
            LearningLoungeDesignInfo info = null;
            List<LearningLoungeDesignInfo> infoList = new List<LearningLoungeDesignInfo>();
            IDataReader drInfo = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetDesignInfo"))
                {
                    db.AddInParameter(dbCommand, "@designInfoId", DbType.Int32, designInfoId);
                    drInfo = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drInfo.Read())
                    {
                        info = new LearningLoungeDesignInfo()
                        {
                            DesignInfoId = Convert.ToInt32(drInfo["DesignInfoId"], CultureInfo.InvariantCulture),
                            Logo = Convert.IsDBNull(drInfo["Logo"]) ? null : Convert.ToBase64String((byte[])(drInfo["Logo"])),
                            LogoThumbnail = Convert.IsDBNull(drInfo["LogoThumbnail"]) ? null : Convert.ToBase64String((byte[])(drInfo["LogoThumbnail"])),
                            Description = Convert.ToString(drInfo["Description"], CultureInfo.InvariantCulture),
                            Brand = Convert.ToString(drInfo["Brand"], CultureInfo.InvariantCulture),
                            ServiceType = Convert.ToString(drInfo["ServiceType"], CultureInfo.InvariantCulture),
                            Header = Convert.ToString(drInfo["Header"], CultureInfo.InvariantCulture),
                            PicsFactsLink = Convert.ToString(drInfo["PicsFactsLink"], CultureInfo.InvariantCulture),
                            AsiaPacificLink = Convert.ToString(drInfo["AsiaPacificLink"], CultureInfo.InvariantCulture),
                            EMEALink = Convert.ToString(drInfo["EMEALink"], CultureInfo.InvariantCulture),
                            LatinAmericaLink = Convert.ToString(drInfo["LatinAmericaLink"], CultureInfo.InvariantCulture),
                            NorthAmericaLink = Convert.ToString(drInfo["NorthAmericaLink"], CultureInfo.InvariantCulture),
                            DesignStudioLink = Convert.ToString(drInfo["DesignStudioLink"], CultureInfo.InvariantCulture)
                        };

                        infoList.Add(info);
                    }


                    drInfo.Close();
                }

                return infoList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeDesignInfo>();
                }
            }

        }

        /// <summary>
        /// Add or update a new FAQ item.
        /// </summary>
        /// <returns></returns>
        public static void UpsertFAQ(LearningLoungeFAQ newFAQ)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpsertFAQ"))
                {
                    db.AddInParameter(dbCommand, "@faqId", DbType.String, newFAQ.FAQId);
                    db.AddInParameter(dbCommand, "@title", DbType.String, newFAQ.Title);
                    db.AddInParameter(dbCommand, "@description", DbType.String, newFAQ.Description);
                    db.AddInParameter(dbCommand, "@titleColor", DbType.String, newFAQ.TitleColor);
                    db.AddInParameter(dbCommand, "@enabled", DbType.Boolean, newFAQ.Enabled);

                    db.ExecuteNonQuery(dbCommand);

                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

        }

        /// <summary>
        /// Delete an FAQ item.
        /// </summary>
        /// <returns></returns>
        public static void DeleteFAQ(int faqId)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeDeleteFAQ"))
                {
                    db.AddInParameter(dbCommand, "@faqId", DbType.Int32, faqId);
                    db.ExecuteNonQuery(dbCommand);

                }

            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

        }

        /// <summary>
        /// Add or update a feature content slider image on the Learning Lounge homepage.
        /// </summary>
        /// <returns></returns>
        public static String UpsertGallerySliderImage(LearningLoungeGallery newItem)
        {
            var isUpdating = false;

            try
            {
                if (newItem.GalleryImageId > 0)
                {
                    isUpdating = true;
                }

                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpsertGalleryImage"))
                {
                    db.AddInParameter(dbCommand, "@galleryImageId", DbType.Int32, newItem.GalleryImageId);

                    if (!String.IsNullOrEmpty(newItem.Image))
                    {
                        db.AddInParameter(dbCommand, "@image", DbType.Binary, Convert.FromBase64String(newItem.Image));
                    }

                    db.AddInParameter(dbCommand, "@clickActionTypeId", DbType.Int32, newItem.ClickActionTypeId);
                    db.AddInParameter(dbCommand, "@clickActionURL", DbType.String, newItem.ClickActionURL);
                    db.AddInParameter(dbCommand, "@clickActionTarget", DbType.String, newItem.ClickActionTarget);
                    db.AddInParameter(dbCommand, "@enabled", DbType.Boolean, newItem.Enabled);

                    SqlParameter RetParam = new SqlParameter("ReturnValue", DBNull.Value);
                    RetParam.Direction = ParameterDirection.ReturnValue;
                    dbCommand.Parameters.Add(RetParam);

                    db.ExecuteNonQuery(dbCommand);

                    if (isUpdating == false)
                    {
                        newItem.GalleryImageId = Convert.ToInt32(RetParam.Value, CultureInfo.InvariantCulture);
                    }
                }

                foreach (var template in newItem.TemplateContent)
                {
                    if (String.IsNullOrEmpty(template.ContentImage) == false || String.IsNullOrEmpty(template.ContentText) == false)
                    {
                        using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpsertTemplateContent"))
                        {
                            db.AddInParameter(dbCommand, "@refId", DbType.Int32, newItem.GalleryImageId);
                            db.AddInParameter(dbCommand, "@templateId", DbType.Int32, template.TemplateId);
                            db.AddInParameter(dbCommand, "@contentType", DbType.String, "gallery");
                            db.AddInParameter(dbCommand, "@contentName", DbType.String, template.ContentName);
                            db.AddInParameter(dbCommand, "@contentText", DbType.String, template.ContentText);

                            if (!String.IsNullOrEmpty(template.ContentImage))
                            {
                                db.AddInParameter(dbCommand, "@contentImage", DbType.Binary, Convert.FromBase64String(template.ContentImage));
                            }

                            db.ExecuteNonQuery(dbCommand);
                        }
                    }
                }
                return "SUCCESS";
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Delete a home page slider.
        /// </summary>
        /// <returns></returns>
        public static String DeleteGalleryImage(int galleryImageId)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeDeleteGalleryImage"))
                {
                    db.AddInParameter(dbCommand, "@galleryImageId", DbType.Int32, galleryImageId);
                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCESS";

            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Delete a home page tile.
        /// </summary>
        /// <returns></returns>
        public static String DeleteTileImage(int tileImageId)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeDeleteTile"))
                {
                    db.AddInParameter(dbCommand, "@tileImageId", DbType.Int32, tileImageId);
                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCESS";

            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Add or update a tile on the Learning Lounge homepage.
        /// </summary>
        /// <returns></returns>
        public static String UpsertTile(LearningLoungeTile newItem)
        {
            var isUpdating = false;

            try
            {
                if (newItem.TileImageId > 0)
                {
                    isUpdating = true;
                }

                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpsertTileImage"))
                {
                    db.AddInParameter(dbCommand, "@tileImageId", DbType.Int32, newItem.TileImageId);

                    if (!String.IsNullOrEmpty(newItem.Image))
                    {
                        db.AddInParameter(dbCommand, "@image", DbType.Binary, Convert.FromBase64String(newItem.Image));
                    }

                    db.AddInParameter(dbCommand, "@clickActionTypeId", DbType.Int32, newItem.ClickActionTypeId);
                    db.AddInParameter(dbCommand, "@clickActionURL", DbType.String, newItem.ClickActionURL);
                    db.AddInParameter(dbCommand, "@clickActionTarget", DbType.String, newItem.ClickActionTarget);
                    db.AddInParameter(dbCommand, "@enabled", DbType.Boolean, newItem.Enabled);
                    db.AddInParameter(dbCommand, "@restrictToUserTypeId", DbType.Int32, newItem.RestrictToUserTypeId);

                    SqlParameter RetParam = new SqlParameter("ReturnValue", DBNull.Value);
                    RetParam.Direction = ParameterDirection.ReturnValue;
                    dbCommand.Parameters.Add(RetParam);

                    db.ExecuteNonQuery(dbCommand);

                    if (isUpdating == false)
                    {
                        newItem.TileImageId = Convert.ToInt32(RetParam.Value, CultureInfo.InvariantCulture);
                    }
                }

                foreach (var template in newItem.TemplateContent)
                {
                    if (String.IsNullOrEmpty(template.ContentImage) == false || String.IsNullOrEmpty(template.ContentText) == false)
                    {
                        using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpsertTemplateContent"))
                        {
                            db.AddInParameter(dbCommand, "@refId", DbType.Int32, newItem.TileImageId);
                            db.AddInParameter(dbCommand, "@templateId", DbType.Int32, template.TemplateId);
                            db.AddInParameter(dbCommand, "@contentType", DbType.String, "tile");
                            db.AddInParameter(dbCommand, "@contentName", DbType.String, template.ContentName);
                            db.AddInParameter(dbCommand, "@contentText", DbType.String, template.ContentText);

                            if (!String.IsNullOrEmpty(template.ContentImage))
                            {
                                db.AddInParameter(dbCommand, "@contentImage", DbType.Binary, Convert.FromBase64String(template.ContentImage));
                            }

                            db.ExecuteNonQuery(dbCommand);
                        }
                    }
                }

                return "SUCCESS";
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To retrieve the contact information for a map on the AD&C contacts page.
        /// We return the Director since it contains all of the required fields for all types of contacts.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeDirector</returns>
        public static List<LearningLoungeDirector> GetContactInfo(String mapName)
        {
            LearningLoungeDirector contact;
            List<LearningLoungeDirector> contactList = new List<LearningLoungeDirector>();
            IDataReader drInfo = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetContactInfo"))
                {
                    db.AddInParameter(dbCommand, "@mapName", DbType.String, mapName);
                    drInfo = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drInfo.Read())
                    {
                        contact = new LearningLoungeDirector()
                        {
                            ContactId = Convert.ToInt32(drInfo["ContactId"], CultureInfo.InvariantCulture),
                            ContactTypeId = Convert.ToInt32(drInfo["ContactTypeId"], CultureInfo.InvariantCulture),
                            name = Convert.ToString(drInfo["Name"], CultureInfo.InvariantCulture),
                            position = Convert.ToString(drInfo["Position"], CultureInfo.InvariantCulture),
                            phone = Convert.ToString(drInfo["Phone"], CultureInfo.InvariantCulture),
                            region = Convert.ToString(drInfo["Region"], CultureInfo.InvariantCulture),
                            region_slug = Convert.ToString(drInfo["RegionSlug"], CultureInfo.InvariantCulture),
                        };

                        contactList.Add(contact);
                    }


                    drInfo.Close();
                }

                return contactList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeDirector>();
                }
            }

        }

        /// <summary>
        /// Update a contact.
        /// </summary>
        /// <returns></returns>
        public static String UpdateContact(LearningLoungeContact contact)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpdateContact"))
                {
                    db.AddInParameter(dbCommand, "@contactId", DbType.Int32, contact.ContactId);
                    db.AddInParameter(dbCommand, "@name", DbType.String, contact.name);
                    db.AddInParameter(dbCommand, "@position", DbType.String, contact.position);
                    db.AddInParameter(dbCommand, "@phone", DbType.String, contact.phone);

                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCESS";

            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Add new CEU Credit.
        /// </summary>
        /// <param name="newCEU">The new CEU to add.</param>
        /// <returns>SUCCESS or EXCEPTION</returns>
        public static String AddCEU(LearningLoungeCEU newCEU)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeInsertCEU"))
                {
                    db.AddInParameter(dbCommand, "@courseTitle", DbType.String, newCEU.CourseTitle);
                    db.AddInParameter(dbCommand, "@company", DbType.String, newCEU.Company);
                    db.AddInParameter(dbCommand, "@contact", DbType.String, newCEU.Contact);
                    db.AddInParameter(dbCommand, "@phone", DbType.String, newCEU.Phone);
                    db.AddInParameter(dbCommand, "@topicLink", DbType.String, newCEU.TopicLink);
                    db.AddInParameter(dbCommand, "@expireDate", DbType.DateTime, newCEU.ExpireDate);
                    db.AddInParameter(dbCommand, "@cost", DbType.Decimal, newCEU.Cost);
                    db.AddInParameter(dbCommand, "@BrandStandardCompliance", DbType.Boolean, newCEU.BrandStandardCompliance);
                    db.AddInParameter(dbCommand, "@AIAAccredited", DbType.Boolean, newCEU.AIAAccredited);
                    db.AddInParameter(dbCommand, "@AIANumCEUs", DbType.String, newCEU.AIANumCEUs);
                    db.AddInParameter(dbCommand, "@LEEDAccredited", DbType.Boolean, newCEU.LEEDAccredited);
                    db.AddInParameter(dbCommand, "@LEEDNumCEUs", DbType.String, newCEU.LEEDNumCEUs);
                    db.AddInParameter(dbCommand, "@submittedByEmail", DbType.String, newCEU.SubmittedByEmail);

                    if (!String.IsNullOrEmpty(newCEU.Image))
                    {
                        db.AddInParameter(dbCommand, "@image", DbType.Binary, Convert.FromBase64String(newCEU.Image));
                    }

                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCESS";
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To retrieve the CEU credits for the CEU Credits page of the Learning Lounge.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeDirector</returns>
        public static List<LearningLoungeCEU> GetCEUCredits(int userTypeId)
        {
            LearningLoungeCEU ceu;
            List<LearningLoungeCEU> ceuList = new List<LearningLoungeCEU>();
            IDataReader drInfo = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetCEU"))
                {
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, userTypeId);
                    drInfo = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drInfo.Read())
                    {
                        ceu = new LearningLoungeCEU()
                        {
                            CEUCreditId = Convert.ToInt32(drInfo["CEUCreditId"], CultureInfo.InvariantCulture),
                            DateCreated = Convert.ToDateTime(drInfo["DateCreated"], CultureInfo.InvariantCulture),
                            CourseTitle = Convert.ToString(drInfo["CourseTitle"], CultureInfo.InvariantCulture),
                            Company = Convert.ToString(drInfo["Company"], CultureInfo.InvariantCulture),
                            Contact = Convert.ToString(drInfo["Contact"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drInfo["Phone"], CultureInfo.InvariantCulture),
                            TopicLink = Convert.ToString(drInfo["TopicLink"], CultureInfo.InvariantCulture),
                            ExpireDate = Convert.IsDBNull(drInfo["ExpireDate"]) ? DateTime.MinValue : Convert.ToDateTime((drInfo["ExpireDate"])),
                            BrandStandardCompliance = Convert.ToBoolean(drInfo["BrandStandardCompliance"], CultureInfo.InvariantCulture),
                            AIAAccredited = Convert.ToBoolean(drInfo["AIAAccredited"], CultureInfo.InvariantCulture),
                            AIANumCEUs = Convert.ToString(drInfo["AIANumCEUs"], CultureInfo.InvariantCulture),
                            LEEDAccredited = Convert.ToBoolean(drInfo["LEEDAccredited"], CultureInfo.InvariantCulture),
                            LEEDNumCEUs = Convert.ToString(drInfo["LEEDNumCEUs"], CultureInfo.InvariantCulture),
                            IsApproved = Convert.ToBoolean(drInfo["IsApproved"], CultureInfo.InvariantCulture),
                            ApprovedDate = Convert.IsDBNull(drInfo["ApprovedDate"]) ? DateTime.MinValue : Convert.ToDateTime((drInfo["ApprovedDate"])),
                            SubmittedByEmail = Convert.ToString(drInfo["SubmittedByEmail"], CultureInfo.InvariantCulture),
                            Image = Convert.IsDBNull(drInfo["Image"]) ? null : Convert.ToBase64String((byte[])(drInfo["Image"])),
                        };

                        ceuList.Add(ceu);
                    }


                    drInfo.Close();
                }

                return ceuList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeCEU>();
                }
            }

        }

        /// <summary>
        /// Delete a CEU Credit.
        /// </summary>
        /// <returns>"SUCCESS" or "EXCEPTION"</returns>
        public static String DeleteCEU(int ceuCreditId)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeDeleteCEU"))
                {
                    db.AddInParameter(dbCommand, "@ceuCreditId", DbType.Int32, ceuCreditId);
                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCESS";
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Approve a CEU Credit.
        /// </summary>
        /// <returns>"SUCCESS" or "EXCEPTION"</returns>
        public static String ApproveCEU(int ceuCreditId)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeApproveCEU"))
                {
                    db.AddInParameter(dbCommand, "@ceuCreditId", DbType.Int32, ceuCreditId);
                    db.ExecuteNonQuery(dbCommand);
                }

                return "SUCCESS";
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Add new meeting event.
        /// </summary>
        /// <param name="partner"></param>
        /// <param name="partnerCategories"></param>
        /// <returns></returns>
        public static String AddMeeting(LearningLoungeMeeting newMeeting)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeInsertMeeting"))
                {
                    db.AddInParameter(dbCommand, "@meetingType", DbType.String, newMeeting.MeetingType);
                    db.AddInParameter(dbCommand, "@company", DbType.String, newMeeting.Company);
                    db.AddInParameter(dbCommand, "@contact", DbType.String, newMeeting.Contact);
                    db.AddInParameter(dbCommand, "@phone", DbType.String, newMeeting.Phone);
                    db.AddInParameter(dbCommand, "@products", DbType.String, newMeeting.Products);
                    db.AddInParameter(dbCommand, "@BrandStandardCompliance", DbType.Boolean, newMeeting.BrandStandardCompliance);
                    db.AddInParameter(dbCommand, "@AIAAccredited", DbType.Boolean, newMeeting.AIAAccredited);
                    db.AddInParameter(dbCommand, "@AIANumCEUs", DbType.String, newMeeting.AIANumCEUs);
                    db.AddInParameter(dbCommand, "@LEEDAccredited", DbType.Boolean, newMeeting.LEEDAccredited);
                    db.AddInParameter(dbCommand, "@LEEDNumCEUs", DbType.String, newMeeting.LEEDNumCEUs);
                    db.AddInParameter(dbCommand, "@submittedByEmail", DbType.String, newMeeting.SubmittedByEmail);
                    
                    db.ExecuteNonQuery(dbCommand);

                    return "SUCCESS";
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

            
        }

        /// <summary>
        /// Add or update a new Media Center item.
        /// </summary>
        /// <returns></returns>
        public static String UpsertMediaCenterItem(LearningLoungeMediaCenterItem newItem)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpsertMediaCenterItem"))
                {
                    db.AddInParameter(dbCommand, "@mediaCenterId", DbType.String, newItem.MediaCenterId);
                    db.AddInParameter(dbCommand, "@contentType", DbType.String, newItem.ContentType);
                    db.AddInParameter(dbCommand, "@contentText", DbType.String, newItem.ContentText);

                    if (!String.IsNullOrEmpty(newItem.ContentImage))
                    {
                        db.AddInParameter(dbCommand, "@contentImage", DbType.Binary, Convert.FromBase64String(newItem.ContentImage));
                    }

                    db.AddInParameter(dbCommand, "@caption", DbType.String, newItem.Caption);
                    db.AddInParameter(dbCommand, "@keywords", DbType.String, newItem.Keywords);
                    db.AddInParameter(dbCommand, "@enabled", DbType.Boolean, newItem.Enabled);

                    db.ExecuteNonQuery(dbCommand);

                    return "SUCCESS";
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Get the data for the media center on the learning lounge media center page.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeMediaCenterItem</returns>
        public static List<LearningLoungeMediaCenterItem> GetMediaCenterItems(int userTypeId)
        {
            LearningLoungeMediaCenterItem media = null;
            List<LearningLoungeMediaCenterItem> mediaList = new List<LearningLoungeMediaCenterItem>();
            IDataReader drMediaItems = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());

                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetMediaCenterItems"))
                {
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, userTypeId);
                    drMediaItems = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drMediaItems.Read())
                    {
                        media = new LearningLoungeMediaCenterItem()
                        {
                            MediaCenterId = Convert.ToInt32(drMediaItems["MediaCenterId"], CultureInfo.InvariantCulture),
                            Caption = Convert.ToString(drMediaItems["Caption"], CultureInfo.InvariantCulture),
                            ContentImage = Convert.IsDBNull(drMediaItems["ContentImage"]) ? null : Convert.ToBase64String((byte[])(drMediaItems["ContentImage"])),
                            ContentText = Convert.ToString(drMediaItems["ContentText"], CultureInfo.InvariantCulture),
                            ContentType = Convert.ToString(drMediaItems["ContentType"], CultureInfo.InvariantCulture),
                            Keywords = Convert.ToString(drMediaItems["Keywords"], CultureInfo.InvariantCulture),            
                            DisplayOrder = Convert.IsDBNull(drMediaItems["DisplayOrder"]) ? 0 :Convert.ToInt32(drMediaItems["DisplayOrder"], CultureInfo.InvariantCulture),                        
                            Enabled = Convert.ToBoolean(drMediaItems["Enabled"], CultureInfo.InvariantCulture),
                        };

                        mediaList.Add(media);
                    }


                    drMediaItems.Close();
                }

                return mediaList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeMediaCenterItem>();
                }
            }

        }

        /// <summary>
        /// Delete a media center item.
        /// </summary>
        /// <returns></returns>
        public static String DeleteMediaCenterItem(int mediaCenterId)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeDeleteMediaCenterItem"))
                {
                    db.AddInParameter(dbCommand, "@mediaCenterId", DbType.Int32, mediaCenterId);
                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCES";
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Delete a calendar event.
        /// </summary>
        /// <returns></returns>
        public static String DeleteCalendarEvent(int eventId)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeDeleteCalendarEvent"))
                {
                    db.AddInParameter(dbCommand, "@eventId", DbType.Int32, eventId);
                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCESS";

            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Get the data for the template content on the Learning Lounge home page.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeTemplateContent</returns>
        public static List<LearningLoungeTemplateContent> GetTemplateContent(int tileOrGalleryId)
        {
            LearningLoungeTemplateContent tc = null;
            List<LearningLoungeTemplateContent> tcList = new List<LearningLoungeTemplateContent>();
            IDataReader drTemplateContent = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());

                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetTemplateInfo"))
                {
                    db.AddInParameter(dbCommand, "@refId", DbType.Int32, tileOrGalleryId);
                    drTemplateContent = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drTemplateContent.Read())
                    {
                        tc = new LearningLoungeTemplateContent()
                        {
                            TemplateId = Convert.ToInt32(drTemplateContent["TemplateId"], CultureInfo.InvariantCulture),
                            ContentImage = Convert.IsDBNull(drTemplateContent["ContentImage"]) ? null : Convert.ToBase64String((byte[])(drTemplateContent["ContentImage"])),
                            ContentType = Convert.ToString(drTemplateContent["ContentType"], CultureInfo.InvariantCulture),
                            ContentName = Convert.ToString(drTemplateContent["ContentName"], CultureInfo.InvariantCulture),
                            ContentText = Convert.ToString(drTemplateContent["ContentText"], CultureInfo.InvariantCulture)
                        };

                        tcList.Add(tc);
                    }


                    drTemplateContent.Close();
                }

                return tcList;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new List<LearningLoungeTemplateContent>();
                }
            }

        }

        /// <summary>
        /// Get the details for the FAQs on the learning lounge support center page.
        /// </summary>
        /// <returns>List of Entities.LearningLoungeFAQ</returns>
        public static LearningLoungeSupportCenterInfo GetSupportCenterInfo()
        {
            LearningLoungeSupportCenterInfo info = new LearningLoungeSupportCenterInfo();
            IDataReader drFAQDetails = null;

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());

                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeGetSupportCenterInfo"))
                {
                    drFAQDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drFAQDetails.Read())
                    {
                        info = new LearningLoungeSupportCenterInfo()
                        {
                            About = Convert.ToString(drFAQDetails["About"], CultureInfo.InvariantCulture),
                            ContactInfo = Convert.ToString(drFAQDetails["Contact"], CultureInfo.InvariantCulture),
                        };

                    }


                    drFAQDetails.Close();
                }

                return info;
            }
            catch (Exception ex)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    //If we are not rethrowing the exception, then return an empty list.
                    return new LearningLoungeSupportCenterInfo();
                }
            }

        }

        /// <summary>
        /// Update the about and contact information on the support center page.
        /// </summary>
        /// <returns></returns>
        public static String UpdateSupportCenterInfo(LearningLoungeSupportCenterInfo info)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpdateSupportInfo"))
                {
                    db.AddInParameter(dbCommand, "@about", DbType.String, info.About);
                    db.AddInParameter(dbCommand, "@contact", DbType.String, info.ContactInfo);

                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCESS";

            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Reorder items.
        /// </summary>
        /// <returns></returns>
        public static String UpdateDisplayOrder(int id, String itemType, int increment)
        {

            try
            {
                SqlDatabase db = new SqlDatabase(SQLHelper.GetConnectionString());
                using (DbCommand dbCommand = db.GetStoredProcCommand("LearningLoungeUpdateDisplayOrder"))
                {
                    db.AddInParameter(dbCommand, "@itemId", DbType.Int32, id);
                    db.AddInParameter(dbCommand, "@itemType", DbType.String, itemType);
                    db.AddInParameter(dbCommand, "@increment", DbType.Int32, increment);

                    db.ExecuteNonQuery(dbCommand);

                }

                return "SUCCESS";

            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }

                return "EXCEPTION";
            }

        }
    }
}
