﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.SqlServer.Server;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class ProjectDataAccess
    {
        /// <summary>
        ///Get Project list for the search
        /// </summary>
        /// <param name="projectSearch"></param>
        /// <param name="totalRecordCount"></param>
        /// <returns name="Project"></returns>
        public static IList<Project> GetProjectList(ProjectSearch projectSearch, ref int totalRecordCount)
        {
            IList<Project> projectCollection = new List<Project>();
            IDataReader drProjectList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectList))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, projectSearch.UserId);
                    if (String.IsNullOrEmpty(projectSearch.ProjectTypeId))
                        db.AddInParameter(dbCommand, "@projectTypeId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@projectTypeId", DbType.Int32, projectSearch.ProjectTypeId);

                    if (String.IsNullOrEmpty(projectSearch.OwnerId))
                        db.AddInParameter(dbCommand, "@ownerId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@ownerId", DbType.String, projectSearch.OwnerId);

                    if (string.IsNullOrWhiteSpace(projectSearch.RegionId))
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, projectSearch.RegionId);
                    //Addition of country
                    if (string.IsNullOrWhiteSpace(projectSearch.CountryId))
                        db.AddInParameter(dbCommand, "@countryId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@countryId", DbType.String, projectSearch.CountryId);


                    if (string.IsNullOrWhiteSpace(projectSearch.BrandId))
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, projectSearch.BrandId);
                   
                    if (string.IsNullOrWhiteSpace(projectSearch.O2OStatusId))
                        db.AddInParameter(dbCommand, "@o2oStatusId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@o2oStatusId", DbType.String, projectSearch.O2OStatusId);

                    if (string.IsNullOrWhiteSpace(projectSearch.ProjectManagerId))
                        db.AddInParameter(dbCommand, "@projectManagerId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@projectManagerId", DbType.String, projectSearch.ProjectManagerId);

                    if (string.IsNullOrWhiteSpace(projectSearch.PropertyTypeId))
                        db.AddInParameter(dbCommand, "@propertyTypeId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@propertyTypeId", DbType.Int32, projectSearch.PropertyTypeId);

                    if (string.IsNullOrWhiteSpace(projectSearch.SCStatusId))
                        db.AddInParameter(dbCommand, "@scStatusId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@scStatusId", DbType.Int32, projectSearch.SCStatusId);

                    db.AddInParameter(dbCommand, "@searchExpression", DbType.String, projectSearch.QuickSearchExpression);
                    db.AddInParameter(dbCommand, "@searchType", DbType.String, projectSearch.SearchFlag);
                    db.AddInParameter(dbCommand, "@sortExpression", DbType.String, projectSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int32, projectSearch.PageSize);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, projectSearch.SortDirection);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, projectSearch.PageIndex);

                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drProjectList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProjectList.Read())
                    {
                        {
                            projectCollection.Add(new Project()
                            {
                                Property = new Facility()
                                {
                                    BrandDescription = Convert.ToString(drProjectList["BrandName"], CultureInfo.InvariantCulture),
                                    BrandSegment = drProjectList["BrandSegment"] == DBNull.Value ? string.Empty : Convert.ToString(drProjectList["BrandSegment"], CultureInfo.InvariantCulture),
                                    FacilityId = Convert.ToInt32(drProjectList["FacilityId"], CultureInfo.InvariantCulture),
                                    FacilityUniqueName = Convert.ToString(drProjectList["FacilityName"], CultureInfo.InvariantCulture),
                                    PropertyTypeDescription = Convert.ToString(drProjectList["PropertyTypeName"], CultureInfo.InvariantCulture),
                                    RegionDescription = Convert.ToString(drProjectList["RegionName"], CultureInfo.InvariantCulture),
                                    Country = Convert.ToString(drProjectList["CountryNmae"], CultureInfo.InvariantCulture)
                                },
                                ProjectId = Convert.ToInt32(drProjectList["ProjectId"], CultureInfo.InvariantCulture),
                                ProjectTypeDescription = Convert.ToString(drProjectList["ProjectTypeName"], CultureInfo.InvariantCulture),
                                OwnerName = Convert.ToString(drProjectList["Owner"], CultureInfo.InvariantCulture),
                                PMName = Convert.ToString(drProjectList["ProjectManager"], CultureInfo.InvariantCulture),
                                ConfigPercent = Convert.ToDouble(drProjectList["ConfigPercent"], CultureInfo.InvariantCulture),
                                Excp = Convert.ToInt32(drProjectList["Exception"], CultureInfo.InvariantCulture),
                                UpdatedOn = drProjectList["UpdatedDate"] != DBNull.Value ?
                                   (DateTime?)Convert.ToDateTime(drProjectList["UpdatedDate"], CultureInfo.InvariantCulture) : null,
                                O2OStatusDescription = Convert.ToString(drProjectList["O2OStatus"], CultureInfo.InvariantCulture),
                                SCStatusDescription = Convert.ToString(drProjectList["ProjectStatusDesc"], CultureInfo.InvariantCulture),
                            }
                            );
                        }
                    }
                    drProjectList.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);

                    drProjectList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectList != null && !drProjectList.IsClosed)
                {
                    drProjectList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectCollection;
        }

        /// <summary>
        ///To get list of the search Filters
        /// </summary>
        /// <returns name="SearchFilters"></returns>
        public static IList<SearchFilters> GetSearchFilterData(int userId)
        {
            IDataReader drSearchFilter = null;

            IList<SearchFilters> searchFilters = new List<SearchFilters>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectFilterData))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drSearchFilter.Read())
                    {
                        searchFilters.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            PropertyTypeId = Convert.ToInt32(drSearchFilter["PropertyTypeId"], CultureInfo.InvariantCulture),
                            PropertyTypeDescription = Convert.ToString(drSearchFilter["PropertyTypeName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drSearchFilter["CountryName"], CultureInfo.InvariantCulture),
                            O2OStatusId = Convert.ToString(drSearchFilter["O2OStatusId"], CultureInfo.InvariantCulture),
                            O2OStatus = Convert.ToString(drSearchFilter["O2OStatus"], CultureInfo.InvariantCulture),
                            OwnerId = Convert.ToString(drSearchFilter["OwnerId"], CultureInfo.InvariantCulture),
                            Owner = Convert.ToString(drSearchFilter["Owner"], CultureInfo.InvariantCulture),
                            ProjectManagerId = Convert.ToString(drSearchFilter["ProjectManagerId"], CultureInfo.InvariantCulture),
                            ProjectManager = Convert.ToString(drSearchFilter["ProjectManager"], CultureInfo.InvariantCulture),
                        }

                      );
                    }

                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        ///Get list of the user by project Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="User"></returns>
        public static IList<User> GetProjectUsers(int projectId)
        {
            IList<User> userCollection = new List<User>();
            IDataReader drUserList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetUserListByProjectId))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    drUserList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drUserList.Read())
                    {
                        userCollection.Add(new User()
                        {
                            UserId = Convert.ToInt32(drUserList["UserId"].ToString()),
                            Name = drUserList["Name"].ToString(),
                            HiltonUserName = Convert.IsDBNull(drUserList["HiltonId"]) ? "-" : drUserList["HiltonId"].ToString(),
                            Email = drUserList["Email"].ToString(),
                            UserTypeDescription = drUserList["UserTypeDescription"].ToString().ToLower() != "owner" ? drUserList["ConsultantRole"].ToString() : drUserList["UserTypeDescription"].ToString(),
                            UserRole = drUserList["ConsultantRole"].ToString(),
                            ReceivePartnerCommunication = Convert.ToBoolean(drUserList["ReceivePartnerCommunication"], CultureInfo.InvariantCulture),
                        });
                    }

                    drUserList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drUserList != null && !drUserList.IsClosed)
                {
                    drUserList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userCollection;
        }

        /// <summary>
        ///Get the list of the user project mapping history
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns  name="UserProjectHistory"></returns>
        public static IList<UserProjectHistory> GetProjectUsersHistory(int projectId)
        {
            IList<UserProjectHistory> userCollection = new List<UserProjectHistory>();
            IDataReader drUserList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetUsersListHistoryByProjectId))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    drUserList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drUserList.Read())
                    {
                        userCollection.Add(new UserProjectHistory()
                        {
                            ModifiedDate = drUserList["UpdateDate"] != DBNull.Value ?
                                   (DateTime?)Convert.ToDateTime(drUserList["UpdateDate"], CultureInfo.InvariantCulture) : null,
                              ModifiedTime = drUserList["UpdateTime"] != DBNull.Value ? Convert.ToDateTime(drUserList["UpdateTime"], CultureInfo.InvariantCulture).ToShortTimeString() : string.Empty,
                            Name = drUserList["Name"].ToString(),
                            UserTypeDescription = drUserList["UserTypeDescription"].ToString(),
                        });
                    }
                    drUserList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drUserList != null && !drUserList.IsClosed)
                {
                    drUserList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userCollection;
        }

        /// <summary>
        ///Get the project details by project Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="projectDetails"></returns>
        public static Project GetProjectDetails(int projectId)
        {
            Project projectDetails = null;
            IDataReader drProjectDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectDetailById))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    drProjectDetails = (IDataReader)db.ExecuteReader(dbCommand);
                    while (drProjectDetails.Read())
                    {
                        projectDetails = new Project()
                        {
                            FacilityId = Convert.ToInt32(drProjectDetails["FacilityId"], CultureInfo.InvariantCulture),
                            BrandDescription = drProjectDetails["BrandName"].ToString(),
                            BrandSegment = drProjectDetails["BrandSegment"].Equals(DBNull.Value) ? null : drProjectDetails["BrandSegment"].ToString(),
                            RegionDescription = drProjectDetails["RegionName"].ToString(),
                            CountryDescription = drProjectDetails["CountryName"].ToString(),
                            ProjectTypeDescription = drProjectDetails["ProjectTypeName"].ToString(),
                            PropertyTypeDescription = drProjectDetails["PropertyTypeName"].ToString(),
                            O2OStatusDescription = drProjectDetails["O2OStatus"].ToString(),
                            ProjectStatus = Convert.ToInt32(drProjectDetails["StatusId"], CultureInfo.InvariantCulture),
                            ConfigPercent = Convert.ToDouble(drProjectDetails["ConfigPercent"].ToString(), CultureInfo.InvariantCulture),
                            FacilityLocation = drProjectDetails["ADDRESS"].ToString(),
                            OwnerName = drProjectDetails["ProjectManager"].ToString(),
                            IsFeatured = (bool)drProjectDetails["IsFeatured"],
                            FeaturedDescription = drProjectDetails["FeaturedDescription"].ToString(),
                            FeaturedThumbnailImage = drProjectDetails["FeaturedProjectThumbnailImage"].Equals(DBNull.Value) ? null : (byte[])drProjectDetails["FeaturedProjectThumbnailImage"],
                            ImageName = drProjectDetails["FeaturedProjectImageName"].ToString(),
                            TemplateId = Convert.ToInt32(drProjectDetails["ProjectTemplateId"], CultureInfo.InvariantCulture)
                        };
                    }
                    drProjectDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectDetails != null && !drProjectDetails.IsClosed)
                {
                    drProjectDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectDetails;
        }

        /// <summary>
        ///Get the project Image by project Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="projectDetails"></returns>
        public static Project GetProjectImage(int projectId)
        {
            Project projectDetails = null;
            IDataReader drProjectDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectImage))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    drProjectDetails = (IDataReader)db.ExecuteReader(dbCommand);
                    while (drProjectDetails.Read())
                    {
                        projectDetails = new Project()
                        {
                            FeaturedImage = drProjectDetails["FeaturedProjectImage"].Equals(DBNull.Value) ? null : (byte[])drProjectDetails["FeaturedProjectImage"],
                            ImageName = drProjectDetails["FeaturedProjectImageName"].ToString()
                        };
                    }

                    drProjectDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectDetails != null && !drProjectDetails.IsClosed)
                {
                    drProjectDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectDetails;
        }

        /// <summary>
        ///Get the list of the category by the projectId
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="categoryListByProjectId"></returns>
        public static IList<Category> GetCategoryListByProjectId(int projectId)
        {
            IList<Category> categoryListByProjectId = new List<Category>();

            //category.PartnerProducts = new List<CategoryPartnerProduct>();
            IDataReader drCategoryList = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectCatDetailById))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    drCategoryList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCategoryList.Read())
                    {
                        categoryListByProjectId.Add(new Category()
                        {
                            RowId = Convert.ToInt32(drCategoryList["RowId"].ToString(), CultureInfo.InvariantCulture),
                            CategoryId = Convert.ToInt32(drCategoryList["CatId"].ToString(), CultureInfo.InvariantCulture),
                            CategoryName = drCategoryList["CatDesc"].ToString(),
                            Level = Convert.ToInt32(drCategoryList["Level"].ToString(), CultureInfo.InvariantCulture),
                            IsLeaf = Convert.ToBoolean(drCategoryList["IsLeaf"].ToString(), CultureInfo.InvariantCulture),
                            IsFeatured = Convert.ToBoolean(drCategoryList["IsFeatured"], CultureInfo.InvariantCulture),
                            IsRequired = Convert.ToBoolean((Convert.ToInt16(drCategoryList["IsRequired"], CultureInfo.InvariantCulture))),
                            ProjectConfigDetailsId = Convert.ToInt32(drCategoryList["ProjectConfigDetailsId"].ToString(), CultureInfo.InvariantCulture),
                            ProjectConfigurationId = Convert.ToInt32(drCategoryList["ProjectConfigurationId"].ToString(), CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drCategoryList["ParentCatId"].ToString(), CultureInfo.InvariantCulture),
                            RootParentId = Convert.ToInt32(drCategoryList["RootParentCatId"].ToString(), CultureInfo.InvariantCulture)
                        });
                    }
                    drCategoryList.NextResult();
                    IList<CategoryPartnerProduct> catPartnerProduct = new List<CategoryPartnerProduct>();
                    while (drCategoryList.Read())
                    {
                        catPartnerProduct.Add(new CategoryPartnerProduct()
                        {
                            CategoryId = Convert.ToInt32(drCategoryList["CatId"], CultureInfo.InvariantCulture),
                            //Isfeatured = Convert.ToBoolean(drCategoryList["IsFeatured"]),
                            PartnerId = Convert.ToInt32(drCategoryList["PartnerId"], CultureInfo.InvariantCulture),
                            PartnerName = drCategoryList["CompanyName"].ToString(),
                            ProductId = Convert.IsDBNull(drCategoryList["ProductId"]) ? 0 : Convert.ToInt32(drCategoryList["ProductId"], CultureInfo.InvariantCulture),
                            ProductName = Convert.IsDBNull(drCategoryList["ProductName"]) ? null : drCategoryList["ProductName"].ToString(),
                            //IsRequired = Convert.ToInt16(drCategoryList["IsRequired"]),
                            ProjectLeafCategoryDetailsId = Convert.ToInt32(drCategoryList["ProjectLeafCategoryDetailsId"], CultureInfo.InvariantCulture),
                            IsOther = Convert.ToBoolean(drCategoryList["IsOther"], CultureInfo.InvariantCulture)
                        });
                    }

                    foreach (Category category in categoryListByProjectId)
                    {
                        category.PartnerProducts = new List<CategoryPartnerProduct>();
                        foreach (CategoryPartnerProduct partnerProduct in catPartnerProduct)
                        {
                            if (category.CategoryId == partnerProduct.CategoryId)
                            {
                                category.PartnerProducts.Add(partnerProduct);
                            }
                        }

                                            
                           
                    }

                    drCategoryList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drCategoryList != null && !drCategoryList.IsClosed)
                {
                    drCategoryList.Close();
                }

                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return categoryListByProjectId.OrderBy(x=>x.RowId).ToList();
        }

        /// <summary>
        ///Get the result when user is mapped to the project
        /// </summary>
        /// <param name="user"></param>
        /// <returns name="result"></returns>
        public static int AddUserToProject(User user)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.AddUsersToProject))
                {
                    db.AddInParameter(dbCommand, "@name", DbType.String, user.Name);
                    db.AddInParameter(dbCommand, "@firstName", DbType.String, user.FirstName);
                    db.AddInParameter(dbCommand, "@lastName", DbType.String, user.LastName);
                    db.AddInParameter(dbCommand, "@addedBy", DbType.String, user.UpdatedByUserId);
                    db.AddInParameter(dbCommand, "@email", DbType.String, user.Email);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, user.ProjectId);
                    db.AddInParameter(dbCommand, "@hiltonUserName", DbType.String, user.HiltonUserName);
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, user.UserTypeId);
                    db.AddInParameter(dbCommand, "@recPartnerComm", DbType.Boolean, user.ReceivePartnerCommunication);
                    db.AddInParameter(dbCommand, "@culture", DbType.String, user.Culture);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///Get the result when user is unmapped from the project
        /// </summary>
        /// <param name="user"></param>
        /// <returns name="result"></returns>
        public static bool DeleteUserFromProject(User user)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.DeleteUserProjectMapping))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.String, user.UserId);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, user.ProjectId);
                    result = db.ExecuteNonQuery(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///Get the product list by partner Id
        /// </summary>
        /// <param name="user"></param>
        /// <returns name="Product"></returns>
        public static IList<Product> GetProductByPartnerId(int partnerId)
        {
            IList<Product> productCollection = new List<Product>();
            IDataReader drProductList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetLeafCatPartnersByProjectId))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, partnerId);
                    drProductList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProductList.Read())
                    {
                        productCollection.Add(new Product()
                        {
                            ProductId = Convert.IsDBNull(drProductList["ProductId"]) ? 0 : Convert.ToInt32(drProductList["ProductId"], CultureInfo.InvariantCulture),
                            ProductName = drProductList["ProductName"].ToString(),
                            CatId = Convert.ToInt32(drProductList["CatId"].ToString(), CultureInfo.InvariantCulture),
                            PartnerId = Convert.ToInt32(drProductList["PartnerId"].ToString(), CultureInfo.InvariantCulture)
                        });
                    }
                    drProductList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProductList != null && !drProductList.IsClosed)
                {
                    drProductList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return productCollection;
        }

        /// <summary>
        ///Get the partner list of the leaf category
        /// </summary>
        /// <param name="user"></param>
        /// <returns name="result"></returns>
        public static Tuple<IList<Partner>, IList<Product>> GetLeafCatPartners(int projectId)
        {
            IList<Partner> partnerCollection = new List<Partner>();
            IList<Product> productCollection = new List<Product>();
            IDataReader drPartnerList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetLeafCatPartnersByProjectId))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    drPartnerList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drPartnerList.Read())
                    {
                        Partner partner = new Partner();
                        partner.PartnerId = Convert.ToString(drPartnerList["PartnerId"], CultureInfo.InvariantCulture);
                        partner.CompanyName = drPartnerList["CompanyName"].ToString();
                        partner.PartnerCategory = new PartnerCategories();
                        partner.IsOther = Convert.ToBoolean(drPartnerList["IsOther"], CultureInfo.InvariantCulture);
                        partner.PartnerCategory.CategoryId = Convert.ToInt32(drPartnerList["CatId"].ToString(), CultureInfo.InvariantCulture);
                        partnerCollection.Add(partner);
                    }

                    drPartnerList.NextResult();
                    while (drPartnerList.Read())
                    {
                        Product product = new Product();
                        product.PartnerId = Convert.ToInt32(drPartnerList["PartnerId"], CultureInfo.InvariantCulture);
                        product.ProductId = Convert.ToInt32(drPartnerList["ProductId"], CultureInfo.InvariantCulture);
                        product.ProductName = drPartnerList["ProductName"].ToString();
                        productCollection.Add(product);
                    }
                    drPartnerList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerList != null && !drPartnerList.IsClosed)
                {
                    drPartnerList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return new Tuple<IList<Partner>, IList<Product>>(partnerCollection, productCollection);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="catPartnerProductList"></param>
        /// <returns></returns>
        private static IEnumerable<SqlDataRecord> CreateSqlDataRecords(IList<CategoryPartnerProduct> catPartnerProductList)
        {
            SqlMetaData[] metaData = new SqlMetaData[3];
            metaData[0] = new SqlMetaData("ProjectLeafCategoryDetailsId", SqlDbType.Int);
            metaData[1] = new SqlMetaData("PartnerId", SqlDbType.Int);
            metaData[2] = new SqlMetaData("ProductId", SqlDbType.Int);

            SqlDataRecord resultRecord = new SqlDataRecord(metaData);

            foreach (CategoryPartnerProduct catPartProduct in catPartnerProductList)
            {
                resultRecord.SetInt32(0, catPartProduct.ProjectLeafCategoryDetailsId);
                resultRecord.SetInt32(1, catPartProduct.PartnerId);
                resultRecord.SetInt32(2, catPartProduct.ProductId);

                yield return resultRecord;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="catList"></param>
        /// <returns></returns>
        private static IEnumerable<SqlDataRecord> CreateSqlDataRecords1(IList<Category> catList)
        {
            SqlMetaData[] metaData = new SqlMetaData[5];
            metaData[0] = new SqlMetaData("ProjectConfigDetailsId", SqlDbType.Int);
            metaData[1] = new SqlMetaData("IsFeatured", SqlDbType.Bit);
            metaData[2] = new SqlMetaData("IsRequired", SqlDbType.Bit);
            metaData[3] = new SqlMetaData("ProjectConfigurationId", SqlDbType.Int);
            metaData[4] = new SqlMetaData("CatId", SqlDbType.Int);

            SqlDataRecord resultRecord = new SqlDataRecord(metaData);

            foreach (Category category in catList)
            {
                resultRecord.SetInt32(0, category.ProjectConfigDetailsId);
                resultRecord.SetBoolean(1, category.IsFeatured);
                resultRecord.SetBoolean(2, Convert.ToBoolean(category.IsRequired));
                resultRecord.SetInt32(3, category.ProjectConfigurationId);
                resultRecord.SetInt32(4, category.CategoryId);
                yield return resultRecord;
            }
        }

        /// <summary>
        /// Get the result after the partner product mapping for the leaf category
        /// </summary>
        /// <param name="catPartnerProductList"></param>
        /// <param name="catList"></param>
        /// <returns name="result"></returns>
        public static int UpdatePartnerProductLeafCategory(IList<CategoryPartnerProduct> catPartnerProductList, IList<Category> catList, Project projectDetails)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateProjectLeafCategoryPartnerProductMapping))
                {
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "LeafCategoryPartnerProduct";
                    param.SqlDbType = SqlDbType.Structured;
                    if (catPartnerProductList.Count == 0)
                        param.Value = null;
                    else
                        param.Value = CreateSqlDataRecords(catPartnerProductList);
                    param.Direction = ParameterDirection.Input;
                    dbCommand.Parameters.Add(param);

                    SqlParameter param1 = new SqlParameter();
                    param1.ParameterName = "catList";
                    param1.SqlDbType = SqlDbType.Structured;
                    param1.Value = CreateSqlDataRecords1(catList);
                    param1.Direction = ParameterDirection.Input;
                    dbCommand.Parameters.Add(param1);
                    db.AddInParameter(dbCommand, "@userId", DbType.String, projectDetails.UserId);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectDetails.ProjectId);
                    result = db.ExecuteNonQuery(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Get the result when project details is updated
        /// </summary>
        /// <param name="project"></param>
        /// <returns name ="result"></returns>
        public static int UpdateProjectDetails(Project project)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateProjectDetails))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, project.ProjectId);
                    db.AddInParameter(dbCommand, "@isFeatured", DbType.Boolean, project.IsFeatured);
                    db.AddInParameter(dbCommand, "@featuredDescription", DbType.String, project.FeaturedDescription);
                    db.AddInParameter(dbCommand, "@featuredImage", DbType.Binary, project.FeaturedImage);
                    db.AddInParameter(dbCommand, "@featuredThumbnailImage", DbType.Binary, project.FeaturedThumbnailImage);
                    db.AddInParameter(dbCommand, "@featuredImageName", DbType.String, project.ImageName);
                    db.AddInParameter(dbCommand, "@projectstatus", DbType.Int32, project.ProjectStatus);
                    db.AddInParameter(dbCommand, "@userid", DbType.Int32, project.UserId);
                    result = db.ExecuteNonQuery(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///Get the user list of type consultants
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="User"></returns>
        public static IList<User> GetProjectConsultants(int projectId)
        {
            IList<User> userCollection = new List<User>();
            IDataReader drUserList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetConsultantListByProjectId))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    drUserList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drUserList.Read())
                    {
                        userCollection.Add(new User()
                        {
                            UserId = Convert.ToInt32(drUserList["UserId"], CultureInfo.InvariantCulture),
                            Name = drUserList["Name"].ToString(),
                            FirstName = drUserList["FirstName"].ToString(),
                            LastName = drUserList["LastName"].ToString(),
                            Email = drUserList["Email"].ToString(),
                            UserStatus = drUserList["UserProjectActivationStatus"].ToString(),
                            UserRoleId = drUserList["RoleId"] != DBNull.Value ? (int?)Convert.ToInt32(drUserList["RoleId"], CultureInfo.InvariantCulture) : null,
                            UserTypeId = Convert.ToInt32(drUserList["UserType"], CultureInfo.InvariantCulture),
                            UserStatusId = Convert.ToInt32(drUserList["StatusId"], CultureInfo.InvariantCulture),
                            UserTypeDescription = drUserList["UserTypeDescription"].ToString().ToLower() != "owner" ? drUserList["ConsultantRole"].ToString() : drUserList["UserTypeDescription"].ToString(),
                            UserRole = drUserList["ConsultantRole"].ToString(),
                            ReceivePartnerCommunication = Convert.ToBoolean(drUserList["ReceivePartnerCommunication"], CultureInfo.InvariantCulture),
                            ProjectId = Convert.ToInt32(drUserList["ProjectId"], CultureInfo.InvariantCulture),
                            IsRemoved = Convert.ToBoolean(drUserList["IsDeleted"], CultureInfo.InvariantCulture)
                        });
                    }
                    drUserList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drUserList != null && !drUserList.IsClosed)
                {
                    drUserList.Close();
                }

                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userCollection;
        }

        /// <summary>
        ///Get the project details by project id and userId
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="userId"></param>
        /// <returns name="ProjectDetail"></returns>
        public static ProjectDetail GetFeaturedProjectProfile(int projectId, int userId)
        {
            IDataReader drProjectProfileDetails = null;
            ProjectDetail projectDetails = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetFeaturedProjectProfileByProjectId))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drProjectProfileDetails = (IDataReader)db.ExecuteReader(dbCommand);
                    if (drProjectProfileDetails.Read())
                    {
                        projectDetails = new ProjectDetail()
                        {
                            ProjectId = Convert.ToInt32(drProjectProfileDetails["ProjectId"], CultureInfo.InvariantCulture),
                            ProjectManager = Convert.ToString(drProjectProfileDetails["ProjectManager"], CultureInfo.InvariantCulture),
                            ProjectTypeName = Convert.ToString(drProjectProfileDetails["ProjectTypeName"], CultureInfo.InvariantCulture),
                            BrandName = Convert.ToString(drProjectProfileDetails["Brand"], CultureInfo.InvariantCulture),
                            Address = Convert.ToString(drProjectProfileDetails["Address"], CultureInfo.InvariantCulture),
                            Address2 = Convert.ToString(drProjectProfileDetails["Address2"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.ToString(drProjectProfileDetails["ZipCode"], CultureInfo.InvariantCulture),
                            City = Convert.ToString(drProjectProfileDetails["City"], CultureInfo.InvariantCulture),
                            Country = Convert.ToString(drProjectProfileDetails["Country"], CultureInfo.InvariantCulture),
                        };
                    }
                    drProjectProfileDetails.NextResult();
                    projectDetails.ProjectCategoriesDetails = new List<ProjectConfigDetails>();
                    while (drProjectProfileDetails.Read())
                    {
                        projectDetails.ProjectCategoriesDetails.Add(new ProjectConfigDetails()
                        {
                            ProjectId = projectDetails.ProjectId,
                            CategoryId = Convert.ToInt32(drProjectProfileDetails["CategoryId"], CultureInfo.InvariantCulture),
                            CategoryName = drProjectProfileDetails["CategoryName"].ToString(),
                            CategoryLevel = Convert.ToInt32(drProjectProfileDetails["Level"], CultureInfo.InvariantCulture),
                            IsLeaf = Convert.ToBoolean(drProjectProfileDetails["IsLeaf"], CultureInfo.InvariantCulture),
                            IsActive = Convert.ToBoolean(drProjectProfileDetails["IsActive"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drProjectProfileDetails["ParentCategory"], CultureInfo.InvariantCulture),
                            RootParentId = Convert.ToInt32(drProjectProfileDetails["RootParentCatID"], CultureInfo.InvariantCulture),
                            HasProducts = Convert.ToBoolean(drProjectProfileDetails["HasProducts"], CultureInfo.InvariantCulture),
                            IsRequired = drProjectProfileDetails["IsRequired"] == DBNull.Value ? false : Convert.ToBoolean(drProjectProfileDetails["IsRequired"], CultureInfo.InvariantCulture),
                            CategoryDescription = drProjectProfileDetails["CatDesc"] == DBNull.Value ? null : Convert.ToString(drProjectProfileDetails["CatDesc"], CultureInfo.InvariantCulture),
                            SequenceId = Convert.ToInt32(drProjectProfileDetails["SequenceId"], CultureInfo.InvariantCulture),
                        });
                    }

                    drProjectProfileDetails.NextResult();
                    IList<ProjectConfigPartnerProduct> partnerProducts = new List<ProjectConfigPartnerProduct>();
                    while (drProjectProfileDetails.Read())
                    {
                        ProjectConfigPartnerProduct product = new ProjectConfigPartnerProduct();
                        product.IsOther = Convert.ToBoolean(drProjectProfileDetails["IsOther"], CultureInfo.InvariantCulture);
                        product.PartnerId = Convert.ToInt32(drProjectProfileDetails["PartnerId"], CultureInfo.InvariantCulture);
                        product.PartnerName = drProjectProfileDetails["PartnerName"].ToString();
                        product.ProductId = Convert.IsDBNull(drProjectProfileDetails["ProductId"]) ? 0 : Convert.ToInt32(drProjectProfileDetails["ProductId"], CultureInfo.InvariantCulture);
                        product.ProductName = Convert.IsDBNull(drProjectProfileDetails["ProductName"]) ? string.Empty : drProjectProfileDetails["ProductName"].ToString();
                        product.CategoryId = Convert.ToInt32(drProjectProfileDetails["CatId"], CultureInfo.InvariantCulture);
                        product.SkuNumber = Convert.IsDBNull(drProjectProfileDetails["SKUNumber"]) ? string.Empty : drProjectProfileDetails["SKUNumber"].ToString();
                        product.PartnerImage = drProjectProfileDetails["PartnerImage"].Equals(DBNull.Value) ? null : (byte[])drProjectProfileDetails["PartnerImage"];
                        product.ProductThumbnailImage = drProjectProfileDetails["ProductThumbnailImage"].Equals(DBNull.Value) ? null : (byte[])drProjectProfileDetails["ProductThumbnailImage"];
                        product.ProductDescription = Convert.IsDBNull(drProjectProfileDetails["ProductDescription"]) ? string.Empty : Convert.ToString(drProjectProfileDetails["ProductDescription"], CultureInfo.InvariantCulture);
                        product.ContactEmail = Convert.IsDBNull(drProjectProfileDetails["ContactEmail"]) ? string.Empty : Convert.ToString(drProjectProfileDetails["ContactEmail"], CultureInfo.InvariantCulture);
                        product.ContactFirstName = Convert.IsDBNull(drProjectProfileDetails["ContactFirstName"]) ? string.Empty : Convert.ToString(drProjectProfileDetails["ContactFirstName"], CultureInfo.InvariantCulture);
                        product.ContactLastName = Convert.IsDBNull(drProjectProfileDetails["ContactLastName"]) ? string.Empty : Convert.ToString(drProjectProfileDetails["ContactLastName"], CultureInfo.InvariantCulture);
                        product.SpecSheetPDFName = Convert.IsDBNull(drProjectProfileDetails["ContactLastName"]) ? string.Empty : Convert.ToString(drProjectProfileDetails["SpecSheetPDFName"], CultureInfo.InvariantCulture);
                        partnerProducts.Add(product);
                    }

                    foreach (ProjectConfigDetails category in projectDetails.ProjectCategoriesDetails)
                    {
                        category.LeafCategoryDetails = new ProjectConfigLeafCategoryDetail();
                        category.LeafCategoryDetails.PartnerProductDetails = new List<ProjectConfigPartnerProduct>();
                        foreach (ProjectConfigPartnerProduct partnerProduct in partnerProducts)
                        {
                            if (partnerProduct.CategoryId == category.CategoryId)
                            {
                                category.LeafCategoryDetails.PartnerProductDetails.Add(partnerProduct);
                            }
                        }
                    }

                    drProjectProfileDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectProfileDetails != null && !drProjectProfileDetails.IsClosed)
                {
                    drProjectProfileDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return projectDetails;
        }

        /// <summary>
        ///get partner list from db
        /// </summary>
        /// <param name="categoryPartnerProduct"></param>
        /// <returns></returns>
        public static IList<Partner> GetPartnerList(CategoryPartnerProduct categoryPartnerProduct)
        {
            IDataReader drPartnerList = null;
            IList<Partner> partnerList = new List<Partner>();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnersByTemplateCategory))
                {
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, categoryPartnerProduct.TemplateId);
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, categoryPartnerProduct.CategoryId);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, categoryPartnerProduct.ProjectId);
                    drPartnerList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drPartnerList.Read())
                    {
                        partnerList.Add(new Partner()
                        {
                            PartnerId = drPartnerList["PartnerId"].ToString(),
                            CompanyName = drPartnerList["PartnerName"].ToString()
                        });
                    }

                    drPartnerList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerList != null && !drPartnerList.IsClosed)
                {
                    drPartnerList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return partnerList;
        }

        /// <summary>
        ///gets product list from db for a selected partner
        /// </summary>
        /// <param name="categoryPartnerProduct"></param>
        /// <returns></returns>
        public static IList<Product> GetProductList(CategoryPartnerProduct categoryPartnerProduct)
        {
            IDataReader drProductList = null;
            IList<Product> productList = new List<Product>();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductByPartnerId))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, categoryPartnerProduct.PartnerId);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, categoryPartnerProduct.ProjectId);
                    drProductList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProductList.Read())
                    {
                        productList.Add(new Product()
                        {
                            ProductId = Convert.ToInt32(drProductList["ProductId"], CultureInfo.InvariantCulture),
                            ProductName = drProductList["ProductName"].ToString()
                        });
                    }

                    drProductList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProductList != null && !drProductList.IsClosed)
                {
                    drProductList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return productList;
        }
    }
}