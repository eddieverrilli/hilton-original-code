﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class UserDataAccess
    {
        /// <summary>
        /// Add the user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="createdByUserId"></param>
        /// <returns></returns>
        public static int AddUser(User user, int createdByUserId)
        {
            int userId = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsertUser))
                {
                    if (user.UserTypeId == (int)UserTypeEnum.Owner || user.UserTypeId == (int)UserTypeEnum.Consultant)
                    {
                        SetUserProjectMappingData(dbCommand, user.AssociatedProjects);
                    }
                    if (user.UserTypeId == (int)UserTypeEnum.Administrator)
                    {
                        SetAdminPermissionData(dbCommand, user.AdminPermission);
                        SetCategoryAddition(dbCommand, user.AccessibleCategoryList);
                    }

                    if (user.UserTypeId == (int)UserTypeEnum.Partner)
                    {
                        SetUserPartnerMappingData(dbCommand, user.AssociatedPartners);
                    }
                    string hiltonId = (user.HiltonUserName == string.Empty) ? null : Convert.ToString(user.HiltonUserName, CultureInfo.InvariantCulture);
                    db.AddInParameter(dbCommand, "@firstName", DbType.String, user.FirstName);
                    db.AddInParameter(dbCommand, "@lastName", DbType.String, user.LastName);
                    db.AddInParameter(dbCommand, "@email", DbType.String, user.Email);
                    db.AddInParameter(dbCommand, "@hiltonId", DbType.String, hiltonId);
                    //db.AddInParameter(dbCommand, "@company", DbType.String, user.Company);
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, user.UserTypeId);
                    db.AddInParameter(dbCommand, "@userStatusId", DbType.Int32, user.UserStatusId);
                    db.AddInParameter(dbCommand, "@createdByUserId", DbType.Int32, createdByUserId); //will be fetched from session variable
                    db.AddInParameter(dbCommand, "@culture", DbType.String, user.Culture);
                    SqlParameter retValParam = new SqlParameter("@userId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    userId = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userId;
        }

        /// <summary>
        /// Get the images
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Partner> GetImages()
        {
            IDataReader drImages = null;
            List<Partner> partnerCollection = new List<Partner>();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetImages))
                {
                    drImages = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drImages.Read())
                    {
                        Partner partnerImage = new Partner();
                        partnerImage.CompanyName = Convert.ToString(drImages["CompanyName"], CultureInfo.InvariantCulture);
                        partnerImage.CompanyLogoImage = (byte[])drImages["CompanyLogo"];
                        partnerImage.PartnerId = Convert.ToString(drImages["PartnerId"], CultureInfo.InvariantCulture);
                        partnerCollection.Add(partnerImage);
                    }

                    drImages.Close();
                }
            }
            catch (Exception exception)
            {
                if (drImages != null && !drImages.IsClosed)
                {
                    drImages.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerCollection;
        }

        /// <summary>
        ///  Gets the search filter data
        /// </summary>
        /// <returns></returns>
        public static IList<SearchFilters> GetSearchFilterData()
        {
            IDataReader drSearchFilter = null;
            IList<SearchFilters> searchFilters = new List<SearchFilters>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectSearchFiltersData))
                {
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drSearchFilter.Read())
                    {
                        searchFilters.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            ProjectTypeId = Convert.ToInt32(drSearchFilter["ProjectTypeId"], CultureInfo.InvariantCulture),
                            ProjectTypeDescription = Convert.ToString(drSearchFilter["ProjectTypeName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture)
                        });
                    }

                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }

                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        ///  Gets the user details
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static User GetUserDetails(int userId)
        {
            User user = new User();
            IDataReader drUserDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetUserDetails))
                {
                    db.AddInParameter(dbCommand, "@userId ", DbType.Int32, userId);

                    drUserDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drUserDetails.Read())
                    {
                        {
                            user = new User()
                            {
                                UserId = Convert.ToInt32(drUserDetails["UserId"], CultureInfo.InvariantCulture),
                                LastName = Convert.ToString(drUserDetails["LastName"], CultureInfo.InvariantCulture),
                                FirstName = Convert.ToString(drUserDetails["FirstName"], CultureInfo.InvariantCulture),
                                //Company = Convert.ToString(drUserDetails["Company"], CultureInfo.InvariantCulture),
                                HiltonUserId = Convert.IsDBNull(drUserDetails["HiltonId"]) ? 0 : Convert.ToInt32(drUserDetails["HiltonId"], CultureInfo.InvariantCulture),
                                HiltonUserName = Convert.IsDBNull(drUserDetails["HiltonUserName"]) ? string.Empty : Convert.ToString(drUserDetails["HiltonUserName"], CultureInfo.InvariantCulture),
                                Email = Convert.ToString(drUserDetails["Email"], CultureInfo.InvariantCulture),
                                UserStatusId = Convert.ToInt32(drUserDetails["StatusId"], CultureInfo.InvariantCulture)
                            };
                        }
                    }

                    drUserDetails.NextResult();
                    user.UserAccessLogs = new List<UserAccessLog>();
                    while (drUserDetails.Read())
                    {
                        user.UserAccessLogs.Add(new UserAccessLog
                        {
                            LogOn = Convert.ToDateTime(drUserDetails["LoginDate"], CultureInfo.InvariantCulture)
                        });
                    }

                    drUserDetails.NextResult();
                    user.ActivationEmailDetails = new List<UserActivationEmail>();
                    while (drUserDetails.Read())
                    {
                        user.ActivationEmailDetails.Add(new UserActivationEmail
                        {
                            Email = Convert.ToString(drUserDetails["Email"], CultureInfo.InvariantCulture),
                            RequestSentDateTime = Convert.ToDateTime(drUserDetails["RequestSendDateTime"], CultureInfo.InvariantCulture)
                        });
                    }

                    drUserDetails.NextResult(); //To get the associated project info for user
                    user.AssociatedProjects = new List<AssociatedProject>();
                    while (drUserDetails.Read())
                    {
                        user.AssociatedProjects.Add(new AssociatedProject()
                        {
                            Property = new Facility()
                            {
                                FacilityUniqueName = Convert.ToString(drUserDetails["FacilityName"], CultureInfo.InvariantCulture),
                                Brand = Convert.ToString(drUserDetails["BrandName"], CultureInfo.InvariantCulture),
                                Address = Convert.ToString(drUserDetails["Address1"], CultureInfo.InvariantCulture),
                                Address2 = Convert.ToString(drUserDetails["Address2"], CultureInfo.InvariantCulture),
                                City = Convert.ToString(drUserDetails["CityName"], CultureInfo.InvariantCulture),
                                State = Convert.ToString(drUserDetails["StateName"], CultureInfo.InvariantCulture),
                                ZipCode = Convert.ToString(drUserDetails["ZipCode"], CultureInfo.InvariantCulture),
                                Country = Convert.ToString(drUserDetails["CountryName"], CultureInfo.InvariantCulture)
                            },
                            User = new User()
                            {
                                UserRole = Convert.ToString(drUserDetails["RoleDesc"], CultureInfo.InvariantCulture),
                                UserRoleId = Convert.ToInt32(drUserDetails["RoleId"], CultureInfo.InvariantCulture)
                            },
                            ProjectId = Convert.ToInt32(drUserDetails["ProjectId"], CultureInfo.InvariantCulture),
                            ProjectTypeId = Convert.ToInt32(drUserDetails["ProjectTypeId"], CultureInfo.InvariantCulture),
                            ProjectTypeDescription = Convert.ToString(drUserDetails["ProjectTypeName"], CultureInfo.InvariantCulture),
                            ProjectAddedDate = drUserDetails["ProjectAddedDate"] != DBNull.Value ?
                                   (DateTime?)Convert.ToDateTime(drUserDetails["ProjectAddedDate"], CultureInfo.InvariantCulture) : null,

                        });
                    }

                    drUserDetails.NextResult(); //To select the regions for a admin
                    user.AdminPermission = new AdminPermission();
                    user.AdminPermission.Regions = new List<Region>();
                    while (drUserDetails.Read())
                    {
                        user.AdminPermission.Regions.Add(new Region()
                        {
                            RegionId = Convert.ToInt32(drUserDetails["RegionId"], CultureInfo.InvariantCulture)
                        });
                    }

                    drUserDetails.NextResult(); //To select permission for a admin
                    user.AdminPermission.Permissions = new List<Permission>();
                    while (drUserDetails.Read())
                    {
                        user.AdminPermission.Permissions.Add(new Permission()
                        {
                            PermissionId = Convert.ToInt32(drUserDetails["AdminPermissionId"], CultureInfo.InvariantCulture),
                            PermissionDescription = Convert.ToString(drUserDetails["AdminPermissionDesc"], CultureInfo.InvariantCulture),
                            LinkenMenuId = Convert.IsDBNull(drUserDetails["LinkedMenuId"]) ? 0 : Convert.ToInt16(drUserDetails["LinkedMenuId"], CultureInfo.InvariantCulture),
                        });
                    }

                    drUserDetails.NextResult(); //To select the user type of user
                    if (drUserDetails.Read())
                    {
                        user.UserTypeId = Convert.ToInt32(drUserDetails["UserTypeId"], CultureInfo.InvariantCulture);
                        user.UserTypeDescription = Convert.ToString(drUserDetails["UserTypeDesc"], CultureInfo.InvariantCulture);
                    }

                    //drUserDetails.NextResult(); ///To select the company of user
                    //if (drUserDetails.Read())
                    //{
                    //    user.Company = Convert.ToString(drUserDetails["Company"], CultureInfo.InvariantCulture);
                    //}

                    drUserDetails.NextResult(); //To select the categories for user access

                    user.AccessibleCategoryList = new List<Category>();
                    while (drUserDetails.Read())
                    {
                        user.AccessibleCategoryList.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drUserDetails["CatId"], CultureInfo.InvariantCulture),
                            ParentCategoryHierarchy = Convert.ToString(drUserDetails["ParentCategoryHierarchy"], CultureInfo.InvariantCulture),
                            AccessibleForUser = true
                        });
                    }
                    drUserDetails.NextResult(); //To select permission for a admin
                    user.AssociatedPartners = new List<Partner>();
                    while (drUserDetails.Read())
                    {
                        user.AssociatedPartners.Add(new Partner()
                        {
                            PartnerId = Convert.ToString(drUserDetails["PartnerId"], CultureInfo.InvariantCulture),
                            CompanyDescription = Convert.ToString(drUserDetails["CompanyDescription"], CultureInfo.InvariantCulture),
                        });
                    }

                    drUserDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drUserDetails != null && !drUserDetails.IsClosed)
                {
                    drUserDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return user;
        }

        /// <summary>
        ///  Gets the user list
        /// </summary>
        /// <param name="userSearch"></param>
        /// <returns></returns>
        public static IList<User> GetUserList(UserSearch userSearch, ref int totalRecordCount)
        {
            IList<User> userCollection = new List<User>();
            IDataReader drUsers = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetUserList))
                {
                    if (String.IsNullOrEmpty(userSearch.StatusId))
                        db.AddInParameter(dbCommand, "@userStatusId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@userStatusId", DbType.String, userSearch.StatusId);

                    if (String.IsNullOrEmpty(userSearch.UserId))
                        db.AddInParameter(dbCommand, "@userId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@userId", DbType.String, userSearch.UserId);

                    if (String.IsNullOrEmpty(userSearch.TypeId))
                        db.AddInParameter(dbCommand, "@userTypeId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@userTypeId", DbType.String, userSearch.TypeId);

                    if (userSearch.LastLoginTo == null)
                        db.AddInParameter(dbCommand, "@lastLoginTo", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@lastLoginTo", DbType.DateTime, userSearch.LastLoginTo);

                    if (userSearch.LastLoginFrom == null)
                        db.AddInParameter(dbCommand, "@lastLoginFrom", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@lastLoginFrom", DbType.DateTime, userSearch.LastLoginFrom);

                    if (userSearch.ActivateDateTo == null)
                        db.AddInParameter(dbCommand, "@activationDateTo", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@activationDateTo", DbType.DateTime, userSearch.ActivateDateTo);

                    if (userSearch.ActivationDateFrom == null)
                        db.AddInParameter(dbCommand, "@activationDateFrom", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@activationDateFrom", DbType.DateTime, userSearch.ActivationDateFrom);

                    db.AddInParameter(dbCommand, "@searchSource", DbType.String, userSearch.SearchSource);
                    db.AddInParameter(dbCommand, "@searchExpression", DbType.String, userSearch.QuickSearchExpression);
                    db.AddInParameter(dbCommand, "@searchType", DbType.String, userSearch.SearchFlag);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, userSearch.PageIndex);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, userSearch.PageSize);
                    db.AddInParameter(dbCommand, "@sortExpression", DbType.String, (String.IsNullOrEmpty(userSearch.SortExpression)) ? (object)DBNull.Value : userSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, userSearch.SortDirection);
                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drUsers = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drUsers.Read())
                    {
                        {
                            userCollection.Add(new User()
                            {
                                UserId = Convert.ToInt32(drUsers["UserId"], CultureInfo.InvariantCulture),
                                LastName = Convert.ToString(drUsers["LastName"], CultureInfo.InvariantCulture),
                                FirstName = Convert.ToString(drUsers["FirstName"], CultureInfo.InvariantCulture),
                                Company = Convert.ToString(drUsers["Company"], CultureInfo.InvariantCulture),
                                HiltonUserName = Convert.ToString(drUsers["HiltonUserId"], CultureInfo.InvariantCulture),
                                UserTypeId = Convert.ToInt32(drUsers["UserTypeId"], CultureInfo.InvariantCulture),
                                UserTypeDescription = Convert.ToString(drUsers["UserTypeDesc"], CultureInfo.InvariantCulture),
                                UserStatus = Convert.ToString(drUsers["UserStatusDesc"], CultureInfo.InvariantCulture),
                                ActivationDate = drUsers["ActivationDate"] != DBNull.Value ?
                                    (DateTime?)Convert.ToDateTime(drUsers["ActivationDate"], CultureInfo.InvariantCulture) : null,
                                LastLogOn = drUsers["LastLoginDate"] != DBNull.Value ?
                                    (DateTime?)Convert.ToDateTime(drUsers["LastLoginDate"], CultureInfo.InvariantCulture) : null,
                            }
                            );
                        }
                    }

                    drUsers.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);

                    drUsers.Close();
                }
            }
            catch (Exception exception)
            {
                if (drUsers != null && !drUsers.IsClosed)
                {
                    drUsers.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userCollection;
        }

        /// <summary>
        /// Search the projects for a given project type, brand and region
        /// </summary>
        /// <param name="projectTypeId"></param>
        /// <param name="brandId"></param>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public static IList<AssociatedProject> SearchProject(int projectTypeId, int brandId, int regionId)
        {
            IList<AssociatedProject> projectCollection = new List<AssociatedProject>();
            IDataReader drProjectsDetail = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectsForUser))
                {
                    db.AddInParameter(dbCommand, "@projectTypeId ", DbType.Int32, projectTypeId);
                    db.AddInParameter(dbCommand, "@brandId ", DbType.Int32, brandId);
                    db.AddInParameter(dbCommand, "@regionId ", DbType.Int32, regionId);

                    drProjectsDetail = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProjectsDetail.Read())
                    {
                        projectCollection.Add(new AssociatedProject()
                        {
                            Property = new Facility()
                            {
                                FacilityUniqueName = Convert.ToString(drProjectsDetail["FacilityName"], CultureInfo.InvariantCulture),
                                Brand = Convert.ToString(drProjectsDetail["BrandName"], CultureInfo.InvariantCulture),
                                Address = Convert.ToString(drProjectsDetail["Address1"], CultureInfo.InvariantCulture),
                                Address2 = Convert.ToString(drProjectsDetail["Address2"], CultureInfo.InvariantCulture),
                                City = Convert.ToString(drProjectsDetail["CityName"], CultureInfo.InvariantCulture),
                                State = Convert.ToString(drProjectsDetail["StateName"], CultureInfo.InvariantCulture),
                                ZipCode = Convert.ToString(drProjectsDetail["ZipCode"], CultureInfo.InvariantCulture),
                                Country = Convert.ToString(drProjectsDetail["CountryName"], CultureInfo.InvariantCulture),
                            },
                            ProjectId = Convert.ToInt32(drProjectsDetail["ProjectId"], CultureInfo.InvariantCulture),
                            ProjectTypeId = Convert.ToInt32(drProjectsDetail["ProjectTypeId"], CultureInfo.InvariantCulture),
                            ProjectTypeDescription = Convert.ToString(drProjectsDetail["ProjectTypeName"], CultureInfo.InvariantCulture),
                            ProjectAddedDate = Convert.IsDBNull(drProjectsDetail["ProjectAddedDate"]) ? null : (DateTime?)(drProjectsDetail["ProjectAddedDate"]),
                            ProjectOwner = Convert.ToString(drProjectsDetail["Owner"], CultureInfo.InvariantCulture)
                        });
                    }

                    drProjectsDetail.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectsDetail != null && !drProjectsDetail.IsClosed)
                {
                    drProjectsDetail.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectCollection;
        }

        /// <summary>
        /// Update the user details
        /// </summary>
        /// <param name="user"></param>
        /// <param name="modifiedByUserId"></param>
        /// <returns></returns>
        public static int UpdateUser(User user, int modifiedByUserId)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateUserDetails))
                {
                    if (user.UserTypeId == (int)UserTypeEnum.Owner || user.UserTypeId == (int)UserTypeEnum.Consultant)
                    {
                        SetUserProjectMappingData(dbCommand, user.AssociatedProjects.Where(p => p.IsToBeAdded == true).ToList());
                        SetRemoveUserProjectMappingData(dbCommand, user.AssociatedProjects.Where(p => p.IsToBeRemoved == true).ToList());
                    }
                    if (user.UserTypeId == (int)UserTypeEnum.Administrator)
                    {
                        SetAdminPermissionData(dbCommand, user.AdminPermission);
                        SetCategoryAddition(dbCommand, user.AccessibleCategoryList.Where(p => p.AccessibleForUser == true).ToList());
                        SetCategoryRemoval(dbCommand, user.AccessibleCategoryList.Where(p => p.UnaccessibleForUser == true).ToList());
                    }
                    if (user.UserTypeId == (int)UserTypeEnum.Partner)
                    {
                        SetUserPartnerMappingData(dbCommand, user.AssociatedPartners);
                    }

                    string hiltonUserName = (user.HiltonUserName == string.Empty) ? null : user.HiltonUserName;
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, user.UserId);
                    db.AddInParameter(dbCommand, "@firstName", DbType.String, user.FirstName);
                    db.AddInParameter(dbCommand, "@lastName", DbType.String, user.LastName);
                    db.AddInParameter(dbCommand, "@email", DbType.String, user.Email);
                    db.AddInParameter(dbCommand, "@hiltonId", DbType.String, hiltonUserName);
                    db.AddInParameter(dbCommand, "@userStatusId", DbType.Int32, user.UserStatusId);
                    //db.AddInParameter(dbCommand, "@company", DbType.String, user.Company);
                    db.AddInParameter(dbCommand, "@userTypeId", DbType.Int32, user.UserTypeId);
                    db.AddInParameter(dbCommand, "@modifiedByUserId", DbType.Int32, modifiedByUserId); //will be fetched from session variable
                    db.AddInParameter(dbCommand, "@culture", DbType.String, user.Culture);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Set the admin permission data to be saved in data base
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="adminPermission"></param>
        private static void SetAdminPermissionData(DbCommand dbCommand, AdminPermission adminPermission)
        {
            //Bind the regions
            using (DataTable regionTables = new DataTable())
            {
                regionTables.Locale = CultureInfo.InvariantCulture;
                regionTables.Columns.Add(new DataColumn("RegionId", typeof(Int32)));
                foreach (var region in adminPermission.Regions)
                {
                    var row = regionTables.NewRow();
                    row["RegionId"] = region.RegionId;
                    regionTables.Rows.Add(row);
                }

                SqlParameter regions = new SqlParameter("@adminRegion", SqlDbType.Structured);
                regions.Direction = ParameterDirection.Input;
                regions.Value = regionTables;
                dbCommand.Parameters.Add(regions);
            }

            //Bind the Permissions
            using (DataTable permissionTables = new DataTable())
            {
                permissionTables.Locale = CultureInfo.InvariantCulture;
                permissionTables.Columns.Add(new DataColumn("PermissionId", typeof(Int32)));
                foreach (var permission in adminPermission.Permissions)
                {
                    var row = permissionTables.NewRow();
                    row["PermissionId"] = permission.PermissionId;
                    permissionTables.Rows.Add(row);
                }

                SqlParameter permissions = new SqlParameter("@adminPermission", SqlDbType.Structured);
                permissions.Direction = ParameterDirection.Input;
                permissions.Value = permissionTables;
                dbCommand.Parameters.Add(permissions);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="categoryCollection"></param>
        private static void SetCategoryAddition(DbCommand dbCommand, IList<Category> categoryCollection)
        {
            using (DataTable categoryAdditionTable = new DataTable())
            {
                categoryAdditionTable.Locale = CultureInfo.InvariantCulture;
                categoryAdditionTable.Columns.Add(new DataColumn("categoryId", typeof(string)));

                foreach (var category in categoryCollection)
                {
                    var row = categoryAdditionTable.NewRow();
                    row["CategoryId"] = category.CategoryId;
                    categoryAdditionTable.Rows.Add(row);
                }

                SqlParameter standardMapping = new SqlParameter("@adminCategoriesAccess", SqlDbType.Structured);
                standardMapping.Direction = ParameterDirection.Input;
                standardMapping.Value = categoryAdditionTable;
                dbCommand.Parameters.Add(standardMapping);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="categoryStandardList"></param>
        private static void SetCategoryRemoval(DbCommand dbCommand, IList<Category> categoryCollection)
        {
            using (DataTable categoryAdditionTable = new DataTable())
            {
                categoryAdditionTable.Locale = CultureInfo.InvariantCulture;
                categoryAdditionTable.Columns.Add(new DataColumn("categoryId", typeof(string)));

                foreach (var category in categoryCollection)
                {
                    var row = categoryAdditionTable.NewRow();
                    row["CategoryId"] = category.CategoryId;
                    categoryAdditionTable.Rows.Add(row);
                }

                SqlParameter standardMapping = new SqlParameter("@adminCategoriesRemoval", SqlDbType.Structured);
                standardMapping.Direction = ParameterDirection.Input;
                standardMapping.Value = categoryAdditionTable;
                dbCommand.Parameters.Add(standardMapping);
            }
        }

        /// <summary>
        ///  Set the user project mapping
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="associatedProjects"></param>
        private static void SetRemoveUserProjectMappingData(DbCommand dbCommand, IList<AssociatedProject> associatedProjects)
        {
            using (DataTable projectAssciation = new DataTable())
            {
                projectAssciation.Locale = CultureInfo.InvariantCulture;
                projectAssciation.Columns.Add(new DataColumn("ProjectId", typeof(Int32)));
                projectAssciation.Columns.Add(new DataColumn("RoleId", typeof(Int32)));
                projectAssciation.Columns.Add(new DataColumn("ConsultantRoleId", typeof(Int32)));
                foreach (var project in associatedProjects)
                {
                    var row = projectAssciation.NewRow();
                    row["ProjectId"] = project.ProjectId;
                    if (project.User.UserRoleId == (int)UserTypeEnum.Owner)
                    {
                        row["RoleId"] = project.User.UserRoleId;
                        row["ConsultantRoleId"] = DBNull.Value;
                    }
                    else
                    {
                        row["RoleId"] = (int)UserTypeEnum.Consultant;
                        row["ConsultantRoleId"] = project.User.UserRoleId;
                    }
                    projectAssciation.Rows.Add(row);
                }

                SqlParameter projects = new SqlParameter("@removeUserProjectMapping", SqlDbType.Structured);
                projects.Direction = ParameterDirection.Input;
                projects.Value = projectAssciation;
                dbCommand.Parameters.Add(projects);
            }
        }

        /// <summary>
        ///  Set user project mapping
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="associatedProjects"></param>
        private static void SetUserProjectMappingData(DbCommand dbCommand, IList<AssociatedProject> associatedProjects)
        {
            using (DataTable projectAssciation = new DataTable())
            {
                projectAssciation.Locale = CultureInfo.InvariantCulture;
                projectAssciation.Columns.Add(new DataColumn("ProjectId", typeof(Int32)));
                projectAssciation.Columns.Add(new DataColumn("RoleId", typeof(Int32)));
                projectAssciation.Columns.Add(new DataColumn("ConsultantRoleId", typeof(Int32)));
                foreach (var project in associatedProjects)
                {
                    var row = projectAssciation.NewRow();
                    row["ProjectId"] = project.ProjectId;
                    if (project.User.UserRoleId == (int)UserTypeEnum.Owner)
                    {
                        row["RoleId"] = project.User.UserRoleId;
                        row["ConsultantRoleId"] = DBNull.Value;
                    }
                    else
                    {
                        row["RoleId"] = (int)UserTypeEnum.Consultant;
                        row["ConsultantRoleId"] = project.User.UserRoleId;
                    }
                    projectAssciation.Rows.Add(row);
                }

                SqlParameter projects = new SqlParameter("@userProjectMapping", SqlDbType.Structured);
                projects.Direction = ParameterDirection.Input;
                projects.Value = projectAssciation;
                dbCommand.Parameters.Add(projects);
            }
        }
        private static void SetUserPartnerMappingData(DbCommand dbCommand, IList<Partner> associatedPartners)
        {
            using (DataTable partnersAssociation = new DataTable())
            {
                partnersAssociation.Locale = CultureInfo.InvariantCulture;
                partnersAssociation.Columns.Add(new DataColumn("PartnerId", typeof(int)));

                foreach (var partner in associatedPartners)
                {
                    var row = partnersAssociation.NewRow();
                    row["PartnerId"] = partner.PartnerId;
                    partnersAssociation.Rows.Add(row);
                }

                SqlParameter partners = new SqlParameter("@userPartnerMapping", SqlDbType.Structured);
                partners.Direction = ParameterDirection.Input;
                partners.Value = partnersAssociation;
                dbCommand.Parameters.Add(partners);
            }
        }


        /// <summary>
        /// Validate the user project mapping
        /// </summary>
        /// <param name="activationCode"></param>
        /// <returns></returns>
        public static int ValidateUserProjectMapping(string activationCode)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.VerifyUserProjectMapping))
                {
                    db.AddInParameter(dbCommand, "@activationCode", DbType.String, activationCode);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Get the activation emails
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static IList<UserActivationEmail> GetActivationEmails(int userId)
        {
            IList<UserActivationEmail> activationEmails = null;
            IDataReader drActivationEmails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetUserActivationEmails))
                {
                    db.AddInParameter(dbCommand, "@userId ", DbType.Int32, userId);

                    drActivationEmails = (IDataReader)db.ExecuteReader(dbCommand);

                    activationEmails = new List<UserActivationEmail>();
                    while (drActivationEmails.Read())
                    {
                        activationEmails.Add(new UserActivationEmail
                        {
                            Email = Convert.ToString(drActivationEmails["Email"], CultureInfo.InvariantCulture),
                            RequestSentDateTime = Convert.ToDateTime(drActivationEmails["RequestSendDateTime"], CultureInfo.InvariantCulture)
                        });
                    }

                    drActivationEmails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drActivationEmails != null && !drActivationEmails.IsClosed)
                {
                    drActivationEmails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return activationEmails;
        }
    }
}