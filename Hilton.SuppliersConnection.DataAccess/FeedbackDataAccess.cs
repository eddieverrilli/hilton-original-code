﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class FeedbackDataAccess
    {
        /// <summary>
        /// Adds A Feedback
        /// </summary>
        /// <param name="feedback"></param>
        /// <returns>Boolean value</returns>
        public static bool AddFeedback(Feedback feedback)
        {
            bool result = false;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.AddFeedback))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, feedback.PartnerId);
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, feedback.ProductId);
                    if(feedback.SenderId != 0)
                        db.AddInParameter(dbCommand, "@senderId", DbType.Int32, feedback.SenderId);
                    else
                        db.AddInParameter(dbCommand, "@senderId", DbType.Int32, DBNull.Value);
                    db.AddInParameter(dbCommand, "@senderEmail", DbType.String, feedback.SenderEmail);
                    db.AddInParameter(dbCommand, "@feedbackDetails", DbType.String, feedback.Details);
                    db.AddInParameter(dbCommand, "@raitingId", DbType.Int32, feedback.RatingId);
                    db.AddInParameter(dbCommand, "@status", DbType.Int32, feedback.FeedbackStatusId);
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, feedback.TemplateId);
                    db.AddInParameter(dbCommand, "@toFirstName", DbType.String, feedback.ToFirstName);
                    db.AddInParameter(dbCommand, "@toLastName", DbType.String, feedback.ToLastName);
                    db.AddInParameter(dbCommand, "@toEmail", DbType.String, feedback.ToEmail);
                    db.AddInParameter(dbCommand, "@culture", DbType.String, feedback.culture);
                    db.AddInParameter(dbCommand, "@senderFirstName", DbType.String, feedback.SenderFirstName);
                    db.AddInParameter(dbCommand, "@senderLastName", DbType.String, feedback.SenderLastName);
                    db.AddInParameter(dbCommand, "@senderHiltonId", DbType.String, feedback.SenderHiltonId);
                    if (db.ExecuteNonQuery(dbCommand) > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        public static bool AddFeedbackComment(FeedbackComment comment)
        {
            bool result = false;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsFeedbackComment))
                {
                    db.AddInParameter(dbCommand, "@feedbackId", DbType.Int32, comment.FeedbackId);
                    db.AddInParameter(dbCommand, "@submittedBy", DbType.Int32, comment.Submittedby);
                  
                    db.AddInParameter(dbCommand, "@comment", DbType.String, comment.Comment);
                   
                    if (db.ExecuteNonQuery(dbCommand) > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
      

        /// <summary>
        /// Gets the Feedback List
        /// </summary>
        /// <param name="feedbackSearch"></param>
        /// <returns>List of Feedback</returns>
        public static IList<Feedback> GetFeedbackList(FeedbackSearch feedbackSearch, ref int totalRecordCount)
        {
            IList<Feedback> feedbackCollection = new List<Feedback>();
             IList<FeedbackComment> commentCollection = new List<FeedbackComment>();
            IDataReader drFeedback = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetFeedbackList))
                {
                    if (feedbackSearch.DateFrom == null)
                        db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, feedbackSearch.DateFrom);

                    if (feedbackSearch.DateTo == null)
                        db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, feedbackSearch.DateTo);

                    if (string.IsNullOrEmpty(feedbackSearch.PartnerId))
                        db.AddInParameter(dbCommand, "@partnerId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@partnerId", DbType.String, feedbackSearch.PartnerId);

                    if (string.IsNullOrEmpty(feedbackSearch.RatingId))
                        db.AddInParameter(dbCommand, "@ratingId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@ratingId", DbType.String, feedbackSearch.RatingId);

                    if (string.IsNullOrEmpty(feedbackSearch.StatusId))
                        db.AddInParameter(dbCommand, "@statusId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@statusId", DbType.String, feedbackSearch.StatusId);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, feedbackSearch.UserId);
                    db.AddInParameter(dbCommand, "@searchExpression", DbType.String, feedbackSearch.QuickSearchExpression);
                    db.AddInParameter(dbCommand, "@searchType", DbType.String, feedbackSearch.SearchFlag);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, feedbackSearch.PageIndex);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, feedbackSearch.PageSize);
                    if (string.IsNullOrEmpty(feedbackSearch.SortExpression))
                        db.AddInParameter(dbCommand, "@sortExpression", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@sortExpression", DbType.String, feedbackSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, feedbackSearch.SortDirection);
                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drFeedback = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drFeedback.Read())
                    {
                        {
                            feedbackCollection.Add(new Feedback()
                            {
                                FeedbackId = Convert.ToInt32(drFeedback["FeedbackId"], CultureInfo.InvariantCulture),
                                PartnerCompany = Convert.ToString(drFeedback["CompanyName"], CultureInfo.InvariantCulture),
                                ProductName = Convert.ToString(drFeedback["ProductName"], CultureInfo.InvariantCulture),
                                RatingDescription = Convert.ToString(drFeedback["RaitingName"], CultureInfo.InvariantCulture),
                                Details = Convert.ToString(drFeedback["FeedbackDetails"], CultureInfo.InvariantCulture),
                                SubmittedDateTime = Convert.ToDateTime(drFeedback["SubmittedDate"], CultureInfo.InvariantCulture),
                                From = Convert.ToString(drFeedback["FirstName"], CultureInfo.InvariantCulture) + " " + Convert.ToString(drFeedback["LastName"], CultureInfo.InvariantCulture),
                                FeedbackStatusId = Convert.ToInt16(drFeedback["Status"], CultureInfo.InvariantCulture),
                                FeedbackStatusDesc = Convert.ToString(drFeedback["FeedbackStatusDesc"], CultureInfo.InvariantCulture)
                            }
                            );
                        }
                    }
                    
                    drFeedback.NextResult();
                    while (drFeedback.Read())
                    {
                        {
                            commentCollection.Add(new FeedbackComment()
                            {
                                FeedbackId = Convert.ToInt32(drFeedback["FeedbackId"], CultureInfo.InvariantCulture),
                                CommentId = Convert.ToInt32(drFeedback["CommentId"], CultureInfo.InvariantCulture),
                                Comment = Convert.ToString(drFeedback["Comment"], CultureInfo.InvariantCulture),
                                SubmittedbyFullName = Convert.ToString(drFeedback["Submittedby"], CultureInfo.InvariantCulture),
                                SubmittedDate = Convert.ToDateTime(drFeedback["SubmittedDate"], CultureInfo.InvariantCulture)
                            }
                            );

                        }
                    }

                    drFeedback.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);


                }

                drFeedback.Close();

                
                foreach(Feedback feedback in feedbackCollection)
                {
                    List<FeedbackComment> feedbackComments = new List<FeedbackComment>();

                    feedbackComments = commentCollection.Where(c => c.FeedbackId == feedback.FeedbackId).ToList<FeedbackComment>();
                    if (feedbackComments != null)
                        feedback.FeedbackComments = feedbackComments.OrderByDescending(c=>c.SubmittedDate).ToList();
                    
                    //feedback.Details += "<br/>" + string.Join("<br/>", commentCollection.Where(c => c.FeedbackId == feedback.FeedbackId).OrderByDescending(c=>c.Submittedby).Select(c => "By: "+ c.SubmittedbyFullName+" Added:" +c.SubmittedDate.ToString("f") + " Comment:" +c.Comment ));      
                }
            }
            catch (Exception exception)
            {
                if (drFeedback != null && !drFeedback.IsClosed)
                {
                    drFeedback.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return feedbackCollection;
        }

        /// <summary>
        /// Update a Feedback
        /// </summary>
        /// <param name="feedback"></param>
        /// <returns></returns>
        public static int UpdateFeedback(IList<Feedback> feedbacks)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateFeedbacks))
                {
                    DataTable feedbackUpdatesDataTable;
                    // create data table to insert items
                    using (feedbackUpdatesDataTable = new DataTable("ListUpdates"))
                    {
                        feedbackUpdatesDataTable.Locale = CultureInfo.InvariantCulture;
                        feedbackUpdatesDataTable.Columns.Add("RecordId", typeof(int));
                        feedbackUpdatesDataTable.Columns.Add("StatusId", typeof(int));

                        feedbacks.ToList().ForEach(x =>
                        {
                            feedbackUpdatesDataTable.Rows.Add(x.FeedbackId, x.FeedbackStatusId);
                        });
                        SqlParameter feedbackUpdatesParam = new SqlParameter("@feedbacks", feedbackUpdatesDataTable);
                        feedbackUpdatesParam.SqlDbType = SqlDbType.Structured;
                        feedbackUpdatesParam.Direction = ParameterDirection.Input;
                        dbCommand.Parameters.Add(feedbackUpdatesParam);

                        SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                        retValParam.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(retValParam);

                        db.ExecuteNonQuery(dbCommand);
                        result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}