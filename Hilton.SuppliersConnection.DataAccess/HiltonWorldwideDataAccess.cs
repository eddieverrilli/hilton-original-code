﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class HiltonWorldwideDataAccess
    {
        /// <summary>
        /// Get Hilton WorldwideData
        /// </summary>
        /// <returns>HiltonWorldwide</returns>
        public static HiltonWorldwide GetHiltonWorldwideData(string brandCode, string regionCode, string propertyTypeCode)
        {
            IDataReader drHiltonWorldwide = null;

            HiltonWorldwide hiltonWorldwideDataList = new HiltonWorldwide();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetHiltonWorldwideData))
                {
                    db.AddInParameter(dbCommand, "@brandCode", DbType.String, brandCode);
                    db.AddInParameter(dbCommand, "@regionCode", DbType.String, regionCode);
                    db.AddInParameter(dbCommand, "@propertyTypeName", DbType.String, propertyTypeCode);
                    drHiltonWorldwide = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drHiltonWorldwide.Read())
                    {
                        hiltonWorldwideDataList = new HiltonWorldwide()
                        {
                            BrandId = Convert.IsDBNull(drHiltonWorldwide["BrandId"]) ? null : Convert.ToString(drHiltonWorldwide["BrandId"], CultureInfo.InvariantCulture),
                            PropertyTypeId = Convert.IsDBNull(drHiltonWorldwide["PropertyTypeId"]) ? null : Convert.ToString(drHiltonWorldwide["PropertyTypeId"], CultureInfo.InvariantCulture),
                            RegionId = Convert.IsDBNull(drHiltonWorldwide["RegionId"]) ? null : Convert.ToString(drHiltonWorldwide["RegionId"], CultureInfo.InvariantCulture),
                        };
                    }

                    drHiltonWorldwide.Close();
                }
            }
            catch (Exception exception)
            {
                if (drHiltonWorldwide != null && !drHiltonWorldwide.IsClosed)
                {
                    drHiltonWorldwide.Close();
                }

                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return hiltonWorldwideDataList;
        }
    }
}