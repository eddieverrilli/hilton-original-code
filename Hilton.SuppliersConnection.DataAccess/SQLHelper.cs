﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Hilton.Infrastructure.Encryption;

namespace Hilton.SuppliersConnection.DataAccess
{
    class SQLHelper
    {
        public static string GetConnectionString()
        {
            return ConStringSingleton.GetInstance();
        }
    }


    public sealed class ConStringSingleton
    {
        static String ConnectionString = "";

        static ConStringSingleton instance = null;

        ConStringSingleton()
        {
            String strkey = "", strdecpwd = "", strdecUserId = "";

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;
            SqlConnectionStringBuilder constringBuilder = new SqlConnectionStringBuilder(connectionString);
            strkey = System.Configuration.ConfigurationManager.AppSettings["EncryptionDecryptionKey"].ToString();
            strdecpwd = AESEncryption.AESDecryptText(constringBuilder.Password, strkey);
            strdecUserId = AESEncryption.AESDecryptText(constringBuilder.UserID, strkey);
            constringBuilder.UserID = strdecUserId;
            constringBuilder.Password = strdecpwd;
            ConnectionString = constringBuilder.ConnectionString;
           // ConnectionString =  "Data Source=10.81.47.24;Initial Catalog=SuppliersConnectionSit;User Id=SCApp;Password=P@ssW0rd";
        }

        public static string GetInstance()
        {
            if (instance == null)
            {
                instance = new ConStringSingleton();
            }
            return ConnectionString;
        }
    }
}
