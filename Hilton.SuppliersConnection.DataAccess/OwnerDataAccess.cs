﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class OwnerDataAccess
    {
        /// <summary>
        /// To Add Project User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int AddProjectUser(User user)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.AddProjectUser))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, user.ProjectId);
                    db.AddInParameter(dbCommand, "@firstName", DbType.String, user.FirstName);
                    db.AddInParameter(dbCommand, "@lastName ", DbType.String, user.LastName);
                    db.AddInParameter(dbCommand, "@email", DbType.String, user.Email);
                    if (user.UserRoleId.HasValue)
                    {
                        db.AddInParameter(dbCommand, "@userRoleId", DbType.Int32, user.UserRoleId);
                    }
                    db.AddInParameter(dbCommand, "@receivePartnerCommunication", DbType.Boolean, user.ReceivePartnerCommunication);
                    db.AddInParameter(dbCommand, "@updateByUserId", DbType.Int32, user.UpdatedByUserId);
                    db.AddInParameter(dbCommand, "@culture", DbType.String, user.Culture);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Export all partner option
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public static DataSet ExportAllPartnerOption(int projectId)
        {
            DataSet projectProfileDataSet = null;
            try
            {
                using (projectProfileDataSet = new DataSet())
                {
                    projectProfileDataSet.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetAllProductForProjectProfile);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    projectProfileDataSet = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectProfileDataSet;
        }

        /// <summary>
        /// Export selected partner option
        /// </summary>
        /// <param name="partnerOption"></param>
        /// <returns></returns>
        public static DataSet ExportSelectedPartners(int projectId, int userId)
        {
            DataSet projectProfileDataSet = null;
            try
            {
                using (projectProfileDataSet = new DataSet())
                {
                    projectProfileDataSet.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectProfileByProjectId);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    projectProfileDataSet = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectProfileDataSet;
        }

        /// <summary>
        /// Get the project profile details of project id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public static ProjectDetail GetProjectProfile(int projectId, int userId)
        {
            IDataReader drProjectProfileDetails = null;
            ProjectDetail projectDetails = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectProfileByProjectId);
                db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                drProjectProfileDetails = (IDataReader)db.ExecuteReader(dbCommand);
                if (drProjectProfileDetails.Read())
                {
                    projectDetails = new ProjectDetail()
                    {
                        ProjectId = Convert.ToInt32(drProjectProfileDetails["ProjectId"], CultureInfo.InvariantCulture),
                        ProjectManager = Convert.ToString(drProjectProfileDetails["ProjectManager"], CultureInfo.InvariantCulture),
                        ProjectTypeName = Convert.ToString(drProjectProfileDetails["ProjectTypeName"], CultureInfo.InvariantCulture),
                        BrandName = Convert.ToString(drProjectProfileDetails["BrandName"], CultureInfo.InvariantCulture),
                        Address = Convert.ToString(drProjectProfileDetails["Address"], CultureInfo.InvariantCulture),
                        Address2 = Convert.ToString(drProjectProfileDetails["Address2"], CultureInfo.InvariantCulture),
                        ZipCode = Convert.ToString(drProjectProfileDetails["ZipCode"], CultureInfo.InvariantCulture),
                        City = Convert.ToString(drProjectProfileDetails["City"], CultureInfo.InvariantCulture),
                        Country = Convert.ToString(drProjectProfileDetails["Country"], CultureInfo.InvariantCulture),
                        CreatedDate = Convert.ToDateTime(drProjectProfileDetails["CreatedDate"], CultureInfo.InvariantCulture),
                        FacilityName = Convert.ToString(drProjectProfileDetails["FacilityUniqueName"], CultureInfo.InvariantCulture),
                        State = Convert.ToString(drProjectProfileDetails["State"], CultureInfo.InvariantCulture)
                    };
                }
                drProjectProfileDetails.NextResult();
                projectDetails.ProjectCategoriesDetails = new List<ProjectConfigDetails>();
                while (drProjectProfileDetails.Read())
                {
                    projectDetails.ProjectCategoriesDetails.Add(new ProjectConfigDetails()
                    {
                        ProjectId = projectDetails.ProjectId,
                        CategoryId = Convert.ToInt32(drProjectProfileDetails["CategoryId"], CultureInfo.InvariantCulture),
                        CategoryName = drProjectProfileDetails["CategoryName"].ToString(),
                        CategoryLevel = Convert.ToInt32(drProjectProfileDetails["Level"], CultureInfo.InvariantCulture),
                        IsLeaf = Convert.ToBoolean(drProjectProfileDetails["IsLeaf"], CultureInfo.InvariantCulture),
                        IsActive = Convert.ToBoolean(drProjectProfileDetails["IsActive"], CultureInfo.InvariantCulture),
                        ParentCategoryId = Convert.ToInt32(drProjectProfileDetails["ParentCategory"], CultureInfo.InvariantCulture),
                        RootParentId = Convert.ToInt32(drProjectProfileDetails["RootParentCatID"], CultureInfo.InvariantCulture),
                        HasProducts = Convert.ToBoolean(drProjectProfileDetails["HasProducts"], CultureInfo.InvariantCulture),
                        IsRequired = drProjectProfileDetails["IsRequired"] == DBNull.Value ? false : Convert.ToBoolean(drProjectProfileDetails["IsRequired"], CultureInfo.InvariantCulture),
                        CategoryDescription = drProjectProfileDetails["CatDesc"] == DBNull.Value ? null : Convert.ToString(drProjectProfileDetails["CatDesc"], CultureInfo.InvariantCulture),
                        SequenceId = Convert.ToInt32(drProjectProfileDetails["SequenceId"], CultureInfo.InvariantCulture)
                    });
                }

                drProjectProfileDetails.NextResult();
                IList<ProjectConfigPartnerProduct> partnerProducts = new List<ProjectConfigPartnerProduct>();
                while (drProjectProfileDetails.Read())
                {
                    ProjectConfigPartnerProduct product = new ProjectConfigPartnerProduct();
                    product.IsOther = Convert.ToBoolean(drProjectProfileDetails["IsOther"], CultureInfo.InvariantCulture);
                    product.PartnerName = Convert.IsDBNull(drProjectProfileDetails["PartnerName"]) ? string.Empty : drProjectProfileDetails["PartnerName"].ToString();
                    product.ProductName = drProjectProfileDetails["ProductName"].ToString();
                    product.CategoryId = Convert.ToInt32(drProjectProfileDetails["CatId"], CultureInfo.InvariantCulture);
                    product.SkuNumber = Convert.IsDBNull(drProjectProfileDetails["SKUNumber"]) ? string.Empty : drProjectProfileDetails["SKUNumber"].ToString();
                    partnerProducts.Add(product);
                }

                foreach (ProjectConfigDetails category in projectDetails.ProjectCategoriesDetails)
                {
                    category.LeafCategoryDetails = new ProjectConfigLeafCategoryDetail();
                    category.LeafCategoryDetails.PartnerProductDetails = new List<ProjectConfigPartnerProduct>();
                    foreach (ProjectConfigPartnerProduct partnerProduct in partnerProducts)
                    {
                        if (partnerProduct.CategoryId == category.CategoryId)
                        {
                            category.LeafCategoryDetails.PartnerProductDetails.Add(partnerProduct);
                        }
                    }
                }

                drProjectProfileDetails.Close();
            }
            catch (Exception exception)
            {
                if (drProjectProfileDetails != null && !drProjectProfileDetails.IsClosed)
                {
                    drProjectProfileDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return projectDetails;
        }

        /// <summary>
        /// Get the project list for owner
        /// </summary>
        /// <param name="myProjectSearch"></param>
        /// <returns></returns>
        public static IList<Project> GetProjectListForOwner(MyProjectsSearch myProjectSearch)
        {
            IList<Project> projectCollection = new List<Project>();
            IDataReader drProjectList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectListByUserId))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, myProjectSearch.UserId);
                    db.AddInParameter(dbCommand, "@sortExpression", DbType.String, (string.IsNullOrEmpty(myProjectSearch.SortExpression)) ? (object)DBNull.Value : myProjectSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, myProjectSearch.SortDirection);
                    drProjectList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProjectList.Read())
                    {
                        projectCollection.Add(new Project()
                        {
                            Property = new Facility()
                            {
                                Address = Convert.IsDBNull(drProjectList["Address"]) ? string.Empty : drProjectList["Address"].ToString(),
                                Address2 = Convert.IsDBNull(drProjectList["Address2"]) ? string.Empty : drProjectList["Address2"].ToString(),
                                ZipCode = Convert.IsDBNull(drProjectList["ZipCode"]) ? string.Empty : drProjectList["ZipCode"].ToString(),
                                City = Convert.IsDBNull(drProjectList["CityName"]) ? string.Empty : drProjectList["CityName"].ToString(),
                                State = Convert.IsDBNull(drProjectList["StateName"]) ? string.Empty : drProjectList["StateName"].ToString(),
                                BrandDescription = Convert.IsDBNull(drProjectList["BrandName"]) ? string.Empty : drProjectList["BrandName"].ToString(),
                                RegionDescription = Convert.IsDBNull(drProjectList["RegionName"]) ? string.Empty : drProjectList["RegionName"].ToString(),
                                Country = Convert.IsDBNull(drProjectList["Country"]) ? string.Empty : drProjectList["Country"].ToString(),
                                FacilityUniqueName = Convert.IsDBNull(drProjectList["FacilityName"]) ? string.Empty : drProjectList["FacilityName"].ToString(),
                            },
                            ProjectId = Convert.ToInt32(drProjectList["ProjectId"], CultureInfo.InvariantCulture),
                            ProjectTypeId = Convert.ToInt32(drProjectList["ProjectTypeId"], CultureInfo.InvariantCulture),
                            ProjectTypeDescription = drProjectList["ProjectTypeDescription"].ToString(),
                            ProjectAddedDate = Convert.ToDateTime(drProjectList["ProjectAddedDate"], CultureInfo.InvariantCulture),
                            ConfigPercent = Convert.ToDouble(drProjectList["ProjectConfiguration"], CultureInfo.InvariantCulture)
                        });
                    }

                    drProjectList.NextResult();
                    IList<User> associatedConsultants = new List<User>();
                    while (drProjectList.Read())
                    {
                        associatedConsultants.Add(new User()
                        {
                            UserId = Convert.ToInt32(drProjectList["UserId"], CultureInfo.InvariantCulture),
                            Name = drProjectList["Name"].ToString(),
                            UserTypeDescription = drProjectList["UserTypeDescription"].ToString(),
                            ProjectId = Convert.ToInt32(drProjectList["ProjectId"], CultureInfo.InvariantCulture),
                            UserStatus = drProjectList["UserProjectActivationStatus"].ToString(),
                            ReceivePartnerCommunication = Convert.ToBoolean(drProjectList["ReceivePartnerCommunication"], CultureInfo.InvariantCulture),
                            UserRole = drProjectList["ConsultantRole"] == DBNull.Value ? "Owner" : drProjectList["ConsultantRole"].ToString(),
                            UserRoleId = drProjectList["RoleId"] != DBNull.Value ? (int?)Convert.ToInt32(drProjectList["RoleId"], CultureInfo.InvariantCulture) : null,
                            UserTypeId = Convert.ToInt32(drProjectList["UserType"], CultureInfo.InvariantCulture),
                            UserStatusId = Convert.ToInt32(drProjectList["StatusId"], CultureInfo.InvariantCulture),
                            IsRemoved = Convert.ToBoolean(drProjectList["IsDeleted"], CultureInfo.InvariantCulture)
                        });
                    }

                    foreach (Project project in projectCollection)
                    {
                        project.AssociatedUserCollection = new List<User>();
                        foreach (User user in associatedConsultants)
                        {
                            if (user.ProjectId == project.ProjectId && user.IsRemoved == false)
                            {
                                project.AssociatedUserCollection.Add(user);
                            }
                        }
                    }

                    drProjectList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectList != null && !drProjectList.IsClosed)
                {
                    drProjectList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectCollection;
        }

        /// <summary>
        /// Gets the project profile data
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public static DataSet ExportProjectProfile(int projectId)
        {
            DataSet projectProfileDataSet = null;

            try
            {
                using (projectProfileDataSet = new DataSet())
                {
                    projectProfileDataSet.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectProfileByProjectId);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    projectProfileDataSet = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectProfileDataSet;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public static IList<PartnerProductOption> GetPartnerProductOption(PartnerProdOptionSearch searchCriteria, ref int totalRecordCount)
        {
            IDataReader drPartnerProdOption = null;
            IList<PartnerProductOption> partnerProducts = new List<PartnerProductOption>();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerProductOption))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, searchCriteria.ProjectId);
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, searchCriteria.CategoryId);

                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drPartnerProdOption = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drPartnerProdOption.Read())
                    {
                        partnerProducts.Add(new PartnerProductOption
                        {
                            ProductId = Convert.IsDBNull(drPartnerProdOption["ProductId"]) ? 0 : Convert.ToInt32(drPartnerProdOption["ProductId"], CultureInfo.InvariantCulture),
                            PartnerId = Convert.ToInt32(drPartnerProdOption["PartnerId"], CultureInfo.InvariantCulture),
                            PartnerLogo = drPartnerProdOption["PartnerLogo"].Equals(DBNull.Value) ? null : (byte[])drPartnerProdOption["PartnerLogo"],
                            SpecSheetPdf = drPartnerProdOption["SpecSheetPDF"].Equals(DBNull.Value) ? null : (byte[])drPartnerProdOption["SpecSheetPDF"],
                            PartnerName = drPartnerProdOption["PartnerName"].ToString(),
                            SpecSheetPdfName = Convert.IsDBNull(drPartnerProdOption["SpecSheetPDFName"]) ? null : drPartnerProdOption["SpecSheetPDFName"].ToString(),
                            ProductNumber = Convert.IsDBNull(drPartnerProdOption["ProductNumber"]) ? null : drPartnerProdOption["ProductNumber"].ToString(),
                            ProductName = Convert.IsDBNull(drPartnerProdOption["ProductName"]) ? null : drPartnerProdOption["ProductName"].ToString(),
                            IsSelected = Convert.ToBoolean(drPartnerProdOption["Selected"], CultureInfo.InvariantCulture),
                            IsOther = Convert.ToBoolean(drPartnerProdOption["IsOther"], CultureInfo.InvariantCulture),
                            CatId = Convert.ToInt32(drPartnerProdOption["CatId"], CultureInfo.InvariantCulture),
                            ProductContactEmailAddress = Convert.IsDBNull(drPartnerProdOption["ProductContactEmailAddress"]) ? null : drPartnerProdOption["ProductContactEmailAddress"].ToString(),
                            ProductImageName = Convert.IsDBNull(drPartnerProdOption["ProductImageName"]) ? null : drPartnerProdOption["ProductImageName"].ToString(),
                            ProductImage = drPartnerProdOption["ProductImage"].Equals(DBNull.Value) ? null : (byte[])drPartnerProdOption["ProductImage"],
                            ToFirstName = Convert.IsDBNull(drPartnerProdOption["ProductContactFirstName"]) ? null : drPartnerProdOption["ProductContactFirstName"].ToString(),
                            ToLastName = Convert.IsDBNull(drPartnerProdOption["ProductContactLastName"]) ? null : drPartnerProdOption["ProductContactLastName"].ToString(),
                        });
                    }
                    drPartnerProdOption.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);

                    drPartnerProdOption.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerProdOption != null && !drPartnerProdOption.IsClosed)
                {
                    drPartnerProdOption.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return partnerProducts;
        }

        /// <summary>
        /// To Get Partner Product Option Details
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public static PartnerProductOption GetPartnerProductOptionDetails(int categoryId, int projectId, int userId)
        {
            IDataReader drPartnerProdOption = null;
            PartnerProductOption partnerProductDetails = new PartnerProductOption();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerProductOptionDetails))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, categoryId);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drPartnerProdOption = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drPartnerProdOption.Read())
                    {
                        partnerProductDetails = new PartnerProductOption()
                        {
                            CategoryHeirarchy = drPartnerProdOption["CategoryHeirarchy"].ToString(),
                            BrandName = Convert.ToString(drPartnerProdOption["BrandName"], CultureInfo.InvariantCulture),
                            Address = Convert.ToString(drPartnerProdOption["Address"], CultureInfo.InvariantCulture),
                            Address2 = Convert.ToString(drPartnerProdOption["Address2"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.ToString(drPartnerProdOption["ZipCode"], CultureInfo.InvariantCulture),
                            City = Convert.ToString(drPartnerProdOption["City"], CultureInfo.InvariantCulture),
                            Country = Convert.ToString(drPartnerProdOption["Country"], CultureInfo.InvariantCulture),
                            State = Convert.ToString(drPartnerProdOption["StateName"], CultureInfo.InvariantCulture),
                            FacilityName = Convert.ToString(drPartnerProdOption["FacilityName"], CultureInfo.InvariantCulture),
                        };
                    }

                    drPartnerProdOption.NextResult();
                    Contact hiltonContact = new Contact();
                    if (drPartnerProdOption.Read())
                    {
                        hiltonContact = new Contact()
                        {
                            FirstName = drPartnerProdOption["Contact"].ToString(),
                        };
                    }

                    drPartnerProdOption.NextResult();
                    //                CategoryStandard categoryStandards = new CategoryStandard() // COMMENTED BY CODEIT.RIGHT;
                    List<CategoryStandard> categoryStandardCollection = new List<CategoryStandard>();
                    CategoryStandard catStandard;
                    while (drPartnerProdOption.Read())
                    {
                        catStandard = new CategoryStandard()
                        {
                            StandardNumber = drPartnerProdOption["StandardNumber"].ToString(),
                            StandardDescription = drPartnerProdOption["StandardDescription"].ToString(),
                            CategoryId = Convert.ToInt32(drPartnerProdOption["Category"], CultureInfo.InvariantCulture),
                        };
                        categoryStandardCollection.Add(catStandard);
                    }

                    if (categoryStandardCollection.Count > 0)
                    {
                        partnerProductDetails.CategoryStandard = categoryStandardCollection;
                    }

                    Contact contact = new Contact();
                    partnerProductDetails.Contact = contact;
                    partnerProductDetails.Contact.FirstName = hiltonContact.FirstName != null ? hiltonContact.FirstName.ToString(CultureInfo.InvariantCulture) : string.Empty;

                    drPartnerProdOption.Close();
                }
                //    PENDING FOR HBS INTEGRATION
                //    CategoryStandard catStandards = new CategoryStandard();
                //    partnerProductDetails.CategoryStandard = catStandards;
                //    partnerProductDetails.CategoryStandard.StandardNumber = categoryStandards.StandardNumber.ToString();
                //    partnerProductDetails.CategoryStandard.StandardDescription = categoryStandards.StandardDescription.ToString();
                //    partnerProductDetails.CategoryStandard.CategoryId = categoryStandards.CategoryId;
            }
            catch (Exception exception)
            {
                if (drPartnerProdOption != null && !drPartnerProdOption.IsClosed)
                {
                    drPartnerProdOption.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return partnerProductDetails;
        }

        /// <summary>
        /// To Get Product Pdf
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static PartnerProductOption GetProductPdf(int productId, bool isOther)
        {
            IDataReader drPartnerProdOption = null;
            PartnerProductOption productPdfDetails = new PartnerProductOption();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductPdf))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, productId);
                    db.AddInParameter(dbCommand, "@isOther", DbType.Boolean, isOther);
                    drPartnerProdOption = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drPartnerProdOption.Read())
                    {
                        productPdfDetails = new PartnerProductOption()
                        {
                            SpecSheetPdfName = drPartnerProdOption["SpecSheetPDFName"].ToString(),
                            SpecSheetPdf = drPartnerProdOption["SpecSheetPDF"].Equals(DBNull.Value) ? null : (byte[])drPartnerProdOption["SpecSheetPDF"],
                        };
                    }

                    drPartnerProdOption.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerProdOption != null && !drPartnerProdOption.IsClosed)
                {
                    drPartnerProdOption.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return productPdfDetails;
        }

        /// <summary>
        /// Update Product Selection
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="flgSelect"></param>
        public static void UpdateProductSelection(int productId, bool isSelected, bool isOther, int partnerId, int projectId, int catId, int updateByUserid)
        {
            int result = 0;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateProductSelection))
                {
                    if (productId == 0)
                        db.AddInParameter(dbCommand, "@productId", DbType.Int32, null);
                    else
                        db.AddInParameter(dbCommand, "@productId", DbType.Int32, productId);
                    db.AddInParameter(dbCommand, "@isSelected", DbType.Boolean, isSelected);
                    db.AddInParameter(dbCommand, "@isOther", DbType.Boolean, isOther);
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, catId);
                    db.AddInParameter(dbCommand, "@updateByUserId", DbType.Int32, updateByUserid);

                    result = db.ExecuteNonQuery(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Saves Non Approved Products
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="productName"></param>
        /// <param name="manufacturer"></param>
        /// <param name="modelNumber"></param>
        /// <param name="reason"></param>
        public static void SaveNonApprovedProducts(int projectId, string productName, string manufacturer, string modelNumber, string reason, int categoryId, string specSheetName, Byte[] specSheet, int updatedByUserId)
        {
            int result = 0;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsNonApprovedProducts))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    db.AddInParameter(dbCommand, "@productName", DbType.String, productName);
                    db.AddInParameter(dbCommand, "@manufacturer", DbType.String, manufacturer);
                    db.AddInParameter(dbCommand, "@modelNumber", DbType.String, modelNumber);
                    db.AddInParameter(dbCommand, "@reason", DbType.String, reason);
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, categoryId);
                    if (string.IsNullOrEmpty(specSheetName))
                    {
                        db.AddInParameter(dbCommand, "@specSheetName", DbType.String, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@specSheetName", DbType.String, specSheetName);
                    }
                    if (specSheet == null)
                    {
                        db.AddInParameter(dbCommand, "@specSheet", DbType.Binary, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@specSheet", DbType.Binary, specSheet);
                    }
                    db.AddInParameter(dbCommand, "@updateByUserId", DbType.Int32, updatedByUserId);

                    result = db.ExecuteNonQuery(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// To Get SC Configuration Percentage
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="percent"></param>
        public static string GetSCConfigurationPercent(int projectId)
        {
            IDataReader drPartnerProdOption = null;
            string percent = "0";
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetSCConfigurationPercent))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);

                    SqlParameter retValParam = new SqlParameter("@percent", SqlDbType.NVarChar, 500);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drPartnerProdOption = (IDataReader)db.ExecuteReader(dbCommand);

                    drPartnerProdOption.NextResult();
                    percent = Convert.ToString(retValParam.Value, CultureInfo.InvariantCulture);

                    drPartnerProdOption.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerProdOption != null && !drPartnerProdOption.IsClosed)
                {
                    drPartnerProdOption.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return percent;
        }

        /// <summary>
        /// Update Project User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int UpdateProjectUser(User user)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateProjectUser))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, user.ProjectId);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, user.UserId);
                    db.AddInParameter(dbCommand, "@firstName", DbType.String, user.FirstName);
                    db.AddInParameter(dbCommand, "@lastName ", DbType.String, user.LastName);
                    db.AddInParameter(dbCommand, "@email", DbType.String, user.Email);
                    if (user.UserRoleId.HasValue)
                    {
                        db.AddInParameter(dbCommand, "@userRoleId", DbType.Int32, user.UserRoleId);
                    }
                    db.AddInParameter(dbCommand, "@receivePartnerCommunication", DbType.Boolean, user.ReceivePartnerCommunication);
                    db.AddInParameter(dbCommand, "@updateByUserId", DbType.Int32, user.UpdatedByUserId);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return result;
        }

        /// <summary>
        /// Remove Project Consultant Association
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int RemoveProjectConsultantAssociation(User user)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.RemoveProjectConsultantAssociation))
                {
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, user.ProjectId);
                    db.AddInParameter(dbCommand, "@firstName", DbType.String, user.FirstName);
                    db.AddInParameter(dbCommand, "@lastName", DbType.String, user.LastName);
                    db.AddInParameter(dbCommand, "@email", DbType.String, user.Email);
                    if (user.UserRoleId.HasValue)
                    {
                        db.AddInParameter(dbCommand, "@userRoleId", DbType.Int32, user.UserRoleId);
                    }

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}