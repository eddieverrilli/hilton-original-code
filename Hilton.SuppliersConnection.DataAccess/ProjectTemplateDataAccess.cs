﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class ProjectTemplateDataAccess
    {
        /// <summary>
        ///saves a new category to db
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public static bool AddCategory(Category category, int projectTemplateId)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.AddCategoryForProjectTemplate))
                {
                    db.AddInParameter(dbCommand, "@categoryName", DbType.String, category.CategoryName);
                    db.AddInParameter(dbCommand, "@parentCategoryId", DbType.Int32, category.ParentCategoryId);
                    db.AddInParameter(dbCommand, "@rootParentCatId ", DbType.Int32, category.RootParentId);
                    db.AddInParameter(dbCommand, "@level", DbType.Int32, category.Level);
                    db.AddInParameter(dbCommand, "@isLeaf", DbType.Boolean, category.IsLeaf);
                    db.AddInParameter(dbCommand, "@isDeleted", DbType.Boolean, false);
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, projectTemplateId);
                    db.AddInParameter(dbCommand, "@description", DbType.String, category.Description);
                    db.AddInParameter(dbCommand, "@contactInformation", DbType.String, category.ContactInfo);
                    db.AddInParameter(dbCommand, "@styleDescription", DbType.String, category.StyleDescription);
                    db.AddInParameter(dbCommand, "@status", DbType.Boolean, category.IsActive);
                    db.AddInParameter(dbCommand, "@isRequired", DbType.Boolean, category.IsRequired);
                    db.AddInParameter(dbCommand, "@isNewPartnersReq", DbType.Boolean, category.IsNewPartnersRequired);
                    db.AddInParameter(dbCommand, "@isNewProductsReq", DbType.Boolean, category.IsNewProductsReq);
                    db.AddInParameter(dbCommand, "@sequenceId", DbType.String, category.SequenceId);
                    db.AddInParameter(dbCommand, "@standardNumbers", DbType.String, category.StandardNumbers);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///saves a new partner/ product to db
        /// </summary>
        /// <param name="partnerProductXml"></param>
        /// <returns></returns>
        public static bool AddPartnerProduct(CategoryPartnerProduct partnerProduct)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.AddPartnerProductForCategory))
                {
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerProduct.CategoryId);
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, partnerProduct.TemplateId);
                    db.AddInParameter(dbCommand, "@partnerId ", DbType.Int32, partnerProduct.PartnerId);
                    if (partnerProduct.ProductId == -1)
                    {
                        db.AddInParameter(dbCommand, "@productId ", DbType.Int32, null);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@productId ", DbType.Int32, partnerProduct.ProductId);
                    }
                    db.AddInParameter(dbCommand, "@status", DbType.Boolean, partnerProduct.IsActive);
                    db.AddInParameter(dbCommand, "@userId ", DbType.Int32, null);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///copy template to target template
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public static bool CopyTemplate(ProjectTemplate sourceTemplate, ProjectTemplate destinationTemplate, bool categoriesWithProducts)
        {
            int result = -2;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.CopyTemplate))
                {
                    db.AddInParameter(dbCommand, "@sourceTemplateId", DbType.Int32, sourceTemplate.TemplateId);
                    db.AddInParameter(dbCommand, "@destBrandId", DbType.Int32, destinationTemplate.BrandId);
                    db.AddInParameter(dbCommand, "@destTypeId", DbType.Int32, destinationTemplate.PropertyTypeId);
                    db.AddInParameter(dbCommand, "@destRegionId", DbType.Int32, destinationTemplate.RegionId);
                    db.AddInParameter(dbCommand, "@destCountryId", DbType.Int32, destinationTemplate.CountryId);
                    db.AddInParameter(dbCommand, "@categoryWithProducts", DbType.Boolean, categoriesWithProducts);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, sourceTemplate.UserId);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            if (result == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///export project template to excel
        /// </summary>
        /// <param name="projectTemplate"></param>
        public static DataSet ExportProjectTemplate(int projectTemplateId)
        {
            ProjectTemplate projectTemplate = new ProjectTemplate();
            projectTemplate.Categories = new List<Category>();
            DataSet projectTemplateDataSet = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetCategoryDetailsByTemplateId))
                {
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, projectTemplateId);
                    using (projectTemplateDataSet = new DataSet())
                    {
                        projectTemplateDataSet = db.ExecuteDataSet(dbCommand);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectTemplateDataSet;
        }

        /// <summary>
        ///retrieves brand list from db
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public static IList<Brand> GetBrandList(int regionId)
        {
            IList<Brand> brandCollection = new List<Brand>();
            IDataReader drBrandList = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetBrandListByRegionId))
                {
                    db.AddInParameter(dbCommand, "@regionId", DbType.Int32, regionId);
                    drBrandList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drBrandList.Read())
                    {
                        brandCollection.Add(new Brand()
                        {
                            BrandId = Convert.ToInt32(drBrandList["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = drBrandList["BrandName"].ToString()
                        });
                    }

                    drBrandList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drBrandList != null && !drBrandList.IsClosed)
                {
                    drBrandList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return brandCollection;
        }

        /// <summary>
        ///get partner list from db
        /// </summary>
        /// <returns></returns>
        public static IList<Partner> GetActivePartnerList()
        {
            IDataReader drPartnerList = null;
            IList<Partner> partnerList = new List<Partner>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetActivePartners))
                {
                    drPartnerList = (IDataReader)db.ExecuteReader(dbCommand);
                    while (drPartnerList.Read())
                    {
                        partnerList.Add(new Partner()
                        {
                            PartnerId = drPartnerList["PartnerId"].ToString(),
                            CompanyName = drPartnerList["PartnerName"].ToString()
                        });
                    }

                    drPartnerList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerList != null && !drPartnerList.IsClosed)
                {
                    drPartnerList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return partnerList;
        }

        /// <summary>
        ///gets product list from db for a selected partner
        /// </summary>
        /// <param name="categoryPartnerProduct"></param>
        /// <returns></returns>
        public static IList<Product> GetProductList(CategoryPartnerProduct categoryPartnerProduct)
        {
            IDataReader drProductList = null;
            IList<Product> productList = new List<Product>();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductsForPartner))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, categoryPartnerProduct.PartnerId);
                    drProductList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProductList.Read())
                    {
                        productList.Add(new Product()
                        {
                            ProductId = Convert.ToInt32(drProductList["ProductId"], CultureInfo.InvariantCulture),
                            ProductName = drProductList["ProductName"].ToString()
                        });
                    }

                    drProductList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProductList != null && !drProductList.IsClosed)
                {
                    drProductList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return productList;
        }

        /// <summary>
        ///get the entire project template ( list of categories ) from db for a particular templateId
        /// </summary>
        /// <param name="projectTemplateId"></param>
        /// <returns></returns>
        public static ProjectTemplate GetProjectTemplateDetail(int projectTemplateId)
        {
            IDataReader drProjectTemplateDetails = null;
            ProjectTemplate projectTemplate = new ProjectTemplate();
            projectTemplate.Categories = new List<Category>();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetCategoryDetailsByTemplateId))
                {
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, projectTemplateId);
                    drProjectTemplateDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drProjectTemplateDetails.Read())
                    {
                        projectTemplate.BrandName = drProjectTemplateDetails["BrandName"].ToString();
                        projectTemplate.RegionDescription = drProjectTemplateDetails["RegionName"].ToString();
                        projectTemplate.CountryName = drProjectTemplateDetails["CountryName"].ToString();
                        //projectTemplate.PropertyTypeDescription = drProjectTemplateDetails["PropertyTypeName"].ToString();
                    }

                    drProjectTemplateDetails.NextResult();
                    while (drProjectTemplateDetails.Read())
                    {
                        projectTemplate.Categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drProjectTemplateDetails["CategoryID"], CultureInfo.InvariantCulture),
                            CategoryName = drProjectTemplateDetails["CategoryName"].ToString(),
                            Level = Convert.ToInt32(drProjectTemplateDetails["Level"], CultureInfo.InvariantCulture),
                            IsLeaf = Convert.ToBoolean(drProjectTemplateDetails["IsLeaf"], CultureInfo.InvariantCulture),
                            IsActive = Convert.ToBoolean(drProjectTemplateDetails["IsActive"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drProjectTemplateDetails["ParentCategory"], CultureInfo.InvariantCulture),
                            RootParentId = Convert.ToInt32(drProjectTemplateDetails["RootParentCatID"], CultureInfo.InvariantCulture),
                            IsNewPartnersRequired = drProjectTemplateDetails["IsNewPartnersReq"] != DBNull.Value ?
                                (bool?)Convert.ToBoolean(drProjectTemplateDetails["IsNewPartnersReq"], CultureInfo.InvariantCulture) : false,
                            IsRequired = drProjectTemplateDetails["IsRequired"] != DBNull.Value ?
                                (bool?)Convert.ToBoolean(drProjectTemplateDetails["IsRequired"], CultureInfo.InvariantCulture) : false,
                            IsNewProductsReq = drProjectTemplateDetails["IsNewProductsReq"] != DBNull.Value ?
                                (bool?)Convert.ToBoolean(drProjectTemplateDetails["IsNewProductsReq"], CultureInfo.InvariantCulture) : false,
                            HasProducts = Convert.ToBoolean(drProjectTemplateDetails["HasProducts"], CultureInfo.InvariantCulture),
                            SequenceId = drProjectTemplateDetails["SequenceId"].ToString(),
                            Description = drProjectTemplateDetails["OptionalDescription"].ToString(),
                            StyleDescription = drProjectTemplateDetails["StyleDescription"].ToString(),
                            StandardNumbers = drProjectTemplateDetails["StandardNumbers"].ToString(),
                            ContactInfo = drProjectTemplateDetails["ContactInfo"].ToString(),
                            IsEnabled = Convert.ToBoolean(drProjectTemplateDetails["IsEnabled"], CultureInfo.InvariantCulture),
                        });
                    }

                    drProjectTemplateDetails.NextResult();
                    IList<CategoryPartnerProduct> partnerProducts = new List<CategoryPartnerProduct>();
                    while (drProjectTemplateDetails.Read())
                    {
                        partnerProducts.Add(new CategoryPartnerProduct()
                        {
                            ProductId = Convert.IsDBNull(drProjectTemplateDetails["ProductId"]) ? 0 : Convert.ToInt32(drProjectTemplateDetails["ProductId"], CultureInfo.InvariantCulture),
                            PartnerId = Convert.ToInt32(drProjectTemplateDetails["PartnerId"], CultureInfo.InvariantCulture),
                            PartnerName = drProjectTemplateDetails["PartnerName"].ToString(),
                            ProductName = Convert.IsDBNull(drProjectTemplateDetails["ProductName"]) ? string.Empty : drProjectTemplateDetails["ProductName"].ToString(),
                            IsActive = Convert.ToBoolean(drProjectTemplateDetails["ActiveStatus"], CultureInfo.InvariantCulture),
                            CategoryId = Convert.ToInt32(drProjectTemplateDetails["CatId"], CultureInfo.InvariantCulture)
                        });
                    }

                    drProjectTemplateDetails.NextResult();
                    IList<CategoryStandard> categoryStandards = new List<CategoryStandard>();
                    while (drProjectTemplateDetails.Read())
                    {
                        categoryStandards.Add(new CategoryStandard()
                        {
                            StandardNumber = drProjectTemplateDetails["StandardNumber"].ToString(),
                            StandardDescription = drProjectTemplateDetails["StandardDescription"].ToString(),
                            CategoryId = Convert.ToInt32(drProjectTemplateDetails["Category"], CultureInfo.InvariantCulture),
                        });
                    }

                    foreach (Category category in projectTemplate.Categories)
                    {
                        category.PartnerProducts = new List<CategoryPartnerProduct>();
                        foreach (CategoryPartnerProduct partnerProduct in partnerProducts)
                        {
                            if (partnerProduct.CategoryId == category.CategoryId)
                            {
                                category.PartnerProducts.Add(partnerProduct);
                            }
                        }
                    }

                    foreach (Category category in projectTemplate.Categories)
                    {
                        category.CategoryStandards = new List<CategoryStandard>();
                        foreach (CategoryStandard standard in categoryStandards)
                        {
                            if (standard.CategoryId == category.CategoryId)
                            {
                                category.CategoryStandards.Add(standard);
                            }
                        }
                    }

                    drProjectTemplateDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectTemplateDetails != null && drProjectTemplateDetails.IsClosed)
                {
                    drProjectTemplateDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return projectTemplate;
        }

        /// <summary>
        /// Get a list of templates for the given search criteria
        /// </summary>
        /// <param name="projectSearch"></param>
        /// <returns></returns>
        public static IList<ProjectTemplate> GetProjectTemplateList(TemplateSearch templateSearch, ref int totalRecordCount)
        {
            IList<ProjectTemplate> projectTemplateCollection = new List<ProjectTemplate>();
            IDataReader drProjectTemplate = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectTemplateList))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, templateSearch.UserId);
                    if (String.IsNullOrEmpty(templateSearch.RegionId))
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, templateSearch.RegionId);

                    if (String.IsNullOrEmpty(templateSearch.CountryId))
                        db.AddInParameter(dbCommand, "@countryId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@countryId", DbType.Int32, templateSearch.CountryId);

                    if (String.IsNullOrEmpty(templateSearch.BrandId))
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, templateSearch.BrandId);

                    if (String.IsNullOrEmpty(templateSearch.PropertyTypeId))
                        db.AddInParameter(dbCommand, "@propertyType", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@propertyType", DbType.Int32, templateSearch.PropertyTypeId);

                    if (String.IsNullOrEmpty(templateSearch.StatusId))
                        db.AddInParameter(dbCommand, "@status", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@status", DbType.Int32, templateSearch.StatusId);

                    db.AddInParameter(dbCommand, "@searchExpression", DbType.String, templateSearch.QuickSearchExpression);
                    db.AddInParameter(dbCommand, "@searchType", DbType.String, templateSearch.SearchFlag);
                    db.AddInParameter(dbCommand, "@sortExpression", DbType.String, templateSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int32, templateSearch.PageSize);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, templateSearch.SortDirection);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, templateSearch.PageIndex);

                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drProjectTemplate = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProjectTemplate.Read())
                    {
                        projectTemplateCollection.Add(new ProjectTemplate()
                        {
                            TemplateId = Convert.ToInt32(drProjectTemplate["ProjectTemplateId"], CultureInfo.InvariantCulture),
                            BrandName = Convert.ToString(drProjectTemplate["BrandName"], CultureInfo.InvariantCulture),
                            PropertyTypeDescription = Convert.ToString(drProjectTemplate["PropertyTypeName"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drProjectTemplate["RegionName"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drProjectTemplate["CountryName"], CultureInfo.InvariantCulture),
                            NoOfCategories = Convert.ToInt32(drProjectTemplate["TotalCategories"], CultureInfo.InvariantCulture),
                            NoOfProjects = Convert.ToInt32(drProjectTemplate["TotalProjects"], CultureInfo.InvariantCulture),
                            StatusId = Convert.ToInt32(drProjectTemplate["ProjectTemplateStatusId"], CultureInfo.InvariantCulture)
                        }
                        );
                    }
                    drProjectTemplate.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);

                    drProjectTemplate.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectTemplate != null && !drProjectTemplate.IsClosed)
                {
                    drProjectTemplate.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectTemplateCollection;
        }

        /// <summary>
        /// Retrieve the search filters for project templates
        /// </summary>
        /// <returns></returns>
        public static IList<SearchFilters> GetProjectTemplateSearchFilterItems(int userId)
        {
            IDataReader drSearchFilter = null;

            IList<SearchFilters> searchFilters = new List<SearchFilters>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectTemplateFilterData))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drSearchFilter.Read())
                    {
                        searchFilters.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            PropertyTypeId = Convert.ToInt32(drSearchFilter["PropertyTypeId"], CultureInfo.InvariantCulture),
                            PropertyTypeDescription = Convert.ToString(drSearchFilter["PropertyTypeName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drSearchFilter["CountryName"], CultureInfo.InvariantCulture)

                        }

                      );
                    }

                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        /// Retrieve property types list
        /// </summary>
        /// <param name="selectedRegionId"></param>
        /// <param name="selectedBrandId"></param>
        /// <param name="sourceProjectTemplateId"></param>
        /// <returns></returns>
        public static IList<PropertyType> GetPropertyTypeList(int selectedRegionId, int selectedBrandId, int sourceProjectTemplateId)
        {
            IList<PropertyType> propertyTypeCollection = new List<PropertyType>();
            IDataReader drPropertyTypeList = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPropertyTypeListByRegionBrand))
                {
                    db.AddInParameter(dbCommand, "@regionId", DbType.Int32, selectedRegionId);
                    db.AddInParameter(dbCommand, "@brandId", DbType.Int32, selectedBrandId);
                    db.AddInParameter(dbCommand, "@sourceProjectTemplateId", DbType.Int32, sourceProjectTemplateId);
                    drPropertyTypeList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drPropertyTypeList.Read())
                    {
                        propertyTypeCollection.Add(new PropertyType()
                        {
                            PropertyTypeId = Convert.ToInt32(drPropertyTypeList["PropertyTypeID"], CultureInfo.InvariantCulture),
                            PropertyTypeDescription = drPropertyTypeList["PropertyTypeName"].ToString()
                        });
                    }

                    drPropertyTypeList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPropertyTypeList != null && !drPropertyTypeList.IsClosed)
                {
                    drPropertyTypeList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return propertyTypeCollection;
        }

        /// <summary>
        ///retrieves brand/region/type list from db.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sourceTemplateId"></param>
        /// <returns></returns>
        public static IList<BrandRegionType> GetBrandRegionTypeList(int userId, int sourceTemplateId)
        {
            IList<BrandRegionType> brandRegionTypeCollection = new List<BrandRegionType>();
            IDataReader drRegionList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetBrandRegionTypeForCopyTemplate))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    db.AddInParameter(dbCommand, "@sourceTemplateId", DbType.Int32, sourceTemplateId);
                    drRegionList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drRegionList.Read())
                    {
                        brandRegionTypeCollection.Add(new BrandRegionType()
                        {
                            //Remove property type
                            //BrandRegionPropertyTypeId = drRegionList["BrandId"].ToString() + "," + drRegionList["RegionId"].ToString() + "," + drRegionList["CountryId"].ToString() + "," + drRegionList["PropertyTypeId"].ToString(),
                            //BrandRegionPropertyTypeDesc = drRegionList["BrandName"].ToString() + "/" + drRegionList["RegionName"].ToString() + "/" + drRegionList["CountryName"].ToString() + "/" + drRegionList["PropertyTypeName"].ToString()
                            BrandRegionPropertyTypeId = drRegionList["BrandId"].ToString() + "," + drRegionList["RegionId"].ToString() + "," + drRegionList["CountryId"].ToString(),
                            BrandRegionPropertyTypeDesc = drRegionList["BrandName"].ToString() + "/" + drRegionList["RegionName"].ToString() + "/" + drRegionList["CountryName"].ToString()
                        });
                    }

                    drRegionList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drRegionList != null && !drRegionList.IsClosed)
                {
                    drRegionList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return brandRegionTypeCollection;
        }

        /// <summary>
        ///retrieves region list from db.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static IList<Region> GetRegionList(int userId)
        {
            IList<Region> regionCollection = new List<Region>();
            IDataReader drRegionList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetRegionByUserId))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drRegionList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drRegionList.Read())
                    {
                        regionCollection.Add(new Region()
                        {
                            RegionId = Convert.ToInt32(drRegionList["RegionID"], CultureInfo.InvariantCulture),
                            RegionDescription = drRegionList["RegionName"].ToString()
                        });
                    }

                    drRegionList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drRegionList != null && !drRegionList.IsClosed)
                {
                    drRegionList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return regionCollection;
        }

        /// <summary>
        /// update a particular category in DB
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public static bool UpdateCategory(Category category, int projectTemplateId)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateCategoryForProjectTemplate))
                {
                    SetStandardCategoryMapping(dbCommand, category.CategoryStandards.Where(p => p.IsToBeMapped == true).ToList());
                    SetStandardCategoryMappingRemoval(dbCommand, category.CategoryStandards.Where(p => p.IsToBeRemoved == true).ToList());
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, category.CategoryId);
                    db.AddInParameter(dbCommand, "@categoryName", DbType.String, category.CategoryName);
                    db.AddInParameter(dbCommand, "@parentCategoryId", DbType.Int32, category.ParentCategoryId);
                    db.AddInParameter(dbCommand, "@rootParentCatId ", DbType.Int32, category.RootParentId);
                    db.AddInParameter(dbCommand, "@level", DbType.Int32, category.Level);
                    db.AddInParameter(dbCommand, "@isLeaf", DbType.Boolean, category.IsLeaf);
                    db.AddInParameter(dbCommand, "@isDeleted", DbType.Boolean, false);
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, projectTemplateId);
                    db.AddInParameter(dbCommand, "@description", DbType.String, category.Description);
                    db.AddInParameter(dbCommand, "@contactInformation", DbType.String, category.ContactInfo);
                    db.AddInParameter(dbCommand, "@styleDescription", DbType.String, category.StyleDescription);
                    db.AddInParameter(dbCommand, "@status", DbType.Boolean, category.IsActive);
                    db.AddInParameter(dbCommand, "@standardNumbers", DbType.String, category.StandardNumbers);
                    db.AddInParameter(dbCommand, "@sequenceId", DbType.String, category.SequenceId);
                    if (category.IsRequired.HasValue)
                    {
                        db.AddInParameter(dbCommand, "@isRequired", DbType.Boolean, category.IsRequired);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@isRequired", DbType.Boolean, null);
                    }
                    if (category.IsNewPartnersRequired.HasValue)
                    {
                        db.AddInParameter(dbCommand, "@isNewPartnersReq", DbType.Boolean, category.IsNewPartnersRequired);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@isNewPartnersReq", DbType.Boolean, null);
                    }

                    if (category.IsNewProductsReq.HasValue)
                    {
                        db.AddInParameter(dbCommand, "@isNewProductsReq", DbType.Boolean, category.IsNewProductsReq);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@isNewProductsReq", DbType.Boolean, null);
                    }
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    dbCommand.CommandTimeout = 180;
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///update the status of category list
        /// </summary>
        /// <param name="statusUpdateXML"></param>
        /// <returns></returns>
        public static bool UpdateCategoryListConfig(int projectTemplateId, IList<Category> categoryList)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateCategoryConfig))
                {
                    SetCategoryStatusMapping(dbCommand, categoryList);
                    SetLeafCategoryConfigMapping(dbCommand, categoryList.Where(p => p.IsLeaf == true).ToList());
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, projectTemplateId);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// update category status
        /// </summary>
        /// <param name="projectTemplateId"></param>
        /// <param name="categoryId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public static bool UpdateCategoryStatus(int projectTemplateId, int categoryId, bool isActive)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateCategoryStatus))
                {
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, projectTemplateId);
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, categoryId);
                    db.AddInParameter(dbCommand, "@isActive", DbType.Boolean, isActive);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// update category status fro multiple categories in one go
        /// </summary>
        /// <param name="projectTemplateId"></param>
        /// <param name="categoryId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public static bool UpdateCategoryStatusForMultipleCategories(int projectTemplateId, string categoryIds, bool isActive)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdCategoryStatusForMultipleIDs))
                {
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, projectTemplateId);
                    db.AddInParameter(dbCommand, "@categoryIds", DbType.String, categoryIds);
                    db.AddInParameter(dbCommand, "@isActive", DbType.Boolean, isActive);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///update partner/ product in db.
        /// </summary>
        /// <param name="statusUpdateXML"></param>
        /// <returns></returns>
        public static bool UpdatePartnerProduct(CategoryPartnerProduct partnerProduct)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdatePartnerProductForCategory))
                {
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerProduct.CategoryId);
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, partnerProduct.TemplateId);
                    db.AddInParameter(dbCommand, "@partnerId ", DbType.Int32, partnerProduct.PartnerId);
                    if (partnerProduct.ProductId == -1)
                    {
                        db.AddInParameter(dbCommand, "@productId ", DbType.Int32, null);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@productId ", DbType.Int32, partnerProduct.ProductId);
                    }
                    if (partnerProduct.OldProductId == 0)
                    {
                        db.AddInParameter(dbCommand, "@oldProductId ", DbType.Int32, null);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@oldProductId ", DbType.Int32, partnerProduct.OldProductId);
                    }
                    if (partnerProduct.OldPartnerId == 0)
                    {
                        db.AddInParameter(dbCommand, "@oldPartnerId ", DbType.Int32, null);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@oldPartnerId ", DbType.Int32, partnerProduct.OldPartnerId);
                    }
                    db.AddInParameter(dbCommand, "@userId ", DbType.Int32, null);

                    db.AddInParameter(dbCommand, "@status", DbType.Boolean, partnerProduct.IsActive);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///update project template status
        /// </summary>
        /// <param name="projectTemplate"></param>
        /// <returns></returns>
        public static int UpdateProjectTemplate(IList<ProjectTemplate> projectTemplate)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateProjectTemplate))
                {
                    DataTable projectTemplateUpdatesDataTable;
                    // create data table to insert items
                    using (projectTemplateUpdatesDataTable = new DataTable("ListUpdates"))
                    {
                        projectTemplateUpdatesDataTable.Locale = CultureInfo.InvariantCulture;
                        projectTemplateUpdatesDataTable.Columns.Add("RecordId", typeof(int));
                        projectTemplateUpdatesDataTable.Columns.Add("StatusId", typeof(int));

                        projectTemplate.ToList().ForEach(x =>
                        {
                            projectTemplateUpdatesDataTable.Rows.Add(x.TemplateId, x.StatusId);
                        });
                        SqlParameter feedbackUpdatesParam = new SqlParameter("@projectTemplate", projectTemplateUpdatesDataTable);
                        feedbackUpdatesParam.SqlDbType = SqlDbType.Structured;
                        feedbackUpdatesParam.Direction = ParameterDirection.Input;
                        dbCommand.Parameters.Add(feedbackUpdatesParam);

                        SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                        retValParam.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(retValParam);

                        db.ExecuteNonQuery(dbCommand);
                        result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// set category status parameter
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="categoryList"></param>
        private static void SetCategoryStatusMapping(DbCommand dbCommand, IList<Category> categoryList)
        {
            using (DataTable categoryConfigList = new DataTable())
            {
                categoryConfigList.Locale = CultureInfo.InvariantCulture;
                categoryConfigList.Columns.Add(new DataColumn("CategoryId", typeof(Int32)));
                categoryConfigList.Columns.Add(new DataColumn("IsActive", typeof(bool)));
                categoryConfigList.Columns.Add(new DataColumn("IsLeaf", typeof(bool)));
                foreach (var category in categoryList)
                {
                    var row = categoryConfigList.NewRow();
                    row["CategoryId"] = category.CategoryId;
                    row["IsActive"] = category.IsActive;
                    row["IsLeaf"] = category.IsLeaf;
                    categoryConfigList.Rows.Add(row);
                }

                SqlParameter categoryConfig = new SqlParameter("@categoryConfig", SqlDbType.Structured);
                categoryConfig.Direction = ParameterDirection.Input;
                categoryConfig.Value = categoryConfigList;
                dbCommand.Parameters.Add(categoryConfig);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="categoryList"></param>
        private static void SetLeafCategoryConfigMapping(DbCommand dbCommand, System.Collections.Generic.IList<Category> categoryList)
        {
            using (DataTable categoryConfigList = new DataTable())
            {
                categoryConfigList.Locale = CultureInfo.InvariantCulture;
                categoryConfigList.Columns.Add(new DataColumn("CategoryId", typeof(Int32)));
                categoryConfigList.Columns.Add(new DataColumn("IsRequired", typeof(bool)));
                categoryConfigList.Columns.Add(new DataColumn("IsNewPartnerReq", typeof(bool)));
                categoryConfigList.Columns.Add(new DataColumn("IsNewProductReq", typeof(bool)));
                foreach (var category in categoryList)
                {
                    var row = categoryConfigList.NewRow();
                    row["CategoryId"] = category.CategoryId;
                    row["IsRequired"] = category.IsRequired;
                    row["IsNewPartnerReq"] = category.IsNewPartnersRequired;
                    row["IsNewProductReq"] = category.IsNewProductsReq;

                    categoryConfigList.Rows.Add(row);
                }

                SqlParameter categoryConfig = new SqlParameter("@leafCategoryConfig", SqlDbType.Structured);
                categoryConfig.Direction = ParameterDirection.Input;
                categoryConfig.Value = categoryConfigList;
                dbCommand.Parameters.Add(categoryConfig);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="categoryStandardList"></param>
        private static void SetStandardCategoryMapping(DbCommand dbCommand, IList<CategoryStandard> categoryStandardList)
        {
            using (DataTable standardAssociation = new DataTable())
            {
                standardAssociation.Columns.Add(new DataColumn("StandardNumber", typeof(string)));
                standardAssociation.Columns.Add(new DataColumn("StandardDescription", typeof(string)));
                foreach (var standard in categoryStandardList)
                {
                    var row = standardAssociation.NewRow();
                    row["StandardNumber"] = standard.StandardNumber;
                    row["StandardDescription"] = standard.StandardDescription;
                    standardAssociation.Rows.Add(row);
                }

                SqlParameter standardMapping = new SqlParameter("@standardMapping", SqlDbType.Structured);
                standardMapping.Direction = ParameterDirection.Input;
                standardMapping.Value = standardAssociation;
                dbCommand.Parameters.Add(standardMapping);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="categoryStandardList"></param>
        private static void SetStandardCategoryMappingRemoval(DbCommand dbCommand, IList<CategoryStandard> categoryStandardList)
        {
            using (DataTable standardAssociation = new DataTable())
            {
                standardAssociation.Columns.Add(new DataColumn("StandardNumber", typeof(string)));
                standardAssociation.Columns.Add(new DataColumn("StandardDescription", typeof(string)));
                foreach (var standard in categoryStandardList)
                {
                    var row = standardAssociation.NewRow();
                    row["StandardNumber"] = standard.StandardNumber;
                    row["StandardDescription"] = standard.StandardDescription;
                    standardAssociation.Rows.Add(row);
                }

                SqlParameter standardMapping = new SqlParameter("@removeStandardMapping", SqlDbType.Structured);
                standardMapping.Direction = ParameterDirection.Input;
                standardMapping.Value = standardAssociation;
                dbCommand.Parameters.Add(standardMapping);
            }
        }

        /// <summary>
        ///retrieves region list from db.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static IList<Brand> GetProjectTemplateActiveBrands()
        {
            IList<Brand> brandCollection = new List<Brand>();
            IDataReader drBrandList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectTemplateActiveBrands))
                {
                    //db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drBrandList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drBrandList.Read())
                    {
                        brandCollection.Add(new Brand()
                        {
                            BrandId = Convert.ToInt32(drBrandList["BrandID"], CultureInfo.InvariantCulture),
                            BrandDescription = drBrandList["BrandName"].ToString()
                        });
                    }

                    drBrandList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drBrandList != null && !drBrandList.IsClosed)
                {
                    drBrandList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return brandCollection;
        }

        /// <summary>
        ///retrieves region list from db.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static IList<ProjectTemplate> GetProjectTemplatesByBrandId(int brandId)
        {
            IList<ProjectTemplate> ProjectTemplateCollection = new List<ProjectTemplate>();
            IDataReader drProjectTemplateList = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectTemplatesByBrandId))
                {
                    db.AddInParameter(dbCommand, "@brandId", DbType.Int32, brandId);
                    drProjectTemplateList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProjectTemplateList.Read())
                    {
                        ProjectTemplateCollection.Add(new ProjectTemplate()
                        {
                            TemplateId = Convert.ToInt32(drProjectTemplateList["ProjectTemplateId"]),
                            BrandId = Convert.ToInt32(drProjectTemplateList["BrandId"]),
                            BrandName = drProjectTemplateList["BrandName"].ToString(),
                            RegionId = Convert.ToInt32(drProjectTemplateList["RegionId"]),
                            RegionDescription = drProjectTemplateList["RegionName"].ToString(),
                            CountryId = Convert.ToInt32(drProjectTemplateList["CountryId"]),
                            CountryName = drProjectTemplateList["CountryName"].ToString(),
                        });
                    }

                    drProjectTemplateList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectTemplateList != null && !drProjectTemplateList.IsClosed)
                {
                    drProjectTemplateList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return ProjectTemplateCollection;
        }

        public static int CopyMultipleProjectTemplate(IList<ProjectTemplate> projectTemplate, IList<Category> Categories, int copyFromBrandId, int copyToBrandId, int userId)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.CopyMultipleProjectTemplate))
                {
                    db.AddInParameter(dbCommand, "@CopyFromBrandId", DbType.Int32, copyFromBrandId);
                    db.AddInParameter(dbCommand, "@CopyToBrandId", DbType.Int32, copyToBrandId);
                    db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                    DataTable projectTemplateUpdatesDataTable;
                    // create data table to insert items
                    using (projectTemplateUpdatesDataTable = new DataTable("ProjectTemplateMappingType"))
                    {
                        projectTemplateUpdatesDataTable.Locale = CultureInfo.InvariantCulture;
                        projectTemplateUpdatesDataTable.Columns.Add("TemplateId", typeof(int));
                        projectTemplateUpdatesDataTable.Columns.Add("BrandId", typeof(int));
                        projectTemplateUpdatesDataTable.Columns.Add("RegionId", typeof(int));
                        projectTemplateUpdatesDataTable.Columns.Add("CountryId", typeof(int));

                        projectTemplate.ToList().ForEach(x =>
                        {
                            projectTemplateUpdatesDataTable.Rows.Add(x.TemplateId, copyFromBrandId, x.RegionId, x.CountryId);
                        });
                        SqlParameter templateUpdatesParam = new SqlParameter("@projectTemplate", projectTemplateUpdatesDataTable);
                        templateUpdatesParam.SqlDbType = SqlDbType.Structured;
                        templateUpdatesParam.Direction = ParameterDirection.Input;
                        dbCommand.Parameters.Add(templateUpdatesParam);
                    }
                    DataTable category;
                    // create data table to insert items
                    using (category = new DataTable("IdCollectionType"))
                    {
                        category.Locale = CultureInfo.InvariantCulture;
                        category.Columns.Add("RecordId", typeof(int));
                        Categories.ToList().ForEach(x =>
                        {
                            category.Rows.Add(x.CategoryId);
                        });
                        SqlParameter categoryParam = new SqlParameter("@categoryIds", category);
                        categoryParam.SqlDbType = SqlDbType.Structured;
                        categoryParam.Direction = ParameterDirection.Input;
                        dbCommand.Parameters.Add(categoryParam);
                    }

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    dbCommand.CommandTimeout = 500; // addded this bcz add all possible combination of category, brands, region and country..it will take time 
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}