﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class GeneralSettingsDataAccess
    {
        /// <summary>
        /// This method get general settings
        /// </summary>
        /// <param name="generalSetting"></param>
        /// <returns>GeneralSettings</returns>
        public static GeneralSettings GetGeneralSettings()
        {
            GeneralSettings generalSetting = null;
            IDataReader drGeneralSettings = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetGeneralSettings))
                {
                    drGeneralSettings = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drGeneralSettings.Read())
                    {
                        generalSetting = new GeneralSettings()
                        {
                            PartnerAccountRequestEmail = Convert.ToString(drGeneralSettings["PartnerAccountRequestEmail"], CultureInfo.InvariantCulture),
                            ProductRequestEmail = Convert.ToString(drGeneralSettings["ProductRequestEmail"], CultureInfo.InvariantCulture),
                            FeedbackEmail = Convert.ToString(drGeneralSettings["FeedbackEmail"], CultureInfo.InvariantCulture),
                            GoldPartnershipAmount = Convert.ToString(drGeneralSettings["GoldPartnershipTypePricing"], CultureInfo.InvariantCulture),
                            RegularPartnershipAmount = Convert.ToString(drGeneralSettings["RegularPartnershipTypePricing"], CultureInfo.InvariantCulture),
                            FeaturedProjectVisibility = Convert.ToBoolean(drGeneralSettings["FeaturedProjectVisibility"], CultureInfo.InvariantCulture)
                        };
                    }

                    generalSetting.TermsAndConditions = new List<Tuple<string, string>>();
                    drGeneralSettings.NextResult();
                    while (drGeneralSettings.Read())
                    {
                        generalSetting.TermsAndConditions.Add(new Tuple<string, string>(Convert.ToString(drGeneralSettings["VersionId"], CultureInfo.InvariantCulture), Convert.ToString(drGeneralSettings["TermsAndCondition"], CultureInfo.InvariantCulture)));
                    }
                }

                drGeneralSettings.Close();
            }
            catch (Exception exception)
            {
                if (drGeneralSettings != null && !drGeneralSettings.IsClosed)
                {
                    drGeneralSettings.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return generalSetting;
        }

        /// <summary>
        /// This method update general settings
        /// </summary>
        /// <param name="generalSetting"></param>
        /// <returns></returns>
        public static int UpdateGeneralSettings(GeneralSettings generalSetting)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateGeneralSettings))
                {
                    db.AddInParameter(dbCommand, "@partnerAccountRequests", DbType.String, generalSetting.PartnerAccountRequestEmail);
                    db.AddInParameter(dbCommand, "@productRequestseEmail", DbType.String, generalSetting.ProductRequestEmail);
                    db.AddInParameter(dbCommand, "@feedbackEmail", DbType.String, generalSetting.FeedbackEmail);
                    db.AddInParameter(dbCommand, "@navigationVisible", DbType.Boolean, generalSetting.FeaturedProjectVisibility);
                    db.AddInParameter(dbCommand, "@goldPartner", DbType.String, generalSetting.GoldPartnershipAmount);
                    db.AddInParameter(dbCommand, "@regularPartner", DbType.String, generalSetting.RegularPartnershipAmount);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Add  terms and condition version in the database
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public static int AddTermsAndConditionsVersion(string termsAndCondition, int addedByUserId)
        {
            int result = 0;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.AddTermsAndConditionVersion))
                {
                    db.AddInParameter(dbCommand, "@addedByuserId", DbType.String, addedByUserId);
                    db.AddInParameter(dbCommand, "@termsAndConditions", DbType.String, termsAndCondition);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}