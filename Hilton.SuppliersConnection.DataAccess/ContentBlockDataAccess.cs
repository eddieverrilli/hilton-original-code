﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class ContentBlockDataAccess
    {
        /// <summary>
        /// Adds a Content Block.
        /// </summary>
        /// <param name="contentBlock"></param>
        /// <returns></returns>
        public static int AddContentBlock(ContentBlock contentBlock)
        {
            int result = -1;
            try
            {
                if (contentBlock != null)
                {
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsertContentBlock))
                    {
                        db.AddInParameter(dbCommand, "@section", DbType.String, contentBlock.Section);
                        db.AddInParameter(dbCommand, "@title", DbType.String, contentBlock.Title);
                        db.AddInParameter(dbCommand, "@description", DbType.String, contentBlock.Description);
                        db.AddInParameter(dbCommand, "@cultureTypeId", DbType.Int16, contentBlock.LanguageResourceDetail.CultureTypeId);
                        db.AddInParameter(dbCommand, "@content", DbType.String, contentBlock.LanguageResourceDetail.Content);
                        SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                        retValParam.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(retValParam);

                        db.ExecuteNonQuery(dbCommand);
                        result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Deletes A content Block
        /// </summary>
        /// <param name="contentBlock"></param>
        /// <returns></returns>
        public static int DeleteContentBlock(int contentBlockId)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.DeleteContentBlock))
                {
                    db.AddInParameter(dbCommand, "@contentBlockId", DbType.Int32, contentBlockId);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets Content Block Details
        /// </summary>
        /// <param name="contentSearch"></param>
        /// <returns></returns>
        public static IList<ContentBlock> GetContentBlock(ContentSearch contentSearch, ref int totalRecordCount)
        {
            IDataReader drContentBlock = null;
            IList<ContentBlock> contentBlockCollection = new List<ContentBlock>();

            try
            {
                if (contentSearch != null)
                {
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetContentBlockList))
                    {
                        db.AddInParameter(dbCommand, "@section", DbType.String, (string.IsNullOrEmpty(contentSearch.Section)) ? (object)DBNull.Value : contentSearch.Section);
                        db.AddInParameter(dbCommand, "@searchExpression", DbType.String, contentSearch.QuickSearchExpression);
                        db.AddInParameter(dbCommand, "@searchType", DbType.String, contentSearch.SearchFlag);
                        db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, contentSearch.PageIndex);
                        db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, contentSearch.PageSize);
                        db.AddInParameter(dbCommand, "@sortExpression", DbType.String, (string.IsNullOrEmpty(contentSearch.SortExpression)) ? (object)DBNull.Value : contentSearch.SortExpression);
                        db.AddInParameter(dbCommand, "@sortDirection", DbType.String, contentSearch.SortDirection);
                        SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                        retValParam.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(retValParam);

                        drContentBlock = (IDataReader)db.ExecuteReader(dbCommand);

                        while (drContentBlock.Read())
                        {
                            contentBlockCollection.Add(new ContentBlock()
                            {
                                ContentBlockId = Convert.ToInt32(drContentBlock["ContentBlockId"], CultureInfo.InvariantCulture),
                                Section = Convert.ToString(drContentBlock["Section"], CultureInfo.InvariantCulture),
                                Title = Convert.ToString(drContentBlock["Title"], CultureInfo.InvariantCulture),
                                Description = Convert.ToString(drContentBlock["Description"], CultureInfo.InvariantCulture),
                            }
                            );
                        }
                        drContentBlock.NextResult();
                        totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);

                        drContentBlock.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                if (drContentBlock != null && !drContentBlock.IsClosed)
                {
                    drContentBlock.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contentBlockCollection;
        }

        /// <summary>
        /// This method get the content block details
        /// </summary>
        /// <param name="contentBlockId"></param>
        /// <returns>ContentBlock</returns>
        public static ContentBlock GetContentBlockDetails(int contentBlockId)
        {
            IDataReader drContentBlockDetails = null;
            ContentBlock contentBlock = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetContentBlockDetails))
                {
                    db.AddInParameter(dbCommand, "@contentBlockId", DbType.Int32, contentBlockId);
                    drContentBlockDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drContentBlockDetails.Read())
                    {
                        contentBlock = new ContentBlock()
                        {
                            ContentBlockId = Convert.ToInt32(drContentBlockDetails["ContentBlockId"], CultureInfo.InvariantCulture),
                            Section = Convert.ToString(drContentBlockDetails["Section"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drContentBlockDetails["Title"], CultureInfo.InvariantCulture),
                            Description = Convert.ToString(drContentBlockDetails["Description"], CultureInfo.InvariantCulture),
                            LanguageResourceId = Convert.ToInt32(drContentBlockDetails["LanguageResourceId"], CultureInfo.InvariantCulture),
                        };
                    }

                    drContentBlockDetails.NextResult();
                    contentBlock.LanguageResourceDetails = new List<LanguageResourceDetail>();
                    while (drContentBlockDetails.Read())
                    {
                        contentBlock.LanguageResourceDetails.Add(new LanguageResourceDetail()
                        {
                            CultureTypeId = Convert.ToInt32(drContentBlockDetails["CultureTypeId"], CultureInfo.InvariantCulture),
                            Content = Convert.ToString(drContentBlockDetails["Content"], CultureInfo.InvariantCulture),
                        }
                        );
                    }

                    drContentBlockDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drContentBlockDetails != null && !drContentBlockDetails.IsClosed)
                {
                    drContentBlockDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contentBlock;
        }

        /// <summary>
        /// Gets the Language Resource
        /// </summary>
        /// <returns></returns>
        public static IList<LanguageResourceDetail> GetLanguageResourceDetails
        {
            get
            {
                IDataReader drLanguageResourceDetails = null;
                IList<LanguageResourceDetail> languageResourceCollection = new List<LanguageResourceDetail>();
                try
                {
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetLanguageResourceDetails))
                    {
                        drLanguageResourceDetails = (IDataReader)db.ExecuteReader(dbCommand);

                        while (drLanguageResourceDetails.Read())
                        {
                            languageResourceCollection.Add(new LanguageResourceDetail()
                            {
                                LanguageResourceId = Convert.ToInt32(drLanguageResourceDetails["LanguageResourceId"], CultureInfo.InvariantCulture),
                                Title = Convert.ToString(drLanguageResourceDetails["Title"], CultureInfo.InvariantCulture),
                                Description = Convert.ToString(drLanguageResourceDetails["Description"], CultureInfo.InvariantCulture),
                                CultureTypeId = Convert.ToInt32(drLanguageResourceDetails["CultureTypeId"], CultureInfo.InvariantCulture),
                                CultureCode = Convert.ToString(drLanguageResourceDetails["CultureCode"], CultureInfo.InvariantCulture),
                                Content = Convert.ToString(drLanguageResourceDetails["Content"], CultureInfo.InvariantCulture),
                                IsNeutralLanguage = Convert.ToBoolean(drLanguageResourceDetails["NeutralLanguage"], CultureInfo.InvariantCulture),
                            }
                            );
                        }

                        drLanguageResourceDetails.Close();
                    }
                }
                catch (Exception exception)
                {
                    if (drLanguageResourceDetails != null && !drLanguageResourceDetails.IsClosed)
                    {
                        drLanguageResourceDetails.Close();
                    }
                    bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return languageResourceCollection;
            }
        }

        /// <summary>
        /// Updates Content Block
        /// </summary>
        /// <param name="contentBlock"></param>
        /// <returns></returns>
        public static int UpdateContentBlock(ContentBlock contentBlock)
        {
            int result = -1;
            try
            {
                if (contentBlock != null)
                {
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateContentBlock))
                    {
                        db.AddInParameter(dbCommand, "@mode", DbType.String, contentBlock.Mode);
                        db.AddInParameter(dbCommand, "@contentBlockId", DbType.Int32, contentBlock.ContentBlockId);
                        db.AddInParameter(dbCommand, "@section", DbType.String, contentBlock.Section);
                        db.AddInParameter(dbCommand, "@title", DbType.String, contentBlock.Title);
                        db.AddInParameter(dbCommand, "@languageResourceId", DbType.Int32, contentBlock.LanguageResourceId);
                        db.AddInParameter(dbCommand, "@description", DbType.String, contentBlock.Description);
                        db.AddInParameter(dbCommand, "@cultureTypeId", DbType.Int16, contentBlock.LanguageResourceDetail.CultureTypeId);
                        db.AddInParameter(dbCommand, "@content", DbType.String, contentBlock.LanguageResourceDetail.Content);
                        SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                        retValParam.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(retValParam);

                        db.ExecuteNonQuery(dbCommand);
                        result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Update Content Blocks
        /// </summary>
        /// <returns></returns>
        public static IList<ContentBlock> UpdateContentBlocks()
        {
            IDataReader drContentBlocks = null;
            IList<ContentBlock> contentBlockCollection = new List<ContentBlock>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetContentBlocks))
                {
                    drContentBlocks = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drContentBlocks.Read())
                    {
                        contentBlockCollection.Add(new ContentBlock()
                        {
                            ContentBlockId = Convert.ToInt32(drContentBlocks["ContentBlockId"], CultureInfo.InvariantCulture),
                            Section = Convert.ToString(drContentBlocks["Section"], CultureInfo.InvariantCulture),
                            LanguageResourceId = Convert.ToInt32(drContentBlocks["LanguageResourceId"], CultureInfo.InvariantCulture),
                        }
                        );
                    }

                    drContentBlocks.Close();
                }
            }
            catch (Exception exception)
            {
                if (drContentBlocks != null && !drContentBlocks.IsClosed)
                {
                    drContentBlocks.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contentBlockCollection;
        }
    }
}