﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class EmailTemplateDataAccess
    {
        /// <summary>
        /// Adds an Email Template
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <returns></returns>
        public static int AddEmailTemplate(EmailTemplate emailTemplate)
        {
            int result = -1;
            try
            {
                if (emailTemplate != null)
                {
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateEmailTemplate))
                    {
                        db.AddInParameter(dbCommand, "@mode", DbType.String, emailTemplate.Mode);
                        db.AddInParameter(dbCommand, "@emailTemplateName", DbType.String, emailTemplate.EmailTemplateName);
                        db.AddInParameter(dbCommand, "@subject", DbType.String, emailTemplate.Subject);
                        db.AddInParameter(dbCommand, "@body", DbType.String, emailTemplate.Body);
                        db.AddInParameter(dbCommand, "@description", DbType.String, emailTemplate.Description);
                        db.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, emailTemplate.Status);
                        SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                        retValParam.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(retValParam);

                        db.ExecuteNonQuery(dbCommand);
                        result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets Email Template Details
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <param name="totalRecordCount"></param>
        /// <returns></returns>
        public static IList<EmailTemplate> GetEmailTemplate(EmailTemplateSearch emailTemplateSearch)
        {
            IDataReader drEmailTemplate = null;
            IList<EmailTemplate> emailTemplateCollection = new List<EmailTemplate>();

            try
            {
                if (emailTemplateSearch != null)
                {
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetEmailTemplateList))
                    {
                        db.AddInParameter(dbCommand, "@sortExpression", DbType.String, (string.IsNullOrEmpty(emailTemplateSearch.SortExpression)) ? (object)DBNull.Value : emailTemplateSearch.SortExpression);
                        db.AddInParameter(dbCommand, "@sortDirection", DbType.String, emailTemplateSearch.SortDirection);

                        drEmailTemplate = (IDataReader)db.ExecuteReader(dbCommand);

                        while (drEmailTemplate.Read())
                        {
                            emailTemplateCollection.Add(new EmailTemplate()
                            {
                                EmailTemplateId = Convert.ToInt32(drEmailTemplate["EmailTemplateId"], CultureInfo.InvariantCulture),
                                EmailTemplateName = Convert.ToString(drEmailTemplate["EmailTemplateName"], CultureInfo.InvariantCulture),
                                Status = Convert.ToBoolean(drEmailTemplate["Status"], CultureInfo.InvariantCulture),
                                Description = Convert.ToString(drEmailTemplate["Description"], CultureInfo.InvariantCulture),
                            }
                            );
                        }

                        drEmailTemplate.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (drEmailTemplate != null && !drEmailTemplate.IsClosed)
                {
                    drEmailTemplate.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
            }

            return emailTemplateCollection;
        }

        /// <summary>
        /// Gets Email Template Details
        /// </summary>
        /// <param name="emailTemplateId"></param>
        /// <returns>EmailTemplate</returns>
        public static EmailTemplate GetEmailTemplateDetails(int emailTemplateId)
        {
            IDataReader drcontentEmailTemaplateDetails = null;
            EmailTemplate emailTemplate = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetEmailTemplateDetails))
                {
                    db.AddInParameter(dbCommand, "@emailTemplateId", DbType.Int32, emailTemplateId);
                    drcontentEmailTemaplateDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drcontentEmailTemaplateDetails.Read())
                    {
                        emailTemplate = new EmailTemplate()
                        {
                            EmailTemplateId = Convert.ToInt32(drcontentEmailTemaplateDetails["EmailTemplateId"], CultureInfo.InvariantCulture),
                            Description = Convert.ToString(drcontentEmailTemaplateDetails["Description"], CultureInfo.InvariantCulture),
                            Body = Convert.ToString(drcontentEmailTemaplateDetails["Body"], CultureInfo.InvariantCulture),
                            Subject = Convert.ToString(drcontentEmailTemaplateDetails["Subject"], CultureInfo.InvariantCulture),
                            Status = Convert.ToBoolean(drcontentEmailTemaplateDetails["IsActive"], CultureInfo.InvariantCulture),
                            EmailTemplateName = Convert.ToString(drcontentEmailTemaplateDetails["EmailTemplateName"], CultureInfo.InvariantCulture),
                        };
                    }

                    drcontentEmailTemaplateDetails.Close();
                }
            }
            catch (Exception ex)
            {
                if (drcontentEmailTemaplateDetails != null && !drcontentEmailTemaplateDetails.IsClosed)
                {
                    drcontentEmailTemaplateDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref ex);
                if (rethrow)
                {
                    throw ex;
                }
            }
            return emailTemplate;
        }

        /// <summary>
        /// Update an Email Template
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <returns></returns>
        public static int UpdateEmailTemplate(EmailTemplate emailTemplate)
        {
            int result = -1;
            try
            {
                if (emailTemplate != null)
                {
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateEmailTemplate))
                    {
                        db.AddInParameter(dbCommand, "@mode", DbType.String, emailTemplate.Mode);
                        db.AddInParameter(dbCommand, "@emailTemplateId", DbType.Int32, emailTemplate.EmailTemplateId);
                        db.AddInParameter(dbCommand, "@emailTemplateName", DbType.String, emailTemplate.EmailTemplateName);
                        db.AddInParameter(dbCommand, "@subject", DbType.String, emailTemplate.Subject);
                        db.AddInParameter(dbCommand, "@body", DbType.String, emailTemplate.Body);
                        db.AddInParameter(dbCommand, "@description", DbType.String, emailTemplate.Description);
                        db.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, emailTemplate.Status);
                        SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                        retValParam.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(retValParam);

                        db.ExecuteNonQuery(dbCommand);
                        result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Send mail to users
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <returns></returns>
        public static bool SendMailtoUsers(EmailTemplate emailRecipients)
        {
            bool result = false;
            try
            {
                if (emailRecipients != null)
                {
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.SendMailtoUsers))
                    {
                        db.AddInParameter(dbCommand, "@templateid", DbType.Int32, emailRecipients.EmailTemplateId);
                        db.AddInParameter(dbCommand, "@recipient", DbType.String, emailRecipients.RecipientsMailAddress);
                        db.AddInParameter(dbCommand, "@ToName", DbType.String, emailRecipients.ToName);
                        db.AddInParameter(dbCommand, "@from", DbType.String, emailRecipients.SenderEmailAddress);
                        db.AddInParameter(dbCommand, "@FromName", DbType.String, emailRecipients.FromName);
                        db.AddInParameter(dbCommand, "@message", DbType.String, emailRecipients.Body);

                        db.ExecuteNonQuery(dbCommand);
                        result = true;
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}