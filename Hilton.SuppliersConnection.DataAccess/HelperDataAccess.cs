﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Globalization; 
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class HelperDataAccess
    {
        /// <summary>
        ///  Get the categories for filter
        /// </summary>
        /// <returns>List of Category Entity</returns>
        public static IList<Category> GetActiveCategories()
        {
            IList<Category> categories = new List<Category>();
            IDataReader drCategories = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetActiveCategories))
                {
                    drCategories = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCategories.Read())
                    {
                        categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drCategories["CategoryId"], CultureInfo.InvariantCulture),
                            CategoryName = Convert.ToString(drCategories["CategoryName"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drCategories["ParentCategoryId"], CultureInfo.InvariantCulture),
                            RootParentId = Convert.ToInt32(drCategories["RootParentId"], CultureInfo.InvariantCulture),
                            Level = Convert.ToInt32(drCategories["Level"], CultureInfo.InvariantCulture),
                            IsLeaf = Convert.ToBoolean(drCategories["IsLeaf"], CultureInfo.InvariantCulture),
                            SequenceId = drCategories["SequenceId"].ToString(),
                            ParentCategoryHierarchy = Convert.ToString(drCategories["ParentCategoryHierarchy"], CultureInfo.InvariantCulture),
                        });
                    }
                }
                drCategories.Close();
            }
            catch (Exception exception)
            {
                if (drCategories != null && !drCategories.IsClosed)
                {
                    drCategories.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return categories;
        }

        /// <summary>
        /// To get Auto Complete List
        /// </summary>
        /// <param name="dynamicData"></param>
        public static void GetAutoCompleteList(DynamicDropDownData dynamicData)
        {
            IDataReader drDynamicData = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetAutoCompleteList))
                {
                    drDynamicData = (IDataReader)db.ExecuteReader(dbCommand);

                    //Getting Section auto complete list
                    dynamicData.SectionDataList = new Collection<KeyValuePair<string, string>>();
                    while (drDynamicData.Read())
                    {
                        dynamicData.SectionDataList.Add(new KeyValuePair<string, string>(
                            Convert.ToString(drDynamicData["DynamicDataKey"], CultureInfo.InvariantCulture),
                            Convert.ToString(drDynamicData["DynamicDataValue"], CultureInfo.InvariantCulture)));
                    }

                    drDynamicData.NextResult();

                    //Getting Title auto complete list
                    dynamicData.TitleDataList = new Collection<KeyValuePair<string, string>>();
                    while (drDynamicData.Read())
                    {
                        dynamicData.TitleDataList.Add(new KeyValuePair<string, string>(
                            Convert.ToString(drDynamicData["DynamicDataKey"], CultureInfo.InvariantCulture),
                            Convert.ToString(drDynamicData["DynamicDataValue"], CultureInfo.InvariantCulture)));
                    }

                    drDynamicData.Close();
                }
            }
            catch (Exception exception)
            {
                if (drDynamicData != null && !drDynamicData.IsClosed)
                {
                    drDynamicData.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// To Get Configuration Settings
        /// </summary>
        /// <returns>ConfigurationSetting</returns>
        public static ConfigurationSetting GetConfigurationSettings()
        {
            IDataReader drConfigurationSettings = null;
            ConfigurationSetting configurationSettings = new ConfigurationSetting();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetConfigurationSettings))
                {
                    drConfigurationSettings = (IDataReader)db.ExecuteReader(dbCommand);

                    configurationSettings.AppSettings = new List<KeyValuePair<string, string>>();

                    //Getting Application Settings
                    while (drConfigurationSettings.Read())
                    {
                        configurationSettings.AppSettings.Add(new KeyValuePair<string, string>(
                             Convert.ToString(drConfigurationSettings["Key"], CultureInfo.InvariantCulture),
                             Convert.ToString(drConfigurationSettings["Value"], CultureInfo.InvariantCulture)));
                    }

                    drConfigurationSettings.NextResult();

                    //Getting Application Messages
                    configurationSettings.ApplicationMessages = new List<ApplicationMessageDetails>();

                    //Getting Application Settings
                    while (drConfigurationSettings.Read())
                    {
                        configurationSettings.ApplicationMessages.Add(new ApplicationMessageDetails()
                        {
                            MessageId = Convert.ToInt32(drConfigurationSettings["MessageId"], CultureInfo.InvariantCulture),
                            MessageCode = Convert.ToString(drConfigurationSettings["MessageCode"], CultureInfo.InvariantCulture),
                            Message = Convert.ToString(drConfigurationSettings["Message"], CultureInfo.InvariantCulture),
                            CultureCode = Convert.ToString(drConfigurationSettings["CultureCode"], CultureInfo.InvariantCulture),
                            CultureTypeId = Convert.ToInt32(drConfigurationSettings["CultureId"], CultureInfo.InvariantCulture),
                            IsNeutralLanguage = Convert.ToBoolean(drConfigurationSettings["NeutralLanguage"], CultureInfo.InvariantCulture),
                        }
                           );
                    }

                    drConfigurationSettings.Close();
                }
            }

            catch (Exception exception)
            {
                if (drConfigurationSettings != null && !drConfigurationSettings.IsClosed)
                {
                    drConfigurationSettings.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return configurationSettings;
        }

        /// <summary>
        /// Link the web account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public static IList<ReportColumns> GetReportColumnDetails(int reportId)
        {
            IDataReader drReportColumnDetails = null;
            IList<ReportColumns> reportColumnsDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetReportColumnDetails))
                {
                    db.AddInParameter(dbCommand, "@ReportId", DbType.Int32, reportId);

                    drReportColumnDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    reportColumnsDetails = new List<ReportColumns>();

                    while (drReportColumnDetails.Read())
                    {
                        reportColumnsDetails.Add(new ReportColumns()
                        {
                            ReportId = Convert.ToInt32(drReportColumnDetails["ReportId"]),
                            AutoFit = Convert.ToBoolean(drReportColumnDetails["Autofit"]),
                            CellColor = Convert.ToString(drReportColumnDetails["CellColor"]),
                            ColumnName = Convert.ToString(drReportColumnDetails["ColumnName"]),
                            HAlign = Convert.ToString(drReportColumnDetails["HAlign"]),
                            VAlign = Convert.ToString(drReportColumnDetails["VAlign"]),
                            HeaderBgColor = Convert.ToString(drReportColumnDetails["HeaderBgColor"]),
                            Height = Convert.ToInt32(drReportColumnDetails["Height"]),
                            Width = Convert.ToInt32(drReportColumnDetails["Width"]),
                            Visible = Convert.ToBoolean(drReportColumnDetails["Visible"]).Equals(true) ? "block" : "none",
                            HeaderHAlign = Convert.ToString(drReportColumnDetails["HeaderHAlign"]),
                            HeaderVAlign = Convert.ToString(drReportColumnDetails["HeaderVAlign"]),
                            HeaderTextColor = Convert.ToString(drReportColumnDetails["HeaderTextColor"]),
                        });
                    }
                }
            }
            catch (Exception genericException)
            {
                if (drReportColumnDetails != null && !drReportColumnDetails.IsClosed)
                {
                    drReportColumnDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return reportColumnsDetails;
        }

        /// <summary>
        /// To Get Dynamic DropDownList
        /// </summary>
        /// <param name="feedback"></param>
        /// <returns></returns>
        public static void GetDynamicDropDownList(DynamicDropDown dropDown)
        {
            IDataReader drDropdown = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetDynamicDropDown))
                {
                    drDropdown = (IDataReader)db.ExecuteReader(dbCommand);

                    dropDown.SectionDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.SectionDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["SectionValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["SectionText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    //Populate partner dropdown
                    drDropdown.NextResult();
                    dropDown.FeedbackPartnerDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.FeedbackPartnerDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["SectionValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["SectionText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    //populate sender dropdown
                    drDropdown.NextResult();
                    dropDown.SenderDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.SenderDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["SenderValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["SenderText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //populate recipient
                    dropDown.RecipientDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.RecipientDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["RecipientValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["RecipientText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //populate Region
                    dropDown.MessageRegionDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.MessageRegionDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["RegionValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["RegionText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Partner dropdown collection
                    dropDown.PartnerDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.PartnerDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["PartnerId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["Description"], CultureInfo.InvariantCulture)
                        }
                      );
                    }

                    drDropdown.NextResult();
                    //Getting active partner drop down collection
                    dropDown.ActivePartnersDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.ActivePartnersDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["PartnerId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["CompanyName"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting term and conditions versions in drop down collection
                    dropDown.TermsAndConditions = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.TermsAndConditions.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["VersionId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["VersionName"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.Close();
                }
            }

            catch (Exception exception)
            {
                if (drDropdown != null && !drDropdown.IsClosed)
                {
                    drDropdown.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// To Get Static DropDownList
        /// </summary>
        /// <param name="feedback"></param>
        /// <returns></returns>
        public static void GetStaticDropDownList(StaticDropDown dropDown)
        {
            IDataReader drDropdown = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetStaticDropDowns))
                {
                    drDropdown = (IDataReader)db.ExecuteReader(dbCommand);

                    //Getting Language dropdown collection
                    dropDown.LanguageDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.LanguageDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["CultureTypeId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["Description"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Feedback status dropdown collection
                    dropDown.FeedbackStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.FeedbackStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["StatusValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["StatusText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Rating dropdown collection
                    dropDown.RatingDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.RatingDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["RatingValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["RatingText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Region dropdown collection
                    dropDown.RegionDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.RegionDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["RegionId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["Description"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Brand dropdown collection
                    dropDown.BrandDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.BrandDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["BrandId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["Description"], CultureInfo.InvariantCulture),
                            ParentDataField = Convert.ToString(drDropdown["BrandSegmentName"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Property dropdown collection
                    dropDown.PropertyTypesDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.PropertyTypesDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["PropertyTypeId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["Description"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Product Status dropdown collection
                    dropDown.ProductStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.ProductStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["StatusId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["Description"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Message Status dropdown collection
                    dropDown.MessageStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.MessageStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["StatusValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["Description"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting User Type dropdown collection
                    dropDown.UserTypesDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.UserTypesDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["UserTypeId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["UserTypeDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting User Status dropdown collection
                    dropDown.UserStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.UserStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["UserStatusId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["UserStatusDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Project Status dropdown collection
                    dropDown.ProjectStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.ProjectStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["ProjectStatusId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["ProjectStatusDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Project Status dropdown collection
                    dropDown.CountryDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.CountryDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["CountryId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["CountryName"], CultureInfo.InvariantCulture),
                            ParentDataField = Convert.ToString(drDropdown["RegionId"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Project Status dropdown collection
                    dropDown.PartnerStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.PartnerStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["PartnershipStatusId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["PartnershipStatusDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Project Template Status dropdown collection
                    dropDown.ProjectTemplateStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.ProjectTemplateStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["ProjectTemplateStatusValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["ProjectTemplateStatusDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting Project Status dropdown collection
                    dropDown.ProjectTypesDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.ProjectTypesDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["ProjectTypeId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["ProjectTypeDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting states drop down collection
                    dropDown.StateDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.StateDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["StateId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["StateName"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting admin permission drop down collection
                    dropDown.AdminPermissionsDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.AdminPermissionsDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["AdminPermissionId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["AdminPermissionDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }
                    drDropdown.NextResult();
                    //Getting owner + all consultant role drop down collection
                    dropDown.OwnerAndConsultantRolesDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.OwnerAndConsultantRolesDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["RoleId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["RoleDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting partnership type  drop down collection
                    dropDown.PartnershipTypeDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.PartnershipTypeDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["PartnershipTypeId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["PartnershipTypeDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting partnership status  drop down collection
                    dropDown.PartnershipStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.PartnershipStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["PartnershipStatusId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["PartnershipStatusDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting partnership type  drop down collection
                    dropDown.PaymentTypesDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.PaymentTypesDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["PaymentTypeId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["PaymentTypeDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.NextResult();
                    //Getting check status  drop down collection
                    dropDown.TransactionStatusDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.TransactionStatusDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["TransactionStatusId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["TransactionStatusDesc"], CultureInfo.InvariantCulture)
                        }
                       );
                    }
                    drDropdown.NextResult();
                    dropDown.UserProjectMappingTypeDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.UserProjectMappingTypeDropDown.Add(new DropDownItem()
                                                {
                                                    DataValueField = Convert.ToString(drDropdown["UserTypeId"], CultureInfo.InvariantCulture),
                                                    DataTextField = Convert.ToString(drDropdown["UserTypeDesc"], CultureInfo.InvariantCulture)
                                                }
                                                );
                    }
                    drDropdown.NextResult();
                    dropDown.ConsultantRoleDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDown.ConsultantRoleDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["RoleId"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["RoleDesc"], CultureInfo.InvariantCulture)
                        }
                                                );
                    }
                    drDropdown.Close();
                }
            }

            catch (Exception exception)
            {
                if (drDropdown != null && !drDropdown.IsClosed)
                {
                    drDropdown.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Update Auto Complete List
        /// </summary>
        /// <param name="dynamicData"></param>
        /// <param name="listType"></param>
        public static void UpdateAutoCompleteList(DynamicDropDownData dynamicData, string listType)
        {
            IDataReader drDynamicData = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetAutoCompleteList))
                {
                    db.AddInParameter(dbCommand, "@listType", DbType.String, listType);
                    drDynamicData = (IDataReader)db.ExecuteReader(dbCommand);

                    switch (listType)
                    {
                        //Update Section auto complete list
                        case AutoCompleteListConstants.SectionAutoCompleteList:
                            dynamicData.SectionDataList = new Collection<KeyValuePair<string, string>>();
                            while (drDynamicData.Read())
                            {
                                dynamicData.SectionDataList.Add(new KeyValuePair<string, string>(
                                    Convert.ToString(drDynamicData["DynamicDataKey"], CultureInfo.InvariantCulture),
                                    Convert.ToString(drDynamicData["DynamicDataValue"], CultureInfo.InvariantCulture)));
                            }
                            break;
                        //Update Title auto complete list
                        case AutoCompleteListConstants.TitleAutoCompleteList:
                            dynamicData.TitleDataList = new Collection<KeyValuePair<string, string>>();
                            while (drDynamicData.Read())
                            {
                                dynamicData.TitleDataList.Add(new KeyValuePair<string, string>(
                                    Convert.ToString(drDynamicData["DynamicDataKey"], CultureInfo.InvariantCulture),
                                    Convert.ToString(drDynamicData["DynamicDataValue"], CultureInfo.InvariantCulture)));
                            }
                            break;
                    }

                    drDynamicData.Close();
                }
            }
            catch (Exception exception)
            {
                if (drDynamicData != null && !drDynamicData.IsClosed)
                {
                    drDynamicData.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Update Dynamic DropDownList
        /// </summary>
        /// <param name="dropDown"></param>
        /// <param name="dropDownType"></param>
        public static void UpdateDynamicDropDownList(DynamicDropDown dropDown, string dropDownType)
        {
            IDataReader drDropdown = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateDynamicDropDown))
                {
                    db.AddInParameter(dbCommand, "@dropDownType", DbType.String, dropDownType);
                    drDropdown = (IDataReader)db.ExecuteReader(dbCommand);

                    switch (dropDownType)
                    {
                        //Getting section dropdown collection
                        case DropDownConstants.SectionDropDown:
                            dropDown.SectionDropDown = new List<DropDownItem>();
                            while (drDropdown.Read())
                            {
                                dropDown.SectionDropDown.Add(new DropDownItem()
                                {
                                    DataValueField = Convert.ToString(drDropdown["SectionValue"], CultureInfo.InvariantCulture),

                                    DataTextField = Convert.ToString(drDropdown["SectionText"], CultureInfo.InvariantCulture)
                                }
                               );
                            }
                            break;
                        //Getting partner dropdown collection
                        case DropDownConstants.PartnerDropDown:
                            dropDown.PartnerDropDown = new List<DropDownItem>();
                            while (drDropdown.Read())
                            {
                                dropDown.PartnerDropDown.Add(new DropDownItem()
                                {
                                    DataValueField = Convert.ToString(drDropdown["SectionValue"], CultureInfo.InvariantCulture),

                                    DataTextField = Convert.ToString(drDropdown["SectionText"], CultureInfo.InvariantCulture)
                                }
                               );
                            }
                            break;
                        //Getting terms and Condition dropdown collection
                        case DropDownConstants.TermsAndConditions:
                            dropDown.TermsAndConditions = new List<DropDownItem>();
                            while (drDropdown.Read())
                            {
                                dropDown.TermsAndConditions.Add(new DropDownItem()
                                {
                                    DataValueField = Convert.ToString(drDropdown["SectionValue"], CultureInfo.InvariantCulture),

                                    DataTextField = Convert.ToString(drDropdown["SectionText"], CultureInfo.InvariantCulture)
                                }
                               );
                            }
                            break;
                    }

                    drDropdown.Close();
                }
            }
            catch (Exception exception)
            {
                if (drDropdown != null && !drDropdown.IsClosed)
                {
                    drDropdown.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Gets the Regular Expression validation expressions
        /// </summary>
        /// <returns></returns>
        public static IList<RegularExpressionTypeDetails> GetRegularExpressionTypes()
        {
            IDataReader drRegularExpressionTypesDetails = null;
            IList<RegularExpressionTypeDetails> regularExpressionTypesCollection = new List<RegularExpressionTypeDetails>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetRegularExpressionTypeDetails))
                {
                    drRegularExpressionTypesDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drRegularExpressionTypesDetails.Read())
                    {
                        regularExpressionTypesCollection.Add(new RegularExpressionTypeDetails()
                        {
                            RegularExpressionTypeId = Convert.ToInt32(drRegularExpressionTypesDetails["RegularExpressionTypeId"], CultureInfo.InvariantCulture),
                            RegularExpression = Convert.ToString(drRegularExpressionTypesDetails["RegularExpression"], CultureInfo.InvariantCulture),
                            Description = Convert.ToString(drRegularExpressionTypesDetails["Description"], CultureInfo.InvariantCulture),
                            CultureTypeId = Convert.ToInt32(drRegularExpressionTypesDetails["CultureTypeId"], CultureInfo.InvariantCulture),
                            CultureCode = Convert.ToString(drRegularExpressionTypesDetails["CultureCode"], CultureInfo.InvariantCulture),
                            IsNeutralLanguage = Convert.ToBoolean(drRegularExpressionTypesDetails["NeutralLanguage"], CultureInfo.InvariantCulture),
                        }
                        );
                    }

                    drRegularExpressionTypesDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drRegularExpressionTypesDetails != null && !drRegularExpressionTypesDetails.IsClosed)
                {
                    drRegularExpressionTypesDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return regularExpressionTypesCollection;
        }

        /// <summary>
        /// Gets the validation messages
        /// </summary>
        /// <returns></returns>
        public static IList<ValidationMessageDetails> GetValidationMessages()
        {
            IDataReader drValidationMessageDetails = null;
            IList<ValidationMessageDetails> validationMessageDetailCollection = new List<ValidationMessageDetails>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetValidationMessageDetails))
                {
                    drValidationMessageDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drValidationMessageDetails.Read())
                    {
                        validationMessageDetailCollection.Add(new ValidationMessageDetails()
                        {
                            ValiationMessageId = Convert.ToInt32(drValidationMessageDetails["ValiationMessageId"], CultureInfo.InvariantCulture),
                            Message = Convert.ToString(drValidationMessageDetails["Message"], CultureInfo.InvariantCulture),
                            CultureTypeId = Convert.ToInt32(drValidationMessageDetails["CultureTypeId"], CultureInfo.InvariantCulture),
                            CultureCode = Convert.ToString(drValidationMessageDetails["CultureCode"], CultureInfo.InvariantCulture),
                            IsNeutralLanguage = Convert.ToBoolean(drValidationMessageDetails["NeutralLanguage"], CultureInfo.InvariantCulture),
                        }
                        );
                    }

                    drValidationMessageDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drValidationMessageDetails != null && !drValidationMessageDetails.IsClosed)
                {
                    drValidationMessageDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return validationMessageDetailCollection;
        }

        /// <summary>
        /// To Get Product Pdf
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static Product GetProductPdf(int productId)
        {
            IDataReader drProductDetails = null;
            Product productPdfDetails = new Product();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductPdf))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, productId);
                    db.AddInParameter(dbCommand, "@isOther", DbType.Boolean, false);
                    drProductDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drProductDetails.Read())
                    {
                        productPdfDetails = new Product()
                        {
                            SpecSheetPdfName = drProductDetails["SpecSheetPDFName"].ToString(),
                            SpecSheetPdf = drProductDetails["SpecSheetPDF"].Equals(DBNull.Value) ? null : (byte[])drProductDetails["SpecSheetPDF"],
                        };
                    }

                    drProductDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProductDetails != null && !drProductDetails.IsClosed)
                {
                    drProductDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return productPdfDetails;
        }
    }
}