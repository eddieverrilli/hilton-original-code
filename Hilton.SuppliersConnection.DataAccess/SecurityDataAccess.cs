﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class SecurityDataAccess
    {
        /// <summary>
        /// Save he terms and condition agreed by the users
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public static bool RecordTermsAndConditionAgreement(Acknowledgement termAndcondition)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.RecordTermsAndCondition))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.String, termAndcondition.UserId);
                    db.AddInParameter(dbCommand, "@termAndConditionAgreed", DbType.String, termAndcondition.TermAndCondition);
                    db.AddInParameter(dbCommand, "@termsAndConditionVersion ", DbType.String, termAndcondition.TermAndConditionVersion);
                    db.AddInParameter(dbCommand, "@IPAddress", DbType.String, termAndcondition.IPAddress);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result == 1)
                return true;
            else
                return false;
        }

        /// <summary>
        ///  Gets the user permission
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<UserPermissions> GetMenus(int userTypeId, bool adminMenu, int userId)
        {
            IDataReader drUserPermissionDetails = null;
            List<UserPermissions> lstUserPermissions = new List<UserPermissions>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetMenus))
                {
                    db.AddInParameter(dbCommand, "UserTypeId", DbType.Int32, userTypeId);
                    db.AddInParameter(dbCommand, "AdminMenu", DbType.Boolean, adminMenu);
                    db.AddInParameter(dbCommand, "UserId", DbType.Int32, userId);
                    drUserPermissionDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drUserPermissionDetails.Read())
                    {
                        lstUserPermissions.Add(new UserPermissions()
                        {
                            MenuStructureId = Convert.ToInt32(drUserPermissionDetails["MenuStructureID"], CultureInfo.InvariantCulture),
                            UserTypeId = Convert.ToInt32(drUserPermissionDetails["UserTypeID"], CultureInfo.InvariantCulture),
                            MenuId = Convert.ToInt32(drUserPermissionDetails["MenuId"], CultureInfo.InvariantCulture),
                            MenuName = Convert.ToString(drUserPermissionDetails["MenuName"], CultureInfo.InvariantCulture),
                            MenuContent = Convert.ToString(drUserPermissionDetails["MenuContent"], CultureInfo.InvariantCulture),
                            Command = Convert.ToString(drUserPermissionDetails["Command"], CultureInfo.InvariantCulture),
                            ParentMenuId = Convert.ToInt32(drUserPermissionDetails["ParentMenuId"], CultureInfo.InvariantCulture),
                            Sequence = Convert.ToInt32(drUserPermissionDetails["Sequence"], CultureInfo.InvariantCulture),
                            DisplayDirection = Convert.ToInt32(drUserPermissionDetails["DisplayDirection"], CultureInfo.InvariantCulture),
                        });
                    }

                    drUserPermissionDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drUserPermissionDetails != null && !drUserPermissionDetails.IsClosed)
                {
                    drUserPermissionDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return lstUserPermissions;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static IList<Menu> UpdateMenuAccess(int userId)
        {
            List<Menu> userMenuPermissions = new List<Menu>();
            IDataReader druserMenuPermissions = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateUserPermissionAccess))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.String, userId);
                    druserMenuPermissions = (IDataReader)db.ExecuteReader(dbCommand);
                    while (druserMenuPermissions.Read())
                    {
                        userMenuPermissions.Add(new Menu()
                        {
                            MenuId = Convert.ToInt32(druserMenuPermissions["MenuId"], CultureInfo.InvariantCulture),
                            Name = Convert.ToString(druserMenuPermissions["Name"], CultureInfo.InvariantCulture),
                        });
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userMenuPermissions;
        }

        /// <summary>
        /// Creates user session
        /// </summary>
        /// <param name="userNumber"></param>
        /// <returns>Dataset of user details</returns>
        public static User CreateUserSession(string hiltonId, string hiltonNumber)
        {
            IDataReader drUserDetails = null;
            User user = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetLogOnUserDetails))
                {
                    db.AddInParameter(dbCommand, "@hiltonUserName", DbType.String, hiltonId);
                    db.AddInParameter(dbCommand, "@hiltonId", DbType.String, (hiltonNumber == string.Empty) ? (object)DBNull.Value : hiltonNumber);
                    drUserDetails = (IDataReader)db.ExecuteReader(dbCommand);
                    if (drUserDetails.Read())
                    {
                        user = new User()
                        {
                            FirstName = Convert.ToString(drUserDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drUserDetails["LastName"], CultureInfo.InvariantCulture),
                            UserStatusId = Convert.ToInt32(drUserDetails["StatusId"], CultureInfo.InvariantCulture),
                            HiltonUserId = Convert.IsDBNull(drUserDetails["HiltonUserId"]) ? 0 : Convert.ToInt32(drUserDetails["HiltonUserId"], CultureInfo.InvariantCulture),
                            HiltonUserName = drUserDetails["HiltonUserName"] != null ? Convert.ToString(drUserDetails["HiltonUserName"], CultureInfo.InvariantCulture) : string.Empty,
                            UserTypeId = Convert.ToInt32(drUserDetails["TypeId"], CultureInfo.InvariantCulture),
                            UserId = Convert.ToInt32(drUserDetails["UserId"], CultureInfo.InvariantCulture),
                            //Company = drUserDetails["Company"] != DBNull.Value ?
                            //       Convert.ToString(drUserDetails["Company"], CultureInfo.InvariantCulture) : string.Empty,
                            Email = Convert.ToString(drUserDetails["Email"], CultureInfo.InvariantCulture),
                            //PartnershipType = Convert.ToInt32(drUserDetails["PartnershipType"], CultureInfo.InvariantCulture),
                            //IsPartnershipExpired = Convert.ToBoolean(drUserDetails["IsPartnershipExpired"], CultureInfo.InvariantCulture),
                            ShowTermsAndCondition = Convert.ToBoolean(drUserDetails["ShowTermsAndCondition"], CultureInfo.InvariantCulture),
                            TermsAndCondition = Convert.ToString(drUserDetails["TermsAndCondition"], CultureInfo.InvariantCulture),
                            TermsAndConditionVersion = Convert.ToString(drUserDetails["VersionId"], CultureInfo.InvariantCulture),
                        };
                    }

                    if (user != null)
                    {
                        drUserDetails.NextResult();
                        user.AccessibleCategoryList = new List<Category>();
                        while (drUserDetails.Read())
                        {
                            user.AccessibleCategoryList.Add(new Category()
                            {
                                CategoryId = Convert.ToInt32(drUserDetails["CategoryId"], CultureInfo.InvariantCulture),
                            });
                        }

                        drUserDetails.NextResult();
                        user.Permissions = new List<Permission>();
                        while (drUserDetails.Read())
                        {
                            user.Permissions.Add(new Permission()
                            {
                                PermissionId = Convert.ToInt32(drUserDetails["AdminPermissionId"], CultureInfo.InvariantCulture),
                                PermissionDescription = Convert.ToString(drUserDetails["AdminPermissionDesc"], CultureInfo.InvariantCulture),
                            });
                        }

                        drUserDetails.NextResult();
                        user.MenuAccess = new List<Menu>();
                        while (drUserDetails.Read())
                        {
                            user.MenuAccess.Add(new Menu()
                            {
                                MenuId = Convert.ToInt32(drUserDetails["MenuId"], CultureInfo.InvariantCulture),
                                Name = Convert.ToString(drUserDetails["Name"], CultureInfo.InvariantCulture),
                            });
                        }

                        drUserDetails.NextResult();
                        user.UserRegions = new List<Region>();
                        while (drUserDetails.Read())
                        {
                            user.UserRegions.Add(new Region()
                            {
                                RegionId = Convert.ToInt32(drUserDetails["RegionId"], CultureInfo.InvariantCulture),
                            });
                        }

                        drUserDetails.NextResult();
                        user.AssociatedPartners = new List<Partner>();
                        while (drUserDetails.Read())
                        {
                            user.AssociatedPartners.Add(new Partner()
                            {
                                PartnerId = Convert.ToString(drUserDetails["PartnerId"], CultureInfo.InvariantCulture),
                                CompanyDescription = Convert.ToString(drUserDetails["CompanyDescription"], CultureInfo.InvariantCulture),
                                PartnershipTypeId = Convert.ToInt32(drUserDetails["PartnershipTypeId"], CultureInfo.InvariantCulture),
                                IsPartnershipExpired = Convert.ToBoolean(drUserDetails["IsPartnershipExpired"], CultureInfo.InvariantCulture),
                            });
                        }
                        if(user.AssociatedPartners.Count > 0)
                        {                            
                            user.Company = user.AssociatedPartners[0].PartnerId;
                            user.PartnershipType = user.AssociatedPartners[0].PartnershipTypeId;
                            user.IsPartnershipExpired = user.AssociatedPartners[0].IsPartnershipExpired;
                        }
                    }
                    drUserDetails.Close();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return user;
        }
    }
}