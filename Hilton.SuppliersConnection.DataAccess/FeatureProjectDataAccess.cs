﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class FeatureProjectDataAccess
    {
        /// <summary>
        /// Gets The Features Project
        /// </summary>
        /// <returns>List of Project Entity</returns>
        public static IList<Project> GetFeatureProjects()
        {
            IList<Project> featuredProjectCollection = new List<Project>();

            IDataReader drFeaturedProject = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetFeaturedProjects))
                {
                    drFeaturedProject = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drFeaturedProject.Read())
                    {
                        {
                            featuredProjectCollection.Add(new Project()
                            {
                                Property = new Facility()
                                {
                                    Address = Convert.ToString(drFeaturedProject["Address1"], CultureInfo.InvariantCulture),
                                    Address2 = Convert.ToString(drFeaturedProject["Address2"], CultureInfo.InvariantCulture),
                                    StateAbbreviation = Convert.ToString(drFeaturedProject["StateAbbreviation"], CultureInfo.InvariantCulture),
                                    FacilityUniqueName = Convert.ToString(drFeaturedProject["FacilityUniqueName"], CultureInfo.InvariantCulture),
                                    FacilityLocation = Convert.ToString(drFeaturedProject["CityName"], CultureInfo.InvariantCulture),
                                    PropertyTypeDescription = Convert.ToString(drFeaturedProject["PropertyTypeName"], CultureInfo.InvariantCulture),
                                    ProjectId = Convert.ToInt32(drFeaturedProject["ProjectId"], CultureInfo.InvariantCulture),
                                    Country = Convert.ToString(drFeaturedProject["Country"], CultureInfo.InvariantCulture),
                                    Brand = Convert.ToString(drFeaturedProject["Brand"], CultureInfo.InvariantCulture),
                                },
                                FeaturedImage = Convert.IsDBNull(drFeaturedProject["FeaturedProjectImage"]) ? null : (byte[])(drFeaturedProject["FeaturedProjectImage"]),
                            }
                            );
                        }
                    }

                    drFeaturedProject.Close();
                }
            }
            catch (Exception exception)
            {
                if (drFeaturedProject != null && !drFeaturedProject.IsClosed)
                {
                    drFeaturedProject.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return featuredProjectCollection;
        }

        /// <summary>
        /// Gets Feature Project Details
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns>Dataset</returns>
        public static DataSet GetFeaturedProjectDetails(int projectId, int userId)
        {
            DataSet dataSetFeaturedProjectDetails = null;
            try
            {
                using (dataSetFeaturedProjectDetails = new DataSet())
                {
                    dataSetFeaturedProjectDetails.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetFeaturedProjectProfileByProjectId);
                    db.AddInParameter(dbCommand, "@projectId", DbType.Int32, projectId);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    dataSetFeaturedProjectDetails = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dataSetFeaturedProjectDetails;
        }

        /// <summary>
        /// Gets Project Profile Details According to Project Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns>Project</returns>
        public static Project GetProjectProfileDetailByProjectId(int projectId)
        {
            IDataReader drProjectDetails = null;
            Project project = new Project();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProjectProfileDetailByProjectId))
                {
                    db.AddInParameter(dbCommand, "@ProjectId", DbType.Int32, projectId);
                    drProjectDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    drProjectDetails.Read();
                    {
                        project.Property = new Facility()
                        {
                            State = Convert.ToString(drProjectDetails["StateName"], CultureInfo.InvariantCulture),
                            Address = Convert.ToString(drProjectDetails["Address"], CultureInfo.InvariantCulture),
                            Address2 = Convert.ToString(drProjectDetails["Address2"], CultureInfo.InvariantCulture),
                            City = Convert.ToString(drProjectDetails["CityName"], CultureInfo.InvariantCulture),
                            FacilityId = Convert.ToInt32(drProjectDetails["FacilityId"], CultureInfo.InvariantCulture),
                            FacilityUniqueName = Convert.ToString(drProjectDetails["FacilityUniqueName"], CultureInfo.InvariantCulture),
                            PhoneNumber = Convert.ToString(drProjectDetails["PhoneNumber"], CultureInfo.InvariantCulture),
                            Country = Convert.ToString(drProjectDetails["CountryName"], CultureInfo.InvariantCulture),
                            Brand = Convert.ToString(drProjectDetails["Brand"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.ToString(drProjectDetails["ZipCode"], CultureInfo.InvariantCulture),
                        };
                        project.ProjectId = projectId;
                        project.ProjectTypeDescription = Convert.ToString(drProjectDetails["ProjectTypeName"], CultureInfo.InvariantCulture);
                        project.ProjectAddedDate = Convert.ToDateTime(drProjectDetails["ProjectAddedDate"], CultureInfo.InvariantCulture);
                        project.ProjectCompletionDate = Convert.IsDBNull(drProjectDetails["ProjectCompletionDate"]) ? null : (DateTime?)Convert.ToDateTime(drProjectDetails["ProjectCompletionDate"], CultureInfo.InvariantCulture);
                        project.FeaturedDescription = Convert.ToString(drProjectDetails["FeaturedDescription"], CultureInfo.InvariantCulture);
                        project.TemplateId = Convert.ToInt32(drProjectDetails["ProjectTemplateId"], CultureInfo.InvariantCulture);
                    }

                    drProjectDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectDetails != null && !drProjectDetails.IsClosed)
                {
                    drProjectDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return project;
        }
    }
}