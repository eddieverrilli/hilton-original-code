﻿using System;
using System.Web;
using System.Web.Configuration;
using System.Data.SqlClient;
using Hilton.Infrastructure.Encryption;


namespace Hilton.SuppliersConnection.DataAccess
{
    public class MyPartitionResolver : IPartitionResolver
    {

        public void Initialize()
        {

        }

        public string ResolvePartition(object key)
        {
            return ConnectionStrings.GetInstance();
        }
    }


    public class ConnectionStrings
    {
        static ConnectionStrings instance = null;

        static String ConnectionString = string.Empty;

        private ConnectionStrings()
        {
            string strdecpwd, strdecUserId, strkey;

            var sessionSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            var strconnbuilder = new SqlConnectionStringBuilder(sessionSection.SqlConnectionString);

            strkey = System.Configuration.ConfigurationManager.AppSettings["EncryptionDecryptionKey"].ToString();
            strdecpwd = AESEncryption.AESDecryptText(strconnbuilder.Password, strkey);
            strdecUserId = AESEncryption.AESDecryptText(strconnbuilder.UserID, strkey);
            strconnbuilder.Password = strdecpwd;
            strconnbuilder.UserID = strdecUserId;
            ConnectionString = strconnbuilder.ConnectionString;
        }

        public static string GetInstance()
        {
            if (instance == null)
            {
                instance = new ConnectionStrings();
            }
            return ConnectionString;
        }
    }

}
