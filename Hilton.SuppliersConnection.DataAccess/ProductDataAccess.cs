﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.SqlServer.Server;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class ProductDataAccess
    {
        /// <summary>
        /// Adds a Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public static int AddProduct(Product product, IList<ProductCategory> productCategories)
        {
            int productId = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsertProducts))
                {
                    db.AddInParameter(dbCommand, "@productName", DbType.String, product.ProductName);
                    db.AddInParameter(dbCommand, "@skuNumber", DbType.String, product.ProductSku);
                    db.AddInParameter(dbCommand, "@vendor", DbType.Int32, product.PartnerId);
                    db.AddInParameter(dbCommand, "@specSheetPDFName", DbType.String, product.SpecSheetPdfName);
                    db.AddInParameter(dbCommand, "@specSheetPDF", DbType.Binary, product.SpecSheetPdf);
                    db.AddInParameter(dbCommand, "@productImageName", DbType.String, product.ImageName);
                    db.AddInParameter(dbCommand, "@productImage", DbType.Binary, product.ProductImage);
                    db.AddInParameter(dbCommand, "@productThumbnailImage", DbType.Binary, product.ProductThumbnailImage);
                    db.AddInParameter(dbCommand, "@productWebsite", DbType.String, product.Website);
                    db.AddInParameter(dbCommand, "@productDescription", DbType.String, product.Description);
                    db.AddInParameter(dbCommand, "@contactId", DbType.Int32, product.ContactId);
                    db.AddInParameter(dbCommand, "@contactFirstName", DbType.String, product.Contact.FirstName);
                    db.AddInParameter(dbCommand, "@contactLastName", DbType.String, product.Contact.LastName);
                    db.AddInParameter(dbCommand, "@contactEmailaddress", DbType.String, product.Contact.Email);
                    db.AddInParameter(dbCommand, "@contactFax", DbType.String, product.Contact.Fax);
                    db.AddInParameter(dbCommand, "@contactPhone", DbType.String, product.Contact.Phone);
                    db.AddInParameter(dbCommand, "@statusId", DbType.Int32, product.StatusId);
                    db.AddInParameter(dbCommand, "@requestedById", DbType.Int32, product.RequestedById);
                    db.AddInParameter(dbCommand, "@designReview", DbType.String, product.DesignReviewComments);
                    db.AddInParameter(dbCommand, "@designReviewBy", DbType.String, product.DesignReviewBy);
                    db.AddInParameter(dbCommand, "@techReview", DbType.String, product.TechReviewComments);
                    db.AddInParameter(dbCommand, "@techReviewBy", DbType.String, product.TechReviewBy);
                    db.AddInParameter(dbCommand, "@adminReview", DbType.String, product.AdminReviewComments);
                    db.AddInParameter(dbCommand, "@adminReviewBy", DbType.String, product.AdminReviewBy);
                    db.AddInParameter(dbCommand, "@brandStandards", DbType.String, product.BrandStandards);
                    //db.AddInParameter(dbCommand, "@categoryTable", DbType.String, CreateCategoryHtml(productCategories));
                    db.AddInParameter(dbCommand, "@userId ", DbType.Int32, product.UserId);
                    SqlParameter specParam = new SqlParameter("@productAssignedSpecification", SqlDbType.Structured);
                    specParam.TypeName = "dbo.ProductAssignedSpecificationsForCountry";
                    specParam.Value = CreateSqlDataRecords(productCategories);
                    dbCommand.Parameters.Add(specParam);

                    SqlParameter retValParam = new SqlParameter("@productId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    dbCommand.CommandTimeout = 500; // addded this bcz add all possible combination of category, brands region and country.. will take time 
                    db.ExecuteNonQuery(dbCommand);
                    productId = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return productId;
        }

        /// <summary>
        /// Adds a Product for Review
        /// </summary>
        /// <param name="product"></param>
        /// <param name="productCategories"></param>
        /// <returns></returns>
        public static int AddProductForReview(Product product, IList<ProductCategory> productCategories)
        {
            int productId = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsProductForReview))
                {
                    db.AddInParameter(dbCommand, "@productName", DbType.String, product.ProductName);
                    db.AddInParameter(dbCommand, "@skuNumber", DbType.String, product.ProductSku);
                    db.AddInParameter(dbCommand, "@vendor", DbType.Int32, product.PartnerId);
                    db.AddInParameter(dbCommand, "@specSheetPDFName", DbType.String, product.SpecSheetPdfName);
                    db.AddInParameter(dbCommand, "@specSheetPDF", DbType.Binary, product.SpecSheetPdf);
                    db.AddInParameter(dbCommand, "@productImageName", DbType.String, product.ImageName);
                    db.AddInParameter(dbCommand, "@productImage", DbType.Binary, product.ProductImage);
                    db.AddInParameter(dbCommand, "@productThumbnailImage", DbType.Binary, product.ProductThumbnailImage);
                    db.AddInParameter(dbCommand, "@productWebsite", DbType.String, product.Website);
                    db.AddInParameter(dbCommand, "@productDescription", DbType.String, product.Description);
                    db.AddInParameter(dbCommand, "@contactId", DbType.Int32, product.ContactId);
                    db.AddInParameter(dbCommand, "@contactFirstName", DbType.String, product.Contact.FirstName);
                    db.AddInParameter(dbCommand, "@contactLastName", DbType.String, product.Contact.LastName);
                    db.AddInParameter(dbCommand, "@contactTitle", DbType.String, product.Contact.Title);
                    db.AddInParameter(dbCommand, "@contactEmailaddress", DbType.String, product.Contact.Email);
                    db.AddInParameter(dbCommand, "@contactFax", DbType.String, product.Contact.Fax);
                    db.AddInParameter(dbCommand, "@contactPhone", DbType.String, product.Contact.Phone);
                    db.AddInParameter(dbCommand, "@statusId", DbType.Int32, product.StatusId);
                    db.AddInParameter(dbCommand, "@requestedById", DbType.Int32, product.RequestedById);
                    db.AddInParameter(dbCommand, "@culture", DbType.String, product.Culture);

                    // Parameter to Add for Send Mail Notification
                    db.AddInParameter(dbCommand, "@brandStandards", DbType.String, product.BrandStandards);
                    db.AddInParameter(dbCommand, "@categoryTable", DbType.String, CreateCategoryHtml(productCategories));

                    SqlParameter specParam = new SqlParameter("@productAssignedSpecification", SqlDbType.Structured);
                    specParam.TypeName = "dbo.ProductAssignedSpecificationsForCountry";
                    specParam.Value = CreateSqlDataRecords(productCategories);
                    dbCommand.Parameters.Add(specParam);

                    SqlParameter retValParam = new SqlParameter("@productId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    productId = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return productId;
        }

        /// <summary>
        /// Gets the Products Submitted by a Partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static IList<ProductCategory> GetApprovedPartnerProducts(SearchCriteria searchCriteria)
        {
            ProductCategory productCategory;
            Product product;
            ProductAssignedSpecifications productAssignedSpecifications;
            //ProductPropertyType productPropertyType;
            IList<Product> productCollection;
            IList<ProductCategory> productCategoryCollection = new List<ProductCategory>();
            List<ProductAssignedSpecifications> productAssignedSpecificationsCollection;
            //IList<ProductPropertyType> productPropertyTypeCollection;
            IDataReader drproductCategory;
            Contact contact;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetApprovedProduct);
                db.AddInParameter(dbCommand, "@brandId", DbType.String, (String.IsNullOrEmpty(searchCriteria.BrandId)) ? (object)DBNull.Value : searchCriteria.BrandId);
                //db.AddInParameter(dbCommand, "@propertyTypeId", DbType.String, (String.IsNullOrEmpty(searchCriteria.PropertyTypeId)) ? (object)DBNull.Value : searchCriteria.PropertyTypeId);
                db.AddInParameter(dbCommand, "@regionId", DbType.String, (String.IsNullOrEmpty(searchCriteria.RegionId)) ? (object)DBNull.Value : searchCriteria.RegionId);
                db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, searchCriteria.PartnerId);
                db.AddInParameter(dbCommand, "@countryId", DbType.String, (String.IsNullOrEmpty(searchCriteria.CountryId)) ? (object)DBNull.Value : searchCriteria.CountryId);

                drproductCategory = (IDataReader)db.ExecuteReader(dbCommand);

                while (drproductCategory.Read())
                {
                    var productCategoryColl = productCategoryCollection.Where(x => x.CategoryId == Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture));
                    if (productCategoryColl.Count() == 0)
                    {
                        productCategory = new ProductCategory();
                        productCategory.CategoryId = Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture);
                        productCategory.CategoryName = Convert.ToString(drproductCategory["CatDisplayName"], CultureInfo.InvariantCulture);

                        productAssignedSpecifications = new ProductAssignedSpecifications(); ;
                        productAssignedSpecifications.BrandId = Convert.ToInt32(drproductCategory["BrandId"], CultureInfo.InvariantCulture);
                        productAssignedSpecifications.BrandDescription = Convert.ToString(drproductCategory["BrandName"], CultureInfo.InvariantCulture);
                        productAssignedSpecifications.RegionId = Convert.ToInt32(drproductCategory["RegionId"], CultureInfo.InvariantCulture);
                        productAssignedSpecifications.RegionDescription = Convert.ToString(drproductCategory["RegionName"], CultureInfo.InvariantCulture);
                        productAssignedSpecifications.BrandImageUrl = "../Images/" + System.Web.HttpUtility.UrlDecode(Convert.ToString(drproductCategory["BrandImageName"], CultureInfo.InvariantCulture).Trim()) + ".png";



                        //productPropertyType = new ProductPropertyType();
                        //productPropertyType.CategoryId = Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture);
                        //productPropertyType.PropertyTypeId = Convert.ToInt32(drproductCategory["PropertyTypeId"]);
                        //productPropertyType.PropertyTypeDescription = Convert.ToString(drproductCategory["PropertyTypeName"], CultureInfo.InvariantCulture);

                        //productPropertyTypeCollection = new List<ProductPropertyType>();
                        //productPropertyTypeCollection.Add(productPropertyType);

                        //productAssignedSpecifications.PropertyTypes = productPropertyTypeCollection;

                        if (productAssignedSpecifications.Countries == null)
                        {
                            productAssignedSpecifications.Countries = new List<Country>();
                        }
                        productAssignedSpecifications.Countries.Add(new Country { CountryId = Convert.ToInt32(drproductCategory["CountryId"], CultureInfo.InvariantCulture), CountryName = Convert.ToString(drproductCategory["CountryName"], CultureInfo.InvariantCulture) });

                        product = new Product();
                        product.ProductId = Convert.ToInt32(drproductCategory["ProductId"], CultureInfo.InvariantCulture);
                        product.ProductName = Convert.ToString(drproductCategory["ProductName"], CultureInfo.InvariantCulture);
                        product.ProductSku = Convert.ToString(drproductCategory["SKUNumber"], CultureInfo.InvariantCulture);
                        //product.ProductImage = drproductCategory["ProductImage"].Equals(DBNull.Value) ? null : (byte[])drproductCategory["ProductImage"];
                        product.ImageName = Convert.ToString(drproductCategory["ProductImageName"], CultureInfo.InvariantCulture);
                        product.ProductThumbnailImage = drproductCategory["ProductThumbnailImage"].Equals(DBNull.Value) ? null : (byte[])drproductCategory["ProductThumbnailImage"];
                        product.PartnerName = Convert.ToString(drproductCategory["CompanyName"], CultureInfo.InvariantCulture);
                        product.StatusDescription = Convert.ToString(drproductCategory["ProductStatusDesc"], CultureInfo.InvariantCulture);
                        product.SpecSheetPdfName = Convert.ToString(drproductCategory["SpecSheetPDFName"], CultureInfo.InvariantCulture);

                        contact = new Contact();
                        contact.Email = Convert.ToString(drproductCategory["Email"], CultureInfo.InvariantCulture);

                        product.Contact = contact;

                        productCollection = new List<Product>();
                        productCollection.Add(product);

                        productAssignedSpecifications.Product = productCollection;

                        productAssignedSpecificationsCollection = new List<ProductAssignedSpecifications>();
                        productAssignedSpecificationsCollection.Add(productAssignedSpecifications);

                        productCategory.ProductAssignedSpecifications = productAssignedSpecificationsCollection;

                        productCategoryCollection.Add(productCategory);
                    }
                    else
                    {
                        var productCategoriesCollection = productCategoryCollection.Where(x => x.CategoryId == Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture)).ToList();
                        if (productCategoriesCollection.Count > 0)
                        {

                            productCategoriesCollection.ForEach(x =>
                            {

                                var productCategoryDetailCollection = x.ProductAssignedSpecifications.Where(z => z.BrandId == Convert.ToInt32(drproductCategory["BrandId"], CultureInfo.InvariantCulture) && z.RegionId == Convert.ToInt32(drproductCategory["RegionId"], CultureInfo.InvariantCulture));
                                if (productCategoryDetailCollection.Count() == 0)
                                {
                                    productAssignedSpecifications = new ProductAssignedSpecifications();
                                    productAssignedSpecifications.BrandId = Convert.ToInt32(drproductCategory["BrandId"], CultureInfo.InvariantCulture);
                                    productAssignedSpecifications.BrandDescription = Convert.ToString(drproductCategory["BrandName"], CultureInfo.InvariantCulture);
                                    productAssignedSpecifications.RegionId = Convert.ToInt32(drproductCategory["RegionId"], CultureInfo.InvariantCulture);
                                    productAssignedSpecifications.RegionDescription = Convert.ToString(drproductCategory["RegionName"], CultureInfo.InvariantCulture);
                                    productAssignedSpecifications.BrandImageUrl = "../Images/" + System.Web.HttpUtility.UrlDecode(Convert.ToString(drproductCategory["BrandImageName"], CultureInfo.InvariantCulture).Trim()) + ".png";
                                    // productAssignedSpecifications.CountryId = Convert.ToInt32(drproductCategory["CountryId"], CultureInfo.InvariantCulture);
                                    // productAssignedSpecifications.CountryDescription = Convert.ToString(drproductCategory["CountryName"], CultureInfo.InvariantCulture);

                                    //productPropertyType = new ProductPropertyType();
                                    //productPropertyType.CategoryId = Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture);
                                    //productPropertyType.PropertyTypeDescription = Convert.ToString(drproductCategory["PropertyTypeName"], CultureInfo.InvariantCulture);

                                    // productPropertyTypeCollection = new List<ProductPropertyType>();
                                    //productPropertyTypeCollection.Add(productPropertyType);




                                    //productAssignedSpecifications.PropertyTypes = productPropertyTypeCollection;

                                    if (productAssignedSpecifications.Countries == null)
                                    {
                                        productAssignedSpecifications.Countries = new List<Country>();
                                    }
                                    productAssignedSpecifications.Countries.Add(new Country { CountryId = Convert.ToInt32(drproductCategory["CountryId"], CultureInfo.InvariantCulture), CountryName = Convert.ToString(drproductCategory["CountryName"], CultureInfo.InvariantCulture) });


                                    product = new Product();
                                    product.ProductId = Convert.ToInt32(drproductCategory["ProductId"], CultureInfo.InvariantCulture);
                                    product.ProductName = Convert.ToString(drproductCategory["ProductName"], CultureInfo.InvariantCulture);
                                    product.ProductSku = Convert.ToString(drproductCategory["SKUNumber"], CultureInfo.InvariantCulture);
                                    product.ImageName = Convert.ToString(drproductCategory["ProductImageName"], CultureInfo.InvariantCulture);
                                    product.ProductThumbnailImage = drproductCategory["ProductThumbnailImage"].Equals(DBNull.Value) ? null : (byte[])drproductCategory["ProductThumbnailImage"];
                                    product.PartnerName = Convert.ToString(drproductCategory["CompanyName"], CultureInfo.InvariantCulture);
                                    product.StatusDescription = Convert.ToString(drproductCategory["ProductStatusDesc"], CultureInfo.InvariantCulture);
                                    product.SpecSheetPdfName = Convert.ToString(drproductCategory["SpecSheetPDFName"], CultureInfo.InvariantCulture);

                                    contact = new Contact();
                                    contact.Email = Convert.ToString(drproductCategory["Email"], CultureInfo.InvariantCulture);

                                    product.Contact = contact;

                                    productCollection = new List<Product>();

                                    productCollection.Add(product);

                                    productAssignedSpecifications.Product = productCollection;

                                    x.ProductAssignedSpecifications.Add(productAssignedSpecifications);
                                }
                                else
                                {
                                    productCategoryDetailCollection.ToList().ForEach(y =>
                                    {
                                        if (y.BrandId == Convert.ToInt32(drproductCategory["BrandId"], CultureInfo.InvariantCulture) && x.CategoryId == Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture) &&
                                            y.Countries.FirstOrDefault(country => country.CountryId == Convert.ToInt32(drproductCategory["CountryId"], CultureInfo.InvariantCulture)) == null)
                                        {
                                            y.Countries.Add(new Country { CountryId = Convert.ToInt32(drproductCategory["CountryId"], CultureInfo.InvariantCulture), CountryName = Convert.ToString(drproductCategory["CountryName"], CultureInfo.InvariantCulture) });
                                        }
                                        //var propertyTypes = y.PropertyTypes.Where(z => z.PropertyTypeDescription == Convert.ToString(drproductCategory["PropertyTypeName"], CultureInfo.InvariantCulture));

                                        //if (propertyTypes.Count() > 0)
                                        //{
                                        if (productCategoryDetailCollection.First().Product.Where(i => i.ProductId.Equals(Convert.ToInt32(drproductCategory["ProductId"], CultureInfo.InvariantCulture))).Count() == 0)
                                        {
                                            Product product1 = new Product();
                                            product1.ProductId = Convert.ToInt32(drproductCategory["ProductId"], CultureInfo.InvariantCulture);
                                            product1.ProductName = Convert.ToString(drproductCategory["ProductName"], CultureInfo.InvariantCulture);
                                            product1.ProductSku = Convert.ToString(drproductCategory["SKUNumber"], CultureInfo.InvariantCulture);
                                            product1.ImageName = Convert.ToString(drproductCategory["ProductImageName"], CultureInfo.InvariantCulture);
                                            product1.ProductThumbnailImage = drproductCategory["ProductThumbnailImage"].Equals(DBNull.Value) ? null : (byte[])drproductCategory["ProductThumbnailImage"];
                                            product1.PartnerName = Convert.ToString(drproductCategory["CompanyName"], CultureInfo.InvariantCulture);
                                            product1.StatusDescription = Convert.ToString(drproductCategory["ProductStatusDesc"], CultureInfo.InvariantCulture);
                                            product1.SpecSheetPdfName = Convert.ToString(drproductCategory["SpecSheetPDFName"], CultureInfo.InvariantCulture);

                                            contact = new Contact();
                                            contact.Email = Convert.ToString(drproductCategory["Email"], CultureInfo.InvariantCulture);

                                            product1.Contact = contact;

                                            productCategoryDetailCollection.First().Product.Add(product1);
                                        }
                                        //}
                                        //else
                                        //{
                                        //    ProductPropertyType productPropertyType1 = new ProductPropertyType();
                                        //    productPropertyType1.CategoryId = Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture);
                                        //    productPropertyType1.PropertyTypeDescription = Convert.ToString(drproductCategory["PropertyTypeName"], CultureInfo.InvariantCulture);

                                        //    productCategoryDetailCollection.First().PropertyTypes.Add(productPropertyType1);

                                        //    if (productCategoryDetailCollection.First().Product.Where(i => i.ProductId.Equals(Convert.ToInt32(drproductCategory["ProductId"], CultureInfo.InvariantCulture))).Count() == 0)
                                        //    {
                                        //        Product product1 = new Product();
                                        //        product1.ProductId = Convert.ToInt32(drproductCategory["ProductId"], CultureInfo.InvariantCulture);
                                        //        product1.ProductName = Convert.ToString(drproductCategory["ProductName"], CultureInfo.InvariantCulture);
                                        //        product1.ProductSku = Convert.ToString(drproductCategory["SKUNumber"], CultureInfo.InvariantCulture);
                                        //        product1.ImageName = Convert.ToString(drproductCategory["ProductImageName"], CultureInfo.InvariantCulture);
                                        //        product1.ProductThumbnailImage = drproductCategory["ProductThumbnailImage"].Equals(DBNull.Value) ? null : (byte[])drproductCategory["ProductThumbnailImage"];
                                        //        product1.PartnerName = Convert.ToString(drproductCategory["CompanyName"], CultureInfo.InvariantCulture);
                                        //        product1.StatusDescription = Convert.ToString(drproductCategory["ProductStatusDesc"], CultureInfo.InvariantCulture);
                                        //        product1.SpecSheetPdfName = Convert.ToString(drproductCategory["SpecSheetPDFName"], CultureInfo.InvariantCulture);

                                        //        contact = new Contact();
                                        //        contact.Email = Convert.ToString(drproductCategory["Email"], CultureInfo.InvariantCulture);

                                        //        product1.Contact = contact;

                                        //        productCategoryDetailCollection.First().Product.Add(product1);
                                        //    }
                                        //}
                                    });
                                }
                            });
                        }
                    }
                }
            }

            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return productCategoryCollection;
        }

        /// <summary>
        /// To Get Categories
        /// </summary>
        /// <returns></returns>
        public static IList<Category> GetCategories()
        {
            IList<Category> subCategories = new List<Category>();
            IDataReader drCategories = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetCategoryList))
                {
                    drCategories = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCategories.Read())
                    {
                        subCategories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drCategories["CatId"], CultureInfo.InvariantCulture),
                            CategoryName = Convert.ToString(drCategories["CatDesc"], CultureInfo.InvariantCulture),
                            ParentCategoryId = drCategories["ParentCatId"].Equals(DBNull.Value) ? -1 : Convert.ToInt32(drCategories["ParentCatId"], CultureInfo.InvariantCulture),
                            RootId = Convert.ToInt32(drCategories["RootParentCatId"], CultureInfo.InvariantCulture)
                        }
                                             );
                    }

                    drCategories.Close();
                }
            }
            catch (Exception exception)
            {
                if (drCategories != null && !drCategories.IsClosed)
                {
                    drCategories.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return subCategories;
        }

        /// <summary>
        /// GetsCountries For Region
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public static IList<DropDownItem> GetCountriesForRegion(int regionId)
        {
            IDataReader drCountries;

            IList<DropDownItem> countries = new List<DropDownItem>();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetCountriesForRegion))
                {
                    db.AddInParameter(dbCommand, "@regionId", DbType.Int32, regionId);

                    drCountries = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCountries.Read())
                    {
                        countries.Add(new DropDownItem()

                        {
                            DataTextField = Convert.ToString(drCountries["CountryName"], CultureInfo.InvariantCulture),

                            DataValueField = Convert.ToString(drCountries["CountryId"], CultureInfo.InvariantCulture)
                        });
                    }
                }
            }

            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);

                if (rethrow)
                {
                    throw genericException;
                }
            }

            return countries;
        }


        public static IList<Country> GetCountriesForRegionAndProduct(string countryarr)
        {
            IDataReader drCountries;

            IList<Country> countries = new List<Country>();

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);


                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetCountriesForRegionAndProduct))
                {
                    db.AddInParameter(dbCommand, "@countries", DbType.String, countryarr);

                    drCountries = (IDataReader)db.ExecuteReader(dbCommand);
                    while (drCountries.Read())
                    {
                        countries.Add(new Country()

                        {
                            RegionId = Convert.ToInt16(drCountries["AffectedRegionId"], CultureInfo.InvariantCulture),
                            RegionName = Convert.ToString(drCountries["RegionName"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drCountries["CountryName"], CultureInfo.InvariantCulture),

                            CountryId = Convert.ToInt16(drCountries["CountryId"], CultureInfo.InvariantCulture)
                        });
                    }

                }
            }

            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);

                if (rethrow)
                {
                    throw genericException;
                }
            }

            return countries;
        }

        /// <summary>
        /// Gets Product Assigned Specifications
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static IList<ProductCategory> GetProductAssignedSpecifications(int productId)
        {
            ProductCategory productCategory;
            ProductAssignedSpecifications productAssignedSpecifications;            
            IList<ProductCategory> productCategoryCollection = new List<ProductCategory>();
            List<ProductAssignedSpecifications> productAssignedSpecificationsCollection;            
            IDataReader drproductCategory = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductAssignedSpecifications))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, productId);

                    drproductCategory = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drproductCategory.Read())
                    {
                        var productCategoryColl = productCategoryCollection.Where(x => x.CategoryId == Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture));
                        if (productCategoryColl.Count() == 0)
                        {
                            productCategory = new ProductCategory();
                            productCategory.CategoryId = Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture);
                            productCategory.CategoryName = Convert.ToString(drproductCategory["CatDisplayName"], CultureInfo.InvariantCulture);

                            productAssignedSpecifications = new ProductAssignedSpecifications();
                            productAssignedSpecifications.BrandId = Convert.ToInt32(drproductCategory["BrandId"], CultureInfo.InvariantCulture);
                            productAssignedSpecifications.BrandDescription = Convert.ToString(drproductCategory["BrandName"], CultureInfo.InvariantCulture);
                            productAssignedSpecifications.RegionId = Convert.ToInt32(drproductCategory["RegionId"], CultureInfo.InvariantCulture);
                            productAssignedSpecifications.RegionDescription = Convert.ToString(drproductCategory["RegionName"], CultureInfo.InvariantCulture);
                            productAssignedSpecifications.CountryId = Convert.ToInt32(drproductCategory["CountryId"], CultureInfo.InvariantCulture);
                            productAssignedSpecifications.CountryDescription = Convert.ToString(drproductCategory["CountryName"], CultureInfo.InvariantCulture);
                            productAssignedSpecifications.CategoryId = Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture);                           

                            productAssignedSpecificationsCollection = new List<ProductAssignedSpecifications>();
                            productAssignedSpecificationsCollection.Add(productAssignedSpecifications);

                            productCategory.ProductAssignedSpecifications = productAssignedSpecificationsCollection;
                            productCategoryCollection.Add(productCategory);
                        }
                        else
                        {
                            var productCategoriesCollection = productCategoryCollection.Where(x => x.CategoryId == Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture)).ToList();
                            if (productCategoriesCollection.Count > 0)
                            {
                                productCategoriesCollection.ForEach(x =>
                                {
                                    var productCategoryDetailCollection = x.ProductAssignedSpecifications.Where(z => z.BrandId == Convert.ToInt32(drproductCategory["BrandId"], CultureInfo.InvariantCulture) && z.RegionId == Convert.ToInt32(drproductCategory["RegionId"], CultureInfo.InvariantCulture) && z.CountryId == Convert.ToInt32(drproductCategory["CountryId"], CultureInfo.InvariantCulture));
                                    if (productCategoryDetailCollection.Count() == 0)
                                    {
                                        productAssignedSpecifications = new ProductAssignedSpecifications();
                                        productAssignedSpecifications.BrandId = Convert.ToInt32(drproductCategory["BrandId"], CultureInfo.InvariantCulture);
                                        productAssignedSpecifications.BrandDescription = Convert.ToString(drproductCategory["BrandName"], CultureInfo.InvariantCulture);
                                        productAssignedSpecifications.RegionId = Convert.ToInt32(drproductCategory["RegionId"], CultureInfo.InvariantCulture);
                                        productAssignedSpecifications.RegionDescription = Convert.ToString(drproductCategory["RegionName"], CultureInfo.InvariantCulture);
                                        productAssignedSpecifications.CountryId = Convert.ToInt32(drproductCategory["CountryId"], CultureInfo.InvariantCulture);
                                        productAssignedSpecifications.CountryDescription = Convert.ToString(drproductCategory["CountryName"], CultureInfo.InvariantCulture);
                                        productAssignedSpecifications.CategoryId = Convert.ToInt32(drproductCategory["CatId"], CultureInfo.InvariantCulture);                                      

                                        x.ProductAssignedSpecifications.Add(productAssignedSpecifications);
                                    }                                   
                                });
                            }
                        }
                    }

                    drproductCategory.Close();
                }
            }
            catch (Exception exception)
            {
                if (drproductCategory != null && !drproductCategory.IsClosed)
                {
                    drproductCategory.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return productCategoryCollection;
        }

        /// <summary>
        /// Ges tProduct Details
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static Product GetProductDetails(int productId)
        {
            Product product = new Product();
            IDataReader drProduct = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductDetails))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, productId);

                    drProduct = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProduct.Read())
                    {
                        product.ProductId = Convert.ToInt32(drProduct["ProductId"], CultureInfo.InvariantCulture);
                        product.ProductName = Convert.ToString(drProduct["ProductName"], CultureInfo.InvariantCulture);
                        product.ProductSku = Convert.ToString(drProduct["SKUNumber"], CultureInfo.InvariantCulture);
                        product.ImageName = Convert.ToString(drProduct["ProductImageName"], CultureInfo.InvariantCulture);
                        product.ProductThumbnailImage = drProduct["ProductThumbnailImage"].Equals(DBNull.Value) ? null : (byte[])drProduct["ProductThumbnailImage"];
                        product.ProductImage = drProduct["ProductImage"].Equals(DBNull.Value) ? null : (byte[])drProduct["ProductImage"];
                        product.SpecSheetPdf = drProduct["SpecSheetPDF"].Equals(DBNull.Value) ? null : (byte[])(drProduct["SpecSheetPDF"]);
                        product.SpecSheetPdfName = Convert.ToString(drProduct["SpecSheetPDFName"], CultureInfo.InvariantCulture);
                        product.Description = Convert.ToString(drProduct["ProductDescription"], CultureInfo.InvariantCulture);
                        product.Website = Convert.ToString(drProduct["ProductWebsite"], CultureInfo.InvariantCulture);
                        product.PartnerId = Convert.ToInt32(drProduct["Vendor"], CultureInfo.InvariantCulture);
                        product.ContactId = Convert.ToInt32(drProduct["ContactId"], CultureInfo.InvariantCulture);
                        product.Contact = new Contact()
                        {
                            FirstName = Convert.ToString(drProduct["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drProduct["LastName"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drProduct["Email"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drProduct["Phone"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drProduct["Fax"], CultureInfo.InvariantCulture)
                        };
                        product.StatusId = Convert.ToInt32(drProduct["StatusId"], CultureInfo.InvariantCulture);
                        product.RequestedDate = Convert.ToString(drProduct["RequestDate"], CultureInfo.InvariantCulture);
                        product.RequestedBy = Convert.ToString(drProduct["RequestedBy"], CultureInfo.InvariantCulture);
                        product.RequestedById = Convert.ToInt16(drProduct["RequestedById"], CultureInfo.InvariantCulture);
                        product.PartnerName = Convert.ToString(drProduct["PartnerName"], CultureInfo.InvariantCulture);
                        product.AdminReviewBy = Convert.ToString(drProduct["AdminReviewBy"], CultureInfo.InvariantCulture);
                        product.AdminReviewComments = Convert.ToString(drProduct["AdminReview"], CultureInfo.InvariantCulture);
                        product.DesignReviewComments = Convert.ToString(drProduct["DesignReview"], CultureInfo.InvariantCulture);
                        product.DesignReviewBy = Convert.ToString(drProduct["DesignReviewBy"], CultureInfo.InvariantCulture);
                        product.TechReviewComments = Convert.ToString(drProduct["TechnicalReview"], CultureInfo.InvariantCulture);
                        product.TechReviewBy = Convert.ToString(drProduct["TechnicalReviewBy"], CultureInfo.InvariantCulture);
                    }

                    drProduct.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProduct != null && !drProduct.IsClosed)
                {
                    drProduct.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }

            return product;
        }

        /// <summary>
        /// Gets Product Details For Review
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static Product GetProductDetailsForReview(int productId)
        {
            Product product = null;
            IDataReader drProduct;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductDetailsForReview))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, productId);

                    drProduct = (IDataReader)db.ExecuteReader(dbCommand);
                    product = new Product();
                    while (drProduct.Read())
                    {
                        product.ProductId = Convert.ToInt32(drProduct["ProductId"], CultureInfo.InvariantCulture);
                        product.ProductName = Convert.ToString(drProduct["ProductName"], CultureInfo.InvariantCulture);
                        product.ProductSku = Convert.ToString(drProduct["SKUNumber"], CultureInfo.InvariantCulture);
                        product.ImageName = Convert.ToString(drProduct["ProductImageName"], CultureInfo.InvariantCulture);
                        product.ProductThumbnailImage = drProduct["ProductThumbnailImage"].Equals(DBNull.Value) ? null : (byte[])drProduct["ProductThumbnailImage"];
                        product.SpecSheetPdf = drProduct["SpecSheetPDF"].Equals(DBNull.Value) ? null : (byte[])(drProduct["SpecSheetPDF"]);
                        product.SpecSheetPdfName = Convert.ToString(drProduct["SpecSheetPDFName"], CultureInfo.InvariantCulture);
                        product.Description = Convert.ToString(drProduct["ProductDescription"], CultureInfo.InvariantCulture);
                        product.Website = Convert.ToString(drProduct["ProductWebsite"], CultureInfo.InvariantCulture);
                        product.PartnerId = Convert.ToInt32(drProduct["Vendor"], CultureInfo.InvariantCulture);
                        product.ContactId = Convert.ToInt32(drProduct["ContactId"], CultureInfo.InvariantCulture);
                        product.Contact = new Contact()
                        {
                            FirstName = Convert.ToString(drProduct["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drProduct["LastName"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drProduct["Title"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drProduct["Email"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drProduct["Phone"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drProduct["Fax"], CultureInfo.InvariantCulture)
                        };
                        product.StatusId = Convert.ToInt32(drProduct["StatusId"], CultureInfo.InvariantCulture);
                        product.RequestedDate = Convert.ToString(drProduct["RequestDate"], CultureInfo.InvariantCulture);
                        product.RequestedBy = Convert.ToString(drProduct["RequestedBy"], CultureInfo.InvariantCulture);
                        product.PartnerName = Convert.ToString(drProduct["PartnerName"], CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return product;
        }

        /// <summary>
        /// Gets Product Details For Review
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static Product GetProductImage(int productId)
        {
            Product product = null;
            IDataReader drProduct;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductImage))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, productId);

                    drProduct = (IDataReader)db.ExecuteReader(dbCommand);
                    product = new Product();
                    while (drProduct.Read())
                    {
                        product.ImageName = Convert.ToString(drProduct["ProductImageName"], CultureInfo.InvariantCulture);
                        product.ProductImage = drProduct["ProductImage"].Equals(DBNull.Value) ? null : (byte[])drProduct["ProductImage"];
                    }
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return product;
        }

        /// <summary>
        /// Gets Product List
        /// </summary>
        /// <param name="productSearch"></param>
        /// <returns></returns>
        public static IList<Product> GetProductList(ProductSearch productSearch, ref int totalRecordCount)
        {
            IList<Product> productCollection = new List<Product>();
            IDataReader drProductDetails = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductList))
                {
                    db.AddInParameter(dbCommand, "userId", DbType.Int32, productSearch.UserId);
                    if (String.IsNullOrEmpty(productSearch.PartnerId))
                        db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, productSearch.PartnerId);

                    if (String.IsNullOrEmpty(productSearch.RegionId))
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, productSearch.RegionId);

                    if (String.IsNullOrEmpty(productSearch.CountryId))
                        db.AddInParameter(dbCommand, "@countryId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@countryId", DbType.Int32, productSearch.CountryId);

                    if (String.IsNullOrEmpty(productSearch.BrandId))
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, productSearch.BrandId);

                    if (String.IsNullOrEmpty(productSearch.PropertyTypeId))
                        db.AddInParameter(dbCommand, "@propertyTypeId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@propertyTypeId", DbType.Int32, productSearch.PropertyTypeId);

                    if (String.IsNullOrEmpty(productSearch.SubCategoryId))
                    {
                        if (String.IsNullOrEmpty(productSearch.CategoryId))
                            db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, DBNull.Value);
                        else
                            db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, productSearch.CategoryId);
                    }
                    else
                        db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, productSearch.SubCategoryId);

                    if (String.IsNullOrEmpty(productSearch.StatusId))
                        db.AddInParameter(dbCommand, "@statusId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@statusId", DbType.Int32, productSearch.StatusId);

                    db.AddInParameter(dbCommand, "@searchExpression", DbType.String, productSearch.QuickSearchExpression);
                    db.AddInParameter(dbCommand, "@searchType", DbType.String, productSearch.SearchFlag);
                    db.AddInParameter(dbCommand, "@sortExpression", DbType.String, productSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int32, productSearch.PageSize);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, productSearch.SortDirection);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, productSearch.PageIndex);

                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drProductDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProductDetails.Read())
                    {
                        {
                            productCollection.Add(new Product()
                            {
                                ProductId = Convert.ToInt32(drProductDetails["ProductId"], CultureInfo.InvariantCulture),

                                ModalName = Convert.ToString(drProductDetails["SKUNumber"], CultureInfo.InvariantCulture),

                                ProductName = Convert.ToString(drProductDetails["ProductName"], CultureInfo.InvariantCulture),

                                PartnerName = Convert.ToString(drProductDetails["CompanyName"], CultureInfo.InvariantCulture),

                                StatusDescription = Convert.ToString(drProductDetails["ProductStatusDesc"], CultureInfo.InvariantCulture),
                            }
                            );
                        }
                    }
                    drProductDetails.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);

                    drProductDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProductDetails != null && !drProductDetails.IsClosed)
                {
                    drProductDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return productCollection;
        }

        /// <summary>
        /// To Get Product Search Filter Data
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetProductSearchFilterData(int userId)
        {
            IDataReader drSearchFilter = null;

            IList<SearchFilters> searchFilters = new List<SearchFilters>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetProductSearchFiltersData))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drSearchFilter.Read())
                    {
                        searchFilters.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            PropertyTypeId = Convert.ToInt32(drSearchFilter["PropertyTypeId"], CultureInfo.InvariantCulture),
                            PropertyTypeDescription = Convert.ToString(drSearchFilter["PropertyTypeName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drSearchFilter["CountryName"], CultureInfo.InvariantCulture)
                        }
                     );
                    }

                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        /// Gets Search Filter Data
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetSearchFilterData(int partnerId)
        {
            IDataReader drSearchFilter = null;

            IList<SearchFilters> searchFilters = new List<SearchFilters>();
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetSearchFiltersData))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);

                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drSearchFilter.Read())
                    {
                        searchFilters.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            //PropertyTypeId = Convert.ToInt32(drSearchFilter["PropertyTypeId"], CultureInfo.InvariantCulture),
                            //PropertyTypeDescription = Convert.ToString(drSearchFilter["PropertyTypeName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drSearchFilter["CountryName"], CultureInfo.InvariantCulture)
                        }

                                          );
                    }

                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        /// Updates Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public static int UpdateProduct(Product product, IList<ProductCategory> productCategoryCollection)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateProductDetails))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, product.ProductId);
                    db.AddInParameter(dbCommand, "@productName", DbType.String, product.ProductName);
                    db.AddInParameter(dbCommand, "@skuNumber", DbType.String, product.ProductSku);
                    db.AddInParameter(dbCommand, "@vendor", DbType.Int32, product.PartnerId);
                    db.AddInParameter(dbCommand, "@specSheetPDFName", DbType.String, product.SpecSheetPdfName);
                    db.AddInParameter(dbCommand, "@specSheetPDF", DbType.Binary, product.SpecSheetPdf);
                    db.AddInParameter(dbCommand, "@productImageName", DbType.String, product.ImageName);
                    db.AddInParameter(dbCommand, "@productImage", DbType.Binary, product.ProductImage);
                    db.AddInParameter(dbCommand, "@productThumbnailImage", DbType.Binary, product.ProductThumbnailImage);
                    db.AddInParameter(dbCommand, "@productWebsite", DbType.String, product.Website);
                    db.AddInParameter(dbCommand, "@productDescription", DbType.String, product.Description);
                    db.AddInParameter(dbCommand, "@contactId", DbType.Int32, product.ContactId);
                    db.AddInParameter(dbCommand, "@contactFirstName", DbType.String, product.Contact.FirstName);
                    db.AddInParameter(dbCommand, "@contactLastName", DbType.String, product.Contact.LastName);
                    db.AddInParameter(dbCommand, "@contactEmailaddress", DbType.String, product.Contact.Email);
                    db.AddInParameter(dbCommand, "@contactFax", DbType.String, product.Contact.Fax);
                    db.AddInParameter(dbCommand, "@contactPhone", DbType.String, product.Contact.Phone);
                    db.AddInParameter(dbCommand, "@statusId", DbType.Int32, product.StatusId);
                    db.AddInParameter(dbCommand, "@designReview", DbType.String, product.DesignReviewComments);
                    db.AddInParameter(dbCommand, "@designReviewBy", DbType.String, product.DesignReviewBy);
                    db.AddInParameter(dbCommand, "@techReview", DbType.String, product.TechReviewComments);
                    db.AddInParameter(dbCommand, "@techReviewBy", DbType.String, product.TechReviewBy);
                    db.AddInParameter(dbCommand, "@adminReview", DbType.String, product.AdminReviewComments);
                    db.AddInParameter(dbCommand, "@adminReviewBy", DbType.String, product.AdminReviewBy);
                    db.AddInParameter(dbCommand, "@brandStandards", DbType.String, product.BrandStandards);
                    db.AddInParameter(dbCommand, "@categoryTable", DbType.String, CreateCategoryHtml(productCategoryCollection));
                    db.AddInParameter(dbCommand, "@culture", DbType.String, product.Culture);
                    db.AddInParameter(dbCommand, "@userId ", DbType.Int32, product.UserId);

                    SqlParameter specParam = new SqlParameter("@productAssignedSpecification", SqlDbType.Structured);
                    specParam.TypeName = "dbo.ProductAssignedSpecificationsForCountry";
                    specParam.Value = CreateSqlDataRecords(productCategoryCollection);
                    dbCommand.Parameters.Add(specParam);

                    SqlParameter retValParam = new SqlParameter("@resultId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    dbCommand.CommandTimeout = 500; // addded this bcz add all possible combination of category, brands region and country.. will take time 
                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return result;
        }

        /// <summary>
        /// Updates Product For Review
        /// </summary>
        /// <param name="product"></param>
        /// <param name="productCategoryCollection"></param>
        /// <returns></returns>
        public static int UpdateProductForReview(Product product)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateProductForReview))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, product.ProductId);
                    db.AddInParameter(dbCommand, "@productName", DbType.String, product.ProductName);
                    db.AddInParameter(dbCommand, "@contactFirstName", DbType.String, product.Contact.FirstName);
                    db.AddInParameter(dbCommand, "@contactLastName", DbType.String, product.Contact.LastName);
                    db.AddInParameter(dbCommand, "@contactTitle", DbType.String, product.Contact.Title);
                    db.AddInParameter(dbCommand, "@contactEmailaddress", DbType.String, product.Contact.Email);
                    db.AddInParameter(dbCommand, "@contactFax", DbType.String, product.Contact.Fax);
                    db.AddInParameter(dbCommand, "@contactPhone", DbType.String, product.Contact.Phone);

                    SqlParameter retValParam = new SqlParameter("@resultId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return result;
        }

        /// <summary>
        /// Removes Product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static int RemoveProduct(int productId)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.RemoveProduct))
                {
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, productId);

                    SqlParameter retValParam = new SqlParameter("@resultId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return result;
        }


        /// <summary>
        /// Creates Category HTML
        /// </summary>
        /// <param name="productCategories"></param>
        /// <returns></returns>
        private static string CreateCategoryHtml(IList<ProductCategory> productCategories)
        {
            StringBuilder categoryStringBuilder = new StringBuilder();
            categoryStringBuilder.Append("<table border = \"0\" cellpadding = \"0\" cellspacing = \"0\" width = \"100%\">");
            categoryStringBuilder.Append("<tr>");
            categoryStringBuilder.Append("<td width=\"30%\">&nbsp;</td>");
            categoryStringBuilder.Append("<td width=\"70%\">&nbsp;</td>");
            categoryStringBuilder.Append("</tr>");
            foreach (ProductCategory productCategory in productCategories)
            {
                categoryStringBuilder.Append("<tr>");
                categoryStringBuilder.Append("<td align =\"left\"><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#7d706c; text-transform:uppercase\">Category</span></td>");
                categoryStringBuilder.Append("<td><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#a39c9a;\">" + productCategory.CategoryName + "</span></td>");
                categoryStringBuilder.Append("</tr>");
                categoryStringBuilder.Append("<tr>");
                categoryStringBuilder.Append("<td height=\"8\" colspan=\"2\"></td></tr>");
                foreach (ProductAssignedSpecifications pas in productCategory.ProductAssignedSpecifications)
                {
                    //string propTypes = string.Empty;
                    //foreach (ProductPropertyType propType in pas.PropertyTypes)
                    //{
                    //    propTypes = propTypes + propType.PropertyTypeDescription + ",";
                    //}
                    //int lastCommaIndex = propTypes.LastIndexOf(",");
                    //propTypes = propTypes.Substring(0, lastCommaIndex);
                    // Adding Brands
                    categoryStringBuilder.Append("<tr>");
                    categoryStringBuilder.Append("<td align =\"left\"><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#7d706c; text-transform:uppercase\">Brand</span></td>");
                    categoryStringBuilder.Append("<td><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#a39c9a;\">" + pas.BrandDescription + "</span></td>");
                    categoryStringBuilder.Append("</tr>");

                    //Adding PropTypes
                    //categoryStringBuilder.Append("<tr>");
                    //categoryStringBuilder.Append("<td align =\"left\"><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#7d706c; text-transform:uppercase\">Property Type</span></td>");
                    //categoryStringBuilder.Append("<td><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#a39c9a;\">" + propTypes + "</span></td>");
                    //categoryStringBuilder.Append("</tr>");

                    //Adding Region
                    categoryStringBuilder.Append("<tr>");
                    categoryStringBuilder.Append("<td align =\"left\"><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#7d706c; text-transform:uppercase\">Region</span></td>");
                    categoryStringBuilder.Append("<td><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#a39c9a;\">" + pas.RegionDescription + "</span></td>");
                    categoryStringBuilder.Append("</tr>");
                    //Adding Country
                    categoryStringBuilder.Append("<tr>");
                    categoryStringBuilder.Append("<td align =\"left\"><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#7d706c; text-transform:uppercase\">Country</span></td>");
                    categoryStringBuilder.Append("<td><span style=\"font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#a39c9a;\">" + pas.CountryDescription + "</span></td>");
                    categoryStringBuilder.Append("</tr>");
                    //Adding Space
                    categoryStringBuilder.Append("<tr>");
                    categoryStringBuilder.Append("<td height=\"8\" colspan=\"2\"></td></tr>");
                }
            }
            categoryStringBuilder.Append("<tr>");
            categoryStringBuilder.Append("<td>&nbsp;</td>");
            categoryStringBuilder.Append("<td>&nbsp;</td></tr>");
            categoryStringBuilder.Append("</table>");
            return categoryStringBuilder.ToString();
        }

        /// <summary>
        /// CreatesSql Data Records
        /// </summary>
        /// <param name="productCategories"></param>
        /// <returns></returns>
        private static IEnumerable<SqlDataRecord> CreateSqlDataRecords(IList<ProductCategory> productCategories)
        {
            SqlMetaData[] metaData = new SqlMetaData[6];

            metaData[0] = new SqlMetaData("ProductId", SqlDbType.Int);
            metaData[1] = new SqlMetaData("PropertyBrandId", SqlDbType.Int);
            metaData[2] = new SqlMetaData("PropertyTypeId", SqlDbType.Int); //remove this field later, it is used in Table Type
            metaData[3] = new SqlMetaData("PropertyRegionId", SqlDbType.Int);
            metaData[4] = new SqlMetaData("PropertyCountryId", SqlDbType.Int);
            metaData[5] = new SqlMetaData("CatId", SqlDbType.Int);

            SqlDataRecord resultRecord = new SqlDataRecord(metaData);

            foreach (ProductCategory productCategory in productCategories)
            {
                IList<ProductAssignedSpecifications> productAssignedSpecs = productCategory.ProductAssignedSpecifications;

                foreach (ProductAssignedSpecifications specs in productAssignedSpecs)
                {
                    //IList<ProductPropertyType> propTypes = specs.PropertyTypes;

                    //foreach (ProductPropertyType propTypeItem in propTypes)
                    //{
                    resultRecord.SetInt32(0, 1);
                    resultRecord.SetInt32(1, specs.BrandId);
                    resultRecord.SetInt32(2, 1);
                    resultRecord.SetInt32(3, specs.RegionId);
                    resultRecord.SetInt32(4, specs.CountryId);
                    resultRecord.SetInt32(5, productCategory.CategoryId);
                    yield return resultRecord;
                    //}
                }
            }
        }
    }
}