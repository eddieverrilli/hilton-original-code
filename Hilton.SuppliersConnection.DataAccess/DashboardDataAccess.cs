﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class DashboardDataAccess
    {
        /// <summary>
        /// Gets the dashboard statistics
        /// </summary>
        /// <returns>dashboard</returns>
        public static Dashboard GetDashboardStatistics
        {
            get
            {
                IDataReader drDashboardDetails = null;
                Dashboard dashboard = new Dashboard();
                try
                {
                    string ConnectionString = SQLHelper.GetConnectionString();
                    SqlDatabase db = new SqlDatabase(ConnectionString);
                    using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetDashboardDetails))
                    {
                        drDashboardDetails = (IDataReader)db.ExecuteReader(dbCommand);

                        if (drDashboardDetails.Read())
                        {
                            dashboard.PartnerStatistics = new PartnerStatistics()
                            {
                                CurrentGoldPartners = Convert.ToInt32(drDashboardDetails["CurrentGoldPartners"], CultureInfo.InvariantCulture),
                                CurrentRegularPartners = Convert.ToInt32(drDashboardDetails["CurrentRegularPartners"], CultureInfo.InvariantCulture),
                                OutstandingGoldPartnersRequest = Convert.ToInt32(drDashboardDetails["OutstandingGoldPartnersReq"], CultureInfo.InvariantCulture),
                                OutstandingRegularPartnersRequest = Convert.ToInt32(drDashboardDetails["OutstandingRegularPartnersReq"], CultureInfo.InvariantCulture),
                                OutstandingGoldPartnersReqAmount = Convert.ToDecimal(drDashboardDetails["OutstandingGoldPartnersReqAmt"], CultureInfo.InvariantCulture),
                                OutstandingRegularPartnersReqAmount = Convert.ToDecimal(drDashboardDetails["OutstandingRegularPartnersReqAmt"], CultureInfo.InvariantCulture),
                                TotalGoldPartnershipAmtCollected = Convert.ToDecimal(drDashboardDetails["TotalGoldPartnershipAmtCollected"], CultureInfo.InvariantCulture),
                                TotalRegularPartnershipAmtCollected = Convert.ToDecimal(drDashboardDetails["TotalRegularPartnershipAmtCollected"], CultureInfo.InvariantCulture),
                                ActivatedGoldPartnerInCurrentMonth = Convert.ToInt32(drDashboardDetails["ActivatedGoldPartnerInCurrentMonth"], CultureInfo.InvariantCulture),
                                ActivatedRegularPartnerInCurrentMonth = Convert.ToInt32(drDashboardDetails["ActivatedRegularPartnerInCurrentMonth"], CultureInfo.InvariantCulture),
                                ActivatedGoldPartnerAmountInCurrentMonth = Convert.ToDecimal(drDashboardDetails["ActivatedGoldPartnerAmountInCurrentMonth"], CultureInfo.InvariantCulture),
                                ActivatedRegualrPartnerAmountInCurrentMonth = Convert.ToDecimal(drDashboardDetails["ActivatedRegualrPartnerAmountInCurrentMonth"], CultureInfo.InvariantCulture),
                                ActivatedGoldPartnerInCurrentYear = Convert.ToInt32(drDashboardDetails["ActivatedGoldPartnerInCurrentYear"], CultureInfo.InvariantCulture),
                                ActivatedRegularPartnerInCurrentYear = Convert.ToInt32(drDashboardDetails["ActivatedRegularPartnerInCurrentYear"], CultureInfo.InvariantCulture),
                                ActivatedGoldPartnerAmountInCurrentYear = Convert.ToDecimal(drDashboardDetails["ActivatedGoldPartnerAmountInCurrentYear"], CultureInfo.InvariantCulture),
                                ActivatedRegularPartnerAmountInCurrentYear = Convert.ToDecimal(drDashboardDetails["ActivatedRegularPartnerAmountInCurrentYear"], CultureInfo.InvariantCulture),
                                PendingGoldPartnerRequest = Convert.ToInt32(drDashboardDetails["PendingGoldPartnerRequest"], CultureInfo.InvariantCulture),
                                PendingRegularPartnerRequest = Convert.ToInt32(drDashboardDetails["PendingRegularPartnerRequest"], CultureInfo.InvariantCulture)
                            };
                        }

                        drDashboardDetails.NextResult();
                        if (drDashboardDetails.Read())
                        {
                            dashboard.ProductStatistics = new ProductStatistics()
                            {
                                PendingRequest = Convert.ToInt32(drDashboardDetails["PendingRequest"], CultureInfo.InvariantCulture),
                                OtherSelections = Convert.ToInt32(drDashboardDetails["OtherSelections"], CultureInfo.InvariantCulture),
                                TotalProducts = Convert.ToInt32(drDashboardDetails["TotalProducts"], CultureInfo.InvariantCulture)
                            };
                        }

                        drDashboardDetails.NextResult();
                        dashboard.ProjectStatistics = new List<ProjectStatistics>();

                        while (drDashboardDetails.Read())
                        {
                            dashboard.ProjectStatistics.Add(new ProjectStatistics()
                            {
                                TotalProjects = Convert.ToInt32(drDashboardDetails["TotalProjects"], CultureInfo.InvariantCulture),
                                NotConfiguredProjectsPercent = Convert.ToInt32(drDashboardDetails["NotConfiguredProjectsPer"], CultureInfo.InvariantCulture),
                                FullConfiguredProjectsPercent = Convert.ToInt32(drDashboardDetails["FullConfiguredProjectsPer"], CultureInfo.InvariantCulture),
                                PartialConfiguredProjectsPercent = Convert.ToInt32(drDashboardDetails["PartialConfiguredProjectsPer"], CultureInfo.InvariantCulture),
                                ProjectType = Convert.ToString(drDashboardDetails["ProjectTypeName"], CultureInfo.InvariantCulture),
                                ProjectTypeId = Convert.ToInt32(drDashboardDetails["ProjectTypeId"], CultureInfo.InvariantCulture)
                            }
                            );
                        }

                        drDashboardDetails.NextResult();
                        dashboard.UserStatistics = new List<UserStatistics>();
                        while (drDashboardDetails.Read())
                        {
                            dashboard.UserStatistics.Add(new UserStatistics()
                            {
                                UserType = Convert.ToString(drDashboardDetails["UserTypeDesc"], CultureInfo.InvariantCulture),
                                Total = Convert.ToInt32(drDashboardDetails["Total"], CultureInfo.InvariantCulture),
                                TotalLogOn = Convert.ToInt32(drDashboardDetails["TotalLogin"], CultureInfo.InvariantCulture),
                                AverageLogOn = Convert.ToDouble(drDashboardDetails["AvgLogins"], CultureInfo.InvariantCulture),
                                TotalAssocProj = Convert.ToString(drDashboardDetails["AssocProj"], CultureInfo.InvariantCulture)
                            }
                            );
                        }

                        drDashboardDetails.NextResult();
                        if (drDashboardDetails.Read())
                        {
                            dashboard.FeedbackStatistics = new FeedbackStatistics()
                            {
                                FeedbackSubmitted = Convert.ToInt32(drDashboardDetails["FeedbackSubmitted"], CultureInfo.InvariantCulture),
                                MessageToOwners = Convert.ToInt32(drDashboardDetails["MessageToOwners"], CultureInfo.InvariantCulture),
                                MessageToPartners = Convert.ToInt32(drDashboardDetails["MessageToPartners"], CultureInfo.InvariantCulture)
                            };
                        }

                        drDashboardDetails.Close();
                    }
                }
                catch (Exception exception)
                {
                    if (drDashboardDetails != null && !drDashboardDetails.IsClosed)
                    {
                        drDashboardDetails.Close();
                    }
                    bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }

                return dashboard;
            }
        }



        /// <summary>
        /// Fetch Dashboard Export Report Data
        /// </summary>
        /// <returns></returns>
        public static DataSet GetDashboardExportReportDataSet()
        {
            DataSet dashboardExportReportDataSet = null;
            try
            {
                using (dashboardExportReportDataSet = new DataSet())
                {
                    dashboardExportReportDataSet.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.DownloadDashboardExportReport);

                    dashboardExportReportDataSet = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return dashboardExportReportDataSet;
        }


    }
}