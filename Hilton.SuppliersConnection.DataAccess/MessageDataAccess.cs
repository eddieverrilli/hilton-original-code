﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class MessageDataAccess
    {
        /// <summary>
        /// To Export Messages
        /// </summary>
        /// <param name="messageSearch"></param>
        /// <returns></returns>
        public static DataSet ExportMessage(MessageSearch messageSearch)
        {
            DataSet messageCollection = null;
            try
            {
                using (messageCollection = new DataSet())
                {
                    messageCollection.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetMessageList);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, messageSearch.UserId);
                    if (messageSearch.DateFrom == null)
                        db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, messageSearch.DateFrom);

                    if (messageSearch.DateTo == null)
                        db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, messageSearch.DateTo);

                    if (string.IsNullOrWhiteSpace(messageSearch.SenderId))
                        db.AddInParameter(dbCommand, "@senderId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@senderId", DbType.Int32, messageSearch.SenderId);

                    if (string.IsNullOrEmpty(messageSearch.RecipientId))
                        db.AddInParameter(dbCommand, "@recipient", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@recipient", DbType.String, messageSearch.RecipientId);

                    if (string.IsNullOrEmpty(messageSearch.SenderType))
                        db.AddInParameter(dbCommand, "@senderType", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@senderType", DbType.String, messageSearch.SenderType);

                    if (String.IsNullOrEmpty(messageSearch.RegionId))
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, messageSearch.RegionId);

                    db.AddInParameter(dbCommand, "@searchExpression", DbType.String, messageSearch.QuickSearchExpression);
                    db.AddInParameter(dbCommand, "@searchType", DbType.String, messageSearch.SearchFlag);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, messageSearch.PageIndex);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, messageSearch.PageSize);
                    if (String.IsNullOrEmpty(messageSearch.SortExpression))
                        db.AddInParameter(dbCommand, "@sortExpression", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@sortExpression", DbType.String, messageSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, messageSearch.SortDirection);
                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    messageCollection = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
                //
            }
            return messageCollection;
        }

        /// <summary>
        /// This method will get the message list page sender and recipient dropdown list
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static MessageDropDown GetMessageDropDownsList(int userId)
        {
            MessageDropDown dropDowns = new MessageDropDown();
            IDataReader drDropdown = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetSenderRecipeintDropDown))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drDropdown = (IDataReader)db.ExecuteReader(dbCommand);

                    //Populate Sender dropdown
                    dropDowns.SenderDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDowns.SenderDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["SenderValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["SenderText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    //Populate recipient dropdown
                    drDropdown.NextResult();
                    dropDowns.RecipientDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDowns.RecipientDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["RecipientValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["RecipientText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    //Populate region dropdown
                    drDropdown.NextResult();
                    dropDowns.RegionDropDown = new List<DropDownItem>();
                    while (drDropdown.Read())
                    {
                        dropDowns.RegionDropDown.Add(new DropDownItem()
                        {
                            DataValueField = Convert.ToString(drDropdown["RegionValue"], CultureInfo.InvariantCulture),
                            DataTextField = Convert.ToString(drDropdown["RegionText"], CultureInfo.InvariantCulture)
                        }
                       );
                    }

                    drDropdown.Close();
                }
            }
            catch (Exception exception)
            {
                if (drDropdown != null && !drDropdown.IsClosed)
                {
                    drDropdown.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dropDowns;
        }

        /// <summary>
        /// Get Message List
        /// </summary>
        /// <param name="messageSearch"></param>
        /// <returns>List of Messages</returns>
        public static IList<Message> GetMessageList(MessageSearch messageSearch, ref int totalRecordCount)
        {
            IList<Message> messageCollection = new List<Message>();
            IDataReader drMessages = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetMessageList))
                {
                    if (messageSearch.DateFrom == null)
                        db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@fromDate", DbType.DateTime, messageSearch.DateFrom);

                    if (messageSearch.DateTo == null)
                        db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@toDate", DbType.DateTime, messageSearch.DateTo);

                    if (string.IsNullOrWhiteSpace(messageSearch.SenderId))
                        db.AddInParameter(dbCommand, "@senderId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@senderId", DbType.Int32, messageSearch.SenderId);

                    if (string.IsNullOrEmpty(messageSearch.RecipientId))
                        db.AddInParameter(dbCommand, "@recipient", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@recipient", DbType.String, messageSearch.RecipientId);

                    if (string.IsNullOrEmpty(messageSearch.SenderType))
                        db.AddInParameter(dbCommand, "@senderType", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@senderType", DbType.String, messageSearch.SenderType);

                    if (String.IsNullOrEmpty(messageSearch.RegionId))
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, messageSearch.RegionId);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, messageSearch.UserId);
                    db.AddInParameter(dbCommand, "@searchExpression", DbType.String, messageSearch.QuickSearchExpression);
                    db.AddInParameter(dbCommand, "@searchType", DbType.String, messageSearch.SearchFlag);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, messageSearch.PageIndex);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, messageSearch.PageSize);
                    if (String.IsNullOrEmpty(messageSearch.SortExpression))
                        db.AddInParameter(dbCommand, "@sortExpression", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@sortExpression", DbType.String, messageSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, messageSearch.SortDirection);
                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drMessages = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drMessages.Read())
                    {
                        {
                            messageCollection.Add(new Message()
                            {
                                MessageDateTime = Convert.ToDateTime(drMessages["MesssageDate"], CultureInfo.InvariantCulture),
                                SenderType = Convert.ToString(drMessages["SenderType"], CultureInfo.InvariantCulture),
                                RecipientName = Convert.ToString(drMessages["RecipientFirstName"] + " " + Convert.ToString(drMessages["RecipientLastName"], CultureInfo.InvariantCulture), CultureInfo.InvariantCulture),
                                SenderName = Convert.ToString(drMessages["FirstName"] + " " + Convert.ToString(drMessages["LastName"], CultureInfo.InvariantCulture), CultureInfo.InvariantCulture),
                                CategoryName = Convert.ToString(drMessages["CatName"], CultureInfo.InvariantCulture),
                                MessageStatusId = Convert.ToInt32(drMessages["MessageStatusId"], CultureInfo.InvariantCulture),
                                Details = Convert.ToString(drMessages["MessageDetails"], CultureInfo.InvariantCulture),
                                MessageId = Convert.ToInt32(drMessages["MessageId"], CultureInfo.InvariantCulture),
                            }
                            );
                        }
                    }

                    drMessages.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                }

                drMessages.Close();
            }
            catch (Exception exception)
            {
                if (drMessages != null && !drMessages.IsClosed)
                {
                    drMessages.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return messageCollection;
        }

        /// <summary>
        /// To Update Message
        /// </summary>
        /// <param name="messages"></param>
        /// <returns></returns>
        public static int UpdateMessage(IList<Message> messages)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateMessage))
                {
                    DataTable messageUpdatesDataTable;
                    // create data table to insert items

                    using (messageUpdatesDataTable = new DataTable("MessageUpdates"))
                    {
                        messageUpdatesDataTable.Locale = CultureInfo.InvariantCulture;
                        messageUpdatesDataTable.Columns.Add("MessageId", typeof(int));
                        messageUpdatesDataTable.Columns.Add("StatusId", typeof(int));

                        messages.ToList().ForEach(x =>
                        {
                            messageUpdatesDataTable.Rows.Add(x.MessageId, x.MessageStatusId);
                        });

                        SqlParameter messageUpdatesParam = new SqlParameter("@messages", messageUpdatesDataTable);
                        messageUpdatesParam.SqlDbType = SqlDbType.Structured;
                        messageUpdatesParam.Direction = ParameterDirection.Input;
                        dbCommand.Parameters.Add(messageUpdatesParam);

                        SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                        retValParam.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(retValParam);

                        db.ExecuteNonQuery(dbCommand);
                        result = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// To Insert Message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool InsertMessage(Message message)
        {
            bool result = false;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString();   SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.AddMessage))
                {
                    db.AddInParameter(dbCommand, "@senderId", DbType.Int32, message.SenderId);
                    db.AddInParameter(dbCommand, "@productId", DbType.Int32, message.ProductId);
                    db.AddInParameter(dbCommand, "@toFirstName", DbType.String, message.ToFirstName);
                    db.AddInParameter(dbCommand, "@toLastName", DbType.String, message.ToLastName);
                    db.AddInParameter(dbCommand, "@isCopyToSelf", DbType.Boolean, message.IsCopyToSelf);
                    db.AddInParameter(dbCommand, "@regionId", DbType.Int32, message.RegionId);
                    db.AddInParameter(dbCommand, "@messageDetails", DbType.String, message.Details);
                    db.AddInParameter(dbCommand, "@senderType", DbType.String, message.SenderType);
                    db.AddInParameter(dbCommand, "@statusId", DbType.Int32, message.StatusId);
                    db.AddInParameter(dbCommand, "@brandId", DbType.Int32, message.BrandId);
                    db.AddInParameter(dbCommand, "@propertyTypeId", DbType.Int32, message.PropertyTypeId);
                    db.AddInParameter(dbCommand, "@templateId", DbType.Int32, message.TemplateId);
                    db.AddInParameter(dbCommand, "@toEmail", DbType.String, message.ToEmail);
                    db.AddInParameter(dbCommand, "@address", DbType.String, message.Address);
                    db.AddInParameter(dbCommand, "@partnerId", DbType.String, message.PartnerId);
                    db.AddInParameter(dbCommand, "@categoryDisplayName", DbType.String, message.CategoryDisplayName);

                    if (message.ProjectId != 0)
                    {
                        db.AddInParameter(dbCommand, "@projectId", DbType.Int32, message.ProjectId);
                        db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, message.CatId);
                    }

                    if (db.ExecuteNonQuery(dbCommand) > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}