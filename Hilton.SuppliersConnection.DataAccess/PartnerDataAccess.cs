﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.SqlServer.Server;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Hilton.SuppliersConnection.DataAccess
{
    public static class PartnerDataAccess
    {
        /// <summary>
        /// Add new partner
        /// </summary>
        /// <param name="partner"></param>
        /// <param name="partnerCategories"></param>
        /// <returns></returns>
        public static int AddPartner(Partner partner, IList<PartnerCategories> partnerCategories)
        {
            int partnerId = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsertPartner))
                {
                    db.AddInParameter(dbCommand, "@companyName", DbType.String, partner.CompanyName);
                    db.AddInParameter(dbCommand, "@webSiteLink", DbType.String, partner.WebSiteLink);
                    db.AddInParameter(dbCommand, "@companyAddress1", DbType.String, partner.CompanyAddress1);
                    db.AddInParameter(dbCommand, "@companyAddress2", DbType.String, partner.CompanyAddress2);
                    db.AddInParameter(dbCommand, "@countryId", DbType.Int32, partner.CountryId);
                    db.AddInParameter(dbCommand, "@city", DbType.String, partner.City);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, partner.UserId);
                    if (partner.StateId != 0)
                    {
                        db.AddInParameter(dbCommand, "@stateId", DbType.Int32, partner.StateId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@stateId", DbType.Int32, null);
                    }

                    if (string.IsNullOrWhiteSpace(partner.ZipCode))
                    {
                        db.AddInParameter(dbCommand, "@zip", DbType.String, null);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@zip", DbType.String, partner.ZipCode);
                    }
                    db.AddInParameter(dbCommand, "@companyDescription", DbType.String, partner.CompanyDescription);
                    db.AddInParameter(dbCommand, "@IsSam", DbType.Boolean, partner.IsSAM);
                    db.AddInParameter(dbCommand, "@IsRecPartner", DbType.Boolean, partner.IsRecPartner);
                    db.AddInParameter(dbCommand, "@companyLogo", DbType.Binary, partner.CompanyLogoImage);
                    db.AddInParameter(dbCommand, "@companyLogoThumbnail", DbType.Binary, partner.CompanyLogoThumbnailImage);
                    db.AddInParameter(dbCommand, "@companyLogoName", DbType.String, partner.CompanyLogoName);
                    db.AddInParameter(dbCommand, "@partnershipStatus", DbType.Int32, partner.PartnershipStatusId);
                    db.AddInParameter(dbCommand, "@submitProducts", DbType.Boolean, partner.SubmitProducts);
                    db.AddInParameter(dbCommand, "@isAutomatedRenewalEmailReq", DbType.Boolean, partner.IsAutomatedRenewalEmailReq);
                    db.AddInParameter(dbCommand, "@partnershipType", DbType.Int32, partner.PartnershipTypeId);

                    SqlParameter specParam = new SqlParameter("@partnerCategories", SqlDbType.Structured);
                    specParam.TypeName = "dbo.PartnerCategories";
                    specParam.Value = CreateSqlDataRecords(partnerCategories);
                    dbCommand.Parameters.Add(specParam);

                    if (partner.ActiveDate != null)
                    {
                        db.AddInParameter(dbCommand, "@activeDate", DbType.DateTime, partner.ActiveDate);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@activeDate", DbType.DateTime, DBNull.Value);
                    }
                    if (partner.ExpirationDate != null)
                    {
                        db.AddInParameter(dbCommand, "@expirationDate", DbType.DateTime, partner.ExpirationDate);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@expirationDate", DbType.DateTime, DBNull.Value);
                    }

                    SqlParameter contact = new SqlParameter("@partnerContacts", SqlDbType.Structured);
                    contact.TypeName = "dbo.PartnerContactCountryType";
                    contact.Direction = ParameterDirection.Input;
                    contact.Value = ConvertToContacts(partner.Contacts);
                    dbCommand.Parameters.Add(contact);

                    SqlParameter retValParam = new SqlParameter("@partnerId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    partnerId = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return partnerId;
        }

        /// <summary>
        /// Check Duplicate Partner
        /// </summary>
        /// <param name="companyName"></param>
        /// <returns></returns>
        public static int CheckDuplicatePartner(string companyName)
        {
            int partnerId = 0;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.CheckDuplicatePartner))
                {
                    db.AddInParameter(dbCommand, "@companyName", DbType.String, companyName);

                    SqlParameter retValParam = new SqlParameter("@partnerId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    partnerId = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return partnerId;
        }

        /// <summary>
        /// Get the partner details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns>Partner</returns>
        public static Partner GetPartnerDetails(int partnerId)
        {
            Partner partner = null;
            IDataReader drPartnerDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerDetails))
                {
                    db.AddInParameter(dbCommand, "@partnerId ", DbType.Int32, partnerId);

                    drPartnerDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drPartnerDetails.Read())
                    {
                        partner = new Partner()
                        {
                            CompanyName = Convert.ToString(drPartnerDetails["CompanyName"], CultureInfo.InvariantCulture),
                            CompanyAddress1 = Convert.ToString(drPartnerDetails["CompanyAddress1"], CultureInfo.InvariantCulture),
                            CompanyAddress2 = Convert.IsDBNull(drPartnerDetails["CompanyAddress2"]) ? string.Empty : Convert.ToString(drPartnerDetails["CompanyAddress2"], CultureInfo.InvariantCulture),
                            Country = Convert.ToString(drPartnerDetails["CountryName"], CultureInfo.InvariantCulture),
                            City = Convert.ToString(drPartnerDetails["CityName"], CultureInfo.InvariantCulture),
                            State = Convert.IsDBNull(drPartnerDetails["StateName"]) ? null : Convert.ToString(drPartnerDetails["StateName"], CultureInfo.InvariantCulture),
                            StateId = Convert.IsDBNull(drPartnerDetails["StateId"]) ? 0 : Convert.ToInt32(drPartnerDetails["StateId"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.IsDBNull(drPartnerDetails["ZipCode"]) ? null : Convert.ToString(drPartnerDetails["ZipCode"], CultureInfo.InvariantCulture),
                            CompanyDescription = Convert.IsDBNull(drPartnerDetails["CompanyDescription"]) ? string.Empty : Convert.ToString(drPartnerDetails["CompanyDescription"], CultureInfo.InvariantCulture),
                            IsSAM = Convert.ToBoolean(drPartnerDetails["Sam"], CultureInfo.InvariantCulture),
                            IsRecPartner = Convert.IsDBNull(drPartnerDetails["IsRecPartner"]) ? false : Convert.ToBoolean(drPartnerDetails["IsRecPartner"], CultureInfo.InvariantCulture),
                            CompanyLogoThumbnailImage = Convert.IsDBNull(drPartnerDetails["CompanyLogoThumbnail"]) ? null : (byte[])(drPartnerDetails["CompanyLogoThumbnail"]),
                            CompanyLogoName = Convert.IsDBNull(drPartnerDetails["CompanyLogoName"]) ? null : Convert.ToString(drPartnerDetails["CompanyLogoName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drPartnerDetails["CountryId"], CultureInfo.InvariantCulture),
                            WebSiteLink = Convert.ToString(drPartnerDetails["WebSite"], CultureInfo.InvariantCulture)
                        };
                    }
                    drPartnerDetails.NextResult(); //To get the details for primary contact
                    partner.Contacts = new List<Contact>();
                    if (drPartnerDetails.Read())
                    {
                        partner.Contacts.Add(new Contact()
                        {
                            ContactId = Convert.ToString(drPartnerDetails["ContactId"], CultureInfo.InvariantCulture),
                            ContactTypeId = Convert.ToInt32(drPartnerDetails["ContactTypeId"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drPartnerDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drPartnerDetails["LastName"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drPartnerDetails["Title"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drPartnerDetails["Phone"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drPartnerDetails["Fax"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drPartnerDetails["Email"], CultureInfo.InvariantCulture),
                        });
                    }
                    drPartnerDetails.NextResult(); //To get the details for secondary contact
                    //                    partner.Contacts = new List<Contact>();
                    if (drPartnerDetails.Read())
                    {
                        partner.Contacts.Add(new Contact()
                        {
                            ContactId = Convert.ToString(drPartnerDetails["ContactId"], CultureInfo.InvariantCulture),
                            ContactTypeId = Convert.ToInt32(drPartnerDetails["ContactTypeId"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drPartnerDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drPartnerDetails["LastName"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drPartnerDetails["Title"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drPartnerDetails["Phone"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drPartnerDetails["Fax"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drPartnerDetails["Email"], CultureInfo.InvariantCulture),
                        });
                    }
                    drPartnerDetails.NextResult(); //To get the details for regional contacts
                    while (drPartnerDetails.Read())
                    {
                        partner.Contacts.Add(new Contact()
                        {
                            ContactId = Convert.ToString(drPartnerDetails["ContactId"], CultureInfo.InvariantCulture),
                            ContactTypeId = Convert.ToInt32(drPartnerDetails["ContactTypeId"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drPartnerDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drPartnerDetails["LastName"], CultureInfo.InvariantCulture),
                            Title = Convert.IsDBNull(drPartnerDetails["Title"]) ? null : Convert.ToString(drPartnerDetails["Title"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drPartnerDetails["Phone"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drPartnerDetails["Fax"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drPartnerDetails["Email"], CultureInfo.InvariantCulture),
                            RegionId = Convert.IsDBNull(drPartnerDetails["ContactRegionId"]) ? 0 : Convert.ToInt32(drPartnerDetails["ContactRegionId"], CultureInfo.InvariantCulture),
                            CountryId = Convert.IsDBNull(drPartnerDetails["CountryId"]) ? 0 : Convert.ToInt32(drPartnerDetails["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drPartnerDetails["CountryName"], CultureInfo.InvariantCulture)
                        });
                    }
                    drPartnerDetails.NextResult();
                    if (drPartnerDetails.Read())
                    {
                        partner.PartnershipStatusId = Convert.ToInt32(drPartnerDetails["PartnershipStatusId"], CultureInfo.InvariantCulture);
                        partner.PartnershipReqSendDate = Convert.ToDateTime(drPartnerDetails["RequestDate"], CultureInfo.InvariantCulture);
                        partner.SubmitProducts = Convert.ToBoolean(drPartnerDetails["SubmitProducts"], CultureInfo.InvariantCulture);
                        partner.IsAutomatedRenewalEmailReq = Convert.ToBoolean(drPartnerDetails["IsAutomatedRenewalEmailReq"], CultureInfo.InvariantCulture);
                        partner.PartnershipTypeId = Convert.ToInt32(drPartnerDetails["PartnershipType"], CultureInfo.InvariantCulture);

                        partner.ActiveDate = drPartnerDetails["ActivationDate"] != DBNull.Value ? (DateTime?)drPartnerDetails["ActivationDate"] : null;
                        partner.ExpirationDate = drPartnerDetails["ExpriationDate"] != DBNull.Value ? (DateTime?)drPartnerDetails["ExpriationDate"] : null;
                    }
                    drPartnerDetails.NextResult();
                    partner.PaymentRequestDetails = new PaymentRequestDetails();
                    if (drPartnerDetails.Read())
                    {
                        partner.PaymentRequestDetails.RequestSentTo = Convert.ToString(drPartnerDetails["ToEmail"], CultureInfo.InvariantCulture);
                        partner.PaymentRequestDetails.RequestSentOn = Convert.ToDateTime(drPartnerDetails["RequestDate"], CultureInfo.InvariantCulture);
                    }

                    drPartnerDetails.NextResult();
                    if (drPartnerDetails.Read())
                    {
                        partner.PaymentRequestDetails.GoldAmount = Convert.ToString(drPartnerDetails["GoldRequestAmount"], CultureInfo.InvariantCulture);
                        partner.PaymentRequestDetails.RegularAmount = Convert.ToString(drPartnerDetails["ReqRequestAmount"], CultureInfo.InvariantCulture);
                    }

                    drPartnerDetails.NextResult();
                    partner.AssociatedWebAccount = new List<PartnerLinkedWebAccount>();
                    while (drPartnerDetails.Read())
                    {
                        partner.AssociatedWebAccount.Add(new PartnerLinkedWebAccount()
                        {
                            HiltonId = Convert.ToString(drPartnerDetails["HiltonId"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drPartnerDetails["Email"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drPartnerDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drPartnerDetails["LastName"], CultureInfo.InvariantCulture),
                            StatusDesc = Convert.ToString(drPartnerDetails["StatusDesc"], CultureInfo.InvariantCulture),
                            PartnerId = partnerId,
                            UserId = Convert.ToInt32(drPartnerDetails["UserId"], CultureInfo.InvariantCulture),
                        });
                    }

                    drPartnerDetails.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drPartnerDetails != null && !drPartnerDetails.IsClosed)
                {
                    drPartnerDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return partner;
        }

        /// <summary>
        /// To Get Partner Profile Details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns>Partner Entity</returns>
        public static Tuple<IList<SearchFilters>, Partner> GetPartnerProfileDetails(int partnerId, string brandId, string propertyTypeId, string regionId, string countryId, string catId)
        {
            IList<SearchFilters> searchContacts = new List<SearchFilters>();
            Partner partner = null;
            IDataReader drPartnerDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerProfileDetails))
                {
                    db.AddInParameter(dbCommand, "@PartnerId ", DbType.Int32, partnerId);

                    if (brandId != "0")
                    {
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, brandId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    }

                    if (regionId != "0")
                    {
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, regionId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, DBNull.Value);
                    }

                    if (countryId != "0")
                    {
                        db.AddInParameter(dbCommand, "@countryId", DbType.String, countryId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@countryId", DbType.String, DBNull.Value);
                    }

                    if (catId != "0")
                    {
                        db.AddInParameter(dbCommand, "@catId", DbType.String, catId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@catId", DbType.String, DBNull.Value);
                    }
                    dbCommand.CommandTimeout = 120;
                    drPartnerDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drPartnerDetails.Read())
                    {
                        partner = new Partner()
                        {
                            CompanyName = Convert.ToString(drPartnerDetails["CompanyName"], CultureInfo.InvariantCulture),
                            CompanyAddress1 = Convert.ToString(drPartnerDetails["CompanyAddress1"], CultureInfo.InvariantCulture),
                            CompanyAddress2 = Convert.ToString(drPartnerDetails["CompanyAddress2"], CultureInfo.InvariantCulture),
                            CompanyDescription = Convert.ToString(drPartnerDetails["CompanyDescription"], CultureInfo.InvariantCulture),
                            IsSAM = Convert.ToBoolean(drPartnerDetails["Sam"], CultureInfo.InvariantCulture),
                            CompanyLogoImage = Convert.IsDBNull(drPartnerDetails["CompanyLogo"]) ? null : (byte[])(drPartnerDetails["CompanyLogo"]),
                            Country = Convert.ToString(drPartnerDetails["CountryName"], CultureInfo.InvariantCulture),
                            State = Convert.ToString(drPartnerDetails["StateName"], CultureInfo.InvariantCulture),
                            City = Convert.ToString(drPartnerDetails["CityName"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.ToString(drPartnerDetails["ZipCode"], CultureInfo.InvariantCulture)
                        };
                    }

                    drPartnerDetails.NextResult(); //To get the details for primary contact
                    partner.Contacts = new List<Contact>();

                    while (drPartnerDetails.Read())
                    {
                        partner.Contacts.Add(new Contact()
                        {
                            FirstName = Convert.ToString(drPartnerDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drPartnerDetails["LastName"], CultureInfo.InvariantCulture),
                            ContactTypeId = Convert.ToInt32(drPartnerDetails["ContactTypeId"], CultureInfo.InvariantCulture),
                            Address1 = Convert.ToString(drPartnerDetails["CompanyAddress1"], CultureInfo.InvariantCulture),
                            Address2 = Convert.ToString(drPartnerDetails["CompanyAddress2"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drPartnerDetails["Email"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drPartnerDetails["Phone"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drPartnerDetails["Title"], CultureInfo.InvariantCulture),
                            ContactTypeDesc = Convert.ToString(drPartnerDetails["ContactTypeDesc"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drPartnerDetails["ContactRegionId"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drPartnerDetails["ContactCountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drPartnerDetails["CountryName"], CultureInfo.InvariantCulture),
                            RegionName = Convert.ToString(drPartnerDetails["RegionName"], CultureInfo.InvariantCulture),
                            Website = Convert.ToString(drPartnerDetails["Website"], CultureInfo.InvariantCulture),
                            ContactId = Convert.ToString(drPartnerDetails["ContactId"], CultureInfo.InvariantCulture)
                        });

                        searchContacts.Add(new SearchFilters()
                        {
                            ContactTypeId = Convert.ToInt32(drPartnerDetails["ContactCountryId"], CultureInfo.InvariantCulture),
                            ContactTypeDesc = Convert.ToString(drPartnerDetails["ContactTypeDesc"], CultureInfo.InvariantCulture)
                        });
                    }

                    drPartnerDetails.NextResult();

                    while (drPartnerDetails.Read())
                    {
                        searchContacts.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drPartnerDetails["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drPartnerDetails["BrandName"], CultureInfo.InvariantCulture),
                            //PropertyTypeId = Convert.ToInt32(drPartnerDetails["PropertyTypeId"], CultureInfo.InvariantCulture),
                            //PropertyTypeDescription = Convert.ToString(drPartnerDetails["PropertyTypeName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drPartnerDetails["RegionId"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drPartnerDetails["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drPartnerDetails["CountryName"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drPartnerDetails["RegionName"], CultureInfo.InvariantCulture)
                        });
                    }

                    drPartnerDetails.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drPartnerDetails != null && !drPartnerDetails.IsClosed)
                {
                    drPartnerDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return new Tuple<IList<SearchFilters>, Partner>(searchContacts, partner);
        }

        /// <summary>
        /// To Get Partner Image
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns>Partner Entity</returns>
        public static Partner GetPartnerImage(int partnerId)
        {
            Partner partner = null;
            IDataReader drPartnerDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerImage))
                {
                    db.AddInParameter(dbCommand, "@PartnerId ", DbType.Int32, partnerId);

                    drPartnerDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drPartnerDetails.Read())
                    {
                        partner = new Partner()
                        {
                            CompanyLogoName = Convert.ToString(drPartnerDetails["CompanyLogoName"], CultureInfo.InvariantCulture),
                            CompanyLogoImage = Convert.IsDBNull(drPartnerDetails["CompanyLogo"]) ? null : (byte[])(drPartnerDetails["CompanyLogo"]),
                        };
                    }

                    drPartnerDetails.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drPartnerDetails != null && !drPartnerDetails.IsClosed)
                {
                    drPartnerDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return partner;
        }

        /// <summary>
        /// To Get Partnership Amount
        /// </summary>
        /// <returns></returns>
        public static DataSet GetPartnershipAmount()
        {
            DataSet dsPartnershipAmountDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnershipAmountDetails))
                {
                    using (dsPartnershipAmountDetails = new DataSet())
                    {
                        dsPartnershipAmountDetails = db.ExecuteDataSet(dbCommand);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dsPartnershipAmountDetails;
        }

        /// <summary>
        /// Creates Sql Data Records
        /// </summary>
        /// <param name="partnerCategories"></param>
        /// <returns></returns>
        private static IEnumerable<SqlDataRecord> CreateSqlDataRecords(IList<PartnerCategories> partnerCategories)
        {
            SqlMetaData[] metaData = new SqlMetaData[7];
            metaData[0] = new SqlMetaData("PartnerId", SqlDbType.Int);
            metaData[1] = new SqlMetaData("CategoryId", SqlDbType.Int);
            metaData[2] = new SqlMetaData("BrandId", SqlDbType.Int);
            metaData[3] = new SqlMetaData("PropertyTypeId", SqlDbType.Int);
            metaData[4] = new SqlMetaData("RegionId", SqlDbType.Int);
            metaData[5] = new SqlMetaData("IsAvailable", SqlDbType.Bit);
            metaData[6] = new SqlMetaData("CountryId", SqlDbType.Int);

            SqlDataRecord resultRecord = new SqlDataRecord(metaData);

            foreach (PartnerCategories partnerCategory in partnerCategories)
            {
                IList<PartnerCategoryDetails> partnerAssignedSpecs = partnerCategory.PartnerCategoriesDetails;

                foreach (PartnerCategoryDetails specs in partnerAssignedSpecs)
                {

                    if (specs.Countries != null && specs.Countries.Count != 0)
                    {
                        foreach (Country country in specs.Countries)
                        {
                            resultRecord.SetInt32(0, partnerCategory.PartnerId);
                            resultRecord.SetInt32(1, partnerCategory.CategoryId);
                            resultRecord.SetInt32(2, specs.BrandId);
                            resultRecord.SetInt32(3, 1);
                            resultRecord.SetInt32(4, specs.RegionId);
                            resultRecord.SetBoolean(5, specs.IsAvailable);
                            resultRecord.SetInt32(6, country.CountryId);

                            yield return resultRecord;
                        }
                    }
                    else
                    {
                        resultRecord.SetInt32(0, partnerCategory.PartnerId);
                        resultRecord.SetInt32(1, partnerCategory.CategoryId);
                        resultRecord.SetInt32(2, specs.BrandId);
                        resultRecord.SetInt32(3, 1);
                        resultRecord.SetInt32(4, specs.RegionId);
                        resultRecord.SetBoolean(5, specs.IsAvailable);
                        resultRecord.SetInt32(6, 0);

                        yield return resultRecord;
                    }
                    //}for
                }
            }
        }

        /// <summary>
        /// Get countries for a region
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns>List of DropDownItem Entity</returns>
        public static IList<DropDownItem> GetCountriesForRegion(int regionId)
        {
            IList<DropDownItem> countries = new List<DropDownItem>();
            IDataReader drCountries = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetCountriesForRegion))
                {
                    db.AddInParameter(dbCommand, "@regionId", DbType.Int32, regionId);

                    drCountries = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCountries.Read())
                    {
                        countries.Add(new DropDownItem()
                        {
                            DataTextField = Convert.ToString(drCountries["CountryName"], CultureInfo.InvariantCulture),
                            DataValueField = Convert.ToString(drCountries["CountryId"], CultureInfo.InvariantCulture)
                        });
                    }
                    drCountries.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drCountries != null && !drCountries.IsClosed)
                {
                    drCountries.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return countries;
        }

        /// <summary>
        /// Gets Partner Approved Categories
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns>List of PartnerCategories</returns>
        public static IList<PartnerCategories> GetApprovedPartnerCategory(SearchCriteria searchCriteria)
        {
            IList<PartnerCategories> approvedPartnerCategoryCollection = new List<PartnerCategories>();
            IDataReader drPartnerCategoryDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnersApprovedCategories))
                {
                    db.AddInParameter(dbCommand, "@brandId", DbType.String, (String.IsNullOrEmpty(searchCriteria.BrandId)) ? (object)DBNull.Value : searchCriteria.BrandId);
                    db.AddInParameter(dbCommand, "@regionId", DbType.String, (String.IsNullOrEmpty(searchCriteria.RegionId)) ? (object)DBNull.Value : searchCriteria.RegionId);
                    db.AddInParameter(dbCommand, "@countryid", DbType.String, (String.IsNullOrEmpty(searchCriteria.CountryId)) ? (object)DBNull.Value : searchCriteria.CountryId);
                    //  db.AddInParameter(dbCommand, "@propertyTypeId", DbType.String, (String.IsNullOrEmpty(searchCriteria.PropertyTypeId)) ? (object)DBNull.Value : searchCriteria.PropertyTypeId);
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, searchCriteria.PartnerId);

                    drPartnerCategoryDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drPartnerCategoryDetails.Read())
                    {
                        approvedPartnerCategoryCollection.Add(
                                                               new PartnerCategories()
                                                               {
                                                                   CategoryId = Convert.ToInt32(drPartnerCategoryDetails["CatId"], CultureInfo.InvariantCulture),
                                                                   CategoryDisplayName = Convert.ToString(drPartnerCategoryDetails["CatDisplayName"], CultureInfo.InvariantCulture),

                                                                   PartnerCategoriesDetails = new List<PartnerCategoryDetails>()
																								  {
																									new PartnerCategoryDetails()
																										{
																											BrandDescription = Convert.ToString(drPartnerCategoryDetails["BrandName"], CultureInfo.InvariantCulture),
																											BrandImageUrl = "../Images/"+System.Web.HttpUtility.UrlDecode(Convert.ToString(drPartnerCategoryDetails["BrandImageName"], CultureInfo.InvariantCulture).Trim())+".png",
																											//PropertyTypeDescription = Convert.ToString(drPartnerCategoryDetails["PropertyTypeName"], CultureInfo.InvariantCulture),
																											RegionDescription = Convert.ToString(drPartnerCategoryDetails["RegionName"], CultureInfo.InvariantCulture),
																											StyleDescription = string.Empty,
																											StandardDescription = Convert.IsDBNull(drPartnerCategoryDetails["StandardDescription"]) ? "-" : Convert.ToString(drPartnerCategoryDetails["StandardDescription"], CultureInfo.InvariantCulture),
																											CategoryStandard = new  List<CategoryStandard>()
																											   {
																												   new CategoryStandard()
																												   {
																													   StandardDescription = Convert.ToString(drPartnerCategoryDetails["StandardDescription"], CultureInfo.InvariantCulture),
																												   }
																											   },
                                                                                                              Countries =new List<Country>{new Country{CountryName= Convert.ToString(drPartnerCategoryDetails["CountryName"], CultureInfo.InvariantCulture)}}
																										}
																								  }
                                                               }
                            );
                    }
                    drPartnerCategoryDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerCategoryDetails != null && !drPartnerCategoryDetails.IsClosed)
                {
                    drPartnerCategoryDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return approvedPartnerCategoryCollection;
        }

        /// <summary>
        /// Convert To Partnerships
        /// </summary>
        /// <param name="genericList"></param>
        /// <returns></returns>
        public static DataTable ConvertToPartnerships(IList<AppliedPartnershipOpportunity> genericList)
        {
            DataTable dataTable = null;
            try
            {
                using (dataTable = new DataTable("dataTablePartnershipCategories"))
                {
                    dataTable.Locale = CultureInfo.InvariantCulture;
                    dataTable.Columns.Add("CategoryId", typeof(int));
                    dataTable.Columns.Add("BrandId", typeof(int));
                    dataTable.Columns.Add("PropertyTypeId", typeof(int));
                    dataTable.Columns.Add("RegionId", typeof(int));
                    dataTable.Columns.Add("CountryId", typeof(int));
                    genericList.ToList().ForEach(x =>
                    {
                        dataTable.Rows.Add(x.CategoryId, x.BrandId, x.PropertyTypeId, x.RegionId, x.CountryId);
                    });
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return dataTable;
        }

        /// <summary>
        /// Get Application Category Hierarchy
        /// </summary>
        /// <param name="catIds"></param>
        /// <returns>List of ApprovedPartnershipSpecification Entity</returns>
        public static IList<ApprovedPartnershipSpecification> GetAppCategoryHierarchy(string catIds)
        {
            IList<ApprovedPartnershipSpecification> appCatHierarchy = new List<ApprovedPartnershipSpecification>();
            IDataReader drAppCatHierarchy = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetAppCategoryHierarchy))
                {
                    db.AddInParameter(dbCommand, "@CatIds", DbType.String, catIds);
                    drAppCatHierarchy = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drAppCatHierarchy.Read())
                    {
                        appCatHierarchy.Add(new ApprovedPartnershipSpecification()
                        {
                            CategoryId = Convert.ToInt32(drAppCatHierarchy["Child"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drAppCatHierarchy["Parent"], CultureInfo.InvariantCulture),
                            Level = Convert.ToInt32(drAppCatHierarchy["Level"], CultureInfo.InvariantCulture),
                            Sequence = Convert.ToString(drAppCatHierarchy["Sequence"], CultureInfo.InvariantCulture),
                            CategoryName = Convert.ToString(drAppCatHierarchy["CategotyName"], CultureInfo.InvariantCulture),
                        });
                    }
                    drAppCatHierarchy.Close();
                }
            }
            catch (Exception exception)
            {
                if (drAppCatHierarchy != null && !drAppCatHierarchy.IsClosed)
                {
                    drAppCatHierarchy.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return appCatHierarchy;
        }

        /// <summary>
        /// Get Approved Categories
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static DataSet GetApprovedCategories(int partnerId)
        {
            DataSet dsAppCatDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.SelectPartnerApprovedCategories))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    using (dsAppCatDetails = new DataSet())
                    {
                        dsAppCatDetails = db.ExecuteDataSet(dbCommand);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dsAppCatDetails;
        }

        /// <summary>
        /// Get Partner Application Categories And Products
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="brandId"></param>
        /// <param name="propertyTypeId"></param>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public static ProjectTemplate GetPartnerAppCatAndProducts(int partnerId, string brandId, string propertyTypeId, string regionId, string countryId, string catId)
        {
            ProjectTemplate projectTemplate = new ProjectTemplate();
            projectTemplate.PartnerProductTrial = new List<PartnerProductTrial>();
            IDataReader drPartnerProduct = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerProfileAndProductDetails))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);

                    if (brandId != "0")
                    {
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, brandId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    }

                    if (regionId != "0")
                    {
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, regionId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, DBNull.Value);
                    }

                    if (countryId != "0")
                    {
                        db.AddInParameter(dbCommand, "@CountryId", DbType.String, countryId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@CountryId", DbType.String, DBNull.Value);
                    }
                    if (catId != "0")
                    {
                        db.AddInParameter(dbCommand, "@catId", DbType.String, catId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@catId", DbType.String, DBNull.Value);
                    }
                    dbCommand.CommandTimeout = 120;
                    drPartnerProduct = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drPartnerProduct.Read())
                    {
                        projectTemplate.PartnerProductTrial.Add(new PartnerProductTrial()
                        {
                            CatId = Convert.ToInt32(drPartnerProduct["CatId"], CultureInfo.InvariantCulture),
                            CategoryDisplayName = drPartnerProduct["CategoryDisplayName"].ToString(),
                            BrandId = Convert.ToInt32(drPartnerProduct["BrandId"], CultureInfo.InvariantCulture),
                            BrandName = drPartnerProduct["BrandName"].ToString(),
                            RegionId = Convert.ToInt32(drPartnerProduct["RegionId"], CultureInfo.InvariantCulture),
                            RegionName = drPartnerProduct["RegionName"].ToString(),
                            CountryId = Convert.ToInt32(drPartnerProduct["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = drPartnerProduct["CountryName"].ToString(),
                            ProductId = Convert.IsDBNull(drPartnerProduct["ProductId"]) ? 0 : Convert.ToInt32(drPartnerProduct["ProductId"], CultureInfo.InvariantCulture),
                            CompanyName = Convert.ToString(drPartnerProduct["CompanyName"], CultureInfo.InvariantCulture),
                            CompanyLogo = Convert.IsDBNull(drPartnerProduct["CompanyLogo"]) ? null : (byte[])(drPartnerProduct["CompanyLogo"]),
                            ImageName = Convert.ToString(drPartnerProduct["ProductImageName"], CultureInfo.InvariantCulture),
                            ProductImage = Convert.IsDBNull(drPartnerProduct["ProductThumbnailImage"]) ? null : (byte[])(drPartnerProduct["ProductThumbnailImage"]),
                            ProductName = Convert.IsDBNull(drPartnerProduct["ProductName"]) ? string.Empty : drPartnerProduct["ProductName"].ToString(),
                            ProductDescription = Convert.IsDBNull(drPartnerProduct["ProductDescription"]) ? string.Empty : drPartnerProduct["ProductDescription"].ToString(),
                            SkuNumber = Convert.IsDBNull(drPartnerProduct["SKUNumber"]) ? string.Empty : drPartnerProduct["SKUNumber"].ToString(),
                            SpecSheetPDFName = Convert.ToString(drPartnerProduct["SpecSheetPDFName"], CultureInfo.InvariantCulture),
                            Row = Convert.ToInt32(drPartnerProduct["Row"], CultureInfo.InvariantCulture),
                        });
                    }
                    drPartnerProduct.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerProduct != null && !drPartnerProduct.IsClosed)
                {
                    drPartnerProduct.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectTemplate;
        }


        /// <summary>
        /// Gets Categories For Become Partner
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="brandId"></param>
        /// <param name="propertyTypeId"></param>
        /// <param name="regionId"></param>
        /// <returns>ProjectTemplate</returns>
        public static ProjectTemplate GetCategoriesForBecomePartner(int categoryId, int brandId, int regionId, int CountryId)
        {
            ProjectTemplate projectTemplate = new ProjectTemplate();
            projectTemplate.Categories = new List<Category>();
            IDataReader drProjectTemplateDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetCategoriesForBecomePartner))
                {
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, categoryId);
                    db.AddInParameter(dbCommand, "@brandId", DbType.Int32, brandId);
                    //db.AddInParameter(dbCommand, "@propertyTypeId", DbType.Int32, propertyTypeId);
                    db.AddInParameter(dbCommand, "@regionId", DbType.Int32, regionId);
                    db.AddInParameter(dbCommand, "@CountryId", DbType.Int32, CountryId);

                    drProjectTemplateDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProjectTemplateDetails.Read())
                    {
                        projectTemplate.Categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drProjectTemplateDetails["CatId"], CultureInfo.InvariantCulture),
                            CategoryName = drProjectTemplateDetails["CatName"].ToString(),
                            Level = Convert.ToInt32(drProjectTemplateDetails["Level"], CultureInfo.InvariantCulture),
                            StandardDescription = drProjectTemplateDetails["StandardDescription"].ToString(),
                            IsLeaf = Convert.ToBoolean(drProjectTemplateDetails["IsLeaf"], CultureInfo.InvariantCulture),
                            IsActive = Convert.ToBoolean(drProjectTemplateDetails["IsActive"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drProjectTemplateDetails["ParentCatId"], CultureInfo.InvariantCulture),
                            RootParentId = Convert.ToInt32(drProjectTemplateDetails["RootParentCatId"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drProjectTemplateDetails["BrandId"], CultureInfo.InvariantCulture),
                            BrandName = drProjectTemplateDetails["BrandName"].ToString(),
                            //PropertyTypeId = Convert.ToInt32(drProjectTemplateDetails["PropertyTypeId"], CultureInfo.InvariantCulture),
                            //PropertyTypeName = drProjectTemplateDetails["PropertyTypeName"].ToString(),
                            RegionId = Convert.ToInt32(drProjectTemplateDetails["RegionId"], CultureInfo.InvariantCulture),
                            RegionName = drProjectTemplateDetails["RegionName"].ToString(),
                            CountryId = Convert.ToInt32(drProjectTemplateDetails["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = drProjectTemplateDetails["CountryName"].ToString(),
                            IsNewPartnersRequired = (bool?)Convert.ToBoolean(drProjectTemplateDetails["IsNewPartnersReq"], CultureInfo.InvariantCulture),
                        });

                        projectTemplate.BrandName = drProjectTemplateDetails["BrandName"].ToString();
                        //projectTemplate.PropertyTypeDescription = drProjectTemplateDetails["PropertyTypeName"].ToString();
                        projectTemplate.RegionDescription = drProjectTemplateDetails["RegionName"].ToString();
                    }
                    drProjectTemplateDetails.NextResult();
                    projectTemplate.TermsAndCondition = new TermsAndConditions();
                    while (drProjectTemplateDetails.Read())
                    {
                        projectTemplate.TermsAndCondition.VersionId = Convert.ToInt32(drProjectTemplateDetails["VersionId"]);
                        projectTemplate.TermsAndCondition.TermsAndConditionDesc = drProjectTemplateDetails["TermsAndCondition"].ToString();
                    }
                    drProjectTemplateDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectTemplateDetails != null && !drProjectTemplateDetails.IsClosed)
                {
                    drProjectTemplateDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return projectTemplate;
        }

        /// <summary>
        /// Gets Categories For Submit Product
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="brandId"></param>
        /// <param name="propertyTypeId"></param>
        /// <param name="regionId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static ProjectTemplate GetCategoriesForSubmitProduct(int categoryId, int brandId, int propertyTypeId, int regionId, int countryId, int partnerId)
        {
            ProjectTemplate projectTemplate = new ProjectTemplate();
            projectTemplate.Categories = new List<Category>();
            IDataReader drProjectTemplateDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetCategoriesForSubmitNewProduct))
                {
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, categoryId);
                    db.AddInParameter(dbCommand, "@brandId", DbType.Int32, brandId);
                    //db.AddInParameter(dbCommand, "@propertyTypeId", DbType.Int32, propertyTypeId);
                    db.AddInParameter(dbCommand, "@regionId", DbType.Int32, regionId);
                    db.AddInParameter(dbCommand, "@countryId", DbType.Int32, countryId);
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);

                    drProjectTemplateDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProjectTemplateDetails.Read())
                    {
                        projectTemplate.Categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drProjectTemplateDetails["CatId"], CultureInfo.InvariantCulture),
                            CategoryName = drProjectTemplateDetails["CatName"].ToString(),
                            Level = Convert.ToInt32(drProjectTemplateDetails["Level"], CultureInfo.InvariantCulture),
                            StandardDescription = drProjectTemplateDetails["StandardDescription"].ToString(),
                            IsLeaf = Convert.ToBoolean(drProjectTemplateDetails["IsLeaf"], CultureInfo.InvariantCulture),
                            IsActive = Convert.ToBoolean(drProjectTemplateDetails["IsActive"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drProjectTemplateDetails["ParentCatId"], CultureInfo.InvariantCulture),
                            RootParentId = Convert.ToInt32(drProjectTemplateDetails["RootParentCatId"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drProjectTemplateDetails["BrandId"], CultureInfo.InvariantCulture),
                            BrandName = drProjectTemplateDetails["BrandName"].ToString(),
                            RegionId = Convert.ToInt32(drProjectTemplateDetails["RegionId"], CultureInfo.InvariantCulture),
                            RegionName = drProjectTemplateDetails["RegionName"].ToString(),
                            CountryId = Convert.ToInt32(drProjectTemplateDetails["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = drProjectTemplateDetails["CountryName"].ToString(),
                            IsNewProductsReq = (bool?)Convert.ToBoolean(drProjectTemplateDetails["IsNewProductsReq"], CultureInfo.InvariantCulture),
                        });

                        projectTemplate.BrandName = drProjectTemplateDetails["BrandName"].ToString();
                        //projectTemplate.PropertyTypeDescription = drProjectTemplateDetails["PropertyTypeName"].ToString();
                        projectTemplate.RegionDescription = drProjectTemplateDetails["RegionName"].ToString();
                        projectTemplate.CountryName = drProjectTemplateDetails["CountryName"].ToString();

                    }
                    drProjectTemplateDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectTemplateDetails != null && !drProjectTemplateDetails.IsClosed)
                {
                    drProjectTemplateDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return projectTemplate;
        }

        /// <summary>
        /// Gets Categories For Submitted Products
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="brandId"></param>
        /// <param name="propertyTypeId"></param>
        /// <param name="regionId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static ProjectTemplate GetCategoriesForSubmittedProducts(int categoryId, int brandId, int propertyTypeId, int regionId, int countryId, int partnerId, int mode)
        {
            ProjectTemplate projectTemplate = new ProjectTemplate();
            projectTemplate.Categories = new List<Category>();
            IDataReader drProjectTemplateDetails = null;
            string storedProcedure = string.Empty;

            if (mode == (int)ModeEnum.Add)
                storedProcedure = DataAccessConstants.GetCategoriesForSubmittedProductsAdd;
            else
                storedProcedure = DataAccessConstants.GetCategoriesForSubmittedProductsEdit;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(storedProcedure))
                {
                    db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, categoryId);
                    db.AddInParameter(dbCommand, "@brandId", DbType.Int32, brandId);
                    //db.AddInParameter(dbCommand, "@propertyTypeId", DbType.Int32, propertyTypeId);
                    db.AddInParameter(dbCommand, "@regionId", DbType.Int32, regionId);
                    db.AddInParameter(dbCommand, "@countryId", DbType.Int32, countryId);
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);

                    drProjectTemplateDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drProjectTemplateDetails.Read())
                    {
                        projectTemplate.Categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drProjectTemplateDetails["CatId"], CultureInfo.InvariantCulture),
                            CategoryName = drProjectTemplateDetails["CatName"].ToString(),
                            Level = Convert.ToInt32(drProjectTemplateDetails["Level"], CultureInfo.InvariantCulture),
                            StandardDescription = drProjectTemplateDetails["StandardDescription"].ToString(),
                            IsLeaf = Convert.ToBoolean(drProjectTemplateDetails["IsLeaf"], CultureInfo.InvariantCulture),
                            IsActive = Convert.ToBoolean(drProjectTemplateDetails["IsActive"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drProjectTemplateDetails["ParentCatId"], CultureInfo.InvariantCulture),
                            RootParentId = Convert.ToInt32(drProjectTemplateDetails["RootParentCatId"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drProjectTemplateDetails["BrandId"], CultureInfo.InvariantCulture),
                            BrandName = drProjectTemplateDetails["BrandName"].ToString(),
                            //PropertyTypeId = Convert.ToInt32(drProjectTemplateDetails["PropertyTypeId"], CultureInfo.InvariantCulture),
                            //PropertyTypeName = drProjectTemplateDetails["PropertyTypeName"].ToString(),
                            RegionId = Convert.ToInt32(drProjectTemplateDetails["RegionId"], CultureInfo.InvariantCulture),
                            RegionName = drProjectTemplateDetails["RegionName"].ToString(),
                            CountryId = Convert.ToInt32(drProjectTemplateDetails["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = drProjectTemplateDetails["CountryName"].ToString(),
                            IsNewProductsReq = (bool?)Convert.ToBoolean(drProjectTemplateDetails["IsNewProductsReq"], CultureInfo.InvariantCulture),
                        });

                        projectTemplate.BrandName = drProjectTemplateDetails["BrandName"].ToString();
                        //projectTemplate.PropertyTypeDescription = drProjectTemplateDetails["PropertyTypeName"].ToString();
                        projectTemplate.RegionDescription = drProjectTemplateDetails["RegionName"].ToString();
                        projectTemplate.CountryName = drProjectTemplateDetails["CountryName"].ToString();
                    }
                    drProjectTemplateDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drProjectTemplateDetails != null && !drProjectTemplateDetails.IsClosed)
                {
                    drProjectTemplateDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return projectTemplate;
        }

        /// <summary>
        /// To Get Categories
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns>List of Category Entity</returns>
        public static IList<Category> GetCategories()
        {
            IList<Category> categories = new List<Category>();
            IDataReader drCategories = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetActiveCategories))
                {
                    drCategories = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCategories.Read())
                    {
                        categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drCategories["CategoryId"], CultureInfo.InvariantCulture),
                            CategoryName = Convert.ToString(drCategories["CategoryName"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drCategories["ParentCategoryId"], CultureInfo.InvariantCulture),
                            Level = Convert.ToInt32(drCategories["Level"], CultureInfo.InvariantCulture),
                            IsLeaf = Convert.ToBoolean(drCategories["IsLeaf"], CultureInfo.InvariantCulture),
                            SequenceId = Convert.ToString(drCategories["SequenceId"], CultureInfo.InvariantCulture)
                        });
                    }
                    drCategories.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drCategories != null && !drCategories.IsClosed)
                {
                    drCategories.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return categories;
        }

        /// <summary>
        ///  Get the partner account status
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="userId"></param>
        /// <returns>AccountStatus</returns>
        public static AccountStatus GetPartnerAccountStatus(int partnerId, int userId)
        {
            AccountStatus accountStatus = null; ;
            IDataReader drAccountStatusDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetAccountStatusDetails))
                {
                    db.AddInParameter(dbCommand, "@partnerId ", DbType.Int32, partnerId);
                    db.AddInParameter(dbCommand, "@userId ", DbType.Int32, userId);

                    drAccountStatusDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drAccountStatusDetails.Read())
                    {
                        accountStatus = new AccountStatus()
                        {
                            CompanyName = Convert.ToString(drAccountStatusDetails["CompanyName"], CultureInfo.InvariantCulture),
                            CompanyAddress1 = Convert.ToString(drAccountStatusDetails["CompanyAddress1"], CultureInfo.InvariantCulture),
                            CompanyAddress2 = Convert.IsDBNull(drAccountStatusDetails["CompanyAddress2"]) ? null : Convert.ToString(drAccountStatusDetails["CompanyAddress2"], CultureInfo.InvariantCulture),
                            Country = Convert.ToString(drAccountStatusDetails["CountryName"], CultureInfo.InvariantCulture),
                            City = Convert.ToString(drAccountStatusDetails["CityName"], CultureInfo.InvariantCulture),
                            State = Convert.IsDBNull(drAccountStatusDetails["StateName"]) ? null : Convert.ToString(drAccountStatusDetails["StateName"], CultureInfo.InvariantCulture),
                            StateId = Convert.IsDBNull(drAccountStatusDetails["StateId"]) ? 0 : Convert.ToInt32(drAccountStatusDetails["StateId"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.ToString(drAccountStatusDetails["ZipCode"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drAccountStatusDetails["CountryId"], CultureInfo.InvariantCulture),
                            CompanyDescription = Convert.ToString(drAccountStatusDetails["CompanyDescription"], CultureInfo.InvariantCulture),
                            WebSite = Convert.ToString(drAccountStatusDetails["WebSite"], CultureInfo.InvariantCulture),
                            PartnershipActiveDate = drAccountStatusDetails["ActivationDate"] != DBNull.Value ?
                                      (DateTime?)Convert.ToDateTime(drAccountStatusDetails["ActivationDate"], CultureInfo.InvariantCulture) : null,

                            PartnershipExpirationDate = drAccountStatusDetails["ExpriationDate"] != DBNull.Value ?
                                      (DateTime?)Convert.ToDateTime(drAccountStatusDetails["ExpriationDate"], CultureInfo.InvariantCulture) : null,

                            PartnershipTypeId = Convert.ToInt32(drAccountStatusDetails["PartnershipTypeId"], CultureInfo.InvariantCulture),
                            HiltonId = Convert.ToString(drAccountStatusDetails["HiltonId"], CultureInfo.InvariantCulture),
                            PartnershipTypeDesc = Convert.ToString(drAccountStatusDetails["PartnershipTypeDesc"], CultureInfo.InvariantCulture),
                            CompanyLogoImage = Convert.IsDBNull(drAccountStatusDetails["CompanyLogo"]) ? null : (byte[])(drAccountStatusDetails["CompanyLogo"]),
                            CompanyLogoName = Convert.IsDBNull(drAccountStatusDetails["CompanyLogoName"]) ? null : Convert.ToString(drAccountStatusDetails["CompanyLogoName"], CultureInfo.InvariantCulture),
                        };
                    }

                    drAccountStatusDetails.NextResult(); //To get the details for contacts
                    accountStatus.Contacts = new List<Contact>();

                    while (drAccountStatusDetails.Read())
                    {
                        accountStatus.Contacts.Add(new Contact()
                        {
                            ContactId = Convert.ToString(drAccountStatusDetails["ContactId"], CultureInfo.InvariantCulture),
                            ContactTypeId = Convert.ToInt32(drAccountStatusDetails["ContactTypeId"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drAccountStatusDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drAccountStatusDetails["LastName"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drAccountStatusDetails["Title"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drAccountStatusDetails["Phone"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drAccountStatusDetails["Fax"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drAccountStatusDetails["Email"], CultureInfo.InvariantCulture),
                            RegionId = Convert.IsDBNull(drAccountStatusDetails["ContactRegionId"]) ? 0 : Convert.ToInt32(drAccountStatusDetails["ContactRegionId"], CultureInfo.InvariantCulture),
                            RegionName = Convert.ToString(drAccountStatusDetails["RegionName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.IsDBNull(drAccountStatusDetails["ContactCountryId"]) ? 0 : Convert.ToInt32(drAccountStatusDetails["ContactCountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drAccountStatusDetails["CountryName"], CultureInfo.InvariantCulture)
                        });
                    }

                    drAccountStatusDetails.NextResult(); //To get the details for contacts
                    accountStatus.Countries = new List<Country>();

                    while (drAccountStatusDetails.Read())
                    {
                        accountStatus.Countries.Add(new Country()
                        {
                            CountryId = Convert.IsDBNull(drAccountStatusDetails["CountryId"]) ? 0 : Convert.ToInt32(drAccountStatusDetails["CountryId"], CultureInfo.InvariantCulture),
                            RegionId = Convert.IsDBNull(drAccountStatusDetails["RegionId"]) ? 0 : Convert.ToInt32(drAccountStatusDetails["RegionId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drAccountStatusDetails["CountryName"], CultureInfo.InvariantCulture)
                        });
                    }

                    drAccountStatusDetails.NextResult();
                    while (drAccountStatusDetails.Read())
                    {
                        accountStatus.PaymentCode = Convert.ToString(drAccountStatusDetails["PaymentCode"], CultureInfo.InvariantCulture);
                    }

                    drAccountStatusDetails.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drAccountStatusDetails != null && !drAccountStatusDetails.IsClosed)
                {
                    drAccountStatusDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return accountStatus;
        }

        /// <summary>
        /// To Get Partner Contact Details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns>Contact</returns>
        public static Contact GetPartnerContactDetails(int partnerId)
        {
            Contact contactDetails = null;
            IDataReader drContactDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerContactDetails))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    drContactDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drContactDetails.Read())
                    {
                        contactDetails = new Contact()
                        {
                            ContactId = Convert.ToString(drContactDetails["ContactId"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drContactDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drContactDetails["LastName"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drContactDetails["Email"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drContactDetails["Fax"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drContactDetails["Phone"], CultureInfo.InvariantCulture),
                            Title = Convert.ToString(drContactDetails["Title"], CultureInfo.InvariantCulture),
                        };
                    }

                    drContactDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drContactDetails != null && !drContactDetails.IsClosed)
                {
                    drContactDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contactDetails;
        }

        /// <summary>
        /// To Get Partner List
        /// </summary>
        /// <param name="partnerSearch"></param>
        /// <param name="totalRecordCount"></param>
        /// <returns>List of Partner Entity</returns>
        public static IList<Partner> GetPartnerList(PartnerSearch partnerSearch, ref int totalRecordCount)
        {
            IList<Partner> partnerCollection = new List<Partner>();
            IDataReader drPartnerList = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerList))
                {
                    if (string.IsNullOrWhiteSpace(partnerSearch.RegionId))
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, partnerSearch.RegionId);
                    if (string.IsNullOrWhiteSpace(partnerSearch.CountryId))
                        db.AddInParameter(dbCommand, "@countryID", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@countryID", DbType.Int32, partnerSearch.CountryId);
                    if (string.IsNullOrWhiteSpace(partnerSearch.BrandId))
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, partnerSearch.BrandId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.TertioryCategoryId))
                    {
                        if (String.IsNullOrWhiteSpace(partnerSearch.SubCategoryId))
                        {
                            if (String.IsNullOrWhiteSpace(partnerSearch.CategoryId))
                            {
                                db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, DBNull.Value);
                            }
                            else
                            {
                                db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.CategoryId);
                            }
                        }
                        else
                        {
                            db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.SubCategoryId);
                        }
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.TertioryCategoryId);
                    }

                    if (string.IsNullOrWhiteSpace(partnerSearch.StatusId))
                        db.AddInParameter(dbCommand, "@statusId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@statusId", DbType.Int32, partnerSearch.StatusId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.PartnershipTypeId))
                        db.AddInParameter(dbCommand, "@partnershipTypeId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@partnershipTypeId", DbType.Int32, partnerSearch.PartnershipTypeId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.IsSAMPartner))
                        db.AddInParameter(dbCommand, "@IsSAMPartner", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@IsSAMPartner", DbType.Int32, partnerSearch.IsSAMPartner);

                    if (partnerSearch.RequestDateFrom == null)
                        db.AddInParameter(dbCommand, "@requestDateFrom", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@requestDateFrom", DbType.DateTime, partnerSearch.RequestDateFrom);

                    if (partnerSearch.RequestDateTo == null)
                        db.AddInParameter(dbCommand, "@requestDateTo", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@requestDateTo", DbType.DateTime, partnerSearch.RequestDateTo);

                    if (partnerSearch.ExpirationDateFrom == null)
                        db.AddInParameter(dbCommand, "@expirationDateFrom", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@expirationDateFrom", DbType.DateTime, partnerSearch.ExpirationDateFrom);

                    if (partnerSearch.ExpirationDateTo == null)
                        db.AddInParameter(dbCommand, "@expirationDateTo", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@expirationDateTo", DbType.DateTime, partnerSearch.ExpirationDateTo);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, partnerSearch.UserId);
                    db.AddInParameter(dbCommand, "@searchExpression", DbType.String, partnerSearch.QuickSearchExpression);
                    db.AddInParameter(dbCommand, "@searchType", DbType.String, partnerSearch.SearchFlag);
                    db.AddInParameter(dbCommand, "@sortExpression", DbType.String, partnerSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int32, partnerSearch.PageSize);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, partnerSearch.SortDirection);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, partnerSearch.PageIndex);

                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    drPartnerList = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drPartnerList.Read())
                    {
                        {
                            partnerCollection.Add(new Partner()
                            {
                                PartnerId = Convert.ToString(drPartnerList["PartnerId"], CultureInfo.InvariantCulture),
                                CompanyName = Convert.ToString(drPartnerList["CompanyName"], CultureInfo.InvariantCulture),
                                PartnershipTypeDesc = Convert.ToString(drPartnerList["PartnershipTypeDesc"], CultureInfo.InvariantCulture),
                                PrimaryContactName = Convert.ToString(drPartnerList["PrimaryContactName"], CultureInfo.InvariantCulture),
                                PartnershipStatusDesc = Convert.ToString(drPartnerList["PartnershipStatusDesc"], CultureInfo.InvariantCulture),
                                PartnershipExpirationDate = drPartnerList["ExpriationDate"] != DBNull.Value ?
                                  (DateTime?)Convert.ToDateTime(drPartnerList["ExpriationDate"], CultureInfo.InvariantCulture) : null,
                                PartnershipReqSendDate = drPartnerList["RequestDate"] != DBNull.Value ?
                                    (DateTime?)Convert.ToDateTime(drPartnerList["RequestDate"], CultureInfo.InvariantCulture) : null,
                                flagDelPartner = Convert.ToString(drPartnerList["flagDelPartner"], CultureInfo.InvariantCulture),
                            }
                            );
                        }
                    }
                    drPartnerList.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);

                    drPartnerList.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerList != null && !drPartnerList.IsClosed)
                {
                    drPartnerList.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerCollection;
        }

        /// <summary>
        /// Gets Partner Search Result
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="totalRecordCount"></param>
        /// <returns>List of PartnerSearchResult Entity</returns>
        public static IList<PartnerSearchResult> GetPartnerSearchResult(PartnerSearch searchCriteria, ref int totalRecordCount)
        {
            PartnerSearchResult partnerSearchResult;
            IList<PartnerSearchResult> partnerSearchResultCollection = new List<PartnerSearchResult>();
            Partner partner;

            Contact contact;

            IList<Region> regionCollection;
            Region region;

            IList<Country> countryCollection;
            Country country;

            IList<Brand> brandCollection;
            Brand brand;

            //IList<PropertyType> propertyTypeCollection;
            //PropertyType propertyType;

            IList<Category> categoryCollection;
            Category category;

            IDataReader drPartnerSearchResult = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetRecommendedPartners))
                {
                    if (searchCriteria.RegionId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.String, searchCriteria.RegionId);

                    if (searchCriteria.BrandId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, searchCriteria.BrandId);

                    //if (searchCriteria.PropertyTypeId == DropDownConstants.DefaultValue)
                    //    db.AddInParameter(dbCommand, "@propertyTypeId", DbType.String, DBNull.Value);
                    //else
                    //    db.AddInParameter(dbCommand, "@propertyTypeId", DbType.String, searchCriteria.PropertyTypeId);

                    if (searchCriteria.CategoryId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@catId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@catId", DbType.String, searchCriteria.CategoryId);

                    if (searchCriteria.CountryId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@countryId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@countryId", DbType.String, searchCriteria.CountryId);
                    if (searchCriteria.PartnerId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@partnerId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@partnerId", DbType.String, searchCriteria.PartnerId);

                    if (searchCriteria.primary_contactId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@primary_contactId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@primary_contactId", DbType.String, searchCriteria.primary_contactId);

                    if (searchCriteria.secondary_contactId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@secondary_contactId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@secondary_contactId", DbType.String, searchCriteria.secondary_contactId);

                    if (searchCriteria.regional_contactId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@regional_contactId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regional_contactId", DbType.String, searchCriteria.regional_contactId);

                    if (searchCriteria.brandSegmentId == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@brandSegmentId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@brandSegmentId", DbType.String, searchCriteria.brandSegmentId);

                    if (searchCriteria.states == DropDownConstants.DefaultValue)
                        db.AddInParameter(dbCommand, "@stateId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@stateId", DbType.String, searchCriteria.states);



                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, searchCriteria.PageIndex);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, searchCriteria.PageSize);
                    db.AddInParameter(dbCommand, "@sortExpression", DbType.String, searchCriteria.SortExpression);
                    db.AddInParameter(dbCommand, "@sortDirection", DbType.String, searchCriteria.SortDirection);



                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);



                    dbCommand.CommandTimeout = 120;
                    drPartnerSearchResult = (IDataReader)db.ExecuteReader(dbCommand);


                    while (drPartnerSearchResult.Read())
                    {
                        var partnerCategoryColl = partnerSearchResultCollection.Where(x => x.PartnerId == Convert.ToInt32(drPartnerSearchResult["PartnerId"], CultureInfo.InvariantCulture));
                        if (partnerCategoryColl.Count() == 0)
                        {
                            partner = new Partner()
                            {
                                PartnerId = Convert.ToString(drPartnerSearchResult["PartnerId"], CultureInfo.InvariantCulture),
                                CompanyName = Convert.ToString(drPartnerSearchResult["CompanyName"], CultureInfo.InvariantCulture),
                                WebSiteLink = Convert.ToString(drPartnerSearchResult["WebSiteLink"], CultureInfo.InvariantCulture),
                            };

                            if (drPartnerSearchResult["CompanyLogo"] != DBNull.Value)
                                partner.CompanyLogoImage = (Byte[])(drPartnerSearchResult["CompanyLogo"]);
                            partner.IsSAM = (bool)(drPartnerSearchResult["SAM"]);

                            contact = new Contact()
                            {
                                FirstName = Convert.ToString(drPartnerSearchResult["FirstName"], CultureInfo.InvariantCulture),
                                LastName = Convert.ToString(drPartnerSearchResult["LastName"], CultureInfo.InvariantCulture),
                                Phone = Convert.ToString(drPartnerSearchResult["Phone"], CultureInfo.InvariantCulture),
                                Email = Convert.ToString(drPartnerSearchResult["Email"], CultureInfo.InvariantCulture),
                                Title = Convert.ToString(drPartnerSearchResult["Title"], CultureInfo.InvariantCulture),
                            };


                            regionCollection = new List<Region>();
                            region = new Region()
                            {
                                RegionDescription = Convert.ToString(drPartnerSearchResult["RegionName"], CultureInfo.InvariantCulture),
                            };
                            regionCollection.Add(region);


                            countryCollection = new List<Country>();
                            country = new Country()
                            {
                                CountryName = Convert.ToString(drPartnerSearchResult["CountryName"], CultureInfo.InvariantCulture),
                            };
                            countryCollection.Add(country);

                            brandCollection = new List<Brand>();
                            brand = new Brand()
                            {
                                BrandDescription = Convert.ToString(drPartnerSearchResult["BrandName"], CultureInfo.InvariantCulture),
                            };
                            brandCollection.Add(brand);

                            categoryCollection = new List<Category>();
                            category = new Category()
                            {
                                CategoryName = Convert.ToString(drPartnerSearchResult["CatName"], CultureInfo.InvariantCulture),
                            };
                            categoryCollection.Add(category);

                            partnerSearchResult = new PartnerSearchResult()
                            {
                                PartnerId = Convert.ToInt32(drPartnerSearchResult["PartnerId"], CultureInfo.InvariantCulture),
                                Brands = brandCollection,
                                Region = regionCollection,
                                Country = countryCollection,
                                //  PropertyType = propertyTypeCollection,
                                Contact = contact,
                                Partner = partner,
                                Categories = categoryCollection,
                            };

                            partnerSearchResultCollection.Add(partnerSearchResult);
                        }
                        else
                        {
                            var partnerResultCollection = partnerSearchResultCollection.Where(x => x.PartnerId == Convert.ToInt32(drPartnerSearchResult["PartnerId"], CultureInfo.InvariantCulture)).ToList();
                            if (partnerResultCollection.Count > 0)
                            {
                                partnerResultCollection.ForEach(x =>
                                {
                                    var partnerBrandCollection = x.Brands.Where(z => z.BrandDescription == Convert.ToString(drPartnerSearchResult["BrandName"], CultureInfo.InvariantCulture));
                                    if (partnerBrandCollection.Count() == 0)
                                    {
                                        brand = new Brand()
                                        {
                                            BrandDescription = Convert.ToString(drPartnerSearchResult["BrandName"], CultureInfo.InvariantCulture),
                                        };
                                        x.Brands.Add(brand);
                                    }

                                    var partnerRegionCollection = x.Region.Where(z => z.RegionDescription == Convert.ToString(drPartnerSearchResult["RegionName"], CultureInfo.InvariantCulture));
                                    if (partnerRegionCollection.Count() == 0)
                                    {
                                        region = new Region()
                                        {
                                            RegionDescription = Convert.ToString(drPartnerSearchResult["RegionName"], CultureInfo.InvariantCulture),
                                        };
                                        x.Region.Add(region);
                                    }
                                    var partnerCountryCollection = x.Country.Where(z => z.CountryName == Convert.ToString(drPartnerSearchResult["CountryName"], CultureInfo.InvariantCulture));
                                    if (partnerCountryCollection.Count() == 0)
                                    {
                                        country = new Country()
                                        {
                                            CountryName = Convert.ToString(drPartnerSearchResult["CountryName"], CultureInfo.InvariantCulture),
                                        };
                                        x.Country.Add(country);
                                    }

                                    var partnerCategoriesCollection = x.Categories.Where(z => z.CategoryName == Convert.ToString(drPartnerSearchResult["CatName"], CultureInfo.InvariantCulture));
                                    if (partnerCategoriesCollection.Count() == 0)
                                    {
                                        category = new Category()
                                        {
                                            CategoryName = Convert.ToString(drPartnerSearchResult["CatName"], CultureInfo.InvariantCulture),
                                        };
                                        x.Categories.Add(category);
                                    }
                                });
                            }
                        }
                    }
                    drPartnerSearchResult.NextResult();
                    totalRecordCount = Convert.ToInt16(retValParam.Value, CultureInfo.InvariantCulture);
                    drPartnerSearchResult.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPartnerSearchResult != null && !drPartnerSearchResult.IsClosed)
                {
                    drPartnerSearchResult.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerSearchResultCollection;
        }

        /// <summary>
        /// Gets Payment Gateway Parameters
        /// </summary>
        /// <returns></returns>
        public static PaymentGatewayParameters GetPaymentGatewayParameters()
        {
            PaymentGatewayParameters parameters = null;
            IDataReader drPaymentGParameters = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerContactDetails))
                {
                    drPaymentGParameters = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drPaymentGParameters.Read())
                    {
                        parameters = new PaymentGatewayParameters()
                        {
                            HiltonMerchantId = Convert.ToInt32(drPaymentGParameters["HiltonMerchantId"], CultureInfo.InvariantCulture),
                            HiltonUserId = Convert.ToInt32(drPaymentGParameters["HiltonUserId"], CultureInfo.InvariantCulture),
                            Pin = Convert.ToString(drPaymentGParameters["Pin"], CultureInfo.InvariantCulture),
                            TransactionType = Convert.ToString(drPaymentGParameters["TransactionType"], CultureInfo.InvariantCulture),
                            ShowForm = Convert.ToBoolean(drPaymentGParameters["ShowForm"], CultureInfo.InvariantCulture),
                        };
                    }
                    drPaymentGParameters.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPaymentGParameters != null && !drPaymentGParameters.IsClosed)
                {
                    drPaymentGParameters.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return parameters;
        }

        /// <summary>
        /// Gets Payment Request Amounts
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataSet GetPaymentRequestAmounts(int paymentRequestId)
        {
            DataSet dsPaymentAccount = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPaymentRequestAmounts))
                {
                    db.AddInParameter(dbCommand, "@paymentRequestId", DbType.Int32, paymentRequestId);
                    using (dsPaymentAccount = new DataSet())
                    {
                        dsPaymentAccount = db.ExecuteDataSet(dbCommand);
                    }
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dsPaymentAccount;
        }

        /// <summary>
        /// Gets Search Filter Data
        /// </summary>
        /// <returns></returns>
        public static CategoriesFilterHierarchy GetPartnershipOpportunitiesSearchFilterData(int userId)
        {
            CategoriesFilterHierarchy searchFilters = new CategoriesFilterHierarchy();
            IDataReader drSearchFilter = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnershipOpportunitiesFilterData))
                {
                    dbCommand.CommandTimeout = 120;
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    searchFilters.FilteredCBRTData = new List<SearchFilters>();
                    while (drSearchFilter.Read())
                    {
                        searchFilters.FilteredCBRTData.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            CatId = Convert.ToInt32(drSearchFilter["RootCatId"], CultureInfo.InvariantCulture),
                            CategoryDescription = Convert.ToString(drSearchFilter["RootCatName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drSearchFilter["CountryName"], CultureInfo.InvariantCulture)
                        }

                      );
                    }
                    drSearchFilter.NextResult();
                    searchFilters.FilteredHierarchyCBRTData = new List<SearchFilters>();
                    while (drSearchFilter.Read())
                    {
                        searchFilters.FilteredHierarchyCBRTData.Add(new SearchFilters()
                        {
                            CatId = Convert.ToInt32(drSearchFilter["CatId"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drSearchFilter["CountryName"], CultureInfo.InvariantCulture),
                        }
                    );
                    }

                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return searchFilters;
        }

        /// <summary>
        /// Gets Search Filter Data For Vendor
        /// </summary>
        /// <returns></returns>
        public static CategoriesFilterHierarchy GetSearchFilterDataForVendor(int userId)
        {
            CategoriesFilterHierarchy searchFilters = new CategoriesFilterHierarchy();
            IDataReader drSearchFilter = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetRecommendedVendorsFilterData))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    searchFilters.FilteredCBRTData = new List<SearchFilters>();
                    while (drSearchFilter.Read())
                    {
                        searchFilters.FilteredCBRTData.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            CatId = Convert.ToInt32(drSearchFilter["RootCatId"], CultureInfo.InvariantCulture),
                            CategoryDescription = Convert.ToString(drSearchFilter["RootCatName"], CultureInfo.InvariantCulture),

                        }
                    );
                    }

                    drSearchFilter.NextResult();
                    searchFilters.FilteredHierarchyCBRTData = new List<SearchFilters>();
                    while (drSearchFilter.Read())
                    {
                        searchFilters.FilteredHierarchyCBRTData.Add(new SearchFilters()
                        {
                            CatId = Convert.ToInt32(drSearchFilter["CatId"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                        }
                    );
                    }

                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        ///  Get the categories for filter
        /// </summary>
        /// <returns>List of Category Entity</returns>
        public static IList<Category> GetRecommendedVendorsActiveCategories()
        {
            IList<Category> categories = new List<Category>();
            IDataReader drCategories = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetRecommendedVendorsActiveCategories))
                {
                    drCategories = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCategories.Read())
                    {
                        categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drCategories["CategoryId"], CultureInfo.InvariantCulture),
                            CategoryName = Convert.ToString(drCategories["CategoryName"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drCategories["ParentCategoryId"], CultureInfo.InvariantCulture),
                            Level = Convert.ToInt32(drCategories["Level"], CultureInfo.InvariantCulture),
                            IsLeaf = Convert.ToBoolean(drCategories["IsLeaf"], CultureInfo.InvariantCulture),
                            SequenceId = drCategories["SequenceId"].ToString(),
                            ParentCategoryHierarchy = Convert.ToString(drCategories["ParentCategoryHierarchy"], CultureInfo.InvariantCulture),
                        });
                    }
                }
                drCategories.Close();
            }
            catch (Exception exception)
            {
                if (drCategories != null && !drCategories.IsClosed)
                {
                    drCategories.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return categories;
        }

        /// <summary>
        ///  Get the categories for filter
        /// </summary>
        /// <returns>List of Category Entity</returns>
        public static IList<Category> GetAddEditProductActiveCategories(int partnerId, int mode)
        {
            IList<Category> categories = new List<Category>();
            IDataReader drCategories = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetAddEditProductActiveCategories))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    db.AddInParameter(dbCommand, "@mode", DbType.Int32, mode);
                    drCategories = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCategories.Read())
                    {
                        categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drCategories["CategoryId"], CultureInfo.InvariantCulture),
                            CategoryName = Convert.ToString(drCategories["CategoryName"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drCategories["ParentCategoryId"], CultureInfo.InvariantCulture),
                            Level = Convert.ToInt32(drCategories["Level"], CultureInfo.InvariantCulture),
                            IsLeaf = Convert.ToBoolean(drCategories["IsLeaf"], CultureInfo.InvariantCulture),
                            SequenceId = drCategories["SequenceId"].ToString(),
                            ParentCategoryHierarchy = Convert.ToString(drCategories["ParentCategoryHierarchy"], CultureInfo.InvariantCulture),
                        });
                    }
                }
                drCategories.Close();
            }
            catch (Exception exception)
            {
                if (drCategories != null && !drCategories.IsClosed)
                {
                    drCategories.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return categories;
        }

        /// <summary>
        ///  Get the categories for filter on Become a Partner
        /// </summary>
        /// <returns>List of Category Entity</returns>
        public static IList<Category> GetBecomePartnerCategories()
        {
            IList<Category> categories = new List<Category>();
            IDataReader drCategories = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetBecomePartnerCategories))
                {
                    drCategories = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drCategories.Read())
                    {
                        categories.Add(new Category()
                        {
                            CategoryId = Convert.ToInt32(drCategories["CategoryId"], CultureInfo.InvariantCulture),
                            CategoryName = Convert.ToString(drCategories["CategoryName"], CultureInfo.InvariantCulture),
                            ParentCategoryId = Convert.ToInt32(drCategories["ParentCategoryId"], CultureInfo.InvariantCulture),
                            Level = Convert.ToInt32(drCategories["Level"], CultureInfo.InvariantCulture),
                            IsLeaf = Convert.ToBoolean(drCategories["IsLeaf"], CultureInfo.InvariantCulture),
                            SequenceId = drCategories["SequenceId"].ToString(),
                            ParentCategoryHierarchy = Convert.ToString(drCategories["ParentCategoryHierarchy"], CultureInfo.InvariantCulture),
                        });
                    }
                }
                drCategories.Close();
            }
            catch (Exception exception)
            {
                if (drCategories != null && !drCategories.IsClosed)
                {
                    drCategories.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return categories;
        }

        /// <summary>
        /// Gets Partner Profile Search Filter Data
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetPartnerProfileSearchFilterData(int partnerId)
        {
            IList<SearchFilters> searchContacts = new List<SearchFilters>();
            IDataReader drSearchContact = null;
            try
            {
                Partner partner = null;
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerProfileDetails))
                {
                    db.AddInParameter(dbCommand, "@PartnerId", DbType.Int32, partnerId);
                    //change start -added this because 4 new parameters have been added to GetPartnerProfileDetails procedure
                    db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    // db.AddInParameter(dbCommand, "@propertyTypeId", DbType.String, DBNull.Value);
                    db.AddInParameter(dbCommand, "@regionId", DbType.String, DBNull.Value);
                    db.AddInParameter(dbCommand, "@countryId", DbType.String, DBNull.Value);
                    db.AddInParameter(dbCommand, "@catId", DbType.String, DBNull.Value);
                    //change end

                    drSearchContact = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drSearchContact.Read())
                    {
                        partner = new Partner()
                        {
                            CompanyLogoImage = Convert.IsDBNull(drSearchContact["CompanyLogo"]) ? null : (byte[])(drSearchContact["CompanyLogo"]),
                            CompanyName = Convert.ToString(drSearchContact["CompanyName"], CultureInfo.InvariantCulture),
                            CompanyAddress1 = Convert.ToString(drSearchContact["CompanyAddress1"], CultureInfo.InvariantCulture),
                            CompanyAddress2 = Convert.ToString(drSearchContact["CompanyAddress2"], CultureInfo.InvariantCulture),
                            CompanyDescription = Convert.ToString(drSearchContact["CompanyDescription"], CultureInfo.InvariantCulture),
                            IsSAM = Convert.ToBoolean(drSearchContact["Sam"], CultureInfo.InvariantCulture),
                        };
                    }

                    drSearchContact.NextResult(); //To get the details for primary contact
                    partner.Contacts = new List<Contact>();

                    while (drSearchContact.Read())
                    {
                        searchContacts.Add(new SearchFilters()
                        {
                            ContactTypeId = Convert.ToInt32(drSearchContact["ContactRegionId"], CultureInfo.InvariantCulture),
                            ContactTypeDesc = Convert.ToString(drSearchContact["ContactTypeDesc"], CultureInfo.InvariantCulture)
                        });
                    }
                    drSearchContact.NextResult();

                    while (drSearchContact.Read())
                    {
                        searchContacts.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchContact["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchContact["BrandName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchContact["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchContact["RegionName"], CultureInfo.InvariantCulture)
                        });
                    }
                    drSearchContact.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchContact != null && !drSearchContact.IsClosed)
                {
                    drSearchContact.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchContacts;
        }

        /// <summary>
        /// Get Submit Product Search Filter Data
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static CategoriesFilterHierarchy GetSubmitProductSearchFilterData(int partnerId, int mode)
        {
            CategoriesFilterHierarchy searchFilters = new CategoriesFilterHierarchy();
            IDataReader drSearchFilter = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetSubmitProductSearchFilterData))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    db.AddInParameter(dbCommand, "@mode", DbType.Int32, mode);
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    searchFilters.FilteredCBRTData = new List<SearchFilters>();
                    while (drSearchFilter.Read())
                    {
                        searchFilters.FilteredCBRTData.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            CatId = Convert.ToInt32(drSearchFilter["RootCatId"], CultureInfo.InvariantCulture),
                            CategoryDescription = Convert.ToString(drSearchFilter["RootCatName"], CultureInfo.InvariantCulture),
                            //PropertyTypeId = Convert.ToInt32(drSearchFilter["PropertyTypeId"], CultureInfo.InvariantCulture),
                            //PropertyTypeDescription = Convert.ToString(drSearchFilter["PropertyTypeName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drSearchFilter["CountryName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture)
                        }

                      );
                    }

                    drSearchFilter.NextResult();
                    searchFilters.FilteredHierarchyCBRTData = new List<SearchFilters>();
                    while (drSearchFilter.Read())
                    {
                        searchFilters.FilteredHierarchyCBRTData.Add(new SearchFilters()
                        {
                            CatId = Convert.ToInt32(drSearchFilter["CatId"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture),
                            //PropertyTypeId = Convert.ToInt32(drSearchFilter["PropertyTypeId"], CultureInfo.InvariantCulture),
                            //PropertyTypeDescription = Convert.ToString(drSearchFilter["PropertyTypeName"], CultureInfo.InvariantCulture),
                        }
                    );
                    }
                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return searchFilters;
        }

        /// <summary>
        /// Get My Product Search Filter Data
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetMyProductSearchFilterData(int partnerId)
        {
            IList<SearchFilters> searchFilters = new List<SearchFilters>();
            IDataReader drSearchFilter = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetMyProductSearchFilterData))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drSearchFilter.Read())
                    {
                        searchFilters.Add(new SearchFilters()
                        {
                            BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture),
                            BrandDescription = Convert.ToString(drSearchFilter["BrandName"], CultureInfo.InvariantCulture),
                            CatId = Convert.ToInt32(drSearchFilter["CatId"], CultureInfo.InvariantCulture),
                            CategoryDescription = Convert.ToString(drSearchFilter["CatName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture),
                            RegionDescription = Convert.ToString(drSearchFilter["RegionName"], CultureInfo.InvariantCulture),
                            CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drSearchFilter["CountryName"], CultureInfo.InvariantCulture)
                        }
                      );
                    }
                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return searchFilters;
        }

        /// <summary>
        /// Link the web account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public static IList<PartnerLinkedWebAccount> LinkedWebAccount(PartnerLinkedWebAccount account, ref int result)
        {
            IDataReader drLinkedWebAccountDetails = null;
            IList<PartnerLinkedWebAccount> webAccounts = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.LinkWebAccount))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, account.PartnerId);
                    db.AddInParameter(dbCommand, "@firstName", DbType.String, account.FirstName);
                    db.AddInParameter(dbCommand, "@lastName", DbType.String, account.LastName);
                    db.AddInParameter(dbCommand, "@culture", DbType.String, account.Culture);
                    if (string.IsNullOrWhiteSpace(account.HiltonId))
                    {
                        db.AddInParameter(dbCommand, "@hiltonId", DbType.String, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@hiltonId", DbType.String, account.HiltonId);
                    }
                    db.AddInParameter(dbCommand, "@email", DbType.String, account.Email);
                    db.AddInParameter(dbCommand, "@createdByUserId", DbType.String, account.CreatedByUserId);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    drLinkedWebAccountDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    webAccounts = new List<PartnerLinkedWebAccount>();
                    while (drLinkedWebAccountDetails.Read())
                    {
                        webAccounts.Add(new PartnerLinkedWebAccount()
                        {
                            HiltonId = Convert.ToString(drLinkedWebAccountDetails["HiltonId"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drLinkedWebAccountDetails["Email"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drLinkedWebAccountDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drLinkedWebAccountDetails["LastName"], CultureInfo.InvariantCulture),
                            StatusDesc = Convert.ToString(drLinkedWebAccountDetails["StatusDesc"], CultureInfo.InvariantCulture),
                            PartnerId = account.PartnerId,
                            UserId = Convert.ToInt32(drLinkedWebAccountDetails["UserId"], CultureInfo.InvariantCulture)
                        });
                    }
                    drLinkedWebAccountDetails.NextResult();
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);

                    drLinkedWebAccountDetails.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drLinkedWebAccountDetails != null && !drLinkedWebAccountDetails.IsClosed)
                {
                    drLinkedWebAccountDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return webAccounts;
        }

        /// <summary>
        /// Sends Payment Request
        /// </summary>
        /// <param name="paymentRequest"></param>
        /// <returns></returns>
        public static PaymentRequestDetails SendPaymentRequest(PaymentRequest paymentRequest)
        {
            IDataReader drPaymentRequestDetails = null;
            PaymentRequestDetails paymentRequestDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsertPaymentRequest))
                {
                    dbCommand.CommandTimeout = 300;
                    db.AddInParameter(dbCommand, "@goldAmount", DbType.Double, paymentRequest.GoldAmount);
                    db.AddInParameter(dbCommand, "@regularAmount", DbType.Double, paymentRequest.RegularAmount);
                    db.AddInParameter(dbCommand, "@sentToEmail", DbType.String, paymentRequest.RequestSentTo);
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, paymentRequest.PartnerId);
                    db.AddInParameter(dbCommand, "@culture", DbType.String, paymentRequest.Culture);
                    drPaymentRequestDetails = (IDataReader)db.ExecuteReader(dbCommand);
                    if (drPaymentRequestDetails.Read())
                    {
                        paymentRequestDetails = new PaymentRequestDetails()
                        {
                            RequestSentTo = Convert.ToString(drPaymentRequestDetails["ToEmail"], CultureInfo.InvariantCulture),
                            RequestSentOn = Convert.ToDateTime(drPaymentRequestDetails["RequestDate"], CultureInfo.InvariantCulture),
                        };
                    }
                    drPaymentRequestDetails.Close();
                }
            }
            catch (Exception exception)
            {
                if (drPaymentRequestDetails != null && !drPaymentRequestDetails.IsClosed)
                {
                    drPaymentRequestDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return paymentRequestDetails;
        }

        /// <summary>
        /// Submits Partner Application
        /// </summary>
        /// <param name="partnerApplication"></param>
        /// <returns></returns>
        public static bool SubmitPartnerApplication(PartnerApplication partnerApplication)
        {
            IDataReader drPartnerApplicationDetails = null;
            try
            {
                using (DataTable dtContacts = ConvertToContacts(partnerApplication.Contacts))
                {
                    using (DataTable dtPartnerships = ConvertToPartnerships(partnerApplication.AppliedParntershipOpportunities))
                    {
                        string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                        using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.InsertPartnerApplicationDetails))
                        {
                            dbCommand.Parameters.Add(new SqlParameter("@partnershipType", partnerApplication.PartnershipType));
                            dbCommand.Parameters.Add(new SqlParameter("@partnershipAmount", partnerApplication.PartnerShipAmount));
                            dbCommand.Parameters.Add(new SqlParameter("@companyName", partnerApplication.CompanyName));
                            dbCommand.Parameters.Add(new SqlParameter("@webSiteLink", partnerApplication.WebSiteLink));
                            dbCommand.Parameters.Add(new SqlParameter("@companyLogoName", partnerApplication.CompanyLogoName));
                            dbCommand.Parameters.Add(new SqlParameter("@companyLogo", partnerApplication.CompanyLogoImage));
                            dbCommand.Parameters.Add(new SqlParameter("@companyLogoThumbnail", partnerApplication.CompanyLogoImageThumbnail));
                            dbCommand.Parameters.Add(new SqlParameter("@companyAddress1", partnerApplication.CompanyAddress1));
                            dbCommand.Parameters.Add(new SqlParameter("@companyAddress2", partnerApplication.CompanyAddress2));
                            dbCommand.Parameters.Add(new SqlParameter("@countryID", partnerApplication.Country));
                            dbCommand.Parameters.Add(new SqlParameter("@cityName", partnerApplication.City));

                            if (partnerApplication.UserId != 0)
                            {
                                db.AddInParameter(dbCommand, "@userId", DbType.Int32, partnerApplication.UserId);
                            }
                            else
                            {
                                db.AddInParameter(dbCommand, "@userId", DbType.Int32, null);
                            }

                            if (partnerApplication.State != "0")
                            {
                                db.AddInParameter(dbCommand, "@stateId", DbType.Int32, partnerApplication.State);
                            }
                            else
                            {
                                db.AddInParameter(dbCommand, "@stateId", DbType.Int32, null);
                            }
                            if (string.IsNullOrWhiteSpace(partnerApplication.ZipCode))
                            {
                                db.AddInParameter(dbCommand, "@zipCode", DbType.String, null);
                            }
                            else
                            {
                                db.AddInParameter(dbCommand, "@zipCode", DbType.String, partnerApplication.ZipCode);
                            }
                            dbCommand.Parameters.Add(new SqlParameter("@companyDescription", partnerApplication.CompanyDescription));
                            dbCommand.Parameters.Add(new SqlParameter("@partnerContacts", dtContacts));
                            dbCommand.Parameters.Add(new SqlParameter("@partnershipCategories", dtPartnerships));

                            if (partnerApplication.TermsAndCondition != null)
                            {
                                dbCommand.Parameters.Add(new SqlParameter("@ipAddress", partnerApplication.TermsAndCondition.IPAddress));
                                dbCommand.Parameters.Add(new SqlParameter("@termsAndConditionVersionid", partnerApplication.TermsAndCondition.VersionId));
                                dbCommand.Parameters.Add(new SqlParameter("@termsAndCondtionDesc", partnerApplication.TermsAndCondition.TermsAndConditionDesc));
                            }

                            drPartnerApplicationDetails = (IDataReader)db.ExecuteReader(dbCommand);

                            drPartnerApplicationDetails.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                if (drPartnerApplicationDetails != null && !drPartnerApplicationDetails.IsClosed)
                {
                    drPartnerApplicationDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return true;
        }

        /// <summary>
        /// Updates Partner Payment Details
        /// </summary>
        /// <param name="paymentForm"></param>
        /// <returns></returns>
        public static int UpdatePartnerPaymentDetails(PartnerPayment partnerPayment)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdatePartnerPaymentDetails))
                {
                    db.AddInParameter(dbCommand, "@paymentRequestId", DbType.Int32, partnerPayment.PaymentRequestId);

                    if (string.IsNullOrWhiteSpace(partnerPayment.PaymentType))
                        db.AddInParameter(dbCommand, "@paymentType", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@paymentType", DbType.Int32, partnerPayment.PaymentType);

                    if (partnerPayment.PartnershipType == 0)
                        db.AddInParameter(dbCommand, "@paidForPartnershipType", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@paidForPartnershipType", DbType.Int32, partnerPayment.PartnershipType);

                    if (string.IsNullOrWhiteSpace(partnerPayment.RequestType))
                        db.AddInParameter(dbCommand, "@requestType", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@requestType", DbType.String, partnerPayment.RequestType);

                    if (string.IsNullOrWhiteSpace(partnerPayment.TransactionId))
                        db.AddInParameter(dbCommand, "@transactionId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@transactionId", DbType.String, partnerPayment.TransactionId);

                    if (string.IsNullOrWhiteSpace(partnerPayment.TransactionStatus))
                        db.AddInParameter(dbCommand, "@transactionStatus", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@transactionStatus", DbType.String, partnerPayment.TransactionStatus);

                    if (string.IsNullOrWhiteSpace(partnerPayment.TransactionApprovalCode))
                        db.AddInParameter(dbCommand, "@transactionApprovalCode", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@transactionApprovalCode", DbType.String, partnerPayment.TransactionApprovalCode);

                    if (string.IsNullOrWhiteSpace(partnerPayment.ErrorCode))
                        db.AddInParameter(dbCommand, "@transactionErrorCode", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@transactionErrorCode", DbType.String, partnerPayment.ErrorCode);

                    if (string.IsNullOrWhiteSpace(partnerPayment.ErrorMessage))
                        db.AddInParameter(dbCommand, "@errorMessage", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@errorMessage", DbType.String, partnerPayment.ErrorMessage);

                    if (string.IsNullOrWhiteSpace(partnerPayment.ErrorName))
                        db.AddInParameter(dbCommand, "@errorName", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@errorName", DbType.String, partnerPayment.ErrorName);

                    if (string.IsNullOrWhiteSpace(partnerPayment.CardNumber))
                        db.AddInParameter(dbCommand, "@cardNumber", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@cardNumber", DbType.String, partnerPayment.CardNumber);

                    if (string.IsNullOrWhiteSpace(partnerPayment.Result))
                        db.AddInParameter(dbCommand, "@transactionResult", DbType.Int16, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@transactionResult", DbType.Int16, Convert.ToInt16(partnerPayment.Result, CultureInfo.InvariantCulture));

                    if (string.IsNullOrWhiteSpace(partnerPayment.AmountPaid))
                        db.AddInParameter(dbCommand, "@amountPaid", DbType.Decimal, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@amountPaid", DbType.Decimal, partnerPayment.AmountPaid);

                    if (string.IsNullOrWhiteSpace(partnerPayment.AmountRequested))
                        db.AddInParameter(dbCommand, "@amountRequested", DbType.Decimal, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@amountRequested", DbType.Decimal, partnerPayment.AmountRequested);

                    if (partnerPayment.TransactionDate == null)
                        db.AddInParameter(dbCommand, "@transactionDate", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@transactionDate", DbType.DateTime, partnerPayment.TransactionDate);
                    db.AddInParameter(dbCommand, "@culture", DbType.String, partnerPayment.Culture);

                    SqlParameter contact = new SqlParameter("@partnerBillingContact", SqlDbType.Structured);
                    contact.TypeName = "dbo.PartnerContactType";
                    contact.Direction = ParameterDirection.Input;
                    contact.Value = ConvertToContactsForPayment(partnerPayment.Contacts);
                    dbCommand.Parameters.Add(contact);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return result;
        }

        /// <summary>
        /// Updates Partner Application
        /// </summary>
        /// <param name="partnerApplication"></param>
        /// <returns></returns>
        public static bool UpdatePartnerApplication(PartnerApplication partnerApplication)
        {
            DataTable dtContacts = ConvertToContacts(partnerApplication.Contacts);
            DataTable dtPartnerships = ConvertToPartnerships(partnerApplication.AppliedParntershipOpportunities);

            IDataReader drPartnerApplicationDetails;

            string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
            DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdatePartnerApplicationDetails);
            dbCommand.Parameters.Add(new SqlParameter("@PartnerId", partnerApplication.PartnerId));
            dbCommand.Parameters.Add(new SqlParameter("@PartnershipType", partnerApplication.PartnershipType));
            dbCommand.Parameters.Add(new SqlParameter("@PartnershipAmount", partnerApplication.PartnerShipAmount));
            dbCommand.Parameters.Add(new SqlParameter("@CompanyName", partnerApplication.CompanyName));
            dbCommand.Parameters.Add(new SqlParameter("@CompanyLogoName", partnerApplication.CompanyLogoName));
            dbCommand.Parameters.Add(new SqlParameter("@CompanyLogo", partnerApplication.CompanyLogoImage));
            dbCommand.Parameters.Add(new SqlParameter("@CompanyAddress1", partnerApplication.CompanyAddress1));
            dbCommand.Parameters.Add(new SqlParameter("@CompanyAddress2", partnerApplication.CompanyAddress2));
            dbCommand.Parameters.Add(new SqlParameter("@CountryId", partnerApplication.Country));
            dbCommand.Parameters.Add(new SqlParameter("@CityName", partnerApplication.City));

            if (partnerApplication.State != "0")
            {
                db.AddInParameter(dbCommand, "@StateId", DbType.Int32, partnerApplication.State);
            }
            else
            {
                db.AddInParameter(dbCommand, "@StateId", DbType.Int32, null);
            }
            if (string.IsNullOrWhiteSpace(partnerApplication.ZipCode))
            {
                db.AddInParameter(dbCommand, "@ZipCode", DbType.String, null);
            }
            else
            {
                db.AddInParameter(dbCommand, "@ZipCode", DbType.String, partnerApplication.ZipCode);
            }
            dbCommand.Parameters.Add(new SqlParameter("@CompanyDescription", partnerApplication.CompanyDescription));
            dbCommand.Parameters.Add(new SqlParameter("@partnerContacts", dtContacts));
            dbCommand.Parameters.Add(new SqlParameter("@partnershipCategories", dtPartnerships));

            if (partnerApplication.UserId != 0)
            {
                db.AddInParameter(dbCommand, "@userId", DbType.Int32, partnerApplication.UserId);
            }
            else
            {
                db.AddInParameter(dbCommand, "@userId", DbType.Int32, null);
            }

            if (partnerApplication.TermsAndCondition != null)
            {
                dbCommand.Parameters.Add(new SqlParameter("@ipAddress", partnerApplication.TermsAndCondition.IPAddress));
                dbCommand.Parameters.Add(new SqlParameter("@termsAndConditionVersionid", partnerApplication.TermsAndCondition.VersionId));
                dbCommand.Parameters.Add(new SqlParameter("@termsAndCondtionDesc", partnerApplication.TermsAndCondition.TermsAndConditionDesc));
            }

            drPartnerApplicationDetails = (IDataReader)db.ExecuteReader(dbCommand);

            return true;
        }
        /// <summary>
        /// Updates Partner Payment Details
        /// </summary>
        /// <param name="paymentForm"></param>
        /// <returns></returns>
        public static void LogErrorPartnerPayment(PartnerPaymentLogError partnerPaymentErrorLog)
        {
            // int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.LogErrorPaymentDetails))
                {
                    db.AddInParameter(dbCommand, "@ErrorCodeRequestQueryStr", DbType.String, partnerPaymentErrorLog.errorCodeRequestQueryStr);
                    db.AddInParameter(dbCommand, "@errorNameRequestQueryStr", DbType.String, partnerPaymentErrorLog.errorNameRequestQueryStr);
                    db.AddInParameter(dbCommand, "@errorMessageRequestQueryStr", DbType.String, partnerPaymentErrorLog.errorMessageRequestQueryStr);
                    db.AddInParameter(dbCommand, "@transactionStatus", DbType.String, partnerPaymentErrorLog.transactionStatus);
                    db.AddInParameter(dbCommand, "@transactionId", DbType.String, partnerPaymentErrorLog.transactionId);
                    db.AddInParameter(dbCommand, "@transactionApprovalCode", DbType.String, partnerPaymentErrorLog.transactionApprovalCode);
                    db.AddInParameter(dbCommand, "@errorCode", DbType.String, partnerPaymentErrorLog.errorCode);
                    db.AddInParameter(dbCommand, "@errorName", DbType.String, partnerPaymentErrorLog.errorName);
                    db.AddInParameter(dbCommand, "@errorMessage", DbType.String, partnerPaymentErrorLog.errorMessage);
                    db.AddInParameter(dbCommand, "@amount", DbType.String, partnerPaymentErrorLog.amount);
                    db.AddInParameter(dbCommand, "@cardNumber", DbType.String, partnerPaymentErrorLog.cardNumber);
                    db.AddInParameter(dbCommand, "@paymentRequestId", DbType.Int32, partnerPaymentErrorLog.paymentRequestId);

                    db.ExecuteNonQuery(dbCommand);
                    //result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            // return result;
        }
        /// <summary>
        /// Updates Partner
        /// </summary>
        /// <param name="partner"></param>
        /// <param name="partnerCategories"></param>
        /// <returns></returns>
        public static int UpdatePartner(Partner partner, IList<PartnerCategories> partnerCategories)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdatePartnerDetails))
                {
                    db.AddInParameter(dbCommand, "@companyName", DbType.String, partner.CompanyName);
                    db.AddInParameter(dbCommand, "@webSiteLink", DbType.String, partner.WebSiteLink);
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partner.PartnerId);
                    db.AddInParameter(dbCommand, "@companyAddress1", DbType.String, partner.CompanyAddress1);
                    db.AddInParameter(dbCommand, "@companyAddress2", DbType.String, partner.CompanyAddress2);
                    db.AddInParameter(dbCommand, "@countryId", DbType.Int32, partner.CountryId);
                    db.AddInParameter(dbCommand, "@city", DbType.String, partner.City);
                    if (partner.StateId != 0)
                    {
                        db.AddInParameter(dbCommand, "@stateId", DbType.Int32, partner.StateId);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@stateId", DbType.Int32, null);
                    }
                    if (string.IsNullOrWhiteSpace(partner.ZipCode))
                    {
                        db.AddInParameter(dbCommand, "@zip", DbType.String, null);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@zip", DbType.String, partner.ZipCode);
                    }
                    db.AddInParameter(dbCommand, "@companyDescription", DbType.String, partner.CompanyDescription);
                    db.AddInParameter(dbCommand, "@IsSam", DbType.Boolean, partner.IsSAM);
                    db.AddInParameter(dbCommand, "@IsRecPartner", DbType.Boolean, partner.IsRecPartner);
                    db.AddInParameter(dbCommand, "@companyLogo", DbType.Binary, partner.CompanyLogoImage);
                    db.AddInParameter(dbCommand, "@companyLogoThumbnail", DbType.Binary, partner.CompanyLogoThumbnailImage);
                    db.AddInParameter(dbCommand, "@companyLogoName", DbType.String, partner.CompanyLogoName);
                    db.AddInParameter(dbCommand, "@partnershipStatus", DbType.Int32, partner.PartnershipStatusId);
                    db.AddInParameter(dbCommand, "@submitProducts", DbType.Boolean, partner.SubmitProducts);
                    db.AddInParameter(dbCommand, "@isAutomatedRenewalEmailReq", DbType.Boolean, partner.IsAutomatedRenewalEmailReq);
                    db.AddInParameter(dbCommand, "@partnershipType", DbType.Int32, partner.PartnershipTypeId);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, partner.UserId);

                    SqlParameter specParam = new SqlParameter("@partnerCategories", SqlDbType.Structured);

                    if (partner.ActiveDate != null)
                    {
                        db.AddInParameter(dbCommand, "@activeDate", DbType.DateTime, partner.ActiveDate);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@activeDate", DbType.DateTime, DBNull.Value);
                    }
                    if (partner.ExpirationDate != null)
                    {
                        db.AddInParameter(dbCommand, "@expirationDate", DbType.DateTime, partner.ExpirationDate);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@expirationDate", DbType.DateTime, DBNull.Value);
                    }

                    specParam.TypeName = "dbo.PartnerCategories";
                    specParam.Value = CreateSqlDataRecords(partnerCategories);
                    dbCommand.Parameters.Add(specParam);

                    SqlParameter contact = new SqlParameter("@partnerContacts", SqlDbType.Structured);
                    contact.TypeName = "dbo.PartnerContactCountryType";
                    contact.Direction = ParameterDirection.Input;
                    contact.Value = ConvertToContacts(partner.Contacts);
                    dbCommand.Parameters.Add(contact);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return result;
        }

        /// <summary>
        /// Converts To Contacts
        /// </summary>
        /// <param name="genericList"></param>
        /// <returns></returns>
        private static DataTable ConvertToContacts(IList<Contact> genericList)
        {
            using (DataTable dataTable = new DataTable("dataTableContacts"))
            {
                dataTable.Locale = CultureInfo.InvariantCulture;
                dataTable.TableName = "dataTableContacts";
                dataTable.Columns.Add("ContactType", typeof(int));
                dataTable.Columns.Add("Email", typeof(string));
                dataTable.Columns.Add("Fax", typeof(string));
                dataTable.Columns.Add("FirstName", typeof(string));
                dataTable.Columns.Add("IsSameAsPrimary", typeof(string));
                dataTable.Columns.Add("LastName", typeof(string));
                dataTable.Columns.Add("Phone", typeof(string));
                dataTable.Columns.Add("ContactRegion", typeof(int));
                dataTable.Columns.Add("ContactCountry", typeof(int));
                dataTable.Columns.Add("Title", typeof(string));
                dataTable.Columns.Add("PartnerID", typeof(int));
                dataTable.Columns.Add("ContactStatusId", typeof(int));

                if (genericList != null)
                {
                    genericList.ToList().ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x.Email) || !string.IsNullOrEmpty(x.FirstName) || !string.IsNullOrEmpty(x.LastName) || !string.IsNullOrEmpty(x.Phone) || !string.IsNullOrEmpty(x.Fax))
                        {
                            dataTable.Rows.Add(x.ContactTypeId, x.Email, x.Fax, x.FirstName, x.IsSameAsPrimary, x.LastName, x.Phone, x.RegionId, x.CountryId, x.Title, null, 1);
                        }
                    });
                }
                return dataTable;
            }
        }

        private static DataTable ConvertToContactsForPayment(IList<Contact> genericList)
        {
            using (DataTable dataTable = new DataTable("dataTableContacts"))
            {
                dataTable.Locale = CultureInfo.InvariantCulture;
                dataTable.TableName = "dataTableContacts";
                dataTable.Columns.Add("ContactType", typeof(int));
                dataTable.Columns.Add("Email", typeof(string));
                dataTable.Columns.Add("Fax", typeof(string));
                dataTable.Columns.Add("FirstName", typeof(string));
                dataTable.Columns.Add("IsSameAsPrimary", typeof(string));
                dataTable.Columns.Add("LastName", typeof(string));
                dataTable.Columns.Add("Phone", typeof(string));
                dataTable.Columns.Add("ContactRegion", typeof(int));
                // dataTable.Columns.Add("ContactCountry", typeof(int));
                dataTable.Columns.Add("Title", typeof(string));
                dataTable.Columns.Add("PartnerID", typeof(int));
                dataTable.Columns.Add("ContactStatusId", typeof(int));

                if (genericList != null)
                {
                    genericList.ToList().ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x.Email) || !string.IsNullOrEmpty(x.FirstName) || !string.IsNullOrEmpty(x.LastName) || !string.IsNullOrEmpty(x.Phone) || !string.IsNullOrEmpty(x.Fax))
                        {
                            dataTable.Rows.Add(x.ContactTypeId, x.Email, x.Fax, x.FirstName, x.IsSameAsPrimary, x.LastName, x.Phone, x.RegionId, x.Title, null, 1);
                        }
                    });
                }
                return dataTable;
            }
        }

        /// <summary>
        /// Converts to DB table type
        /// </summary>
        /// <param name="genericList"></param>
        /// <returns></returns>
        private static DataTable ConvertToContactsForAcountStatus(IList<Contact> genericList)
        {
            using (DataTable dataTable = new DataTable("dataTableContacts"))
            {
                dataTable.Locale = CultureInfo.InvariantCulture;
                dataTable.TableName = "dataTableContacts";
                dataTable.Columns.Add("ContactId", typeof(int));
                dataTable.Columns.Add("ContactType", typeof(int));
                dataTable.Columns.Add("Email", typeof(string));
                dataTable.Columns.Add("Fax", typeof(string));
                dataTable.Columns.Add("FirstName", typeof(string));
                dataTable.Columns.Add("IsSameAsPrimary", typeof(string));
                dataTable.Columns.Add("LastName", typeof(string));
                dataTable.Columns.Add("Phone", typeof(string));
                dataTable.Columns.Add("ContactRegion", typeof(int));
                dataTable.Columns.Add("ContactCountry", typeof(int));
                dataTable.Columns.Add("Title", typeof(string));

                if (genericList != null)
                {
                    genericList.ToList().ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x.Email) || !string.IsNullOrEmpty(x.FirstName) || !string.IsNullOrEmpty(x.LastName) || !string.IsNullOrEmpty(x.Phone) || !string.IsNullOrEmpty(x.Fax) || string.IsNullOrEmpty(x.Title))
                        {
                            dataTable.Rows.Add(x.ContactId, x.ContactTypeId, x.Email, x.Fax, x.FirstName, x.IsSameAsPrimary, x.LastName, x.Phone, x.RegionId, x.CountryId, x.Title);
                        }
                    });
                }
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the category details for a partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static IList<PartnerCategories> GetPartnerCategoryDetails(int partnerId)
        {
            PartnerCategories partnerCategory;
            PartnerCategoryDetails partnerCategoryDetails;
            PartnerPropertyType partnerPropertyType;
            Country country;
            IList<PartnerCategories> partnerCategoryCollection = new List<PartnerCategories>();
            List<PartnerCategoryDetails> partnerCategoryDetailsCollection;
            //IList<PartnerPropertyType> partnerPropertyTypeCollection;
            IList<Country> partnerCountryCollection;
            IDataReader drPartnerCategory = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerCategoryDetails))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);

                    drPartnerCategory = (IDataReader)db.ExecuteReader(dbCommand);

                    while (drPartnerCategory.Read())
                    {
                        var partnerCategoryColl = partnerCategoryCollection.Where(x => x.CategoryId == Convert.ToInt32(drPartnerCategory["CatId"], CultureInfo.InvariantCulture));
                        if (partnerCategoryColl.Count() == 0)
                        {
                            partnerCategory = new PartnerCategories();
                            partnerCategory.PartnerId = partnerId;
                            partnerCategory.CategoryId = Convert.ToInt32(drPartnerCategory["CatId"], CultureInfo.InvariantCulture);
                            partnerCategory.CategoryDisplayName = Convert.ToString(drPartnerCategory["CatDisplayName"], CultureInfo.InvariantCulture);

                            partnerCategoryDetails = new PartnerCategoryDetails();
                            partnerCategoryDetails.BrandId = Convert.ToInt32(drPartnerCategory["BrandId"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.BrandDescription = Convert.ToString(drPartnerCategory["BrandName"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.RegionId = Convert.ToInt32(drPartnerCategory["RegionId"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.RegionDescription = Convert.ToString(drPartnerCategory["RegionName"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.IsAvailable = Convert.ToBoolean(drPartnerCategory["IsAvailable"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.CategoryId = Convert.ToInt32(drPartnerCategory["CatId"], CultureInfo.InvariantCulture);

                            partnerCountryCollection = new List<Country>();
                            if (!Convert.IsDBNull(drPartnerCategory["CountryId"]))
                            {
                                country = new Country()
                                {
                                    CountryId = Convert.ToInt32(drPartnerCategory["CountryId"], CultureInfo.InvariantCulture),
                                    CountryName = Convert.ToString(drPartnerCategory["CountryName"], CultureInfo.InvariantCulture),
                                    RegionId = Convert.ToInt32(drPartnerCategory["RegionId"], CultureInfo.InvariantCulture)
                                };
                                partnerCountryCollection.Add(country);
                            }
                            partnerCategoryDetails.Countries = partnerCountryCollection;

                            partnerCategoryDetailsCollection = new List<PartnerCategoryDetails>();
                            partnerCategoryDetailsCollection.Add(partnerCategoryDetails);

                            partnerCategory.PartnerCategoriesDetails = partnerCategoryDetailsCollection;
                            partnerCategoryCollection.Add(partnerCategory);
                        }
                        else
                        {
                            var partnerCategoriesCollection = partnerCategoryCollection.Where(x => x.CategoryId == Convert.ToInt32(drPartnerCategory["CatId"], CultureInfo.InvariantCulture)).ToList();
                            if (partnerCategoriesCollection.Count > 0)
                            {
                                partnerCategoriesCollection.ForEach(x =>
                                {
                                    var partnerCategoryDetailCollection = x.PartnerCategoriesDetails.Where(z => z.BrandId == Convert.ToInt32(drPartnerCategory["BrandId"], CultureInfo.InvariantCulture) && z.RegionId == Convert.ToInt32(drPartnerCategory["RegionId"], CultureInfo.InvariantCulture));
                                    if (partnerCategoryDetailCollection.Count() == 0)
                                    {
                                        partnerCategoryDetails = new PartnerCategoryDetails();
                                        partnerCategoryDetails.BrandId = Convert.ToInt32(drPartnerCategory["BrandId"], CultureInfo.InvariantCulture);
                                        partnerCategoryDetails.BrandDescription = Convert.ToString(drPartnerCategory["BrandName"], CultureInfo.InvariantCulture);
                                        partnerCategoryDetails.RegionId = Convert.ToInt32(drPartnerCategory["RegionId"], CultureInfo.InvariantCulture);
                                        partnerCategoryDetails.RegionDescription = Convert.ToString(drPartnerCategory["RegionName"], CultureInfo.InvariantCulture);
                                        partnerCategoryDetails.IsAvailable = Convert.ToBoolean(drPartnerCategory["IsAvailable"], CultureInfo.InvariantCulture);
                                        partnerCategoryDetails.CategoryId = Convert.ToInt32(drPartnerCategory["CatId"], CultureInfo.InvariantCulture);

                                        partnerCountryCollection = new List<Country>();
                                        if (!Convert.IsDBNull(drPartnerCategory["CountryId"]))
                                        {
                                            country = new Country()
                                            {
                                                CountryId = Convert.ToInt32(drPartnerCategory["CountryId"], CultureInfo.InvariantCulture),
                                                CountryName = Convert.ToString(drPartnerCategory["CountryName"], CultureInfo.InvariantCulture),
                                                RegionId = Convert.ToInt32(drPartnerCategory["RegionId"], CultureInfo.InvariantCulture)
                                            };
                                            partnerCountryCollection.Add(country);
                                        }
                                        partnerCategoryDetails.Countries = partnerCountryCollection;

                                        //partnerCategoryDetails.PropertyTypes = partnerPropertyTypeCollection;

                                        x.PartnerCategoriesDetails.Add(partnerCategoryDetails);
                                    }
                                    else
                                    {
                                        var partnerCatDetailsCollection = x.PartnerCategoriesDetails.Where(z => z.BrandId == Convert.ToInt32(drPartnerCategory["BrandId"], CultureInfo.InvariantCulture) && z.RegionId == Convert.ToInt32(drPartnerCategory["RegionId"], CultureInfo.InvariantCulture));
                                        //var IsPropertyExist = partnerCatDetailsCollection.FirstOrDefault().PropertyTypes.Any(y => y.PropertyTypeId == Convert.ToInt32(drPartnerCategory["PropertyTypeId"], CultureInfo.InvariantCulture));


                                        var IsCountryExist = partnerCatDetailsCollection.First().Countries.Any(y => y.CountryId == Convert.ToInt32(drPartnerCategory["CountryId"], CultureInfo.InvariantCulture));

                                        if (!IsCountryExist)
                                        {
                                            Country country1 = new Country()
                                            {
                                                CountryId = Convert.ToInt32(drPartnerCategory["CountryId"], CultureInfo.InvariantCulture),
                                                CountryName = Convert.ToString(drPartnerCategory["CountryName"], CultureInfo.InvariantCulture),
                                                RegionId = Convert.ToInt32(drPartnerCategory["RegionId"], CultureInfo.InvariantCulture)
                                            };
                                            partnerCategoryDetailCollection.First().Countries.Add(country1);
                                        }
                                        //}
                                    }
                                }
                                    );
                            }
                        }
                    }
                    drPartnerCategory.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drPartnerCategory != null && !drPartnerCategory.IsClosed)
                {
                    drPartnerCategory.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return partnerCategoryCollection;
        }

        /// <summary>
        /// Gets Renew Account Details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static PartnerApplication GetRenewAccountDetails(int partnerId)
        {
            PartnerApplication partnerApplication = null;
            IDataReader drPartnerDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetRenewAccountDetails))
                {
                    db.AddInParameter(dbCommand, "@PartnerId ", DbType.Int32, partnerId);

                    drPartnerDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drPartnerDetails.Read())
                    {
                        partnerApplication = new PartnerApplication()
                        {
                            PartnerId = Convert.ToInt32(drPartnerDetails["PartnerId"], CultureInfo.InvariantCulture),
                            CompanyName = Convert.ToString(drPartnerDetails["CompanyName"], CultureInfo.InvariantCulture),
                            CompanyAddress1 = Convert.ToString(drPartnerDetails["CompanyAddress1"], CultureInfo.InvariantCulture),
                            CompanyAddress2 = Convert.IsDBNull(drPartnerDetails["CompanyAddress2"]) ? string.Empty : Convert.ToString(drPartnerDetails["CompanyAddress2"], CultureInfo.InvariantCulture),
                            City = Convert.ToString(drPartnerDetails["CityName"], CultureInfo.InvariantCulture),
                            Country = Convert.ToString(drPartnerDetails["CountryId"], CultureInfo.InvariantCulture),
                            State = Convert.IsDBNull(drPartnerDetails["StateId"]) ? null : Convert.ToString(drPartnerDetails["StateId"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.IsDBNull(drPartnerDetails["ZipCode"]) ? null : Convert.ToString(drPartnerDetails["ZipCode"], CultureInfo.InvariantCulture),
                            CompanyDescription = Convert.IsDBNull(drPartnerDetails["CompanyDescription"]) ? string.Empty : Convert.ToString(drPartnerDetails["CompanyDescription"], CultureInfo.InvariantCulture),
                            CompanyLogoImage = Convert.IsDBNull(drPartnerDetails["CompanyLogo"]) ? null : (byte[])(drPartnerDetails["CompanyLogo"]),
                            PartnershipType = Convert.ToInt32(drPartnerDetails["PartnerShipType"], CultureInfo.InvariantCulture),
                            PartnerShipAmount = Convert.IsDBNull(drPartnerDetails["Amount"]) ? 0 : (float)Convert.ToDouble(drPartnerDetails["Amount"], CultureInfo.InvariantCulture)
                        };
                    }

                    drPartnerDetails.NextResult();
                    partnerApplication.AppliedParntershipOpportunities = new List<AppliedPartnershipOpportunity>();

                    //To get the details for Applied Partnerships
                    while (drPartnerDetails.Read())
                    {
                        partnerApplication.AppliedParntershipOpportunities.Add(new AppliedPartnershipOpportunity()
                        {
                            CategoryId = Convert.ToInt32(drPartnerDetails["CatId"], CultureInfo.InvariantCulture),
                            CategoryDescription = Convert.ToString(drPartnerDetails["CatName"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drPartnerDetails["BrandId"], CultureInfo.InvariantCulture),
                            BrandName = Convert.ToString(drPartnerDetails["BrandName"], CultureInfo.InvariantCulture),
                            //PropertyTypeId = Convert.ToInt32(drPartnerDetails["PropertyTypeId"], CultureInfo.InvariantCulture),
                            //PropertyTypeName = Convert.ToString(drPartnerDetails["PropertyTypeName"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drPartnerDetails["RegionId"], CultureInfo.InvariantCulture),
                            RegionName = Convert.ToString(drPartnerDetails["RegionName"], CultureInfo.InvariantCulture),
                        });
                    }
                    drPartnerDetails.NextResult();
                    partnerApplication.Contacts = new List<Contact>();

                    while (drPartnerDetails.Read())
                    {
                        partnerApplication.Contacts.Add(new Contact()
                        {
                            ContactId = Convert.ToString(drPartnerDetails["ContactId"], CultureInfo.InvariantCulture),
                            ContactTypeId = Convert.ToInt32(drPartnerDetails["ContactTypeId"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drPartnerDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drPartnerDetails["LastName"], CultureInfo.InvariantCulture),
                            Title = Convert.IsDBNull(drPartnerDetails["Title"]) ? null : Convert.ToString(drPartnerDetails["Title"], CultureInfo.InvariantCulture),
                            Phone = Convert.ToString(drPartnerDetails["Phone"], CultureInfo.InvariantCulture),
                            Fax = Convert.ToString(drPartnerDetails["Fax"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drPartnerDetails["Email"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drPartnerDetails["ContactRegionId"], CultureInfo.InvariantCulture)
                        });
                    }

                    drPartnerDetails.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drPartnerDetails != null && !drPartnerDetails.IsClosed)
                {
                    drPartnerDetails.Close();
                }

                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return partnerApplication;
        }

        /// <summary>
        /// Gets Payment Code Details
        /// </summary>
        /// <param name="paymentRequestCode"></param>
        /// <returns></returns>
        public static PaymentRequestDetails GetPaymentCodeDetails(string paymentRequestCode)
        {
            PaymentRequestDetails paymentRequestDetails = null;
            IDataReader drPaymentRequest = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPaymentRequestCodeDetails))
                {
                    db.AddInParameter(dbCommand, "@paymentRequestCode", DbType.String, paymentRequestCode);

                    drPaymentRequest = (IDataReader)db.ExecuteReader(dbCommand);

                    if (drPaymentRequest.Read())
                    {
                        paymentRequestDetails = new PaymentRequestDetails()
                        {
                            PaymentRequestId = Convert.ToInt32(drPaymentRequest["PaymentRequestId"], CultureInfo.InvariantCulture),
                            PartnerId = Convert.ToInt32(drPaymentRequest["PartnerId"], CultureInfo.InvariantCulture),
                            PartnershipTypeId = Convert.ToInt32(drPaymentRequest["PartnershipType"], CultureInfo.InvariantCulture),
                            ActivationDate = Convert.IsDBNull(drPaymentRequest["ActivationDate"]) ? null : (DateTime?)Convert.ToDateTime(drPaymentRequest["ActivationDate"], CultureInfo.InvariantCulture),
                            ExpirationDate = Convert.IsDBNull(drPaymentRequest["ExpirationDate"]) ? null : (DateTime?)Convert.ToDateTime(drPaymentRequest["ExpirationDate"], CultureInfo.InvariantCulture),

                        };
                    }
                    drPaymentRequest.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drPaymentRequest != null && !drPaymentRequest.IsClosed)
                {
                    drPaymentRequest.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return paymentRequestDetails;
        }

        /// <summary>
        /// Gets The Hot Leads for a Gold Partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static Tuple<IList<HotLead>, int> GetPartnerHotLeads(ConstructionReportSearch constReportSearch)
        {
            Tuple<IList<HotLead>, int> hotLeadDetails = null;
            // IList<HotLead> hotLeadCollection = new List<HotLead>();
            IDataReader drHotLeads = null;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);

                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerHotLeads))
                {
                    db.AddInParameter(dbCommand, "@PartnerId ", DbType.Int32, constReportSearch.PartnerId);
                    db.AddInParameter(dbCommand, "@sortExpression ", DbType.String, constReportSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@sortDirection ", DbType.String, constReportSearch.SortDirection);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, constReportSearch.PageIndex);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, constReportSearch.PageSize);
                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    drHotLeads = (IDataReader)db.ExecuteReader(dbCommand);

                    var hotLeads = new List<HotLead>();
                    while (drHotLeads.Read())
                    {
                        hotLeads.Add(new HotLead()
                        {
                            ProjectId = Convert.ToInt32(drHotLeads["ProjectId"], CultureInfo.InvariantCulture),
                            CategoryId = Convert.ToInt32(drHotLeads["CatId"], CultureInfo.InvariantCulture),
                            AddedDate = Convert.ToDateTime(drHotLeads["ProjectAddedDate"], CultureInfo.InvariantCulture),
                            CategoryDisplayName = Convert.ToString(drHotLeads["CatDisplayName"], CultureInfo.InvariantCulture),
                            ProductId = Convert.IsDBNull(drHotLeads["ProductId"]) ? string.Empty : Convert.ToString(drHotLeads["ProductId"]),
                            ProductName = drHotLeads["ProductName"].Equals(DBNull.Value) ? string.Empty : Convert.ToString(drHotLeads["ProductName"]),
                            ProjectTypeName = Convert.ToString(drHotLeads["ProjectTypeDescription"], CultureInfo.InvariantCulture),
                            BrandName = Convert.ToString(drHotLeads["BrandName"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drHotLeads["PropertyBrandId"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drHotLeads["PropertyRegionId"], CultureInfo.InvariantCulture),
                            Address = Convert.ToString(drHotLeads["Address"], CultureInfo.InvariantCulture),
                            Address2 = Convert.ToString(drHotLeads["Address2"], CultureInfo.InvariantCulture),
                            CityName = Convert.ToString(drHotLeads["CityName"], CultureInfo.InvariantCulture),
                            StateAbbreviation = Convert.ToString(drHotLeads["StateName"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.ToString(drHotLeads["ZipCode"], CultureInfo.InvariantCulture),
                            FacilityName = Convert.ToString(drHotLeads["FacilityName"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drHotLeads["CountryName"], CultureInfo.InvariantCulture),
                            RepEmailAddress = Convert.ToString(drHotLeads["RepEmailAddress"], CultureInfo.InvariantCulture),
                            ProjectManager = Convert.ToString(drHotLeads["ProjectManager"], CultureInfo.InvariantCulture),
                            RepContactDate = drHotLeads["MessageDate"].Equals(DBNull.Value) ? null : Convert.ToString(drHotLeads["MessageDate"], CultureInfo.InvariantCulture)
                        }

                        );
                    }

                    drHotLeads.NextResult();
                    var result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                    drHotLeads.Close();
                    hotLeadDetails = new Tuple<IList<HotLead>, int>(hotLeads, result);
                }
            }

            catch (Exception genericException)
            {
                if (drHotLeads != null && !drHotLeads.IsClosed)
                {
                    drHotLeads.Close();
                }

                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);

                if (rethrow)
                {
                    throw genericException;
                }
            }
            return hotLeadDetails;
        }

        /// <summary>
        /// Gets The Warm Leads for Partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public static Tuple<IList<WarmLead>, int> GetPartnerWarmLeads(ConstructionReportSearch constReportSearch)
        {
            Tuple<IList<WarmLead>, int> warmLeadDetails = null;
            IDataReader drWarmLeads = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerWarmLeads))
                {
                    db.AddInParameter(dbCommand, "@PartnerId ", DbType.Int32, constReportSearch.PartnerId);
                    db.AddInParameter(dbCommand, "@sortExpression ", DbType.String, constReportSearch.SortExpression);
                    db.AddInParameter(dbCommand, "@sortDirection ", DbType.String, constReportSearch.SortDirection);
                    db.AddInParameter(dbCommand, "@pageIndex", DbType.Int16, constReportSearch.PageIndex);
                    db.AddInParameter(dbCommand, "@pageSize", DbType.Int16, constReportSearch.PageSize);
                    SqlParameter retValParam = new SqlParameter("@totalRecordCount", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    drWarmLeads = (IDataReader)db.ExecuteReader(dbCommand);

                    var warmLeads = new List<WarmLead>();
                    while (drWarmLeads.Read())
                    {
                        warmLeads.Add(new WarmLead()
                        {
                            ProjectId = Convert.ToInt32(drWarmLeads["ProjectId"], CultureInfo.InvariantCulture),
                            CategoryId = Convert.ToInt32(drWarmLeads["CatId"], CultureInfo.InvariantCulture),
                            AddedDate = Convert.ToDateTime(drWarmLeads["ProjectAddedDate"], CultureInfo.InvariantCulture),
                            CategoryDisplayName = Convert.ToString(drWarmLeads["CatDisplayName"], CultureInfo.InvariantCulture),
                            ProductId = Convert.IsDBNull(drWarmLeads["ProductId"]) ? string.Empty : Convert.ToString(drWarmLeads["ProductId"]),
                            ProductName = drWarmLeads["ProductName"].Equals(DBNull.Value) ? string.Empty : Convert.ToString(drWarmLeads["ProductName"]),
                            ProjectTypeName = Convert.ToString(drWarmLeads["ProjectTypeDescription"], CultureInfo.InvariantCulture),
                            BrandName = Convert.ToString(drWarmLeads["BrandName"], CultureInfo.InvariantCulture),
                            PropTypeName = Convert.ToString(drWarmLeads["PropTypeName"], CultureInfo.InvariantCulture),
                            BrandId = Convert.ToInt32(drWarmLeads["PropertyBrandId"], CultureInfo.InvariantCulture),
                            RegionId = Convert.ToInt32(drWarmLeads["PropertyRegionId"], CultureInfo.InvariantCulture),
                            PropertyTypeId = Convert.ToInt32(drWarmLeads["PropertyTypeId"], CultureInfo.InvariantCulture),
                            Address = Convert.ToString(drWarmLeads["Address"], CultureInfo.InvariantCulture),
                            Address2 = Convert.ToString(drWarmLeads["Address2"], CultureInfo.InvariantCulture),
                            CityName = Convert.ToString(drWarmLeads["CityName"], CultureInfo.InvariantCulture),
                            StateAbbreviation = Convert.ToString(drWarmLeads["StateName"], CultureInfo.InvariantCulture),
                            ZipCode = Convert.ToString(drWarmLeads["ZipCode"], CultureInfo.InvariantCulture),
                            FacilityName = Convert.ToString(drWarmLeads["FacilityName"], CultureInfo.InvariantCulture),
                            CountryName = Convert.ToString(drWarmLeads["CountryName"], CultureInfo.InvariantCulture),
                            RepEmailAddress = Convert.ToString(drWarmLeads["RepEmailAddress"], CultureInfo.InvariantCulture),
                            ProjectManager = Convert.ToString(drWarmLeads["ProjectManager"], CultureInfo.InvariantCulture),
                            RepContactDate = drWarmLeads["MessageDate"].Equals(DBNull.Value) ? null : Convert.ToString(drWarmLeads["MessageDate"], CultureInfo.InvariantCulture)
                        }

                        );
                    }

                    drWarmLeads.NextResult();
                    var result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                    drWarmLeads.Close();
                    warmLeadDetails = new Tuple<IList<WarmLead>, int>(warmLeads, result);
                }
            }
            catch (Exception genericException)
            {
                if (drWarmLeads != null && !drWarmLeads.IsClosed)
                {
                    drWarmLeads.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);

                if (rethrow)
                {
                    throw genericException;
                }
            }

            return warmLeadDetails;
        }


        /// <summary>
        /// Fetch Partner Export Report Data
        /// </summary>
        /// <returns></returns>
        public static DataSet GetPartnerExportReportDataSet(PartnerSearch partnerSearch = null)
        {
            DataSet partnerExportReportDataSet = null;
            try
            {
                using (partnerExportReportDataSet = new DataSet())
                {
                    partnerExportReportDataSet.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.DownloadPartnerExportReport);
                    dbCommand.CommandTimeout = 240;
                    if (string.IsNullOrWhiteSpace(partnerSearch.RegionId))
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, partnerSearch.RegionId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.CountryId))
                        db.AddInParameter(dbCommand, "@countryID", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@countryID", DbType.Int32, partnerSearch.CountryId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.BrandId))
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, partnerSearch.BrandId);


                    if (string.IsNullOrWhiteSpace(partnerSearch.TertioryCategoryId))
                    {
                        if (String.IsNullOrWhiteSpace(partnerSearch.SubCategoryId))
                        {
                            if (String.IsNullOrWhiteSpace(partnerSearch.CategoryId))
                            {
                                db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, DBNull.Value);
                            }
                            else
                            {
                                db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.CategoryId);
                            }
                        }
                        else
                        {
                            db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.SubCategoryId);
                        }
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.TertioryCategoryId);
                    }


                    if (string.IsNullOrWhiteSpace(partnerSearch.StatusId))
                        db.AddInParameter(dbCommand, "@statusId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@statusId", DbType.Int32, partnerSearch.StatusId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.PartnershipTypeId))
                        db.AddInParameter(dbCommand, "@partnershipTypeId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@partnershipTypeId", DbType.Int32, partnerSearch.PartnershipTypeId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.IsSAMPartner))
                        db.AddInParameter(dbCommand, "@IsSAMPartner", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@IsSAMPartner", DbType.Int32, partnerSearch.IsSAMPartner);

                    if (partnerSearch.RequestDateFrom == null)
                        db.AddInParameter(dbCommand, "@requestDateFrom", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@requestDateFrom", DbType.DateTime, partnerSearch.RequestDateFrom);

                    if (partnerSearch.RequestDateTo == null)
                        db.AddInParameter(dbCommand, "@requestDateTo", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@requestDateTo", DbType.DateTime, partnerSearch.RequestDateTo);

                    if (partnerSearch.ExpirationDateFrom == null)
                        db.AddInParameter(dbCommand, "@expirationDateFrom", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@expirationDateFrom", DbType.DateTime, partnerSearch.ExpirationDateFrom);

                    if (partnerSearch.ExpirationDateTo == null)
                        db.AddInParameter(dbCommand, "@expirationDateTo", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@expirationDateTo", DbType.DateTime, partnerSearch.ExpirationDateTo);

                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, partnerSearch.UserId);

                    partnerExportReportDataSet = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return partnerExportReportDataSet;
        }


        /// <summary>
        /// Fetch Partner Export Report Data
        /// </summary>
        /// <returns></returns>
        public static DataSet GetPartnerExportRptDataSetDelPartners(PartnerSearch partnerSearch = null, string delPartnersList = null)
        {
            DataSet partnerExportReportDataSet = null;
            try
            {
                using (partnerExportReportDataSet = new DataSet())
                {
                    partnerExportReportDataSet.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.DownloadPartnerExportRptForDelPartners);
                    dbCommand.CommandTimeout = 240;
                    if (string.IsNullOrWhiteSpace(partnerSearch.RegionId))
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@regionId", DbType.Int32, partnerSearch.RegionId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.CountryId))
                        db.AddInParameter(dbCommand, "@countryID", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@countryID", DbType.Int32, partnerSearch.CountryId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.BrandId))
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@brandId", DbType.String, partnerSearch.BrandId);


                    if (string.IsNullOrWhiteSpace(partnerSearch.TertioryCategoryId))
                    {
                        if (String.IsNullOrWhiteSpace(partnerSearch.SubCategoryId))
                        {
                            if (String.IsNullOrWhiteSpace(partnerSearch.CategoryId))
                            {
                                db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, DBNull.Value);
                            }
                            else
                            {
                                db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.CategoryId);
                            }
                        }
                        else
                        {
                            db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.SubCategoryId);
                        }
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@categoryId", DbType.Int32, partnerSearch.TertioryCategoryId);
                    }


                    if (string.IsNullOrWhiteSpace(partnerSearch.StatusId))
                        db.AddInParameter(dbCommand, "@statusId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@statusId", DbType.Int32, partnerSearch.StatusId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.PartnershipTypeId))
                        db.AddInParameter(dbCommand, "@partnershipTypeId", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@partnershipTypeId", DbType.Int32, partnerSearch.PartnershipTypeId);

                    if (string.IsNullOrWhiteSpace(partnerSearch.IsSAMPartner))
                        db.AddInParameter(dbCommand, "@IsSAMPartner", DbType.Int32, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@IsSAMPartner", DbType.Int32, partnerSearch.IsSAMPartner);

                    if (partnerSearch.RequestDateFrom == null)
                        db.AddInParameter(dbCommand, "@requestDateFrom", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@requestDateFrom", DbType.DateTime, partnerSearch.RequestDateFrom);

                    if (partnerSearch.RequestDateTo == null)
                        db.AddInParameter(dbCommand, "@requestDateTo", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@requestDateTo", DbType.DateTime, partnerSearch.RequestDateTo);

                    if (partnerSearch.ExpirationDateFrom == null)
                        db.AddInParameter(dbCommand, "@expirationDateFrom", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@expirationDateFrom", DbType.DateTime, partnerSearch.ExpirationDateFrom);

                    if (partnerSearch.ExpirationDateTo == null)
                        db.AddInParameter(dbCommand, "@expirationDateTo", DbType.DateTime, DBNull.Value);
                    else
                        db.AddInParameter(dbCommand, "@expirationDateTo", DbType.DateTime, partnerSearch.ExpirationDateTo);

                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, partnerSearch.UserId);

                    db.AddInParameter(dbCommand, "@DeletePartnersIDs", DbType.String, delPartnersList);

                    partnerExportReportDataSet = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return partnerExportReportDataSet;
        }

        /// <summary>
        /// Fetch Construction Report Data
        /// </summary>
        /// <returns></returns>
        public static DataSet GetConstructionReportDataSet(int partnerId)
        {
            DataSet constReportDataSet = null;
            try
            {
                using (constReportDataSet = new DataSet())
                {
                    constReportDataSet.Locale = CultureInfo.InvariantCulture;
                    string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                    DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.DownloadConstructionReport);
                    db.AddInParameter(dbCommand, "partnerId", DbType.Int32, partnerId);
                    constReportDataSet = db.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return constReportDataSet;
        }

        /// <summary>
        /// Gets the Accessibility Flag value
        /// </summary>
        /// <returns></returns>
        public static int GetPartnerAccessibilityFlagValue(int partnerId)
        {
            int result = 0;

            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerAccessibilityOnSubmitProduct))
                {
                    db.AddInParameter(dbCommand, "partnerId", DbType.Int32, partnerId);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);

                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return result;
        }

        /// <summary>
        /// Update the account status details
        /// </summary>
        /// <param name="accountStatus"></param>
        /// <returns></returns>
        public static int UpdatePartnerAccountStatus(AccountStatus accountStatus)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdateAccountStatusDetails))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, accountStatus.PartnerId);
                    db.AddInParameter(dbCommand, "@companyAddress1", DbType.String, accountStatus.CompanyAddress1);
                    db.AddInParameter(dbCommand, "@companyAddress2", DbType.String, accountStatus.CompanyAddress2);
                    db.AddInParameter(dbCommand, "@countryId", DbType.Int32, accountStatus.CountryId);
                    db.AddInParameter(dbCommand, "@city", DbType.String, accountStatus.City);
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, accountStatus.UserId);
                    db.AddInParameter(dbCommand, "@companyDescription", DbType.String, accountStatus.CompanyDescription);
                    db.AddInParameter(dbCommand, "@webSite", DbType.String, accountStatus.WebSite);
                    db.AddInParameter(dbCommand, "@companyLogo", DbType.Binary, accountStatus.CompanyLogoImage);
                    db.AddInParameter(dbCommand, "@companyLogoThumbnail", DbType.Binary, accountStatus.CompanyLogoThumbnailImage);
                    db.AddInParameter(dbCommand, "@companyLogoName", DbType.String, accountStatus.CompanyLogoName);
                    if (string.IsNullOrWhiteSpace(accountStatus.HiltonId))
                    {
                        db.AddInParameter(dbCommand, "@hiltonId", DbType.String, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@hiltonId", DbType.String, accountStatus.HiltonId);
                    }
                    if (accountStatus.StateId == 0)
                    {
                        db.AddInParameter(dbCommand, "@stateId", DbType.Int32, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@stateId", DbType.Int32, accountStatus.StateId);
                    }
                    if (string.IsNullOrWhiteSpace(accountStatus.ZipCode))
                    {
                        db.AddInParameter(dbCommand, "@zip", DbType.String, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(dbCommand, "@zip", DbType.String, accountStatus.ZipCode);
                    }

                    SqlParameter contact = new SqlParameter("@partnerContacts", SqlDbType.Structured);
                    contact.TypeName = "dbo.PartnerContactsAcc";
                    contact.Direction = ParameterDirection.Input;
                    contact.Value = ConvertToContactsForAcountStatus(accountStatus.Contacts);
                    dbCommand.Parameters.Add(contact);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return result;
        }

        /// <summary>
        /// Send the activation email
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="partnerId"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static IList<PartnerLinkedWebAccount> SendActivationEmail(int userId, int partnerId, string culture, ref int result)
        {
            IDataReader drLinkedWebAccountDetails = null;
            IList<PartnerLinkedWebAccount> webAccounts = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.SendActivationNotificationEmail))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    db.AddInParameter(dbCommand, "@userId", DbType.String, userId);
                    db.AddInParameter(dbCommand, "@culture", DbType.String, culture);

                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    drLinkedWebAccountDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    webAccounts = new List<PartnerLinkedWebAccount>();
                    while (drLinkedWebAccountDetails.Read())
                    {
                        webAccounts.Add(new PartnerLinkedWebAccount()
                        {
                            HiltonId = Convert.ToString(drLinkedWebAccountDetails["HiltonId"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drLinkedWebAccountDetails["Email"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drLinkedWebAccountDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drLinkedWebAccountDetails["LastName"], CultureInfo.InvariantCulture),
                            StatusDesc = Convert.ToString(drLinkedWebAccountDetails["StatusDesc"], CultureInfo.InvariantCulture),
                            PartnerId = partnerId,
                            UserId = Convert.ToInt32(drLinkedWebAccountDetails["UserId"], CultureInfo.InvariantCulture)
                        });
                    }
                    drLinkedWebAccountDetails.NextResult();
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                    drLinkedWebAccountDetails.Close();
                }
            }
            catch (Exception genericException)
            {
                if (drLinkedWebAccountDetails != null && !drLinkedWebAccountDetails.IsClosed)
                {
                    drLinkedWebAccountDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return webAccounts;
        }

        /// <summary>
        /// Delete Linked Web Account
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static Tuple<IList<PartnerLinkedWebAccount>, int> DeleteLinkedWebAccount(int userId, int partnerId)
        {
            IDataReader drLinkedWebAccountDetails = null;
            Tuple<IList<PartnerLinkedWebAccount>, int> webAccountsDetails = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.DeleteLinkedWebAccount))
                {
                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    SqlParameter retValParam = new SqlParameter("@result", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);
                    drLinkedWebAccountDetails = (IDataReader)db.ExecuteReader(dbCommand);

                    var webAccounts = new List<PartnerLinkedWebAccount>();
                    while (drLinkedWebAccountDetails.Read())
                    {
                        webAccounts.Add(new PartnerLinkedWebAccount()
                        {
                            HiltonId = Convert.ToString(drLinkedWebAccountDetails["HiltonId"], CultureInfo.InvariantCulture),
                            Email = Convert.ToString(drLinkedWebAccountDetails["Email"], CultureInfo.InvariantCulture),
                            FirstName = Convert.ToString(drLinkedWebAccountDetails["FirstName"], CultureInfo.InvariantCulture),
                            LastName = Convert.ToString(drLinkedWebAccountDetails["LastName"], CultureInfo.InvariantCulture),
                            StatusDesc = Convert.ToString(drLinkedWebAccountDetails["StatusDesc"], CultureInfo.InvariantCulture),
                            PartnerId = Convert.ToInt32(drLinkedWebAccountDetails["PartnerId"], CultureInfo.InvariantCulture),
                            UserId = Convert.ToInt32(drLinkedWebAccountDetails["UserId"], CultureInfo.InvariantCulture)
                        });
                    }
                    drLinkedWebAccountDetails.NextResult();
                    var result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                    drLinkedWebAccountDetails.Close();
                    webAccountsDetails = new Tuple<IList<PartnerLinkedWebAccount>, int>(webAccounts, result);
                }
            }
            catch (Exception genericException)
            {
                if (drLinkedWebAccountDetails != null && !drLinkedWebAccountDetails.IsClosed)
                {
                    drLinkedWebAccountDetails.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }
            return webAccountsDetails;
        }
        public static IList<Search> SearchAutoComplete(string name_startsWith)
        {
            IDataReader drSearchFilter = null;
            IList<Search> searchFilterList = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.Search))
                {

                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);
                    searchFilterList = new List<Search>();

                    while (drSearchFilter.Read())
                    {
                        searchFilterList.Add(new Search()
                        {
                            type = Convert.ToString(drSearchFilter["Label"], CultureInfo.InvariantCulture),
                            name = Convert.ToString(drSearchFilter["LabelDesc"], CultureInfo.InvariantCulture),
                            id = Convert.ToString(drSearchFilter["LabelVal"], CultureInfo.InvariantCulture),
                        });
                    }

                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

            }
            return searchFilterList.ToList(); ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public static IList<PartnerCategoryDetails> GetCategoryBrandMappingForPartner(int partnerId)
        {

            IList<PartnerCategoryDetails> mappingData = new List<PartnerCategoryDetails>();

            IDataReader drSearchFilter = null;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.GetPartnerCategoryBrandMappingList))
                {
                    db.AddInParameter(dbCommand, "@partnerId", DbType.Int32, partnerId);
                    drSearchFilter = (IDataReader)db.ExecuteReader(dbCommand);


                    PartnerCategoryDetails partnerCategoryDetails;

                    while (drSearchFilter.Read())
                    {
                        int catid = Convert.ToInt32(drSearchFilter["CatId"]);
                        int brandId = Convert.ToInt32(drSearchFilter["BrandId"]);
                        partnerCategoryDetails = mappingData.FirstOrDefault(x => x.CategoryId == catid && x.BrandId == brandId);

                        if (partnerCategoryDetails == null)
                        {
                            partnerCategoryDetails = new PartnerCategoryDetails();
                            IList<Country> countries = new List<Country>();
                            partnerCategoryDetails.CategoryId = Convert.ToInt32(drSearchFilter["CatId"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.CategoryDisplayName = Convert.ToString(drSearchFilter["CategoryDisplayName"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.BrandId = Convert.ToInt32(drSearchFilter["BrandId"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.BrandDescription = Convert.ToString(drSearchFilter["BrandDescription"], CultureInfo.InvariantCulture);
                            partnerCategoryDetails.BrandSegment = Convert.ToString(drSearchFilter["BrandSegmentName"], CultureInfo.InvariantCulture);

                            countries.Add(new Country
                            {
                                CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                                RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture)
                            });
                            partnerCategoryDetails.Countries = countries;
                            mappingData.Add(partnerCategoryDetails);
                        }
                        else
                        {
                            partnerCategoryDetails.Countries.Add(new Country
                            {
                                CountryId = Convert.ToInt32(drSearchFilter["CountryId"], CultureInfo.InvariantCulture),
                                RegionId = Convert.ToInt32(drSearchFilter["RegionId"], CultureInfo.InvariantCulture)
                            });

                        }

                    }
                    drSearchFilter.Close();
                }
            }
            catch (Exception exception)
            {
                if (drSearchFilter != null && !drSearchFilter.IsClosed)
                {
                    drSearchFilter.Close();
                }
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return mappingData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partnerCategoriesList"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int UpdatePartnerRegionCountries(PartnerCategories partnerCategoriesList, int userId)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.UpdatePartnerRegionCountries))
                {

                    db.AddInParameter(dbCommand, "@userId", DbType.Int32, userId);
                    if (partnerCategoriesList.UpdateFlag)
                        db.AddInParameter(dbCommand, "@addDeleteFlag", DbType.Boolean, 1);
                    else
                        db.AddInParameter(dbCommand, "@addDeleteFlag", DbType.Boolean, 0);
                    SqlParameter specParam = new SqlParameter("@partnerCategories", SqlDbType.Structured);
                    specParam.TypeName = "dbo.PartnerCategories";
                    specParam.Value = CreateCategoryBrandRecords(partnerCategoriesList);
                    dbCommand.Parameters.Add(specParam);

                    result = db.ExecuteNonQuery(dbCommand);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partnerCategories"></param>
        /// <returns></returns>
        private static IEnumerable<SqlDataRecord> CreateCategoryBrandRecords(PartnerCategories partnerCategories)
        {
            SqlMetaData[] metaData = new SqlMetaData[7];
            metaData[0] = new SqlMetaData("PartnerId", SqlDbType.Int);
            metaData[1] = new SqlMetaData("CategoryId", SqlDbType.Int);
            metaData[2] = new SqlMetaData("BrandId", SqlDbType.Int);
            metaData[3] = new SqlMetaData("PropertyTypeId", SqlDbType.Int);
            metaData[4] = new SqlMetaData("RegionId", SqlDbType.Int);
            metaData[5] = new SqlMetaData("IsAvailable", SqlDbType.Bit);
            metaData[6] = new SqlMetaData("CountryId", SqlDbType.Int);

            SqlDataRecord resultRecord = new SqlDataRecord(metaData);

            int partnerid = partnerCategories.PartnerId;

            foreach (PartnerCategoryDetails specs in partnerCategories.PartnerCategoriesDetails)
            {


                foreach (Country country in specs.Countries)
                {
                    resultRecord.SetInt32(0, partnerid);
                    resultRecord.SetInt32(1, specs.CategoryId);
                    resultRecord.SetInt32(2, specs.BrandId);
                    resultRecord.SetInt32(3, 1);
                    resultRecord.SetInt32(4, country.RegionId);
                    resultRecord.SetBoolean(5, specs.IsAvailable);
                    resultRecord.SetInt32(6, country.CountryId);

                    yield return resultRecord;
                }

            }

        }


        public static int DeletePartners(string partnerIds, int userId)
        {
            int result = -1;
            try
            {
                string ConnectionString = SQLHelper.GetConnectionString(); SqlDatabase db = new SqlDatabase(ConnectionString);
                using (DbCommand dbCommand = db.GetStoredProcCommand(DataAccessConstants.DeleteParmanentlyInactivePartners))
                {
                    db.AddInParameter(dbCommand, "@partneridsToDel", DbType.String, partnerIds);
                    db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                    SqlParameter retValParam = new SqlParameter("@resultId", SqlDbType.Int);
                    retValParam.Direction = ParameterDirection.Output;
                    dbCommand.Parameters.Add(retValParam);

                    db.ExecuteNonQuery(dbCommand);
                    result = Convert.ToInt32(retValParam.Value, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception genericException)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref genericException);
                if (rethrow)
                {
                    throw genericException;
                }
            }

            return result;
        }
    }
}
