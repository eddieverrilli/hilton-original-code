﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Utilities
{
    public static class ResourceUtility
    {
        /// <summary>.
        /// This method get content blocks list
        /// </summary>
        /// <returns></returns>
        public static IList<ContentBlock> GetContentBlocks()
        {
            IList<ContentBlock> contentBlockCollection;

            if (!Caching.Exists(CacheConstants.LanguageResourceDetails))
            {
                UpdateContentBlocks();
            }

            contentBlockCollection = Caching.Get<IList<ContentBlock>>(CacheConstants.ContentBlocks);

            return contentBlockCollection;
        }

        /// <summary>.
        /// This method get the language resources list
        /// </summary>
        /// <returns></returns>
        public static IList<LanguageResourceDetail> GetLanguageResourceDetails()
        {
            IList<LanguageResourceDetail> languageResourceCollection;
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.ContentBlockAbsoluteExpiration);
            if (cacheAbsoluteExpiration == "0")
            {
                Caching.Clear(CacheConstants.LanguageResourceDetails);
            }

            if (Caching.Exists(CacheConstants.LanguageResourceDetails))
            {
                languageResourceCollection = Caching.Get<IList<LanguageResourceDetail>>(CacheConstants.LanguageResourceDetails);
            }
            else
            {
                languageResourceCollection = ContentBlockManager.Instance.GetLanguageResourceDetails;
                if (languageResourceCollection.Count > 0)
                {
                    Caching.Clear(CacheConstants.LanguageResourceDetails);
                    if (cacheAbsoluteExpiration != "0")
                        Caching.Add<IList<LanguageResourceDetail>>(languageResourceCollection, CacheConstants.LanguageResourceDetails, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
                }
            }
            return languageResourceCollection;
        }

        /// <summary>
        /// This method get the localized string for specified culture
        /// </summary>
        /// <param name="contentBlockId"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static string GetLocalizedString(int contentBlockId, CultureInfo culture, string defaultText = "")
        {
            string cultureCode = null;

            if (culture != null) //&& (culture.Name == "en-US" || culture.Name == "es-ES" || culture.Name == "fr-CA")
            {
                cultureCode = culture.Name;
            }
            IList<ContentBlock> contentBlockCollection = GetContentBlocks();
            if (contentBlockCollection != null && contentBlockCollection.Count > 0)
            {
                contentBlockCollection = contentBlockCollection.Where(x => x.ContentBlockId == contentBlockId).ToList();

                if (contentBlockCollection.Count > 0)
                {
                    int languageResourceId = contentBlockCollection.FirstOrDefault().LanguageResourceId;
                    IList<LanguageResourceDetail> languageResourceDetails = GetLanguageResourceDetails().Where(x => x.LanguageResourceId == languageResourceId && ((cultureCode == null && x.IsNeutralLanguage == true) || (x.CultureCode == cultureCode))).ToList();
                    if (languageResourceDetails.Count > 0)
                    {
                        string content = languageResourceDetails.First().Content.Trim();
                        int firstIndex = content.ToLower(culture).IndexOf("<p>", StringComparison.InvariantCulture);
                        int lastIndex = content.ToLower(culture).LastIndexOf("</p>", StringComparison.InvariantCulture);
                        if (firstIndex == 0 && lastIndex == content.Length - 4)
                        {
                            content = content.Substring(firstIndex + 3, content.Length - 7);
                        }

                        return content;
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(defaultText))
                return defaultText;
            else
                return string.Empty;
        }

        /// <summary>
        /// Update Content Blocks on delete and addition of content block
        /// </summary>
        public static void UpdateContentBlocks()
        {
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.ContentBlockAbsoluteExpiration);
            if (cacheAbsoluteExpiration != "0")
            {
                IList<ContentBlock> contentBlocks = ContentBlockManager.UpdateContentBlocks();
                if (contentBlocks.Count > 0)
                {
                    Caching.Clear(CacheConstants.ContentBlocks);

                    Caching.Add<IList<ContentBlock>>(contentBlocks, CacheConstants.ContentBlocks, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
                }
                UpdateLanguageResources();
            }
        }

        /// <summary>
        /// Update Language Resources on delete of content block
        /// </summary>
        public static void UpdateLanguageResources()
        {
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.ContentBlockAbsoluteExpiration);
            if (cacheAbsoluteExpiration != "0")
            {
                IList<LanguageResourceDetail> languageResourceCollection = ContentBlockManager.Instance.GetLanguageResourceDetails;
                if (languageResourceCollection.Count > 0)
                {
                    Caching.Clear(CacheConstants.LanguageResourceDetails);

                    Caching.Add<IList<LanguageResourceDetail>>(languageResourceCollection, CacheConstants.LanguageResourceDetails, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
                }
            }
        }
    }
}