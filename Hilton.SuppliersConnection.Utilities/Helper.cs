﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Utilities
{
    public static class Helper
    {
        /// <summary>
        /// This method return the filter collection for brand, region and property type dropdown
        /// </summary>
        /// <param name="searchFilterDataCollection"></param>
        /// <param name="brandSelectedValue"></param>
        /// <param name="regionSelectedValue"></param>
        /// <param name="propertyTypeSelectedValue"></param>
        /// <param name="categorySelectedValue"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetFilteredCollection(IList<SearchFilters> searchFilterDataCollection, int? brandSelectedValue, int? regionSelectedValue, int? propertyTypeSelectedValue)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                IEnumerable<SearchFilters> filteredCollection = searchFilterDataCollection.Where(item => ((regionSelectedValue == null || regionSelectedValue == 0) ? true : item.RegionId.Equals(regionSelectedValue))
                                                                                                     && ((brandSelectedValue == null || brandSelectedValue == 0) ? true : item.BrandId.Equals(brandSelectedValue)));
                //&& ((propertyTypeSelectedValue == null || propertyTypeSelectedValue == 0) ? true : item.PropertyTypeId.Equals(propertyTypeSelectedValue)));
                if (filteredCollection != null)
                {
                    searchFilterCollection = filteredCollection.ToList<SearchFilters>();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }

        /// <summary>
        /// This method return the filter collection for brand, region and property type dropdown
        /// </summary>
        /// <param name="searchFilterDataCollection"></param>
        /// <param name="brandSelectedValue"></param>
        /// <param name="regionSelectedValue"></param>
        /// <param name="propertyTypeSelectedValue"></param>
        /// <param name="categorySelectedValue"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetFilteredCollection(IList<SearchFilters> searchFilterDataCollection, string brandSelectedValue, int? regionSelectedValue, int? propertyTypeSelectedValue)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                var brands = (brandSelectedValue == null || brandSelectedValue == DropDownConstants.DefaultValue) ? null : brandSelectedValue.Split(',').Select(item => int.Parse(item));
                IEnumerable<SearchFilters> filteredCollection = searchFilterDataCollection.Where(item => ((regionSelectedValue == null || regionSelectedValue == 0) ? true : item.RegionId.Equals(regionSelectedValue))
                                                                                                     && (brands == null ? true : brands.Contains(item.BrandId)));
                //&& ((propertyTypeSelectedValue == null || propertyTypeSelectedValue == 0) ? true : item.PropertyTypeId.Equals(propertyTypeSelectedValue)));
                if (filteredCollection != null)
                {
                    searchFilterCollection = filteredCollection.ToList<SearchFilters>();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }


        //For Country Dropdownmethod
        public static IList<SearchFilters> GetFilteredCollectionForCountry(IList<SearchFilters> searchFilterDataCollection, string brandSelectedValue, int? regionSelectedValue, int? CountrySelectedValue, int? propertyTypeSelectedValue)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                var brands = (brandSelectedValue == null || brandSelectedValue == DropDownConstants.DefaultValue) ? null : brandSelectedValue.Split(',').Select(item => int.Parse(item));
                IEnumerable<SearchFilters> filteredCollection = searchFilterDataCollection.Where(item => ((regionSelectedValue == null || regionSelectedValue == 0) ? true : item.RegionId.Equals(regionSelectedValue))
                                                                                                     && ((CountrySelectedValue == null || CountrySelectedValue == 0) ? true : item.CountryId.Equals(CountrySelectedValue)) &&
                                                                                                     (brands == null ? true : brands.Contains(item.BrandId)));
                //&& ((propertyTypeSelectedValue == null || propertyTypeSelectedValue == 0) ? true : item.PropertyTypeId.Equals(propertyTypeSelectedValue)));
                if (filteredCollection != null)
                {
                    searchFilterCollection = filteredCollection.ToList<SearchFilters>();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }
        /// <summary>
        /// This method return the filter collection for brand, region and property type dropdown
        /// </summary>
        /// <param name="searchFilterDataCollection"></param>
        /// <param name="brandSelectedValue"></param>
        /// <param name="regionSelectedValue"></param>
        /// <param name="propertyTypeSelectedValue"></param>
        /// <param name="categorySelectedValue"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetFilteredCollection(IList<SearchFilters> searchFilterDataCollection, string brandSelectedValue, string regionSelectedValue, int? propertyTypeSelectedValue)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                var brands = (brandSelectedValue == null || brandSelectedValue == DropDownConstants.DefaultValue) ? null : brandSelectedValue.Split(',').Select(item => int.Parse(item));
                var regions = (regionSelectedValue == null || regionSelectedValue == DropDownConstants.DefaultValue) ? null : regionSelectedValue.Split(',').Select(item => int.Parse(item));
                IEnumerable<SearchFilters> filteredCollection = searchFilterDataCollection.Where(item => ((regions == null) ? true : regions.Contains(item.RegionId))
                                                                                                     && (brands == null ? true : brands.Contains(item.BrandId)));
                //&& ((propertyTypeSelectedValue == null || propertyTypeSelectedValue == 0) ? true : item.PropertyTypeId.Equals(propertyTypeSelectedValue)));
                if (filteredCollection != null)
                {
                    searchFilterCollection = filteredCollection.ToList<SearchFilters>();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }

        /// <summary>
        /// This method return the filter collection for brand, region, category and property type dropdown
        /// </summary>
        /// <param name="searchFilterDataCollection"></param>
        /// <param name="brandSelectedValue"></param>
        /// <param name="regionSelectedValue"></param>
        /// <param name="propertyTypeSelectedValue"></param>
        /// <param name="categorySelectedValue"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetFilteredCollection(IList<SearchFilters> searchFilterDataCollection, int? brandSelectedValue, int? regionSelectedValue, int? propertyTypeSelectedValue, int? categorySelectedValue)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                IEnumerable<SearchFilters> filteredCollection = searchFilterDataCollection.Where(item => ((regionSelectedValue == null || regionSelectedValue == 0) ? true : item.RegionId.Equals(regionSelectedValue))
                                                                                                     && ((brandSelectedValue == null || brandSelectedValue == 0) ? true : item.BrandId.Equals(brandSelectedValue))
                                                                                                     && ((categorySelectedValue == null || categorySelectedValue == 0) ? true : item.CatId.Equals(categorySelectedValue)));
                //&& ((propertyTypeSelectedValue == null || propertyTypeSelectedValue == 0) ? true : item.PropertyTypeId.Equals(propertyTypeSelectedValue)));
                if (filteredCollection != null)
                {
                    searchFilterCollection = filteredCollection.ToList<SearchFilters>();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }

        /// This method return the filter collection for brand, region,country, category and property type dropdown
        /// </summary>
        /// <param name="searchFilterDataCollection"></param>
        /// <param name="brandSelectedValue"></param>
        /// <param name="regionSelectedValue"></param>
        /// <param name="propertyTypeSelectedValue"></param>
        /// <param name="categorySelectedValue"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetFilteredCollectionForCountry(IList<SearchFilters> searchFilterDataCollection, int? brandSelectedValue, int? regionSelectedValue, int? countrySelectedValue, int? propertyTypeSelectedValue, int? categorySelectedValue)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                IEnumerable<SearchFilters> filteredCollection = searchFilterDataCollection.Where(item => ((regionSelectedValue == null || regionSelectedValue == 0) ? true : item.RegionId.Equals(regionSelectedValue)) &&
                                                                                                         ((countrySelectedValue == null || countrySelectedValue == 0) ? true : item.CountryId.Equals(countrySelectedValue))
                                                                                                     && ((brandSelectedValue == null || brandSelectedValue == 0) ? true : item.BrandId.Equals(brandSelectedValue))
                                                                                                     && ((categorySelectedValue == null || categorySelectedValue == 0) ? true : item.CatId.Equals(categorySelectedValue)));
                //&& ((propertyTypeSelectedValue == null || propertyTypeSelectedValue == 0) ? true : item.PropertyTypeId.Equals(propertyTypeSelectedValue)));
                if (filteredCollection != null)
                {
                    searchFilterCollection = filteredCollection.ToList<SearchFilters>();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }

        /// <summary>
        /// This method return the filter collection for brand, region, category and property type dropdown
        /// </summary>
        /// <param name="searchFilterDataCollection"></param>
        /// <param name="brandSelectedValue"></param>
        /// <param name="regionSelectedValue"></param>
        /// <param name="propertyTypeSelectedValue"></param>
        /// <param name="categorySelectedValue"></param>
        /// <returns></returns>
        public static IList<SearchFilters> GetFilteredCollection(IList<SearchFilters> searchFilterDataCollection, string brandSelectedValue, string regionSelectedValue, int? propertyTypeSelectedValue, int? categorySelectedValue)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                var brands = (brandSelectedValue == null || brandSelectedValue == DropDownConstants.DefaultValue) ? null : brandSelectedValue.Split(',').Select(item => int.Parse(item));
                var regions = (regionSelectedValue == null || regionSelectedValue == DropDownConstants.DefaultValue) ? null : regionSelectedValue.Split(',').Select(item => int.Parse(item));
                IEnumerable<SearchFilters> filteredCollection = searchFilterDataCollection.Where(item => ((regions == null) ? true : regions.Contains(item.RegionId))
                                                                                                      && (brands == null ? true : brands.Contains(item.BrandId))
                                                                                                     && ((categorySelectedValue == null || categorySelectedValue == 0) ? true : item.CatId.Equals(categorySelectedValue)));
                // && ((propertyTypeSelectedValue == null || propertyTypeSelectedValue == 0) ? true : item.PropertyTypeId.Equals(propertyTypeSelectedValue)));
                if (filteredCollection != null)
                {
                    searchFilterCollection = filteredCollection.ToList<SearchFilters>();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }


        /// <summary>
        /// This method return the search filter dropdown data
        /// </summary>
        /// <param name="dropDownType"></param>
        /// <param name="searchFilterDataCollection"></param>
        /// <returns></returns>
        public static IList<DropDownItem> GetSearchFilterDropDownData(string dropDownType, IList<SearchFilters> searchFilterDataCollection)
        {
            IList<DropDownItem> dropDownData = null;
            try
            {
                if (searchFilterDataCollection != null)
                {
                    dropDownData = new List<DropDownItem>();
                    switch (dropDownType)
                    {
                        case DropDownConstants.BrandDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.BrandDescription,
                                    DataValueField = item.BrandId.ToString(CultureInfo.InvariantCulture)
                                }

                                );
                            }
                            break;

                        case DropDownConstants.RegionDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.RegionDescription,
                                    DataValueField = item.RegionId.ToString(CultureInfo.InvariantCulture)
                                }

                                                );
                            }
                            break;

                        case DropDownConstants.PropertyTypesDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.PropertyTypeDescription,
                                    DataValueField = item.PropertyTypeId.ToString(CultureInfo.InvariantCulture)
                                }

                               );
                            }
                            break;

                        case DropDownConstants.OwnerDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection.Where(p => string.IsNullOrWhiteSpace(p.Owner) == false))
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.Owner,
                                    DataValueField = item.OwnerId
                                }

                               );
                            }
                            break;

                        case DropDownConstants.ProjectManagerDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection.Where(p => string.IsNullOrWhiteSpace(p.ProjectManager) == false))
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.ProjectManager,
                                    DataValueField = item.ProjectManagerId
                                }

                               );
                            }
                            break;

                        case DropDownConstants.O2OstatusDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.O2OStatus,
                                    DataValueField = item.O2OStatusId
                                }

                               );
                            }
                            break;

                        case DropDownConstants.CategoriesDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.CategoryDescription,
                                    DataValueField = item.CatId.ToString(CultureInfo.InvariantCulture)
                                }

                               );
                            }
                            break;

                        case DropDownConstants.CountryDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.CountryName,
                                    DataValueField = item.CountryId.ToString(CultureInfo.InvariantCulture)
                                }

                               );
                            }
                            break;

                        case DropDownConstants.ContactDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.ContactTypeDesc,
                                    DataValueField = item.ContactTypeId.ToString(CultureInfo.InvariantCulture)
                                }

                                                );
                            }
                            break;
                        case DropDownConstants.ContactDropDownpartnerProfile:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.ContactTypeDesc,
                                    DataValueField = item.ContactTypeId.ToString(CultureInfo.InvariantCulture)
                                }

                                                );
                            }
                            break;

                        case DropDownConstants.ProjectTypesDropDown:
                            foreach (SearchFilters item in searchFilterDataCollection)
                            {
                                dropDownData.Add(new DropDownItem()
                                {
                                    DataTextField = item.ProjectTypeDescription,
                                    DataValueField = item.ProjectTypeId.ToString(CultureInfo.InvariantCulture)
                                });
                            }
                            break;
                        default:
                            return null;
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
            // if (dropDownType != DropDownConstants.ContactDropDownpartnerProfile)
            return dropDownData.GroupBy(item => item.DataValueField).Select(grp => grp.First()).ToList<DropDownItem>();
            // else
            //   return dropDownData;
        }

        /// <summary>
        /// Process Drop Down Item Change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        /// <param name="brandDropDown"></param>
        /// <param name="regionDropDown"></param>
        /// <param name="propertyTypeDropDown"></param>
        public static void ProcessDropDownItemChange(IList<SearchFilters> searchFiltersData, CultureInfo culture, string sourceDropDown, DropDownList brandDropDown, DropDownList regionDropDown, DropDownList propertyTypeDropDown)
        {
            int brandSelectedValue = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int propertyTypeSelectedValue = 0; // Convert.ToInt32(propertyTypeDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int regionSelectedValue = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture);

            IList<SearchFilters> filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue);

            if (brandSelectedValue == 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
            }
            else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
            }
            else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
            }
            else
            {
                if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                    if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }

                else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0) // && (sourceDropDown == DropDownConstants.BrandDropDown)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0) //&& (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0) //&& (sourceDropDown == DropDownConstants.RegionDropDown)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                }

                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0)
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                    else // Added by amit
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
            }
        }

        /// <summary>
        /// Process Drop Down Item Change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        /// <param name="brandDropDown"></param>
        /// <param name="regionDropDown"></param>
        /// <param name="propertyTypeDropDown"></param>
        public static string ProcessDropDownItemChange(IList<SearchFilters> searchFiltersData, CultureInfo culture, string sourceDropDown, string brandSelectedValue, DropDownList regionDropDown, DropDownList propertyTypeDropDown)
        {
            int propertyTypeSelectedValue = 0; //Convert.ToInt32(propertyTypeDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int regionSelectedValue = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture);

            IList<SearchFilters> filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue);
            var selectedBrandIds = brandSelectedValue != DropDownConstants.DefaultValue ? brandSelectedValue : string.Join(",", searchFiltersData.Select(p => p.BrandId).Distinct());

            if (sourceDropDown == DropDownConstants.BrandDropDown)
            {
                IList<SearchFilters> brandsCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue);
                selectedBrandIds = string.Join(",", brandsCollection.Select(p => p.BrandId).Distinct());
            }

            if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
            }
            else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
            }
            else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                selectedBrandIds = string.Join(",", searchFiltersData.Select(p => p.BrandId).Distinct());
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
            }
            else
            {
                if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                    if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }

                else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != 0) // && (sourceDropDown == DropDownConstants.BrandDropDown)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != 0) //&& (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == 0) //&& (sourceDropDown == DropDownConstants.RegionDropDown)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                }

                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != 0)
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                    else // Added by amit
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
            }
            return selectedBrandIds;
        }


        //Abhishek Change
        public static string ProcessDropDownItemChangeForCountry(IList<SearchFilters> searchFiltersData, CultureInfo culture, string sourceDropDown, string brandSelectedValue, DropDownList regionDropDown, DropDownList CountryDropDown, DropDownList propertyTypeDropDown)
        {
            int propertyTypeSelectedValue = 0;// Convert.ToInt32(propertyTypeDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int regionSelectedValue = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int countrySelectedValue = Convert.ToInt32(CountryDropDown.SelectedValue, CultureInfo.InvariantCulture);

            IList<SearchFilters> filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue);
            var selectedBrandIds = brandSelectedValue != DropDownConstants.DefaultValue ? brandSelectedValue : string.Join(",", searchFiltersData.Select(p => p.BrandId).Distinct());

            if (sourceDropDown == DropDownConstants.BrandDropDown)
            {
                IList<SearchFilters> brandsCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (string)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue);
                selectedBrandIds = string.Join(",", brandsCollection.Select(p => p.BrandId).Distinct());
            }

            if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown))
            {
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, (int?)null);
                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, regionSelectedValue, countrySelectedValue, null);

                BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(219, culture), countrySelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
            }
            else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && countrySelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, null, null, propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
            }
            else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && countrySelectedValue == 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                selectedBrandIds = string.Join(",", searchFiltersData.Select(p => p.BrandId).Distinct());
                // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, (int?)null, (int?)null, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
            }
            else
            {
                if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                    if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, (int?)null, (int?)null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, regionSelectedValue, (int?)null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(searchFiltersData, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    }
                }

                else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && countrySelectedValue == 0) // && (sourceDropDown == DropDownConstants.BrandDropDown)
                {
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);

                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, regionSelectedValue, (int?)null, null);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                }
                else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && countrySelectedValue != 0) // && (sourceDropDown == DropDownConstants.BrandDropDown)
                {
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, (int?)null, (int?)null, propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, regionSelectedValue, countrySelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, (int?)null, (int?)null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, regionSelectedValue, (int?)null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                    else if (sourceDropDown == DropDownConstants.CountryDropDown)
                    {
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, null, null, countrySelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != 0) //&& (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null);
                    BindDropDown(searchFiltersData, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                }

                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == 0) //&& (sourceDropDown == DropDownConstants.RegionDropDown)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                }

                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != 0)
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(searchFiltersData, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (int?)null, propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(searchFiltersData, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue);

                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                    else // Added by amit
                    {
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue,CountryDropDown);
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(searchFiltersData, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    }
                }
                else
                {
                    // filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue);
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(211, culture), countrySelectedValue);
                    }
                }
            }
            return selectedBrandIds;
        }

        /// <summary>
        /// Process Drop Down Item Change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        /// <param name="brandDropDown"></param>
        /// <param name="regionDropDown"></param>
        /// <param name="propertyTypeDropDown"></param>
        /// <param name="categoriesDropDown"></param>
        public static Tuple<string, string> ProcessDropDownItemChange(IList<SearchFilters> searchFiltersData, CultureInfo culture, string sourceDropDown, string brandSelectedValue, string regionSelectedValue, DropDownList propertyTypeDropDown, DropDownList categoriesDropDown, int selectedCategoryId = 0, IList<SearchFilters> searchsData = null)
        {
            int propertyTypeSelectedValue = 0;//Convert.ToInt32(propertyTypeDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int categoriesSelectedValue = (selectedCategoryId == 0) ? Convert.ToInt32(categoriesDropDown.SelectedValue) : selectedCategoryId;

            var selectedBrandIds = brandSelectedValue != DropDownConstants.DefaultValue ? brandSelectedValue : string.Join(",", searchFiltersData.Select(p => p.BrandId).Distinct());
            var selectedRegionIds = regionSelectedValue != DropDownConstants.DefaultValue ? regionSelectedValue : string.Join(",", searchFiltersData.Select(p => p.RegionId).Distinct());

            if (sourceDropDown == DropDownConstants.BrandDropDown)
            {
                IList<SearchFilters> brandsCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                selectedBrandIds = string.Join(",", brandsCollection.Select(p => p.BrandId).Distinct());
            }
            if (sourceDropDown == DropDownConstants.RegionDropDown)
            {
                IList<SearchFilters> regionCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, (string)null, propertyTypeSelectedValue, categoriesSelectedValue);
                selectedRegionIds = string.Join(",", regionCollection.Select(p => p.RegionId).Distinct());
            }

            IList<SearchFilters> filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);

            if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue == DropDownConstants.DefaultValue && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                if (searchsData != null)
                {
                    categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, null, null, null);
                }
                else
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, null);
                }
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
            }
            else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
            {
                selectedRegionIds = string.Join(",", searchFiltersData.Select(p => p.RegionId).Distinct());
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
            }
            else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == DropDownConstants.DefaultValue && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
            }
            else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue == DropDownConstants.DefaultValue && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
            {
                selectedBrandIds = string.Join(",", searchFiltersData.Select(p => p.BrandId).Distinct());
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
            }
            else
            {
                if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (searchsData != null)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchsData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                    }
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }
                else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                    if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, categoriesSelectedValue);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                    if (searchsData != null)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, null, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    }
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == DropDownConstants.DefaultValue && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                    if (searchsData != null)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    }
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == DropDownConstants.DefaultValue && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);

                    selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);

                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }

                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue != 0)
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, categoriesSelectedValue);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (string)null, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, null, null, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (string)null, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                }
                else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == DropDownConstants.DefaultValue && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        }

                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue == DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, propertyTypeSelectedValue, null);
                    selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, null, propertyTypeSelectedValue, null);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue == DropDownConstants.DefaultValue && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue == 0 && regionSelectedValue != DropDownConstants.DefaultValue && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }

                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (string)null, regionSelectedValue, null, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                }
                else if (brandSelectedValue != DropDownConstants.DefaultValue && propertyTypeSelectedValue != 0 && regionSelectedValue == DropDownConstants.DefaultValue && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                    }
                }
                else
                {
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        selectedBrandIds = string.Join(",", filteredCollection.Select(p => p.BrandId).Distinct());
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        selectedRegionIds = string.Join(",", filteredCollection.Select(p => p.RegionId).Distinct());
                    }
                }
            }
            return new Tuple<string, string>(selectedBrandIds, selectedRegionIds);
        }

        /// <summary>
        /// Process Drop Down Item Change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        /// <param name="brandDropDown"></param>
        /// <param name="regionDropDown"></param>
        /// <param name="propertyTypeDropDown"></param>
        /// <param name="categoriesDropDown"></param>
        public static void ProcessDropDownItemChange(IList<SearchFilters> searchFiltersData, CultureInfo culture, string sourceDropDown, DropDownList brandDropDown, DropDownList regionDropDown, DropDownList propertyTypeDropDown, DropDownList categoriesDropDown, int selectedCategoryId = 0, IList<SearchFilters> searchsData = null)
        {
            int brandSelectedValue = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int propertyTypeSelectedValue = 0;// Convert.ToInt32(propertyTypeDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int regionSelectedValue = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int categoriesSelectedValue = (selectedCategoryId == 0) ? Convert.ToInt32(categoriesDropDown.SelectedValue) : selectedCategoryId;

            IList<SearchFilters> filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);

            if (brandSelectedValue == 0 && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                if (searchsData != null)
                {
                    categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, null, null, null);
                }
                else
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, null);
                }
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
            }
            else if (brandSelectedValue == 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
            }
            else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
            }
            else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
            }
            else
            {
                if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (searchsData != null)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                    }
                    //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }
                else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                    if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, regionSelectedValue, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                    if (searchsData != null)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, null, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    }
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        }
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        }
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                    if (searchsData != null)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    }
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        }

                        //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        }
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }

                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0)
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue == 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                }
                else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        }

                        //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        }

                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                }
                else
                {
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
            }
        }

        // For Country Change
        public static void ProcessDropDownItemChangeForCountry(IList<SearchFilters> searchFiltersData, CultureInfo culture, string sourceDropDown, DropDownList brandDropDown, DropDownList regionDropDown, DropDownList countryDropDown, DropDownList propertyTypeDropDown, DropDownList categoriesDropDown, int selectedCategoryId = 0, IList<SearchFilters> searchsData = null)
        {
            int brandSelectedValue = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int propertyTypeSelectedValue = 0;// Convert.ToInt32(propertyTypeDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int regionSelectedValue = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int countrySelectedValue = Convert.ToInt32(countryDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int categoriesSelectedValue = (selectedCategoryId == 0) ? Convert.ToInt32(categoriesDropDown.SelectedValue) : selectedCategoryId;

            IList<SearchFilters> filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);

            if (brandSelectedValue == 0 && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && countrySelectedValue == 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CountryDropDown))
            {
                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, null, null, null, categoriesSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                if (searchsData != null)
                {
                    categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, null, null, null, null);
                }
                else
                {
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, null, null, null, null);
                }
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
            }

            else if (brandSelectedValue == 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && countrySelectedValue == 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                if (searchsData != null)
                {

                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, null, null, null);
                }
                else
                {
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)regionSelectedValue, null, null, null, null);
                }

                //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
            }
            else if (brandSelectedValue == 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && countrySelectedValue != 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                if (searchsData != null)
                {

                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, countrySelectedValue, null, null);
                }
                else
                {
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, null, null);
                }

                BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

            }

            else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && countrySelectedValue == 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown || sourceDropDown == DropDownConstants.CountryDropDown))
            {
                //BindDropDown(searchFiltersData, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, null, null, propertyTypeSelectedValue, null);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                //BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
            }
            else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && countrySelectedValue == 0 && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown || sourceDropDown == DropDownConstants.CountryDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                if (searchsData != null)
                {

                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, null, null, null, null);
                }
                else
                {
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, null, null, null, null);
                }

                //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
            }
            else
            {
                if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && (countrySelectedValue == 0 || countrySelectedValue != 0) && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    if (countrySelectedValue == 0)
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                    else
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (searchsData != null)
                    {
                        if (countrySelectedValue == 0)
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, null, propertyTypeSelectedValue, null);
                        else
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {
                        if (countrySelectedValue == 0)
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, null, propertyTypeSelectedValue, null);
                        else
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);

                    }
                    //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }

                else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0 && (countrySelectedValue == 0 || countrySelectedValue != 0) && (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown || sourceDropDown == DropDownConstants.CountryDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                    if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        if (searchsData != null)
                        {
                            if (countrySelectedValue == 0)
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, null, propertyTypeSelectedValue, null);
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            if (countrySelectedValue == 0)
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, null, propertyTypeSelectedValue, null);
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        if (countrySelectedValue == 0)
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, null, null, categoriesSelectedValue);
                        else
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, null, categoriesSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            if (countrySelectedValue == 0)
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, null, propertyTypeSelectedValue, null);
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, (int?)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            if (countrySelectedValue == 0)
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, null, propertyTypeSelectedValue, null);
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);
                        }
                        //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        if (countrySelectedValue == 0)
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, null, null, categoriesSelectedValue);
                        else
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, null, categoriesSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && (countrySelectedValue == 0 || countrySelectedValue != 0) && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.PropertyTypesDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    if (countrySelectedValue == 0)
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, null, null, categoriesSelectedValue);
                    else
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                    if (searchsData != null)
                    {
                        if (countrySelectedValue == 0)
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, null, null, null);
                        else
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {

                        if (countrySelectedValue == 0)
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null, null);
                        else
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                    }
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0 && (countrySelectedValue == 0 || countrySelectedValue != 0) && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        if (searchsData != null)
                        {
                            if (countrySelectedValue == 0)
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, null, null, null);
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            if (countrySelectedValue == 0)
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, null, null, null);
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                        }
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        if (searchsData != null)
                        {
                            if (countrySelectedValue == 0)
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, null, null, null);
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            if (countrySelectedValue == 0)
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, null, null, null);
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                        }
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        if (countrySelectedValue == 0)
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, null, null, categoriesSelectedValue);
                        else
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }

                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && (countrySelectedValue == 0 || countrySelectedValue != 0) && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    // BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);




                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                    if (searchsData != null)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                    }
                    else
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    }
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && categoriesSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        }

                        //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        }
                        // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && (countrySelectedValue == 0 || countrySelectedValue != 0) && categoriesSelectedValue == 0 && (sourceDropDown == DropDownConstants.CategoriesDropDown))
                {
                    if (countrySelectedValue == 0)
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                    else
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    if (countrySelectedValue == 0)
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    else
                        filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);

                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue == 0 && (countrySelectedValue == 0 || countrySelectedValue != 0) && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        if (countrySelectedValue == 0)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                        }
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        if (countrySelectedValue == 0)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, (int?)null, regionSelectedValue, countrySelectedValue, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        if (searchsData != null)
                        {
                            if (countrySelectedValue == 0)
                            {
                                filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, null, null);
                            }
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchsData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                        }

                        else
                        {
                            if (countrySelectedValue == 0)
                            {
                                filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                            }
                            else
                                filteredCollection = Helper.GetFilteredCollectionForCountry(searchFiltersData, brandSelectedValue, regionSelectedValue, countrySelectedValue, null, null);
                        }
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }

                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && (countrySelectedValue == 0 || countrySelectedValue != 0) && categoriesSelectedValue != 0)
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);

                        }
                        //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);


                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, categoriesSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue == 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, regionSelectedValue, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                }
                else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, categoriesSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        }

                        //  filteredCollection = Helper.GetFilteredCollection(searchFiltersData, null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, (int?)null, null, propertyTypeSelectedValue, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        }

                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                }
                else if (brandSelectedValue == 0 && propertyTypeSelectedValue != 0 && regionSelectedValue != 0 && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue == 0 && categoriesSelectedValue != 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, categoriesSelectedValue);
                    //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        }
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        if (searchsData != null)
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchsData, brandSelectedValue, null, null, null);
                            categoriesSelectedValue = Convert.ToInt32(categoriesDropDown.SelectedValue);
                        }
                        else
                        {
                            filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        }
                        //filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, null, categoriesSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue == 0 && regionSelectedValue != 0 && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null, null);
                    //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, regionSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && propertyTypeSelectedValue != 0 && regionSelectedValue == 0 && categoriesSelectedValue == 0)
                {
                    filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, propertyTypeSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown || sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);

                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null, null);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        filteredCollection = Helper.GetFilteredCollection(searchFiltersData, (int?)null, null, propertyTypeSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                    }
                }
                else
                {
                    // filteredCollection = Helper.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, propertyTypeSelectedValue, categoriesSelectedValue);
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.PropertyTypesDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), categoriesSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.CategoriesDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(204, culture), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), regionSelectedValue);
                        //BindDropDown(filteredCollection, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(214, culture), propertyTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), countrySelectedValue);
                    }
                }
            }
        }

        /// <summary>
        /// Bind Drop Down
        /// </summary>
        /// <param name="searchFilters"></param>
        /// <param name="dropDownType"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        /// <param name="selectedValue"></param>
        private static void BindDropDown(IList<SearchFilters> searchFiltersData, string dropDownType, DropDownList sourceDropDown, string defaultText, int? selectedValue)
        {
            if (string.Compare(dropDownType, DropDownConstants.PropertyTypesDropDown) == 0) return;
            sourceDropDown.DataSource = Helper.GetSearchFilterDropDownData(dropDownType, searchFiltersData).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false).OrderBy(p => p.DataTextField);
            sourceDropDown.DataTextField = "DataTextField";
            sourceDropDown.DataValueField = "DataValueField";
            sourceDropDown.DataBind();
            if (!string.IsNullOrWhiteSpace(defaultText))
                sourceDropDown.Items.Insert(0, new ListItem(defaultText, "0"));
            if (selectedValue.HasValue && selectedValue > 0)
                sourceDropDown.SelectedValue = selectedValue.ToString();
        }
    }
}