﻿using System;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Infrastructure
{
    public static class ConfigurationStore
    {
        /// <summary>
        /// This method get the application error message for specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetApplicationMessages(string key, CultureInfo culture = null)
        {
            ConfigurationSetting configurationSetting;
            string cultureCode;
            if (culture != null)
            {
                cultureCode = culture.Name;
            }
            else
            {
                cultureCode = null;
            }

            //Getting the configuration settings from cache
            configurationSetting = Caching.Get<ConfigurationSetting>(CacheConstants.ConfigurationSettings);
            if (configurationSetting == null || configurationSetting.ApplicationMessages.Count == 0)
            {
                configurationSetting = HelperManager.Instance.GetConfigurationSettings;
                string cacheAbsoluteExpiration = configurationSetting.AppSettings.FirstOrDefault(x => x.Key == AppSettingConstants.ConfigurationSettingsAbsoluteExpiration).Value;

                //Setting the configuration settings in cache
                Caching.Clear(CacheConstants.ConfigurationSettings);
                if (cacheAbsoluteExpiration != "0")
                    Caching.Add<ConfigurationSetting>(configurationSetting, CacheConstants.ConfigurationSettings, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
            }
            var message = configurationSetting.ApplicationMessages.FirstOrDefault(x => x.MessageCode == key && ((cultureCode == null && x.IsNeutralLanguage == true) || (x.CultureCode == cultureCode)));
            if (message != null)
                return message.Message.Trim();
            else
                return string.Empty;
        }

        /// <summary>
        /// This method get the application setting value for specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSetting(string key)
        {
            ConfigurationSetting configurationSetting;

            //Getting the configuration settings from cache
            configurationSetting = Caching.Get<ConfigurationSetting>(CacheConstants.ConfigurationSettings);
            if (configurationSetting == null || configurationSetting.AppSettings.Count == 0)
            {
                configurationSetting = HelperManager.Instance.GetConfigurationSettings;
                string cacheAbsoluteExpiration = configurationSetting.AppSettings.FirstOrDefault(x => x.Key == AppSettingConstants.ConfigurationSettingsAbsoluteExpiration).Value;

                //Setting the configuration settings in cache
                Caching.Clear(CacheConstants.ConfigurationSettings);
                if (cacheAbsoluteExpiration != "0")
                    Caching.Add<ConfigurationSetting>(configurationSetting, CacheConstants.ConfigurationSettings, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
            }
            return configurationSetting.AppSettings.FirstOrDefault(x => x.Key == key).Value;
        }
    }
}