﻿using System;
using System.Drawing;
using System.IO;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Utilities
{
    public static class Extensions
    {
        /// <summary>
        /// This method replace the empty space with non breaking space to preventing content wrap
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ReplaceEmptyString(this String text)
        {
            if (!string.IsNullOrWhiteSpace(text))
                return text.Replace(" ", "&nbsp;");
            else
                return string.Empty;
        }

        /// <summary>
        /// This method convert the image to thumbnail
        /// </summary>
        /// <param name="imageBytes"></param>
        /// <returns></returns>
        public static byte[] ConvertToThumbnail(this byte[] imageBytes)
        {
            MemoryStream stream = new MemoryStream();

            Image image = Image.FromStream(new MemoryStream(imageBytes));
            int ImageWidth = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.ThumbnailImageWidth));
            int ImageHeight = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.ThumbnailImageHeight));
            Image thumbnailImage = image.GetThumbnailImage(ImageWidth, ImageHeight, null, new IntPtr(0));
            // 150, 100,
            thumbnailImage.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

            return stream.ToArray();
        }

        /// <summary>
        /// This method convert the image to thumbnail
        /// </summary>
        /// <param name="imageBytes"></param>
        /// <returns></returns>
        public static byte[] ConvertToThumbnail(this byte[] imageBytes, int width, int height)
        {
            MemoryStream stream = new MemoryStream();
            Image image = Image.FromStream(new MemoryStream(imageBytes));
            Image thumbnailImage = image.GetThumbnailImage(width, height, null, new IntPtr(0));

            thumbnailImage.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

            return stream.ToArray();
        }
    }
}