﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Utilities
{
    public static class ControlValidatorUtility
    {
        /// <summary>.
        /// This method get the list of regular Expressions and store in cache
        /// </summary>
        /// <returns></returns>
        public static IList<RegularExpressionTypeDetails> GetRegularExpressionTypes()
        {
            IList<RegularExpressionTypeDetails> regularExpressionTypeCollection;
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.RegularExpressionAbsoluteExpiration);
            if (cacheAbsoluteExpiration == "0")
            {
                Caching.Clear(CacheConstants.RegularExpressionTypeDetails);
            }

            if (Caching.Exists(CacheConstants.RegularExpressionTypeDetails))
            {
                regularExpressionTypeCollection = Caching.Get<IList<RegularExpressionTypeDetails>>(CacheConstants.RegularExpressionTypeDetails);
            }
            else
            {
                regularExpressionTypeCollection = HelperManager.Instance.GetRegularExpressionTypes;
                if (regularExpressionTypeCollection.Count > 0)
                {
                    Caching.Clear(CacheConstants.RegularExpressionTypeDetails);
                    if (cacheAbsoluteExpiration != "0")
                        Caching.Add<IList<RegularExpressionTypeDetails>>(regularExpressionTypeCollection, CacheConstants.RegularExpressionTypeDetails, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
                }
            }
            return regularExpressionTypeCollection;
        }

        /// <summary>
        /// Get localized regular expression
        /// </summary>
        /// <param name="regularExpressionTypeId"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static string GetLocalizedRegularExpression(int regularExpressionTypeId, CultureInfo culture)
        {
            if (culture != null)
            {
                string cultureCode = culture.Name;

                IList<RegularExpressionTypeDetails> regularExpressionTypeCollection = GetRegularExpressionTypes();
                if (regularExpressionTypeCollection != null && regularExpressionTypeCollection.Count > 0)
                {
                    IList<RegularExpressionTypeDetails> regularExpressionTypeDetails = regularExpressionTypeCollection.Where(x => x.RegularExpressionTypeId == regularExpressionTypeId && ((cultureCode == null && x.IsNeutralLanguage == true) || (x.CultureCode == cultureCode))).ToList();
                    if (regularExpressionTypeDetails.Count > 0)
                    {
                        return regularExpressionTypeDetails.First().RegularExpression.Trim();
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>.
        /// This method get the list of regular Expressions and store in cache
        /// </summary>
        /// <returns></returns>
        public static IList<ValidationMessageDetails> GetValidationMessages()
        {
            IList<ValidationMessageDetails> validationMessageCollection;
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.ValidationMessagesAbsoluteExpiration);
            if (cacheAbsoluteExpiration == "0")
            {
                Caching.Clear(CacheConstants.ValidationMessageDetails);
            }

            if (Caching.Exists(CacheConstants.ValidationMessageDetails))
            {
                validationMessageCollection = Caching.Get<IList<ValidationMessageDetails>>(CacheConstants.ValidationMessageDetails);
            }
            else
            {
                validationMessageCollection = HelperManager.Instance.GetValidationMessages;
                if (validationMessageCollection.Count > 0)
                {
                    Caching.Clear(CacheConstants.ValidationMessageDetails);
                    if (cacheAbsoluteExpiration != "0")
                        Caching.Add<IList<ValidationMessageDetails>>(validationMessageCollection, CacheConstants.ValidationMessageDetails, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
                }
            }
            return validationMessageCollection;
        }

        /// <summary>
        /// Get localized validation message
        /// </summary>
        /// <param name="regularExpressionTypeId"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static string GetLocalizedValidationMessage(int validationMessageId, CultureInfo culture)
        {
            if (culture != null)
            {
                string cultureCode = culture.Name;

                IList<ValidationMessageDetails> validationMessageCollection = GetValidationMessages();
                if (validationMessageCollection != null && validationMessageCollection.Count > 0)
                {
                    IList<ValidationMessageDetails> validationMessageDetails = validationMessageCollection.Where(x => x.ValiationMessageId == validationMessageId && ((cultureCode == null && x.IsNeutralLanguage == true) || (x.CultureCode == cultureCode))).ToList();
                    if (validationMessageDetails.Count > 0)
                    {
                        return validationMessageDetails.First().Message.Trim();
                    }
                }
            }
            return string.Empty;
        }
    }
}