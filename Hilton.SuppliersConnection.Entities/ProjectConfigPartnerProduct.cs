﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProjectConfigPartnerProduct
    {
        public int CategoryId
        {
            get;
            set;
        }

        public bool IsOther
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public string PartnerName
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public string SkuNumber
        {
            get;
            set;
        }

        public byte[] ProductImage
        {
            get;
            set;
        }

        public byte[] ProductThumbnailImage
        {
            get;
            set;
        }

        public byte[] PartnerImage
        {
            get;
            set;
        }

        public string ProductDescription
        {
            get;
            set;
        }

        public string ContactFirstName
        {
            get;
            set;
        }

        public string ContactLastName
        {
            get;
            set;
        }

        public string ContactEmail
        {
            get;
            set;
        }

        public string SpecSheetPDFName
        {
            get;
            set;
        }
    }
}