﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class GeneralSettings
    {
        public bool FeaturedProjectVisibility
        {
            get;
            set;
        }

        public string FeedbackEmail
        {
            get;
            set;
        }

        public string GoldPartnershipAmount
        {
            get;
            set;
        }

        public string PartnerAccountRequestEmail
        {
            get;
            set;
        }

        public string ProductRequestEmail
        {
            get;
            set;
        }

        public string RegularPartnershipAmount
        {
            get;
            set;
        }

        public IList<Tuple<string, string>> TermsAndConditions
        {
            get;
            set;
        }
    }
}