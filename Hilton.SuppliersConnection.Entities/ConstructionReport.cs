﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ConstructionReport
    {
        public string ConstructionCategory
        {
            get;
            set;
        }

        public string ConstructionDate
        {
            get;
            set;
        }

        public string ConstructionIsRepContacted
        {
            get;
            set;
        }

        public string ConstructionLeadType
        {
            get;
            set;
        }

        public string ConstructionProduct
        {
            get;
            set;
        }

        public string ConstructionProjectType
        {
            get;
            set;
        }

        public string ConstructionProperty
        {
            get;
            set;
        }

        public DateTime ConstructionRepContactedDate
        {
            get;
            set;
        }
    }
}