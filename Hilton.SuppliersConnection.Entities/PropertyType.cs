﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PropertyType
    {
        public string PropertyTypeDescription
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }
    }
}