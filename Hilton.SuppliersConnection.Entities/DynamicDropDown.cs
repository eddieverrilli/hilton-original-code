﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class MessageDropDown
    {
        public IList<DropDownItem> RecipientDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> SenderDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> RegionDropDown
        {
            get;
            set;
        }
    }
}