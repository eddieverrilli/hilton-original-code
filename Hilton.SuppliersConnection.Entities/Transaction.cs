﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Transaction
    {
        public DateTime FromDate
        {
            get;
            set;
        }

        public decimal? GoldTransactionsAmount
        {
            get;
            set;
        }

        public int GoldTransactionsNo
        {
            get;
            set;
        }

        public decimal? RegularTransactionsAmount
        {
            get;
            set;
        }

        public int RegularTransactionsNo
        {
            get;
            set;
        }

        public DateTime ToDate
        {
            get;
            set;
        }

        public decimal? Total
        {
            get;
            set;
        }

        public int TotalRecords
        {
            get;
            set;
        }

        public IList<TransactionLineItem> Transactions
        {
            get;
            set;
        }
    }
}