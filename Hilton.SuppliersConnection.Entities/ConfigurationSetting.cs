﻿using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    public class ConfigurationSetting
    {
        public IList<ApplicationMessageDetails> ApplicationMessages
        {
            get;
            set;
        }

        public IList<KeyValuePair<string, string>> AppSettings
        {
            get;
            set;
        }
    }
}