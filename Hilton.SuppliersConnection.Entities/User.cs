﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class User
    {
        public DateTime? ActivationDate
        {
            get;
            set;
        }

        public IList<UserActivationEmail> ActivationEmailDetails
        {
            get;
            set;
        }

        public AdminPermission AdminPermission
        {
            get;
            set;
        }

        public IList<Region> UserRegions
        {
            get;
            set;
        }

        public IList<AssociatedProject> AssociatedProjects
        {
            get;
            set;
        }

        public string Company
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public int HiltonUserId
        {
            get;
            set;
        }

        public string HiltonUserName
        {
            get;
            set;
        }

        public bool IsRemoved
        {
            get;
            set;
        }

        public bool ShowTermsAndCondition
        {
            get;
            set;
        }

        public string TermsAndCondition
        {
            get;
            set;
        }

        public string TermsAndConditionVersion
        {
            get;
            set;
        }

        public DateTime? LastLogOn
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public int ProjectId
        {
            get;
            set;
        }

        public IList<Permission> Permissions
        {
            get;
            set;
        }

        public IList<Menu> MenuAccess
        {
            get;
            set;
        }

        public IList<UserAccessLog> UserAccessLogs
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public string UserRole
        {
            get;
            set;
        }

        public int? UserRoleId
        {
            get;
            set;
        }

        public string UserStatus
        {
            get;
            set;
        }

        public int UserStatusId
        {
            get;
            set;
        }

        public string UserTypeDescription
        {
            get;
            set;
        }

        public int UserTypeId
        {
            get;
            set;
        }

        public bool IsPartnershipExpired
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int PartnershipType
        {
            get;
            set;
        }

        public bool ReceivePartnerCommunication
        {
            get;
            set;
        }

        public IList<Category> AccessibleCategoryList
        {
            get;
            set;
        }

        public int UpdatedByUserId
        {
            get;
            set;
        }

        public string Culture
        {
            get;
            set;
        }

        public string HiltonUserNumber
        {
            get;
            set;
        }
        public IList<Partner> AssociatedPartners
        {
            get;
            set;
        }
    }
}
