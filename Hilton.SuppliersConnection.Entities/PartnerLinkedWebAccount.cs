﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerLinkedWebAccount
    {
        public int PartnerId
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string HiltonId
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string StatusDesc
        {
            get;
            set;
        }

        public int CreatedByUserId
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public string Culture
        {
            get;
            set;
        }
    }
}