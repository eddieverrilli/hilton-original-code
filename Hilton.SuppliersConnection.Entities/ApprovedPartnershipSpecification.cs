﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ApprovedPartnershipSpecification
    {
        public int CategoryId
        {
            get;
            set;
        }

        public string CategoryName
        {
            get;
            set;
        }

        public int Level
        {
            get;
            set;
        }

        public int ParentCategoryId
        {
            get;
            set;
        }

        public string Sequence
        {
            get;
            set;
        }
    }
}