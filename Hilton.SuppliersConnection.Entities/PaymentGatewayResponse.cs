﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PaymentGatewayResponse
    {
        public string TransactionId
        {
            get;
            set;
        }

        public string TrasactionStatus
        {
            get;
            set;
        }

        public string TrasactionApprovalCode
        {
            get;
            set;
        }

        public string ErrorCode
        {
            get;
            set;
        }

        public string ErrorName
        {
            get;
            set;
        }

        public string Amount
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            get;
            set;
        }

        public DateTime TransactionDatetime
        {
            get;
            set;
        }
    }
}