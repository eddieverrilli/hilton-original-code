﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Message
    {
        public int BrandId
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public string CategoryName
        {
            get;
            set;
        }

        public string Details
        {
            get;
            set;
        }

        public DateTime MessageDateTime
        {
            get;
            set;
        }

        public int MessageId
        {
            get;
            set;
        }

        public int MessageStatusId
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }

        public string RecipientName
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }

        public string SenderName
        {
            get;
            set;
        }

        public string SenderType
        {
            get;
            set;
        }

        public int SenderId
        {
            get;
            set;
        }

        public int StatusId
        {
            get;
            set;
        }

        public int TemplateId
        {
            get;
            set;
        }

        public string Address
        {
            get;

            set;
        }

        public string CategoryDisplayName
        {
            get;

            set;
        }

        public string ToFirstName
        {
            get;
            set;
        }

        public string ToLastName
        {
            get;
            set;
        }

        public bool IsCopyToSelf
        {
            get;
            set;
        }

        public string ToEmail
        {
            get;
            set;
        }

        public int ProjectId
        {
            get;
            set;
        }

        public int CatId
        {
            get;
            set;
        }

        public string culture
        {
            get;
            set;
        }
    }
}