﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeMeeting
    {
        public int MeetingId { get; set; }
        public String MeetingType { get; set; }
        public String Company { get; set; }
        public String Contact { get; set; }
        public String Phone { get; set; }
        public String Products { get; set; }
        public bool BrandStandardCompliance { get; set; }
        public bool AIAAccredited { get; set; }
        public String AIANumCEUs { get; set; }
        public bool LEEDAccredited { get; set; }
        public String LEEDNumCEUs { get; set; }
        public String SubmittedByEmail { get; set; }
    }
}
