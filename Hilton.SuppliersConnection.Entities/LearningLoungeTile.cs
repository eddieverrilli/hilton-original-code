﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeTile
    {
        public int TileImageId { get; set; }
        public String Image { get; set; }
        public int ClickActionTypeId { get; set; }
        public String ClickActionTypeName { get; set; }
        public String ClickActionURL { get; set; }
        public String ClickActionTarget { get; set; }
        public int NumVerticalSpaces { get; set; }
        public int NumHorizontalSpaces { get; set; }
        public bool HasRollover { get; set; }
        public bool Enabled { get; set; }
        public int RestrictToUserTypeId { get; set; }
        public List<LearningLoungeTemplateContent> TemplateContent { get; set; }
        public int TemplateId { get; set; }
        public bool AllowDelete { get; set; }
    }
}
