﻿using System;
using System.Collections.Generic; 

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Feedback
    {
        public string Details
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public int SenderId
        {
            get;
            set;
        }

        public int ReceipentId
        {
            get;
            set;
        }

        public int FeedbackId
        {
            get;
            set;
        }

        public int FeedbackStatusId
        {
            get;
            set;
        }

        public string From
        {
            get;
            set;
        }

        public string PartnerCompany
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public string RatingDescription
        {
            get;
            set;
        }

        public int RatingId
        {
            get;
            set;
        }

        public DateTime SubmittedDateTime
        {
            get;
            set;
        }

        public int TemplateId
        {
            get;
            set;
        }

        public string ToFirstName
        {
            get;
            set;
        }

        public string ToLastName
        {
            get;
            set;
        }

        public string ToEmail
        {
            get;
            set;
        }

        public string culture
        {
            get;
            set;
        }

        public string SenderHiltonId
        {
            get;
            set;
        }

        public string SenderFirstName
        {
            get;
            set;
        }

        public string SenderLastName
        {
            get;
            set;
        }

        public string SenderEmail
        {
            get;
            set;
        }

        public string FeedbackStatusDesc
        {
            get;
            set;
        }

        public List<FeedbackComment> FeedbackComments
        {
            get;
            set;
        }

    }
}
