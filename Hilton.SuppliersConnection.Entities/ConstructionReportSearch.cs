﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ConstructionReportSearch
    {
        public string StartPage
        {
            get;
            set;
        }

        public string EndPage
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }
    }
}