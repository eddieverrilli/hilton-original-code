﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerProdOptionSearch
    {
        public int PageIndex
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public int CategoryId
        {
            get;
            set;
        }

        public int ProjectId
        {
            get;
            set;
        }
    }
}