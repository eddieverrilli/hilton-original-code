﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class EmailTemplateSearch
    {
        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }
    }
}