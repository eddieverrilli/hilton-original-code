﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeTemplateContent
    {
        public int TemplateId { get; set; }
        public String ContentName { get; set; }
        public String ContentType { get; set; }
        public String ContentText { get; set; }
        public String ContentImage { get; set; }
    }
}
