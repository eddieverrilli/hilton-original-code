﻿using System;
 
namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerProductTrial
    {
        public int CatId
        {
            get;
            set;
        }

        public int Row
        {
            get;
            set;
        }

        public string CategoryDisplayName
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public string BrandName
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }

        public string PropertyTypeName
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }

        public string RegionName
        {
            get;
            set;
        }
        public int CountryId
        {
            get;
            set;
        }

        public string CountryName
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public byte[] CompanyLogo
        {
            get;
            set;
        }

        public byte[] ProductImage
        {
            get;
            set;
        }

        public string ImageName
        {
            get;
            set;
        }

        public string ProductDescription
        {
            get;
            set;
        }

        public string SkuNumber
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public string CompanyName
        {
            get;
            set;
        }

        public string SpecSheetPDFName
        {
            get;
            set;
        }
    }
}