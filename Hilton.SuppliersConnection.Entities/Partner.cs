﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Partner
    {
        public string City
        {
            get;
            set;
        }

        public string CompanyAddress1
        {
            get;
            set;
        }

        public string CompanyAddress2
        {
            get;
            set;
        }

        public string CompanyDescription
        {
            get;
            set;
        }

        public byte[] CompanyLogoImage
        {
            get;
            set;
        }

        public byte[] CompanyLogoThumbnailImage
        {
            get;
            set;
        }

        public string CompanyLogoName
        {
            get;
            set;
        }

        public string CompanyName
        {
            get;
            set;
        }

        public IList<Contact> Contacts
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public bool IsSAM
        {
            get;
            set;
        }

        public bool IsRecPartner
        {
            get;
            set;
        }

        public PartnerCategories PartnerCategory
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }

        public PaymentRequestDetails PaymentRequestDetails
        {
            get;
            set;
        }

        public DateTime? PartnershipExpirationDate
        {
            get;
            set;
        }

        public DateTime? PartnershipReqSendDate
        {
            get;
            set;
        }

        public string PartnershipStatusDesc
        {
            get;
            set;
        }

        public int PartnershipStatusId
        {
            get;
            set;
        }

        public string PrimaryContactName
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public string ZipCode
        {
            get;
            set;
        }

        public bool IsAutomatedRenewalEmailReq
        {
            get;
            set;
        }

        public bool SubmitProducts
        {
            get;
            set;
        }

        public int CountryId
        {
            get;
            set;
        }

        public int StateId
        {
            get;
            set;
        }

        public DateTime? ActiveDate
        {
            get;
            set;
        }

        public DateTime? ExpirationDate
        {
            get;
            set;
        }

        public IList<PartnerLinkedWebAccount> AssociatedWebAccount
        {
            get;
            set;
        }

        public int PartnershipTypeId
        {
            get;
            set;
        }

        public bool IsOther
        {
            get;
            set;
        }

        public string PartnershipTypeDesc
        {
            get;
            set;
        }

        public string WebSiteLink 
        {
            get;
            set;
        }
        public bool IsPartnershipExpired
        {
            get;
            set;
        }
        public int UserId
        {
            get;
            set;
        }
        public string flagDelPartner
        {
            get;
            set;
        }

    }
}