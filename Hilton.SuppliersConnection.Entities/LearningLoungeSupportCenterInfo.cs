﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeSupportCenterInfo
    {
        public String About { get; set; }
        public String ContactInfo { get; set; }
    }
}
