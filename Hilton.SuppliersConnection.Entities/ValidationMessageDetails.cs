﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ValidationMessageDetails
    {
        public string CultureCode
        {
            get;
            set;
        }

        public int CultureTypeId
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public bool IsNeutralLanguage
        {
            get;
            set;
        }

        public int ValiationMessageId
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }
    }
}