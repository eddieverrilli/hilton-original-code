﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class DynamicDropDownData
    {
        public Collection<KeyValuePair<string, string>> SectionDataList
        {
            get;
            set;
        }

        public Collection<KeyValuePair<string, string>> TitleDataList
        {
            get;
            set;
        }
    }
}