﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class UserActivationEmail
    {
        public string Email
        {
            get;
            set;
        }

        public DateTime RequestSentDateTime
        {
            get;
            set;
        }
    }
}