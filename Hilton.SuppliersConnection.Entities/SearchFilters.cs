﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class SearchFilters
    {
        public string BrandDescription { get; set; }

        public int BrandId { get; set; }

        public string CategoryDescription { get; set; }

        public int CatId { get; set; }

        public int CountryId { get; set; }

        public string CountryName { get; set; }

        public string O2OStatus { get; set; }

        public string O2OStatusId { get; set; }

        public string Owner { get; set; }

        public string OwnerId { get; set; }

        public string ProjectManager { get; set; }

        public string ProjectManagerId { get; set; }

        public string ProjectTypeDescription { get; set; }

        public int ProjectTypeId { get; set; }

        public string PropertyTypeDescription { get; set; }

        public int PropertyTypeId { get; set; }

        public string RegionDescription { get; set; }

        public int RegionId { get; set; }

        public int ContactTypeId { get; set; }

        public string ContactTypeDesc { get; set; }
    }
}