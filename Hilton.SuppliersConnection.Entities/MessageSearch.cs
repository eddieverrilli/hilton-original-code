﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class MessageSearch
    {
        public bool AllClick
        {
            get;
            set;
        }

        public DateTime? DateFrom
        {
            get;
            set;
        }

        public DateTime? DateTo
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string QuickSearchExpression
        {
            get;
            set;
        }

        public string RecipientId
        {
            get;
            set;
        }

        public string RegionId
        {
            get;
            set;
        }

        public string SearchFlag
        {
            get;
            set;
        }

        public string SenderId
        {
            get;
            set;
        }

        public string SenderType
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public int TotalRecordCount
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
    }
}