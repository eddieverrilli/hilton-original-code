﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class TermsAndConditions
    {
        public int VersionId
        {
            get;
            set;
        }

        public string TermsAndConditionDesc
        {
            get;
            set;
        }

        public string IPAddress
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }
    }
}