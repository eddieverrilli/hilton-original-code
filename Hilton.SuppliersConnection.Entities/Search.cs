﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Search
    {
        public string id 
        { 
            get; 
            set; 
        }
        public string name 
        { 
            get; 
            set; 
        }
        public string type 
        { 
            get; 
            set; 
        }
    }
}
