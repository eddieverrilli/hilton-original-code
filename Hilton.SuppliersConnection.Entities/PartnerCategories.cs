﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerCategories
    {
        public string CategoryDisplayName
        {
            get;
            set;
        }

        public int CategoryId
        {
            get;
            set;
        }

        public IList<PartnerCategoryDetails> PartnerCategoriesDetails
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public bool UpdateFlag
        {
            get;
            set;
        }
    }
}