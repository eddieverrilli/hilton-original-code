﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Contact
    {
        public string ContactId
        {
            get;
            set;
        }

        public int ContactTypeId
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string Fax
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public bool IsSameAsPrimary
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }

        public string RegionName
        {
            get;
            set;
        }
        public int CountryId
        {
            get;
            set;
        }

        public string CountryName
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }

        public string ContactTypeDesc
        {
            get;
            set;
        }

        public int ContactTypeIdIndex
        {
            get;
            set;
        }

        public string Address1
        {
            get;
            set;
        }

        public string Address2
        {
            get;
            set;
        }

        public string Website
        {
            get;
            set;
        }
    }
}