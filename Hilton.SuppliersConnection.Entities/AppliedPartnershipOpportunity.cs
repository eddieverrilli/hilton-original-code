﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class AppliedPartnershipOpportunity
    {
        public int CategoryId
        {
            get;
            set;
        }

        public string CategoryDescription
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public string BrandName
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }

        public string PropertyTypeName
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }

        public string RegionName
        {
            get;
            set;
        }
        public int CountryId
        {
            get;
            set;
        }

        public string CountryName
        {
            get;
            set;
        }
    }
}