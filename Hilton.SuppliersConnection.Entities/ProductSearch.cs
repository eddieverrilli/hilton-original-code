﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProductSearch
    {
        public bool AllClick
        {
            get;
            set;
        }

        public string BrandId
        {
            get;
            set;
        }

        public string CategoryId
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSet
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }

        public string PropertyTypeId
        {
            get;
            set;
        }

        public string QuickSearchExpression
        {
            get;
            set;
        }

        public string RegionId
        {
            get;
            set;
        }
        public string CountryId
        {
            get;
            set;
        }
        public string SearchFlag
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public string StatusId
        {
            get;
            set;
        }

        public string SubCategoryId
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public int TotalRecordCount
        {
            get;
            set;
        }

        public string BrandCollection
        {
            get;
            set;
        }
    }
}