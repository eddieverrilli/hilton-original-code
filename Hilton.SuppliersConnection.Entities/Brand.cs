﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Brand
    {
        public string BrandDescription
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public string BrandSegment
        {
            get;
            set;
        }
    }
}