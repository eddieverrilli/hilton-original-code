﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerSearch
    {
        public bool AllClick
        {
            get;
            set;
        }

        public string BrandId
        {
            get;
            set;
        }

        public string CategoryId
        {
            get;
            set;
        }

        public string CountryId
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }
        public string primary_contactId
        {
            get;
            set;
        }

        public string secondary_contactId
        {
            get;
            set;
        }
        public string regional_contactId
        {
            get;
            set;
        }
        public string brandSegmentId
        {
            get;
            set;
        }
        public string states
        {
            get;
            set;
        }

        public DateTime? ExpirationDateFrom
        {
            get;
            set;
        }

        public DateTime? ExpirationDateTo
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSet
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string PropertyTypeId
        {
            get;
            set;
        }

        public string QuickSearchExpression
        {
            get;
            set;
        }

        public string RegionId
        {
            get;
            set;
        }

        public DateTime? RequestDateFrom
        {
            get;
            set;
        }

        public DateTime? RequestDateTo
        {
            get;
            set;
        }

        public string SearchFlag
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public string StatusId
        {
            get;
            set;
        }

        public int TotalRecordCount
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public string StartPage
        {
            get;
            set;
        }

        public string EndPage
        {
            get;
            set;
        }

        public string BrandCollection
        {
            get;
            set;
        }

        public string RegionCollection
        {
            get;
            set;
        }

        public string SubCategoryId
        {
            get;
            set;
        }

        public string PartnershipTypeId
        {
            get;
            set;
        }

        public string CurrentUser
        {
            get;
            set;
        }
        public string IsSAMPartner
        {
            get;
            set;
        }
        public string TertioryCategoryId
        {
            get;
            set;
        }
    }
}