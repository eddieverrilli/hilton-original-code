﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProjectConfigLeafCategoryDetail
    {
        public IList<ProjectConfigPartnerProduct> PartnerProductDetails
        {
            get;
            set;
        }
    }
}