﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeMediaCenterItem
    {
        public int MediaCenterId { get; set; }
        public String ContentType { get; set; }
        public String ContentText { get; set; }
        public String ContentImage { get; set; }
        public String Caption { get; set; }
        public String Keywords { get; set; }
        public bool Enabled { get; set; }
        public int DisplayOrder { get; set; }
    }
}
