﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class UserStatistics
    {
        public double AverageLogOn
        {
            get;
            set;
        }

        public int Total
        {
            get;
            set;
        }

        public int TotalLogOn
        {
            get;
            set;
        }

        public string UserType
        {
            get;
            set;
        }

        public string TotalAssocProj
        {
            get;
            set;
        }
    }
}