﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class MyProjectsSearch
    {
        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public int TotalRecordCount
        {
            get;
            set;
        }
    }
}