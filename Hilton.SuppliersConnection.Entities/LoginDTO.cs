﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    public class LoginDetailDto
    {
        public Guid Id { get; set; }


        public string Email { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string UserNumber { get; set; }
        public string Usertype { get; set; }

        public bool IsAuthenticated { get; set; }

        public string Password { get; set; }
        public bool PingDefaultsSet { get; set; }
        public bool IsHotelUser { get; set; }
        public string HiltId { get; set; }
        public string Inncode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsGuestUser { get; set; }
    }

}
