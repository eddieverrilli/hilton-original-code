﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PaymentTransactionDetails
    {
        public string City
        {
            get;
            set;
        }

        public string CompanyAddress1
        {
            get;
            set;
        }

        public string CompanyAddress2
        {
            get;
            set;
        }

        public string CompanyName
        {
            get;
            set;
        }

        public IList<Contact> Contacts
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public string PartnerCompany
        {
            get;
            set;
        }

        public string PartnerStatus
        {
            get;
            set;
        }

        public string PartnerType
        {
            get;
            set;
        }

        public decimal? PaymentAmount
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public DateTime TransactionDateTime
        {
            get;
            set;
        }

        public string TransactionId
        {
            get;
            set;
        }

        public string TransactionStatus
        {
            get;
            set;
        }

        public string ZipCode
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            get;
            set;
        }
        public IList<TranComment> TransComment
        {
            get;
            set;
        }
    }
}