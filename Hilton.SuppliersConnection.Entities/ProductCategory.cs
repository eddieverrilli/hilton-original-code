﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProductCategory
    {
        public int CategoryId
        {
            get;
            set;
        }

        public string CategoryName
        {
            get;
            set;
        }

        public IList<ProductAssignedSpecifications> ProductAssignedSpecifications
        {
            get;
            set;
        }
    }
}