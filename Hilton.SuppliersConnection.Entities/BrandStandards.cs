﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class BrandStandards
    {
        public IList<BrandStandardAffectedPartner> AffectedPartners
        {
            get;
            set;
        }

        public string Brand
        {
            get;
            set;
        }

        public string Category
        {
            get;
            set;
        }

        public string NewStandard
        {
            get;
            set;
        }

        public string NotifiedDate
        {
            get;
            set;
        }

        public string ParentStandard
        {
            get;
            set;
        }

        public string PreviousStandard
        {
            get;
            set;
        }

        public string PropertyType
        {
            get;
            set;
        }

        public string PublishDate
        {
            get;
            set;
        }

        public string Region
        {
            get;
            set;
        }

        public string StandardDetails
        {
            get;
            set;
        }

        public int StandardId
        {
            get;
            set;
        }

        public string StandardName
        {
            get;
            set;
        }

        public string StandardNumber
        {
            get;
            set;
        }

        public string StandardVersion
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string UpdatedDate
        {
            get;
            set;
        }
    }
}