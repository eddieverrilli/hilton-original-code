﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ContentSearch
    {
        public bool AllClick
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSet
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string QuickSearchExpression
        {
            get;
            set;
        }

        public string SearchFlag
        {
            get;
            set;
        }

        public string Section
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public int TotalRecordCount
        {
            get;
            set;
        }
    }
}