﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class BrandRegionType
    {
        public string BrandRegionPropertyTypeId { get; set; }

        public string BrandRegionPropertyTypeDesc { get; set; }
    }
}