﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    public class TranComment
    {
        public  int Userid
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }

        public string Comment
        {
            get;
            set;
        }       
        public string RecordedDatetime
        {
            get;
            set;
        }
        
    }
}
