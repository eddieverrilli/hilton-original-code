﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class DynamicDropDown
    {
        public IList<DropDownItem> FeedbackPartnerDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> MessageRegionDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> TermsAndConditions
        {
            get;
            set;
        }

        public IList<DropDownItem> PartnerDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> RecipientDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> SectionDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> SenderDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> ActivePartnersDropDown
        {
            get;
            set;
        }
    }
}