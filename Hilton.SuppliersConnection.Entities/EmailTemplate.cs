﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class EmailTemplate
    {
        public string Body
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int EmailTemplateId
        {
            get;
            set;
        }

        public string Mode
        {
            get;
            set;
        }

        public string EmailTemplateName
        {
            get;
            set;
        }

        public bool Status
        {
            get;
            set;
        }

        public string Subject
        {
            get;
            set;
        }

        public string ToName
        {
            get;
            set;
        }

        public string RecipientsMailAddress
        {
            get;
            set;
        }

        public string FromName
        {
            get;
            set;
        }

        public string SenderEmailAddress
        {
            get;
            set;
        }
    }
}