﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ApplicationMessageDetails
    {
        public string CultureCode
        {
            get;
            set;
        }

        public int CultureTypeId
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public bool IsNeutralLanguage
        {
            get;
            set;
        }

        public int MessageId
        {
            get;
            set;
        }

        public string MessageCode
        {
            get;
            set;
        }
    }
}