﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerPaymentLogError
    {
        public string errorCodeRequestQueryStr
        {
            get;
            set;
        }

        public string errorNameRequestQueryStr
        {
            get;
            set;
        }

        public string errorMessageRequestQueryStr
        {
            get;
            set;
        }

        public string transactionStatus
        {
            get;
            set;
        }

        public string transactionId
        {
            get;
            set;
        }

        public string transactionApprovalCode
        {
            get;
            set;
        }

        public string errorCode
        {
            get;
            set;
        }

        public string errorName
        {
            get;
            set;
        }

        public string errorMessage
        {
            get;
            set;
        }

        public string amount
        {
            get;
            set;
        }

        public string cardNumber
        {
            get;
            set;
        }

        public string result
        {
            get;
            set;
        }

        public int paymentRequestId
        {
            get;
            set;
        }

    }
}
