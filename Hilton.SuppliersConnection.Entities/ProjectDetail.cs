﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProjectDetail
    {
        public int ProjectId
        {
            get;
            set;
        }

        public string ProjectManager
        {
            get;
            set;
        }

        public string ProjectTypeName
        {
            get;
            set;
        }

        public string BrandName
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string Address2
        {
            get;
            set;
        }

        public string ZipCode
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public DateTime CreatedDate
        {
            get;
            set;
        }

        public IList<ProjectConfigDetails> ProjectCategoriesDetails
        {
            get;
            set;
        }

        public string FacilityName
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }
    }
}