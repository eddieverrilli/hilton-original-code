﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class DropDownItem
    {
        public string DataTextField
        {
            get;
            set;
        }

        public string DataValueField
        {
            get;
            set;
        }

        public string ParentDataField
        {
            get;
            set;
        }

    }
}