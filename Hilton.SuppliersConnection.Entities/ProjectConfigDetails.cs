﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProjectConfigDetails
    {
        public int CategoryId
        {
            get;
            set;
        }

        public int CategoryLevel
        {
            get;
            set;
        }

        public string CategoryDescription
        {
            get;
            set;
        }

        public string CategoryName
        {
            get;
            set;
        }

        public bool IsLeaf
        {
            get;
            set;
        }

        public ProjectConfigLeafCategoryDetail LeafCategoryDetails
        {
            get;
            set;
        }

        public int ProjectId
        {
            get;
            set;
        }

        public int SequenceId
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public int ParentCategoryId
        {
            get;
            set;
        }

        public int RootParentId
        {
            get;
            set;
        }

        public bool HasProducts
        {
            get;
            set;
        }

        public bool? IsRequired
        {
            get;
            set;
        }
    }
}