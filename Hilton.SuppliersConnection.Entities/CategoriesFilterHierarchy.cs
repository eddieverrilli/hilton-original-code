﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class CategoriesFilterHierarchy
    {
        public IList<SearchFilters> FilteredCBRTData
        {
            get;
            set;
        }

        public IList<SearchFilters> FilteredHierarchyCBRTData
        {
            get;
            set;
        }
    }
}