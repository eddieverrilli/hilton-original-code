﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class FeedbackStatistics
    {
        public int FeedbackSubmitted
        {
            get;
            set;
        }

        public int MessageToOwners
        {
            get;
            set;
        }

        public int MessageToPartners
        {
            get;
            set;
        }
    }
}