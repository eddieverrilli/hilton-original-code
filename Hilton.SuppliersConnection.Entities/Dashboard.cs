﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Dashboard
    {
        public FeedbackStatistics FeedbackStatistics
        {
            get;
            set;
        }

        public PartnerStatistics PartnerStatistics
        {
            get;
            set;
        }

        public ProductStatistics ProductStatistics
        {
            get;
            set;
        }

        public IList<ProjectStatistics> ProjectStatistics
        {
            get;
            set;
        }

        public IList<UserStatistics> UserStatistics
        {
            get;
            set;
        }
    }
}