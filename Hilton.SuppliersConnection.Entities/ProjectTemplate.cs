﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProjectTemplate
    {
        public string BrandName
        {
            get;
            set;
        }

        public IList<Category> Categories
        {
            get;
            set;
        }

        public int NoOfCategories
        {
            get;
            set;
        }

        public int NoOfProjects
        {
            get;
            set;
        }

        public string RegionDescription
        {
            get;
            set;
        }

        public int StatusId
        {
            get;
            set;
        }

        public int TemplateId
        {
            get;
            set;
        }

        public string PropertyTypeDescription
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }

        public int CountryId
        {
            get;
            set;
        }

        public string CountryName
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }

        public IList<PartnerProductTrial> PartnerProductTrial
        {
            get;
            set;
        }

        public TermsAndConditions TermsAndCondition
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
    }
}