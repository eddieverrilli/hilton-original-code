﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class FilterCriteria
    {
        public int CategoryId
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }
        public int CountryId
        {
            get;
            set;
        }
    }
}