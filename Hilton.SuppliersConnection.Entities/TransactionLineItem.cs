﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class TransactionLineItem
    {
        public string ContactFirstName
        {
            get;
            set;
        }

        public string ContactLastName
        {
            get;
            set;
        }

        public string ContactName
        {
            get;
            set;
        }

        public string PartnerCompany
        {
            get;
            set;
        }

        public string PartnerType
        {
            get;
            set;
        }

        public decimal? PaymentAmount
        {
            get;
            set;
        }

        public int PaymentRecordId
        {
            get;
            set;
        }

        public DateTime? TransactionDateTime
        {
            get;
            set;
        }

        public string TransactionStatus
        {
            get;
            set;
        }

        public string TransactionId
        {
            get;
            set;
        }
    }
}