﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Country
    {
        public int CountryId { get; set; }

        public string CountryName { get; set; }

        public int RegionId { get; set; }
        public string RegionName { get; set; }
    }
}