﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Product
    {
        public string AdminReviewBy
        {
            get;
            set;
        }

        public string AdminReviewComments
        {
            get;
            set;
        }

        public string BrandStandards
        {
            get;
            set;
        }

        public Contact Contact
        {
            get;
            set;
        }

        public int ContactId
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string DesignReviewBy
        {
            get;
            set;
        }

        public string DesignReviewComments
        {
            get;
            set;
        }

        public string ImageName
        {
            get;
            set;
        }

        public string ModalName
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public string PartnerName
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public Byte[] ProductImage
        {
            get;
            set;
        }

        public Byte[] ProductThumbnailImage
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public string ProductSku
        {
            get;
            set;
        }

        public string RequestedBy
        {
            get;
            set;
        }

        public int RequestedById
        {
            get;
            set;
        }

        public string RequestedDate
        {
            get;
            set;
        }

        public Byte[] SpecSheetPdf
        {
            get;
            set;
        }

        public string SpecSheetPdfName
        {
            get;
            set;
        }

        public string StatusDescription
        {
            get;
            set;
        }

        public int StatusId
        {
            get;
            set;
        }

        public string TechReviewBy
        {
            get;
            set;
        }

        public string TechReviewComments
        {
            get;
            set;
        }

        public string Website
        {
            get;
            set;
        }

        public int CatId
        {
            get;
            set;
        }

        public string Culture
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
    }
}