﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Acknowledgement
    {
        public string TermAndCondition
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public string IPAddress
        {
            get;
            set;
        }

        public string TermAndConditionVersion
        {
            get;
            set;
        }

        public DateTime VersionDatetime
        {
            get;
            set;
        }

        public int CreatedByUserId
        {
            get;
            set;
        }
    }
}