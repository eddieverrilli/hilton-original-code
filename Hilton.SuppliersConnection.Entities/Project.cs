﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Project
    {
        public double ConfigPercent
        {
            get;
            set;
        }

        public int Excp
        {
            get;
            set;
        }

        public string FeaturedDescription
        {
            get;
            set;
        }

        public byte[] FeaturedImage
        {
            get;
            set;
        }

        public byte[] FeaturedThumbnailImage
        {
            get;
            set;
        }

        public bool IsFeatured
        {
            get;
            set;
        }

        public string O2OStatusDescription
        {
            get;
            set;
        }

        public string OwnerName
        {
            get;
            set;
        }

        public IList<User> AssociatedUserCollection
        {
            get;
            set;
        }

        public string PMName
        {
            get;
            set;
        }

        public DateTime ProjectAddedDate
        {
            get;
            set;
        }

        public int ProjectId
        {
            get;
            set;
        }

        public string ProjectTypeDescription
        {
            get;
            set;
        }

        public int ProjectTypeId
        {
            get;
            set;
        }

        public string SCStatusDescription
        {
            get;
            set;
        }

        public Facility Property
        {
            get;
            set;
        }

        public int TemplateId
        {
            get;
            set;
        }

        public DateTime? UpdatedOn
        {
            get;
            set;
        }

        public string BrandDescription
        {
            get;
            set;
        }

        public int FacilityId
        {
            get;
            set;
        }

        public string FacilityLocation
        {
            get;
            set;
        }

        public string PropertyTypeDescription
        {
            get;
            set;
        }

        public string RegionDescription
        {
            get;
            set;
        }
        //Country Description
        public string CountryDescription
        {
            get;
            set;
        }

        public string ImageName
        {
            get;
            set;
        }

        public DateTime? ProjectCompletionDate
        {
            get;
            set;
        }

        public int ProjectStatus
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
        public string BrandSegment
        {
            get;
            set;
        }
    }
}