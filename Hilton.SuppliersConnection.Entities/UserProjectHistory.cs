﻿using System;
namespace Hilton.SuppliersConnection.Entities
{
    public class UserProjectHistory
    {
        public DateTime? ModifiedDate
        {
            get;
            set;
        }

        public string ModifiedTime
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string UserTypeDescription
        {
            get;
            set;
        }
    }
}