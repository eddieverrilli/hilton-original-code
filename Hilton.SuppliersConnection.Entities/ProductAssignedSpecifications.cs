﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProductAssignedSpecifications
    {
        public string BrandDescription
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public string BrandImageUrl
        {
            get;
            set;
        }

        public IList<Product> Product
        {
            get;
            set;
        }

        public IList<ProductPropertyType> PropertyTypes
        {
            get;
            set;
        }

        public string RegionDescription
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }
        public string CountryDescription
        {
            get;
            set;
        }

        public int CountryId
        {
            get;
            set;
        }

        public IList<Country> Countries { get; set; }

        public int CategoryId
        {
            get;
            set;
        }
    }
}