﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Category
    {
        public int BrandId
        {
            get;
            set;
        }

        public string BrandName
        {
            get;
            set;
        }

        public string StandardNumbers
        {
            get;
            set;
        }

        public int CategoryId
        {
            get;
            set;
        }

        public string CategoryName
        {
            get;
            set;
        }

        public IList<CategoryStandard> CategoryStandards
        {
            get;
            set;
        }

        public string ContactInfo
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public bool HasProducts
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public bool IsLeaf
        {
            get;
            set;
        }

        public bool? IsNewPartnersRequired
        {
            get;
            set;
        }

        public bool? IsNewProductsReq
        {
            get;
            set;
        }

        public bool? IsRequired
        {
            get;
            set;
        }

        public int Level
        {
            get;
            set;
        }

        public int ParentCategoryId
        {
            get;
            set;
        }

        public IList<CategoryPartnerProduct> PartnerProducts
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }

        public string PropertyTypeName
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }

        public string RegionName
        {
            get;
            set;
        }
        public int CountryId
        {
            get;
            set;
        }

        public string CountryName
        {
            get;
            set;
        }
        public int RootId
        {
            get;
            set;
        }

        public int RootParentId
        {
            get;
            set;
        }

        public string SequenceId
        {
            get;
            set;
        }

        public string StandardDescription
        {
            get;
            set;
        }

        public string StyleDescription
        {
            get;
            set;
        }

        public bool AccessibleForUser
        {
            get;
            set;
        }

        public bool UnaccessibleForUser
        {
            get;
            set;
        }

        public string ParentCategoryHierarchy
        {
            get;
            set;
        }

        public bool IsFeatured
        {
            get;
            set;
        }

        public int ProjectConfigDetailsId
        {
            get;
            set;
        }

        public bool IsEnabled
        {
            get;
            set;
        }

        public int ProjectConfigurationId
        {
            get;
            set;
        }

        public int RowId
        {
            get;
            set;
        }

    }
}