﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerPropertyType
    {
        public int CategoryId
        {
            get;
            set;
        }

        public string PropertyTypeDescription
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }
    }
}