﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LanguageResourceDetail
    {
        public string Content
        {
            get;
            set;
        }

        public string CultureCode
        {
            get;
            set;
        }

        public int CultureTypeId
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public bool IsNeutralLanguage
        {
            get;
            set;
        }

        public int LanguageResourceId
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }
    }
}