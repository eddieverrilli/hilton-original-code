﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerProductOption
    {
        public int CatId
        {
            get;
            set;
        }

        public string CategoryHeirarchy
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public string PartnerName
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public byte[] PartnerLogo
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public string ProductNumber
        {
            get;
            set;
        }

        public Contact Contact
        {
            get;
            set;
        }

        public bool IsSelected
        {
            get;
            set;
        }

        public bool IsOther
        {
            get;
            set;
        }

        public string SpecSheetPdfName
        {
            get;
            set;
        }

        public byte[] SpecSheetPdf
        {
            get;
            set;
        }

        public IList<CategoryStandard> CategoryStandard
        {
            get;
            set;
        }

        public string ProductContactEmailAddress
        {
            get;
            set;
        }

        public string ProductImageName
        {
            get;
            set;
        }

        public byte[] ProductImage
        {
            get;
            set;
        }

        public string ToFirstName
        {
            get;
            set;
        }

        public string ToLastName
        {
            get;
            set;
        }

        public string BrandName
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string Address2
        {
            get;
            set;
        }

        public string ZipCode
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public string FacilityName
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }
    }
}