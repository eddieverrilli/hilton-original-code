﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProductPropertyType
    {
        public int CategoryId
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public string PropertyTypeDescription
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }
    }
}