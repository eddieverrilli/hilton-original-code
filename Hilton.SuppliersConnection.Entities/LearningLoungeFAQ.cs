﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeFAQ
    {
        public int FAQId { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public String TitleColor { get; set; }
        public int DisplayOrder { get; set; }
        public Boolean Enabled { get; set; }
    }
}
