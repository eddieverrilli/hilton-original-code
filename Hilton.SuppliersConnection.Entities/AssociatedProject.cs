﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class AssociatedProject
    {
        public Facility Property
        {
            get;
            set;
        }

        public User User
        {
            get;
            set;
        }

        public bool IsToBeAdded
        {
            get;
            set;
        }

        public bool IsToBeRemoved
        {
            get;
            set;
        }

        public DateTime? ProjectAddedDate
        {
            get;
            set;
        }

        public int ProjectId
        {
            get;
            set;
        }

        public string ProjectOwner
        {
            get;
            set;
        }

        public string ProjectTypeDescription
        {
            get;
            set;
        }

        public int ProjectTypeId
        {
            get;
            set;
        }
    }
}