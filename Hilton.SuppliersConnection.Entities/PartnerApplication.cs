﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerApplication
    {
        public IList<AppliedPartnershipOpportunity> AppliedParntershipOpportunities
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public string CompanyAddress1
        {
            get;
            set;
        }

        public string CompanyAddress2
        {
            get;
            set;
        }

        public string CompanyDescription
        {
            get;
            set;
        }

        public byte[] CompanyLogoImage
        {
            get;
            set;
        }

        public byte[] CompanyLogoImageThumbnail
        {
            get;
            set;
        }

        public string CompanyLogoName
        {
            get;
            set;
        }

        public string CompanyName
        {
            get;
            set;
        }

        public IList<Contact> Contacts
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public string CountryName
        {
            get;
            set;
        }

        public int PartnershipType
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public string StateName
        {
            get;
            set;
        }

        public string ZipCode
        {
            get;
            set;
        }

        public float PartnerShipAmount
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public TermsAndConditions TermsAndCondition
        {
            get;
            set;
        }

        public string WebSiteLink
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
    }
}