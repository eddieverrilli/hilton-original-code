﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Region
    {
        public string RegionDescription
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }
    }
}