﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class StaticDropDown
    {
        public IList<DropDownItem> BrandDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> CountryDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> FeedbackStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> LanguageDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> MessageStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> PartnerStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> ProductStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> ProjectStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> ProjectTemplateStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> ProjectTypesDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> PropertyTypesDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> PaymentTypesDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> TransactionStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> RatingDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> RegionDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> UserStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> UserTypesDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> StateDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> OwnerAndConsultantRolesDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> AdminPermissionsDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> PartnershipStatusDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> PartnershipTypeDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> UserProjectMappingTypeDropDown
        {
            get;
            set;
        }

        public IList<DropDownItem> ConsultantRoleDropDown
        {
            get;
            set;
        }
    }
}