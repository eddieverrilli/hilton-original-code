﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeGallery
    {
        public int GalleryImageId { get; set; }
        public String Image { get; set; }
        public int ClickActionTypeId { get; set; }
        public String ClickActionTypeName { get; set; }
        public String ClickActionURL { get; set; }
        public String ClickActionTarget { get; set; }
        public bool Enabled { get; set; }
        public List<LearningLoungeTemplateContent> TemplateContent { get; set; }
        public int TemplateId { get; set; }
        public bool AllowDelete { get; set; }
        public String ImageURL { get; set; }
        public String ImageType { get; set; }
    }
}
