﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class UserPermissions
    {
        public int MenuStructureId
        {
            get;
            set;
        }

        public int UserTypeId
        {
            get;
            set;
        }

        public int MenuId
        {
            get;
            set;
        }

        public string MenuName
        {
            get;
            set;
        }

        public string MenuContent
        {
            get;
            set;
        }

        public string Command
        {
            get;
            set;
        }

        public int ParentMenuId
        {
            get;
            set;
        }

        public int Sequence
        {
            get;
            set;
        }

        public int DisplayDirection
        {
            get;
            set;
        }
    }
}