﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeCEU
    {
        public int CEUCreditId { get; set; }
        public DateTime DateCreated { get; set; }
        public String CourseTitle { get; set; }
        public String Company { get; set; }
        public String Contact { get; set; }
        public String Phone { get; set; }
        public String TopicLink { get; set; }
        public DateTime ExpireDate { get; set; }
        public decimal Cost { get; set; }
        public bool BrandStandardCompliance { get; set; }
        public bool AIAAccredited { get; set; }
        public String AIANumCEUs { get; set; }
        public bool LEEDAccredited { get; set; }
        public String LEEDNumCEUs { get; set; }
        public bool IsApproved { get; set; }
        public DateTime ApprovedDate { get; set; }
        public String SubmittedByEmail { get; set; }
        public String Image { get; set; }
    }
}
