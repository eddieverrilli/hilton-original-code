﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ReportColumns
    {
        public int ReportId
        {
            get;
            set;
        }

        public bool AutoFit
        {
            get;
            set;
        }

        public string CellColor
        {
            get;
            set;
        }

        public string ColumnName
        {
            get;
            set;
        }

        //public string Format
        //{
        //    get;
        //    set;
        //}
        public string HAlign
        {
            get;
            set;
        }

        public string VAlign
        {
            get;
            set;
        }

        public string HeaderHAlign
        {
            get;
            set;
        }

        public string HeaderTextColor
        {
            get;
            set;
        }

        public string HeaderVAlign
        {
            get;
            set;
        }

        public string HeaderBgColor
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public int Width
        {
            get;
            set;
        }

        //public string Type
        //{
        //    get;
        //    set;
        //}
        public string Visible
        {
            get;
            set;
        }

        //public bool WrapText
        //{
        //    get;
        //    set;
        //}
    }
}