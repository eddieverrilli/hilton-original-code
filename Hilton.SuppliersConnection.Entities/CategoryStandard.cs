﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class CategoryStandard
    {
        public int CategoryId
        {
            get;
            set;
        }

        public string StandardDescription
        {
            get;
            set;
        }

        public string StandardNumber
        {
            get;
            set;
        }

        public bool IsToBeMapped
        {
            get;
            set;
        }

        public bool IsToBeRemoved
        {
            get;
            set;
        }
    }
}