﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class UserSearch
    {
        public DateTime? ActivateDateTo
        {
            get;
            set;
        }

        public DateTime? ActivationDateFrom
        {
            get;
            set;
        }

        public bool AllClick
        {
            get;
            set;
        }

        public string UserId
        {
            get;
            set;
        }

        public DateTime? LastLoginFrom
        {
            get;
            set;
        }

        public DateTime? LastLoginTo
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSet
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string QuickSearchExpression
        {
            get;
            set;
        }

        public string SearchFlag
        {
            get;
            set;
        }

        public string SearchSource
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public string StatusId
        {
            get;
            set;
        }

        public int TotalRecordCount
        {
            get;
            set;
        }

        public string TypeId
        {
            get;
            set;
        }
    }
}