﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerStatistics
    {
        public int CurrentGoldPartners
        {
            get;
            set;
        }

        public int CurrentRegularPartners
        {
            get;
            set;
        }

        public decimal OutstandingGoldPartnersReqAmount
        {
            get;
            set;
        }

        public int OutstandingGoldPartnersRequest
        {
            get;
            set;
        }

        public decimal OutstandingRegularPartnersReqAmount
        {
            get;
            set;
        }

        public int OutstandingRegularPartnersRequest
        {
            get;
            set;
        }

        public int PendingGoldPartnerRequest
        {
            get;
            set;
        }

        public int PendingRegularPartnerRequest
        {
            get;
            set;
        }

        public decimal TotalGoldPartnershipAmtCollected
        {
            get;
            set;
        }

        public decimal TotalRegularPartnershipAmtCollected
        {
            get;
            set;
        }

        public int ActivatedGoldPartnerInCurrentMonth
        {
            get;
            set;
        }

        public int ActivatedRegularPartnerInCurrentMonth
        {
            get;
            set;
        }
        public int ActivatedGoldPartnerInCurrentYear
        {
            get;
            set;
        }

        public int ActivatedRegularPartnerInCurrentYear
        {
            get;
            set;
        }

        public decimal ActivatedGoldPartnerAmountInCurrentMonth
        {
            get;
            set;
        }

        public decimal ActivatedRegualrPartnerAmountInCurrentMonth
        {
            get;
            set;
        }

        public decimal ActivatedGoldPartnerAmountInCurrentYear
        {
            get;
            set;
        }

        public decimal ActivatedRegularPartnerAmountInCurrentYear
        {
            get;
            set;
        }

    }
}