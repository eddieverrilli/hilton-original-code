﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class UserAccessLog
    {
        public DateTime LogOn
        {
            get;
            set;
        }
    }
}