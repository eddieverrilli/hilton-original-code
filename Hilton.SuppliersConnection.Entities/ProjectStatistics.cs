﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProjectStatistics
    {
        public int FullConfiguredProjectsPercent
        {
            get;
            set;
        }

        public int NotConfiguredProjectsPercent
        {
            get;
            set;
        }

        public int PartialConfiguredProjectsPercent
        {
            get;
            set;
        }

        public string ProjectType
        {
            get;
            set;
        }

        public int TotalProjects
        {
            get;
            set;
        }

        public int ProjectTypeId
        {
            get;
            set;
        }
    }
}