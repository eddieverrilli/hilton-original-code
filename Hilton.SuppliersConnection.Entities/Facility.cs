﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Facility
    {
        public DateTime AddedDate
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string Address2
        {
            get;
            set;
        }

        public string FacilityLocation
        {
            get;
            set;
        }

        public string ArchitectContact
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public string BrandDescription
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public string StateAbbreviation
        {
            get;
            set;
        }

        public string ContractorCompany
        {
            get;
            set;
        }

        public string ContractorContact
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public int FacilityId
        {
            get;
            set;
        }

        public string FacilityUniqueName
        {
            get;
            set;
        }

        public string Owner
        {
            get;
            set;
        }

        public string OwnerContact
        {
            get;
            set;
        }

        public string OwnerRepContact
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }

        public string PropertyTypeDescription
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }

        public string RegionDescription
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public DateTime UpdatedDate
        {
            get;
            set;
        }

        public string ZipCode
        {
            get;
            set;
        }

        public int ProjectId
        {
            get;
            set;
        }

        public string Brand
        {
            get;
            set;
        }

        public string BrandSegment
        {
            get;
            set;
        }
    }
}