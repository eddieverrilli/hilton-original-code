﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerPayment
    {
        public string AmountPaid
        {
            get;
            set;
        }

        public string CardNumber
        {
            get;
            set;
        }

        public string Result
        {
            get;
            set;
        }

        public int PaymentRequestId
        {
            get;
            set;
        }

        public string PaymentType
        {
            get;
            set;
        }

        public string RequestType
        {
            get;
            set;
        }

        public DateTime? TransactionDate
        {
            get;
            set;
        }

        public string TransactionId
        {
            get;
            set;
        }

        public string ErrorCode
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            get;
            set;
        }

        public string ErrorName
        {
            get;
            set;
        }

        public string TransactionApprovalCode
        {
            get;
            set;
        }

        public string TransactionStatus
        {
            get;
            set;
        }

        public string AmountRequested
        {
            get;
            set;
        }

        public int PartnershipType
        {
            get;
            set;
        }

        public IList<Contact> Contacts
        {
            get;
            set;
        }

        public string Culture
        {
            get;
            set;
        }
    }
}