﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class WarmLead
    {
        public int ProjectId
        {
            get;
            set;
        }

        public string ProjectTypeName
        {
            get;
            set;
        }

        public string ProductId
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public string BrandName
        {
            get;
            set;
        }

        public string ProjectManager
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public string PropTypeName
        {
            get;
            set;
        }

        public int PropertyTypeId
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }

        public string CategoryDisplayName
        {
            get;
            set;
        }

        public DateTime AddedDate
        {
            get;
            set;
        }

        public string ZipCode
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string Address2
        {
            get;
            set;
        }

        public string CityName
        {
            get;
            set;
        }

        public string StateAbbreviation
        {
            get;
            set;
        }

        public string RepEmailAddress
        {
            get;
            set;
        }

        public string RepContactDate
        {
            get;
            set;
        }

        public int CategoryId
        {
            get;
            set;
        }

        public string CountryName
        {
            get;
            set;
        }

        public string FacilityName
        {
            get;
            set;
        }
    }
}