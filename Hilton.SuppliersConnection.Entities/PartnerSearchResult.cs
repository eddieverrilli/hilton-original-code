﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerSearchResult
    {
        public IList<Brand> Brands
        {
            get;
            set;
        }

        public IList<Category> Categories
        {
            get;
            set;
        }

        public Contact Contact
        {
            get;
            set;
        }

        public Partner Partner
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public IList<PropertyType> PropertyType
        {
            get;
            set;
        }

        public IList<Region> Region
        {
            get;
            set;
        }
        public IList<Country> Country
        {
            get;
            set;
        }
    }
}