﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PaymentRequest
    {
        public string GoldAmount
        {
            get;
            set;
        }

        public string RegularAmount
        {
            get;
            set;
        }

        public string RequestSentTo
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }

        public string Culture
        {
            get;
            set;
        }
    }
}