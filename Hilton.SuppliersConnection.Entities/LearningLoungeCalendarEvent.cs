﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeCalendarEvent
    {
        public int EventId { get; set; }
        public String name { get; set; }
        public String image { get; set; }

        private DateTime _startDate = DateTime.MinValue;
        public DateTime StartDate {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
                day = _startDate.Day;
                month = _startDate.Month;
                year = _startDate.Year;

                if (_endDate != DateTime.MinValue) {
                    this.duration = this.duration = _endDate.Day - _startDate.Day + 1;
                }
                else
                {
                    this.duration = 1;
                }

                SetTime();
            }
        }

        private DateTime _endDate = DateTime.MinValue;
        public DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;                

                if (_endDate == DateTime.MinValue)
                {
                    duration = 1;
                } else {                    

                    if (_startDate != DateTime.MinValue)
                    {
                        this.duration = _endDate.Day - _startDate.Day + 1;
                    }
                }

                SetTime();
            }
        }

        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public int startHour { get; set; }
        public int endHour { get; set; }
        public String time { get; set; }
        public int duration { get; set; }
        public int color { get; set; }
        public String location { get; set; }
        public String description { get; set; }
        public String RestrictToUserTypes { get; set; }
        public bool Enabled { get; set; }
        public String URL { get; set; }
        public int[] CategoryIds { get; set; }
        public int[] UserTypeIds { get; set; }
        public int DuplicatedEventId { get; set; }

        private void SetTime()
        {
            startHour = _startDate.Hour;
            endHour = _endDate.Hour;

            if (_endDate == DateTime.MinValue && _startDate != DateTime.MinValue)
            {
                time = "All Day";                
            }
            else if (_endDate != DateTime.MinValue && _startDate != DateTime.MinValue)
            {
                if (this.duration > 1)
                {
                    time = _startDate.ToString("M/d") + " " + _startDate.ToShortTimeString() + " - " + _endDate.ToString("M/d") + " " + _endDate.ToShortTimeString();
                }
                else
                {
                    time = _startDate.ToShortTimeString() + " - " + _endDate.ToShortTimeString();
                }
                
            }
        }
    }
}
