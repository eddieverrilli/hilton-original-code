﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PaymentGatewayParameters
    {
        public int HiltonMerchantId
        {
            get;
            set;
        }

        public int HiltonUserId
        {
            get;
            set;
        }

        public string Pin
        {
            get;
            set;
        }

        public string TransactionType
        {
            get;
            set;
        }

        public bool ShowForm
        {
            get;
            set;
        }
    }
}