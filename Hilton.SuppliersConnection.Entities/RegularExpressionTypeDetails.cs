﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class RegularExpressionTypeDetails
    {
        public string Description
        {
            get;
            set;
        }

        public string CultureCode
        {
            get;
            set;
        }

        public int CultureTypeId
        {
            get;
            set;
        }

        public bool IsNeutralLanguage
        {
            get;
            set;
        }

        public int RegularExpressionTypeId
        {
            get;
            set;
        }

        public string RegularExpression
        {
            get;
            set;
        }
    }
}