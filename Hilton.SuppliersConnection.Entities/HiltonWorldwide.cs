﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class HiltonWorldwide
    {
        public string BrandId
        {
            get;
            set;
        }

        public string RegionId
        {
            get;
            set;
        }

        public string PropertyTypeId
        {
            get;
            set;
        }

        public string BrandCollection
        {
            get;
            set;
        }

        public string RegionCollection
        {
            get;
            set;
        }
    }
}