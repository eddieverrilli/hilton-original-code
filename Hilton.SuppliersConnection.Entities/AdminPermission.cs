﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class AdminPermission
    {
        public IList<Permission> Permissions
        {
            get;
            set;
        }

        public IList<Region> Regions
        {
            get;
            set;
        }
    }
}