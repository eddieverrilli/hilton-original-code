﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class SearchCriteria
    {
        public string BrandId { get; set; }

        public string PartnerId { get; set; }

        public string PropertyTypeId { get; set; }

        public string RegionId { get; set; }

        public string CountryId { get; set; }

        public string BrandCollection { get; set; }
    }
}