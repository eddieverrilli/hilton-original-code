﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Permission
    {
        public string PermissionDescription
        {
            get;
            set;
        }

        public int PermissionId
        {
            get;
            set;
        }

        public int LinkenMenuId
        {
            get;
            set;
        }
    }
}