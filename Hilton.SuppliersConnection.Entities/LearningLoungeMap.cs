﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    public class LearningLoungeMap
    {
        public String name {get; set;}
        public String subtitle {get; set;}
        public String description {get; set;}
        public String nickname {get; set;}
        public String slug {get; set;}
        public bool has_multiple_maps {get; set;}
        public LearningLoungeMapDetail[] map { get; set; }

    }

    public class LearningLoungeMapDetail
    {
        public String name { get; set; }
        public String slug { get; set; }
        public String description { get; set; }
        public String callout { get; set; }
        public String subtitle { get; set; }
        public LearningLoungeContactShell main_contacts { get; set; }
        public LearningLoungeContactShell general_contacts { get; set; }
        public LearningLoungeContactShell additional_contacts { get; set; }
        public LearningLoungeButtonShell sidebar_navigation { get; set; }
        public LearningLoungeBrandShell sidebar_brands { get; set; }
        public LearningLoungeButtonShell sidebar_print_options { get; set; }
        public LearningLoungeDirectorShell regional_directors { get; set; }
        public LearningLoungeRegionListbox region_listbox { get; set; }
    }

    public class LearningLoungeContactShell
    {
        public String label { get; set; }
        public LearningLoungeContact[] contacts { get; set; }
    }

    public class LearningLoungeContact
    {
        public int ContactId { get; set; }
        public int ContactTypeId { get; set; }
        public String region { get; set; }
        public String name { get; set; }
        public String position { get; set; }
        public String phone { get; set; }
    }

    public class LearningLoungeButtonShell
    {
        public LearningLoungeButton[] buttons { get; set; }
    }

    public class LearningLoungeBrandShell
    {
        public LearningLoungeButton[] brand { get; set; }
    }

    public class LearningLoungeButton
    {
        public String label { get; set; }
        public String slug { get; set; }
    }

    public class LearningLoungeDirector: LearningLoungeContact
    {
        public String region_slug { get; set; }
    }

    public class LearningLoungeDirectorShell
    {
        public LearningLoungeDirector[] directors { get; set; }
    }

    public class LearningLoungeListboxOption
    {
        public String value { get; set; }
        public String label { get; set; }
        public String region { get; set; }
    }

    public class LearningLoungeRegionListbox
    {
        public String title { get; set; }
        public LearningLoungeListboxOption[] options { get; set; }
    }
}
