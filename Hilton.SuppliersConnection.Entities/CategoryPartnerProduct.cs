﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class CategoryPartnerProduct
    {
        public int CategoryId
        {
            get;
            set;
        }

        public byte[] CompanyLogoImage
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public string PartnerName
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public int TemplateId
        {
            get;
            set;
        }

        public int IsRequired
        {
            get;
            set;
        }

        public bool IsFeatured
        {
            get;
            set;
        }

        public int ProjectLeafCategoryDetailsId
        {
            get;
            set;
        }

        public int OldProductId
        {
            get;
            set;
        }

        public int OldPartnerId
        {
            get;
            set;
        }

        public int ProjectId
        {
            get;
            set;
        }

        public bool IsOther
        {
            get;
            set;
        }
    }
}