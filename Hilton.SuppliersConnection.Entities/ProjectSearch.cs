﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProjectSearch
    {
        public bool AllClick
        {
            get;
            set;
        }

        public string BrandId
        {
            get;
            set;
        }

        public string O2OStatusId
        {
            get;
            set;
        }

        public string OwnerId
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSet
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string ProjectManagerId
        {
            get;
            set;
        }

        public string ProjectTypeId
        {
            get;
            set;
        }

        public string PropertyTypeId
        {
            get;
            set;
        }

        public string QuickSearchExpression
        {
            get;
            set;
        }

        public string RegionId
        {
            get;
            set;
        }
        //Addition of country
        public string CountryId
        {
            get;
            set;
        }
        public string SCStatusId
        {
            get;
            set;
        }

        public string SearchFlag
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public int TotalRecordCount
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public string BrandCollection
        {
            get;
            set;
        }
    }
}