﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class BrandStandardAffectedPartner
    {
        public string CompanyName
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }

        public string StandardId
        {
            get;
            set;
        }
    }
}