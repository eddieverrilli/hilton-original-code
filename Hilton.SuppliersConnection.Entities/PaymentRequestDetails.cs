﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PaymentRequestDetails
    {
        public string GoldAmount
        {
            get;
            set;
        }

        public string RegularAmount
        {
            get;
            set;
        }

        public DateTime RequestSentOn
        {
            get;
            set;
        }

        public string RequestSentTo
        {
            get;
            set;
        }

        public int PartnerId
        {
            get;
            set;
        }

        public int PaymentRequestId
        {
            get;
            set;
        }

        public int PartnershipTypeId
        {
            get;
            set;
        }

        public  DateTime? ActivationDate
        {
            get;
            set;
        }

        public DateTime? ExpirationDate
        {
            get;
            set;
        }
    }
}
