﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class Menu
    {
        public int MenuId
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}