﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class TransactionSearch
    {
        public DateTime? DateFrom
        {
            get;
            set;
        }

        public DateTime? DateTo
        {
            get;
            set;
        }

        public string PartnerShipTypeId
        {
            get;
            set;
        }

        public string PartnerStatusId
        {
            get;
            set;
        }

        public string TransactionStatusId
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSet
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string QuickSearchExpression
        {
            get;
            set;
        }

        public string SearchFlag
        {
            get;
            set;
        }

        public string SearchSource
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public int TotalRecordCount
        {
            get;
            set;
        }

        public bool AllClick
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
    }
}