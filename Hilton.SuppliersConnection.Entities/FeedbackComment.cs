﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class FeedbackComment
    {
        public string Comment
        {
            get;
            set;
        }

        public int CommentId
        {
            get;
            set;
        }

        public int FeedbackId
        {
            get;
            set;
        }

        public int Submittedby
        {
            get;
            set;
        }

        public string SubmittedbyFullName
        {
            get;
            set;
        }

        public DateTime SubmittedDate
        {
            get;
            set;
        }

    }
}