﻿using System;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ProductStatistics
    {
        public int OtherSelections
        {
            get;
            set;
        }

        public int PendingRequest
        {
            get;
            set;
        }

        public int TotalProducts
        {
            get;
            set;
        }
    }
}