﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum PropertyTypeEnum : int
    {
        None,
        Hotel,
        Resort,
        Casino,
        Residence,
    }
}