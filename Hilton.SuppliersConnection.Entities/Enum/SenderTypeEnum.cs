﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum SenderTypeEnum : int
    {
        None,
        Partner,
        Owner,
    }
}