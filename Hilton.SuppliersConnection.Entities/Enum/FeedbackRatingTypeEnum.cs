﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum FeedbackRatingTypeEnum : int
    {
        None,
        VeryPositive,
        Positive,
        Neutral,
        Negative,
        VeryNegative,
    }
}