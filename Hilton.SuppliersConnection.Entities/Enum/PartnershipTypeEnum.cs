﻿namespace Hilton.SuppliersConnection.Entities
{
    public enum PartnershipTypeEnum : int
    {
        None,
        GoldPartner,
        Partner,
    }
}