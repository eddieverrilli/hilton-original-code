﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum CultureTypeEnum : int
    {
        None,
        English,
        Spanish,
        French
    }
}