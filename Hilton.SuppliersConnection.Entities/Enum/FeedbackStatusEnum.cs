﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum FeedbackStatusEnum : int
    {
        None,
        New,
        Archived,
    }
}