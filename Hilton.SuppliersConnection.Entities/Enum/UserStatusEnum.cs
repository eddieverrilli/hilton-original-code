﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum UserStatusEnum : int
    {
        None,
        Active,
        Inactive,
        Pending,
    }
}