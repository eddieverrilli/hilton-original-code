﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum ContactTypeEnum : int
    {
        None,
        Primary,
        Secondary,
        Regional,
        Billing,
    }
}