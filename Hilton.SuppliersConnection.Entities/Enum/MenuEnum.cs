﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum MenuEnum : int
    {
        None,
        AboutSupplierConnection,
        WhatIsSupplierConnection,
        HowToUseSupplierConnection,
        HowToGetAccount,
        HowToBecomeRPartner,
        ContactUs,
        FeaturedProjects,
        RecommendedPartners,
        BrowseRecommendedPartners,
        BecomeRPartner,
        PaymentRequestEmail,
        MyAccount,
        MyProjects,
        ApprovedCategories,
        MyProducts,
        ConstructionReport,
        AccountStatus,
        ProjectTemplates,
        Projects,
        Partners,
        Products,
        Settings,
        Users,
        Feedback,
        Messages,
        Transactions,
        Standards,
        AdminConstructionReport
    }
}
