﻿namespace Hilton.SuppliersConnection.Entities
{
    public enum ReportEnum : int
    {
        none,
        ConstructionReport,
        Transaction,
    }
}
