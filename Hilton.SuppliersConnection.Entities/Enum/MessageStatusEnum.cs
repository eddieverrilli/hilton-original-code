﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum MessageStatusEnum : int
    {
        None,
        New,
        Archived,
    }
}