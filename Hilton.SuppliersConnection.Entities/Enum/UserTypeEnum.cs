﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum UserTypeEnum : int
    {
        None,
        Visitor,
        Partner,
        Owner,
        Consultant,
        Administrator,
        Corporate
    }
}
