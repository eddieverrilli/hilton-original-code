﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum ConsultantTypeEnum : int
    {
        None,
        Architect,
        InteriorDesigner,

        //Contractor,
        OwnerRepresentative,

        ProjectManager,
    }
}