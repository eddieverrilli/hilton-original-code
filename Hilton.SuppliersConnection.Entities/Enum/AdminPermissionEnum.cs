﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum AdminPermissionEnum : int
    {
        None,
        ProjectTemplates,
        Projects,
        Partner,
        Products,
        Settings,
        Users,
        AdministrativeUsers,
        Feedback,
        Messages,
        Transactions,
        ProductReview,
        CategoryEntry,
        Dashboard,
    }
}