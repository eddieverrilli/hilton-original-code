﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum PartnershipStatusEnum : int
    {
        None,
        New,
        Active,
        Inactive,
        PaymentPendingGold,
        PaymentPendingPartner,
       // Rejected,
    }
}