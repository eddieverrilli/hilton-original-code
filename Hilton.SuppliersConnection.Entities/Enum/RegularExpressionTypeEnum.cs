﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum RegularExpressionTypeEnum : int
    {
        None,
        NameValidator,
        WebsiteValidator,
        EmailValidator,
        AmountTwoDecimalValidator,
        NumericValidator,
        TitleValidator,
        HiltonIdValidator,
        PhoneValidator,
        ZipCodeValidator,
        FaxValidator,
        PhoneValidatorUS,
        FaxValidatorUS
    }
}