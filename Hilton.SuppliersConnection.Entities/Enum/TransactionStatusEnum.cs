﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum TransactionStatusEnum : int
    {
        None,
        Complete,
        Pending,
        Declined,
        Error,
        Reject,
        Redirect,
    }
}