﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum PaymentTypeEnum : int
    {
        None,
        CreditCard,
        MailCheck,
    }
}