﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum SearchFlagEnum : int
    {
        None,
        Filter,
        QuickSearch,
    }
}