﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum ProductStatusEnum : int
    {
        None,
        Approved,
        Deactivated,
        New,
        Rejected,
        Removed,
        Pending,
        PendingRemoval,
    }
}