﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum ProjectTemplateStatusEnum : int
    {
        None,
        Active,
        NotActive,
    }
}