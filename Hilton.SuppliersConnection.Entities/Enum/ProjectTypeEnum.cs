﻿namespace Hilton.SuppliersConnection.Entities
{
    /// <summary>
    ///
    /// </summary>
    public enum ProjectTypeEnum : int
    {
        None,
        Conversion,
        NewDevelopment,
        RoomAddition,
        DCPIP,
        BrandChange,
        SpecialProject,
        ProposedProject,
        QAPIP,
        AdaptiveReuse,
        Extension,
        Renewal,
        Completed
    }
}