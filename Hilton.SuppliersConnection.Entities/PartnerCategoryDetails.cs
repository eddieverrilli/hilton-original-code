﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class PartnerCategoryDetails
    {
        public string CategoryDisplayName
        {
            get;
            set;
        }

        public string BrandDescription
        {
            get;
            set;
        }

        public int BrandId
        {
            get;
            set;
        }

        public string BrandSegment
        {
            get;
            set;
        }

        public string BrandImageUrl
        {
            get;
            set;
        }

        public string PropertyTypeDescription
        {
            get;
            set;
        }

        ////To Be Removed Later
        public IList<PartnerPropertyType> PropertyTypes
        {
            get;
            set;
        }

        public IList<Country> Countries
        {
            get;
            set;
        }

        public IList<Region> Regions
        {
            get;
            set;
        }

        public string RegionDescription
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }       

        public string StyleDescription
        {
            get;
            set;
        }

        public bool IsAvailable
        {
            get;
            set;
        }

        public IList<CategoryStandard> CategoryStandard
        {
            get;
            set;
        }

        public string StandardDescription
        {
            get;
            set;
        }

        public int CategoryId
        {
            get;
            set;
        }
        public IList<Brand> Brands
        {
            get;
            set;
        }


    }
}