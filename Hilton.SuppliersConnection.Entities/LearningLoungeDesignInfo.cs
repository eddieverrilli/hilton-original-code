﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeDesignInfo
    {
        public int DesignInfoId { get; set; }
        public String Logo { get; set; }
        public String LogoThumbnail { get; set; }
        public String Description { get; set; }
        public String Brand { get; set; }
        public String ServiceType { get; set; }
        public String Header { get; set; }
        public String PicsFactsLink { get; set; }
        public String AsiaPacificLink { get; set; }
        public String EMEALink { get; set; }
        public String LatinAmericaLink { get; set; }
        public String NorthAmericaLink { get; set; }
        public String DesignStudioLink { get; set; }

    }
}
