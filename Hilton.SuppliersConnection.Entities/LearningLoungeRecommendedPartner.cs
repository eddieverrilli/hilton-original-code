﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class LearningLoungeRecommendedPartner
    {
        public String CompanyLogoImage
        {
            get;
            set;
        }

        public int PartnerId { get; set; }
    }
}
