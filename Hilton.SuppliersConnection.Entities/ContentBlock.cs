﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class ContentBlock
    {
        public int ContentBlockId
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public LanguageResourceDetail LanguageResourceDetail
        {
            get;
            set;
        }

        public IList<LanguageResourceDetail> LanguageResourceDetails
        {
            get;
            set;
        }

        public int LanguageResourceId
        {
            get;
            set;
        }

        public string Mode
        {
            get;
            set;
        }

        public string Section
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }
    }
}