﻿using System;
using System.Collections.Generic;

namespace Hilton.SuppliersConnection.Entities
{
    [Serializable]
    public class AccountStatus
    {
        public string City
        {
            get;
            set;
        }

        public string CompanyAddress1
        {
            get;
            set;
        }

        public string CompanyAddress2
        {
            get;
            set;
        }

        public string CompanyName
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public string PartnerId
        {
            get;
            set;
        }

        public DateTime? PartnershipActiveDate
        {
            get;
            set;
        }

        public DateTime? PartnershipExpirationDate
        {
            get;
            set;
        }

        public string PartnershipTypeDesc
        {
            get;
            set;
        }

        public int PartnershipTypeId
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public string ZipCode
        {
            get;
            set;
        }

        public int CountryId
        {
            get;
            set;
        }

        public string HiltonId
        {
            get;
            set;
        }

        public int StateId
        {
            get;
            set;
        }

        public string WebSite
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string Fax
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public byte[] CompanyLogoImage
        {
            get;
            set;
        }

        public byte[] CompanyLogoThumbnailImage
        {
            get;
            set;
        }

        public string CompanyLogoName
        {
            get;
            set;
        }

        public IList<Contact> Contacts
        {
            get;
            set;
        }

        public IList<Region> Regions
        {
            get;
            set;
        }
        public IList<Country> Countries
        {
            get;
            set;
        }
        public string PaymentCode
        {
            get;
            set;
        }

        public string CompanyDescription
        {
            get;
            set;
        }
    }
}
