﻿namespace Hilton.SuppliersConnection.Constants
{
    public sealed class AutoCompleteListConstants
    {
        public const string SectionAutoCompleteList = "SectionAutoComplete";
        public const string TitleAutoCompleteList = "TitleAutoComplete";

        private AutoCompleteListConstants()
        { }
    }
}