﻿namespace Hilton.SuppliersConnection.Constants
{
    public sealed class AppSettingConstants
    {
        public const string AdminDashboardUrl = "Admin/Dashboard.aspx";
        public const string ConfigurationSettingsAbsoluteExpiration = "ConfigurationSettingsAbsoluteExpiration";
        public const string ContentBlockAbsoluteExpiration = "ContentBlockAbsoluteExpiration";
        public const string ValidationMessagesAbsoluteExpiration = "ValidationMessagesAbsoluteExpiration";
        public const string RegularExpressionAbsoluteExpiration = "RegularExpressionAbsoluteExpiration";
        public const string DefaultCacheAbsoluteExpiration = "8";
        public const string DynamicDataAbsoluteExpiration = "DynamicDataAbsoluteExpiration";
        public const string DynamicDropDownAbsoluteExpiration = "DynamicDropDownAbsoluteExpiration1";
        public const string ImageFileMaxSize = "ImageFileMaxSize";
        public const string MaxPageSize = "MaxPageSize";
        public const string ConstructionReportMaxPageSize = "ConstructionReportMaxPageSize";
        public const string PageNumberDisplayCount = "PageNumberDisplayCount";
        public const string PageSize = "PageSize";
        public const string PdfFileMaxSize = "PdfFileMaxSize";
        public const string StaticDropDownAbsoluteExpiration = "StaticDropDownAbsoluteExpiration";
        public const string SSLPin = "SSLPin";
        public const string SSLMerchantId = "SSLMerchantId";
        public const string SSLShowForm = "SSLShowForm";
        public const string SSLTransactionType = "SSLTransactionType";
        public const string SSLUserId = "SSLUserId";
        public const string ExportToExcelMaxPageSize = "ExportToExcelMaxPageSize";
        public const string ThumbnailImageWidth = "ThumbnailImageWidth";
        public const string ThumbnailImageHeight = "ThumbnailImageHeight";
        public const string RenewButtonDisplayInterval = "RenewBtnDispalyInterval";
        public const string Culture = "Culture";

        //Added for CE # 86
        public const string UserId = "USERID";
        public const string UserDescription = "USERDESC";
        public const string UserNumber = "USERNUMBER";
        public const string UserType = "USERTYPE";
        public const string UserEmail = "EMailAddress";
        public const string EmployeeId = "employeeid";

        public const string GoogleAnalyticsId = "GoogleAnalyticsId";


        /// <summary>
        ///
        /// </summary>
        private AppSettingConstants()
        { }
    }
}
