﻿namespace Hilton.SuppliersConnection.Constants
{
    public sealed class UIConstants
    {
        public const string ActivationGo = "ActivationGo";
        public const string Add = "Add";
        public const string All = "All";
        public const string AscAbbreviation = "ASC";
        public const string Ascending = "Ascending";
        public const string Descending = "Descending";
        public const string BrandDescription = "BrandDescription";
        public const string BrandId = "BrandId";
        public const string BrandName = "BrandName";
        public const string Change = "Change";
        public const string CompanyName = "CompanyName";
        public const string ContentBlockId = "ContentBlockId";
        public const string CheckPaymentAmount = "CheckPaymentAmount";
        public const string ContentType = "application/vnd.ms-excel";
        public const string DescAbbreviation = "DESC";
        public const string Edit = "Edit";
        public const string EmailTemplateId = "EmailTemplateId";
        public const string ConstructionReportExcelFileName = "attachment;filename=ConstructionReport.xls";
        public const string TransactionSummaryExcelFileName = "attachment;filename=TransactionSummaryReport.xls";
        public const string ProjectProfileExcelFileName = "attachment;filename=ProjectProfile.xls";
        public const string ProjectTemplateDetailsExcelFileName = "attachment;filename=ProjectTemplateDetails.xls";
        public const string FeedbackDefaultCommandSourceId = "partnerLinkButton";
        public const string LastName = "LastName";
        public const string LoginOnGo = "LoginGo";
        public const string MessageDateTime = "MessageDateTime";
        public const string MessageDefaultCommandSourceId = "senderLinkButton";
        public const string Mode = "Mode";
        public const string Model = "SKUNumber";
        public const string New = "New";
        public const string PartnerId = "PartnerId";
        public const string PartnershipTypeId = "PartnershipTypeId";
        public const string PaymentRequestId = "PaymentRequestId";
        public const string ProductListId = "ProductId";
        public const string ProjectId = "ProjectId";
        public const string ProjectTemplate = "ProjectTemplate";
        public const string SenderName = "Sender";
        public const string ProjectAddedDate = "ProjectAddedDate";
        public const string Default = "Default";
        public const string TemplateId = "ProjectTemplateId";
        public const string Update = "Update";
        public const string UserId = "UserId";
        public const string TransactionDate = "TransactionDate";
        public const string PaymentRecordId = "PaymentRecordId";
        public const string TransactionType = "TransactionType";
        public const string Check = "Check";
        public const string Choose = "Choose";
        public const string CategoryId = "CategoryId";
        public const string Required = "Required";
        public const string Optional = "Optional";
        public const string RemoveSelection = "Remove Selection";
        public const string SelectPartnerSelection = "Select & Notify Partner";
        public const string CheckBoxText = "    Selected";
        public const string FeedbackExcelFileName = "attachment;filename=Feedbacks.xls";
        public const string MessageExcelFileName = "attachment;filename=Messages.xls";
        public const string MaxPageSize = "9999";
        public const string Image = "image";
        public const string Pdf = "pdf";
        public const string UserListUI = "Users.aspx";
        public const string ProjectTemplateListUI = "ProjectTemplates.aspx";
        public const string TransactionListUI = "Transactions.aspx";
        public const string ProductListUI = "Products.aspx";
        public const string ProductDetailsUI = "ProductDetails.aspx";
        public const string PartnerListUI = "Partners.aspx";
        public const string ProjectListUI = "Projects.aspx";
        public const string And = " and ";
        public const string CorporateUserType = "C";
        public const string ActivationDate = "ActivationDate";
        public const string ExpirationDate = "ExpirationDate";
        public const string UpgradeFlag = "UpgradePartner";
        public const string CategoryDisplayName = "CategoryDisplayName";
        public const string StatusPermanantlyInactive = "7";

        /// <summary>
        ///
        /// </summary>
        private UIConstants()
        { }
    }
}
