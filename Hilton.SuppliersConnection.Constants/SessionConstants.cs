﻿namespace Hilton.SuppliersConnection.Constants
{
    public sealed class SessionConstants
    {
        public const string ContentSearch = "ContentSearch";
        public const string EmailTemplateSearch = "EmailTemplateSearch";
        public const string PartnerSearch = "PartnerSearch";
        public const string ProductSearch = "ProductSearch";
        public const string ProjectSearch = "ProjectSearch";
        public const string TemplateOperationResult = "TemplateOperationResult";
        public const string TemplateSearch = "TemplateSearch";
        public const string TemplateDetailSearch = "TemplateDetailSearch";
        public const string ProjectTemplateId = "ProjectTemplateId";
        public const string UserData = "UserData";
        public const string User = "User";
        public const string UserId = "UserId";
        public const string IsAuthorized = "IsAuthorized";
        public const string WebMenu = "WebMenu";
        public const string AdminMenu = "AdminMenu";
        public const string UserSearch = "UserSearch";
        public const string FilterCriteria = "FilterCrieteria";
        public const string AppliedOpportunityDetails = "AppliedOpportunityDetails";
        public const string ApplicationDetails = "ApplicationDetails";
        public const string ContactDetails = "ContactDetails";
        public const string PaymentGatewayResponse = "PaymentGatewayResponse";
        public const string TransactionSearch = "TransactionSearch";
        public const string ProjectTemplate = "ProjectTemplate";
        public const string PartnerPayment = "PartnerPayment";
        public const string PaymentRequestId = "PaymentRequestId";
        public const string PartnerProdOptionSearch = "PartnerProdOptionSearch";
        public const string EditPartnerApplication = "EditPartnerApplication";
        public const string ProductId = "ProductId";
        public const string ProjectId = "ProjectId";
        public const string PrintPartnerApplication = "PrintPartnerApplication";
        public const string RecommendedPartnerSearch = "RecommendedPartnerSearch";
        public const string PartnerCriteria = "PartnerCriteria";
        public const string ProductImageBytes = "ProductImageBytes";
        public const string ProductSpecSheetPdf = "ProductSpecSheetPDF";
        public const string ProductSpecSheetPdfName = "ProductSpecSheetPDFName";
        public const string SelectedPartnerId = "SelectedPartnerId";
        public const string SelectedProductId = "SelectedProductId";
        public const string SelectedPartnerProductStatus = "SelectedPartnerProductStatus";
        public const string PartnerId = "PartnerId";
        public const string AllCascadeCategoryIds = "AllCascadeCategoryIds";
        public const string HiltonWorldwideData = "HiltonWorldwideData";
        public const string PartnerProfile = "PartnerProfile";
        public const string PartnerProfileLeafCategory = "PartnerProfileLeafCategory";
        public const string TermsAndCondition = "TermsAndCondition";
        public const string ConstructionReportHotLead = "ConstructionReportHotLead";
        public const string ConstructionReportWarmLead = "ConstructionReportWarmLead";
        public const string ReturnFromCommentPopup = "ReturnFromCommentPopup";
        public const string ConstructionReportMenu = "ConstructionReportMenu";

        /// <summary>
        ///
        /// </summary>
        private SessionConstants()
        { }
    }
}
