﻿namespace Hilton.SuppliersConnection.Constants
{
    public sealed class PaymentGatewayConstants
    {
        public const string SSLPin = "ssl_pin";
        public const string SSLMerchantId = "ssl_merchant_id";
        public const string SSLShowForm = "ssl_show_form";
        public const string SSLTransactionType = "ssl_transaction_type";
        public const string SSLUserId = "ssl_user_id";
        public const string SSLAmount = "ssl_amount";
        public const string SSLResultMessage = "ssl_result_message";
        public const string SSLTransactionId = "ssl_txn_id";
        public const string SSLApprovalCode = "ssl_approval_code";
        public const string SSLErrorCode = "errorCode";
        public const string SSLErrorName = "errorName";
        public const string SSLErrorMessage = "errorMsg";
        public const string SSLTransactionTime = "ssl_txn_time";
        public const string SSLCardNumber = "ssl_card_number";
        public const string SSLResult = "ssl_result";
        public const string Approval = "APPROVAL";
        public const string Declined = "DECLINED";
        public const string Error = "ERROR";
        public const string Code = "Code";
        public const string New = "New";
        public const string UpgradePartner = "UpgradePartner";

        /// <summary>
        ///
        /// </summary>
        private PaymentGatewayConstants()
        { }
    }
}