﻿namespace Hilton.SuppliersConnection.Constants
{
    public sealed class CacheConstants
    {
        public const string ConfigurationSettings = "ConfigurationSettings";
        public const string ContentBlocks = "ContentBlocks";
        public const string DynamicData = "DynamicData";
        public const string DynamicDropDown = "DynamicDropDown";
        public const string LanguageResourceDetails = "LanguageResourceDetails";
        public const string ValidationMessageDetails = "ValidationMessageDetails";
        public const string RegularExpressionTypeDetails = "RegularExpressionTypeDetails";
        public const string StaticDropDown = "StaticDropDown";
        public const string UserData = "UserData";

        /// <summary>
        ///
        /// </summary>
        private CacheConstants()
        { }
    }
}