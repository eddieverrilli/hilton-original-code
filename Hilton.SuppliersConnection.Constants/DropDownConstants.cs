﻿namespace Hilton.SuppliersConnection.Constants
{
    public sealed class DropDownConstants
    {
        public const string BrandDropDown = "BrandDropDown";
public const string CategoriesDropDown = "CategoriesDropDown";
        public const string CountryDropDown = "CountryDropDown";
        public const string FeedbackPartnerDropDown = "FeedbackPartnerDropDown";
        public const string FeedbackStatusDropDown = "FeedbackStatusDropDown";
        public const string TransactionStatusDropDown = "TransactionStatusDropDown";
        public const string LanguageDropDown = "LanguageDropDown";
        public const string MessageRegionDropDown = "MessageRegionDropDown";
        public const string MessageStatusDropDown = "MessageStatusDropDown";
        public const string None = "None";
        public const string O2OstatusDropDown = "o2ostatusDropDown";
        public const string OwnerDropDown = "OwnerDropDown";
        public const string PartnerDropDown = "PartnerDropDown";
        public const string TermsAndConditions = "TermsAndConditions";
        public const string PartnerStatusDropDown = "PartnerStatusDropDown";
        public const string ProductStatusDropDown = "ProductStatusDropDown";
        public const string ProjectManagerDropDown = "ProjectManagerDropDown";
        public const string ProjectStatusDropDown = "ProjectStatusDropDown";
        public const string ProjectTemplateStatusDropDown = "ProjectTemplateStatusDropDown";
        public const string ProjectTypesDropDown = "ProjectTypesDropDown";
        public const string PropertyTypesDropDown = "PropertyTypesDropDown";
        public const string PaymentTypesDropDown = "PaymentTypesDropDown";
        public const string RatingDropDown = "RatingDropDown";
        public const string RecipientDropDown = "RecipientDropDown";
        public const string RegionDropDown = "RegionDropDown";
        public const string SectionDropDown = "SectionDropDown";
        public const string SenderDropDown = "SenderDropDown";
        public const string SenderType = "SenderTypeDropDown";
        public const string StatusDropDown = "StatusDropDown";
        public const string UserStatusDropDown = "UserStatusDropDown";
        public const string DefaultValue = "0";
        public const string UserTypesDropDown = "UserTypesDropDown";
        public const string UserProjectMappingTypeDropDown = "UserProjectMappingTypeDropDown";
        public const string StateDropDown = "StateDropDown";
        public const string ContactDropDown = "ContactDropDown";
        public const string ContactDropDownpartnerProfile = "ContactDropDownPartnerprofile";
        public const string AdminPermissionsDropDown = "AdminPermissionsDropDown";
        public const string ActivePartnersDropDown = "ActivePartnersDropDown";
        public const string OwnerAndConsultantRolesDropDown = "OwnerAndConsultantRolesDropDown";
        public const string PartnershipStatusDropDown = "PartnershipStatusDropDown";
        public const string PartnershipTypeDropDown = "PartnershipTypeDropDown";
        public const string UnitedStates = "1";
        public const string ConsultantRoleDropDown = "ConsultantRoleDropDown";

        /// <summary>
        ///
        /// </summary>
        private DropDownConstants()
        { }
    }
}