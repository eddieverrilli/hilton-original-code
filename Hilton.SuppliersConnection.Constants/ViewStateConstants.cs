﻿namespace Hilton.SuppliersConnection.Constants
{
    public sealed class ViewStateConstants
    {
        public const string AllPageClick = "AllPageClick";
        public const string CategoriesViewState = "CategoriesViewState";
        public const string CommandSourceId = "CommandSourceId";
        public const string ContentBlockViewState = "ContentBlockViewState";
        public const string CurrentPageIndex = "CurrentPageIndex";
        public const string FeedbackViewState = "FeedbackViewState";
        public const string FilterSearchData = "FilterSearchData";
        public const string FilterHierarchySearchData = "FilterHierarchySearchData";
        public const string LastPageClick = "LastPageClick";
        public const string LeafCategoryPartnerProducts = "LeafCategoryPartnerProducts";
        public const string MessageViewState = "MessageViewState";
        public const string PageNumberDisplayCount = "PageNumberDisplayCount";
        public const string PageSet = "PageSet";
        public const string PageSize = "PageSize";
        public const string ProjectId = "ProjectId";
        public const string ProductAssignedSpecification = "ProductAssignedSpecification";
        public const string ProductCategories = "ProductCategories";
        public const string ProductViewState = "ProductViewState";
        public const string ProjectTemplate = "ProjectTemplate";
        public const string SelectedBrand = "SelectedBrand";
        public const string SelectedPropertyType = "SelectedPropertyType";
        public const string SelectedRegion = "SelectedRegion";
        public const string SortDirection = "SortDirection";
        public const string SortExpression = "SortExpression";
        public const string HotLeadSortDirection = "HotLeadSortDirection";
        public const string HotLeadSortExpression = "HotLeadSortExpression";
        public const string WarmLeadSortDirection = "WarmLeadSortDirection";
        public const string WarmLeadSortExpression = "WarmLeadSortExpression";
        public const string TotalPages = "TotalPages";
        public const string TotalRecordCount = "TotalRecordCount";
        public const string CurrentSelectedCategoryId = "CurrentSelectedCategoryId";
        public const string SelectedCategory = "SelectedCategory";
        public const string CountriesViewState = "CountriesViewState";
        public const string RegionalContactsViewState = "RegionalContactsViewState";
        public const string RegionalContactDropDownViewState = "RegionalContactDropDownViewState";
        public const string PartnerCategoriesViewState = "PartnerCategoriesViewState";
        public const string GoldAmount = "GoldAmount";
        public const string RegularAmount = "RegularAmount";
        public const string PartnerContact = "PartnerContact";
        public const string PartnerDetails = "PartnerDetails";
        public const string AllProjectsViewState = "AllProjectsViewState";
        public const string AssociatedProjectViewState = "AssociatedProjectViewState";
        public const string ApprovedCategoriesViewState = "ApprovedCategoriesViewState";
        public const string PartnerProdOptionViewState = "PartnerProdOptionViewState";
        public const string CategoryListAccessViewSate = "CategoryListAccessViewSate";
        public const string CurrentProjectId = "ProjectId";
        public const string MyProjectsViewState = "MyProjectsViewState";
        public const string SelectedValues = "SelectedValues";
        public const string PageMode = "PageMode";
        public const string ConsultantUsers = "ConsultantUsers";
        public const string CurrentUserId = "CurrentUserId";
        public const string AddOrEdit = "AddOrEdit";
        public const string TermsAndconditions = "TermsAndconditions";
        public const string PrimaryContactId = "PrimaryContactId";
        public const string SecondaryContactId = "SecondaryContactId";
        public const string PreviousSelectedRegion = "PreviousSelectedRegion";
        public const string PreviousSelectedCountry = "PreviousSelectedCountry";
        public const string PartnerId = "PartnerId";
        public const string PaymentCode = "PaymentCode";
        public const string PaymentRequestId = "PaymentRequestId";
        public const string PartnershipTypeId = "PartnershipTypeId";
        public const string ActivationDate = "ActivationDate";
        public const string ExpirationDate = "ExpirationDate";
        public const string UpgradePartnerToGold = "UpgradePartnerToGold";
        public const string MultiSearch = "MultiSearch";
        public const string AddOrDelete = "AddOrDelete";
        public const string FilteredCountryRegion = "FilteredCountryRegion";

        /// <summary>
        ///
        /// </summary>
        private ViewStateConstants()
        { }
    }
}