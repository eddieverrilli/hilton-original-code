﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebMaster.master"
    CodeBehind="AccessDenied.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.AccessDenied" %>

<asp:Content ID="contentContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="main-content">
        <h2>
            <asp:Label ID="accessDeniedHeaderLabel" runat="server" CBID="0" Text="Access Denied"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="accessDeniedDescriptionLabel" runat="server" CBID="0" Text=""></asp:Label></p>
    </div>
    <div class="main">
        <div class="inner-content">
            <h3>
                <asp:Label ID="accessDeniedSectionLabel" runat="server" CBID="0" Text="Access Denied"></asp:Label>
            </h3>
            <div class="middle-curve">
                <asp:Label ID="accessDeniedMessageLabel" runat="server" CBID="0" Text="You are not authorized to view secured content. Please contact system administrator."></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>