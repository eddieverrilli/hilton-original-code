﻿<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/MasterPages/WebMaster.master"
    ViewStateMode="Inherit" CodeBehind="TermsAndCondition.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.TermsAndCondition" %>

<asp:Content ID="contentContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="main-content">
        <h2>
            <asp:Label ID="headerLabel" runat="server" CBID="1114" Text="Terms and Condition"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="headerDescLabel" runat="server" Text=""></asp:Label>
        </p>
    </div>
    <div class="main">
        <div class="inner-content" style="margin-right:0px;">
            <h3>
                <asp:Label ID="sectionHeaderLabel" runat="server" CBID="1114" Text="Terms and Condition"></asp:Label>
            </h3>
            <div class="middle-curve">
                <asp:Label ID="termsAndConditionTextLabel" runat="server"></asp:Label>
                <asp:HiddenField ID="termsAndConditionVersionHiddenField" runat="server" />
                <br />
                <br />
                <div>
                    <asp:CheckBox ID="termsAgreementCheckBox" runat="server" ViewStateMode="Enabled"
                        CssClass="chkbox" ValidationGroup="Agree" CBID="0" Text=" I Agree with Terms and Condition" />
                    <asp:CustomValidator ID="termCustomerValidator" CssClass="errorText" Display="Dynamic"
                        ViewStateMode="Enabled" runat="server" ValidationGroup="Agree" OnServerValidate="TermsAndCondition_ServerValidate"></asp:CustomValidator>
                </div>
                <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <br />
                        <div id="resultMessageDiv" runat="server">
                        </div>
                        <br />
                        <div class="termandcondition">
                            <div class="form-btn">
                                <asp:LinkButton ID="agreeLinkButton" ValidationGroup="Agree" ViewStateMode="Enabled"
                                    Text="Agree" runat="server" CBID="0" OnClick="AgreeClick"></asp:LinkButton>
                                <asp:LinkButton ID="disagreeLinkButton" runat="server" CBID="0" CssClass="cancel"
                                    Text="Disagree" OnClick="DisagreeClick"></asp:LinkButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="clear">
                    &nbsp;</div>
            </div>
        </div>
    </div>
</asp:Content>