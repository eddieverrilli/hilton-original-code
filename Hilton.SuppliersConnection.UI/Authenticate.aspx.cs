﻿using System;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Authenticate : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string redirectURL = Request.QueryString["SSORedirect"];

                ClearSession(SessionConstants.User);
                SetSession<User>(SessionConstants.User, new User());
                OnQLogin(redirectURL);
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                Response.Redirect("~/ErrorPage.aspx", false);
            }
        }
    }
}