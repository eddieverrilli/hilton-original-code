﻿using System;
using System.Threading;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI
{
    public partial class DownloadImage : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int productId = Convert.ToInt32(Request.QueryString.Get("productId"));
                int mode = 0;
                if (Request.QueryString.Get("mode") != "undefined")
                {
                    mode = Convert.ToInt32(Request.QueryString.Get("mode"));
                }
                if (mode != 0)
                {
                    Product productPDF =  HelperManager.Instance.GetProductPdf(productId);
                    string fileName = productPDF.SpecSheetPdfName;
                    byte[] fileBytes = productPDF.SpecSheetPdf;
                    ProcessFileDownload("pdf", fileBytes, fileName); 
                }
                else
                {
                    IProductManager productManager = ProductManager.Instance;
                    Product product = productManager.GetProductImage(productId);

                    ProcessFileDownload("image", product.ProductImage, product.ImageName);
                }

                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// To Process file download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            string extension = string.Empty;

            switch (fileType)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "image":
                    string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    int upperBound = tempString.GetUpperBound(0);
                    contentType = "image/" + tempString[upperBound];
                    break;
                default:
                    contentType = "";
                    break;
            };
            if (fileBytes != null)
            {
                Response.Buffer = true;
                Response.Charset = "";
                Response.ClearHeaders();
                Response.ContentType = contentType;
                Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
                Response.BinaryWrite(fileBytes);
                Response.Flush();
                Response.Clear();
                Response.End();
            }
        }
    }
}