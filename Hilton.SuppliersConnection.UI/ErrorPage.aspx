﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" in browser="mozilla" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Suppliers' Connection</title>
    <link type="text/css" href="../Styles/thickbox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../Styles/main.css" />
    <!--[if IE 8]>
<link type='text/css' href='../Styles/ie8.css' rel='stylesheet' />
<![endif]-->
    <link type='text/css' href='../Styles/basic.css' rel='stylesheet' />
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script src="../Scripts/common.js" type="text/javascript"></script>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <div id="divMaster" runat="server">
        <asp:Panel ID="pnlMaster" runat="server" Style="width: 100%; height: 100%">
            <div class="header">
                <div class="wrapper">
                    <h1>
                        <asp:HyperLink ID="homeHyperlink" runat="server" NavigateUrl="~/">Hilton Worldwide</asp:HyperLink>
                    </h1>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <div class="main-content">
                        <h2>
                            <asp:Label ID="errorHeaderLabel" runat="server" CBID="0" Text="Error"></asp:Label>
                        </h2>
                        <p>
                            <asp:Label ID="errorDescriptionLabel" runat="server" CBID="0" Text=""></asp:Label></p>
                    </div>
                    <div class="main">
                        <div class="inner-content">
                            <h3>
                                <asp:Label ID="errorSectionLabel" runat="server" CBID="0" Text="Error"></asp:Label>
                            </h3>
                            <div class="middle-curve">
                                <asp:Label ID="errorMessageLabel" runat="server" CBID="0" Text="An error occurred while processing the request."></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
            </div>
            <div class="footer">
                <div class="wrapper">
                </div>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>