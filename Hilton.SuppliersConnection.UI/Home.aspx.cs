﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

public partial class Home : PageBase
{
    private string _imageURL;
    private IUserManager _userManager;
    private IHiltonWorldwideManager _hiltonWorldwideManager;
    private bool IsUserSessionExists = false;

    /// <summary>
    /// property image
    /// </summary>
    public string PropertyImage
    {
        get { return _imageURL; }
        set { _imageURL = value; }
    }

    /// <summary>
    /// Get the image url
    /// </summary>
    /// <param name="strUrl"></param>
    /// <returns></returns>
    public string ImageUrl(string strUrl)
    {
        string url;
        try
        {
            url = Page.ResolveUrl(strUrl);
        }
        catch (Exception exception)
        {
            url = string.Empty;
            UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        }
        return url;
    }

    /// <summary>
    /// Set the manager instance
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        try
        {
            _userManager = UserManager.Instance;
            IsUserSessionExists = IsSessionExists(SessionConstants.User);
            whatIsSuppliersConnectionHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(13, culture, whatIsSuppliersConnectionHyperLink.Text));
            howToUseSuppliersConnectionHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(14, culture, howToUseSuppliersConnectionHyperLink.Text));
            getAnAccountHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(15, culture, getAnAccountHyperLink.Text));
            howToBecomeRecommendedPartnerHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(16, culture, howToBecomeRecommendedPartnerHyperLink.Text));
        }
        catch (Exception exception)
        {
            UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        }
    }

    /// <summary>
    /// Bind the data to controls
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        //    // case when coming from OnQInsider. The APPID will come in request
        //    if (Request.Params["APPID"] != null)
        //    {
        //        Response.Redirect("~/Authenticate.aspx?SSORedirect=Home.aspx");
        //    }
        //    else
        //    {
        //        headerLabel.Text = ResourceUtility.GetLocalizedString(28, culture, headerLabel.Text);

        //        // Case when user is coming from Hilton Worldwide portal to suppliers connection website
        //        ProcessHiltonworldwideRedirect();

        //        if (!Page.IsPostBack)
        //            SetRecommendedPartnerIncludeImages();

        //        PageBase pageBase = new PageBase();
        //        User user = pageBase.GetSession<User>(SessionConstants.User);
        //        if (user != null && (user.UserTypeId == Convert.ToInt32(UserTypeEnum.Owner) || user.UserTypeId == Convert.ToInt32(UserTypeEnum.Consultant)))
        //        {
        //            outFitHotelPropertyHyperlink.Visible = true;
        //            outFitHotelPropertyHyperlink.NavigateUrl = "~/Account/ProjectList.aspx";
        //        }
        //        else
        //        {
        //            outFitHotelPropertyHyperlink.Visible = false;
        //        }
        //    }

        //string isAuthroizedUser = GetSession<string>(SessionConstants.IsAuthorized);
        //if (!string.IsNullOrEmpty(isAuthroizedUser) && isAuthroizedUser == "false")
        //{
        //    unauthorizedaccess.Visible = true;
        //    ConfigureResultMessage(authorizationDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.UnauthorizedUserMessage, culture));
        //    ClearSession(SessionConstants.IsAuthorized);
        //}
        //else
        //{
        //    unauthorizedaccess.Visible = false;
        //    authorizationDiv.Attributes.Remove("class");
        //    authorizationDiv.InnerHtml = string.Empty;
        //}
        //}
        //catch (ThreadAbortException)
        //{ }
        //catch (Exception exception)
        //{
        //    UserInterfaceExceptionHandler.HandleExcetion(ref exception);
        //    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        //}

        var url = this.Page.Request.RawUrl.ToString();
        if (!url.Contains("IndexChanged"))
        {
            Response.Redirect("~/learninglounge/home.aspx", false);
        }
        var assersion = Request.Form["SAMLResponse"];

        if (assersion != null)
        {
            Session["assersion"] = assersion;
            OnQLogin("");

        }

            headerLabel.Text = ResourceUtility.GetLocalizedString(28, culture, headerLabel.Text);

            // Case when user is coming from Hilton Worldwide portal to suppliers connection website
            ProcessHiltonworldwideRedirect();

            if (!Page.IsPostBack)
                SetRecommendedPartnerIncludeImages();

            PageBase pageBase = new PageBase();
            User user = pageBase.GetSession<User>(SessionConstants.User);
            if (user != null && (user.UserTypeId == Convert.ToInt32(UserTypeEnum.Owner) || user.UserTypeId == Convert.ToInt32(UserTypeEnum.Consultant)))
            {
                outFitHotelPropertyHyperlink.Visible = true;
                outFitHotelPropertyHyperlink.NavigateUrl = "~/Account/ProjectList.aspx";
            }
            else
            {
                outFitHotelPropertyHyperlink.Visible = false;
            }
        

        string isAuthroizedUser = GetSession<string>(SessionConstants.IsAuthorized);
        if (!string.IsNullOrEmpty(isAuthroizedUser) && isAuthroizedUser == "false")
        {
            unauthorizedaccess.Visible = true;
            ConfigureResultMessage(authorizationDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.UnauthorizedUserMessage, culture));
            ClearSession(SessionConstants.IsAuthorized);
        }
        else
        {
            unauthorizedaccess.Visible = false;
            authorizationDiv.Attributes.Remove("class");
            authorizationDiv.InnerHtml = string.Empty;
        }
    }

    /// <summary>
    /// Will set the visibility of become a partner link
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

      
        bool isBecomePartnerAccessible = ValidateMenuAuthorization((int)MenuEnum.BecomeRPartner);
        if (!isBecomePartnerAccessible)
            becomePartnerListItem.Visible = false;
        else
            becomePartnerListItem.Visible = true;


    }

    /// <summary>
    ///  triggers for binding each item in the data list.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DatalistRecommendedPartner_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            ListViewItem item = e.Item;
            if (item.ItemType == ListViewItemType.DataItem)
            {
                ImageButton partnerLinkButton = (ImageButton)(item.FindControl("partnerImageButton"));
                System.Web.UI.WebControls.Image partnerImg = (System.Web.UI.WebControls.Image)(partnerLinkButton.FindControl("partnerImg"));

                //  if (GetSession<User>(SessionConstants.User) != null)
                if (IsUserSessionExists)
                {
                    partnerLinkButton.Visible = true;
                    partnerLinkButton.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(((Partner)(e.Item.DataItem)).CompanyLogoImage);
                    partnerImg.Visible = false;
                    partnerLinkButton.CommandArgument = Convert.ToString(((Partner)(e.Item.DataItem)).PartnerId, CultureInfo.InvariantCulture);
                }
                else
                {
                    partnerImg.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(((Partner)(e.Item.DataItem)).CompanyLogoImage);
                }
            }
        }
        catch (Exception exception)
        {
            UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        }
    }

    /// <summary>
    /// triggers when a command is fired from any item
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DatalistRecommendedPartner_OnItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            SetSession<string>(SessionConstants.PartnerId, e.CommandArgument.ToString());
            ClearSession(SessionConstants.FilterCriteria);
            ClearSession(SessionConstants.AllCascadeCategoryIds);
            ClearSession(SessionConstants.ProductImageBytes);
            Response.Redirect("~/Partners/PartnerProfile.aspx", false);
        }
        catch (ThreadAbortException)
        { }
        catch (Exception exception)
        {
            UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        }
    }

    /// <summary>
    /// Set the recommended partner image
    /// </summary>
    private void SetRecommendedPartnerIncludeImages()
    {
        var recommendedPartnerImages = UserManager.Instance.GetImages;
        if (recommendedPartnerImages.Count == 0)
        {
            recPartnersLabel.Visible = false;
            datalistRecommendedPartner.Visible = false;
            partnerLogos.Visible = false;
        }

        datalistRecommendedPartner.DataSource = recommendedPartnerImages;
        datalistRecommendedPartner.DataBind();
    }

    /// <summary>
    /// Process Hilton worldwide Redirect
    /// </summary>
    private void ProcessHiltonworldwideRedirect()
    {
        string brandCode = string.Empty;
        string regionCode = string.Empty;
        string propertyTypeCode = string.Empty;

        ClearSession(SessionConstants.HiltonWorldwideData);

        if (Request.QueryString != null)
        {
            brandCode = Server.UrlDecode(Request.QueryString["brand"]);
            regionCode = Server.UrlDecode(Request.QueryString["region"]);
            propertyTypeCode = Server.UrlDecode(Request.QueryString["type"]);
        }

        //Post implementation for Hilton worldwide redirection
        NameValueCollection nvc = Request.Form;

        if (!string.IsNullOrEmpty(nvc["brand"]))
        {
            brandCode = Server.UrlDecode(nvc["brand"].ToString(CultureInfo.CurrentCulture));
        }
        if (!string.IsNullOrEmpty(nvc["region"]))
        {
            regionCode = Server.UrlDecode(nvc["region"].ToString(CultureInfo.InvariantCulture));
        }
        if (!string.IsNullOrEmpty(nvc["type"]))
        {
            propertyTypeCode = Server.UrlDecode(nvc["type"].ToString(CultureInfo.InvariantCulture));
        }

        if (!String.IsNullOrWhiteSpace(brandCode) && !String.IsNullOrWhiteSpace(regionCode) && !String.IsNullOrWhiteSpace(propertyTypeCode))
        {
            _hiltonWorldwideManager = HiltonWorldwideManager.Instance;
            HiltonWorldwide hiltonWorldwideData = _hiltonWorldwideManager.GetHiltonWorldwideData(brandCode, regionCode, propertyTypeCode);
            SetSession<HiltonWorldwide>(SessionConstants.HiltonWorldwideData, hiltonWorldwideData);
        }
    }
}