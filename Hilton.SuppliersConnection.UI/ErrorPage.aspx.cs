﻿using System;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ErrorPage : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Bind GoogleAnalyticsId
            Page.Header.DataBind(); 
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
    }
}