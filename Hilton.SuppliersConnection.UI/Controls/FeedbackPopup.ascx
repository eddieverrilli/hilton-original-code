﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeedbackPopup.ascx.cs"
    Inherits="Hilton.SuppliersConnection.UI.FeedbackPopup" %>
<div id="submit-feedback" class="modal">
    <asp:LinkButton ID="linkbtnCrossIcon" CssClass="cross simplemodal-overlay" runat="server">
        <img id="imgCrossIcon" alt="" /></asp:LinkButton>
    <h2>
        <asp:Label ID="lblSubmitFeedback" runat="server" Text="Submit Feedback"></asp:Label></h2>
    <div class="content">
        <div class="box2">
            <h4>
                <asp:Label ID="lblYourInformation" runat="server" Text="YOUR INFORMATION"></asp:Label></h4>
            <div class="content">
                <h5>
                    <asp:Label ID="lblInformationUserName" runat="server" Text="Roger Anyuser"></asp:Label></h5>
                <p class="email">
                    <asp:LinkButton ID="LinkFeedbackEmail" runat="server">roger.anyuser@email.com</asp:LinkButton></p>
                <p class="phone">
                    (614) 555-1234</p>
            </div>
        </div>
        <h3>
            <asp:Label ID="lblProvideFeedback" runat="server" Text="Please provide feedback on:"></asp:Label></h3>
        <p>
            <span>Kohler Bathtub ABCD</span></p>
        <div class="rating">
            <h3>
                <asp:Label ID="lblRating" runat="server" Text="Rating:"></asp:Label></h3>
            <asp:DropDownList ID="ddlRating" runat="server">
                <asp:ListItem>Neutral</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="comment">
            <h3>
                <asp:Label ID="lblComments" runat="server" Text="comments: (Limited to 100 words or less)" />
            </h3>
            <p>
                <%--<textarea name="textarea" id="textarea" cols="45" rows="5">Entered Text</textarea>--%>
                <asp:TextBox ID="TxtboxComments" runat="server" TextMode="MultiLine">Entered Text</asp:TextBox>
            </p>
            <div class="form-btn">
                <asp:HyperLink ID="linkbtnCancel" runat="server" CssClass="simplemodal-overlay">CANCEL</asp:HyperLink>
                <asp:HyperLink ID="linkbtnSend" runat="server" CssClass="thankyou simplemodal-overlay">SEND</asp:HyperLink>
            </div>
            <div class="word-count">
                <div class="word">
                    Word Count:
                </div>
                <div class="count">
                    0</div>
                <%--<div class="count">
                    <asp:Label ID="lblCount" runat="server" ></asp:Label></div>--%>
            </div>
        </div>
    </div>
</div>
<div id="thankyou" class="modal">
    <h2>
        Feedback Received</h2>
    <div class="content">
        <h4>
            Thank You!</h4>
        <p>
            Your feedback has been received.</p>
        <div class="form-btn">
            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="simplemodal-overlay">CLOSE</asp:LinkButton></div>
    </div>
</div>