﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Constants;

namespace Hilton.SuppliersConnection.UI
{
    public partial class PagingControl : UserControl
    {
        public event EventHandler AllClick;

        public event EventHandler FirstClick;

        public event EventHandler LastClick;

        public event EventHandler NextClick;

        public event EventHandler PagedViewClick;

        public event EventHandler PageNumberClick;

        public event EventHandler PreviousClick;

        /// <summary>
        ///
        /// </summary>
        public bool AllPageClick
        {
            get
            {
                this.EnsureChildControls();
                object vs = ViewState[ViewStateConstants.AllPageClick];
                if (vs != null)
                {
                    return Convert.ToBoolean(vs.ToString(), CultureInfo.InvariantCulture);
                }
                else
                {
                    return false;
                }
            }
            set
            {
                this.EnsureChildControls();
                ViewState[ViewStateConstants.AllPageClick] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int CurrentPageIndex
        {
            get
            {
                this.EnsureChildControls();
                object vs = ViewState[ViewStateConstants.CurrentPageIndex];
                if (vs != null)
                {
                    return int.Parse(vs.ToString(), CultureInfo.InvariantCulture);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                this.EnsureChildControls();
                ViewState[ViewStateConstants.CurrentPageIndex] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public bool LastPageClick
        {
            get
            {
                this.EnsureChildControls();
                object vs = ViewState[ViewStateConstants.LastPageClick];
                if (vs != null)
                {
                    return Convert.ToBoolean(vs.ToString(), CultureInfo.InvariantCulture);
                }
                else
                {
                    return false;
                }
            }
            set
            {
                this.EnsureChildControls();
                ViewState[ViewStateConstants.LastPageClick] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int PageNumberDisplayCount
        {
            get
            {
                this.EnsureChildControls();
                object vs = ViewState[ViewStateConstants.PageNumberDisplayCount];
                if (vs != null)
                {
                    return int.Parse(vs.ToString(), CultureInfo.InvariantCulture);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                this.EnsureChildControls();
                ViewState[ViewStateConstants.PageNumberDisplayCount] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int PageSet
        {
            get
            {
                this.EnsureChildControls();
                object vs = ViewState[ViewStateConstants.PageSet];
                if (vs != null)
                {
                    return int.Parse(vs.ToString(), CultureInfo.InvariantCulture);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                this.EnsureChildControls();
                ViewState[ViewStateConstants.PageSet] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int PageSize
        {
            get
            {
                this.EnsureChildControls();
                object vs = ViewState[ViewStateConstants.PageSize];
                if (vs != null)
                {
                    return int.Parse(vs.ToString(), CultureInfo.InvariantCulture);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                this.EnsureChildControls();
                ViewState[ViewStateConstants.PageSize] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int TotalPages
        {
            get
            {
                this.EnsureChildControls();
                object vs = ViewState[ViewStateConstants.TotalPages];
                if (vs != null)
                {
                    return int.Parse(vs.ToString(), CultureInfo.InvariantCulture);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                this.EnsureChildControls();
                ViewState[ViewStateConstants.TotalPages] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int TotalRecordCount
        {
            get
            {
                this.EnsureChildControls();
                object vs = ViewState[ViewStateConstants.TotalRecordCount];
                if (vs != null)
                {
                    return int.Parse(vs.ToString(), CultureInfo.InvariantCulture);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                this.EnsureChildControls();
                ViewState[ViewStateConstants.TotalRecordCount] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            EnsureChildControls();

            pageRepeater.Controls.OfType<RepeaterItem>().ToList().ForEach(repeaterItem =>
            {
                if (repeaterItem.HasControls())
                {
                    repeaterItem.Controls.OfType<LinkButton>().ToList().ForEach(linkButton => linkButton.Click += new EventHandler(GridNavigator_PageNumberClick));
                }
            }
            );
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (TotalRecordCount > 0)
            {
                if (!AllPageClick)
                {
                    BindPageNumberRepeater(TotalRecordCount, PageNumberDisplayCount, CurrentPageIndex, PageSize, LastPageClick);
                }
                SetPaginationVisibility(TotalRecordCount, PageSize, AllPageClick);
            }
            else
            {
                HidePaginationVisibility();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void Page_Load(object sender, EventArgs args)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args != null)
            {
                LinkButton pageNumberLinkButton = (LinkButton)(args.Item.FindControl("pageNumberLinkButton"));
                pageNumberLinkButton.Visible = true;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="totalRecordCount"></param>
        /// <param name="pageNumberDisplayCount"></param>
        /// <param name="currentPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageSet"></param>
        /// <param name="lastPageClick"></param>
        private void BindPageNumberRepeater(int totalRecordCount, int pageNumberDisplayCount, int currentPageIndex, int pageSize, bool lastPageClick)
        {
            int totalPageCount = totalRecordCount / pageSize;
            if ((totalRecordCount % pageSize) > 0)
            {
                totalPageCount += 1;
            }

            if (lastPageClick)
            {
                PageSet = ((totalPageCount - pageNumberDisplayCount) / (pageNumberDisplayCount / 2)) + ((totalPageCount - pageNumberDisplayCount) % (pageNumberDisplayCount / 2));
                if (PageSet < 0)
                {
                    PageSet = 0;
                }
                LastPageClick = false;
            }

            //Logic to extend pagenation
            int startIndex = 1 + (PageSet * (pageNumberDisplayCount / 2));
            int endIndex;
            if (pageNumberDisplayCount > totalPageCount)
            {
                endIndex = totalPageCount;
            }
            else
            {
                endIndex = pageNumberDisplayCount + (PageSet * (pageNumberDisplayCount / 2));
            }

            if ((endIndex < totalPageCount) && (currentPageIndex + 1) == endIndex)
            {
                PageSet += 1;
                startIndex = (currentPageIndex + 1) - (pageNumberDisplayCount / 2);
                endIndex = endIndex + (pageNumberDisplayCount / 2);
            }

            if (endIndex > totalPageCount)
            {
                int diff = endIndex - totalPageCount;
                startIndex = startIndex - diff;
                endIndex = endIndex - diff;
            }

            if ((startIndex > 1) && (currentPageIndex + 1) == startIndex)
            {
                if (PageSet > 0)
                {
                    PageSet -= 1;
                }
                startIndex = 1 + (PageSet * (pageNumberDisplayCount / 2));
                endIndex = startIndex + (pageNumberDisplayCount - 1);
                if (endIndex > totalPageCount)
                {
                    endIndex = totalPageCount;
                }
            }

            ArrayList pageNumberArrayList = new ArrayList();

            for (int pageCount = startIndex; pageCount <= endIndex; pageCount++)
            {
                pageNumberArrayList.Add(pageCount);
            }

            pageRepeater.DataSource = pageNumberArrayList;
            pageRepeater.DataBind();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void GridNavigator_AllClick(object sender, System.EventArgs args)
        {
            if (AllClick != null)
            {
                //Pagination Control Setting
                CurrentPageIndex = 0;
                PageSet = 0;
                AllPageClick = true;

                AllClick(this, args);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void GridNavigator_FirstClick(object sender, System.EventArgs args)
        {
            if (FirstClick != null)
            {
                //Pagination Control Setting
                CurrentPageIndex = 0;
                PageSet = 0;

                FirstClick(this, args);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void GridNavigator_LastClick(object sender, System.EventArgs args)
        {
            if (LastClick != null)
            {
                //Pagination Control Setting
                int totalRecordCount = TotalRecordCount;
                int pageCount = (totalRecordCount / PageSize);
                if ((totalRecordCount % PageSize) > 0)
                {
                    pageCount += 1;
                }
                CurrentPageIndex = pageCount - 1;
                LastPageClick = true;

                LastClick(this, args);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void GridNavigator_NextClick(object sender, System.EventArgs args)
        {
            if (NextClick != null)
            {
                //Pagination Control Setting
                CurrentPageIndex += 1;

                NextClick(this, args);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void GridNavigator_PagedViewClick(object sender, System.EventArgs args)
        {
            if (PagedViewClick != null)
            {
                //Pagination Control Setting
                CurrentPageIndex = 0;
                PageSet = 0;
                AllPageClick = false;

                PagedViewClick(this, args);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void GridNavigator_PageNumberClick(object sender, System.EventArgs args)
        {
            if (PageNumberClick != null)
            {
                PageNumberClick(sender, args);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void GridNavigator_PreviousClick(object sender, System.EventArgs args)
        {
            if (PreviousClick != null)
            {
                //Pagination Control Setting
                CurrentPageIndex -= 1;

                PreviousClick(this, args);
            }
        }

        /// <summary>
        ///
        /// </summary>
        private void HidePaginationVisibility()
        {
            firstLinkButton.Visible = false;
            previousLinkButton.Visible = false;
            nextLinkButton.Visible = false;
            lastLinkButton.Visible = false;
            pageViewLinkButton.Visible = false;
            allLinkButton.Visible = false;
            pageRepeater.Visible = false;
            pageNumberDiv.Visible = false;
        }

        /// <summary>
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.firstLinkButton.Click += new System.EventHandler(GridNavigator_FirstClick);
            this.previousLinkButton.Click += new System.EventHandler(GridNavigator_PreviousClick);
            this.nextLinkButton.Click += new System.EventHandler(GridNavigator_NextClick);
            this.lastLinkButton.Click += new System.EventHandler(GridNavigator_LastClick);
            this.pageViewLinkButton.Click += new System.EventHandler(GridNavigator_PagedViewClick);
            this.allLinkButton.Click += new System.EventHandler(GridNavigator_AllClick);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="totalRecordCount"></param>
        /// <param name="pageDisplayCount"></param>
        /// <param name="currentPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="allPageClick"></param>
        private void SetPaginationVisibility(int totalRecordCount, int pageSize, bool allPageClick)
        {
            if (allPageClick)
            {
                HidePaginationVisibility();
                pageRepeater.Visible = false;
                //  pageViewLinkButton.Visible = true; // Setting to true will display paged view option to the user
                allLinkButton.Visible = true;
                allLinkButton.Attributes.Add("class", "active");
                allLinkButton.Attributes.Add("href", "javascript:void(0);");
            }
            else
            {
                pageViewLinkButton.Visible = false;
                pageRepeater.Visible = true;
                pageNumberDiv.Visible = false; // Setting to true will display total pages to the user
                allLinkButton.Attributes.Remove("class");
                allLinkButton.Attributes.Remove("href");
                int pageCount = (TotalRecordCount / PageSize);
                if ((totalRecordCount % pageSize) > 0)
                {
                    pageCount += 1;
                }

                if (pageCount > 1)
                {
                    allLinkButton.Visible = true;
                }
                else
                {
                    allLinkButton.Visible = false;
                }

                totalPagesLabel.Text = pageCount.ToString(CultureInfo.InvariantCulture);
                //Hide next link for last record
                if (CurrentPageIndex == (pageCount - 1))
                {
                    nextLinkButton.Visible = false;
                    lastLinkButton.Visible = false;
                }
                else
                {
                    nextLinkButton.Visible = true;
                    lastLinkButton.Visible = true;
                }

                //Hide previous link for first record
                if (CurrentPageIndex == 0)
                {
                    previousLinkButton.Visible = false;
                    firstLinkButton.Visible = false;
                }
                else
                {
                    previousLinkButton.Visible = true;
                    firstLinkButton.Visible = true;
                }

                pageRepeater.Controls.OfType<RepeaterItem>().ToList().ForEach(repeaterItem =>
                {
                    if (repeaterItem.HasControls())
                    {
                        repeaterItem.Controls.OfType<LinkButton>().ToList().ForEach(linkButton =>
                            {
                                if (Convert.ToInt32(linkButton.Text, CultureInfo.InvariantCulture) == CurrentPageIndex + 1)
                                {
                                    linkButton.Attributes.Add("class", "active");
                                    linkButton.Attributes.Add("href", "javascript:void(0);");
                                    currentPageLabel.Text = (CurrentPageIndex + 1).ToString(CultureInfo.InvariantCulture);
                                }
                            }
                            );
                    }
                }
                );
            }
        }
    }
}