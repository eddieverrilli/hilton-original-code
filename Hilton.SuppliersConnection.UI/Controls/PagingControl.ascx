﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingControl.ascx.cs"
    Inherits="Hilton.SuppliersConnection.UI.PagingControl" %>
<div class="paging-number" runat="server" id="pageNumberDiv" visible="false">
    <span class="label">Page :</span>
    <asp:Label ID="currentPageLabel" CssClass="tdLabel1" runat="server"></asp:Label>
    <span class="label">of </span>
    <asp:Label ID="totalPagesLabel" CssClass="tdLabel1" runat="server"></asp:Label>
</div>
<div class="paging">
    <asp:LinkButton ID="firstLinkButton" Visible="false" runat="server" Text="&#8249;&#8249 FIRST" />
    <asp:LinkButton ID="previousLinkButton" Visible="false" runat="server" Text="&#8249;&#8249 PREV" />
    <asp:Repeater ID="pageRepeater" runat="server" OnItemDataBound="PageRepeater_ItemDataBound">
        <ItemTemplate>
            <asp:LinkButton ID="pageNumberLinkButton" Visible="false" runat="server" Text='<%# Container.DataItem %>' />
        </ItemTemplate>
    </asp:Repeater>
    <asp:LinkButton ID="nextLinkButton" Visible="false" Text="NEXT &#8250;&#8250" runat="server" />
    <asp:LinkButton ID="lastLinkButton" Visible="false" Text="LAST &#8250;&#8250" runat="server" />
    <asp:LinkButton ID="pageViewLinkButton" Visible="false" Text="Paged View" runat="server" />
    <asp:LinkButton ID="allLinkButton" Visible="false" Text="ALL" runat="server" />
</div>