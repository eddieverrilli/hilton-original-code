﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebMaster.master"
    CodeBehind="ProcessPaymentRequest.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ProcessPaymentRequest" %>

<asp:Content ID="processPayment" ContentPlaceHolderID="cphContent" runat="server">
    <div class="main-content">
        <h2>
            Process Payment
        </h2>
        <p>
            <asp:Label ID="lblHeaderDescription" runat="server" Text=""></asp:Label></p>
    </div>
    <div class="main">
        <div class="inner-content">
            <h3>
                Process Payment</h3>
            <div class="middle-curve">
                <asp:Label ID="errorMessageLabel" runat="server"></asp:Label>
            </div>
        </div>
</asp:Content>