﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True" MaintainScrollPositionOnPostback="true"
    CodeBehind="PaymentForm.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.PaymentForm" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="RecommendedPartnersHyperLink" runat="server" NavigateUrl="~/Partners/RecommendedPartners.aspx"
            CBID="3" Text="Recommended Partners"></asp:HyperLink>
        <span>/</span>
        <asp:HyperLink ID="BecomePartnerHyperLink" runat="server" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"
            CBID="37" Text="Become a Partner"></asp:HyperLink>
        <span>/</span> <span>
            <asp:Label ID="breadCrumLabel" runat="server" CBID="945" Text="Complete Application"></asp:Label></span>
    </div>
    <div class="main-content">
        <div class="steps">
            <a href="#" class="done"></a><a href="#" class="active"></a><a href="#"></a><a href="#">
            </a>
        </div>
        <h2>
            <asp:Label ID="pageHeaderLabel" runat="server" CBID="42" Text="Pay and Activate Your Account"></asp:Label></h2>
        <p>
            <asp:Label ID="pageIntroductionLabel" runat="server" CBID="43" Text=""></asp:Label></p>
    </div>
    <div class="main">
        <div class="payment-form">
            <div class="accordion approved-cat">
                <h3>
                    <asp:Label ID="approvedCategoriesLabel" runat="server" CBID="231" Text="Approved Categories" /></h3>
                <div class="panel3">
                    <asp:GridView ID="selectedCategoriesGridView" GridLines="None" runat="server" AutoGenerateColumns="false"
                        OnRowDataBound="SelectedCategoriesGridView_RowDataBound">
                        <RowStyle BorderStyle="None" />
                        <Columns>
                            <asp:BoundField DataField="CategoryName" HeaderText="Category" ItemStyle-CssClass="col1"
                                HeaderStyle-CssClass="col1">
                                <HeaderStyle CssClass="col1"></HeaderStyle>
                                <ItemStyle CssClass="col1"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField ItemStyle-CssClass="col2" HeaderStyle-CssClass="col2" HeaderText="Region">
                                <ItemTemplate>
                                    <asp:Repeater ID="regionRepeaters" runat="server">
                                        <ItemTemplate>
                                            <p>
                                                <%# Container.DataItem %>
                                            </p>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col3" HeaderStyle-CssClass="col3" HeaderText="Brand">
                                <ItemTemplate>
                                    <asp:Repeater ID="brandRepeaters" runat="server">
                                        <ItemTemplate>
                                            <p>
                                                <%# Container.DataItem %>
                                            </p>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Property Type" ItemStyle-CssClass="col4" HeaderStyle-CssClass="col4">
                                <ItemTemplate>
                                    <itemstyle cssclass="col4"></itemstyle>
                                    <asp:Repeater ID="repeaterTypes" runat="server">
                                        <ItemTemplate>
                                            <p>
                                                <%# Container.DataItem %>
                                            </p>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="accordion account-type">
                <h3>
                    <asp:Label ID="accountTypeLabel" runat="server" CBID="165" Text="Account Type"></asp:Label>
                </h3>
                <div class="panel3">
                    <table width="100%">
                        <tr>
                            <td class="eligible" colspan="3">
                                <asp:Label ID="accountTypeIntroLabel" runat="server" CBID="946" Text="Choose the partner type for which you’d like to apply:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="col1">
                                <asp:RadioButton ID="goldLevelPartnerRadioButton" GroupName="PaymentType" runat="server" />
                            </td>
                            <td class="col2">
                                <div>
                                    <span class="gold-level"></span>
                                    <h6>
                                        <asp:Label ID="goldPartnerLabel" runat="server" CBID="411" Text=" Gold-Level Partner"></asp:Label>
                                    </h6>
                                    <p>
                                        <asp:Label ID="goldPartnerIntroLabel" runat="server" CBID="947" Text="Silver-Level benefits, plus access to Hilton’s Construction Report and Lead Generation."></asp:Label></p>
                                </div>
                            </td>
                            <td class="col3">
                                <h6>
                                    <asp:Label ID="goldAmountLabel" runat="server"></asp:Label></h6>
                                <p>
                                    <asp:Label ID="perYearGoldLabel" runat="server" CBID="510" Text="Per Year"></asp:Label></p>
                                <asp:HiddenField ID="goldAmountHiddenField" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="border">
                            </td>
                        </tr>
                        <tr>
                            <td class="col1">
                                <asp:RadioButton ID="regularPartnerRadioButton" GroupName="PaymentType" runat="server" />
                            </td>
                            <td class="col2">
                                <div>
                                    <span class="normal-level"></span>
                                    <h6>
                                        <asp:Label ID="regularPartnerLabel" runat="server" CBID="612" Text="Partner"></asp:Label></h6>
                                    <p>
                                        <asp:Label ID="regularPartnerIntroLabel" runat="server" CBID="948" Text="Listing on Suppliers’ Connection"></asp:Label></p>
                                </div>
                            </td>
                            <td class="col3">
                                <h6>
                                    <asp:Label ID="regularAmountLabel" runat="server"></asp:Label></h6>
                                <p>
                                    <asp:Label ID="perYearLabel" runat="server" CBID="510" Text="Per Year"></asp:Label></p>
                                <asp:HiddenField ID="regularAmountHiddenField" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="information-forms">
                <div class="contact-form whiteBG">
                    <h3>
                        <asp:Label ID="contactInformationLabel" runat="server" CBID="325" Text=" Contact Information"></asp:Label></h3>
                    <div class="content">
                        <ul>
                            <li>
                                <div class="label">
                                </div>
                                <div class="input mandatory mandatoryBG">
                                    <span>Indicates a Required Field</span></div>
                            </li>
                            <li>
                                <asp:Label ID="firstNameLabel" runat="server" CBID="100" Text="First Name" CssClass="label"></asp:Label>
                                <div class="input mandatory mandatoryBG ">
                                    <asp:TextBox ID="firstNameTextBox" runat="server" ReadOnly="true" ViewStateMode="Enabled"
                                        CssClass="textbox readonly"></asp:TextBox>
                                </div>
                                <asp:Label ID="firstNameValiationLabel" runat="server" Text="" CssClass="errorText "></asp:Label>
                            </li>
                            <li>
                                <asp:Label ID="lastNameLabel" runat="server" CBID="112" Text="Last Name" CssClass="label"></asp:Label>
                                <div class="input mandatory mandatoryBG">
                                    <asp:TextBox ID="lastNameTextBox" runat="server" ReadOnly="true" ViewStateMode="Enabled"
                                        CssClass="textbox readonly"></asp:TextBox>
                                </div>
                                <asp:Label ID="lastNameValiationLabel" runat="server" Text="" CssClass="errorText"></asp:Label>
                            </li>
                            <li>
                                <asp:Label ID="titleLabel" runat="server" CBID="711" Text="Title" CssClass="label"></asp:Label>
                                <div class="input">
                                    <asp:TextBox ID="titleTextBox" runat="server" ReadOnly="true" ViewStateMode="Enabled"
                                        CssClass="textbox readonly"></asp:TextBox>
                                </div>
                            </li>
                            <li>
                                <asp:Label ID="phoneLabel" runat="server" CBID="125" Text="Phone" CssClass="label"></asp:Label>
                                <div class="input mandatory mandatoryBG">
                                    <asp:TextBox ID="phoneTextBox" runat="server" ReadOnly="true" ViewStateMode="Enabled"
                                        MaxLength="32" CssClass="textbox readonly"></asp:TextBox>
                                </div>
                                <br />
                                <asp:RequiredFieldValidator ID="phoneRequriedFieldValidator" ViewStateMode="Enabled"
                                    SetFocusOnError="true" runat="server" VMTI="6" ValidationGroup="PaymentForm"
                                    ControlToValidate="phoneTextBox" CssClass="errorText paymentForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="phoneRegularExpressionValidator" ViewStateMode="Enabled"
                                    SetFocusOnError="true" runat="server" ControlToValidate="phoneTextBox" VMTI="7"
                                    CssClass="errorText paymentForm" ValidationGroup="PaymentForm" Display="Dynamic"></asp:RegularExpressionValidator>
                            </li>
                            <li>
                                <asp:Label ID="faxLabel" runat="server" CBID="142" Text="Fax" CssClass="label"></asp:Label>
                                <div class="input mandatory mandatoryBG">
                                    <asp:TextBox ID="faxTextBox" runat="server" ViewStateMode="Enabled" ReadOnly="true"
                                        CssClass="textbox readonly"></asp:TextBox>
                                </div>
                            </li>
                            <li>
                                <asp:Label ID="emailLabel" runat="server" CBID="385" Text="Email Address" CssClass="label"></asp:Label>
                                <div class="input mandatory mandatoryBG">
                                    <asp:TextBox ID="emailAddressTextBox" runat="server" ReadOnly="true" ViewStateMode="Enabled"
                                        CssClass="textbox readonly"></asp:TextBox>
                                </div>
                                <asp:Label ID="emailValiationLabel" runat="server" Text="" CssClass="errorText"></asp:Label>
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div class="contact-form flR whiteBG">
                    <h3>
                        <asp:Label ID="billingInfoLabel" runat="server" CBID="250" Text="Billing Information"></asp:Label></h3>
                    <asp:UpdatePanel ID="billingInfoUpdatePanel" ViewStateMode="Enabled" runat="server"
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="content">
                                <ul>
                                    <li class="checkbox">
                                        <div class="label">
                                        </div>
                                        <div class="input">
                                            <asp:CheckBox ID="sameContactInfoCheckBox" runat="server" ViewStateMode="Enabled"
                                                CBID="633" Text="Same as contact?" OnCheckedChanged="chkSameContactInfo_Checked"
                                                AutoPostBack='true' />
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="billingFirstNameLabel" runat="server" CBID="100" Text="First Name"
                                            CssClass="label"></asp:Label>
                                        <div class="input mandatory mandatoryBG">
                                            <asp:TextBox ID="billingfirstNameTextBox" runat="server" ViewStateMode="Enabled"
                                                CssClass="textbox"></asp:TextBox>
                                        </div>
                                        <br />
                                        <asp:RequiredFieldValidator ID="billingFirstNameRequiredField" ViewStateMode="Enabled"
                                            SetFocusOnError="true" runat="server" VMTI="1" ValidationGroup="PaymentForm"
                                            ControlToValidate="billingfirstNameTextBox" CssClass="errorText paymentForm"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="billingFirstNameRegularExpressionValidator" ViewStateMode="Enabled"
                                            SetFocusOnError="true" runat="server" ControlToValidate="billingfirstNameTextBox"
                                            VMTI="2" CssClass="errorText paymentForm" ValidationGroup="PaymentForm" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </li>
                                    <li>
                                        <asp:Label ID="billingLastNameLabel" runat="server" CBID="112" Text="Last Name" CssClass="label"></asp:Label>
                                        <div class="input mandatory mandatoryBG">
                                            <asp:TextBox ID="billingLastNameTextBox" runat="server" ViewStateMode="Enabled" CssClass="textbox"></asp:TextBox>
                                        </div>
                                        <br />
                                        <asp:RequiredFieldValidator ID="lastNameRequiredFieldValidator" ViewStateMode="Enabled"
                                            SetFocusOnError="true" runat="server" VMTI="3" ValidationGroup="PaymentForm"
                                            ControlToValidate="billingLastNameTextBox" CssClass="errorText paymentForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="lastNameRegularExpressionValidator" ViewStateMode="Enabled"
                                            SetFocusOnError="true" runat="server" ControlToValidate="billingLastNameTextBox"
                                            VMTI="4" CssClass="errorText paymentForm" ValidationGroup="PaymentForm" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </li>
                                    <li>
                                        <asp:Label ID="billingTitleLabel" runat="server" CBID="711" Text="Title" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="billingTitleTextBox" runat="server" ViewStateMode="Enabled" CssClass="textbox"></asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator ID="billingTitleRegularExpressionValidator" SetFocusOnError="true"
                                            runat="server" ControlToValidate="billingTitleTextBox" VMTI="5" CssClass="errorText paymentForm"
                                            ValidationGroup="PaymentForm" ViewStateMode="Enabled" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </li>
                                    <li>
                                        <asp:Label ID="billingPhoneNumberLabel" runat="server" CBID="125" Text="Phone" CssClass="label"></asp:Label>
                                        <div class="input mandatory mandatoryBG">
                                            <asp:TextBox ID="billingPhoneNumberTextBox" runat="server" ViewStateMode="Enabled"
                                                MaxLength="32" CssClass="textbox"></asp:TextBox>
                                        </div>
                                        <br />
                                        <asp:RequiredFieldValidator ID="billingPhoneNumberRequiredFieldValidator" ViewStateMode="Enabled"
                                            SetFocusOnError="true" runat="server" VMTI="6" ValidationGroup="PaymentForm"
                                            ControlToValidate="billingPhoneNumberTextBox" CssClass="errorText paymentForm"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="billingPhoneNumberRegularExpressionValidator"
                                            ViewStateMode="Enabled" SetFocusOnError="true" runat="server" ControlToValidate="billingPhoneNumberTextBox"
                                            VMTI="7" CssClass="errorText paymentForm" ValidationGroup="PaymentForm" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </li>
                                    <li>
                                        <asp:Label ID="billingFaxLabel" runat="server" CBID="142" Text="Fax" CssClass="label"></asp:Label>
                                        <div class="input mandatory mandatoryBG">
                                            <asp:TextBox ID="billingFaxTextBox" runat="server" ViewStateMode="Enabled" CssClass="textbox"></asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="billingFaxRequiredFieldValidator" ViewStateMode="Enabled"
                                            SetFocusOnError="true" runat="server" VMTI="8" ValidationGroup="PaymentForm"
                                            ControlToValidate="billingFaxTextBox" CssClass="errorText paymentForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="faxBillingRegularExpressionValidator" SetFocusOnError="true"
                                            ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="billingFaxTextBox"
                                            ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="PaymentForm"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </li>
                                    <li>
                                        <asp:Label ID="billingEmailLabel" runat="server" CBID="385" Text="Email Address"
                                            CssClass="label"></asp:Label>
                                        <div class="input mandatory mandatoryBG">
                                            <asp:TextBox ID="billingEmailTextBox" runat="server" ViewStateMode="Enabled" CssClass="textbox"></asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="emailRequiredField" SetFocusOnError="true" ViewStateMode="Enabled"
                                            runat="server" VMTI="10" ValidationGroup="PaymentForm" ControlToValidate="billingEmailTextBox"
                                            CssClass="errorText paymentForm" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" ViewStateMode="Enabled"
                                            SetFocusOnError="true" runat="server" CssClass="errorText paymentForm" VMTI="11"
                                            ControlToValidate="billingEmailTextBox" ValidationGroup="PaymentForm"></asp:RegularExpressionValidator>
                                    </li>
                                </ul>
                                <div class="clear">
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                                <asp:PostBackTrigger ControlID="sameContactInfoCheckBox" />
                            </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="box1">
                <h3>
                    <asp:Label ID="paymentInfoLabel" runat="server" CBID="503" Text="Payment Information"></asp:Label></h3>
                <div class="content">
                    <div class="go">
                        <asp:LinkButton ID="continueLinkButton" runat="server" ViewStateMode="Enabled" CBID="339"
                            OnClick="ContinueLinkButton_Click" Text="Continue" ValidationGroup="PaymentForm"></asp:LinkButton>
                        <asp:HiddenField ID="paymentRequestIdHiddenField" runat="server" />
                    </div>
                    <p>
                        <asp:Label ID="paymentTypeLabel" runat="server" CBID="505" Text="Payment Type"></asp:Label>
                        <asp:DropDownList ID="paymentTypeDropDown" ViewStateMode="Enabled" runat="server">
                        </asp:DropDownList>
                    </p>
                    <asp:RequiredFieldValidator ID="paymentTypeRequiredFieldValidator" ViewStateMode="Enabled"
                        SetFocusOnError="true" runat="server" ErrorMessage="Please select Payment Type"
                        ValidationGroup="PaymentForm" InitialValue="0" ControlToValidate="paymentTypeDropDown"
                        CssClass="errorText paymentFormPad100" Display="Dynamic"></asp:RequiredFieldValidator>
                    <div class="clear">
                        &nbsp;</div>
                    <br />
                    <asp:Label ID="continueWarningLabel" runat="server" CssClass="pytfrm-warning" CBID="1127"
                        Text=""></asp:Label>
                    <br />
                    <div id="resultMessageDiv" runat="server">
                    </div>
                    <br />
                    <div class="clear">
                        &nbsp;</div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="sidePanelHeaderLabel" runat="server" CBID="949" Text="Recommended Partners"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="activateHyperLink" runat="server" CBID="1038" Text="ACTIVATE"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="receiptHyperLink" runat="server" class="active" CBID="1039" Text="RECEIPT"></asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>
