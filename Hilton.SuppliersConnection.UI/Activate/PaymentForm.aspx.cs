﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class PaymentForm : PageBase
    {
        private IPartnerManager _partnerManager;

        /// <summary>
        /// Fires OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _partnerManager = PartnerManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(88, culture, "Payment Form");

                SetGridViewHeaderText();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Sets the Header Text
        /// </summary>
        private void SetGridViewHeaderText()
        {
            selectedCategoriesGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(282, culture, "Category");
            selectedCategoriesGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(591, culture, "Region");
            selectedCategoriesGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(253, culture, "Brand");
            //selectedCategoriesGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(564, culture, "Property Type");
        }

        /// <summary>
        /// Posts the form with collected data
        /// </summary>
        /// <param name="page"></param>
        /// <param name="destinationUrl"></param>
        /// <param name="data"></param>
        public static void RedirectAndPost(Page page, string destinationUrl, NameValueCollection data)
        {
            string strForm = PreparePostForm(destinationUrl, data);
            //the Post Form, this is to submit the Posting form with the request.
            page.Controls.Add(new LiteralControl(strForm));
        }

        /// <summary>
        /// Binds the Approved categories
        /// </summary>
        /// <param name="partnerId"></param>
        protected void BindApprovedCategories(int partnerId)
        {
            try
            {
                _partnerManager = PartnerManager.Instance;
                DataSet dsAppCat = _partnerManager.GetApprovedCategories(partnerId);
                SetViewState<DataSet>(ViewStateConstants.ApprovedCategoriesViewState, dsAppCat);

                StringBuilder strAppCat = new StringBuilder();

                var cat = (from r in dsAppCat.Tables[0].AsEnumerable()
                           select r.Field<Int32>("CategoryId")).Distinct();

                foreach (var c in cat)
                {
                    strAppCat.Append(c.ToString(CultureInfo.InvariantCulture) + ',');
                }

                IList<ApprovedPartnershipSpecification> appPartnerCatHierarchy = _partnerManager.GetAppCategoryHierarchy(strAppCat.ToString());

                ViewState["appPartnerCatHierarchy"] = appPartnerCatHierarchy;

                selectedCategoriesGridView.DataSource = dsAppCat.Tables[0];
                selectedCategoriesGridView.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Binds the DropDowns
        /// </summary>
        protected void BindDropDown()
        {
            try
            {
                BindStaticDropDown(DropDownConstants.PaymentTypesDropDown, paymentTypeDropDown, ResourceUtility.GetLocalizedString(1249, culture, "Please Select"), null);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Fires on contact info check box check changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkSameContactInfo_Checked(object sender, EventArgs e)
        {
            try
            {
                if (sameContactInfoCheckBox.Checked)
                {
                    billingfirstNameTextBox.Text = firstNameTextBox.Text.ToString(CultureInfo.InvariantCulture).Trim();
                    billingLastNameTextBox.Text = lastNameTextBox.Text.ToString(CultureInfo.InvariantCulture).Trim();
                    billingTitleTextBox.Text = titleTextBox.Text.ToString(CultureInfo.InvariantCulture).Trim();
                    billingPhoneNumberTextBox.Text = phoneTextBox.Text.ToString(CultureInfo.InvariantCulture).Trim();
                    billingFaxTextBox.Text = faxTextBox.Text.ToString(CultureInfo.InvariantCulture).Trim();
                    billingEmailTextBox.Text = emailAddressTextBox.Text.ToString(CultureInfo.InvariantCulture).Trim();
                    billingfirstNameTextBox.ReadOnly = true;
                    billingLastNameTextBox.ReadOnly = true;
                    billingTitleTextBox.ReadOnly = true;
                    billingPhoneNumberTextBox.ReadOnly = true;
                    billingFaxTextBox.ReadOnly = true;
                    billingEmailTextBox.ReadOnly = true;
                    billingfirstNameTextBox.CssClass = "textbox readonly";
                    billingLastNameTextBox.CssClass = "textbox readonly";
                    billingPhoneNumberTextBox.CssClass = "textbox readonly";
                    billingTitleTextBox.CssClass = "textbox readonly";
                    billingFaxTextBox.CssClass = "textbox readonly";
                    billingEmailTextBox.CssClass = "textbox readonly";
                }
                else
                {
                    billingfirstNameTextBox.ReadOnly = false;
                    billingLastNameTextBox.ReadOnly = false;
                    billingTitleTextBox.ReadOnly = false;
                    billingPhoneNumberTextBox.ReadOnly = false;
                    billingFaxTextBox.ReadOnly = false;
                    billingEmailTextBox.ReadOnly = false;
                    billingfirstNameTextBox.CssClass = "textbox";
                    billingLastNameTextBox.CssClass = "textbox";
                    billingTitleTextBox.CssClass = "textbox";
                    billingPhoneNumberTextBox.CssClass = "textbox";
                    billingFaxTextBox.CssClass = "textbox";
                    billingEmailTextBox.CssClass = "textbox";
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Fills the Contact info information on fire
        /// </summary>
        /// <param name="partnerId"></param>
        protected void FillContactInformation(int partnerId)
        {
            try
            {
                Contact contact = _partnerManager.GetPartnerContactDetails(partnerId);
                firstNameTextBox.Text = contact.FirstName.ToString(CultureInfo.InvariantCulture).Trim();
                lastNameTextBox.Text = contact.LastName.ToString(CultureInfo.InvariantCulture).Trim();
                titleTextBox.Text = contact.Title.ToString(CultureInfo.InvariantCulture).Trim();
                phoneTextBox.Text = contact.Phone.ToString(CultureInfo.InvariantCulture).Trim();
                faxTextBox.Text = contact.Fax.ToString(CultureInfo.InvariantCulture).Trim();
                emailAddressTextBox.Text = contact.Email.ToString(CultureInfo.InvariantCulture).Trim();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Fires on Continue Link Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContinueLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (paymentTypeDropDown.SelectedIndex != 0 && (goldLevelPartnerRadioButton.Checked || regularPartnerRadioButton.Checked))
                {
                    PartnerPayment partnerPayment = new PartnerPayment();
                    partnerPayment.PaymentRequestId = Convert.ToInt32(paymentRequestIdHiddenField.Value, CultureInfo.InvariantCulture);
                    partnerPayment.RequestType = PaymentGatewayConstants.New;
                    partnerPayment.Contacts = new List<Contact>();
                    partnerPayment.Culture = Convert.ToString(culture, CultureInfo.InvariantCulture);
                    partnerPayment.Contacts.Add(new Contact
                    {
                        FirstName = billingfirstNameTextBox.Text,
                        LastName = billingLastNameTextBox.Text,
                        Title = billingTitleTextBox.Text,
                        Phone = billingPhoneNumberTextBox.Text,
                        Fax = billingFaxTextBox.Text,
                        Email = billingEmailTextBox.Text,
                        ContactTypeId = (int)ContactTypeEnum.Billing,
                    });

                    if (Convert.ToInt16(paymentTypeDropDown.SelectedValue, CultureInfo.InvariantCulture) == (int)PaymentTypeEnum.MailCheck)
                    {
                        if (goldLevelPartnerRadioButton.Checked)
                        {
                            Context.Items[UIConstants.CheckPaymentAmount] = goldAmountHiddenField.Value;
                            partnerPayment.AmountPaid = goldAmountHiddenField.Value;
                            partnerPayment.PartnershipType = (int)PartnershipTypeEnum.GoldPartner;
                        }
                        else
                        {
                            Context.Items[UIConstants.CheckPaymentAmount] = regularAmountHiddenField.Value;
                            partnerPayment.AmountPaid = regularAmountHiddenField.Value;
                            partnerPayment.PartnershipType = (int)PartnershipTypeEnum.Partner;
                        }
                        partnerPayment.PaymentType = ((int)PaymentTypeEnum.MailCheck).ToString(CultureInfo.InvariantCulture);

                        //Update Db to record the before continue state
                        int partnerPaymentId = _partnerManager.UpdatePartnerPaymentDetails(partnerPayment);

                        if (partnerPaymentId > 0)
                        {
                            Server.Transfer("~/Partners/PendingCheckPayment.aspx", false);
                        }
                        else if (partnerPaymentId == 0)
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.CheckRequestAlreadyProcessed, culture));
                        }
                        else
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                        }
                    }
                    else
                    {
                        List<Contact> contactDetails = new List<Contact>();
                        contactDetails.Add(new Contact
                        {
                            FirstName = firstNameTextBox.Text,
                            LastName = lastNameTextBox.Text,
                            Title = titleTextBox.Text,
                            Phone = phoneTextBox.Text,
                            Fax = faxTextBox.Text,
                            Email = emailAddressTextBox.Text,
                            ContactTypeId = (int)ContactTypeEnum.Primary,
                        });
                        contactDetails.Add(new Contact
                        {
                            FirstName = billingfirstNameTextBox.Text,
                            LastName = billingLastNameTextBox.Text,
                            Title = billingTitleTextBox.Text,
                            Phone = billingPhoneNumberTextBox.Text,
                            Fax = billingFaxTextBox.Text,
                            Email = billingEmailTextBox.Text,
                            ContactTypeId = (int)ContactTypeEnum.Billing,
                        });

                        SetSession<List<Contact>>(SessionConstants.ContactDetails, contactDetails);

                        if (goldLevelPartnerRadioButton.Checked)
                        {
                            partnerPayment.AmountRequested = goldAmountHiddenField.Value;
                            partnerPayment.PartnershipType = (int)PartnershipTypeEnum.GoldPartner;
                        }
                        else
                        {
                            partnerPayment.AmountRequested = regularAmountHiddenField.Value;
                            partnerPayment.PartnershipType = (int)PartnershipTypeEnum.Partner;
                        }

                        partnerPayment.PaymentType = ((int)PaymentTypeEnum.CreditCard).ToString(CultureInfo.InvariantCulture);
                        SetSession<PartnerPayment>(SessionConstants.PartnerPayment, partnerPayment);

                        //Update Db to record the before continue
                        int partnerPaymentId = _partnerManager.UpdatePartnerPaymentDetails(partnerPayment);

                        if (partnerPaymentId > 0)
                        {
                            var parameters = GetParameters();
                            RedirectAndPost(this.Page, ConfigurationManager.AppSettings["PaymentGatewayUrl"], parameters);
                        }
                        else if (partnerPaymentId == -2)
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.PaymentAlreadyProcessed, culture));
                        }
                        else
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                        }
                    }
                }
                else
                {
                    //Prompt to select payment type, partnership type etc
                    if (paymentTypeDropDown.SelectedIndex == 0)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.MandatoryPaymentType, culture));
                    }
                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.MandatoryAccountType, culture));
                    }
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Fires Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                resultMessageDiv.Attributes.Remove("class");
                resultMessageDiv.InnerText = string.Empty;

                if (!IsPostBack)
                {
                    BindDropDown();

                    SetRegularExpressions();

                    if (Context.Items[UIConstants.PartnerId] != null && Context.Items[UIConstants.PaymentRequestId] != null && Context.Items[UIConstants.PartnershipTypeId] != null)
                    {
                        int partnerId = Convert.ToInt32(Context.Items[UIConstants.PartnerId], CultureInfo.InvariantCulture);
                        int paymentRequestId = Convert.ToInt32(Context.Items[UIConstants.PaymentRequestId], CultureInfo.InvariantCulture);

                        int partnershipTypeId = Convert.ToInt32(Context.Items[UIConstants.PartnershipTypeId], CultureInfo.InvariantCulture);

                        SetViewState<string>(ViewStateConstants.PartnerId, Convert.ToString(partnerId));
                        SetViewState<string>(ViewStateConstants.PaymentRequestId, Convert.ToString(paymentRequestId));
                        SetViewState<string>(ViewStateConstants.PartnershipTypeId, Convert.ToString(partnershipTypeId));
                        SetViewState<string>(ViewStateConstants.ActivationDate, Convert.ToString(Context.Items[UIConstants.ActivationDate]));
                        SetViewState<string>(ViewStateConstants.ExpirationDate, Convert.ToString(Context.Items[UIConstants.ExpirationDate]));
                        SetViewState<string>(ViewStateConstants.UpgradePartnerToGold, Convert.ToString(Context.Items[UIConstants.UpgradeFlag]));

                        if (!String.IsNullOrEmpty(Convert.ToString(Context.Items[UIConstants.ActivationDate])) && !String.IsNullOrEmpty(Convert.ToString(Context.Items[UIConstants.ExpirationDate])))
                        {
                            if (partnershipTypeId == (int)PartnershipTypeEnum.GoldPartner)
                            {
                                regularPartnerRadioButton.Enabled = false;
                                goldLevelPartnerRadioButton.Checked = true;
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(Context.Items[UIConstants.UpgradeFlag])))
                                {
                                    goldLevelPartnerRadioButton.Enabled = true;
                                    goldLevelPartnerRadioButton.Checked = true;
                                    regularPartnerRadioButton.Enabled = false;
                                }
                                else
                                {
                                    goldLevelPartnerRadioButton.Enabled = false;
                                    regularPartnerRadioButton.Checked = true;
                                }
                            }
                        }
                        else
                        {
                            if (partnershipTypeId == (int)PartnershipTypeEnum.GoldPartner)
                            {
                                regularPartnerRadioButton.Checked = false;
                                goldLevelPartnerRadioButton.Checked = true;
                            }
                            else
                            {
                                goldLevelPartnerRadioButton.Checked = false;
                                regularPartnerRadioButton.Checked = true;
                            }
                        }
                        SetSession<string>(SessionConstants.PaymentRequestId, paymentRequestId.ToString(CultureInfo.InvariantCulture));

                        paymentRequestIdHiddenField.Value = paymentRequestId.ToString(CultureInfo.InvariantCulture);

                        FillContactInformation(partnerId);

                        BindApprovedCategories(partnerId);

                        using (DataSet dsPaymentAmts = _partnerManager.GetPaymentRequestAmounts(paymentRequestId))
                        {
                            goldAmountLabel.Text = Convert.ToDecimal(dsPaymentAmts.Tables[0].Rows[0][0], CultureInfo.InvariantCulture).ToString("C", new CultureInfo("en-US"));
                            goldAmountHiddenField.Value = dsPaymentAmts.Tables[0].Rows[0][0].ToString();

                            regularAmountLabel.Text = Convert.ToDecimal(dsPaymentAmts.Tables[0].Rows[0][1], CultureInfo.InvariantCulture).ToString("C", new CultureInfo("en-US"));
                            regularAmountHiddenField.Value = dsPaymentAmts.Tables[0].Rows[0][1].ToString();
                        }
                    }
                }
                else
                {
                    SetLoginDetails();
                    SetRegularExpressions();

                    if (GetViewState<string>(ViewStateConstants.PartnerId) != null && GetViewState<string>(ViewStateConstants.PaymentRequestId) != null && GetViewState<string>(ViewStateConstants.PartnershipTypeId) != null)
                    {
                        int partnerId = Convert.ToInt32(GetViewState<string>(ViewStateConstants.PartnerId), CultureInfo.InvariantCulture);
                        int paymentRequestId = Convert.ToInt32(GetViewState<string>(ViewStateConstants.PaymentRequestId), CultureInfo.InvariantCulture);

                        int partnershipTypeId = goldLevelPartnerRadioButton.Checked ? (int)PartnershipTypeEnum.GoldPartner : (int)PartnershipTypeEnum.Partner;

                        if (goldLevelPartnerRadioButton.Checked)
                        {
                            SetViewState<string>(ViewStateConstants.PartnershipTypeId, Convert.ToString((int)PartnershipTypeEnum.GoldPartner));
                        }
                        else
                        {
                            SetViewState<string>(ViewStateConstants.PartnershipTypeId, Convert.ToString((int)PartnershipTypeEnum.Partner));
                        }


                        if (!String.IsNullOrEmpty(GetViewState<string>(ViewStateConstants.ActivationDate)) && !String.IsNullOrEmpty(GetViewState<string>(ViewStateConstants.ExpirationDate)))
                        {
                            if (partnershipTypeId == (int)PartnershipTypeEnum.GoldPartner)
                            {
                                regularPartnerRadioButton.Enabled = false;
                                goldLevelPartnerRadioButton.Checked = true;
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(GetViewState<string>(ViewStateConstants.UpgradePartnerToGold)))
                                {
                                    goldLevelPartnerRadioButton.Enabled = true;
                                    regularPartnerRadioButton.Checked = false;
                                }
                                else
                                {
                                    goldLevelPartnerRadioButton.Enabled = false;
                                    regularPartnerRadioButton.Checked = true;
                                }
                            }
                        }
                        else
                        {
                            if (partnershipTypeId == (int)PartnershipTypeEnum.GoldPartner)
                            {
                                regularPartnerRadioButton.Checked = false;
                                goldLevelPartnerRadioButton.Checked = true;
                            }
                            else
                            {
                                goldLevelPartnerRadioButton.Checked = false;
                                regularPartnerRadioButton.Checked = true;
                            }
                        }
                        SetSession<string>(SessionConstants.PaymentRequestId, paymentRequestId.ToString(CultureInfo.InvariantCulture));

                        paymentRequestIdHiddenField.Value = paymentRequestId.ToString(CultureInfo.InvariantCulture);

                        FillContactInformation(partnerId);

                        BindApprovedCategories(partnerId);

                        using (DataSet dsPaymentAmts = _partnerManager.GetPaymentRequestAmounts(paymentRequestId))
                        {
                            goldAmountLabel.Text = Convert.ToDecimal(dsPaymentAmts.Tables[0].Rows[0][0], CultureInfo.InvariantCulture).ToString("C", new CultureInfo("en-US"));
                            goldAmountHiddenField.Value = dsPaymentAmts.Tables[0].Rows[0][0].ToString();

                            regularAmountLabel.Text = Convert.ToDecimal(dsPaymentAmts.Tables[0].Rows[0][1], CultureInfo.InvariantCulture).ToString("C", new CultureInfo("en-US"));
                            regularAmountHiddenField.Value = dsPaymentAmts.Tables[0].Rows[0][1].ToString();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will set the validation expression for regular expression validator based on culture
        /// </summary>
        private void SetRegularExpressions()
        {
            // Regular Expression for Name
            SetValidatorRegularExpressions(billingFirstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(billingTitleRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);
            SetValidatorRegularExpressions(faxBillingRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
            // Regular Expression for website
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(phoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(billingPhoneNumberRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
        }

        /// <summary>
        /// This method will set the login details
        /// </summary>
        private void SetLoginDetails()
        {
            if (this.Master != null)
            {
                var adminButton = this.Master.FindControl("adminLinkButton");
                if (adminButton != null)
                    adminButton.Visible = false;
                var barImage = this.Master.FindControl("barImage");
                if (barImage != null)
                    barImage.Visible = false;

                Label userName = (Label)this.Master.FindControl("userNameLabel");
                LinkButton loginButton = (LinkButton)this.Master.FindControl("loginLinkButton");

                var user = (User)Session[SessionConstants.User];

                if (user != null && userName != null && loginButton != null)
                {
                    userName.Text = "Welcome, " + user.FirstName + " " + user.LastName;
                    loginButton.Text = "Logout";
                    if (user.UserTypeId == (int)UserTypeEnum.Administrator)
                    {
                        if (adminButton != null)
                            adminButton.Visible = true;
                        if (barImage != null)
                            barImage.Visible = true;
                    }
                }
            }
        }

        /// <summary>
        /// Fires RowdataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedCategoriesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataSet dsAppCat = GetViewState<DataSet>(ViewStateConstants.ApprovedCategoriesViewState);
                    IList<ApprovedPartnershipSpecification> appPartnerCatHierarchy = (IList<ApprovedPartnershipSpecification>)ViewState["appPartnerCatHierarchy"];
                    var catHierarcy = Convert.ToString(appPartnerCatHierarchy.Where(x => x.CategoryId == Convert.ToInt32(((DataRowView)(e.Row.DataItem)).Row.ItemArray[0], CultureInfo.InvariantCulture)).Select(c => c.Sequence).FirstOrDefault(), CultureInfo.InvariantCulture);
                    e.Row.Cells[0].Text = catHierarcy;

                    GridViewRow gr = e.Row;
                    Repeater repeaterRegion = new Repeater();
                    repeaterRegion = (Repeater)(gr.FindControl("regionRepeaters"));
                    var regions = (from r in dsAppCat.Tables[2].AsEnumerable()
                                   where r.Field<Int32>("CategoryId") == Convert.ToInt32(((DataRowView)(e.Row.DataItem)).Row.ItemArray[0], CultureInfo.InvariantCulture)
                                   select r.Field<string>("Region")).Distinct();
                    repeaterRegion.DataSource = regions.ToArray<string>();
                    repeaterRegion.DataBind();

                    Repeater repeaterBrand = new Repeater();
                    repeaterBrand = (Repeater)(gr.FindControl("brandRepeaters"));
                    var brand = (from r in dsAppCat.Tables[1].AsEnumerable()
                                 where r.Field<Int32>("CategoryId") == Convert.ToInt32(((DataRowView)(e.Row.DataItem)).Row.ItemArray[0], CultureInfo.InvariantCulture)
                                 select r.Field<string>("Brand")).Distinct();

                    repeaterBrand.DataSource = brand.ToArray<string>();
                    repeaterBrand.DataBind();

                    //Repeater repeaterType = new Repeater();
                    //repeaterType = (Repeater)(gr.FindControl("repeaterTypes"));
                    //var propertyTypes = (from r in dsAppCat.Tables[3].AsEnumerable()
                    //                     where r.Field<Int32>("CategoryId") == Convert.ToInt32(((DataRowView)(e.Row.DataItem)).Row.ItemArray[0], CultureInfo.InvariantCulture)
                    //                     select r.Field<string>("PropertyType")).Distinct();
                    //repeaterType.DataSource = propertyTypes.ToArray<string>();
                    //repeaterType.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Prepares post form HTML
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private static String PreparePostForm(string url, NameValueCollection data)
        {
            string formID = "PostForm";

            StringBuilder strForm = new StringBuilder();
            strForm.Append("<form id=\"" + formID + "\" name=\"" +
                           formID + "\" action=\"" + url +
                           "\" method=\"POST\">");

            foreach (string key in data)
            {
                strForm.Append("<input type=\"hidden\" name=\"" + key +
                               "\" value=\"" + data[key] + "\">");
            }

            strForm.Append("</form>");

            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language='javascript'>");
            strScript.Append("var v" + formID + " = document." +
                             formID + ";");
            strScript.Append("v" + formID + ".submit();");
            strScript.Append("</script>");

            return strForm.ToString() + strScript.ToString();
        }

        /// <summary>
        /// Gets The parameters
        /// </summary>
        /// <returns></returns>
        private NameValueCollection GetParameters()
        {
            var parameters = new NameValueCollection();
            parameters[PaymentGatewayConstants.SSLMerchantId] = ConfigurationStore.GetAppSetting(AppSettingConstants.SSLMerchantId); //PaymentGatewayConstants.SSLMERCHANTID;
            parameters[PaymentGatewayConstants.SSLShowForm] = ConfigurationStore.GetAppSetting(AppSettingConstants.SSLShowForm); //PaymentGatewayConstants.SSLSHOWFORM;
            parameters[PaymentGatewayConstants.SSLTransactionType] = ConfigurationStore.GetAppSetting(AppSettingConstants.SSLTransactionType); //PaymentGatewayConstants.SSLTRANSACTIONTYPE;
            parameters[PaymentGatewayConstants.SSLPin] = ConfigurationStore.GetAppSetting(AppSettingConstants.SSLPin); //PaymentGatewayConstants.SSL_PIN;
            parameters[PaymentGatewayConstants.SSLUserId] = ConfigurationStore.GetAppSetting(AppSettingConstants.SSLUserId); //PaymentGatewayConstants.SSLUSERID;
            if (goldLevelPartnerRadioButton.Checked)
                parameters[PaymentGatewayConstants.SSLAmount] = goldAmountHiddenField.Value.ToString(CultureInfo.InvariantCulture).Trim();
            if (regularPartnerRadioButton.Checked)
                parameters[PaymentGatewayConstants.SSLAmount] = regularAmountHiddenField.Value.ToString(CultureInfo.InvariantCulture).Trim();

            return parameters;
        }
    }
}
