﻿using System;
using System.Threading;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ProcessPaymentRequest : PageBase
    {
        private IPartnerManager _partnerManager;

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _partnerManager = PartnerManager.Instance;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString[PaymentGatewayConstants.Code] != null)
                {
                    SupressPageHeader();

                    string paymentRequestCode = Request.QueryString[PaymentGatewayConstants.Code].ToString();
                    string UpgradePartner = "";
                    if (Request.QueryString[PaymentGatewayConstants.UpgradePartner] != null)
                    {
                        UpgradePartner = Request.QueryString[PaymentGatewayConstants.UpgradePartner].ToString();
                    }
                   
                    PaymentRequestDetails paymentRequestDetails = _partnerManager.GetPaymentCodeDetails(paymentRequestCode);

                    if (paymentRequestDetails != null)
                    {
                        Context.Items[UIConstants.PartnerId] = paymentRequestDetails.PartnerId;
                        Context.Items[UIConstants.PaymentRequestId] = paymentRequestDetails.PaymentRequestId;
                        Context.Items[UIConstants.PartnershipTypeId] = paymentRequestDetails.PartnershipTypeId;
                        Context.Items[UIConstants.ActivationDate] = paymentRequestDetails.ActivationDate;
                        Context.Items[UIConstants.ExpirationDate] = paymentRequestDetails.ExpirationDate;
                        Context.Items[UIConstants.UpgradeFlag] = UpgradePartner;
                        Server.Transfer("~/Activate/PaymentForm.aspx", false);
                    }
                    else
                    {
                        // Display error message that code is invalid or expired.
                        errorMessageLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidCode, culture); 
                    }
                }
                else
                {
                    Response.Redirect("~/AccessDenied.aspx", false);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }
    }
}
