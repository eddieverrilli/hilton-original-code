﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LearningLounge/LearningLoungeMaster.Master" AutoEventWireup="true" CodeBehind="ADC-ContactsAdmin.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LearningLounge.Admin.ADC_ContactsAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

	<!-- modals -->
	<div id="edit-modal" class="modal fade has-top-border">
		<div class="modal-dialog" style="width:470px">
			<div class="modal-content">
				<div class="modal-border">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-row orange" style="margin-top:5px;">
                        <label>Name</label>
					    <input type="text" id="txtName" style="width:400px" />
                    </div>
                    <div class="modal-row orange" style="margin-top:5px;">
                        <label>Phone</label>
					    <input type="text" id="txtPhone" style="width:400px" />
                    </div>
                    <div class="modal-row orange" style="margin-top:5px;">
                        <label>Position</label>
					    <input type="text" id="txtPosition" style="width:400px" />
                    </div>
	  				<div class="modal-row blue" style="margin-top:25px; height:70px;">
                        <div class="col-sm-12 col-md-6">
	  					    <button id="btnSave" type="button" class="btn btn-secondary" data-dismiss="modal">Save</button>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="$('#edit-modal').modal('hide');">Cancel</button>
                        </div>
	  				</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /#brands-modal -->    

	<!-- .container -->
    <div class="container support-center">    
        <div class="adc-admin-title"></div>
        <select id="cboMapType" class="mapAdminSelect" onchange="cboMapType_Change();">
            <option label="Select Map" value="NONE" />
            <option label="North America, Focused Service" value="focused-service" />
            <option label="North America, Full Service" value="full-service" />
            <option label="North America, Tru" value="tru" />
            <option label="Europe, Middle East & Africa" value="emea" />
            <option label="Latin America - Caribbean" value="latin-america-and-caribbean" />
        </select>

        <br /><br />
        <label>Vice President/Coordinator</label>
        <table class="display mapAdminTable" id="tableMain"></table>
        
        <label style="margin-top:50px">Administrative Contacts</label>
        <table class="display mapAdminTable" id="tableGeneral"></table>

        <label style="margin-top:50px">Additional Personnel</label>
        <table class="display mapAdminTable" id="tableAdditional"></table>

        <label style="margin-top:50px">Regional Directors</label>
        <table class="display mapAdminTable" id="tableRegionalDirectors"></table>

    </div>

    <!-- page specific js -->
    <script type="text/javascript">
        var _editContactId;

        (function ($) {
            //hide the nav trigger
            $('#nav-trigger').hide();

        })(jQuery);


        function getContacts() {
            var activeMap = $('#cboMapType').val();

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/GetMapInfo",
                contentType: "application/json; charset=utf-8",
                data: "{mapName: '" + activeMap + "'}",
                dataType: "json",
                success: function (data) {
                    var response = data.d;

                    $.each(response.map, function (mapKey, mapValue) {
                        initMainContactsTable(mapValue.main_contacts.contacts);
                        initGeneralContactsTable(mapValue.general_contacts.contacts);
                        initAdditionalContactsTable(mapValue.additional_contacts.contacts);
                        initRegionalDirectorsTable(mapValue.regional_directors.directors);
                    });
                }

            });
        }

        function cboMapType_Change() {
            $('.adc-admin-title').html($('#cboMapType option:selected')[0].label);

            getContacts();
        }

        function initMainContactsTable(contacts) {
            $('#tableMain').DataTable({
                data: contacts,
                "paging": false,
                "searching": false,
                "bDestroy": true,
                columns: [
                    { data: "name", title: "Name" },
                    { data: "position", title: "Position" },
                    { data: "phone", title: "Phone" },
                    {
                        "title": "Options", "sWidth": "60px", "render":
                            function (data, type, row) {
                                return '<span><a href="javascript:showEditForm(' + row.ContactId + ',\'' + row.name + '\',\'' + row.position + '\',\'' + row.phone + '\');">Edit</a></span>';
                            }
                    }
                ]
            });
        }

        function initGeneralContactsTable(contacts) {
            $('#tableGeneral').DataTable({
                data: contacts,
                "paging": false,
                "searching": false,
                "bDestroy": true,
                columns: [
                    { data: "region", title: "Region" },
                    { data: "name", title: "Name" },
                    { data: "position", title: "Position" },
                    { data: "phone", title: "Phone" },
                    {
                        "title": "Options", "sWidth": "60px", "render":
                            function (data, type, row) {
                                return '<span><a href="javascript:showEditForm(' + row.ContactId + ',\'' + row.name + '\',\'' + row.position + '\',\'' + row.phone + '\');">Edit</a></span>';
                            }
                    }
                ]
            });
        }

        function initAdditionalContactsTable(contacts) {
            $('#tableAdditional').DataTable({
                data: contacts,
                "paging": false,
                "searching": false,
                "bDestroy": true,
                columns: [
                    { data: "name", title: "Name" },
                    { data: "position", title: "Position" },
                    { data: "phone", title: "Phone" },
                    {
                        "title": "Options", "sWidth": "60px", "render":
                            function (data, type, row) {
                                return '<span><a href="javascript:showEditForm(' + row.ContactId + ',\'' + row.name + '\',\'' + row.position + '\',\'' + row.phone + '\');">Edit</a></span>';
                            }
                    }
                ]
            });
        }

        function initRegionalDirectorsTable(contacts) {
            $('#tableRegionalDirectors').DataTable({
                data: contacts,
                "paging": false,
                "searching": false,
                "bDestroy": true,
                columns: [
                    { data: "region", title: "Region" },
                    { data: "region_slug", title: "Area Covered" },
                    { data: "name", title: "Name" },
                    { data: "position", title: "Position" },
                    { data: "phone", title: "Phone" },
                    {
                        "title": "Options", "sWidth": "60px", "render":
                            function (data, type, row) {
                                return '<span><a href="javascript:showEditForm(' + row.ContactId + ',\'' + row.name + '\',\'' + row.position + '\',\'' + row.phone + '\');">Edit</a></span>';
                            }
                    }
                ]
            });
        }

        function saveEdits() {

            var name =$('#txtName').val();
            var phone = $('#txtPhone').val();
            var position = $('#txtPosition').val();

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/UpdateContact",
                data: "{contact: {'ContactId': " + _editContactId + ",'phone': '" + phone + "','position':'" + position + "','name':'" + name + "'}}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var returnValue = data.d;
                    console.log(returnValue);

                    alert('The contact was successfully updated.');
                    getContacts();
                }
            });

        }

        function showEditForm(contactId, name, position, phone) {
            _editContactId = contactId;

            $('#txtName').val(name);
            $('#txtPhone').val(phone);
            $('#txtPosition').val(position);

            $('#edit-modal').modal('show');
        }

        $('#btnSave').on('click', function () {
            saveEdits();
        });
    </script>
</asp:Content>
