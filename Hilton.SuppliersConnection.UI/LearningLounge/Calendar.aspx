﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LearningLounge/LearningLoungeMaster.Master" AutoEventWireup="true" CodeBehind="Calendar.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LearningLounge.Calendar"  %>
<%@ MasterType VirtualPath="~/LearningLounge/LearningLoungeMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
		<!-- calendar calendar css -->
		<link rel="stylesheet" href="css/calendar/calendar.css" />
		<link rel="stylesheet" href="css/calendar/calendar_full.css" />
		<link rel="stylesheet" href="css/calendar/calendar_compact.css" />
		<link rel="stylesheet" href="css/calendar/style.css" />
		<link rel="stylesheet" href="css/calendar/bootstrap-responsive.css" />

		<!-- calendar js -->
		<script src="js/calendar/en.js"></script>
		<script src="js/calendar/calendar.js"></script>
        

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

		<!-- modals --> 
		<div id="create-event-modal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-border">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
							<div class="modal-header-col col1">&nbsp;</div>
							<div class="modal-header-col col2"><h4 class="modal-title"><span id="lblCreateEditDupTitle" class="light">Create</span><span style="margin-left:10px">Calendar Meeting</span></h4></div>
							<div class="modal-header-col col3">&nbsp;</div>
			  			</div> 
			  			<div class="modal-body">
				  			<p class="body-description">Fill out the form below to create an event. To upload an image, click the "BROWSE" button.</p>
				  			<div class="modal-col col1">
					  			<div role="form">
									<div class="form-group">
										<input type="text" class="form-control" id="txtEventName" name="txtEventName" placeholder="Event Name"/>
									</div>
									<div class="form-group">
							            <div class='input-group date' id='picker-start-date'>
							                <input id="txtStartDate" name="txtStartDate" type='text' class="form-control" placeholder="Start Date & Time" />
							                <span class="input-group-addon">
							                    <span class="glyphicon glyphicon-calendar"></span>
							                </span>
							            </div>
								    </div>
								    <div id="event-all-day-container">
										<div class="input-group checkbox">
											<span class="input-group-addon">
												<label><input id="chkAllDayEvent" name="chkAllDayEvent" type="checkbox" aria-label="All Day Event" value="allday" />This is an all day event</label>
											</span>
										</div>
									</div>
									<!-- /#event-users -->
							        <div class="form-group end-date">
							            <div class='input-group date' id='picker-end-date'>
							                <input id="txtEndDate" type='text' class="form-control" placeholder="End Date & Time" runat="server" />
							                <span class="input-group-addon">
							                    <span class="glyphicon glyphicon-calendar"></span>
							                </span>
							            </div>
							        </div>
							        <div class="form-group">
										<input type="text" class="form-control" id="txtLocation" placeholder="Location"  runat="server" />
									</div>
									<div class="form-group">
										<textarea class="form-control" id="txtDescription" rows="5" placeholder="Description"  runat="server" ></textarea>
									</div>
							        <div class="form-group">
										<input type="text" class="form-control" id="txtEventURL" name="txtEventURL" placeholder="Event URL" />
									</div>																		
									<div id="event-categories-container">
										<div class="input-header">Event Categories:</div>
										<p class="input-p">Please select the appropriate categories for this event.</p>
										<div class="input-group checkbox">
											<span class="input-group-addon">
                                                <label><input type="checkbox" id="chkHiltonOffice" name="chkHiltonOffice" value="on" aria-label="Hilton Office" />Hilton Office</label>
												<label><input type="checkbox" id="chkMeetAndGreet" name="chkMeetAndGreet" value="on" aria-label="Meet & Greets" />Meet & Greets</label>
												<label><input type="checkbox" id="chkIndustryEvent" name="chkIndustryEvent" value="on" aria-label="Industry Events" />Industry Events</label>
												<label><input type="checkbox" id="chkLunchAndLearn" name="chkLunchAndLearn" value="on" aria-label="Lunch and Learns"  />Lunch & Learns</label>
                                                <label><input type="checkbox" id="chkHiltonConference" name="chkHiltonConference" value="on" aria-label="Hilton Conferences"  />Hilton Conferences</label>
											</span>
										</div>
									</div>                                
									
									<div id="event-users-container">
										<div class="input-header">Permissions:</div>
										<p class="input-p">Please select who will be allowed to see this event.</p>
										<div class="input-group checkbox">
											<span class="input-group-addon">
												<label><input type="checkbox" id="chkEveryone" name="chkEveryone" aria-label="Everyone" checked  />Everyone</label>
												<label><input type="checkbox" id="chkPartners" name="chkPartners" aria-label="Partners" />Partners</label>
												<label><input type="checkbox" id="chkOwnersConsultants" name="chkOwnersConsultants" aria-label="Owners/Consultants" />Owners/Consultants</label>
												<label><input type="checkbox" id="chkEmployeesOnly" name="chkEmployeesOnly" aria-label="Hilton Employees Only" />Hilton Employees Only</label>
											</span>
										</div>
									</div>
									<!-- /#event-users -->
								</div>
							</div>
				  			<div class="modal-col col2">
					  			<div id="event-image-container"></div>
								<div id="image-upload-wrapper">
									<div id="uploader-text"></div>
									<p class="input-p centered">Acceptable image formats are .jpg or .png & the recommended image width is at least 350px.</p>
									<label class="btn btn-secondary btn-file">
										Browse <input id="fileUpload" type="file" runat="server" style="display: none;" />
									</label>
								</div>
				  			</div>
			  			</div>
			  			<div class="modal-footer">
				  			<button id="btnSubmit" type="submit" onclick="return btnSubmit_Click();" class="btn btn-primary submit">Submit</button>
			  			</div>
					</div>
				</div>
			</div>
		</div>

	<div class="container">

		<!-- .navigation-panel -->
		<div id="navigation-panel-wrapper" class="navigation-panel-mask">
			<div id="navigation-panel">
			    <div class="wrapper">
				    <ul>
                        <li><a href="Home.aspx" target="_blank">Home</a></li>
					    <li><a href="../Partners/RecommendedPartners.aspx" target="_blank">Browse Recommended Partners</a></li>
					    <li><a href="javascript:navigateMyAccount();">My Account</a></li>
					    <li><a href="calendar.aspx" target="_blank">Calendar</a></li>
					    <li><a href="javascript:meetNGreetTileClicked();">Meet & Greet</a></li>
					    <li><a href="adcdirectors.aspx" target="_blank">AD&C Contacts</a></li>
					    <li><a href="designinformation.aspx" target="_blank">Explore Design Information</a></li>
					    <li><a href="CEUCredits.aspx" target="_blank">CEU Credits</a></li>
					    <li><a href="mediacenter.aspx" target="_blank">Media Center & Gallery</a></li>
					    <li><a href="supportcenter.aspx" target="_blank">Support Center</a></li>
				    </ul>

				    <div class="toggle-wrapper" style="display:none;">
					    <div class="toggle-title">Admin Tools</div>
					    <div class="state active" data-toggle-state="on">OFF</div>
					    <div class="toggle toggle-light"></div>
				    </div>

				    <div id="close-navigation-panel"><a href="javascript:void(0);"><i class="fa fa-times"></i> CLOSE</a></div>
			    </div>
		    </div>
		</div>
		<!-- /.navigation-panel -->

		<div class="row">			
			<h1>CALENDAR</h1>
			<div class="tiva-events-calendar full" data-source="ajax" data-view="list"></div>
		</div>
		<!-- /.row -->
		
	</div>
	<!-- /.container -->

    <input type="hidden" id="txtEditId" name="txtEditId" />
    <input type="hidden" id="txtDuplicatedEventId" name="txtDuplicatedEventId" />
    <input type="hidden" id="txtUploadType" name="txtUploadType" value="CALENDAR" />  <%--Set to Gallery or Tile to tell the handler how to process the post--%>

	<!-- global js include -->
	<script src="js/scripts.js"></script>
		
	<script type="text/javascript">
		(function ($) {

		    //Server side control client ids
		    _fileUploadClientId = '<%= fileUpload.ClientID %>';
		    _txtDescriptionClientId = '<%= txtDescription.ClientID %>';
		    _txtLocationClientId = '<%= txtLocation.ClientID %>';

		    /********************************************************************************
			**	VARIABLE DEFINITIONS
			********************************************************************************/
		    var navPanel = $('#navigation-panel');
		    var navPanelState = 'closed';
		    var naPanelClosedPos = -300;
		    var naPanelOpenPos = 0;

		    // main navigation -->
		    $('#nav-trigger').on('click', function () {
		        manageNavigationPanel();
		    })

		    // close main navigation -->
		    $('#close-navigation-panel').on('click', function () {
		        manageNavigationPanel();
		    })

		    $('.toggle').toggles({
		        drag: true, // allow dragging the toggle between positions
		        click: true, // allow clicking on the toggle
		        text: {
		            on: '', // text for the ON position
		            off: '' // and off
		        },
		        on: _isAdminModeToggledOn, // is the toggle ON on init
		        animate: 250, // animation time (ms)
		        easing: 'swing', // animation transition easing function
		        checkbox: null, // the checkbox to toggle (for use in forms)
		        clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
		        width: 50, // width used if not set in css
		        height: 20, // height if not set in css
		        type: 'compact' // if this is set to 'select' then the select style toggle will be used
		    });

		    //set the target and action of the form to handle document uploads.
		    $('#form1').attr("target", "upload-iframe");
		    $('#form1').attr("action", "file-upload.ashx");            

		    function manageNavigationPanel() {
		        if (navPanelState == 'closed') {
		            navPanelState = 'open';

		            $(navPanel).animate({
		                marginLeft: naPanelOpenPos,
		            }, 500, function () {
		                // Animation complete.
		            });
		        } else {
		            navPanelState = 'closed';

		            $(navPanel).animate({
		                marginLeft: naPanelClosedPos,
		            }, 500, function () {
		                // Animation complete.
		            });
		        }
		    }

            // 
            $(window).resize(function () {
                checkWindowWidth();
            });

            function checkWindowWidth() {
                if ($(window).width() <= 992) {
                    $("label[for='conferences']").text("Hilton Conf.");
                    
                } else  {
                    $("label[for='conferences']").text("Hilton Conferences");
                }
            }

		})(jQuery);
	</script>

    <iframe id="upload-iframe" name="upload-iframe" src="" style="display:none;" onload="uploadComplete();"></iframe>
</asp:Content>
