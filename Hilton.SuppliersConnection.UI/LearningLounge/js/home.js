﻿var _galleryData = [];
var _tileData = [];
var _hasMasonryLoaded = false;
var _hasGallerySliderLoaded = false;
var _txtPartnerIdClientId;
var _lnkPartnerIconClientId;


function documentReady() {
    loadGalleryImages();
    loadTileImages();
    loadPartnerLogos();

    //set the target and action of the form to handle document uploads.
    $('#form1').attr("target", "upload-iframe");
    $('#form1').attr("action", "file-upload.ashx");

    $('.toggle').toggles({
        drag: true, // allow dragging the toggle between positions 
        click: true, // allow clicking on the toggle
        text: {
            on: '', // text for the ON position
            off: '' // and off
        },
        on: _isAdminModeToggledOn, // is the toggle ON on init
        animate: 250, // animation time (ms)
        easing: 'swing', // animation transition easing function
        checkbox: null, // the checkbox to toggle (for use in forms)
        clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
        width: 50, // width used if not set in css
        height: 20, // height if not set in css
        type: 'compact' // if this is set to 'select' then the select style toggle will be used
    });


    if (isUserAdmin() && _isAdminModeToggledOn) {
        $('#divCreateSliderLink').show();
        $('#divCreateTileLink').show();
    }

    tinymce.init({
        selector: '#textTemplate1',
        plugins: 'link',
        toolbar: [
          'undo redo | bold italic | link image | alignleft aligncenter alignright | bullist, numlist,'
        ],
        menubar: false,
        width: 400,
        height: 300
    });

    tinymce.init({
        selector: '#textTemplate2',
        plugins: 'link',
        toolbar: [
          'undo redo | bold italic | link image | alignleft aligncenter alignright | bullist, numlist,'
        ],
        menubar: false,
        width: 735,
        height: 200
    });

    $('#edit-slider-image').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    $('#edit-tile-image').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

}  //end documentReady()

function previewSelectedImage(fileUpload) {
    var imgPath = fileUpload[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    var image_holder;

    switch (fileUpload[0].id) {
        case "mainContent_fileUpload":
            image_holder = $('#slider-image-container');
            break;

        case "mainContent_fileTileImage":
            image_holder = $('#tile-image-container');
            break;

        case "mainContent_fileTemplate1Hero":
            image_holder = $('#t1-hero-uploader-wrapper .image-wrapper');
            break;

        case "mainContent_fileTemplate1Callout":
            image_holder = $('#t1-callout-uploader-wrapper .image-wrapper');
            break;

        case "mainContent_fileTemplate2Hero":
            image_holder = $('#t2-hero-uploader-wrapper .image-wrapper');
            break;

        case "mainContent_fileTemplate2Callout1":
            image_holder = $('#t2-callout-1-uploader-wrapper .image-wrapper');
            break;

        case "mainContent_fileTemplate2Callout2":
            image_holder = $('#t2-callout-2-uploader-wrapper .image-wrapper');
            break;

        case "mainContent_fileTemplate2Callout3":
            image_holder = $('#t2-callout-3-uploader-wrapper .image-wrapper');
            break;
    }

    if (extn == 'png' || extn == 'jpg') {

        if (typeof (FileReader) != 'undefined') {

            image_holder.empty();

            var reader = new FileReader();
            reader.onload = function (e) {

                //Clear the background image of the slider image preview img element on the edit form.
                image_holder.css('background-image', '')

                $('<img />', {
                    'src': e.target.result,
                    'class': 'thumb-image',
                    'width': '100%'
                }).appendTo(image_holder);
            }
            image_holder.show();
            reader.readAsDataURL(fileUpload[0].files[0]);

        } else {
            console.log('Your browser does not support FileReader.');
        }
    } else {
        console.log('Please select only .png images');
    }
}

function loadGalleryImages() {

    $('#divCarouselInner').empty();
    //if (_hasGallerySliderLoaded == false) {

    //}

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/GetGallery",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var galleryData = data.d;
            var item;

            for (var i = 0; i < galleryData.length; i++) {
                item = galleryData[i];

                var divOuter = $('#divCarouselInner');

                var link = $('<a>');
                if (item.ClickActionTypeId == 3) {
                    link.attr('href', 'CustomTemplate.aspx?refId=' + item.GalleryImageId);

                } else {
                    link.attr('href', item.ClickActionURL);
                }

                link.attr('target', item.ClickActionTarget);

                var divImage = $('<div>');
                divImage.addClass('fill');

                if (item.ImageType == "BINARY") {
                    divImage.attr('style', "background-image:url('data:image/jpg;base64," + item.Image + "');text-align:center;");
                } else {
                    divImage.attr('style', "background-image:url('" + item.ImageURL + "');text-align:center;");
                }


                if (isUserAdmin() && _isAdminModeToggledOn) {
                    if (i > 0) {
                        divImage.append('<a href="javascript:moveItem(' + item.GalleryImageId + ', \'LEFT\', \'GALLERY\');" target="_self">< Left</a>');
                    }
                    divImage.append('<a href="javascript:showEditSlider(' + item.GalleryImageId + ');" target="_self" style="margin-left:20px">Edit</a>');
                }

                if (isUserAdmin() && _isAdminModeToggledOn && item.AllowDelete) {
                    divImage.append('<a href="javascript:deleteSlider(' + item.GalleryImageId + ');" target="_self" style="margin-left:20px">Delete</a>');
                }

                if (isUserAdmin() && _isAdminModeToggledOn && i < galleryData.length - 1) {
                    divImage.append('<a href="javascript:moveItem(' + item.GalleryImageId + ', \'RIGHT\', \'GALLERY\');" target="_self" style="margin-left:20px;">Right ></a>');
                }

                divImage.attr('id', 'sliderImage' + item.GalleryImageId);

                if (item.Enabled == false) {
                    if (item.ImageType == "BINARY") {
                        divImage.attr('style', "background-image:url('data:image/jpg;base64," + item.Image + "');text-align:center;opacity:.3;");
                    } else {
                        divImage.attr('style', "background-image:url('" + item.ImageURL + "');text-align:center;opacity:.3;");
                    }

                }

                var divInner = $('<div>');

                if (i == 0)
                    $(divInner).addClass("item active");
                else
                    $(divInner).addClass("item");

                link.append(divImage);
                divInner.append(link);
                divOuter.append(divInner);

                _galleryData.push({
                    GalleryImageId: item.GalleryImageId, ClickActionURL: item.ClickActionURL, ClickActionTarget: item.ClickActionTarget,
                    Enabled: item.Enabled, ClickActionTypeId: item.ClickActionTypeId, TemplateId: item.TemplateId, AllowDelete: item.AllowDelete
                });
            }

            if (_hasGallerySliderLoaded) {
                $("#carousel-home.carousel").carousel("pause").removeData();
                $("#carousel-home.carousel").carousel(0);

            } else {
                // initialize carousels -->
                $('#carousel-home.carousel').carousel({
                    interval: 4000
                });

                _hasGallerySliderLoaded = true;
            }

        }
    });
}

function loadTileImages() {
    clearTiles();

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/GetTiles",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var tileData = data.d;
            var tileContainer = $('#divTileContainter');
            var item;

            for (var i = 0; i < tileData.length; i++) {
                item = tileData[i];

                var link = $('<a>');
                if (item.ClickActionTypeId == 3) {
                    link.attr('href', 'CustomTemplate.aspx?refId=' + item.TileImageId);

                } else {
                    link.attr('href', item.ClickActionURL);
                }

                link.attr('target', item.ClickActionTarget);

                var img = $('<img id="tileImage' + item.TileImageId + '">');
                img.attr('src', "data:image/jpg;base64," + item.Image);
                img.attr('alt', 'tile ' + i);

                if (item.HasRollover == true) {
                    img.addClass('rollover');
                }

                link.append(img);

                var div = $('<div>');

                div.addClass('grid-item');

                if (item.ClickActionURL.indexOf("designinformation") > -1) {
                    div.addClass('grid-item--width2');
                }

                if (item.Enabled == false) {
                    div.addClass('disabled');
                }

                div.append(link);

                var adminString = '<div style="margin-top:-40px; margin-left:10px;">';

                if (isUserAdmin() && _isAdminModeToggledOn) {
                    if (i > 0) {
                        adminString += '<a href="javascript:moveItem(' + item.TileImageId + ', \'LEFT\', \'TILE\');" target="_self" style="background-color:white">< Left</a>';
                    };
                    adminString += '<a href="javascript:showEditTile(' + item.TileImageId + ');" target="_self" style="margin-left:20px;background-color:white">Edit</a>';
                }

                if (isUserAdmin() && _isAdminModeToggledOn && item.AllowDelete) {
                    adminString += '<a href="javascript:deleteTile(' + item.TileImageId + ');" target="_self" style="margin-left:20px;background-color:white">Delete</a>'; ;
                }

                if (isUserAdmin() && _isAdminModeToggledOn) {
                    if (i < tileData.length - 1) {
                        adminString += '<a href="javascript:moveItem(' + item.TileImageId + ', \'RIGHT\', \'TILE\');" target="_self" style="margin-left:20px;background-color:white">Right ></a>';
                    }
                    adminString += '</div>';
                    div.append(adminString);
                }

                if (!_hasMasonryLoaded) {
                    tileContainer.append(div);
                } else {
                    $('.grid').append(div).masonry('appended', div);
                }
                _tileData.push({
                    TileImageId: item.TileImageId, ClickActionURL: item.ClickActionURL, ClickActionTarget: item.ClickActionTarget,
                    Enabled: item.Enabled, ClickActionTypeId: item.ClickActionTypeId, TemplateId: item.TemplateId, AllowDelete: item.AllowDelete, RestrictToUserTypeId: item.RestrictToUserTypeId
                });

            }

            if (!_hasMasonryLoaded) {
                //Initialize Mansonry grid.  
                //Doing this action using setTimeout to allow images and divs to be set on the page before running masonry code.  This fixes the issue
                //of the homepage tiles not loading correctly on iOS devices.
                setTimeout(function(){
                    $('.grid').masonry({
                        itemSelector: '.grid-item',
                        columnWidth: '.grid-sizer',
                        percentPosition: true
                    });

                    _hasMasonryLoaded = true;
                }, 1000);

            } else {
                $('.grid').masonry('layout');
            }

        }
    });
}

function loadPartnerLogos() {

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/GetPartnerImages",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var logoData = data.d;
            var logoContainer = $('#recommendedPartnerDiv');
            var item;

            for (var i = 0; i < logoData.length; i += 3) {
                item = logoData[i];

                var divThree = $('<div>');
                divThree.addClass('item');
                if (i == 0) {
                    divThree.addClass('active');
                }

                if (_userTypeId > 0) {
                    divThree.append('<div><a href="javascript:partnerLogoClicked(' + logoData[i].PartnerId + ');"><img style="height:72px;width:106px" src=data:image/jpg;base64,' + logoData[i].CompanyLogoImage + '></a></div>');

                    if (logoData.length - i >= 2) {
                        divThree.append('<div><a href="javascript:partnerLogoClicked(' + logoData[i + 1].PartnerId + ');"><img style="height:72px;width:106px" src=data:image/jpg;base64,' + logoData[i + 1].CompanyLogoImage + '></a></div>');
                    }

                    if (logoData.length - i >= 3) {
                        divThree.append('<div><a href="javascript:partnerLogoClicked(' + logoData[i + 2].PartnerId + ');"><img style="height:72px;width:106px" src=data:image/jpg;base64,' + logoData[i + 2].CompanyLogoImage + '></a></div>');
                    }

                } else {
                    divThree.append('<div><img style="height:72px;width:106px" src=data:image/jpg;base64,' + logoData[i].CompanyLogoImage + '></div>');

                    if (logoData.length - i >= 2) {
                        divThree.append('<div><img style="height:72px;width:106px" src=data:image/jpg;base64,' + logoData[i + 1].CompanyLogoImage + '></div>');
                    }

                    if (logoData.length - i >= 3) {
                        divThree.append('<div><img style="height:72px;width:106px" src=data:image/jpg;base64,' + logoData[i + 2].CompanyLogoImage + '></div>');
                    }
                }

                logoContainer.append(divThree);
            }

            $('#carousel-partners.carousel').carousel({
                interval: 3000
            });


        }
    });
}

function partnerLogoClicked(partnerId) {
    setFormTarget();
    $('#' + _txtPartnerIdClientId).val(partnerId);
    $('#' + _lnkPartnerIconClientId)[0].click();
}


function showEditSlider(id) {

    //Get the background image of the slider whose edit button was clicked.
    var bg = $('#sliderImage' + id).css('background-image');

    //Set the background image of the slider image preview img element on the edit form.
    $('#slider-image-container').css('background-image', bg);

    //Assigning a valid number to the txtGalleryEditId form control 
    $('#txtEditId').val(id);

    //Loop through the array that is storing the gallery data and find
    //the instance that matches id.
    for (var j = 0; j < _galleryData.length; j++) {

        if (_galleryData[j].GalleryImageId == id) {

            //Set the controls on the edit form equal to the appropriate values.            
            $('#txtClickActionURL').val(_galleryData[j].ClickActionURL);
            $('#selectClickType').val(_galleryData[j].ClickActionTypeId);
            $('#optNewWindow').prop('checked', _galleryData[j].ClickActionTarget == "_blank");
            $('#optSameWindow').prop('checked', _galleryData[j].ClickActionTarget == "_self");
            $('#chkEnabled').prop('checked', _galleryData[j].Enabled == true);

            if (_galleryData[j].ClickActionTypeId == 3) {
                $('#divSliderTemplateContainer').show();
                $('#divSliderURLContainer').hide();
                $('#cboSliderTemplate').val(_galleryData[j].TemplateId);
                $('#sliderTemplateThumb').prop('src', 'images/home/template' + _galleryData[j].TemplateId + '-thumbnail.jpg');

            } else {
                $('#divSliderTemplateContainer').hide();
                $('#divSliderURLContainer').show();
            }
            break;
        }
    }

    //Set the upload type form control so that the file-upload handler knows how to process the post data.
    $('#txtUploadType').val("GALLERY");

    $('#edit-slider-image').modal('show');

}

function showCreateSlider() {

    //Clear the background image of the slider image preview img element on the edit form.
    $('#slider-image-container').css('background-image', '')
    $('#slider-image-container').empty();

    //Set the txtGalleryEditId form control to -1 indicating that it is an insert.
    $('#txtEditId').val(-1);

    $('#txtClickActionURL').val('');
    $('#optNewWindow').prop('checked', true);
    $('#chkEnabled').prop('checked', true);

    //Set the upload type form control so that the file-upload handler knows how to process the post data.
    $('#txtUploadType').val("GALLERY");

    $('#cboSliderTemplate').prop('disabled', false);

    $('#edit-slider-image').modal('show');

}

function deleteSlider(id) {
    if (confirm("Are you sure you want to delete this item?") == true) {
        $.ajax({
            type: "POST",
            url: "LearningLoungeService.asmx/DeleteGalleryImage",
            data: "{galleryImageId: " + id + "}",
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var returnValue = data.d;

                loadGalleryImages();
            }
        });
    }
}

function showEditTile(id) {

    //Get the background image of the slider whose edit button was clicked.
    var src = $('#tileImage' + id).prop('src');

    //Set the background image of the slider image preview img element on the edit form.
    $('#tile-image-container').empty();
    $('#tile-image-container').css('background-image', 'url(' + src + ')');

    //Assigning a valid number to the txtEditId form control 
    $('#txtEditId').val(id);

    //Loop through the array that is storing the tile data and find
    //the instance that matches id.
    for (var j = 0; j < _tileData.length; j++) {

        if (_tileData[j].TileImageId == id) {

            //Set the controls on the edit form equal to the appropriate values.            
            $('#txtTileClickActionURL').val(_tileData[j].ClickActionURL);
            $('#cboTileClickActionType').val(_tileData[j].ClickActionTypeId);
            $('#optTileNewWindow').prop('checked', _tileData[j].ClickActionTarget == "_blank");
            $('#optTileSameWindow').prop('checked', _tileData[j].ClickActionTarget == "_self");
            $('#chkTileEnabled').prop('checked', _tileData[j].Enabled == true);

            switch (_tileData[j].RestrictToUserTypeId) {
                case -1:
                    $('#chkEveryone').prop('checked', _tileData[j].ClickActionTarget == "_blank");
                    break;
                case 2:
                    $('#chkPartners').prop('checked', _tileData[j].ClickActionTarget == "_blank");
                    break;
                case 3:
                    $('#chkOwners').prop('checked', _tileData[j].ClickActionTarget == "_blank");
                    break;
                case 4:
                    $('#chkConsultants').prop('checked', _tileData[j].ClickActionTarget == "_blank");
                    break;
            }

            if (_tileData[j].ClickActionTypeId == 3) {
                $('#divTileTemplateContainer').show();
                $('#divTileURLContainer').hide();
                $('#cboTileTemplate').val(_tileData[j].TemplateId);
                $('#tileTemplateThumb').prop('src', 'images/home/template' + _tileData[j].TemplateId + '-thumbnail.jpg');

            } else {
                $('#divTileTemplateContainer').hide();
                $('#divTileURLContainer').show();
            }

            break;
        }
    }

    //Set the upload type form control so that the file-upload handler knows how to process the post data.
    $('#txtUploadType').val("TILE");

    $('#edit-tile-image').modal('show');

}

function showCreateTile() {

    //Clear the background image of the slider image preview img element on the edit form.
    $('#tile-image-container').css('background-image', '');
    $('#tile-image-container').empty();

    //Set the txtGalleryEditId form control to -1 indicating that it is an insert.
    $('#txtEditId').val(-1);

    $('#txtTileClickActionURL').val('');
    $('#optTileNewWindow').prop('checked', true);
    $('#chkTileEnabled').prop('checked', true);

    //Set the upload type form control so that the file-upload handler knows how to process the post data.
    $('#txtUploadType').val("TILE");

    $('#edit-tile-image').modal('show');

}

function deleteTile(id) {

    if (confirm("Are you sure you want to delete this tile?") == true) {
        $.ajax({
            type: "POST",
            url: "LearningLoungeService.asmx/DeleteTileImage",
            data: "{tileImageId: " + id + "}",
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var returnValue = data.d;

                loadTileImages();
            }
        });
    }
}

function btnSubmitTile_Click() {
    $('#edit-tile-image').modal('hide');
}

function btnSubmit_Click() {
    $('#edit-slider-image').modal('hide');
}

function clearTiles() {

    $('#divTileContainter').children('div').each(function () {
        if (this.id != "divRecommendedPartners" && this.id != "divGridSizer") {
            //this.remove();
            $('.grid').masonry('remove', this);
        }
    });
}

function adminToolsToggled() {
    loadGalleryImages();
    loadTileImages();

    if (_isAdminModeToggledOn) {
        $('#divCreateSliderLink').show();
        $('#divCreateTileLink').show();
    } else {
        $('#divCreateSliderLink').hide();
        $('#divCreateTileLink').hide();
    }

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/ToggleAdminTools",
        data: "{isOn: " + _isAdminModeToggledOn + "}",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

        }
    });
}


function cboTileClickActionType_change(sender) {

    if (sender.value == "3") {
        $('#divTileTemplateContainer').show();
        $('#divTileURLContainer').hide();
    } else {
        $('#divTileTemplateContainer').hide();
        $('#divTileURLContainer').show();
    }
}

function cboSliderClickActionType_change(sender) {

    if (sender.value == "3") {
        $('#divSliderTemplateContainer').show();
        $('#divSliderURLContainer').hide();
    } else {
        $('#divSliderTemplateContainer').hide();
        $('#divSliderURLContainer').show();
    }
}

function showTemplateForm() {
    var editId = $('#txtEditId').val();

    $('#t2-tab').removeClass('active');

    if (editId && editId != "-1") {
        $.ajax({
            type: "POST",
            url: "LearningLoungeService.asmx/GetTemplateContent",
            data: "{refId: " + editId + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var contentData = data.d;
                var item;
                var image_holder;

                for (var i = 0; i < contentData.length; i++) {

                    if (contentData[i].TemplateId == 1) {
                        if (contentData[i].ContentName == "hero-image") {
                            setImagePreview('#t1-hero-uploader-wrapper .image-wrapper', contentData[i].ContentImage);

                        } else if (contentData[i].ContentName == "callout-image") {
                            setImagePreview('#t1-callout-uploader-wrapper .image-wrapper', contentData[i].ContentImage);

                        } else if (contentData[i].ContentName == "text1") {
                            tinyMCE.get('textTemplate1').setContent(contentData[i].ContentText);
                        }
                    } else if (contentData[i].TemplateId == 2) {
                        if (contentData[i].ContentName == "hero-image-template2") {
                            setImagePreview('#t2-hero-uploader-wrapper .image-wrapper', contentData[i].ContentImage);

                        } else if (contentData[i].ContentName == "callout-image1-template2") {
                            setImagePreview('#t2-callout-1-uploader-wrapper .image-wrapper', contentData[i].ContentImage);

                        } else if (contentData[i].ContentName == "callout-image2-template2") {
                            setImagePreview('#t2-callout-2-uploader-wrapper .image-wrapper', contentData[i].ContentImage);

                        } else if (contentData[i].ContentName == "callout-image3-template2") {
                            setImagePreview('#t2-callout-3-uploader-wrapper .image-wrapper', contentData[i].ContentImage);

                        } else if (contentData[i].ContentName == "text1-template2") {
                            tinyMCE.get('textTemplate2').setContent(contentData[i].ContentText);
                        }
                    }
                }

            }
        });
    } else {
        //If not editing an existing tile or slider image, clear the template controls.
        tinyMCE.get('textTemplate1').setContent('');
        tinyMCE.get('textTemplate2').setContent('');

        $('#t1-hero-uploader-wrapper .image-wrapper').empty();
        $('#t1-callout-uploader-wrapper .image-wrapper').empty();

        $('#t2-hero-uploader-wrapper .image-wrapper').empty();
        $('#t2-callout-1-uploader-wrapper .image-wrapper').empty();
        $('#t2-callout-2-uploader-wrapper .image-wrapper').empty();
        $('#t2-callout-3-uploader-wrapper .image-wrapper').empty();
    }

    var templatePickerDropdown;
    if ($('#txtUploadType').val() == "TILE") {
        templatePickerDropdown = $('#cboTileTemplate');

    } else if ($('#txtUploadType').val() == "GALLERY") {
        templatePickerDropdown = $('#cboSliderTemplate');
    }

    if (templatePickerDropdown.val() == "1") {
        if (!$('#t1-tab').hasClass('active')) {
            $('#t1-tab').addClass('active');
        }

    } else {
        if (!$('#t2-tab').hasClass('active')) {
            $('#t2-tab').addClass('active');
        }

        $('#t1-tab').removeClass('active');
    }

    $('#page-template-modal').modal('show');
}

function setImagePreview(imageHolderName, image) {
    var image_holder = $(imageHolderName);
    image_holder.empty();
    image_holder.css('background-image', '');

    $('<img />', {
        'src': 'data:image/jpg;base64,' + image,
        'class': 'thumb-image',
        'width': '100%'
    }).appendTo(image_holder);

    image_holder.show();
}

function moveItem(id, direction, type) {
    var increment;

    if (direction == "LEFT") {
        increment = -1;
    } else {
        increment = 1;
    }

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/UpdateDisplayOrder",
        data: "{id: " + id + ", itemType: '" + type + "', increment: " + increment + "}",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var returnValue = data.d;

            if (type == 'TILE') {
                loadTileImages();
            } else {
                loadGalleryImages();
            }
        }
    });

}

function cboSliderTemplate_change() {

    $('#sliderTemplateThumb').prop('src', 'images/home/template' + $('#cboSliderTemplate').val() + '-thumbnail.jpg');
}

function cboTileTemplate_change() {

    $('#tileTemplateThumb').prop('src', 'images/home/template' + $('#cboTileTemplate').val() + '-thumbnail.jpg');
}

function uploadComplete() {

    if ($('#upload-iframe').contents().find("body")[0].innerText.length > 0) {
        alert($('#upload-iframe').contents().find("body")[0].innerText);

        location.reload();

        // Original code to refresh just the slider or tiles.  Couldn't fix a bug with Chrome where navigating to a different page
        // in the same window after creating a tile or slider image, then hitting the back button, the form would submit again and a 
        // 2nd image would be created.  The fix was to simply refresh the entire page.
        //if ($('#txtUploadType').val() == "GALLERY") {
        //    loadGalleryImages();

        //} else {
        //    loadTileImages();            
        //}

    }
}
