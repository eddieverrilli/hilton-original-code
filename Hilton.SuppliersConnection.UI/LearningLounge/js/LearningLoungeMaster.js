﻿function saveMeeting() {

    var brandStandardCompliance;
    var aiaAccredited;
    var leedAccredited;
    var email;
    var meeting = {};
    var isMeetNGreet = $('#meet-and-greet-tab').hasClass('active');

    if (isMeetNGreet) {
        meeting.MeetingType = "MEETANDGREET";
        meeting.Company = $('#txtMeetNGreetCompany').val();
        meeting.Contact = $('#txtMeetNGreetContact').val();
        meeting.Phone = $('#txtMeetNGreetPhone').val();
        meeting.Products = $('#txtMeetNGreetProducts').val();
        brandStandardCompliance = $('input[name=meet-and-greet-brand-standards]:checked').val();
        meeting.AIAAccredited = false;
        meeting.AIANumCEUs = 0;
        meeting.LEEDAccredited = false;
        meeting.LEEDNumCEUs = 0;
        meeting.SubmittedByEmail = $('#txtMeetNGreetEmail').val();

    } else {
        meeting.MeetingType = "LUNCHANDLEARN";
        meeting.Company = $('#txtLunchNLearnCompany').val();
        meeting.Contact = $('#txtLunchNLearnContact').val();
        meeting.Phone = $('#txtLunchNLearnPhone').val();
        meeting.Products = $('#txtLunchNLearnProducts').val();
        brandStandardCompliance = $('input[name=lunch-and-learn-brand-standards]:checked').val();

        aiaAccredited = $('input[name=lunch-and-learn-aia]:checked').val();
        if ($('#txtAIACEUsEarned').val().length == 0) {
            meeting.AIANumCEUs = -1;
        } else {
            meeting.AIANumCEUs = $('#txtAIACEUsEarned').val();
        }

        leedAccredited = $('input[name=lunch-and-learn-leed]:checked').val();
        if ($('#txtLEEDCEUsEarned').val().length == 0) {
            meeting.LEEDNumCEUs = -1;
        } else {
            meeting.LEEDNumCEUs = $('#txtLEEDCEUsEarned').val();
        }

        meeting.SubmittedByEmail = $('#txtLunchNLearnEmail').val();

        meeting.AIAAccredited = (aiaAccredited == "yes");
        meeting.LEEDAccredited = (leedAccredited == "yes");
    }

    meeting.BrandStandardCompliance = (brandStandardCompliance == "yes");


    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/AddMeeting",
        data: "{meeting: " + JSON.stringify(meeting) + "}",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var returnValue = data.d;
            console.log(returnValue);

            $('#meet-and-greet-modal').modal('hide');
            alert("The meeting has been successfully submitted.");

        }
    });

}

function validateMeetAndGreetForm() {

    if ($('#txtMeetNGreetCompany').val().length == 0) {
        alert('Please enter your company name.');
        return false;
    }

    if ($('#txtMeetNGreetContact').val().length == 0) {
        alert('Please enter the contact name.');
        return false;
    }

    if ($('#txtMeetNGreetPhone').val().length == 0) {
        alert('Please enter the contact phone number.');
        return false;
    }

    if ($('#txtMeetNGreetProducts').val().length == 0) {
        alert('Please enter the products to be presented.');
        return false;
    }

    if ($('#txtMeetNGreetEmail').val().length == 0) {
        alert('Please enter an email address.');
        return false;
    }

    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if ($('#txtMeetNGreetEmail').val().search(emailRegEx) == -1) {
        alert('Please enter a valid email address.');
        return false;
    }


    return true;
}

function validateLunchAndLearnForm() {

    if ($('#txtLunchNLearnCompany').val().length == 0) {
        alert('Please enter your company name.');
        return false;
    }

    if ($('#txtLunchNLearnContact').val().length == 0) {
        alert('Please enter the contact name.');
        return false;
    }

    if ($('#txtLunchNLearnPhone').val().length == 0) {
        alert('Please enter the contact phone number.');
        return false;
    }

    if ($('#txtLunchNLearnProducts').val().length == 0) {
        alert('Please enter the products to be presented.');
        return false;
    }


    if ($('#chkLunchAndLearnAIAYes').is(':checked') == true) {
        if ($('#txtAIACEUsEarned').val().length == 0) {
            alert('Please enter the number of AIA credits earned.');
            return false;
        }
    }

    if ($('#chkLunchAndLearnLEEDYes').is(':checked') == true) {
        if ($('#txtLEEDCEUsEarned').val().length == 0) {
            alert('Please enter the number of LEED credits earned.');
            return false;
        }
    }


    if ($('#txtLunchNLearnEmail').val().length == 0) {
        alert('Please enter an email address.');
        return false;
    }

    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if ($('#txtLunchNLearnEmail').val().search(emailRegEx) == -1) {
        alert('Please enter a valid email address.');
        return false;
    }


    return true;
}

function btnLunchNLearnSubmit_Click() {

    if (validateLunchAndLearnForm()) {
        saveMeeting();
        $('#meet-and-greet-modal').modal('hide');
    }

}

function btnMeetNGreetSubmit_Click() {
    if (validateMeetAndGreetForm()) {
        saveMeeting();
        $('#meet-and-greet-modal').modal('hide');
    }
}

function meetNGreetTileClicked() {

    if (_userTypeId == 2 || _userTypeId == 4 || _userTypeId == 5) {

        //Clear the meet and greet form
        $('#txtMeetNGreetCompany').val('');
        $('#txtMeetNGreetContact').val('');
        $('#txtMeetNGreetPhone').val('');
        $('#txtMeetNGreetProducts').val('');
        $('#txtMeetNGreetEmail').val('');

        //clear the lunch and learn form
        $('#txtLunchNLearnCompany').val('');
        $('#txtLunchNLearnContact').val('');
        $('#txtLunchNLearnPhone').val('');
        $('#txtLunchNLearnProducts').val('');
        $('#txtAIACEUsEarned').val('');
        $('#txtLEEDCEUsEarned').val('');
        $('#txtLunchNLearnEmail').val('');

        $('#meet-and-greet-modal').modal('show');
    } else {
        $('#partner-login-modal').modal('show');
    }
}

function populateIntroCopy() {
    var meetGreetIntroTab = $('#meet-and-greet-modal .nav-tabs .meet-greet .intro');
    var meetGreetIntroTabPane = $('#meet-and-greet-modal .tab-content .tab-pane#meet-and-greet-tab .intro');
    var meetGreetIntroText = 'Meet & Greets are informal "show & tell" meetings that offer the AD&C teams an opportunity to view your collateral and/or products & services while and grabbing a quick breakfast.';

    var lunchLearnIntroTab = $('#meet-and-greet-modal .nav-tabs .lunch-learn .intro');
    var lunchLearnIntroTabPane = $('#meet-and-greet-modal .tab-content .tab-pane#lunch-and-learn-tab .intro');
    var lunchLearnIntroText = 'Lunch & Learns are CE accredited luncheons provided by our Suppliers\' Connection Recommended Partners for our AD&C teams.';

    $(meetGreetIntroTab).html(meetGreetIntroText);
    $(meetGreetIntroTabPane).html(meetGreetIntroText);

    $(lunchLearnIntroTab).html(lunchLearnIntroText);
    $(lunchLearnIntroTabPane).html(lunchLearnIntroText);
}