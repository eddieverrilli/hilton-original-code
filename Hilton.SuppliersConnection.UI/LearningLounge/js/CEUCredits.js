﻿var _ceuData = [];
var _hasMasonryLoaded = false;

function showRequestform() {
    $('#request-modal').modal('show');
}

function loadCEUs() {

    clearCEUCredits();

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/GetCEUCredits",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var ceuData = data.d;
            var item;
            var divCEU = $('#divCEUContainer');
            var divClass = "";
            var divFooter = "";
            var adminLinks;
            var image;
            var defaultImageLink;

            for (var i = 0; i < ceuData.length; i++) {
                item = ceuData[i];
                //_faqItems.push(item);

                if (item.AIAAccredited && !item.LEEDAccredited) {
                    divClass = "aia";
                    divFooter = "AIA";
                    defaultImageLink = "images/ceu/aia.jpg";

                } else if (!item.AIAAccredited && item.LEEDAccredited) {
                    divClass = "leed";
                    divFooter = "LEED";
                    defaultImageLink = "images/ceu/leed.jpg";

                } else if (item.AIAAccredited && item.LEEDAccredited) {
                    divClass = "aia-leed";
                    divFooter = "AIA | LEED";
                    defaultImageLink = "images/ceu/aia-leed.jpg";
                }

                //if the user is an admin and admin tools are enabled, show the admin options.
                adminLinks = "";
                if (isUserAdmin() && _isAdminModeToggledOn) {

                    if (i > 0) {
                        adminLinks = '<a href="javascript:moveItem(' + item.CEUCreditId + ', \'LEFT\', \'CEU\');" target="_self" style="background-color:#fff">< Left</a>';
                    }

                    if (!item.IsApproved) {
                        adminLinks += '<a href="javascript:approveCEU(' + item.CEUCreditId + ')" style="margin-left:10px;background-color:#fff">Approve</a>';
                    }
                    
                    adminLinks += '<a href="javascript:showDelete(' + item.CEUCreditId + ')" style="margin-left:10px;background-color:#fff">Delete</a>';

                    if (i < ceuData.length - 1) {
                        adminLinks += '<a href="javascript:moveItem(' + item.CEUCreditId + ', \'RIGHT\', \'CEU\');" target="_self" style="margin-left:10px;background-color:#fff">Right ></a>';
                    }
                }

                if (item.Image) {
                    image = 'data:image/jpg;base64,' + item.Image;
                } else {
                    image = defaultImageLink;
                }

                var divString = '<div class="grid-item ' + divClass + '">' +
                        '<div class="ceu-course-image">' +
                        '    <img class="main" src="' + image + '" />' +
                        '</div>' +
                        '<div class="ceu-course-detail" onclick="showDetail(' + item.CEUCreditId + ');">' +
                        '    <div class="course-title">' +
                                item.CourseTitle +
                        '    </div>' +
                        '    <div class="course-divider">' +
                        '        <img src="images/ceu/divider.png" />' +
                        '    </div>' +
                        '    <div class="course-company">' +
                                item.Company +
                        '    </div>' +
                        '</div>' +
                        '<div class="course-footer">' +
                                divFooter +
                        '</div>' +
                        adminLinks +
                    '</div>';

                if (!_hasMasonryLoaded) {
                    jQuery('.grid').append(divString);
                } else {
                    var $content = $(divString);
                    $('.grid').append($content).masonry('appended', $content);
                }

                _ceuData = ceuData;
            }

            if (!_hasMasonryLoaded) {
                // init Masonry
                var $grid = $('.grid').masonry({
                    itemSelector: '.grid-item',
                    percentPosition: true,
                    columnWidth: '.grid-sizer',
                    gutter: 10
                });

                $grid.imagesLoaded().progress(function () {
                    $grid.masonry();
                });

                $(window).resize(function () {
                    $grid.masonry();
                });

                _hasMasonryLoaded = true;
            } else {
                $('.grid').masonry('layout');
            }

        }
    });

}

function clearCEUCredits() {

    $('#divCEUContainer').children('div').each(function () {
        if (this.id != "divGridSizer") {
            $('.grid').masonry('remove', this);
        }
    });
}

//function submitRequest() {
//    var title = $('#txtTitle').val();
//    var company = $('#txtCompany').val();
//    var contact = $('#txtContact').val();
//    var phone = $('#txtPhone').val();
//    var topicLink = $('#txtTopicLink').val();
//    var expireDate = $('#dtpExpireDate').val();
//    var brandStandardCompliance = $('input[name=brand-standards]:checked').val();
//    var aiaAccredited = $('input[name=aia-accredited]:checked').val();
//    var aiaCEUsEarned = $('#txtAIACEUsEarned').val();
//    var leedAccredited = $('input[name=leed-accredited]:checked').val();
//    var leedCEUsEarned = $('#txtLEEDCEUsEarned').val();
//    var email = $('#txtEmail').val();

//    var ceu = {};
//    ceu.CourseTitle = title;
//    ceu.Company = company;
//    ceu.Contact = contact;
//    ceu.Phone = phone;
//    ceu.TopicLink = topicLink;
//    ceu.ExpireDate = new Date(2016, 10, 1, 23, 59);
//    ceu.BrandStandardCompliance = (brandStandardCompliance == "yes");
//    ceu.AIAAccredited = (aiaAccredited == "yes");

//    if (aiaAccredited == "yes") {
//        ceu.AIANumCEUs = aiaCEUsEarned;
//    } else {
//        ceu.AIANumCEUs = 0;
//    }

//    ceu.LEEDAccredited = (leedAccredited == "yes");

//    if (leedAccredited == "yes") {
//        ceu.LEEDNumCEUs = leedCEUsEarned;
//    } else {
//        ceu.LEEDNumCEUs = 0;
//    }

//    ceu.SubmittedByEmail = email;

//    $.ajax({
//        type: "POST",
//        url: "LearningLoungeService.asmx/AddCEU",
//        data: "{ceu: " + JSON.stringify(ceu) + "}",
//        cache: false,
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            var returnValue = data.d;
//            console.log(returnValue);

//            $('#request-modal').modal('hide');
//            alert("The CEU has been successfully submitted and is awaiting approval by an administrator.");

//        }
//    });


//}

function showDelete(ceuId) {
    if (confirm("Are you sure you want to delete this CEU Credit?") == true) {
        $.ajax({
            type: "POST",
            url: "LearningLoungeService.asmx/DeleteCEU",
            data: "{ceuCreditId: " + ceuId + "}",
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var returnValue = data.d;
                console.log(returnValue);

                loadCEUs();
            }
        });
    }
}

function approveCEU(ceuId) {
    if (confirm("Are you sure you want to approve this CEU Credit?") == true) {
        $.ajax({
            type: "POST",
            url: "LearningLoungeService.asmx/ApproveCEU",
            data: "{ceuCreditId: " + ceuId + "}",
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var returnValue = data.d;
                console.log(returnValue);

                loadCEUs();
            }
        });
    }
}

function adminToolsToggled() {
    loadCEUs();

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/ToggleAdminTools",
        data: "{isOn: " + _isAdminModeToggledOn + "}",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

        }
    });
}

function showDetail(ceuCreditId) {

    for (var j = 0; j < _ceuData.length; j++) {

        if (_ceuData[j].CEUCreditId == ceuCreditId) {

            //Set the controls on the edit form equal to the appropriate values.            
            $('#TitleDetail').html(_ceuData[j].CourseTitle);
            $('#ContactDetail').html(_ceuData[j].Contact);
            $('#EmailDetail').html(_ceuData[j].SubmittedByEmail);
            $('#PhoneDetail').html(_ceuData[j].Phone);
            $('#TopicLinkDetail').html('<a href="' + _ceuData[j].TopicLink + '">' + _ceuData[j].TopicLink + "</a>");

            var expireDate = new Date(parseInt(_ceuData[j].ExpireDate.substr(6)));
            var createdDate = new Date(parseInt(_ceuData[j].DateCreated.substr(6)));

            $('#ExpireDateDetail').html('Expires on ' + expireDate.toDateString());
            $('#CreatedDateDetail').html('Submitted on ' + createdDate.toDateString());
            $('#BrandStandardDetail').html(_ceuData[j].CourseTitle);
            
            //$('#TitleDetail').html(_ceuData[j].CourseTitle);
            if (_ceuData[j].Image == null) {

                if (_ceuData[j].AIAAccredited && !_ceuData[j].LEEDAccredited) {
                    $('#imageDetail').prop('src', 'images/ceu/aia.jpg');

                } else if (!_ceuData[j].AIAAccredited && _ceuData[j].LEEDAccredited) {
                    $('#imageDetail').prop('src', 'images/ceu/leed.jpg');

                } else if (_ceuData[j].AIAAccredited && _ceuData[j].LEEDAccredited) {
                    $('#imageDetail').prop('src', 'images/ceu/aia-leed.jpg');
                }

            } else {
                $('#imageDetail').prop('src', 'data:image/jpg;base64,' + _ceuData[j].Image);
            }

            break;
        }
    }

    $('#ceu-request').hide();
    $('#ceuDetail').show();
    $('#divCEUContainer').hide();
}

function moveItem(id, direction, type) {
    var increment;

    if (direction == "LEFT") {
        increment = -1;
    } else {
        increment = 1;
    }

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/UpdateDisplayOrder",
        data: "{id: " + id + ", itemType: '" + type + "', increment: " + increment + "}",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var returnValue = data.d;

            loadCEUs();
        }
    });

}

function validateCEUCreditForm() {

    if ($('#txtTitle').val().length == 0) {
        alert('Please enter the title.');
        return false;
    }

    if ($('#txtCompany').val().length == 0) {
        alert('Please enter the company name.');
        return false;
    }

    if ($('#txtContact').val().length == 0) {
        alert('Please enter the contact name.');
        return false;
    }

    if ($('#txtPhone').val().length == 0) {
        alert('Please enter the contact phone number.');
        return false;
    }

    if ($('#txtTopicLink').val().length == 0) {
        alert('Please enter the topic link.');
        return false;
    }

    if ($('#dtpExpireDate').val().length == 0) {
        alert('Please enter the expiration date.');
        return false;
    }

    if ($('#optAIA').is(':checked') == true) {
        if ($('#txtAIACEUsEarned_CEU').val().length == 0) {
            alert('Please enter the number of AIA credits earned.');
            return false;
        }
    }

    if ($('#optLEED').is(':checked') == true) {
        if ($('#txtLEEDCEUsEarned_CEU').val().length == 0) {
            alert('Please enter the number of LEED credits earned.');
            return false;
        }
    }


    if ($('#txtEmail').val().length == 0) {
        alert('Please enter an email address.');
        return false;
    }

    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if ($('#txtEmail').val().search(emailRegEx) == -1) {
        alert('Please enter a valid email address.');
        return false;
    }


    return true;
}

function btnSubmit_Click() {

    if (validateCEUCreditForm() == true) {
        return true;

    } else {
        return false;
    }

}

function uploadComplete() {
    if ($('#upload-iframe').contents().find("body")[0].innerText.length > 0) {
        alert($('#upload-iframe').contents().find("body")[0].innerText);
        location.reload();
    }
}