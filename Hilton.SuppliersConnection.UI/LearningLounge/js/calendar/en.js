// Day, Month
var wordMonth = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
var wordDay_sun = "Sunday";
var wordDay_mon = "Monday";
var wordDay_tue = "Tuesday";
var wordDay_wed = "Wednesday";
var wordDay_thu = "Thursday";
var wordDay_fri = "Friday";
var wordDay_sat = "Saturday";

// View Button
var calendar_view = "Month View"
var list_view = "Pinboard View"
var back = "Back"

// Calendar Button
var prev_year = "Prev Year"
var prev_month = "LAST MONTH"
var next_month = "NEXT MONTH"
var next_year = "Next Year"