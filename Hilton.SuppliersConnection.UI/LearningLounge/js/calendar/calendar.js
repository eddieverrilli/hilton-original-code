// Variable
var thisDate = 1;
var today = new Date();
var todaysDay = today.getDay() + 1;
var todaysDate = today.getDate();
var todaysMonth = today.getMonth() + 1;
var todaysYear = today.getFullYear();

var firstDate;
var firstDay;
var lastDate;
var numbDays;
var numevents = 0;
var daycounter = 0;
var calendarString = "";

var monthNum_full = todaysMonth;
var yearNum_full = todaysYear;
var monthNum_compact = todaysMonth;
var yearNum_compact = todaysYear;

var tiva_events = [];
var _fullEventList = [];
var order_num = 0;

// Config variable
var wordDay;
var date_start;

//var _txtEventNameClientId;
var _txtDescriptionClientId;
var _txtLocationClientId;
//var _txtStartDateClientId;
var _txtEndDateClientId;
var _fileUploadClientId;
//var _chkAllDayEventClientId;
var _eventDetailId;
var _currentView;
//var _txtEditIdClientId;
var _hasMasonryLoaded = false;

function getShortText(text, num) {
	if(text) {
		// Get num of word
		var textArray = text.split(" ");
		if (textArray.length > num) {
			return textArray.slice(0, num).join(" ") + " ...";
		}
		return text;
	}
	return "";
}

function sortEventsByDate(a,b) {
	if (a.date < b.date) {
		return -1;
	} else if (a.date > b.date) {
		return 1;
	} else {
		return 0;
	}
}

function sortEventsByUpcoming(a,b) {
	var today_date = new Date(todaysYear, todaysMonth - 1, todaysDate);
	if (Math.abs(a.date - today_date.getTime()) < Math.abs(b.date - today_date.getTime())) {
		return -1;
	} else if (Math.abs(a.date - today_date.getTime()) > Math.abs(b.date - today_date.getTime())) {
		return 1;
	} else {
		return 0;
	}
}

function getEventsByTime(type) {
	var events = [];
	var today_date = new Date(todaysYear, todaysMonth - 1, todaysDate);
	for (var i = 0; i < tiva_events.length; i++) {
		if (type == 'upcoming') {
			if (tiva_events[i].date >= today_date.getTime()) {
				events.push(tiva_events[i]);
			}
		} else {
			if (tiva_events[i].date < today_date.getTime()) {
				events.push(tiva_events[i]);
			}
		}
	}
	return events;
}

// Change month or year on calendar
function changedate(btn, layout) {
	if (btn == "prevyr") {
		eval("yearNum_" + layout + "--;");
	} else if (btn == "nextyr") {
		eval("yearNum_" + layout + "++;");
	} else if (btn == "prevmo") {
		eval("monthNum_" + layout + "--;");
	} else if (btn == "nextmo") {
		eval("monthNum_" + layout + "++;");
	} else if (btn == "current") {
		eval("monthNum_" + layout + " = todaysMonth;");
		eval("yearNum_" + layout + " = todaysYear;");
	}

	if (monthNum_full == 0) {
		monthNum_full = 12;
		yearNum_full--;
	} else if (monthNum_full == 13) {
		monthNum_full = 1;
		yearNum_full++
	}
	
	if (monthNum_compact == 0) {
		monthNum_compact = 12;
		yearNum_compact--;
	} else if (monthNum_compact == 13) {
		monthNum_compact = 1;
		yearNum_compact++
	}
	
	// Get first day and number days of month
	eval("firstDate = new Date(yearNum_" + layout + ", monthNum_" + layout + " - 1, 1);");
	if (date_start == 'sunday') {
		firstDay = firstDate.getDay() + 1;
	} else {
		firstDay = firstDate.getDay();
	}
	eval("lastDate = new Date(yearNum_" + layout + ", monthNum_" + layout + ", 0);");
	numbDays = lastDate.getDate();
	
	// Create calendar
	eval("createCalendar(layout, firstDay, numbDays, monthNum_" + layout + ", yearNum_" + layout + ");");
	
	return;
}

// Create calendar
function createCalendar(layout, firstDay, numbDays, monthNum, yearNum) { 
    calendarString = '';
    daycounter = 0;
	
    calendarString += '<div class=\"calendar-title\">' + wordMonth[monthNum - 1] + '&nbsp;&nbsp;' + yearNum + '<\/span><\/div>';

    //if (isUserAdmin()) {
    //    calendarString += '<div id="create-event" style="display:none"><a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Create An Event</a></div>';
    //}

	calendarString += '<div class=\"calendar-btn month prev\"><span onClick=\"changedate(\'prevmo\', \'full\')\"><span class="btn-change-date"><i class="fa fa-caret-left"></i>' + prev_month + '<\/span><\/span><\/div>';
	calendarString += '<div class=\"calendar-btn month next\"><span onClick=\"changedate(\'nextmo\', \'full\')\"><span class="btn-change-date">' + next_month + '<i class="fa fa-caret-right"></i><\/span><\/span><\/div>';

	calendarString += '<table class=\"calendar-table table table-bordered\">';
	calendarString += '<tbody>';
	
	calendarString += '<tr class="active">';
	for (var m = 0; m < wordDay.length; m++) {
		calendarString += '<th>' + wordDay[m].substring(0, 3) + '<\/th>';
	}
	calendarString += '<\/tr>';

	thisDate == 1;
	
	for (var i = 1; i <= 6; i++) {
		var k = (i - 1) * 7 + 1;
		if (k < (firstDay + numbDays)) {
			calendarString += '<tr>';
			for (var x = 1; x <= 7; x++) {
				daycounter = (thisDate - firstDay) + 1;
				thisDate++;
				if ((daycounter > numbDays) || (daycounter < 1)) {
					calendarString += '<td class=\"calendar-day-blank\">&nbsp;<\/td>';
				} else {			
					// Saturday or Sunday
					if (date_start == 'sunday') {
						if ((x == 1) || (x == 7)) {
							daycounter_s = '<span class=\"calendar-day-weekend\">' + daycounter + '</span>';
						} else {
							daycounter_s = daycounter;
						}
					} else {
						if ((x == 6) || (x == 7)) {
							daycounter_s = '<span class=\"calendar-day-weekend\">' + daycounter + '</span>';
						} else {
							daycounter_s = daycounter;
						}
					}
					
					if ((todaysDate == daycounter) && (todaysMonth == monthNum)) {
						calendarString += '<td class=\"calendar-day-normal calendar-day-today\">';
					} else {
						calendarString += '<td class=\"calendar-day-normal\">';
					}
					
						calendarString += '<div class="calendar-day-divider"><img src="images/calendar/day-divider.png" /></div>';
					
						if (checkEvents(daycounter, monthNum, yearNum)) {
							if (layout == 'full') {
								calendarString += '<div class=\"calendar-day-event\">';
							} else {
								calendarString += '<div class=\"calendar-day-event\" onmouseover=\"showTooltip(0, \'compact\', ' + daycounter + ', ' + monthNum + ', ' + yearNum + ', this)\" onmouseout=\"clearTooltip(\'compact\', this)\" onclick=\"showEventDetail(0, \'compact\', '  + daycounter + ', ' + monthNum + ', ' + yearNum + ')\">';
							}
								calendarString += daycounter_s;
								
								// Get events of day
								if (layout == 'full') {
									var events = getEvents(daycounter, monthNum, yearNum);
									for (var t = 0; t < events.length; t++) {
										if (typeof events[t] != "undefined") {
											var color = events[t].color ? events[t].color : 1;
											var event_class = "one-day";
											if (events[t].first_day && !events[t].last_day) {
												event_class = "first-day";
											} else if (events[t].last_day && !events[t].first_day) {
												event_class = "last-day";
											} else if (!events[t].first_day && !events[t].last_day) {
												event_class = "middle-day";
											}
																				
											calendarString += '<div class=\"calendar-event-name ' + event_class + '" id=\"' + events[t].id + '\" onmouseover=\"showTooltip(' + events[t].id + ', \'full\', ' + daycounter + ', ' + monthNum + ', ' + yearNum + ', this)\" onmouseout=\"clearTooltip(\'full\', this)\" onclick=\"showEventDetail(' + events[t].id + ', \'full\', ' + daycounter + ', ' + monthNum + ', ' + yearNum + ')\" style="background-color:' + getCategoryColor(events[t]) + '"><span class="event-name">' + getShortText(events[t].name, 2) + '</span><\/div>';
										} else {
											var event_fake;
											if (typeof events[t+1] != "undefined") {
												if (typeof tiva_events[events[t+1].id - 1] != "undefined") { 
													event_fake = getShortText(tiva_events[events[t+1].id - 1].name, 2);
												} else {
													event_fake = "no-name";
												}
											} else {
												event_fake = "no-name";
											}
											calendarString += '<div class=\"calendar-event-name no-name\">' + event_fake + '</div>';
										}
									}
								} else {
									calendarString += '<span class="calendar-event-mark"></span>';
								}
								
								// Tooltip
								calendarString += '<div class=\"tiva-event-tooltip\"><\/div>';
							calendarString += '<\/div>';
						} else {
							calendarString += daycounter_s;
						}
					calendarString += '<\/td>';
				}
			}
			calendarString += '<\/tr>';
		}
	}
	calendarString += '</tbody>';
	calendarString += '</table>';

	jQuery('.tiva-calendar-full').html(calendarString);

	thisDate = 1;
}

// Check day has events or not
function checkEvents(day, month, year) {
	numevents = 0;
	var date_check = new Date(year, Number(month) - 1, day);
	for (var i = 0; i < tiva_events.length; i++) {
		var start_date = new Date(tiva_events[i].year, Number(tiva_events[i].month) - 1, tiva_events[i].day);
		var end_date = new Date(tiva_events[i].year, Number(tiva_events[i].month) - 1, Number(tiva_events[i].day) + Number(tiva_events[i].duration) - 1);
		if ((start_date.getTime() <= date_check.getTime()) && (date_check.getTime() <= end_date.getTime())) {
			numevents++;
		}
	}
	
	if (numevents == 0) {
		return false;
	} else {
		return true;
	}
}

function getOrderNumber(id, day, month, year) {
	var date_check = new Date(year, Number(month) - 1, day);
	var events = [];
	for (var i = 0; i < tiva_events.length; i++) {
		var start_date = new Date(tiva_events[i].year, Number(tiva_events[i].month) - 1, tiva_events[i].day);
		var end_date = new Date(tiva_events[i].year, Number(tiva_events[i].month) - 1, Number(tiva_events[i].day) + Number(tiva_events[i].duration) - 1);
		if ((start_date.getTime() <= date_check.getTime()) && (date_check.getTime() <= end_date.getTime())) {
			var first_day = (start_date.getTime() == date_check.getTime()) ? true : false;
			var event = {id:tiva_events[i].id, name:tiva_events[i].name, day:tiva_events[i].day, month:tiva_events[i].month, year:tiva_events[i].year, first_day:first_day};
			events.push(event);
		}
	}
	
	if (events.length) {
		if (events[0].id == id) {
			var num = order_num;
			order_num = 0;
			return num;	
		} else { 
			order_num++;
			for (var j = 0; j < events.length; j++) {
				if (events[j].id == id) {
					return getOrderNumber(events[j-1].id, events[j-1].day, events[j-1].month, events[j-1].year);
				}
			}
			
		}
	}
	
	return 0;
}

// Get events of day
function getEvents(day, month, year) {
	var n = 0;
	var date_check = new Date(year, Number(month) - 1, day);
	var events = [];
	for (var i = 0; i < tiva_events.length; i++) {
		var start_date = new Date(tiva_events[i].year, Number(tiva_events[i].month) - 1, tiva_events[i].day);
		var end_date = new Date(tiva_events[i].year, Number(tiva_events[i].month) - 1, Number(tiva_events[i].day) + Number(tiva_events[i].duration) - 1);
		if ((start_date.getTime() <= date_check.getTime()) && (date_check.getTime() <= end_date.getTime())) {
			var first_day = (start_date.getTime() == date_check.getTime()) ? true : false;
			var last_day = (end_date.getTime() == date_check.getTime()) ? true : false;
			var event = { id: tiva_events[i].id, name: tiva_events[i].name, first_day: first_day, last_day: last_day, color: tiva_events[i].color, CategoryIds: tiva_events[i].CategoryIds };
			
			if (!first_day) {
				n = getOrderNumber(tiva_events[i].id, tiva_events[i].day, tiva_events[i].month, tiva_events[i].year);
			}
			
			events[n] = event;
			n++;
		}
	}
	
	return events;
}

// Show tooltip when mouse over
function showTooltip(id, layout, day, month, year, el) {
	if (layout == 'full') {
		if (tiva_events[id].image) {
		    var event_image = '<img src="data:image/jpg;base64,' + tiva_events[id].image + '" alt="' + tiva_events[id].name + '" />';
		} else {
			var event_image = '';
		}
		if (tiva_events[id].time) {
			var event_time = '<div class="event-time">' + tiva_events[id].time + '</div>';
		} else {
			var event_time = '';
		}
		
		// Change position of tooltip
		var index = jQuery(el).parent().find('.calendar-event-name').index(el);
		var count = jQuery(el).parent().find('.calendar-event-name').length;
		var bottom = 32 + ((count - index - 1) * 25);
		jQuery(el).parent().find('.tiva-event-tooltip').css('bottom', bottom + 'px');
		
		jQuery(el).parent().find('.tiva-event-tooltip').html(	'<div class="event-tooltip-item">'
																+ event_time
																+ '<div class="event-name">' + tiva_events[id].name + '</div>'
																+ '<div class="event-image">' + event_image + '</div>'
																+ '<div class="event-intro">' + getShortText(tiva_events[id].description, 10) + '</div>'
																+ '</div>'
															);
		jQuery(el).parent().find('.tiva-event-tooltip').css('opacity', '1');
		jQuery(el).parent().find('.tiva-event-tooltip').css('-webkit-transform', 'translate3d(0,0,0) rotate3d(0,0,0,0)');
		jQuery(el).parent().find('.tiva-event-tooltip').css('transform', 'translate3d(0,0,0) rotate3d(0,0,0,0)');
	} else {
		jQuery(el).find('.tiva-event-tooltip').html('');
		var events = getEvents(day, month, year);
		for (var i = 0; i < events.length; i++) {
			if (typeof events[i] != "undefined") {
				if (tiva_events[events[i].id].image) {
				    var event_image = '<img src="data:image/jpg;base64,' + tiva_events[events[i].id].image + '" alt="' + tiva_events[events[i].id].name + '" />';
				} else {
					var event_image = '';
				}
				if (tiva_events[events[i].id].time) {
					var event_time = '<div class="event-time">' + tiva_events[events[i].id].time + '</div>';
				} else {
					var event_time = '';
				}
		
				jQuery(el).find('.tiva-event-tooltip').append(	'<div class="event-tooltip-item">'
																+ event_time
																+ '<div class="event-name">' + tiva_events[events[i].id].name + '</div>'
																+ '<div class="event-image">' + event_image + '</div>'
																+ '<div class="event-intro">' + getShortText(tiva_events[events[i].id].description, 10) + '</div>'
																+ '</div>'
															);
			}
		}
		jQuery(el).find('.tiva-event-tooltip').css('opacity', '1');
		jQuery(el).find('.tiva-event-tooltip').css('-webkit-transform', 'translate3d(0,0,0) rotate3d(0,0,0,0)');
		jQuery(el).find('.tiva-event-tooltip').css('transform', 'translate3d(0,0,0) rotate3d(0,0,0,0)');
	}
}

// Clear tooltip when mouse out
function clearTooltip(layout, el) {
	if (layout == 'full') {
		jQuery(el).parent().find('.tiva-event-tooltip').css('opacity', '0');
		jQuery(el).parent().find('.tiva-event-tooltip').css('-webkit-transform', 'translate3d(0,-10px,0)');
		jQuery(el).parent().find('.tiva-event-tooltip').css('transform', 'translate3d(0,-10px,0)');
	} else {
		jQuery(el).find('.tiva-event-tooltip').css('opacity', '0');
		jQuery(el).find('.tiva-event-tooltip').css('-webkit-transform', 'translate3d(0,-10px,0)');
		jQuery(el).find('.tiva-event-tooltip').css('transform', 'translate3d(0,-10px,0)');
	}
}


function showEventList(layout, max_events) {
	// Sort event via upcoming
	var upcoming_events = getEventsByTime('upcoming');
	upcoming_events.sort(sortEventsByUpcoming);
	//var past_events = getEventsByTime('past');
	//past_events.sort(sortEventsByUpcoming);
    //var tiva_list_events = upcoming_events.concat(past_events);
	var tiva_list_events = upcoming_events.slice(0, max_events);
	//tiva_list_events = tiva_list_events.slice(0, max_events);
	var num_events_loaded;
	
	if (!_hasMasonryLoaded) {
	    num_events_loaded = jQuery('.hero-grid .hero-grid__item').length;

	} else {
	    num_events_loaded = $('.hero-grid').masonry('getItemElements').length;
	}

	if (layout == 'full' && num_events_loaded < 1) {
	    jQuery('.tiva-event-list-full').html('');

		for (var i = 0; i < tiva_list_events.length; i++) {
			// Start date
			var day = new Date(tiva_list_events[i].year, Number(tiva_list_events[i].month) - 1, tiva_list_events[i].day);
			if (date_start == 'sunday') {
				var event_day = wordDay[day.getDay()];
			} else {
				if (day.getDay() > 0) {
					var event_day = wordDay[day.getDay() - 1];
				} else {
					var event_day = wordDay[6];
				}
			}
			var event_date = wordMonth[Number(tiva_list_events[i].month) - 1] + ' ' + tiva_list_events[i].day + ', ' + tiva_list_events[i].year;
			
			// End date
			var event_end_time = '';
			if (tiva_list_events[i].duration > 1) {
				var end_date = new Date(tiva_list_events[i].year, Number(tiva_list_events[i].month) - 1, Number(tiva_list_events[i].day) + Number(tiva_list_events[i].duration) - 1);
				
				if (date_start == 'sunday') {
					var event_end_day = wordDay[end_date.getDay()];
				} else {
					if (end_date.getDay() > 0) {
						var event_end_day = wordDay[end_date.getDay() - 1];
					} else {
						var event_end_day = wordDay[6];
					}
				}
				var event_end_date = wordMonth[Number(end_date.getMonth())] + ' ' + end_date.getDate() + ', ' + end_date.getFullYear();
				event_end_time = ' - ' + event_end_day + ', ' + event_end_date;
			}
			
			// Event time
			if (tiva_list_events[i].time) {
				var event_time = tiva_list_events[i].time;
			} else {
				var event_time = '';
			}
		
			// Event image
			if (tiva_list_events[i].image) {
			    var event_image = '<img src="data:image/jpg;base64,' + tiva_list_events[i].image + '" alt="' + tiva_list_events[i].name + '" />';
			} else {
				var event_image = '';
			}
			
			var divString = '<div id="heroGrid-' + tiva_list_events[i].id + '" class="hero-grid__item" style="background-color:' + getCategoryColor(tiva_list_events[i]) + '">'
                                                + '<div class="hero__img">'
                                                    + '<div class="event-image link" onclick="showEventDetail(' + tiva_list_events[i].id + ', \'full\', 0, 0, 0)">' + event_image + '</div>'
                                                + '</div>'                                                
                                                + '<div class="hero__description">'
                                                    //+ '<div class="event-day">' + event_day + '</div>'
                                                    + '<div class="event-date">' + event_day + ' ' + event_date + event_end_time + '</div>'
                                                    + '<div class="event-divider"><img src="images/calendar/event-date-divider.gif" /></div>'
                                                    + '<div class="event-time">' + event_time + '</div>'
                                                    + '<div class="event-name link" onclick="showEventDetail(' + tiva_list_events[i].id + ', \'full\', 0, 0, 0)">' + tiva_list_events[i].name + '</div>'
                                                    + '<div class="event-intro">' + getShortText(tiva_list_events[i].description, 25) + '</div>'
                                                    + '<div class="event-cat-sep"><hr style="margin:15px 0 5px 0"/></div>'
                                                    + '<div class="event-category"style="text-align:center;"><b>' + getCategoryText(tiva_list_events[i]) + '</b></div>'
                                                + '</div>'
                                            + '</div>';

			if (!_hasMasonryLoaded) {
			    jQuery('.hero-grid').append(divString);

			} else {
			    var $content = $(divString);
			    $('.hero-grid').append($content).masonry('appended', $content);
			}
            			
		}
	
	    //if (document.getElementById("btnLoadMore") == null) {
		//$('#load-more-events-wrapper').remove();

		    //jQuery('.tiva-events-calendar-wrap #calendar-list-wrapper').append('<div id="load-more-events-wrapper">'
            //                                            + '<button id="btnLoadMore" type="button" class="btn btn-primary" data-dismiss="modal">LOAD MORE EVENTS</button>'
            //                                            + '</div>'
            //                                            );
		//}
		
		if (!_hasMasonryLoaded) {
		    var $grid = $('.hero-grid').masonry({
		        resize: true
		    });

		    // init Masonry grid -->
		    setTimeout(function () {
		        $grid.masonry();
		    }, 100);

		    _hasMasonryLoaded = true;

		} else {
		    $('.hero-grid').masonry('layout');
		}
		
	}
}

function getCategoryColor(calEvent) {

    if (calEvent.CategoryIds.indexOf(3) > -1) {  //Industry Events
        return "#65a151";
    } else if (calEvent.CategoryIds.indexOf(4) > -1) {  //Lunch & Learns
        return "#f04d22";
    } if (calEvent.CategoryIds.indexOf(5) > -1) {  //Hilton Conferences
        return "#efa61f";
    } if (calEvent.CategoryIds.indexOf(6) > -1) {   //Meet & Greets
        return "#29b3e6";
    } else {
        return "#871580";   //Hilton Office
    }
} 

function getCategoryText(calEvent) {

    if (calEvent.CategoryIds.indexOf(3) > -1) {  //Industry Events
        return "Industry Event";
    } else if (calEvent.CategoryIds.indexOf(4) > -1) {  //Lunch & Learns
        return "Lunch & Learn";
    } if (calEvent.CategoryIds.indexOf(5) > -1) {  //Hilton Conferences
        return "Hilton Conference";
    } if (calEvent.CategoryIds.indexOf(6) > -1) {   //Meet & Greets
        return "Meet & Greet";
    } else {
        return "Hilton Office";   //Hilton Office
    }
} 

// Show event detail
function showEventDetail(id, layout, day, month, year) {
    _eventDetailId = id;

    //if month view is visible, set text to "Back to Calendar".
    //if list view is visible, set text to "Back to Pinboard".
    var backText;
    if (jQuery('.tiva-events-calendar').find('.calendar-view').hasClass('active')) {
        backText = "Back to Calendar";
        _currentView = "calendar";
    } else {
        backText = "Back to Pinboard";
        _currentView = "list";
    }

	jQuery('.tiva-events-calendar.' + layout + ' .back-calendar').show();
	jQuery('.tiva-events-calendar.' + layout + ' .tiva-calendar').hide();
	//jQuery('.tiva-events-calendar.' + layout + ' .tiva-event-list').hide();
	jQuery('.tiva-events-calendar.' + layout + ' #calendar-list-wrapper').hide();
	jQuery('.tiva-events-calendar.' + layout + ' .tiva-event-detail').fadeIn(1500);
	
	jQuery('.tiva-events-calendar.' + layout + ' .list-view').removeClass('active');
	jQuery('.tiva-events-calendar.' + layout + ' .calendar-view').removeClass('active');
	
	if (layout == 'full') {
		// Start date
		var day = new Date(tiva_events[id].year, Number(tiva_events[id].month) - 1, tiva_events[id].day);
		if (date_start == 'sunday') {
			var event_day = wordDay[day.getDay()];
		} else {
			if (day.getDay() > 0) {
				var event_day = wordDay[day.getDay() - 1];
			} else {
				var event_day = wordDay[6];
			}
		}
		var event_date = wordMonth[Number(tiva_events[id].month) - 1] + ' ' + tiva_events[id].day + ', ' + tiva_events[id].year;
		
		// End date
		var event_end_time = '';
		if (tiva_events[id].duration > 1) {
			var end_date = new Date(tiva_events[id].year, Number(tiva_events[id].month) - 1, Number(tiva_events[id].day) + Number(tiva_events[id].duration) - 1);
			
			if (date_start == 'sunday') {
				var event_end_day = wordDay[end_date.getDay()];
			} else {
				if (end_date.getDay() > 0) {
					var event_end_day = wordDay[end_date.getDay() - 1];
				} else {
					var event_end_day = wordDay[6];
				}
			}
			var event_end_date = wordMonth[Number(end_date.getMonth())] + ' ' + end_date.getDate() + ', ' + end_date.getFullYear();
			event_end_time = ' - ' + event_end_day + ', ' + event_end_date;
		}
		
		// Event time
		if (tiva_events[id].time) {
			var event_time = tiva_events[id].time;
		} else {
			var event_time = '';
		}
		
		// Event image
		if (tiva_events[id].image) {
		    var event_image = '<img src="data:image/jpg;base64,' + tiva_events[id].image + '" alt="' + tiva_events[id].name + '" />';
		} else {
			var event_image = '';
		}
		
		// Event location
		if (tiva_events[id].location) {
			var event_location = tiva_events[id].location;
		} else {
			var event_location = '';
		}
		
	    // Event description
		var event_desc = "";
		if (tiva_events[id].description) {
			event_desc = '<div class="event-desc">' + tiva_events[id].description + '</div>';
		}

	    // Event URL
		var event_url = "";
		if (tiva_events[id].URL) {
		    event_url = '<div class="event-desc"><a href="' + tiva_events[id].URL + '" target="_blank">Click Here to Visit the Event Website</a></div>';
		}

		var adminButtonsDisplay = "none";
		if (isUserAdmin() && _isAdminModeToggledOn) {
		    adminButtonsDisplay = "";
		}

		jQuery('.tiva-event-detail-full').html('<div class="event-item">'
													+ '<div id="manage-event" style="display:' + adminButtonsDisplay + '">'
														+ '<div class="admin-header">Manage Event</div>'
														+ '<div class="admin-options"><a href="#" id="edit-event" class="btn">Edit</a> <a href="#" id="duplicate-event" class="btn">Duplicate</a> <a href="#" id="delete-event" class="btn confirmation">Delete</a></div>'
													+ '</div>'
													+ '<div class="event-image">' + event_image + '</div>'                                                    
													+ '<div class="event-copy">'
														+ '<div class="event-category">' + getCategoryText(tiva_events[id]) + '</div>'
//														+ '<div class="event-day">' + event_day + '</div>'
														+ '<div class="event-date">' + event_day + ' ' + event_date + event_end_time + '</div>'
														+ '<div class="event-divider"><img src="images/calendar/event-date-divider-blue.gif" /></div>'
														+ '<div class="event-time">' + event_time + '</div>'
														+ '<div class="event-location">' + event_location + '</div>'
														+ '<div class="event-name">' + tiva_events[id].name + '</div>'
														+ event_desc
                                                        + event_url
														+ '<span class="back-calendar"><i class="fa fa-caret-left"></i>' + backText + '</span>'
													+ '</div>'
												+ '</div>'
											);
	}
	
	
	// Click - Back calendar btn	
	jQuery('.tiva-events-calendar .back-calendar').click(function () {

		jQuery(this).parents('.tiva-events-calendar').find('.back-calendar').hide();
		jQuery(this).parents('.tiva-events-calendar').find('.tiva-event-detail').hide();

	    //if month view is clicked, show calendar.
	    //if list view is visible, show Pinboard.
		if (_currentView == "calendar") {
		    jQuery(this).parents('.tiva-events-calendar').find('.tiva-calendar').fadeIn(1500);

		    jQuery(this).parents('.tiva-events-calendar').find('.list-view').removeClass('active');
		    jQuery(this).parents('.tiva-events-calendar').find('.calendar-view').addClass('active');
		} else {
		    jQuery(this).parents('.tiva-events-calendar').find('#calendar-list-wrapper').fadeIn(1500);

		    jQuery(this).parents('.tiva-events-calendar').find('.calendar-view').removeClass('active');
		    jQuery(this).parents('.tiva-events-calendar').find('.list-view').addClass('active');
		}
		
		//var initial_view = (typeof jQuery(this).parents('.tiva-events-calendar').attr('data-view') != "undefined") ? jQuery(this).parents('.tiva-events-calendar').attr('data-view') : 'calendar';
		//if (initial_view == 'calendar') {
		//	jQuery(this).parents('.tiva-events-calendar').find('.tiva-calendar').fadeIn(1500);
			
		//	jQuery(this).parents('.tiva-events-calendar').find('.list-view').removeClass('active');
		//	jQuery(this).parents('.tiva-events-calendar').find('.calendar-view').addClass('active');
		//} else {
		//	jQuery(this).parents('.tiva-events-calendar').find('#calendar-list-wrapper').fadeIn(1500);
			
		//	jQuery(this).parents('.tiva-events-calendar').find('.calendar-view').removeClass('active');
		//	jQuery(this).parents('.tiva-events-calendar').find('.list-view').addClass('active');
		//}
	});
	
}  //end showEventDetail

jQuery(document).ready(function() {
    documentReady();

    //call the function that will show the create event link if admin tools are toggled on.
    adminToolsToggled();

    $('#create-event-modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    if ($(window).width() <= 992) {
        $("label[for='conferences']").text("Hilton Conf.");

    } else {
        $("label[for='conferences']").text("Hilton Conferences");
    }
});  //end document.ready

function documentReady() {
    var createLink = '';

    if (isUserAdmin()) {
        createLink = '<div id="create-event" style="display:none"><a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Create An Event</a></div>';
    }

    var meetNGreetLunchNLearnDisplay = 'none';

    //only display the meet n greet and lunch n learn filter boxes if logged in as a partner or administrator or corporate.
    if (_userTypeId == 2 || _userTypeId == 5 || _userTypeId == 6) {
        meetNGreetLunchNLearnDisplay = "";
    }

    // Init calendar full
    if (jQuery('.tiva-events-calendar.full').length) {
        jQuery('.tiva-events-calendar.full').html('<div class="events-calendar-bar">'
														+ '<span class="bar-btn calendar-view"><i class="fa fa-calendar-o"></i>' + calendar_view + '</span>'
														+ '<span class="bar-btn list-view active"><i class="fa fa-list"></i>' + list_view + '</span>'
													+ '</div>'
													+ '<div class="cleardiv"></div>'

													+ '<div class="row">'
														+ '<div class="col-md-12">'
												            + '<div class="navbar-form navbar-search" role="search">'
												                + '<div class="input-group inner-addon left-addon">'
												                	+ '<i class="glyphicon glyphicon-search"></i>'
												                    + '<input id="txtSearch" type="text" class="form-control" placeholder="Smart Search">'
												                   /*
												                    + '<div class="input-group-btn">'
												                        + '<button type="button" class="btn btn-primary btn-info">'
												                            + '<span class="label-icon">Search</span>'
												                        + '</button>'
												                    + '</div>'
												                   */
												                + '</div>'
												            + '</div>'
												        + '</div>'
													+ '</div>'

													+ '<div id="calendar-filters">'
														+ '<div role="form">'
															+ '<div class="row">'
																+ '<div class="col-xs-4 col-xs-offset-0 col-sm-3 col-sm-offset-2">'
																	+ '<fieldset>'
																		+ '<div class="checkbox eventCategoryFilterCheckbox" style="border-color: #efa61f;">'
																			+ '<input id="conferences" type="checkbox" checked="" style="margin-left:-10px;">'
																			+ '<label for="conferences" style="margin-left:8px">'
																				+ 'Hilton Conferences'
																			+ '</label>'
																		+ '</div>'
																		+ '<div class="checkbox eventCategoryFilterCheckbox" style="border-color: #f04d22; display:' + meetNGreetLunchNLearnDisplay + ';">'
																			+ '<input id="lunch-and-learns" type="checkbox" checked="">'
																			+ '<label for="lunch-and-learns">'
																				+ 'Lunch & Learns'
																			+ '</label>'
																		+ '</div>'
																	+ '</fieldset>'
																+ '</div>'
																+ '<div class="col-xs-4 col-sm-3">'
																	+ '<fieldset>'
																		+ '<div class="checkbox eventCategoryFilterCheckbox" style="border-color: #871580;">'
																			+ '<input id="hilton-office" type="checkbox" checked="">'
																			+ '<label for="hilton-office">'
																				+ 'Hilton Office'
																			+ '</label>'
																		+ '</div>'
																		+ '<div class="checkbox eventCategoryFilterCheckbox" style="border-color: #29b3e6; display:' + meetNGreetLunchNLearnDisplay + ';">'
																			+ '<input id="meet-and-greets" type="checkbox" checked="">'
																			+ '<label for="meet-and-greets">'
																				+ 'Meet & Greets'
																			+ '</label>'
																		+ '</div>'
																	+ '</fieldset>'
																+ '</div>'
																+ '<div class="col-xs-4 col-sm-3">'
																	+ '<fieldset>'
																		+ '<div class="checkbox eventCategoryFilterCheckbox" style="border-color: #65a151;">'
																			+ '<input id="industry-events" type="checkbox" checked="">'
																			+ '<label for="industry-events">'
																				+ 'Industry Events'
																			+ '</label>'
																		+ '</div>'
																	+ '</fieldset>'
																+ '</div>'
															+ '</div>'
														+ '</div>'
													+ '</div>'

													+ createLink

													+ '<div class="tiva-events-calendar-wrap">'
														+ '<div class="tiva-calendar-full tiva-calendar"></div>'
														+ '<div id="calendar-list-wrapper"><div class="hero-grid clearfix"></div></div>'
														+ '<div class="tiva-event-detail-full tiva-event-detail"></div>'
													+ '</div>'
												);


    }

    // Show - Hide view
    jQuery('.tiva-events-calendar .back-calendar').hide();
    jQuery('#calendar-list-wrapper').hide();
    jQuery('.tiva-event-detail').hide();

    jQuery('.tiva-events-calendar').each(function (index) {
        // Hide switch button
        var switch_button = (typeof jQuery(this).attr('data-switch') != "undefined") ? jQuery(this).attr('data-switch') : 'show';
        if (switch_button == 'hide') {
            jQuery(this).find('.calendar-view').hide();
            jQuery(this).find('.list-view').hide();

            // Change css of button back
            jQuery(this).find('.events-calendar-bar').css('position', 'relative');
            jQuery(this).find('.back-calendar').css({ "position": "absolute", "margin-top": "15px", "right": "15px" });
            jQuery(this).find('.tiva-event-detail').css('padding-top', '60px');
        }
    });

    // Set wordDay
    date_start = (typeof jQuery('.tiva-events-calendar').attr('data-start') != "undefined") ? jQuery('.tiva-events-calendar').attr('data-start') : 'sunday';
    if (date_start == 'sunday') {
        wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
    } else { // Start with Monday
        wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
    }

    retrieveEvents();

    // Click - Calendar view btn
    jQuery('.tiva-events-calendar .calendar-view').click(function () {
        jQuery(this).parents('.tiva-events-calendar').find('.back-calendar').hide();
        jQuery(this).parents('.tiva-events-calendar').find('#calendar-list-wrapper').hide();
        jQuery(this).parents('.tiva-events-calendar').find('.tiva-event-detail').hide();
        jQuery(this).parents('.tiva-events-calendar').find('.tiva-calendar').fadeIn(1500);

        jQuery(this).parents('.tiva-events-calendar').find('.list-view').removeClass('active');
        jQuery(this).parents('.tiva-events-calendar').find('.calendar-view').addClass('active');
    });

    // Click - List view btn
    jQuery('.tiva-events-calendar .list-view').click(function () {
        jQuery(this).parents('.tiva-events-calendar').find('.back-calendar').hide();
        jQuery(this).parents('.tiva-events-calendar').find('.tiva-calendar').hide();
        jQuery(this).parents('.tiva-events-calendar').find('.tiva-event-detail').hide();
        jQuery(this).parents('.tiva-events-calendar').find('#calendar-list-wrapper').fadeIn(1500);

        jQuery(this).parents('.tiva-events-calendar').find('.calendar-view').removeClass('active');
        jQuery(this).parents('.tiva-events-calendar').find('.list-view').addClass('active');

        var max_events = jQuery(this).parents('.tiva-events-calendar').attr('data-events') ? jQuery(this).parents('.tiva-events-calendar').attr('data-events') : 1000;
        showEventList('full', max_events);
    });

    $('#calendar-filters :checkbox').change(function () {
        applyEventFilter();
    });

    $('#txtSearch').keyup(function (event) {
        applyEventFilter();
    });

    $('.input-group.date').on('click', '.input-group-addon', function () {
        var amPmToggle = $('.bootstrap-datetimepicker-widget').find('.timepicker [data-action=togglePeriod]');
        $(amPmToggle).css({ background: '#eeeeee', color: '#272727', padding: '12px', width: '42px' });

        $(amPmToggle).mouseover(function () {
            $(this).css({ background: '#27ade4', color: '#ffffff' });
        });
        $(amPmToggle).mouseout(function () {
            $(this).css({ background: '#eeeeee', color: '#272727', padding: '12px' });
        });
    })

    $('#picker-start-date').datetimepicker();
    $('#picker-end-date').datetimepicker({
        useCurrent: false //Important! See issue #1075
    });
    $("#picker-start-date").on("dp.change", function (e) {
        $('#picker-end-date').data("DateTimePicker").minDate(e.date);
    });
    $("#picker-end-date").on("dp.change", function (e) {
        $('#picker-start-date').data("DateTimePicker").maxDate(e.date);
    });

    $('#' + _fileUploadClientId).on('change', function () {
        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

        if (extn == 'png' || extn == 'jpg') {

            if (typeof (FileReader) != 'undefined') {

                var image_holder = $('#event-image-container');
                image_holder.empty();

                var reader = new FileReader();
                reader.onload = function (e) {
                    $('<img />', {
                        'src': e.target.result,
                        'class': 'thumb-image'
                    }).appendTo(image_holder);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);

            } else {
                console.log('Your browser does not support FileReader.');
            }
        } else {
            console.log('Please select only .png images');
        }
    });

    $('#event-all-day-container').on('change', '#chkAllDayEvent', function (e) {
        //console.log("all day changing");

        if (e.currentTarget.checked) {
            $('.form-group.end-date').fadeOut(300);
            $('#picker-start-date').data("DateTimePicker").format("MM/DD/YYYY");

        } else {
            $('.form-group.end-date').fadeIn(300);
            $('#picker-start-date').data("DateTimePicker").format(false);
        }

    });

    jQuery('.tiva-events-calendar').on('click', '#delete-event', function () {
        if (confirm("Are you sure you want to delete this event?") == true) {
            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/DeleteCalendarEvent",
                data: "{eventId: " + tiva_events[_eventDetailId].EventId + "}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var returnValue = data.d;

                    if (returnValue == "SUCCESS") {
                        alert('The event was deleted successfully');

                        location.reload();

                        //jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.back-calendar').hide();
                        //jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.tiva-event-detail').hide();

                        //if (_currentView == "calendar") {
                        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.tiva-calendar').fadeIn(1500);

                        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.list-view').removeClass('active');
                        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.calendar-view').addClass('active');

                        //} else {
                        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('#calendar-list-wrapper').fadeIn(1500);

                        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.calendar-view').removeClass('active');
                        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.list-view').addClass('active');
                        //}
                    }
                }
            });
        }
    });

    jQuery('.tiva-events-calendar').on('click', '#duplicate-event', function () {
        showEditForm();

        $('#txtDuplicatedEventId').val(tiva_events[_eventDetailId].EventId);

        $('#txtEditId').val(-1);
    });

    $('.tiva-events-calendar').on('click', '#edit-event', function () {
        showEditForm();
        $('#txtDuplicatedEventId').val(-1);
    });

    $('.tiva-events-calendar.full').on('click', '#create-event', function () {
        $('#lblCreateEditDupTitle').text("Create");

        //Assigning a valid number to the txtEditId form control 
        $('#txtEditId').val(-1);

        var dt = new Date();
        $('#picker-start-date').data("DateTimePicker").date(dt);

        clearEditForm();

        $('#create-event-modal').modal('show');
    })

    function showEditForm() {
        var hours = 0;
        var minutes = 0;

        var start_date = new Date(parseInt(tiva_events[_eventDetailId].StartDate.substr(6)));
        start_date.setHours(tiva_events[_eventDetailId].startHour);

        var end_date = new Date(parseInt(tiva_events[_eventDetailId].EndDate.substr(6)));

        //clear the start and end date pickers.
        $('#picker-start-date').data("DateTimePicker").clear();
        $('#picker-end-date').data("DateTimePicker").clear();

        //if end_date is equal to DateTime.MinValue, then make this an all day event.
        if (end_date.getFullYear() == 1) {
            $('#chkAllDayEvent').prop('checked', true);
            $('#picker-start-date').data("DateTimePicker").format("MM/DD/YYYY");
            $('.form-group.end-date').fadeOut(300);

        } else {
            $('#chkAllDayEvent').prop('checked', false);
            $('.form-group.end-date').fadeIn(300);

            $('#picker-start-date').data("DateTimePicker").format(false);
            end_date.setHours(tiva_events[_eventDetailId].endHour);

            $('#picker-end-date').data("DateTimePicker").date(end_date);
        }
        

        //set the start and end date picker values.
        $('#picker-start-date').data("DateTimePicker").date(start_date);        

        $('#txtEventName').val(tiva_events[_eventDetailId].name);
        $('#' + _txtDescriptionClientId).val(tiva_events[_eventDetailId].description);
        $('#' + _txtLocationClientId).val(tiva_events[_eventDetailId].location);
        $('#txtEventURL').val(tiva_events[_eventDetailId].URL);

        var image_holder = $('#event-image-container');
        image_holder.empty();
        $('<img />', {
            'src': 'data:image/jpg;base64,' + tiva_events[_eventDetailId].image,
            'class': 'thumb-image'
        }).appendTo(image_holder);
        image_holder.show();

        setSelectedCategories(tiva_events[_eventDetailId].CategoryIds);
        setSelectedUserTypes(tiva_events[_eventDetailId].UserTypeIds, tiva_events[_eventDetailId].RestrictToUserTypes);

        //Assigning a valid number to the txtEditId form control 
        $('#txtEditId').val(tiva_events[_eventDetailId].EventId);

        $('#lblCreateEditDupTitle').text('Edit');
        $('#create-event-modal').modal('show');
    }
}

function clearEditForm() {
    $('#chkAllDayEvent').prop('checked', false);
    $('#picker-start-date').data("DateTimePicker").format(false);

    var dt = new Date();
    $('#picker-start-date').data("DateTimePicker").date(dt);

    $('#picker-end-date').data("DateTimePicker").clear();

    $('#txtEventName').val('');
    $('#' + _txtDescriptionClientId).val('');
    $('#' + _txtLocationClientId).val('');
    $('#txtEventURL').val('');

    var image_holder = $('#event-image-container');
    image_holder.empty();

    $("#chkMeetAndGreet").prop("checked", false);
    $("#chkHiltonOffice").prop("checked", false);
    $("#chkIndustryEvent").prop("checked", false);
    $("#chkLunchAndLearn").prop("checked", false);
    $("#chkHiltonConference").prop("checked", false);



}

function applyEventFilter() {
    var selectedCategories = getSelectedCategories();
    var filterText = $("#txtSearch").val().toLowerCase();
    var matchesTextFilter;

    tiva_events = [];
    
    //Loop through the list of events and determine if each event is in the list of selected categories.
    for (var j = 0; j < _fullEventList.length; j++) {                

        matchesTextFilter = false;

        if (filterText.length == 0 || (filterText.length > 0 && (_fullEventList[j].name.toLowerCase().indexOf(filterText) > -1) || _fullEventList[j].description.toLowerCase().indexOf(filterText) > -1)) {
            matchesTextFilter = true;
        }

        //Loop through the list of category ids for the current event.  If a category id is in the selected 
        //categories list, add the event to the tiva_events array which will populate the calendar.
        for (var i = 0; i < _fullEventList[j].CategoryIds.length; i++) {
            if (selectedCategories.indexOf(_fullEventList[j].CategoryIds[i]) > -1 && matchesTextFilter == true) {
                tiva_events.push(_fullEventList[j]);
                
                //Since at least on event category is selected, exit the CategoryIds loop and continue to the next event.
                break;
            }
        }
    }

    if (jQuery('.tiva-events-calendar').find('.calendar-view').hasClass('active')) {
        // Create calendar
        eval("createCalendar('full', firstDay, numbDays, monthNum_full, yearNum_full);");

    } else {
        var max_events = jQuery(this).parents('.tiva-events-calendar').attr('data-events') ? jQuery(this).parents('.tiva-events-calendar').attr('data-events') : 1000;

        //clear the pinboard items
        //$('.hero-grid').html('');
        $('.hero-grid').masonry('remove', $('.hero-grid').masonry('getItemElements'));
            

        //$('.hero-grid').masonry('destroy', this);

        showEventList('full', max_events);
    }

}

function getSelectedCategories() {

    var selectedCategories = [];

    if ($("#meet-and-greets").is(':checked')) {
        selectedCategories.push(6);
    }

    if ($("#lunch-and-learns").is(':checked')) {
        selectedCategories.push(4);
    }

    if ($("#hilton-office").is(':checked')) {
        selectedCategories.push(7);
    }

    if ($("#conferences").is(':checked')) {
        selectedCategories.push(5);
    }

    if ($("#industry-events").is(':checked')) {
        selectedCategories.push(3);
    }

    return selectedCategories;
}

function setSelectedCategories(categories) {

    $("#chkMeetAndGreet").prop("checked", false);
    $("#chkHiltonOffice").prop("checked", false);
    $("#chkIndustryEvent").prop("checked", false);
    $("#chkLunchAndLearn").prop("checked", false);
    $("#chkHiltonConference").prop("checked", false);

    for (var i = 0; i < categories.length; i++) {
        if (categories[i] == 6) {
            $("#chkMeetAndGreet").prop("checked", true);
        } else if (categories[i] == 7) {
            $("#chkHiltonOffice").prop("checked", true);
        } else if (categories[i] == 3) {
            $("#chkIndustryEvent").prop("checked", true);
        } else if (categories[i] == 4) {
            $("#chkLunchAndLearn").prop("checked", true);
        } else if (categories[i] == 5) {
            $("#chkHiltonConference").prop("checked", true);
        }
    }
}

function setSelectedUserTypes(userTypes, restrictToUserTypes) {

    $("#chkEveryone").prop("checked", false);
    $("#chkPartners").prop("checked", false);
    $("#chkOwnersConsultants").prop("checked", false);
    $("#chkEmployeesOnly").prop("checked", false);

    if (restrictToUserTypes == "EVERYONE") {
        $("#chkEveryone").prop("checked", true);

    } else if (restrictToUserTypes == "AUTHENTICATEDUSERS") {
        $("#chkEmployeesOnly").prop("checked", true);

    } else {
        for (var i = 0; i < userTypes.length; i++) {
            if (userTypes[i] == 2) {
                $("#chkPartners").prop("checked", true);
            } else if (userTypes[i] == 3 || userTypes[i] == 4) {
                $("#chkOwnersConsultants").prop("checked", true);
            } 
        }
    }

}

function adminToolsToggled() {

    if (_isAdminModeToggledOn) {
        $('#create-event').show();
    } else {
        $('#create-event').hide();
    }

    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/ToggleAdminTools",
        data: "{isOn: " + _isAdminModeToggledOn + "}",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

        }
    });
}

function btnSubmit_Click() {

    if (validateForm() == true) {
        $('#create-event-modal').modal('hide');
        return true;

    } else {
        return false;
    }

}

function uploadComplete() {
    if ($('#upload-iframe').contents().find("body")[0].innerText.length > 0) {
        alert($('#upload-iframe').contents().find("body")[0].innerText);

        //jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.back-calendar').hide();
        //jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.tiva-event-detail').hide();

        //if (_currentView == "calendar") {
        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.tiva-calendar').fadeIn(1500);

        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.list-view').removeClass('active');
        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.calendar-view').addClass('active');

        //} else {
        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('#calendar-list-wrapper').fadeIn(1500);

        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.calendar-view').removeClass('active');
        //    jQuery('.tiva-events-calendar .back-calendar').parents('.tiva-events-calendar').find('.list-view').addClass('active');
        //}

        location.reload();
    }
}

function retrieveEvents() {
    // Get the events from a web service via ajax.
    $.ajax({
        type: "POST",
        url: "LearningLoungeService.asmx/GetCalendarEvents",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        beforeSend: function () {
            jQuery('.tiva-calendar').html('<div class="loading"><img src="images/calendar/loading.gif" /></div>');
        },
        success: function (eventJson) {
            var data = eventJson.d;

            tiva_events = [];
            _fullEventList = [];

            for (var i = 0; i < data.length; i++) {
                //var event_date = new Date(data[i].year, Number(data[i].month) - 1, data[i].day);
                var event_date = new Date(parseInt(data[i].StartDate.substr(6)));
                data[i].date = event_date.getTime();
                tiva_events.push(data[i]);
            }

            // Sort events by date
            tiva_events.sort(sortEventsByDate);

            for (var j = 0; j < tiva_events.length; j++) {
                tiva_events[j].id = j;
                if (!tiva_events[j].duration) {
                    tiva_events[j].duration = 1;
                }
            }

            _fullEventList = tiva_events;

            // Create calendar
            changedate('current', 'full');
            //changedate('current', 'compact');

            jQuery('.tiva-events-calendar').each(function (index) {
                // Initial view
                var initial_view = (typeof jQuery(this).attr('data-view') != "undefined") ? jQuery(this).attr('data-view') : 'calendar';

                if (initial_view == 'list') {
                    jQuery(this).find('.list-view').click();
                }
            });
        }
    });

}

function validateForm() {
    
    if ($('#txtEventName').val().length == 0) {
        alert('Please enter a name for this event.');
        return false;
    }

    //make sure end date is specified if all day event is not checked.
    if ($('#chkAllDayEvent').is(':checked') == false) {

        if ($('#picker-end-date').data("DateTimePicker").date() == null) {
            alert('Please select an end date and time since this is not an all day event.');
            return false;
        }
        
    }

    //If there are no event categories selected, return false and notify user.
    if (!$('#chkHiltonOffice').prop('checked') && !$('#chkMeetAndGreet').prop('checked') && !$('#chkIndustryEvent').prop('checked') &&
        !$('#chkLunchAndLearn').prop('checked') && !$('#chkHiltonConference').prop('checked')) {
        alert('Please select at least one event category');
        return false;
    }

    //If there are no user types selected, return false and notify user.
    if (!$('#chkEveryone').prop('checked') && !$('#chkPartners').prop('checked') && !$('#chkOwnersConsultants').prop('checked') &&
        !$('#chkEmployeesOnly').prop('checked')) {
        alert('Please select at least one user type');
        return false;
    }

    return true;
}