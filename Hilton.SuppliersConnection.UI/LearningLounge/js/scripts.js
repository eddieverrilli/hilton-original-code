(function($){
	
	/********************************************************************************
	**	VARIABLE DEFINITIONS
	********************************************************************************/
	
	window.isMobile = null;
	window.isTablet = null;	
	window.bp = null;	
	
	/********************************************************************************
	**	EVENT DEFINITIONS
	********************************************************************************/
	
	window.isMobile = isMobile();
	
	//$.browser = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
	
	$(window).resize(function() {
		checkBreakpoint();
	});
	
	/********************************************************************************
	**	FUNCTION DEFINITIONS
	********************************************************************************/
		
	function isMobile() {
		return ('ontouchstart' in document.documentElement);
	}
	
	function checkBreakpoint() {
				
		if ($(window).width() < 480) {
			// do something for small screens
			window.bp = 'xs';
		} else if ($(window).width() >= 420 &&  $(window).width() <= 768) {
			// do something for small screens
			window.bp = 'sm';
		}
		else if ($(window).width() >= 768 &&  $(window).width() <= 992) {
			// do something for medium screens
			window.bp = 'md';
		}
		else if ($(window).width() > 992 &&  $(window).width() <= 1200) {
			// do something for big screens
			window.bp = 'lg';
		}
		else  {
			// do something for huge screens
			window.bp = 'xl';
		}
		
		return window.bp;
	}
	
	checkBreakpoint();
	
    
})(jQuery);