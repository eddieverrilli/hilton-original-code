﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI.LearningLounge
{
    public partial class SupportCenter : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.BodyClass = "support-center";
        }
    }
}