﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LearningLounge/LearningLoungeMaster.Master" AutoEventWireup="true" CodeBehind="CustomTemplate.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LearningLounge.CustomTemplate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

	<!-- .container -->
    <div class="container subpage-1">
		
		<!-- .navigation-panel -->
		<div id="navigation-panel-wrapper" class="navigation-panel-mask">
			<div id="navigation-panel">
			    <div class="wrapper">
				    <ul>
                        <li><a href="Home.aspx" target="_blank">Home</a></li>
					    <li><a href="../Partners/RecommendedPartners.aspx" target="_blank">Browse Recommended Partners</a></li>
					    <li><a href="javascript:navigateMyAccount();">My Account</a></li>
					    <li><a href="calendar.aspx" target="_blank">Calendar</a></li>
					    <li><a href="javascript:meetNGreetTileClicked();">Meet & Greet</a></li>
					    <li><a href="adcdirectors.aspx" target="_blank">AD&C Contacts</a></li>
					    <li><a href="designinformation.aspx" target="_blank">Explore Design Information</a></li>
					    <li><a href="CEUCredits.aspx" target="_blank">CEU Credits</a></li>
					    <li><a href="mediacenter.aspx" target="_blank">Media Center & Gallery</a></li>
					    <li><a href="supportcenter.aspx" target="_blank">Support Center</a></li>
				    </ul>

				    <div id="close-navigation-panel"><a href="javascript:void(0);"><i class="fa fa-times"></i> CLOSE</a></div>
			    </div>
		    </div>
		</div>
		<!-- /.navigation-panel -->

        <div id="template1" style="display:none">
		    <div class="hero">
			    <img id="hero-image" class="img-responsive" src="#" />
		    </div>
		
		    <div class="row">
			    <div id="text1" class="col-xs-12 col-sm-8">
			    </div>
			    <div class="col-xs-12 col-sm-4">
				    <img id="callout-image" class="img-responsive featured-img" src="#" />
			    </div>
		    </div>
        </div>

        <div id="template2" style="display:none">
		    <div class="hero">
			    <img id="hero-image-template2" class="img-responsive" src="#" />
		    </div>
		
		    <div class="row">
			    <div id="text1-template2" class="col-xs-12 copy">
			    </div>
			    <div class="col-xs-12 col-sm-4">
				    <img id="callout-image1-template2" class="img-responsive featured-img" src="#" />
			    </div>
			    <div class="col-xs-12 col-sm-4">
				    <img id="callout-image2-template2" class="img-responsive featured-img" src="#" />
			    </div>
			    <div class="col-xs-12 col-sm-4">
				    <img id="callout-image3-template2" class="img-responsive featured-img" src="#" />
			    </div>
		    </div>
        </div>
	</div>
	<!-- /.container -->

    <!-- page specific js -->
    <script type="text/javascript">

        /********************************************************************************
        **	VARIABLE DEFINITIONS
        ********************************************************************************/
        var navPanel = $('#navigation-panel');
        var navPanelState = 'closed';
        var naPanelClosedPos = -300;
        var naPanelOpenPos = 0;

        
        (function ($) {
            //hide the nav trigger
            $('#nav-trigger').hide();

            // main navigation -->
            $('#nav-trigger').on('click', function () {
                manageNavigationPanel();
            })

            // close main navigation -->
            $('#close-navigation-panel').on('click', function () {
                manageNavigationPanel();
            })

            loadTemplateContent();

            function manageNavigationPanel() {
                if (navPanelState == 'closed') {
                    navPanelState = 'open';

                    $(navPanel).animate({
                        marginLeft: naPanelOpenPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                } else {
                    navPanelState = 'closed';

                    $(navPanel).animate({
                        marginLeft: naPanelClosedPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                }
            }

        })(jQuery);

        function loadTemplateContent() {
            var refId = getParameterByName('refId');

            if (refId) {
                
                $.ajax({
                    type: "POST",
                    url: "LearningLoungeService.asmx/GetTemplateContent",
                    data: "{refId: " + refId + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var contentData = data.d;
                        var item;

                        if (contentData.length > 0) {
                            if (contentData[0].TemplateId == 1) {
                                $('#template1').show();

                            } else if (contentData[0].TemplateId == 2) {
                                $('#template2').show();
                            }
                        }

                        for (var i = 0; i < contentData.length; i++) {
                            
                            if (contentData[i].TemplateId == 1) {

                                if (contentData[i].ContentName == "callout-image" || contentData[i].ContentName == "hero-image") {
                                    $('#' + contentData[i].ContentName).prop('src', 'data:image/jpg;base64,' + contentData[i].ContentImage);

                                } else if (contentData[i].ContentName == "text1") {
                                    $('#text1').append('<div class="templateText">' + contentData[i].ContentText + '</div>');


                                }

                            } else if (contentData[i].TemplateId == 2) {
                                if (contentData[i].ContentName.indexOf("callout-image") > -1 || contentData[i].ContentName == "hero-image-template2") {
                                    $('#' + contentData[i].ContentName).prop('src', 'data:image/jpg;base64,' + contentData[i].ContentImage);

                                } else if (contentData[i].ContentName == "text1-template2") {
                                    $('#text1-template2').append('<div class="templateText">' + contentData[i].ContentText + '</div>');


                                }
                            }                            
                        }

                    }
                });

            } else {
                document.writeln("Please provide a TileImageId or a GalleryImageId.")
            }
        }

        function getParameterByName(name) {
            name = name.toUpperCase().replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search.toUpperCase());
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        </script>
</asp:Content>
