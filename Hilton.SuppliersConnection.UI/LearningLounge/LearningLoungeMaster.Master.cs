﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI.LearningLounge
{
    public partial class LearningLoungeMaster : System.Web.UI.MasterPage
    {
        public string GoogleAnalyticsId
        {
            get {
                return ((PageBase)Page).GoogleAnalyticsId;
                
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    adminLinkButton.Visible = false;
                    barImage.Visible = false;

                    if (Session["AreAdminToolsOn"] == null)
                    {
                        Session["AreAdminToolsOn"] = "false";
                    }                    

                    User loggedInUser = (User)Session[SessionConstants.User];

                    if (loggedInUser != null)
                    {
                        bool isAdminMenu = false;
                        userNameLabel.Text = "Welcome, " + loggedInUser.FirstName + " " + loggedInUser.LastName;
                        loginLinkButton.Text = "Logout";

                        if (loggedInUser.UserTypeId == (int)UserTypeEnum.Administrator)
                        {
                            adminLinkButton.Visible = true;
                            barImage.Visible = true;                            
                        }

                        if (loggedInUser.UserTypeId == (int)UserTypeEnum.Partner)
                        {
                            if (loggedInUser.AssociatedPartners.Count > 1)
                            {
                                //userPartnersDropDown.Visible = true;
                                //lblCompany.Visible = true;
                                //userPartnersDropDown.DataSource = loggedInUser.AssociatedPartners;
                                //userPartnersDropDown.DataValueField = "PartnerId";
                                //userPartnersDropDown.DataTextField = "CompanyDescription";
                                //userPartnersDropDown.DataBind();
                            }
                            else
                            {
                                //userPartnersDropDown.Visible = false;
                                //lblCompany.Visible = false;
                            }

                        }
                        if (!string.IsNullOrWhiteSpace(loggedInUser.Company))
                        {
                            //userPartnersDropDown.SelectedValue = loggedInUser.Company;
                        }
                        //GetMenu(loggedInUser.UserTypeId, isAdminMenu, loggedInUser.UserId);
                    }
                    else
                    {
                        //GetMenu((int)UserTypeEnum.Visitor, false);
                    }
                }

                //Bind GoogleAnalyticsId
                Page.Header.DataBind();
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        ///triggered when button for login is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void LoginLinkButton_Click(Object sender, EventArgs e)
        {
            try
            {
                LogOn();
            }
            catch (ThreadAbortException)
            { }
        }

        /// <summary>
        ///to get the user session from cookie and if does not exists, creates a new cookie
        /// </summary>
        public void LogOn()
        {
            try
            {
                
                if (loginLinkButton.Text == "Logout")
                {
                    Session.Abandon();
                    loginLinkButton.Text = "Login";

                    HttpCookie workingCookie;
                    int cookiesCount = Request.Cookies.Count;
                    for (int i = 0; i < cookiesCount; i++)
                    {
                        workingCookie = Request.Cookies[i];

                        if (Server.HtmlEncode(workingCookie.Name) == "Lobbyopentoken" || (Server.HtmlEncode(workingCookie.Name) == "SessionID"))
                        {
                            //Update Cookie values
                            workingCookie.Value = "";
                            workingCookie.Expires = DateTime.Now.AddDays(-2d);
                            workingCookie.Domain = ".hilton.com";

                            //Add Cookie in Response collection
                            Response.Cookies.Add(workingCookie);
                        }
                    }
                    Response.Redirect("~/LearningLounge/Home.aspx", false);
                }
                else
                {
                    Session["AreAdminToolsOn"] = "false";

                    ((PageBase)Page).ClearSession(SessionConstants.User);
                    ((PageBase)Page).ClearSession(SessionConstants.AdminMenu);
                    ((PageBase)Page).ClearSession(SessionConstants.WebMenu);
                    ((PageBase)Page).OnQLogin(string.Empty);
                }
            }
            catch (ThreadAbortException)
            { }
        }

        /// <summary>
        /// Triggered when linked button for admin section is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AdminLinkButton_Click(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Page.ResolveUrl("~") + AppSettingConstants.AdminDashboardUrl, false);
            }
            catch (ThreadAbortException)
            {
            }
        }

        public string BodyClass
        {
            set
            {
                bodyTag.Attributes.Add("class", value);
            }
        }

        public String GetUserTypeId()
        {
            int userTypeId = -1;

            if (Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Session[SessionConstants.User];
                userTypeId = loggedInUser.UserTypeId;
            }
            return userTypeId.ToString();
        }

        public String AreAdminToolsOn()
        {
            if (Session["AreAdminToolsOn"].ToString().ToUpper() == "TRUE")
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }

}