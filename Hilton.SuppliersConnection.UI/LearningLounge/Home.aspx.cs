﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;
using System.Web.Services;

namespace Hilton.SuppliersConnection.UI.LearningLounge
{
    public partial class Home : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.BodyClass = "home";
        }

        /// <summary>
        ///triggered when button for login is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void LoginLinkButton_Click(Object sender, EventArgs e)
        {
            try
            {
                ((PageBase)Page).ClearSession(SessionConstants.User);
                ((PageBase)Page).ClearSession(SessionConstants.AdminMenu);
                ((PageBase)Page).ClearSession(SessionConstants.WebMenu);
                ((PageBase)Page).OnQLogin(string.Empty);
            }
            catch (System.Threading.ThreadAbortException)
            { }
        }

        protected void lnkPartnerIcon_Click(object sender, EventArgs e)
        {
            try
            {
                SetSession<string>(SessionConstants.PartnerId, txtPartnerId.Value);
                ClearSession(SessionConstants.FilterCriteria);
                ClearSession(SessionConstants.AllCascadeCategoryIds);
                ClearSession(SessionConstants.ProductImageBytes);
                Response.Redirect("~/Partners/PartnerProfile.aspx", false);
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception exception)
            {

            }
        }

    }
}