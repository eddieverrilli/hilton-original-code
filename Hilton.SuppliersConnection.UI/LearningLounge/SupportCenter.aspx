﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LearningLounge/LearningLoungeMaster.Master" AutoEventWireup="true" CodeBehind="SupportCenter.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LearningLounge.SupportCenter" %>

<%@ MasterType VirtualPath="~/LearningLounge/LearningLoungeMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/tinymce/tinymce.min.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

    <!-- modals -->
    <div id="edit-modal" class="modal fade has-top-border">
        <div class="modal-dialog" style="width: 470px">
            <div class="modal-content">
                <div class="modal-border">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-row orange">
                        <div class="row-header" style="margin-top: 5px;">Enter the Title</div>
                        <div class="row" style="margin-top: 5px;">
                            <textarea id="txtTitle" style="width: 89%; margin-left: 25px;"></textarea>
                        </div>
                        <div class="row-header" style="margin-top: 5px;">Select the Title Color</div>
                        <div class="row" style="margin-top: 5px;">
                            <select id="cboTitleColor" style="width: 89%; margin-left: 25px; color: black;">
                                <option value="none">Select Color</option>
                                <option value="dark-blue">Dark Blue</option>
                                <option value="light-blue">Light Blue</option>
                                <option value="light-orange">Light Orange</option>
                                <option value="dark-orange">Dark Orange</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-header" style="margin-top: 5px;">Enter the FAQ Description</div>
                    <div class="modal-row orange" style="margin-top: 5px;">
                        <textarea id="mytextarea">Hello, World!</textarea>
                    </div>
                    <div class="modal-row orange" style="margin-left: 25px;">
                        <div class="checkbox">
                            '																		
							<input id="chkItemEnabled" type="checkbox" checked="" />
                            <label for="chkItemEnabled">
                                Enabled
                            </label>
                        </div>
                    </div>
                    <div class="modal-row blue" style="margin-top: 25px; height: 70px;">
                        <div class="col-sm-12 col-md-6">
                            <button id="btnSave" type="button" class="btn btn-secondary" data-dismiss="modal">Save</button>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="$('#edit-modal').modal('hide');">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /#brands-modal -->

    <!-- .container -->
    <div class="container support-center">

        <!-- .navigation-panel -->
        <div id="navigation-panel-wrapper" class="navigation-panel-mask">
            <div id="navigation-panel">
                <div class="wrapper">
                    <ul>
                        <li><a href="Home.aspx" target="_blank">Home</a></li>
                        <li><a href="../Partners/RecommendedPartners.aspx" target="_blank">Browse Recommended Partners</a></li>
                        <li><a href="javascript:navigateMyAccount();">My Account</a></li>
                        <li><a href="calendar.aspx" target="_blank">Calendar</a></li>
                        <li><a href="javascript:meetNGreetTileClicked();">Meet & Greet</a></li>
                        <li><a href="adcdirectors.aspx" target="_blank">AD&C Contacts</a></li>
                        <li><a href="designinformation.aspx" target="_blank">Explore Design Information</a></li>
                        <li><a href="CEUCredits.aspx" target="_blank">CEU Credits</a></li>
                        <li><a href="mediacenter.aspx" target="_blank">Media Center & Gallery</a></li>
                        <li><a href="supportcenter.aspx" target="_blank">Support Center</a></li>
                    </ul>

                    <div class="toggle-wrapper" style="display: none;">
                        <div class="toggle-title">Admin Tools</div>
                        <div class="state active" data-toggle-state="on">OFF</div>
                        <div class="toggle toggle-light"></div>
                    </div>

                    <div id="close-navigation-panel"><a href="javascript:void(0);"><i class="fa fa-times"></i>CLOSE</a></div>
                </div>
            </div>
        </div>
        <!-- /.navigation-panel -->

        <div class="row">
            <h1>Support Center</h1>
        </div>
        <!-- .grid -->
        <div class="hero" data-js-module="hero">
            <div class="container secondary">
                <div class="row no-gutters">
                    <div class="col-sm-12 col-md-5 support-hero-container about">
                        <div class="support-center-info about">
                            <div class="title">
                                <img src="images/support-center/icon-about.png" />
                                <h3>ABOUT</h3>
                            </div>
                            <div id="divAbout">
                                <p>Hello</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-7 support-hero-container contact">
                        <div class="support-center-info contact">
                            <div class="title">
                                <img src="images/support-center/icon-about.png" />
                                <h3>CONTACT</h3>
                                <span class="note">Need help? Want to ask a question?<br>
                                    Contact us.
                                </span>
                            </div>
                            <div class="row" id="divContact">
                                <%--<div class="col-sm-12 col-md-6">
                                    <p class="headline">Technical Issues, Access Issues, Password Issues or Password Re-Set</p>

                                    <p>Click here to Live Chat with a Help Desk Specialist</p>

                                    <p>
                                        Phone Support<br>
                                        +1 901 748 7870 (The Americas)<br>
                                        +44 (0) 808 234 9532 (UK & Global)<br>
                                    </p>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <p><span class="headline">Other Suppliers' Connection Support</span><br>
                                        <a href="mailTo:suppliersconnection@hilton.com">suppliersconnection@hilton.com</a></p>
                                    <p><span class="headline">Vendor-Owner Complaints/Issues</span><br>
                                        Sherri Litano: <a href="mailTo:sherri.litano@hilton.com">sherri.litano@hilton.com</a></p>
                                    <p>
                                        <span class="headline">Owner and Partner Questions</span><br>
                                        Sherri Litano: <a href="mailTo:sherri.litano@hilton.com">sherri.litano@hilton.com</a><br>
                                        Kerry Burnett: <a href="mailTo:kerry.burnett@hilton.com">kerry.burnett@hilton.com</a><br>
                                        Jeff Lunceford: <a href="mailTo:jeff.lunceford@hilton.com">jeff.lunceford@hilton.com</a>
                                    </p>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button id="btnEditGeneral" type="button" onclick="showEditContactInfo();return false;" style="display:none;">Edit</button>
                        <button id="btnSaveGeneral" type="button" onclick="saveGeneralEdits();return false;" style="display:none;">Save</button>
                        <button id="btnCancelGeneral" type="button" onclick="cancelGeneralEdits();return false;" style="display:none;">Cancel</button>
                    </div>
                </div>

                <div class="page-subtitle">
                    <img id="question-mark" class="title-icon" src="images/support-center/question-mark.png" />
                    <h2>FAQ</h2>
                </div>

                <div id="gridFAQ" class="hero-grid clearfix">
                </div>
            </div>
        </div>
        <!-- /.grid -->

        <div id="divCreateLink" class="row" style="display: none">
            <div id="create-event"><a href="javascript:showEdit(-1);" target="_self"><i class="fa fa-plus" aria-hidden="true"></i>Add FAQ</a></div>
        </div>

        <%-- <div class="row" style="display:none;">
			<div class="col-md-12">
	            <form class="navbar-form navbar-search" role="search">
	                <div class="input-group">
	                    <input type="text" class="form-control" placeholder="Ask your question here!">
	                    <div class="input-group-btn">
	                        <button type="button" class="btn btn-primary btn-info">
	                            <span class="label-icon">Search</span>
	                        </button>
	                    </div>
	                </div>  
	            </form>
	        </div>
		</div>--%>
    </div>
    <!-- /.container -->

    <!-- global js include -->
    <script src="js/scripts.js"></script>

    <!-- page specific js -->
    <script type="text/javascript">

        /********************************************************************************
        **	VARIABLE DEFINITIONS
        ********************************************************************************/
        var navPanel = $('#navigation-panel');
        var navPanelState = 'closed';
        var naPanelClosedPos = -300;
        var naPanelOpenPos = 0;

        var aboutContainer = $('.support-center-info.about');
        var contactContainer = $('.support-center-info.contact');
        var $grid;
        var _faqItems = [];
        var _editFAQIndex;
        var _hasMasonryLoaded = false;

        function showEdit(faqIndex) {

            if (faqIndex > -1) {
                tinyMCE.get('mytextarea').setContent('<p class="hero-description">' + _faqItems[faqIndex].Description + '</p>');

                $("#txtTitle").val(_faqItems[faqIndex].Title);
                _editFAQIndex = faqIndex;

                if (_faqItems[faqIndex].Enabled) {
                    $('#chkItemEnabled').prop('checked', true);
                } else {
                    $('#chkItemEnabled').prop('checked', false);
                }

                $('#cboTitleColor').val(_faqItems[faqIndex].TitleColor);

            } else {
                _editFAQIndex = -1;
                tinyMCE.get('mytextarea').setContent('<p class="hero-description"></p>');
                $("#txtTitle").val('');
                $('#cboTitleColor').val("none");
            }

            $('#edit-modal').modal('show');
        }

        function showDelete(faqIndex) {
            if (confirm("Are you sure you want to delete this FAQ item?") == true) {
                $.ajax({
                    type: "POST",
                    url: "LearningLoungeService.asmx/DeleteFAQ",
                    data: "{faqId: " + _faqItems[faqIndex].FAQId + "}",
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var returnValue = data.d;
                        console.log(returnValue);

                        loadFAQs();
                    }
                });
            }
        }

        function loadFAQs() {

            clearFAQs();

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/GetSupportCenterFAQs",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var faqData = data.d;
                    var item;
                    var divFAQ = $('#gridFAQ');

                    for (var i = 0; i < faqData.length; i++) {
                        item = faqData[i];
                        _faqItems.push(item);

                        var divGridItem = $('<div>');
                        divGridItem.addClass("hero-grid__item " + item.TitleColor);

                        divGridItem.append("<div class='hero__title'>" + item.Title + "</div>");
                        divGridItem.append("<img class='hero__divider' src='images/support-center/divider.gif' />");
                        divGridItem.append("<p class='hero__description'>" + item.Description + "</p>");

                        //if the user is an admin and admin tools are enabled, show the edit button.
                        if (isUserAdmin() && _isAdminModeToggledOn) {
                            if (i > 0) {
                                divGridItem.append('<a href="javascript:moveItem(' + item.FAQId + ', \'LEFT\', \'FAQ\');" target="_self">< Left</a>');
                            }
                            divGridItem.append('<a href="javascript:showEdit(' + i + ')" style="margin-left:10px;">Edit</a>');
                            divGridItem.append('<a href="javascript:showDelete(' + i + ')" style="margin-left:10px">Delete</a>');

                            if (i < faqData.length - 1) {
                                divGridItem.append('<a href="javascript:moveItem(' + item.FAQId + ', \'RIGHT\', \'FAQ\');" target="_self" style="margin-left:10px;">Right ></a>');
                            }

                            if (item.Enabled == false) {
                                divGridItem.append('<span style="margin-left:10px">DISABLED</span>');
                            }
                        }

                        if (!_hasMasonryLoaded) {
                            divFAQ.append(divGridItem);
                        } else {
                            $('.hero-grid').append(divGridItem).masonry('appended', divGridItem);
                        }

                        
                    }

                    if (!_hasMasonryLoaded) {
                        // init Masonry grid -->
                        if ($grid == null) {
                            $grid = $('.hero-grid').masonry({
                                resize: true
                            });
                        }

                        setTimeout(function () {
                            $grid.masonry();
                        }, 100);

                        _hasMasonryLoaded = true;
                    } else {
                        $('.hero-grid').masonry('layout');
                    }


                }
            });

        }

        (function ($) {

            // main navigation -->
            $('#nav-trigger').on('click', function () {
                manageNavigationPanel();
            })

            // close main navigation -->
            $('#close-navigation-panel').on('click', function () {
                manageNavigationPanel();
            })

            tinymce.init({
                selector: '#mytextarea',
                plugins: 'link',
                toolbar: [
                  'undo redo | bold italic | link image | alignleft aligncenter alignright | bullist, numlist,'
                ],
                menubar: false,
                width: 400,
                height: 400
            });

            $('.toggle').toggles({
                drag: true, // allow dragging the toggle between positions
                click: true, // allow clicking on the toggle
                text: {
                    on: '', // text for the ON position
                    off: '' // and off
                },
                on: _isAdminModeToggledOn, // is the toggle ON on init
                animate: 250, // animation time (ms)
                easing: 'swing', // animation transition easing function
                checkbox: null, // the checkbox to toggle (for use in forms)
                clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
                width: 50, // width used if not set in css
                height: 20, // height if not set in css
                type: 'compact' // if this is set to 'select' then the select style toggle will be used
            });

            /********************************************************************************
			**	EVENT DEFINITIONS
			********************************************************************************/

            $(window).resize(function () {
                if (window.bp == 'lg' || window.bp == 'xl') {
                    $(aboutContainer).css('height', $(contactContainer).css('height'));
                } else {
                    $(aboutContainer).css('height', 'auto');
                }
            });

            $('#btnSave').on('click', function () {
                saveEdits();
            });

            /********************************************************************************
			**	FUNCTION DEFINITIONS
			********************************************************************************/

            function init() {
                $(window).trigger('resize');

                loadFAQs();
                getSupportCenerInfo();

                $(aboutContainer).css('height', '261px');
            }

            init();

            function manageNavigationPanel() {
                if (navPanelState == 'closed') {
                    navPanelState = 'open';

                    $(navPanel).animate({
                        marginLeft: naPanelOpenPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                } else {
                    navPanelState = 'closed';

                    $(navPanel).animate({
                        marginLeft: naPanelClosedPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                }
            }

            function saveEdits() {
                var description = tinyMCE.get('mytextarea').getContent();
                description = tinyMCE.DOM.encode(description);

                var title = tinyMCE.DOM.encode($("#txtTitle").val());
                var color = $("#cboTitleColor").val();
                var faqId;
                var enabled;

                if (_editFAQIndex > -1) {
                    faqId = _faqItems[_editFAQIndex].FAQId;
                    enabled = $('#chkItemEnabled').prop('checked');

                } else {
                    faqId = -1;        //a value of -1 tells the procedure that we are adding a new item.
                    enabled = true;
                }


                $.ajax({
                    type: "POST",
                    url: "LearningLoungeService.asmx/AddFAQ",
                    data: "{newFAQ: {'FAQId': " + faqId + ",'Title': '" + title + "','Description':'" + description + "','TitleColor':'" + color + "', 'Enabled':" + enabled + "}}",
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var returnValue = data.d;
                        console.log(returnValue);

                        loadFAQs();
                    }
                });
            }

        })(jQuery);

        function adminToolsToggled() {

            loadFAQs();

            if (_isAdminModeToggledOn) {
                $('#divCreateLink').show();
                $('#btnEditGeneral').show();

            } else {
                $('#divCreateLink').hide();
                $('#btnEditGeneral').hide();
            }

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/ToggleAdminTools",
                data: "{isOn: " + _isAdminModeToggledOn + "}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                }
            });
        }

        function showEditContactInfo() {
            if (tinymce.editors['divAbout'] == null) {
                tinymce.init({
                    selector: '#divAbout',
                    plugins: 'link',
                    toolbar: [
                      'undo redo | bold italic | link image | alignleft aligncenter alignright | bullist, numlist,'
                    ],
                    menubar: false,
                    width: 400,
                    height: 200
                });

            } else {
                tinymce.editors['divAbout'].show();
            }

            if (tinymce.editors['divContact'] == null) {
                tinymce.init({
                    selector: '#divContact',
                    plugins: 'link code',
                    toolbar: [
                      'undo redo | bold italic | link image | alignleft aligncenter alignright | bullist, numlist, code'
                    ],
                    menubar: false,
                    width: 400,
                    height: 200
                });

            } else {
                tinymce.editors['divContact'].show();
            }

            $('#btnEditGeneral').hide();
            $('#btnSaveGeneral').show();
            $('#btnCancelGeneral').show();

            $(aboutContainer).css('height', '400px');
            $(contactContainer).css('height', '400px');

        }

        function cancelGeneralEdits() {
            $('#btnEditGeneral').show();
            $('#btnSaveGeneral').hide();
            $('#btnCancelGeneral').hide();

            tinymce.editors['divAbout'].hide();
            tinymce.editors['divContact'].hide();
            $(aboutContainer).css('height', '261px');
            $(contactContainer).css('height', '261px');
        }

        function saveGeneralEdits() {
            $('#btnEditGeneral').show();
            $('#btnSaveGeneral').hide();
            $('#btnCancelGeneral').hide();

            var about = tinyMCE.get('divAbout').getContent();
            var contactInfo = tinyMCE.get('divContact').getContent();
            contactInfo = tinyMCE.DOM.encode(contactInfo);

            var info = {};
            info.About = about;
            info.ContactInfo = contactInfo;

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/UpdateSupportCenterInfo",
                data: "{info: " + JSON.stringify(info) + "}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var returnValue = data.d;
                    console.log(returnValue);

                }
            });

            tinymce.editors['divAbout'].hide();
            tinymce.editors['divContact'].hide();
            $(aboutContainer).css('height', '261px');
            $(contactContainer).css('height', '261px');
        }

        function getSupportCenerInfo() {
            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/GetSupportCenterInfo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var info = data.d;

                    $('#divAbout').html(info.About);
                    $('#divContact').html(info.ContactInfo);

                }
            });
        }

        function moveItem(id, direction, type) {
            var increment;

            if (direction == "LEFT") {
                increment = -1;
            } else {
                increment = 1;
            }

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/UpdateDisplayOrder",
                data: "{id: " + id + ", itemType: '" + type + "', increment: " + increment + "}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var returnValue = data.d;

                    loadFAQs();
                }
            });

        }

        function clearFAQs() {
            $('.hero-grid').children('div').each(function () {
                    //this.remove();
                    $('.hero-grid').masonry('remove', this);
            });
        }
    </script>

</asp:Content>
