﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LearningLounge/LearningLoungeMaster.Master" AutoEventWireup="true" CodeBehind="MediaCenter.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LearningLounge.MediaCenter" %>

<%@ MasterType VirtualPath="~/LearningLounge/LearningLoungeMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/lightbox.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

    <div id="edit-media-item" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-border">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                        <div class="modal-header-col col1">&nbsp;</div>
                        <div class="modal-header-col col2">
                            <h4 class="modal-title"><span id="lblCreateEditDupTitle" class="light">Create/Edit</span><span style="margin-left: 10px">Media Content</span></h4>
                        </div>
                        <div class="modal-header-col col3">&nbsp;</div>
                    </div>
                    <div class="modal-body">
                        <p class="body-description">Fill out the form below to edit this media item.</p>
                        <div class="row">
                            <div role="form">
                                <div class="form-group">
                                    <select id="selectContentType" name="selectContentType" style="height: 35px; color: black;" onchange="selectContentType_change(this);return false;">
                                        <option value="none" selected="selected">Select Content Type</option>
                                        <option value="uploaded-image">Upload Image</option>
                                        <option value="online-image">Online Image</option>
                                        <%--<option value="online-image">Embedded Video</option>--%>
                                    </select>
                                </div>
                                <div id="divImageContent" style="display: none;">
                                    <div id="media-image-container" style="border: 1px dotted #666666; min-height: 300px; background-size: 800px; background-repeat: no-repeat;"></div>
                                    <div id="image-upload-wrapper">
                                        <div id="uploader-text"></div>
                                        <p class="input-p centered">Acceptable image formats are .jpg or .png & the recommended image size is 1170 x 372.</p>
                                        <label class="btn btn-secondary btn-file">
                                            Browse
                                            <input id="fileUpload" type="file" style="display: none;" runat="server" onchange="previewSelectedImage($(this));" />
                                        </label>
                                    </div>
                                </div>
                                <div id="divEmbeddedCode" class="form-group" style="display: none;">
                                    <textarea id="txtEmbeddedObjectCode" name="txtEmbeddedObjectCode" placeholder="Enter Embedded HTML Here">Enter Text Here</textarea>
                                </div>
                                <div id="divOnlineImageURL" class="form-group" style="display: none;">
                                    <input id="txtOnlineImageURL" name="txtOnlineImageURL" type='text' class="form-control" placeholder="Image URL" style="width: 400px" />
                                </div>
                                <div class="form-group">
                                    <input id="txtCaption" name="txtCaption" type='text' class="form-control" placeholder="Caption" style="width: 400px" />
                                </div>
                                <div class="form-group">
                                    <input id="txtKeywords" name="txtKeywords" type='text' class="form-control" placeholder="Keywords" style="width: 400px" />
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input id="chkEnabled" name="chkEnabled" type="checkbox" value="enabled" />Enabled</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnSubmit" type="submit" class="btn btn-primary submit" onclick="btnSubmit_Click();">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <h1>Media Center & Gallery</h1>
        </div>

        <!-- .navigation-panel -->
        <div id="navigation-panel-wrapper" class="navigation-panel-mask" style="top:0px">
            <div id="navigation-panel">
                <div class="wrapper">
                    <ul>
                        <li><a href="Home.aspx" target="_blank">Home</a></li>
                        <li><a href="../Partners/RecommendedPartners.aspx" target="_blank">Browse Recommended Partners</a></li>
                        <li><a href="javascript:navigateMyAccount();">My Account</a></li>
                        <li><a href="calendar.aspx" target="_blank">Calendar</a></li>
                        <li><a href="javascript:meetNGreetTileClicked();">Meet & Greet</a></li>
                        <li><a href="adcdirectors.aspx" target="_blank">AD&C Contacts</a></li>
                        <li><a href="designinformation.aspx" target="_blank">Explore Design Information</a></li>
                        <li><a href="CEUCredits.aspx" target="_blank">CEU Credits</a></li>
                        <li><a href="mediacenter.aspx" target="_blank">Media Center & Gallery</a></li>
                        <li><a href="supportcenter.aspx" target="_blank">Support Center</a></li>
                    </ul>

                    <div class="toggle-wrapper" style="display: none;">
                        <div class="toggle-title">Admin Tools</div>
                        <div class="state active" data-toggle-state="on">OFF</div>
                        <div class="toggle toggle-light"></div>
                    </div>

                    <div id="close-navigation-panel"><a href="javascript:void(0);"><i class="fa fa-times"></i>CLOSE</a></div>
                </div>
            </div>
        </div>
        <!-- /.navigation-panel -->

        <div id="gallery-filters">
            <div class="navbar-form navbar-search" role="search">

                <div class="input-group inner-addon left-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input id="txtSearch" type="text" class="form-control" placeholder="Smart Search" />
                </div>

            </div>
        </div>

        <div class="row">

            <div id="gridContainer" class="grid">
                <div id="divGridSizer" class="grid-sizer"></div>
            </div>

        </div>

        <div class="row">
            <div id="divCreateMedia" style="display: none;"><a href="javascript:showCreateMedia();" target="_self">Add Media Content</a></div>
        </div>
        <!-- /.row -->

    </div>

    <input type="hidden" id="txtEditId" name="txtEditId" />
    <input type="hidden" id="txtUploadType" name="txtUploadType" value="MEDIACENTER" />  <%--Set to Gallery or Tile to tell the handler how to process the post--%>

    <iframe id="upload-iframe" name="upload-iframe" src="" style="display:none;" onload="uploadComplete();"></iframe>

    <!-- global js include -->
    <script src="js/scripts.js"></script>

    <script src="js/lightbox.js"></script>

    <script>
        var _mediaContentItems = [];
        var _hasMasonryLoaded = false;

        (function ($) {
            var navPanel = $('#navigation-panel');
            var navPanelState = 'closed';
            var naPanelClosedPos = -300;
            var naPanelOpenPos = 0;
            var _hasMasonryLoaded = false;

            // main navigation -->
            $('#nav-trigger').on('click', function () {
                manageNavigationPanel();
            })

            // close main navigation -->
            $('#close-navigation-panel').on('click', function () {
                manageNavigationPanel();
            })

            //set the target and action of the form to handle document uploads.
            $('#form1').attr("target", "upload-iframe");
            $('#form1').attr("action", "file-upload.ashx");

            $('.toggle').toggles({
                drag: true, // allow dragging the toggle between positions
                click: true, // allow clicking on the toggle
                text: {
                    on: '', // text for the ON position
                    off: '' // and off
                },
                on: _isAdminModeToggledOn, // is the toggle ON on init
                animate: 250, // animation time (ms)
                easing: 'swing', // animation transition easing function
                checkbox: null, // the checkbox to toggle (for use in forms)
                clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
                width: 50, // width used if not set in css
                height: 20, // height if not set in css
                type: 'compact' // if this is set to 'select' then the select style toggle will be used
            });

            if (isUserAdmin() && _isAdminModeToggledOn) {
                $('#divCreateMedia').show();
            }

            function manageNavigationPanel() {
                if (navPanelState == 'closed') {
                    navPanelState = 'open';

                    $(navPanel).animate({
                        marginLeft: naPanelOpenPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                } else {
                    navPanelState = 'closed';

                    $(navPanel).animate({
                        marginLeft: naPanelClosedPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                }
            }

            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,
                'showImageNumberLabel': false,
                'fadeDuration': 1000,
                'resizeDuration': 1000,
                'positionFromTop': 0,
                'disableScrolling': true
            })

            $('#txtSearch').keyup(function (event) {
                filterItems();
            });

            loadMediaCenter();

        })(jQuery);

        function showDelete(mediaCenterId) {
            if (confirm("Are you sure you want to delete this item?") == true) {
                $.ajax({
                    type: "POST",
                    url: "LearningLoungeService.asmx/DeleteMediaCenterItem",
                    data: "{mediaCenterId: " + mediaCenterId + "}",
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var returnValue = data.d;
                        console.log(returnValue);

                        loadMediaCenter();
                    }
                });
            }
        }

        function loadMediaCenter() {

            clearMediaGrid();

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/GetMediaCenterItems",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var mediaCenterItems = data.d;
                    var item;
                    var divGridContainer = $('#gridContainer');
                    var img;
                    var href;
                    var adminLinks;

                    for (var i = 0; i < mediaCenterItems.length; i++) {
                        item = mediaCenterItems[i];

                        if (item.ContentType == "uploaded-image") {
                            img = '<img id="imgid'  + item.MediaCenterId + '" src="data:image/jpg;base64,' + item.ContentImage + '" />';
                            href = 'data:image/jpg;base64,' + item.ContentImage;

                        } else if (item.ContentType == "online-image") {
                            img = '<img id="imgid' + item.MediaCenterId + '" src="' + item.ContentText + '" />';
                            href = item.ContentText;
                        }

                        //if the user is an admin and admin tools are enabled, show the admin options.
                        adminLinks = "";
                        if (isUserAdmin() && _isAdminModeToggledOn) {
                            if (i > 0) {
                                adminLinks = '<a href="javascript:moveItem(' + item.MediaCenterId + ', \'LEFT\', \'MEDIACENTER\');" target="_self">< Left</a>';
                            }

                            adminLinks += '<a href="javascript:showEdit(' + item.MediaCenterId + ')" style="margin-left:10px;">Edit</a>';
                            adminLinks += '<a href="javascript:showDelete(' + item.MediaCenterId + ')" style="margin-left:10px;">Delete</a>';

                            if (i < mediaCenterItems.length - 1) {
                                adminLinks += '<a href="javascript:moveItem(' + item.MediaCenterId + ', \'RIGHT\', \'MEDIACENTER\');" target="_self" style="margin-left:10px;">Right ></a>';
                            }
                        }

                        var opacity = "";
                        if (item.Enabled == false) {
                            opacity = 'disabled"';
                        }

                        var divString = '<div class="grid-item ' + opacity + '" id="gridItem' + item.MediaCenterId + '">' +
                            '<a href="' + href + '" class="example-image-link" data-lightbox="example-set" data-title="' + item.Caption + '" title="' + item.Caption + '">' +
                                img + '</a>' +
                                '<div>' + adminLinks + '</div>' +
                        '</div>';

                        if (!_hasMasonryLoaded) {
                            divGridContainer.append(divString);

                        } else {
                            var $content = $(divString);
                            $('.grid').append($content).masonry('appended', $content);
                        }

                        _mediaContentItems.push({ MediaCenterId: item.MediaCenterId, Caption: item.Caption, ContentText: item.ContentText, Enabled: item.Enabled, ContentType: item.ContentType, Keywords: item.Keywords });

                    }

                    if (!_hasMasonryLoaded) {
                        // init Masonry
                        var $grid = $('.grid').masonry({
                            itemSelector: '.grid-item',
                            percentPosition: true,
                            columnWidth: '.grid-sizer'
                        });

                        //$('.grid').masonry('layout');

                        $(window).resize(function () {
                            $grid.masonry();
                        });

                        _hasMasonryLoaded = true;

                    } else {
                        $('.grid').masonry('layout');
                    }
                }
            });

        }

        function clearMediaGrid() {

            $('#gridContainer').children('div').each(function () {
                if (this.id != "divGridSizer") {
                    //this.remove();
                    $('.grid').masonry('remove', this);
                }
            });
        }

        function selectContentType_change(control) {

            switch (control.value) {
                case "uploaded-image":
                    $('#divImageContent').show();
                    $('#divEmbeddedCode').hide();
                    $('#divOnlineImageURL').hide();
                    break;

                case "online-image":
                    $('#divImageContent').hide();
                    $('#divEmbeddedCode').hide();
                    $('#divOnlineImageURL').show();
                    break;

                case "Embedded Video":
                    $('#divImageContent').hide();
                    $('#divEmbeddedCode').show();
                    $('#divOnlineImageURL').hide();
                    break;
            }
        }

        function showCreateMedia() {
            //Set the txtGalleryEditId form control to -1 indicating that it is an insert.
            $('#txtEditId').val(-1);

            $('#edit-media-item').modal('show');
        }

        function btnSubmit_Click() {
            $('#edit-media-item').modal('hide');
        }

        function adminToolsToggled() {
            loadMediaCenter();

            if (_isAdminModeToggledOn) {
                $('#divCreateMedia').show();
            } else {
                $('#divCreateMedia').hide();
            }

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/ToggleAdminTools",
                data: "{isOn: " + _isAdminModeToggledOn + "}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                }
            });
        }

        function showEdit(id) {

            //Get the background image of the slider whose edit button was clicked.
            var src = $('#imgid' + id).prop('src');

            //Set the background image of the slider image preview img element on the edit form.
            var image_holder = $('#media-image-container');
            image_holder.empty();
            $('<img />', {
                'src': src,
                'class': 'thumb-image',
                'height': '300px'
            }).appendTo(image_holder);
            //$('#media-image-container').css('background-image', src);

            //Assigning a valid number to the txtGalleryEditId form control 
            $('#txtEditId').val(id);

            //Loop through the array that is storing the gallery data and find
            //the instance that matches id.
            for (var j = 0; j < _mediaContentItems.length; j++) {

                if (_mediaContentItems[j].MediaCenterId == id) {

                    //Set the controls on the edit form equal to the appropriate values.            
                    $('#txtOnlineImageURL').val(_mediaContentItems[j].ContentText);
                    $('#selectContentType').val(_mediaContentItems[j].ContentType);
                    $('#txtCaption').val(_mediaContentItems[j].Caption);
                    $('#txtKeywords').val(_mediaContentItems[j].Keywords);
                    $('#chkEnabled').prop('checked', _mediaContentItems[j].Enabled == true);

                    if (_mediaContentItems[j].ContentType == 'uploaded-image') {
                        $('#divImageContent').show();
                        $('#divEmbeddedCode').hide();
                        $('#divOnlineImageURL').hide();

                    } else if (_mediaContentItems[j].ContentType == 'online-image') {
                        $('#divImageContent').hide();
                        $('#divEmbeddedCode').hide();
                        $('#divOnlineImageURL').show();
                    }
                    break;
                }
            }

            $('#edit-media-item').modal('show');

        }

        function previewSelectedImage(fileUpload) {
            var imgPath = fileUpload[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $('#media-image-container');

            if (extn == 'png' || extn == 'jpg') {

                if (typeof (FileReader) != 'undefined') {

                    image_holder.empty();

                    var reader = new FileReader();
                    reader.onload = function (e) {

                        //Clear the background image of the slider image preview img element on the edit form.
                        image_holder.css('background-image', '')

                        $('<img />', {
                            'src': e.target.result,
                            'class': 'thumb-image',
                            'height': '300px'
                        }).appendTo(image_holder);
                    }
                    image_holder.show();
                    reader.readAsDataURL(fileUpload[0].files[0]);

                } else {
                    console.log('Your browser does not support FileReader.');
                }
            } else {
                console.log('Please select only .png images');
            }
        }

        function filterItems() {
            
            var textSearch = $('#txtSearch').val();
            
            for (var j = 0; j < _mediaContentItems.length; j++) {
                item = _mediaContentItems[j];

                if (item.Caption.toLowerCase().indexOf(textSearch.toLowerCase()) > -1 || item.Keywords.toLowerCase().indexOf(textSearch.toLowerCase()) > -1) {
                    $('#gridItem' + item.MediaCenterId).show();
                } else {
                    $('#gridItem' + item.MediaCenterId).hide();
                }

            }

            $('.grid').masonry('layout');
        }

        function moveItem(id, direction, type) {
            var increment;

            if (direction == "LEFT") {
                increment = -1;
            } else {
                increment = 1;
            }

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/UpdateDisplayOrder",
                data: "{id: " + id + ", itemType: '" + type + "', increment: " + increment + "}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var returnValue = data.d;

                    loadMediaCenter();
                }
            });

        }

        function uploadComplete() {
            if ($('#upload-iframe').contents().find("body")[0].innerText.length > 0) {
                alert($('#upload-iframe').contents().find("body")[0].innerText);

                location.reload();
            }
        }
    </script>
    <!-- /js -->

</asp:Content>
