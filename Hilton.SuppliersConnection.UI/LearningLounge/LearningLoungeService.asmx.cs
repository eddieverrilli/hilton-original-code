﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Constants;
using System.Web.Script.Serialization;

namespace Hilton.SuppliersConnection.UI.LearningLounge
{
    /// <summary>
    /// Summary description for LearningLoungeService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class LearningLoungeService : System.Web.Services.WebService
    {
        private ILearningLoungeManager _learningloungeManager;
        private int _defaultUserTypeId = 5;   //Always change this to -1 before checking in.  5=admin, -1=public.

        [WebMethod(EnableSession = true)]
        public List<LearningLoungeGallery> GetGallery()
        {
            int userTypeId = _defaultUserTypeId;

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                userTypeId = loggedInUser.UserTypeId;
            }

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetGalleryDetails(userTypeId);
        }


        [WebMethod(EnableSession = true)]
        public List<LearningLoungeTile> GetTiles()
        {
            int userTypeId = _defaultUserTypeId;

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                userTypeId = loggedInUser.UserTypeId;
            }

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetTileGridDetails(userTypeId);
        }

        [WebMethod(EnableSession = true)]
        public List<LearningLoungeFAQ> GetSupportCenterFAQs()
        {
            int userTypeId = _defaultUserTypeId;

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                userTypeId = loggedInUser.UserTypeId;
            }

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetSupportCenterFAQs(userTypeId);
        }

        [WebMethod(EnableSession = true)]
        public List<LearningLoungeCalendarEvent> GetCalendarEvents()
        {
            int userTypeId = _defaultUserTypeId;

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                userTypeId = loggedInUser.UserTypeId;
            }

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetCalendarEvents(userTypeId);
        }

        [WebMethod(EnableSession = true)]
        public List<LearningLoungeDesignInfo> GetDesignInfo(int designInfoId)
        {

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetLearningLoungeDesignInfo(designInfoId);
        }

        [WebMethod(EnableSession = true)]
        public String AddFAQ(LearningLoungeFAQ newFAQ)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    newFAQ.Description = HttpUtility.HtmlDecode(newFAQ.Description);
                    newFAQ.Title = HttpUtility.HtmlDecode(newFAQ.Title);
                    return _learningloungeManager.UpsertFAQ(newFAQ);
                }
                
            }

            return "You must be logged in as an administrator to perform this action";
        }

        [WebMethod(EnableSession = true)]
        public String DeleteFAQ(int faqId)
        {
            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.DeleteFAQ(faqId);
                }

            }

            return "You must be logged in as an administrator to perform this action";

        }

        [WebMethod(EnableSession = true)]
        public String DeleteGalleryImage(int galleryImageId)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.DeleteGalleryImage(galleryImageId);
                }

            }

            return "You must be logged in as an administrator to perform this action";
            
        }

        [WebMethod(EnableSession = true)]
        public String DeleteTileImage(int tileImageId)
        {
            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.DeleteTileImage(tileImageId);
                }

            }

            return "You must be logged in as an administrator to perform this action";
        }

        [WebMethod(EnableSession = true)]
        public LearningLoungeMap GetMapInfo(String mapName)
        {

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            List<LearningLoungeDirector> allContacts = _learningloungeManager.GetContactInfo(mapName);

            LearningLoungeMap mapMain = new LearningLoungeMap();
            SetMapMainInfo(mapName, ref mapMain);

            LearningLoungeMapDetail mapDetail = new LearningLoungeMapDetail();
            SetMapDetail(mapName, ref mapDetail);

            mapDetail.main_contacts = GetMainContacts(allContacts);

            //The main_contacts.label is the map title directly underneath the map.  
            //For North America, Tru and Focused Service, this label should be Focused Service Architecture & Construction.
            //For North America, Full Service, this label should be Luxury & Full Service Architecture & Construction.
            //For all other maps, this label should be blank.
            if (mapName != "emea" && mapName != "latin-america-and-caribbean")
            {
                if (mapName == "full-service")
                {
                    mapDetail.main_contacts.label = "Luxury & Full Service Architecture & Construction";
                }
                else
                {
                    mapDetail.main_contacts.label = "Focused Service Architecture & Construction";
                }
            }
            else
            {
                mapDetail.main_contacts.label = "";
            }

            mapDetail.general_contacts = GetGeneralContacts(allContacts);            
            mapDetail.additional_contacts = GetAdditionalContacts(allContacts);
            mapDetail.sidebar_navigation = GetSidebarNavigation();
            mapDetail.sidebar_brands = GetSidebarBrands(mapName);
            mapDetail.sidebar_print_options = GetPrintableMapLinks(mapName);
            mapDetail.regional_directors = GetRegionalDirectors(allContacts);
            mapDetail.region_listbox = GetRegionListboxes(mapName);

            mapMain.map = new LearningLoungeMapDetail[] { mapDetail };

            return mapMain;
        }

        [WebMethod(EnableSession = true)]
        public String UpdateContact(LearningLoungeContact contact)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.UpdateContact(contact);
                }

            }

            return "You must be logged in as an administrator to perform this action";            
        }

        [WebMethod(EnableSession = true)]
        public List<LearningLoungeCEU> GetCEUCredits()
        {
            int userTypeId = _defaultUserTypeId;

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                userTypeId = loggedInUser.UserTypeId;
            }

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetCEUCredits(userTypeId);
        }

        [WebMethod(EnableSession = true)]
        public String AddCEU(LearningLoungeCEU ceu)
        {

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            String returnValue = _learningloungeManager.AddCEU(ceu);

            if (returnValue == "SUCCESS")
            {

            }

            return returnValue;
        }


        [WebMethod(EnableSession = true)]
        public String DeleteCEU(int ceuCreditId)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.DeleteCEU(ceuCreditId);
                }

            }

            return "You must be logged in as an administrator to perform this action";  

            
        }

        [WebMethod(EnableSession = true)]
        public String ApproveCEU(int ceuCreditId)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.ApproveCEU(ceuCreditId);
                }

            }

            return "You must be logged in as an administrator to perform this action"; 

            
        }

        [WebMethod(EnableSession = true)]
        public String AddMeeting(LearningLoungeMeeting meeting)
        {

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.AddMeeting(meeting);
        }

        [WebMethod(EnableSession = true)]
        public List<LearningLoungeMediaCenterItem> GetMediaCenterItems()
        {
            int userTypeId = _defaultUserTypeId;

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                userTypeId = loggedInUser.UserTypeId;
            }

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetMediaCenterItems(userTypeId);
        }

        [WebMethod(EnableSession = true)]
        public String DeleteMediaCenterItem(int mediaCenterId)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.DeleteMediaCenterItem(mediaCenterId);
                }

            }

            return "You must be logged in as an administrator to perform this action"; 

            
        }
         
        [WebMethod(EnableSession = true)]
        public String DeleteCalendarEvent(int eventId)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.DeleteCalendarEvent(eventId);
                }

            }

            return "You must be logged in as an administrator to perform this action"; 

            
        }

        [WebMethod()]
        public List<LearningLoungeTemplateContent> GetTemplateContent(int refId)
        {

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetTemplateContent(refId);
        }

        [WebMethod(EnableSession = true)]
        public String UpdateSupportCenterInfo(LearningLoungeSupportCenterInfo info)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    info.About = Server.HtmlDecode(info.About);
                    info.ContactInfo = Server.HtmlDecode(info.ContactInfo);

                    return _learningloungeManager.UpdateSupportCenterInfo(info);
                }

            }

            return "You must be logged in as an administrator to perform this action"; 

        }

        [WebMethod(EnableSession = true)]
        public LearningLoungeSupportCenterInfo GetSupportCenterInfo()
        {

            if (_learningloungeManager == null)
                _learningloungeManager = LearningLoungeManager.Instance;

            return _learningloungeManager.GetSupportCenterInfo();
        }

        [WebMethod(EnableSession = true)]
        public String UpdateDisplayOrder(int id, String itemType, int increment)
        {

            if (Context.Session != null && Context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)Context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId != 5)
                {
                    return "You must be logged in as an administrator to perform this action";
                }
                else
                {
                    if (_learningloungeManager == null)
                        _learningloungeManager = LearningLoungeManager.Instance;

                    return _learningloungeManager.UpdateDisplayOrder(id, itemType, increment);
                }

            }

            return "You must be logged in as an administrator to perform this action"; 

            
        }

        [WebMethod(EnableSession = true)]
        public String ToggleAdminTools(bool isOn)
        {
            Context.Session["AreAdminToolsOn"] = isOn.ToString();
            return "";
        }

        [WebMethod()]
        public List<LearningLoungeRecommendedPartner> GetPartnerImages()
        {
            var recommendedPartnerImages = UserManager.Instance.GetImages;
            var images = recommendedPartnerImages.Select(p => new LearningLoungeRecommendedPartner()
                {
                    PartnerId = Convert.ToInt32(p.PartnerId),
                    CompanyLogoImage = Convert.ToBase64String(p.CompanyLogoImage)
                });


            return images.ToList();

        }

        private void SetMapMainInfo(String mapName, ref LearningLoungeMap mapMain)
        {

            switch (mapName)
            {
                case "focused-service":
                    mapMain.name = "North America";
                    mapMain.subtitle = "North America Focused Service";
                    mapMain.description = "";
                    mapMain.nickname = "na";
                    mapMain.slug = "north-america";
                    mapMain.has_multiple_maps = true;
                    break;

                case "full-service":
                    mapMain.name = "North America";
                    mapMain.subtitle = "<span class=\"strong\">North America</span> Luxury & Full Service";
                    mapMain.description = "";
                    mapMain.nickname = "na";
                    mapMain.slug = "north-america";
                    mapMain.has_multiple_maps = true;
                    break;

                case "tru":
                    mapMain.name = "North America";
                    mapMain.subtitle = "<span class=\"strong\">North America</span> Focused Service";
                    mapMain.description = "";
                    mapMain.nickname = "na";
                    mapMain.slug = "north-america";
                    mapMain.has_multiple_maps = true;
                    break;

                case "emea":
                    mapMain.name = "Europe, Middle East & Africa";
                    mapMain.subtitle = "<span class=\"strong\">Europe, Middle East & Africa</span>";
                    mapMain.description = "";
                    mapMain.nickname = "emea";
                    mapMain.slug = "emea";
                    mapMain.has_multiple_maps = false;
                    break;

                case "latin-america-and-caribbean":
                    mapMain.name = "Latin America & Caribbean";
                    mapMain.subtitle = "<span class=\"strong\">Latin America & Caribbean</span>";
                    mapMain.description = "";
                    mapMain.nickname = "lac";
                    mapMain.slug = "latin-america-and-caribbean";
                    mapMain.has_multiple_maps = false;
                    break;
            }
        }

        private void SetMapDetail(String mapName, ref LearningLoungeMapDetail mapDetail)
        {

            switch (mapName)
            {
                case "focused-service":
                    mapDetail.name = "Focused Service";
                    mapDetail.slug = "focused-service";
                    mapDetail.description = "";
                    mapDetail.callout = "Click the map below to find your AD&C contact for";
                    mapDetail.subtitle = "Hampton, Hilton Garden Inn, Homewood Suites, Home2 Suites";
                    break;

                case "full-service":
                    mapDetail.name = "Luxury & Full Service";
                    mapDetail.slug = "full-service";
                    mapDetail.description = "";
                    mapDetail.callout = "Click the map below to find your AD&C contact for";
                    mapDetail.subtitle = "Curio, Hilton, Doubletree & Embassy Suites";
                    break;

                case "tru":
                    mapDetail.name = "Focused Service";
                    mapDetail.slug = "focused-service";
                    mapDetail.description = "";
                    mapDetail.callout = "Click the map below to find your AD&C contact for";
                    mapDetail.subtitle = "Tru by Hilton";
                    break;

                case "emea":
                    mapDetail.name = "";
                    mapDetail.slug = "";
                    mapDetail.description = "";
                    mapDetail.callout = "Click the map below to select your region.";
                    mapDetail.subtitle = "";
                    break;

                case "latin-america-and-caribbean":
                    mapDetail.name = "";
                    mapDetail.slug = "";
                    mapDetail.description = "";
                    mapDetail.callout = "Click the map below to select your region.";
                    mapDetail.subtitle = "";
                    break;
            }
        }

        private LearningLoungeContactShell GetMainContacts(List<LearningLoungeDirector> allContacts)
        {
            LearningLoungeContactShell shell = new LearningLoungeContactShell();
            shell.label = "";

            //var mainContacts = allContacts.Select(x => new LearningLoungeContact() { name = x.name, position = x.position, phone = x.phone });                 
            var mainContacts = from x in allContacts where x.ContactTypeId == 1 select new LearningLoungeContact() { ContactId = x.ContactId, ContactTypeId = x.ContactTypeId, name = x.name, position = x.position, phone = x.phone };
            shell.contacts = mainContacts.ToArray();

            return shell;
        }

        private LearningLoungeContactShell GetGeneralContacts(List<LearningLoungeDirector> allContacts)
        {
            LearningLoungeContactShell shell = new LearningLoungeContactShell();
            shell.label = "";

            var generalContacts = from x in allContacts
                                  where x.ContactTypeId == 2
                                  select new LearningLoungeContact()
                                  {
                                      ContactId = x.ContactId,
                                      ContactTypeId = x.ContactTypeId,
                                      name = x.name,
                                      position = x.position,
                                      phone = x.phone,
                                      region = x.region
                                  };

            shell.contacts = generalContacts.ToArray();

            return shell;
        }

        private LearningLoungeContactShell GetAdditionalContacts(List<LearningLoungeDirector> allContacts)
        {
            LearningLoungeContactShell shell = new LearningLoungeContactShell();
            shell.label = "";

            var additionalContacts = from x in allContacts
                                     where x.ContactTypeId == 3
                                     select new LearningLoungeContact()
                                     {
                                         ContactId = x.ContactId,
                                         ContactTypeId = x.ContactTypeId,
                                         name = x.name,
                                         position = x.position,
                                         phone = x.phone,
                                     };

            shell.contacts = additionalContacts.ToArray();

            return shell;
        }

        private LearningLoungeButtonShell GetSidebarNavigation()
        {
            LearningLoungeButtonShell shell = new LearningLoungeButtonShell();
            List<LearningLoungeButton> lst = new List<LearningLoungeButton>();
            LearningLoungeButton btn;

            btn = new LearningLoungeButton();
            btn.label = "Focused Service Map";
            btn.slug = "focused-service";
            lst.Add(btn);

            btn = new LearningLoungeButton();
            btn.label = "Full Service Map";
            btn.slug = "full-service";
            lst.Add(btn);

            btn = new LearningLoungeButton();
            btn.label = "Tru Map";
            btn.slug = "tru";
            lst.Add(btn);

            shell.buttons = lst.ToArray();

            return shell;
        }

        private LearningLoungeBrandShell GetSidebarBrands(String mapName)
        {
            LearningLoungeBrandShell shell = new LearningLoungeBrandShell();
            List<LearningLoungeButton> lst = new List<LearningLoungeButton>();
            LearningLoungeButton btn;

            switch (mapName)
            {
                case "full-service":
                    btn = new LearningLoungeButton();
                    btn.label = "Hilton Hotel & Resorts";
                    btn.slug = "hilton";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Doubletree by HILTON";
                    btn.slug = "doubletree";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Embassy Suites by HILTON";
                    btn.slug = "embassy";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Curio a Collection by HILTON";
                    btn.slug = "curio";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Conrad Hotel & Resorts";
                    btn.slug = "conrad";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Canopy by HILTON";
                    btn.slug = "canopy";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Waldorf Astoria Hotel & Resorts";
                    btn.slug = "waldorf-astoria";
                    lst.Add(btn);

                    break;

                case "focused-service":
                    btn = new LearningLoungeButton();
                    btn.label = "Hampton by HILTON";
                    btn.slug = "hampton";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Hilton Garden Inn";
                    btn.slug = "hilton-garden-inn";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Homewood Suites by HILTON";
                    btn.slug = "homewood";
                    lst.Add(btn);

                    btn = new LearningLoungeButton();
                    btn.label = "Home2 Suites by HILTON";
                    btn.slug = "home2";
                    lst.Add(btn);

                    break;

                case "tru":
                    btn = new LearningLoungeButton();
                    btn.label = "Tru by Hilton";
                    btn.slug = "tru";
                    lst.Add(btn);

                    break;
            }

            shell.brand = lst.ToArray();

            return shell;
        }

        private LearningLoungeDirectorShell GetRegionalDirectors(List<LearningLoungeDirector> allContacts)
        {
            LearningLoungeDirectorShell shell = new LearningLoungeDirectorShell();

            var directorContacts = from x in allContacts
                                   where x.ContactTypeId == 4
                                   select new LearningLoungeDirector()
                                   {
                                       ContactId = x.ContactId,
                                       ContactTypeId = x.ContactTypeId,
                                       name = x.name,
                                       position = x.position,
                                       phone = x.phone,
                                       region = x.region,
                                       region_slug = x.region_slug
                                   };

            shell.directors = directorContacts.ToArray();

            return shell;
        }

        private LearningLoungeRegionListbox GetRegionListboxes(String mapName)
        {
            LearningLoungeRegionListbox shell = new LearningLoungeRegionListbox();
            List<LearningLoungeListboxOption> lst = new List<LearningLoungeListboxOption>();
            LearningLoungeListboxOption option;

            shell.title = "Find your AD&C Contact by selecting your state below!";

            switch (mapName)
            {
                case "full-service":
                    option = new LearningLoungeListboxOption();
                    option.value = "northeast";
                    option.label = "Northeast";
                    option.region = "Northeast";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "northwest";
                    option.label = "Northwest";
                    option.region = "Northwest";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "southeast";
                    option.label = "Southeast";
                    option.region = "Southeast";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "southwest";
                    option.label = "Southwest";
                    option.region = "Southwest";
                    lst.Add(option);
                    break;

                case "focused-service":
                    option = new LearningLoungeListboxOption();
                    option.value = "AL";
                    option.label = "Alabama";
                    option.region = "AL-FL";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "AK";
                    option.label = "Alaska";
                    option.region = "AK-WA-OR-ID-UT";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "AZ";
                    option.label = "Arizona";
                    option.region = "AZ-NM";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "AR";
                    option.label = "Arkansas";
                    option.region = "OK-AR-LA";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "CA-top";
                    option.label = "California North";
                    option.region = "upperCA-HI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "CA-bottom";
                    option.label = "California South";
                    option.region = "NV-lowerCA";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "canada";
                    option.label = "Canada";
                    option.region = "Canada";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "CO";
                    option.label = "Colorado";
                    option.region = "MT-ND-MN-SD-WY-CO-NE-KS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "CT";
                    option.label = "Connecticut";
                    option.region = "ME-VT-NY-NH-MA-CT-RI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "DE";
                    option.label = "Delaware";
                    option.region = "WV-DE-NJ";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "DC";
                    option.label = "District Of Columbia";
                    option.region = "DC";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "FL";
                    option.label = "Florida";
                    option.region = "AL-FL";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "GA";
                    option.label = "Georgia";
                    option.region = "GA-MS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "HI";
                    option.label = "Hawaii";
                    option.region = "upperCA-HI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "ID";
                    option.label = "Idaho";
                    option.region = "AK-WA-OR-ID-UT";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "IL";
                    option.label = "Illinois";
                    option.region = "MI-IL-IN-OH";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "IN";
                    option.label = "Indiana";
                    option.region = "MI-IL-IN-OH";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "IA";
                    option.label = "Iowa";
                    option.region = "WI-IA-MO";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "KS";
                    option.label = "Kansas";
                    option.region = "MT-ND-MN-SD-WY-CO-NE-KS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "KY";
                    option.label = "Kentucky";
                    option.region = "NC-KY";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "LA";
                    option.label = "Louisiana";
                    option.region = "OK-AR-LA";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "ME";
                    option.label = "Maine";
                    option.region = "ME-VT-NY-NH-MA-CT-RI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "MD";
                    option.label = "Maryland";
                    option.region = "WV-DE-NJ";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "MA";
                    option.label = "Massachusetts";
                    option.region = "ME-VT-NY-NH-MA-CT-RI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "MI";
                    option.label = "Michigan";
                    option.region = "MI-IL-IN-OH";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "MN";
                    option.label = "Minnesota";
                    option.region = "MT-ND-MN-SD-WY-CO-NE-KS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "MS";
                    option.label = "Mississippi";
                    option.region = "GA-MS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "MT";
                    option.label = "Montana";
                    option.region = "MT-ND-MN-SD-WY-CO-NE-KS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "NE";
                    option.label = "Nebraska";
                    option.region = "MT-ND-MN-SD-WY-CO-NE-KS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "NV";
                    option.label = "Nevada";
                    option.region = "NV-lowerCA";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "NH";
                    option.label = "New Hampshire";
                    option.region = "ME-VT-NY-NH-MA-CT-RI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "NJ";
                    option.label = "New Jersey";
                    option.region = "NJ";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "NM";
                    option.label = "New Mexico";
                    option.region = "AZ-NM";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "MS";
                    option.label = "Mississippi";
                    option.region = "GA-MS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "NY";
                    option.label = "New York";
                    option.region = "ME-VT-NY-NH-MA-CT-RI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "NC";
                    option.label = "North Carolina";
                    option.region = "NC-KY";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "ND";
                    option.label = "North Dakota";
                    option.region = "MT-ND-MN-SD-WY-CO-NE-KS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "OH";
                    option.label = "Ohio";
                    option.region = "MI-IL-IN-OH";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "OK";
                    option.label = "Oklahoma";
                    option.region = "OK-AR-LA";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "OR";
                    option.label = "Oregon";
                    option.region = "AK-WA-OR-ID-UT";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "PA";
                    option.label = "Pennsylvania";
                    option.region = "PA-VA";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "RI";
                    option.label = "Rhode Island";
                    option.region = "ME-VT-NY-NH-MA-CT-RI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "SC";
                    option.label = "South Carolina";
                    option.region = "TN-SC";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "SD";
                    option.label = "South Dakota";
                    option.region = "MT-ND-MN-SD-WY-CO-NE-KS";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "TX";
                    option.label = "Texas North";
                    option.region = "TX";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "TX-bottom";
                    option.label = "Texas South";
                    option.region = "TX-bottom";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "TN";
                    option.label = "Tennessee";
                    option.region = "TN-SC";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "UT";
                    option.label = "Utah";
                    option.region = "AK-WA-OR-ID-UT";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "VT";
                    option.label = "Vermont";
                    option.region = "ME-VT-NY-NH-MA-CT-RI";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "VA";
                    option.label = "Virginia";
                    option.region = "PA-VA";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "WA";
                    option.label = "Washington";
                    option.region = "AK-WA-OR-ID-UT";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "WV";
                    option.label = "West Virginia";
                    option.region = "WV-DE-NJ";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "WI";
                    option.label = "Wisconsin";
                    option.region = "WI-IA-MO";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "WY";
                    option.label = "Wyoming";
                    option.region = "MT-ND-MN-SD-WY-CO-NE-KS";
                    lst.Add(option);

                    break;

                case "tru":
                    option = new LearningLoungeListboxOption();
                    option.value = "eastern";
                    option.label = "Eastern";
                    option.region = "Eastern";
                    lst.Add(option);

                    option = new LearningLoungeListboxOption();
                    option.value = "western";
                    option.label = "Western";
                    option.region = "Western";
                    lst.Add(option);

                    break;
            }

            shell.options = lst.ToArray();

            return shell;
        }

        private LearningLoungeButtonShell GetPrintableMapLinks(String mapName)
        {
            LearningLoungeButtonShell returnValue = new LearningLoungeButtonShell();
            List<LearningLoungeButton> listBtn = new List<LearningLoungeButton>();
            LearningLoungeButton btn = new LearningLoungeButton();

            switch (mapName)
            {
                case "emea":
                    btn.label = "Europe, Middle East & Africa";
                    btn.slug = "emea";
                    listBtn.Add(btn);
                    returnValue.buttons = listBtn.ToArray();
                    break;

                case "latin-america-and-caribbean":
                    btn.label = "Europe, Middle East & Africa";
                    btn.slug = "latin-america-and-caribbean";
                    listBtn.Add(btn);
                    returnValue.buttons = listBtn.ToArray();
                    break;
            }

            return returnValue;
        }
    }
}
