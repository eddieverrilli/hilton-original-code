﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LearningLounge/LearningLoungeMaster.Master" AutoEventWireup="true" CodeBehind="DesignInformation.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LearningLounge.DesignInformation" %>
<%@ MasterType VirtualPath="~/LearningLounge/LearningLoungeMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var _redirectURL = "";

        function showRedirectDialog(redirectURL) {
            _redirectURL = redirectURL;

            $('#brands-modal').modal('hide');

            $('#password-modal').modal('show');
            
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

	<!-- modals -->
	<div id="brands-modal" class="modal fade has-top-border">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-border">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
					<div id="carousel-brands" class="carousel slide">
				        <!-- .carousel-inner -->
						<div class="carousel-inner">

						</div>
						<!-- /.carousel-inner -->
						
				        <!-- .carousel-control -->
				        <a class="left carousel-control" href="#carousel-brands" data-slide="prev">
				            <span class="icon-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
				        </a>
				        <a class="right carousel-control" href="#carousel-brands" data-slide="next">
				            <span class="icon-next"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
				        </a>
				        <!-- /.carousel-control -->
				    </div>			  			
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /#brands-modal -->
	
	<div id="redirect-modal" class="modal fade has-top-border utility-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-border">
					<!--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>-->
		  			<div class="modal-body">
	  					<p>You are being re-directed to a password-protected page.<br>The username and password are <span class="regular">'hilton2016'</span>.</p>
	  					<div class="modal-row blue">
	  						<button type="button" class="btn btn-primary" data-dismiss="modal">CONTINUE</button>
	  					</div>
	  					<p class="note">No thanks, <a href="#" data-dismiss="modal">please take me back.</a></p>
		  			</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /#redirect-modal -->
	
	<div id="password-modal" class="modal fade has-top-border utility-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-border">
		  			<div class="modal-body">
	  					<p>You are being redirected to a password protected site.  <br />If you have the login credentials, click continue.  
                              If you need credentials, <br />please <a href="ADCDirectors.aspx" target="_blank">click here</a> to contact your AD&C Director.</p>
	  					<div class="modal-row blue">
	  						<button id="btnRedirect" type="button" class="btn btn-primary" data-dismiss="modal">GOT IT!</button>
	  					</div>
		  			</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /#password-modal -->

	<!-- .container -->
    <div class="container">

		<!-- .navigation-panel -->
		<div id="navigation-panel-wrapper" class="navigation-panel-mask">
			<div id="navigation-panel">
			    <div class="wrapper">
				    <ul>
                        <li><a href="Home.aspx" target="_blank">Home</a></li>
					    <li><a href="../Partners/RecommendedPartners.aspx" target="_blank">Browse Recommended Partners</a></li>
					    <li><a href="javascript:navigateMyAccount();">My Account</a></li>
					    <li><a href="calendar.aspx" target="_blank">Calendar</a></li>
					    <li><a href="javascript:meetNGreetTileClicked();">Meet & Greet</a></li>
					    <li><a href="adcdirectors.aspx" target="_blank">AD&C Contacts</a></li>
					    <li><a href="designinformation.aspx" target="_blank">Explore Design Information</a></li>
					    <li><a href="CEUCredits.aspx" target="_blank">CEU Credits</a></li>
					    <li><a href="mediacenter.aspx" target="_blank">Media Center & Gallery</a></li>
					    <li><a href="supportcenter.aspx" target="_blank">Support Center</a></li>
				    </ul>
				    <div id="close-navigation-panel"><a href="javascript:void(0);"><i class="fa fa-times"></i> CLOSE</a></div>
			    </div>
		    </div>
		</div>
		<!-- /.navigation-panel -->
        		
	    <div class="row no-gutters">		   		    
		    <h1>Design Information</h1>
		    <p class="page-description">Find all the information you need to design a Hilton Worldwide brand in a specific market. To view the standards, prototypes, design guides and other supporting documents for each Brand and each Region, please select the Brand below.</p>
	    </div>
	    <div id="brands-container" class="row">

	    </div>        
	</div>
	<!-- /.container -->	

    <!--   Hidden   -->
        <asp:TextBox ID="txtHiddenIsUserAuthenticated" runat="server" Style="display:none" />
    <!-- /.Hidden -->

    <!-- global js include -->
    <script src="js/scripts.js"></script>

    <!-- page specific js -->
    <script type="text/javascript">
        (function ($) {

            /********************************************************************************
			**	VARIABLE DEFINITIONS
			********************************************************************************/
            var navPanel = $('#navigation-panel');
            var navPanelState = 'closed';
            var naPanelClosedPos = -300;
            var naPanelOpenPos = 0;

            var activeBrand = null;
            var activeCarouselItem = null;
            
            /********************************************************************************
			**	EVENT DEFINITIONS
			********************************************************************************/
            // main navigation -->
            $('#nav-trigger').on('click', function () {
                manageNavigationPanel();
            })

            // close main navigation -->
            $('#close-navigation-panel').on('click', function () {
                manageNavigationPanel();
            })

            function manageNavigationPanel() {
                if (navPanelState == 'closed') {
                    navPanelState = 'open';

                    $(navPanel).animate({
                        marginLeft: naPanelOpenPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                } else {
                    navPanelState = 'closed';

                    $(navPanel).animate({
                        marginLeft: naPanelClosedPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                }
            }

            $('.modal-trigger').on('click', function () {
                var targetModal = $(this).attr('data-target');
                $(targetModal).modal('show');
            });

            $('#btnRedirect').on('click', function () {
                console.log(_redirectURL);
                window.open(_redirectURL);
            });

            /********************************************************************************
			**	FUNCTION DEFINITIONS
			********************************************************************************/

            function loadDesignInformation() {

                $.ajax({
                    type: "POST",
                    url: "LearningLoungeService.asmx/GetDesignInfo",
                    data: "{ designInfoId:-1 }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var designInfoData = data.d;
                        var active;
                        var designStudioVisibility;
                        var northAmericaEnableDisable;
                        var emeaEnableDisable;
                        var latinAmericaEnableDisable;
                        var asiaEnableDisable;
                        var item;

                        for (var i = 0; i < designInfoData.length; i++) {
                            item = designInfoData[i];
                            
                            if (i == 0) {
                                active = " active";
                            } else {
                                active = "";
                            }
                            
                            if (item.DesignStudioLink) {
                                designStudioVisibility = '';

                            } else {
                                designStudioVisibility = 'style="display:none;"';
                            }

                            if (item.NorthAmericaLink) {
                                northAmericaEnableDisable = '';

                            } else {
                                northAmericaEnableDisable = 'disabled=true';
                            }

                            if (item.LatinAmericaLink) {
                                latinAmericaEnableDisable = '';

                            } else {
                                latinAmericaEnableDisable = 'disabled=true';
                            }

                            if (item.EMEALink) {
                                emeaEnableDisable = '';

                            } else {
                                emeaEnableDisable = 'disabled=true';
                            }

                            if (item.AsiaPacificLink) {
                                asiaEnableDisable = '';

                            } else {
                                asiaEnableDisable = 'disabled=true';
                            }

                            $('.carousel-inner').append('<div id="' + item.Brand + '" class="item' + active + '">' +
								'<div class="modal-content-wrapper">' +
									'<div class="modal-header">' +
										'<div class="row">' +
											'<div class="col-sm-12 col-md-5 modal-brand-logo">' +
												'<img class="img-responsive" src="data:image/jpg;base64,' + item.Logo + '" alt="Hampton by Hilton"/>' +
											'</div>' +
											'<div class="col-sm-12 col-md-7 modal-brand-intro">' +
												'<p>' + item.Description + '</p>' +
												'<a href="' + item.PicsFactsLink + '" target="_blank">For photos, facts, opportunities, and other information for this brand, click here.</a>' +
											'</div>' +
										'</div>' +
						  			'</div>' +
						  			'<div class="modal-body">' +
							  			'<div class="modal-row orange">' +
						  					'<div class="row-header">Choose from the regions below to see design information specific to this brand.</div>' +
						  					'<div class="row">' +
							  					'<div class="col-sm-12 col-md-6">' +
							  						'<button type="button" onclick="return showRedirectDialog(\'' + item.NorthAmericaLink + '\');" class="btn btn-primary" data-territory="north-america" ' + northAmericaEnableDisable + '>NORTH AMERICA</button>' +
							  					'</div>' +
							  					'<div class="col-sm-12 col-md-6">' +
							  						'<button type="button" onclick="showRedirectDialog(\'' + item.AsiaPacificLink + '\'); return false;" class="btn btn-primary" data-territory="asia-pacific" ' + asiaEnableDisable + '>ASIA PACIFIC</button>' +
							  					'</div>' +
							  					'<div class="col-sm-12 col-md-6">' +
							  						'<button type="button" onclick="showRedirectDialog(\'' + item.LatinAmericaLink + '\'); return false;" class="btn btn-primary" data-territory="latin-america-caribbean" ' + latinAmericaEnableDisable + '>LATIN AMERICA & THE CARIBBEAN</button>' +
							  					'</div>' +
							  					'<div class="col-sm-12 col-md-6">' +
							  						'<button type="button" onclick="showRedirectDialog(\'' + item.EMEALink + '\'); return false;" class="btn btn-primary" data-territory="europe-middle-east-africa" ' + emeaEnableDisable + '>EUROPE/MIDDLE EAST & AFRICA</button>' +
							  					'</div>' +
						  					'</div>' +
							  			'</div>' +
							  			'<div class="modal-row blue" ' + designStudioVisibility + '>' +
											'<div class="row-header">Check out the Design Studio.</div>' +
						  					'<div class="row">' +
							  					'<div class="col-sm-12">' +
							  						'<button onclick="window.open(\'' + item.DesignStudioLink + '\'); return false;" class="btn btn-primary" data-territory="north-america">DESIGN STUDIO</button>' +
							  					'</div>' +
						  					'</div>' +
							  			'</div>' +
							  			'<div class="modal-row blue">' +
						  					'<div class="row">' +
							  					'<div class="col-sm-12">' +
							  						'<button onclick="window.open(\'ADCDirectors.aspx\'); return false;" class="btn btn-primary" data-territory="north-america">Contact Your Design Rep</button>' +
							  					'</div>' +
						  					'</div>' +
							  			'</div>' +
						  			'</div>' +
					  			'</div>' +
							'</div>');

                            $('#brands-container').append('' +
                                '<div class="col-xs-6 col-md-4 col-lg-3">' +
				                    '<div class="logo-container" data-brand="' + item.Brand + '">' +
					                    '<img class="img-responsive" src="data:image/jpg;base64,' + item.LogoThumbnail + '" alt="Hampton by Hilton"/>' +
				                    '</div>' +
			                    '</div>');
                        }

                        var brandLogos = $('#brands-container .logo-container');

                        // manage brand logo click -->
                        $(brandLogos).on('click', function () {
                            activeBrand = $(this).attr('data-brand');
                            activeCarouselItem = $(this).parent().index();

                            // manage logo active state -->
                            deactivateBrands();
                            $(this).find('img').addClass('active');

                            $('#carousel-brands.carousel').carousel(activeCarouselItem);
                            $('#brands-modal').modal('show');
                        });

                        $('#brands-modal').on('hidden.bs.modal', function () {
                            deactivateBrands();
                        });

                        // init carousel -->
                        $('#carousel-brands.carousel').carousel({
                            interval: false //stops carousel rotation
                        });

                        // manage the active brand -->
                        $('#carousel-brands.carousel').on('slide.bs.carousel', function (e) {
                            activeBrand = $(e.relatedTarget).attr('id');

                            deactivateBrands();
                            $('#brands-container').find("[data-brand='" + activeBrand + "']").find('img').addClass('active');

                        });
                    }
                });

            }

            function deactivateBrands() {
                $('.logo-container img').removeClass('active');
            }

            loadDesignInformation();

        })(jQuery);
    </script>

</asp:Content>
