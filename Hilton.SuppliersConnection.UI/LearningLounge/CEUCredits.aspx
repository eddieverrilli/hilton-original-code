﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LearningLounge/LearningLoungeMaster.Master" AutoEventWireup="true" CodeBehind="CEUCredits.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LearningLounge.CEUCredits" %>
<%@ MasterType VirtualPath="~/LearningLounge/LearningLoungeMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/imagesloaded.min.js"></script>
    <script src="js/CEUCredits.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

		<!-- modals -->
		<div id="request-modal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-border">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
							<div class="modal-header-col col1">&nbsp;</div>
							<div class="modal-header-col col2"><h4 class="modal-title"><span>&nbsp;&nbsp;&nbsp;&nbsp;CEU Request Form&nbsp;&nbsp;&nbsp;&nbsp;</span></h4></div>
							<div class="modal-header-col col3">&nbsp;</div>
			  			</div>
			  			<div class="modal-body">
				  			
				  			<div class="ceu-icon-share">
				  				<img src="images/ceu/share.png" alt="Share Your CE Presentation" />
				  			</div>
				  			<div class="ceu-intro">
					  			Do you have a CEU-accredited course? Submit a request to Hilton to have your program featured here!
				  			</div>
			  				<div class="row">
			  					<div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1">
					  				<div class="row">
						  				<div class="col-xs-12 col-sm-6">
							  				<div class="form-group">
							  					<input id="txtTitle" name="txtTitle" type="text" class="form-control" placeholder="TITLE" style="width:400px" />
							  				</div>
						  				</div>
					  				</div>
					  				<div class="form-group">
					  					<input id="txtCompany" name="txtCompany" type="text" class="form-control" placeholder="COMPANY" />
					  				</div>
					  				<div class="form-group">
					  					<input id="txtContact" name="txtContact" type="text" class="form-control" placeholder="CONTACT" />
					  				</div>
					  				<div class="row">
						  				<div class="col-xs-12 col-sm-6">
							  				<div class="form-group">
							  					<input id="txtPhone" name="txtPhone" type="text" class="form-control" placeholder="PHONE NUMBER" />
							  				</div>
						  				</div>
					  				</div>
					  				<div class="form-group">
					  					<input id="txtTopicLink" name="txtTopicLink" type="text" class="form-control" placeholder="Web Address (start with http:// or https://)" />
					  				</div>
					  				<div class="form-group">
					  					<input id="dtpExpireDate" name="dtpExpireDate" type="text" class="form-control" placeholder="EXPIRATION DATE" />
					  				</div>
					  				<div class="form-group radio">
						  				<label class="radio-main">Does product comply with brand standards?</label> 
						  				<label class="radio-inline">
						  					<input type="radio" name="brand-standards" value="yes" /> Yes 
						  				</label> 
						  				<label class="radio-inline">
						  					<input type="radio" name="brand-standards" value="no" checked="checked" /> No
						  				</label>
					  				</div>
					  				<div class="form-group radio aia-accredited">
						  				<label class="radio-main">AIA Accredited?</label> 
						  				<label class="radio-inline">
						  					<input type="radio" id="optAIA" name="aia-accredited" value="yes" /> Yes
						  				</label>
						  				<label class="radio-inline">
						  					<input type="radio" name="aia-accredited" value="no" checked="checked" /> No
						  				</label>
						  				<input id="txtAIACEUsEarned_CEU" name="txtAIACEUsEarned_CEU" type="text" class="form-control small" placeholder="If yes, CEU’s earned" />
					  				</div>
					  				<div class="form-group radio leed-accredited">
						  				<label class="radio-main">LEED Accredited?</label> 
						  				<label class="radio-inline">
						  					<input type="radio" id="optLEED" name="leed-accredited" value="yes" /> Yes
						  				</label>
						  				<label class="radio-inline">
						  					<input type="radio" name="leed-accredited" value="no" checked="checked" /> No
						  				</label>
						  				<input id="txtLEEDCEUsEarned_CEU" name="txtLEEDCEUsEarned_CEU" type="text" class="form-control small" placeholder="If yes, CEU’s earned" />
					  				</div>
					  				<div class="form-group">
					  					<input id="txtEmail" name="txtEmail" type="text" class="form-control" placeholder="Email address for CEU communication" style="width:400px" />
					  				</div>
					  				<div class="form-group">
					  					<input id="fileCEU" type="file" class="form-control" style="width:400px" runat="server" />
                                          <label>Select an image to be displayed with your CEU credit.</label>
					  				</div>
					  				<div class="form-group">
					  					<input type="submit" class="btn btn-white" value="Submit" onclick="return btnSubmit_Click()" />
					  				</div>
			  					</div>
			  				</div>			  				
			  			</div>
					</div>
				</div>
			</div>
		</div>

    <div class="container">

		<!-- .navigation-panel -->
		<div id="navigation-panel-wrapper" class="navigation-panel-mask">
			<div id="navigation-panel">
			<div class="wrapper">
				<ul>
                    <li><a href="Home.aspx" target="_blank">Home</a></li>
					<li><a href="../Partners/RecommendedPartners.aspx" target="_blank">Browse Recommended Partners</a></li>
					<li><a href="javascript:navigateMyAccount();">My Account</a></li>
					<li><a href="calendar.aspx" target="_blank">Calendar</a></li>
					<li><a href="javascript:meetNGreetTileClicked();">Meet & Greet</a></li>
					<li><a href="adcdirectors.aspx" target="_blank">AD&C Contacts</a></li>
					<li><a href="designinformation.aspx" target="_blank">Explore Design Information</a></li>
					<li><a href="CEUCredits.aspx" target="_blank">CEU Credits</a></li>
					<li><a href="mediacenter.aspx" target="_blank">Media Center & Gallery</a></li>
					<li><a href="supportcenter.aspx" target="_blank">Support Center</a></li>
				</ul>

				<div class="toggle-wrapper" style="display:none;">
					<div class="toggle-title">Admin Tools</div>
					<div class="state active" data-toggle-state="on">OFF</div>
					<div class="toggle toggle-light"></div>
				</div>

				<div id="close-navigation-panel"><a href="javascript:void(0);"><i class="fa fa-times"></i> CLOSE</a></div>
			</div>
		</div>
		</div>
		<!-- /.navigation-panel -->

        <div class="row">
            <h1>CEU Credits</h1>
        </div>

        <p class="page-description">Need continuing education to maintain your AIA license or <a href="http://www.usgbc.org/leed" target="_blank">LEED accreditation</a>? We have a wide variety of free courses and all courses listed provide AIA CE units and/or GBCI CE hours. Courses listed here are available online anytime, 24/7/365!</p>

        <div id="ceu-request">
            <p class="regular">Would you like to have your CEU course featured here?</p>
            <p>Click below to submit your course for consideration.</p>
            <div class="navbar-form navbar-search" role="search">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button id="request-modal-trigger" type="submit" class="btn btn-primary submit" onclick="showRequestform(); return false;">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar-form navbar-search" role="search" style="display:none;">
            <div class="input-group inner-addon left-addon">
                <i class="glyphicon glyphicon-search"></i>
                <input type="text" class="form-control" placeholder="SEARCH FOR A COURSE" />
            </div>
        </div>

        <div class="row">
            <div id="divCEUContainer" class="grid">
                <div id="divGridSizer" class="grid-sizer"></div>
            </div>
        </div>
        <!-- /.row -->

        <div id="ceuDetail" style="display:none;margin-left:100px">
            <div class="ceuDetailImage"><img id="imageDetail" src="images/ceu/aia.jpg" /></div>
		    <table class="ceuDetailInfo">
                <tr><td><div class="ceuTitle" id="TitleDetail"></div></td></tr>
                <tr><td><div id="CompanyDetail"></div></td></tr>
                <tr><td><div id="ContactDetail"></div></td></tr>
                <tr><td><div id="PhoneDetail"></div></td></tr>
                <tr><td><div id="TopicLinkDetail"></div></td></tr>
                <tr><td><div id="ExpireDateDetail"></div></td></tr>
                <tr><td><div id="CreatedDateDetail"></div></td></tr>
                <tr><td><div id="BrandStandardDetail"></div></td></tr>
                <tr><td><div id="AIAAccreditedDetail"></div></td></tr>
                <tr><td><div id="AIACEUsEarnedDetail"></div></td></tr>
                <tr><td><div id="LEEDAccreditedDetail"></div></td></tr>
                <tr><td><div id="LEEDCEUsEarnedDetail"></div></td></tr>
                <tr><td><div id="EmailDetail"></div></td></tr>
		    </table>
            <div class="row" style="margin-top:20px;">
                <span class="back-ceu-list"><i class="fa fa-caret-left"></i>Back to CEU List</span>
            </div>
        </div>

    </div>
    <!-- /.container -->

    <input type="hidden" id="txtEditId" name="txtEditId" />
    <input type="hidden" id="txtUploadType" name="txtUploadType" value="CEU" />

		<!-- global js include -->
		<script src="js/scripts.js"></script>

		<script type="text/javascript">
		    (function ($) {
		        var navPanel = $('#navigation-panel');
		        var navPanelState = 'closed';
		        var naPanelClosedPos = -300;
		        var naPanelOpenPos = 0;

		        // main navigation -->
		        $('#nav-trigger').on('click', function () {
		            manageNavigationPanel();
		        })

		        // close main navigation -->
		        $('#close-navigation-panel').on('click', function () {
		            manageNavigationPanel();
		        })

		        function manageNavigationPanel() {
		            if (navPanelState == 'closed') {
		                navPanelState = 'open';

		                $(navPanel).animate({
		                    marginLeft: naPanelOpenPos,
		                }, 500, function () {
		                    // Animation complete.
		                });
		            } else {
		                navPanelState = 'closed';

		                $(navPanel).animate({
		                    marginLeft: naPanelClosedPos,
		                }, 500, function () {
		                    // Animation complete.
		                });
		            }
		        }

		        $('.toggle').toggles({
		            drag: true, // allow dragging the toggle between positions
		            click: true, // allow clicking on the toggle
		            text: {
		                on: '', // text for the ON position
		                off: '' // and off
		            },
		            on: _isAdminModeToggledOn, // is the toggle ON on init
		            animate: 250, // animation time (ms)
		            easing: 'swing', // animation transition easing function
		            checkbox: null, // the checkbox to toggle (for use in forms)
		            clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
		            width: 50, // width used if not set in css
		            height: 20, // height if not set in css
		            type: 'compact' // if this is set to 'select' then the select style toggle will be used
		        });

		        $('#dtpExpireDate').datetimepicker({
		            format: "MM/DD/YYYY"
		        });

		        loadCEUs();

		        //set the target and action of the form to handle document uploads.
		        $('#form1').attr("target", "upload-iframe");
		        $('#form1').attr("action", "file-upload.ashx");

		        jQuery('.back-ceu-list').click(function () {
		            $('#ceuDetail').hide();
		            $('#divCEUContainer').show();
                    $('#ceu-request').show();
		        });
		    })(jQuery);

		    
		</script>

    <iframe id="upload-iframe" name="upload-iframe" src="" style="display:none;" onload="uploadComplete();"></iframe>

</asp:Content>
