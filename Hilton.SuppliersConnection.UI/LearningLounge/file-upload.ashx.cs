﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;

namespace Hilton.SuppliersConnection.UI.LearningLounge
{
    /// <summary>
    /// Summary description for file_upload
    /// </summary>
    public class file_upload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            String uploadType = GetFormValueString("txtUploadType", context);
            HttpPostedFile postedFile;

            //Need to ensure the user is an admin here.
            //If this is not a CEU request, make sure the user is an administrator.
            //if (uploadType != "CEU" && userIsAdmin(context) == false)
            //{
            //    context.Response.Write("You must be logged in as an administrator to perform this action.");
            //}

            if (uploadType == "GALLERY")
            {
                postedFile = GetFormValueFile("fileUpload", context);
                context.Response.Write(ProcessGalleryImage(context, postedFile));
            }
            else if (uploadType == "TILE")
            {
                postedFile = GetFormValueFile("fileTileImage", context);
                context.Response.Write(ProcessTileImage(context, postedFile));
            }
            else if (uploadType == "CALENDAR")
            {
                postedFile = GetFormValueFile("fileUpload", context);
                context.Response.Write(ProcessCalendarPost(context, postedFile));
            }
            else if (uploadType == "MEDIACENTER")
            {
                context.Response.Write(ProcessMediaContent(context));
            }
            else if (uploadType == "CEU")
            {
                context.Response.Write(ProcessCEU(context));
            }
            
            
        }

        private bool userIsAdmin(HttpContext context)
        {
            if (context.Session[SessionConstants.User] != null)
            {
                User loggedInUser = (User)context.Session[SessionConstants.User];
                if (loggedInUser.UserTypeId == 5)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false; 
        }

        private String ProcessGalleryImage(HttpContext context, HttpPostedFile file)
        {
            String clickTypeId = GetFormValueString("selectClickType", context);
            String clickActionURL = GetFormValueString("txtClickActionURL", context);
            String target = GetFormValueString("target", context);
            String enabled = GetFormValueString("chkEnabled", context);
            String editId = GetFormValueString("txtEditId", context);
                   
            LearningLoungeGallery newItem = new LearningLoungeGallery();
            newItem.GalleryImageId = Convert.ToInt32(editId);

            if (!clickActionURL.StartsWith("http") && !clickActionURL.StartsWith("https"))
            {
                newItem.ClickActionURL = "http://" + clickActionURL;
            }
            else
            {
                newItem.ClickActionURL = clickActionURL;
            }

            newItem.ClickActionTarget = target;
            newItem.Enabled = (enabled == "enabled");
            newItem.ClickActionTypeId = Convert.ToInt32(clickTypeId);
            newItem.TemplateContent = GetTemplateContent(context, clickTypeId, "gallery");

            byte[] fileBytes = ToByteArray(file.InputStream);

            newItem.Image = Convert.ToBase64String(fileBytes);

            ILearningLoungeManager mgr = LearningLoungeManager.Instance;
            String returnValue =  mgr.UpsertGallerySliderImage(newItem);

            if (returnValue == "SUCCESS" && Convert.ToInt32(editId) == -1)
            {
                return "The feature content slider image was successfully created.";
            }
            else if (returnValue == "SUCCESS" && Convert.ToInt32(editId) > -1)
            {
                return "The feature content slider image was successfully updated.";
            }
            else
            {
                return returnValue;
            }

        }

        private String ProcessTileImage(HttpContext context, HttpPostedFile file)
        {
            String clickTypeId = GetFormValueString("cboTileClickActionType", context);
            String clickActionURL = GetFormValueString("txtTileClickActionURL", context);
            String target = GetFormValueString("tileTarget", context);
            String permission = GetFormValueString("tilePermissions", context);
            String enabled = GetFormValueString("chkTileEnabled", context);
            String editId = GetFormValueString("txtEditId", context);

            if (String.IsNullOrEmpty(permission))
            {
                permission = "-1";
            }

            LearningLoungeTile newItem = new LearningLoungeTile();
            newItem.TileImageId = Convert.ToInt32(editId);

            if (!clickActionURL.StartsWith("http") && !clickActionURL.StartsWith("https"))
            {
                newItem.ClickActionURL = "http://" + clickActionURL;
            }
            else
            {
                newItem.ClickActionURL = clickActionURL;
            }

            newItem.ClickActionTarget = target;
            newItem.Enabled = (enabled == "enabled");
            newItem.ClickActionTypeId = Convert.ToInt32(clickTypeId);
            newItem.RestrictToUserTypeId = Convert.ToInt32(permission);
            newItem.TemplateContent = GetTemplateContent(context, clickTypeId, "tile");

            byte[] fileBytes = ToByteArray(file.InputStream);
            newItem.Image = Convert.ToBase64String(fileBytes);

            ILearningLoungeManager mgr = LearningLoungeManager.Instance;
            
            String returnValue = mgr.UpsertTile(newItem);

            if (returnValue == "SUCCESS" && Convert.ToInt32(editId) == -1)
            {
                return "The tile was successfully created.";
            }
            else if (returnValue == "SUCCESS" && Convert.ToInt32(editId) > -1)
            {
                return "The tile was successfully updated.";
            }
            else
            {
                return returnValue;
            }
        }

        private List<LearningLoungeTemplateContent> GetTemplateContent(HttpContext context, String clickTypeId, String contentType)
        {
            LearningLoungeTemplateContent templateContent;
            List<LearningLoungeTemplateContent> templateContentList = new List<LearningLoungeTemplateContent>();
            int templateId = -1;
            byte[] fileBytes;
            HttpPostedFile file;

            if (contentType == "gallery")
            {
                templateId = Convert.ToInt32(context.Request.Form["cboSliderTemplate"]);
            }
            else if (contentType == "tile")
            {
                templateId = Convert.ToInt32(context.Request.Form["cboTileTemplate"]);
            }

            if (clickTypeId == "3")
            {
                if (templateId == 1)
                {
                    templateContent = new LearningLoungeTemplateContent();
                    templateContent.ContentText = context.Request.Form["textTemplate1"].ToString();
                    templateContent.ContentName = "text1";
                    templateContent.ContentType = contentType;
                    templateContent.ContentImage = "";
                    templateContent.TemplateId = templateId;
                    templateContentList.Add(templateContent);

                    templateContent = new LearningLoungeTemplateContent();
                    templateContent.ContentText = "";
                    templateContent.ContentName = "hero-image";
                    templateContent.ContentType = contentType;

                    file = GetFormValueFile("fileTemplate1Hero", context);
                    if (file != null)
                    {
                        fileBytes = ToByteArray(file.InputStream);
                        templateContent.ContentImage = Convert.ToBase64String(fileBytes);
                        templateContent.TemplateId = templateId;
                        templateContentList.Add(templateContent);
                    }

                    templateContent = new LearningLoungeTemplateContent();
                    templateContent.ContentText = "";
                    templateContent.ContentName = "callout-image";
                    templateContent.ContentType = contentType;
                    file = GetFormValueFile("fileTemplate1Callout", context);
                    if (file != null)
                    {
                        fileBytes = ToByteArray(file.InputStream);
                        templateContent.ContentImage = Convert.ToBase64String(fileBytes);
                        templateContent.TemplateId = templateId;
                        templateContentList.Add(templateContent);
                    }
                }
                else if (templateId == 2)
                {
                    templateContent = new LearningLoungeTemplateContent();
                    templateContent.ContentText = context.Request.Form["textTemplate2"].ToString();
                    templateContent.ContentName = "text1-template2";
                    templateContent.ContentType = contentType;
                    templateContent.ContentImage = "";
                    templateContent.TemplateId = templateId;
                    templateContentList.Add(templateContent);

                    templateContent = new LearningLoungeTemplateContent();
                    templateContent.ContentText = "";
                    templateContent.ContentName = "hero-image-template2";
                    templateContent.ContentType = contentType;

                    file = GetFormValueFile("fileTemplate2Hero", context);
                    if (file != null)
                    {
                        fileBytes = ToByteArray(file.InputStream);
                        templateContent.ContentImage = Convert.ToBase64String(fileBytes);
                        templateContent.TemplateId = templateId;
                        templateContentList.Add(templateContent);
                    }

                    templateContent = new LearningLoungeTemplateContent();
                    templateContent.ContentText = "";
                    templateContent.ContentName = "callout-image1-template2";
                    templateContent.ContentType = contentType;
                    file = GetFormValueFile("fileTemplate2Callout1", context);
                    if (file != null)
                    {
                        fileBytes = ToByteArray(file.InputStream);
                        templateContent.ContentImage = Convert.ToBase64String(fileBytes);
                        templateContent.TemplateId = templateId;
                        templateContentList.Add(templateContent);
                    }

                    templateContent = new LearningLoungeTemplateContent();
                    templateContent.ContentText = "";
                    templateContent.ContentName = "callout-image2-template2";
                    templateContent.ContentType = contentType;
                    file = GetFormValueFile("fileTemplate2Callout2", context);
                    if (file != null)
                    {
                        fileBytes = ToByteArray(file.InputStream);
                        templateContent.ContentImage = Convert.ToBase64String(fileBytes);
                        templateContent.TemplateId = templateId;
                        templateContentList.Add(templateContent);
                    }

                    templateContent = new LearningLoungeTemplateContent();
                    templateContent.ContentText = "";
                    templateContent.ContentName = "callout-image3-template2";
                    templateContent.ContentType = contentType;
                    file = GetFormValueFile("fileTemplate2Callout3", context);
                    if (file != null)
                    {
                        fileBytes = ToByteArray(file.InputStream);
                        templateContent.ContentImage = Convert.ToBase64String(fileBytes);
                        templateContent.TemplateId = templateId;
                        templateContentList.Add(templateContent);
                    }
                }
            }

            return templateContentList;
        }

        private String ProcessMediaContent(HttpContext context)
        {
            HttpPostedFile postedFile = GetFormValueFile("fileUpload", context);
            String enabled = context.Request.Form["chkEnabled"];
            String editId = context.Request.Form["txtEditId"];

            LearningLoungeMediaCenterItem newItem = new LearningLoungeMediaCenterItem();
            newItem.MediaCenterId = Convert.ToInt32(editId);
            newItem.ContentType = context.Request.Form["selectContentType"];
            newItem.ContentText = context.Request.Form["txtOnlineImageURL"];
            newItem.Enabled = (enabled == "enabled");
            newItem.Caption = context.Request.Form["txtCaption"];
            newItem.Keywords = context.Request.Form["txtKeywords"];

            if (postedFile != null && newItem.ContentType == "uploaded-image")
            {
                byte[] fileBytes = ToByteArray(postedFile.InputStream);
                newItem.ContentImage = Convert.ToBase64String(fileBytes);
            }
            ILearningLoungeManager mgr = LearningLoungeManager.Instance;

            String returnValue = mgr.UpsertMediaCenterItem(newItem);

            if (returnValue == "SUCCESS" && Convert.ToInt32(editId) == -1)
            {
                return "The gallery image was successfully created.";
            }
            else if (returnValue == "SUCCESS" && Convert.ToInt32(editId) > -1)
            {
                return "The gallery image was successfully updated.";
            }
            else
            {
                return returnValue;
            }
        }

        private String ProcessCalendarPost(HttpContext context, HttpPostedFile file)
        {
            String editId = context.Request.Form["txtEditId"].ToString();
            int duration = 1;
            String time = "";

            String temp = context.Request.Form["txtStartDate"].ToString(); 
            DateTime startDate = Convert.ToDateTime(temp);
            DateTime endDate = DateTime.MinValue;
            String restrictUserTypes = GetRestrictToUserTypes(context);      //will be AUTHENTICATEDUSERS, DEFINEDROLES, or EVERYONE
            String chkAllDayEvent = GetFormValueString("chkAllDayEvent", context);
            String txtEndDate; 

            if (chkAllDayEvent != "allday")
            {
                txtEndDate = GetFormValueString("txtEndDate", context);
                endDate = Convert.ToDateTime(txtEndDate);
                duration = endDate.Subtract(startDate).Days;

            }

            byte[] fileBytes = ToByteArray(file.InputStream);

            LearningLoungeCalendarEvent newEvent = new LearningLoungeCalendarEvent();
            newEvent.EventId = Convert.ToInt32(editId);
            newEvent.name = context.Request.Form["txtEventName"].ToString();
            newEvent.image = Convert.ToBase64String(fileBytes);
            newEvent.description = GetFormValueString("txtDescription", context);
            newEvent.StartDate = startDate;
            newEvent.EndDate = endDate;
            newEvent.day = 1; // Convert.ToInt16(startDayMonthYear[1]);
            newEvent.month = 1; // Convert.ToInt16(startDayMonthYear[0]);
            newEvent.year = 1; // Convert.ToInt16(startDayMonthYear[2]);
            newEvent.time = time;
            newEvent.duration = duration;
            newEvent.location = GetFormValueString("txtLocation", context);
            newEvent.URL = context.Request.Form["txtEventURL"].ToString();

            if (!newEvent.URL.StartsWith("http") && !newEvent.URL.StartsWith("https"))
            {
                newEvent.URL = "http://" + newEvent.URL;
            }

            newEvent.RestrictToUserTypes = restrictUserTypes;
            newEvent.CategoryIds = GetSelectedCategories(context).ToArray();
            newEvent.UserTypeIds = GetSelectedUserTypes(context).ToArray();
            newEvent.Enabled = true;

            if (!String.IsNullOrEmpty(context.Request.Form["txtDuplicatedEventId"])) {
                newEvent.DuplicatedEventId = Convert.ToInt32(context.Request.Form["txtDuplicatedEventId"]);
            }

            if (newEvent.CategoryIds.Length == 1 && newEvent.CategoryIds[0] == 7)  //Hilton Office
            {
                newEvent.color = 1;
            }
            else if (newEvent.CategoryIds.Length == 1 && newEvent.CategoryIds[0] == 6)  //Meet and Greet
            {
                newEvent.color = 2;
            }
            else if (newEvent.CategoryIds.Length == 1 && newEvent.CategoryIds[0] == 3)  //Industry Events
            {
                newEvent.color = 3;
            }
            else if (newEvent.CategoryIds.Length == 1 && newEvent.CategoryIds[0] == 4)  //Lunch and Learns
            {
                newEvent.color = 4;
            }
            else if (newEvent.CategoryIds.Length == 1 && newEvent.CategoryIds[0] == 5)  //Hilton Conferences
            {
                newEvent.color = 5;
            }
            else
            {
                newEvent.color = 6;
            }

            ILearningLoungeManager mgr = LearningLoungeManager.Instance;
            String returnValue = mgr.UpsertCalendarEvent(newEvent);
            if (returnValue == "SUCCESS")
            {
                return "The calendar event has been entered successfully.";
            }
            else
            {
                return "An error occurred while creating the calendar event.";
            }
        }

        private String ProcessCEU(HttpContext context)
        {
            HttpPostedFile postedFile = GetFormValueFile("fileCEU", context);
            String txtTitle = context.Request.Form["txtTitle"];
            String txtCompany = context.Request.Form["txtCompany"];
            String txtContact = context.Request.Form["txtContact"];
            String txtPhone = context.Request.Form["txtPhone"];
            String txtTopicLink = context.Request.Form["txtTopicLink"];
            String dtpExpireDate = context.Request.Form["dtpExpireDate"];
            String brandStandards = context.Request.Form["brand-standards"];
            String aia = context.Request.Form["aia-accredited"];
            String aiaCreditsEarned = context.Request.Form["txtAIACEUsEarned_CEU"];
            String leed = context.Request.Form["leed-accredited"];
            String leedCreditsEarned = context.Request.Form["txtLEEDCEUsEarned_CEU"];
            String txtEmail = context.Request.Form["txtEmail"];

            LearningLoungeCEU newItem = new LearningLoungeCEU();
            newItem.CourseTitle = txtTitle;
            newItem.Company = txtCompany;
            newItem.Contact = txtContact;
            newItem.Phone = txtPhone;

            if (!txtTopicLink.StartsWith("http") && !txtTopicLink.StartsWith("https"))
            {
                newItem.TopicLink = "http://" + txtTopicLink;
            }
            else
            {
                newItem.TopicLink = txtTopicLink;
            }

            if (dtpExpireDate != "")
            {
                newItem.ExpireDate = Convert.ToDateTime(dtpExpireDate);
            }
            else
            {
                newItem.ExpireDate = DateTime.MinValue;
            }

            newItem.BrandStandardCompliance = (brandStandards == "yes");
            newItem.AIAAccredited = (aia == "yes");

            if (!String.IsNullOrEmpty(aiaCreditsEarned))
                newItem.AIANumCEUs = aiaCreditsEarned;

            newItem.LEEDAccredited = (leed == "yes");

            if (!String.IsNullOrEmpty(leedCreditsEarned))
                newItem.LEEDNumCEUs = leedCreditsEarned;

            newItem.SubmittedByEmail = txtEmail;

            if (postedFile != null)
            {
                byte[] fileBytes = ToByteArray(postedFile.InputStream);
                newItem.Image = Convert.ToBase64String(fileBytes);
            }
            ILearningLoungeManager mgr = LearningLoungeManager.Instance;

            String returnValue = mgr.AddCEU(newItem);
            if (returnValue == "SUCCESS")
            {
                return "The CEU credit has been successfully submitted and is awaiting approval by an administrator.";
            }
            else
            {
                return "An error occurred while creating the ceu credit.";
            }
        }

        private byte[] ToByteArray(System.IO.Stream stream)
        {
            stream.Position = 0;
            byte[] buffer = new byte[stream.Length];
            for (int totalBytesCopied = 0; totalBytesCopied < stream.Length; )
                totalBytesCopied += stream.Read(buffer, totalBytesCopied, Convert.ToInt32(stream.Length) - totalBytesCopied);
            return buffer;
        }

        private String GetFormValueString(String controlName, HttpContext context)
        {
            for (int i=0; i<context.Request.Form.AllKeys.Length; i++)
            {
                if (context.Request.Form.AllKeys[i].EndsWith(controlName))
                {
                    return context.Request.Form.GetValues(i)[0].ToString();
                }
            }

            return "";
        }

        private HttpPostedFile GetFormValueFile(String controlName, HttpContext context)
        {
            for (int i = 0; i < context.Request.Files.AllKeys.Length; i++)
            {
                if (context.Request.Files.AllKeys[i].EndsWith(controlName))
                {
                    return context.Request.Files[i];
                }
            }

            return null;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private List<int> GetSelectedCategories(HttpContext context)
        {
            String chkHiltonOffice = GetFormValueString("chkHiltonOffice", context);
            String chkMeetAndGreet = GetFormValueString("chkMeetAndGreet", context);            
            String chkIndustryEvent = GetFormValueString("chkIndustryEvent", context);
            String chkLunchAndLearn = GetFormValueString("chkLunchAndLearn", context);
            String chkHiltonConference = GetFormValueString("chkHiltonConference", context);
            List<int> selectedCategories = new List<int>();

            if (chkHiltonOffice == "on")
            {
                selectedCategories.Add(7);
            }

            if (chkMeetAndGreet == "on")
            {
                selectedCategories.Add(6);
            }

            if (chkIndustryEvent == "on")
            {
                selectedCategories.Add(3);
            }

            if (chkLunchAndLearn == "on")
            {
                selectedCategories.Add(4);
            }

            if (chkHiltonConference == "on")
            {
                selectedCategories.Add(5);
            }

            return selectedCategories;
        }

        private String GetRestrictToUserTypes(HttpContext context)
        {
            String chkEveryone = GetFormValueString("chkEveryone", context);
            String chkEmployeesOnly = GetFormValueString("chkEmployeesOnly", context);
            String chkOwnersConsultants = GetFormValueString("chkOwnersConsultants", context);
            String chkOwners = GetFormValueString("chkOwners", context);
            String chkPartners = GetFormValueString("chkPartners", context);

            if (chkEveryone == "on")
            {
                return "EVERYONE";
            }

            if (chkEmployeesOnly == "on")
            {
                return "AUTHENTICATEDUSERS";
            }

            if (chkOwnersConsultants == "on" || chkOwners == "on" || chkPartners == "on")
            {
                return "DEFINEDROLES";
            }

            return "EVERYONE";
        }

        private List<int> GetSelectedUserTypes(HttpContext context)
        {
            String chkPartners = GetFormValueString("chkPartners", context);
            String chkOwnersConsultants = GetFormValueString("chkOwnersConsultants", context);
            List<int> selectedUserTypes = new List<int>();

            if (chkPartners == "on")
            {
                selectedUserTypes.Add(2);
            }

            if (chkOwnersConsultants == "on")
            {
                selectedUserTypes.Add(3);
                selectedUserTypes.Add(4);
            }

            return selectedUserTypes;
        }
    }
}