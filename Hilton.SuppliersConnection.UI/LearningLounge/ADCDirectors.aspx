﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LearningLounge/LearningLoungeMaster.Master" AutoEventWireup="true" CodeBehind="ADCDirectors.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LearningLounge.ADCDirectors" %>

<%@ MasterType VirtualPath="~/LearningLounge/LearningLoungeMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/detectmobilebrowser.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

    <!-- .container -->
    <div class="container">
        <!-- .navigation-panel -->
        <div id="navigation-panel-wrapper" class="navigation-panel-mask">
            <div id="navigation-panel">
                <div class="wrapper">
                    <ul>
                        <li><a href="Home.aspx" target="_blank">Home</a></li>
                        <li><a href="../Partners/RecommendedPartners.aspx" target="_blank">Browse Recommended Partners</a></li>
                        <li><a href="javascript:navigateMyAccount();">My Account</a></li>
                        <li><a href="calendar.aspx" target="_blank">Calendar</a></li>
                        <li><a href="javascript:meetNGreetTileClicked();">Meet & Greet</a></li>
                        <li><a href="adcdirectors.aspx" target="_blank">AD&C Contacts</a></li>
                        <li><a href="designinformation.aspx" target="_blank">Explore Design Information</a></li>
                        <li><a href="CEUCredits.aspx" target="_blank">CEU Credits</a></li>
                        <li><a href="mediacenter.aspx" target="_blank">Media Center & Gallery</a></li>
                        <li><a href="supportcenter.aspx" target="_blank">Support Center</a></li>
                    </ul>

                    <div class="toggle-wrapper" style="display: none;">
                        <div class="toggle-title">Admin Tools</div>
                        <div class="state active" data-toggle-state="on">OFF</div>
                        <div class="toggle toggle-light"></div>
                    </div>

                    <div id="close-navigation-panel"><a href="javascript:void(0);"><i class="fa fa-times"></i>CLOSE</a></div>
                </div>
            </div>
        </div>
        <!-- /.navigation-panel -->

        <div class="row">

            <!-- main content -->
            <div class="col-md-12 main-content">

                <h1>AD&C CONTACTS</h1>

                <div id="map-selectbox" class="">
                    <div class="detail"></div>
                    <div class="bfh-selectbox bfh-states" data-country="US" data-state="AL">
                        <input type="hidden" value="CA">
                        <a class="bfh-selectbox-toggle form-control" role="button" data-toggle="bfh-selectbox" href="#">
                            <span class="bfh-selectbox-option" data-option="12">Alabama</span>
                            <b class="caret"></b>
                        </a>
                        <div class="bfh-selectbox-options">
                            <div role="listbox">
                                <ul role="option"></ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="maps-navigation">
                    <p class="page-description">Please select a geographic region below<br>
                        to find your AD&C Contact.</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div id="map-navigation-north-america" class="map-navigation region-trigger" data-region="north-america">
                                <%--<div class="title">North America</div>--%>
                                <div class="map-image">
                                    <img class="" src="images/adc-directors/map-north-america.png" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div id="map-navigation-emea" class="map-navigation region-trigger" data-region="emea">
                                <%--<div class="title">EUROPE, MIDDLE EAST<br>& AFRICA</div>--%>
                                <div class="map-image">
                                    <img class="" src="images/adc-directors/map-emea.png" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div id="map-navigation-latin-america-and-caribbean" class="map-navigation region-trigger" data-region="latin-america-and-caribbean">
                                <%--<div class="title">Latin America<br>& Caribbean</div>--%>
                                <div class="map-image">
                                    <img class="" src="images/adc-directors/map-latin-america-and-caribbean.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="page-description">Don't see your region above? <a href="http://www.hiltonworldwide.com/development/architecture-construction" target="_blank">Click here</a>.</p>
                </div>

            </div>
            <!-- /.main-content -->

            <!-- sidebar -->
            <div class="col-md-4 sidebar"></div>
            <!-- /.sidebar -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- global js include -->
    <script src="js/scripts.js"></script>

    <script type="text/javascript">
        (function ($) {

            /********************************************************************************
			**	VARIABLE DEFINITIONS
			********************************************************************************/
            var navPanel = $('#navigation-panel');
            var navPanelState = 'closed';
            var naPanelClosedPos = -300;
            var naPanelOpenPos = 0;

            var numMaps = $('.map-container').length;
            var activeMap = null;
            var activeRegion = null;
            var activeState = null;
            var fadeInSpeed = 400;
            var fadeOutSpeed = 200;
            var windowWidth = null;
            var mapWidth = null;
            var jsonPath = 'json/adc-directors/';
            var directors = [];
            var mapSelectBox = null;

            // main navigation -->
            $('#nav-trigger').on('click', function () {
                manageNavigationPanel();
            });

            // close main navigation -->
            $('#close-navigation-panel').on('click', function () {
                manageNavigationPanel();
            });

            $('.toggle').toggles({
                drag: true, // allow dragging the toggle between positions
                click: true, // allow clicking on the toggle
                text: {
                    on: '', // text for the ON position
                    off: '' // and off
                },
                on: _isAdminModeToggledOn, // is the toggle ON on init
                animate: 250, // animation time (ms)
                easing: 'swing', // animation transition easing function
                checkbox: null, // the checkbox to toggle (for use in forms)
                clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
                width: 50, // width used if not set in css
                height: 20, // height if not set in css
                type: 'compact' // if this is set to 'select' then the select style toggle will be used
            });

            function manageNavigationPanel() {
                if (navPanelState == 'closed') {
                    navPanelState = 'open';

                    $(navPanel).animate({
                        marginLeft: naPanelOpenPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                } else {
                    navPanelState = 'closed';

                    $(navPanel).animate({
                        marginLeft: naPanelClosedPos,
                    }, 500, function () {
                        // Animation complete.
                    });
                }
            }

            // region navigation -->
            $('#maps-navigation .region-trigger').on('mouseover', function (e) {
                manageMainMapHover($(this), e.type);
            });
            $('#maps-navigation .region-trigger').on('mouseout', function (e) {
                manageMainMapHover($(this), e.type);
            });

            // main map navigation -->
            $('.maps-navigation-trigger').on('click', function () {
                initMainMapsNavigation();
            });

            // region navigation -->
            $('.main-content, .sidebar').on('click', '.region-trigger', function () {

                // deactivate all buttons -->
                $('button').removeClass('active');

                $('body').removeClass(activeRegion);

                // set active region -->
                activeRegion = $(this).attr('data-region');

                $('body').addClass(activeRegion);

                if (activeRegion == 'north-america') {
                    activeMap = 'focused-service';
                } else {
                    activeMap = activeRegion;
                }

                if ($('#maps-navigation').css('display') == 'block') {
                    $('#maps-navigation').fadeOut(fadeOutSpeed);
                }

                initRegion();
                hideActiveContainers();

                if (activeMap != "emea" && activeMap != "latin-america-and-caribbean" && $(window).width() <= 992) {
                    $(mapSelectBox).show();
                } else {
                    $(mapSelectBox).hide();
                }
            });

            // state -->
            $('.main-content').on('click', '.map-wrapper path', function (e) {
                var activeStateLabel = '';
                var activeStateId = $(this).attr('id');
                var activeRegionId = $(this).parent().attr('id');

                /* Keep state highlighted on click
                $('.map-wrapper path').removeClass('active');
                $(this).attr('class', 'active');
                */

                if (activeMap == 'focused-service') {
                    if ($(this).parent().parent().attr('id') == 'Canada') {
                        activeStateLabel = 'Canada';
                    } else {
                        activeStateLabel = $('.bfh-selectbox-options ul').find("[data-option='" + activeStateId + "']").attr('id');
                    }
                } else if (activeMap == 'tru') {
                    activeStateLabel = $(e.target).closest('.region-group').attr('id');
                } else if (activeMap == 'emea' || activeMap == 'latin-america-and-caribbean') {
                    activeStateLabel = $(this).parent().attr('data-name');
                } else {
                    activeStateLabel = $(this).parent().attr('id');
                }

                $('.bfh-selectbox-option').html(activeStateLabel);
            });

            // region group -->
            $('.main-content').on('click', '.map-wrapper .region-group', function () {
                activeState = $(this).attr('id');

                // Populate the sidebar director information -->
                $.each(directors, function (key, value) {
                    if (value.region_slug == activeState) {
                        $('.well.director-info-container .director-info.region .region-label').html(value.region);
                        $('.well.director-info-container .director-info.director .director-name').html(value.name);
                        $('.well.director-info-container .director-info.director .director-position').html(value.position + ' <img src="images/adc-directors/director-line.gif" />');
                        $('.well.director-info-container .director-info.phone .director-phone').html(value.phone);
                    }
                });

                $('.well.director-info-container').fadeIn(fadeInSpeed);

            });

            // map navigation -->
            $('.main-content, .sidebar').on('click', '.map-trigger', function () {

                hideActiveContainers();

                // deactivate all buttons -->
                $('button').removeClass('active');

                // set active map -->
                activeMap = $(this).attr('data-map');

                setTimeout(function () {

                    // load the appropriate map -->
                    initMap();

                }, 400);

            });

            // ensure the map gets sized properly -->
            $(window).resize(function () {
                resizeMap();

                // move elements based on browser width -->
                checkWindowWidth();
            });


            /********************************************************************************
            **	FUNCTION DEFINITIONS
            ********************************************************************************/

            function init() {
                //$('#btn-north-america').trigger('click');
            }

            function initMainMapsNavigation() {
                hideActiveContainers();

                $('.map-container, .sidebar, .map-wrapper').fadeOut(fadeOutSpeed);

                setTimeout(function () {
                    $('.container .main-content').attr('class', 'col-xs-12 main-content');
                    $('#maps-navigation').fadeIn(fadeInSpeed);
                }, 500);
            }

            function manageMainMapHover(regionContainer, type) {
                var region = $(regionContainer).attr('data-region');

                if (type == 'mouseover') {
                    $(regionContainer).addClass('active');
                    $(regionContainer).find('.map-image img').attr('src', 'images/adc-directors/map-' + region + '-active.png');
                } else {
                    $(regionContainer).removeClass('active');
                    $(regionContainer).find('.map-image img').attr('src', 'images/adc-directors/map-' + region + '.png');
                }
            }

            function initRegion() {
                hideActiveContainers();
                initMap();
            }

            function initMap() {

                // load map data -->
                buildMapItem();

                window.scrollTo(0, 0);

                setTimeout(function () {

                    $('.container .main-content').attr('class', 'col-xs-12 col-md-8 main-content');

                    $(window).trigger('resize');

                    $('.map-container').fadeIn(fadeInSpeed, function () {
                        $(window).trigger('resize');
                    });

                    $('.sidebar').fadeIn(fadeInSpeed, function () {

                    });

                }, 600);

            }

            function buildMapItem() {

                // remove existing map content -->
                $('.map-container').remove();

                // remove existing sidebar content -->
                $('.sidebar-container').remove();

                $.ajax({
                    type: "POST",
                    url: "LearningLoungeService.asmx/GetMapInfo",
                    contentType: "application/json; charset=utf-8",
                    data: "{mapName: '" + activeMap + "'}",
                    dataType: "json",
                    success: function (data) {
                        var response = data.d;
                        var mapString = '';
                        var sidebarString = '';
                        var brandLogosString = '';
                        var lbOptionsString = '';
                        var hasMultipleMaps = response.has_multiple_maps;

                        // empty listbox -->
                        $('.bfh-selectbox-options ul').empty();

                        // populate listbox title -->

                        $('#map-selectbox .detail').html(response.map[0].region_listbox.title);

                        // create default item -->
                        lbOptionsString += '<li><a tabindex="-1" href="#" class="region-group" id="lb-default" data-option="null">Select A State</a></li>';

                        // gather listbox items -->
                        $.each(response.map[0].region_listbox.options, function (lbKey, lbValue) {
                            lbOptionsString += '<li><a tabindex="-1" href="#" class="region-group" id="' + lbValue.label + '" data-option="' + lbValue.value + '" data-region-code="' + lbValue.region + '">' + lbValue.label + '</a></li>';
                        });

                        // append listbox items to ul container -->
                        $(lbOptionsString).appendTo($('.bfh-selectbox-options ul'));

                        // assign click event to listbox items after its populated -->
                        $('.bfh-selectbox-options').on('click', 'li a', function () {
                            var regionId = $(this).attr('id');
                            var regionCode = $(this).attr('data-region-code');
                            var option = $(this).attr('data-option');

                            console.log('regionCode: ' + regionCode);
                            console.log('regionId: ' + regionId);
                            console.log('option: ' + option);

                            $('.map-wrapper svg').find('#' + regionCode).trigger('click');
                            $('.map-wrapper svg').find('#' + regionId).trigger('click');
                        });

                        // make sure the listbox is visible after it gets populated -->
                        $('#map-selectbox, #map-selectbox .bfh-selectbox').show();


                        $.each(response.map, function (mapKey, mapValue) {

                            /* MAIN CONTENT */

                            // MAP -->
                            mapString += '<div id="' + mapValue.slug + '-map-container" class=\"map-container\">';

                            // header -->
                            mapString += '<div class=\"map-header\">';
                            mapString += '<h2><span class="strong">' + response.name + ' </span>' + mapValue.name + '</h2>';
                            mapString += '</div>';

                            // callout ->
                            mapString += '<div class=\"map-callout\">' + response.map[0].callout + '</div>';

                            // subtitle -->
                            mapString += '<div class=\"map-subtitle\">' + response.map[0].subtitle + '</div>';

                            // wrapper -->
                            mapString += '<div class=\"map-wrapper\"></div>';

                            // contacts -->
                            mapString += '<div class=\"contacts\">';
                            mapString += '<span class="contacts-label regular">' + mapValue.main_contacts.label + '</span>';

                            // main -->
                            mapString += '<div id=\"contacts-main\">';
                            $.each(mapValue.main_contacts.contacts, function (contactKey, contactValue) {
                                mapString += '<div class=\"contact main\">';
                                mapString += '<span class="contact-line">' + contactValue.name + '<span class="contacts-divider">|</span></span>';
                                mapString += '<span class="contact-line strong">' + contactValue.position + '</span>';
                                mapString += '<span class="contact-line"><span class="contacts-divider">|</span>' + contactValue.phone + '</span>';
                                mapString += '</div>';
                            });
                            mapString += '</div>';

                            // general -->
                            mapString += '<div id=\"contacts-general\">';
                            $.each(mapValue.general_contacts.contacts, function (contactKey, contactValue) {
                                mapString += '<div class=\"contact general\">';
                                mapString += '<span class="contact-line region strong">' + contactValue.region + '</span>';
                                mapString += '<span class="contact-line name light">' + contactValue.name + '</span>';
                                mapString += '<span class="contact-line position light">' + contactValue.position + '</span>';
                                mapString += '<span class="contact-line phone regular">' + contactValue.phone + '</span>';
                                mapString += '</div>';
                            });
                            mapString += '</div>';

                            // additional -->
                            mapString += '<div id=\"contacts-additional\">';
                            $.each(mapValue.additional_contacts.contacts, function (contactKey, contactValue) {
                                mapString += '<div class=\"contact additional\">';
                                mapString += '<span class="contact-line">' + contactValue.name + '<span class="contacts-divider">|</span></span>';
                                mapString += '<span class="contact-line strong">' + contactValue.position + '</span>';
                                mapString += '<span class="contact-line"><span class="contacts-divider">|</span>' + contactValue.phone + '</span>';
                                mapString += '</div>';
                            });
                            mapString += '</div>';

                            mapString += '</div>';
                            /*  /.contacts */

                            mapString += '</div>';
                            /*  /.map-container */

                            /* SIDEBAR */
                            sidebarString += '<div id="' + mapValue.slug + '-sidebar-container" class=\"sidebar-container\">';

                            if (hasMultipleMaps) {
                                sidebarString += '<div class="title">CHOOSE A MAP</div>';

                                // nav -->
                                sidebarString += '<div class="well nav">';
                                $.each(mapValue.sidebar_navigation.buttons, function (sidebarNavKey, sidebarNavValue) {
                                    sidebarString += '<button id="btn-' + sidebarNavValue.slug + '" type="button" class="btn btn-primary map-trigger" data-map="' + sidebarNavValue.slug + '" role="button" data-toggle="button">' + sidebarNavValue.label + '</button>';
                                });
                                sidebarString += '</div>';
                            }

                            // director -->
                            sidebarString += '<div class="well director-info-container blue">';

                            sidebarString += '<div class="director-info region">';
                            sidebarString += '<span class="director-icon">';
                            sidebarString += '<img src="images/adc-directors/icon-region.png" />';
                            sidebarString += '</span>';
                            sidebarString += '<span class="region-label">';
                            sidebarString += '</span>';
                            sidebarString += '</div>';

                            sidebarString += '<div class="director-info director">';

                            sidebarString += '<span class="director-icon">';
                            sidebarString += '<img src="images/adc-directors/icon-director.png" />';
                            sidebarString += '</span>';

                            sidebarString += '<span class="director-name">';
                            sidebarString += '</span>';

                            sidebarString += '<span class="director-position">';
                            sidebarString += '</span>';

                            sidebarString += '</div>';

                            sidebarString += '<div class="director-info phone">';
                            sidebarString += '<span class="director-icon">';
                            sidebarString += '<img src="images/adc-directors/icon-phone.png" />';
                            sidebarString += '</span>';
                            sidebarString += '<span class="director-phone">';
                            sidebarString += '</span>';
                            sidebarString += '</div>';

                            sidebarString += '</div>';

                            // regions -->
                            sidebarString += '<div class="well regions">';
                            sidebarString += '<div class="well-header">CHOOSE A DIFFERENT REGION</div>';
                            sidebarString += '<div class="well-region-nav map-navigation region-trigger na light" data-region="north-america">North<br>America</div>';
                            sidebarString += '<div class="well-region-nav map-navigation region-trigger emea light" data-region="emea">Europe,<br>Middle East<br>& Africa</div>';
                            sidebarString += '<div class="well-region-nav map-navigation region-trigger lac light" data-region="latin-america-and-caribbean">Latin America,<br>& Caribbean</div>';
                            sidebarString += '</div>';

                            // Admin -->
                            sidebarString += '<div><a id="adminLink" href="ADC-ContactsAdmin.aspx" target="_blank" style="display:none">Update Contacts</a></div>';

                            // print -->
                            if (activeRegion != 'north-america' && mapValue.sidebar_print_options.buttons) {
                                sidebarString += '<div class="well print">';
                                sidebarString += '<div class="well-header">PRINTABLE MAPS</div>';
                                $.each(mapValue.sidebar_print_options.buttons, function (sidebarNavKey, sidebarNavValue) {
                                    sidebarString += '<button id="btn-' + sidebarNavValue.slug + '" type="button" class="btn btn-primary map-trigger" data-map="' + sidebarNavValue.slug +
                                        '" role="button" data-toggle="button" onclick="showPrintMap();return false;" target="_self">' + sidebarNavValue.label + '</button>';
                                });
                                sidebarString += '</div>';
                            }

                            sidebarString += '</div>';


                            // brands -->
                            brandLogosString += '<div id="sidebar-logos">';
                            $.each(mapValue.sidebar_brands.brand, function (sidebarBrandKey, sidebarBrandValue) {
                                brandLogosString += '<img class="logo" src="images/adc-directors/brand-logo-' + sidebarBrandValue.slug + '.png" alt="' + sidebarBrandValue.name + '" />';
                            });
                            brandLogosString += '<div id="sidebar-print-map-container">';
                            brandLogosString += '<div id="print-map-icon"><img src="images/adc-directors/icon-print.png" alt="Print Map" /></div>';
                            brandLogosString += '<div id="print-map-text">Print Map</div>';
                            brandLogosString += '</div>';
                            brandLogosString += '</div>';



                            // DIRECTORS -->

                            // empty the directors array-->
                            directors = [];

                            // gather the directors associated to current map -->
                            $.each(mapValue.regional_directors.directors, function (directorKey, directorValue) {
                                var region = directorValue.region_slug;
                                directors.push({
                                    region: directorValue.region,
                                    region_slug: directorValue.region_slug,
                                    name: directorValue.name,
                                    position: directorValue.position,
                                    phone: directorValue.phone
                                });
                            });
                        });

                        $(mapString).appendTo($('.main-content'));
                        $(sidebarString).appendTo($('.sidebar'));
                        $(brandLogosString).insertAfter($('#btn-' + activeMap));

                        // move the selectbox-->
                        mapSelectBox = $('#map-selectbox');
                        $(mapSelectBox).insertBefore('.map-wrapper');
                        $(mapSelectBox).find('ul li:first a').trigger('click').remove();
                        
                        $('.map-wrapper').load('svgs/' + activeMap + '.svg');

                        $('#btn-' + activeMap).addClass('active').css('pointer-events', 'none');

                        $('#sidebar-print-map-container').on('click', function () {
                            showPrintMap();
                        });

                        if (_isAdminModeToggledOn && isUserAdmin()) {
                            $('#adminLink').show();
                        } 

                    },
                    error: function (e) {
                        console.log('error');
                        console.log(e);
                    }
                });
            }

            function hideActiveContainers() {

                $('.main-content').prepend(mapSelectBox);

                // hide active region & sidebar -->
                $('.map-container, .sidebar').fadeOut(fadeOutSpeed, function () {
                    // fadeout complete -->
                });
            }

            function resizeMap() {
                mapWidth = $('#' + activeMap + '-map').width();
                $('.map-wrapper svg').css('height', mapWidth);
            }

            function checkWindowWidth() {
                windowWidth = $(window).width();

                if (windowWidth <= 992) {
                    $('.well.director-info-container').insertAfter('#map-selectbox');

                    if (activeMap != "emea" && activeMap != "latin-america-and-caribbean") {
                        $(mapSelectBox).show();
                    } else {
                        $(mapSelectBox).hide();
                    }
                }

                if (windowWidth > 992) {
                    var sideBarNav = $('.map-container .well.nav').detach();
                    var sidebarDirectorInfo = $('.map-container .well.director-info-container').detach();
                    $(sidebarDirectorInfo).insertAfter('.sidebar-container .well.nav');
                    $(mapSelectBox).hide();
                }
            }

            function showPrintMap() {

                switch (activeMap) {
                    case "focused-service":
                        window.open('http://www.hiltonworldwide.com/assets/pdfs/regional_assignment_map_ac_july_2016-foc-nam.pdf');
                        break;
                    case "full-service":
                        window.open('http://www.hiltonworldwide.com/assets/pdfs/regional_assignment_map_ac-july2016-full-lux-nam.pdf');
                        break;
                    case "tru":
                        window.open('http://www.hiltonworldwide.com/assets/pdfs/regional_assignment_map_ac_july_2016-tru-nam.pdf');
                        break;
                    case "emea":
                        window.open('http://www.hiltonworldwide.com/assets/pdfs/regional-assignment-map-ac-feb-2015-all-emea.pdf');
                        break;
                    case "latin-america-and-caribbean":
                        window.open('http://www.hiltonworldwide.com/assets/pdfs/regional-assignment-map-ac-aug-2015-all-lam.pdf');
                        break;
                }
            }

            init();



        })(jQuery);

        function adminToolsToggled() {

            if (_isAdminModeToggledOn) {
                $('#adminLink').show();
            } else {
                $('#adminLink').hide();
            }

            $.ajax({
                type: "POST",
                url: "LearningLoungeService.asmx/ToggleAdminTools",
                data: "{isOn: " + _isAdminModeToggledOn + "}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                }
            });
        }


    </script>
</asp:Content>
