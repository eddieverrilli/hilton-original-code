﻿using System;
using System.Threading;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI.Activate
{
    public partial class ProcessUserProjectMapping : PageBase
    {
        private IUserManager _userManager;

        /// <summary>
        /// Validate the user and get the manager instance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                //Validate the user
                _userManager = UserManager.Instance;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString[PaymentGatewayConstants.Code] != null)
                {
                    SupressPageHeader();

                    string activationCode = Request.QueryString[PaymentGatewayConstants.Code].ToString();
                    var activationStatus = _userManager.ValidateUserProjectMapping(activationCode);

                    if (activationStatus == 0)
                    {
                        //Based on the transaction status customize the page display
                        Response.Redirect("~/", false);
                    }
                    else if (activationStatus == -1)
                    {
                        // Show error message
                        messageLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.AccountActivationFailed, culture);
                    }
                    else if (activationStatus == -2)
                    {
                        // Show error message
                        messageLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.UserMappedToProjectAndActive, culture);
                    }
                }
                else
                {
                    Response.Redirect("~/AccessDenied.aspx", false);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }
    }
}