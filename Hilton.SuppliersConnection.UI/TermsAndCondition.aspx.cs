﻿using System;
using System.Threading;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class TermsAndCondition : PageBase
    {
        /// <summary>
        /// Invoked on OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                this.Title = ResourceUtility.GetLocalizedString(1114, culture, "Terms And Condition");

                User loggedInUser = (User)Session[SessionConstants.User];
                if (loggedInUser != null && Request.RawUrl != "/TermsAndCondition.aspx" && (loggedInUser.UserTypeId == (int)UserTypeEnum.Administrator || loggedInUser.UserTypeId == (int)UserTypeEnum.Owner || loggedInUser.UserTypeId == (int)UserTypeEnum.Consultant) && loggedInUser.ShowTermsAndCondition)
                {
                    Response.Redirect("~/TermsAndCondition.aspx", false);
                }
                else if (loggedInUser != null && Request.RawUrl != "/TermsAndCondition.aspx" && loggedInUser.UserTypeId != (int)UserTypeEnum.Administrator && !loggedInUser.ShowTermsAndCondition)
                {
                    Response.Redirect("~/AccessDenied.aspx", false);
                }
                else if (loggedInUser == null && Request.RawUrl != "/AccessDenied.aspx")
                {
                    Response.Redirect("~/AccessDenied.aspx", false);
                }
                base.OnInit(e);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Suppress page header
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            SupressPageHeader();
        }

        /// <summary>
        ///  Agree Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AgreeClick(object sender, EventArgs e)
        {
            try
            {
                //If user agree then record the term and condition agreement in the database
                if (Page.IsValid)
                {
                    ISecurityManager _securityManager = SecurityManager.Instance;
                    User user = GetSession<User>(SessionConstants.User);
                    if (user != null)
                    {
                        Acknowledgement acknowledgement = new Acknowledgement()
                       {
                           UserId = user.UserId,
                           TermAndCondition = termsAndConditionTextLabel.Text,
                           TermAndConditionVersion = termsAndConditionVersionHiddenField.Value,
                           IPAddress = string.Empty,
                       };
                        bool isUpdateSuccess = _securityManager.RecordTermsAndConditionAgreement(acknowledgement);
                        if (isUpdateSuccess)
                        {
                            user.ShowTermsAndCondition = false;
                            SetSession<User>(SessionConstants.User, user);
                            Response.Redirect("~/", false);
                        }
                        else
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                        }
                    }
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Validate the admin permission
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void TermsAndCondition_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (!termsAgreementCheckBox.Checked)
                {
                    ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.AcceptTermAndCondition, culture));
                    args.IsValid = false;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Disagree Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DisagreeClick(object sender, EventArgs e)
        {
            //If user disagree, the log out the user and redirect to home page
            (this.Master as WebMaster).LoginLinkButton_Click(null, EventArgs.Empty);
        }

        /// <summary>
        /// Invoked on Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = GetSession<User>(SessionConstants.User);
            if (user != null)
            {
                termsAndConditionTextLabel.Text = user.TermsAndCondition;
                termsAndConditionVersionHiddenField.Value = user.TermsAndConditionVersion;
            }
        }
    }
}