﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.UI.BaseClasses;
using System.Configuration;
using System.Web.Configuration;


namespace Hilton.SuppliersConnection.UI
{
    public partial class WebMaster : System.Web.UI.MasterPage
    {
        private ISecurityManager _securityManager;

        public string directURL { get; set; }
       
        public string GoogleAnalyticsId
        {
            get { return ((PageBase)Page).GoogleAnalyticsId; }
        }
        /// <summary>
        /// web menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void WebMenu_MenuItemClick(Object sender, MenuEventArgs e)
        {
            try
            {
                // Display the text of the menu item selected by the user.
                if (!string.IsNullOrWhiteSpace(e.Item.Value))
                {
                    Response.Redirect(e.Item.Value, true);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        /// Triggered when linked button for admin section is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AdminLinkButton_Click(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Page.ResolveUrl("~") + AppSettingConstants.AdminDashboardUrl, false);
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        ///triggered when button for login is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void LoginLinkButton_Click(Object sender, EventArgs e)
        {
            try
            {
                LogOn();
            }
            catch (ThreadAbortException)
            { }
        }

        /// <summary>
        ///to get the user session from cookie and if does not exists, creates a new cookie
        /// </summary>
        public void LogOn()
        {
            try
            {
                if (loginLinkButton.Text == "Logout")
                {
                    Session.Abandon();
                    loginLinkButton.Text = "Login";
                    HttpCookie workingCookie;
                    int cookiesCount = Request.Cookies.Count;
                    for (int i = 0; i < cookiesCount; i++)
                    {
                        workingCookie = Request.Cookies[i];

                        if (Server.HtmlEncode(workingCookie.Name) == "Lobbyopentoken" || (Server.HtmlEncode(workingCookie.Name) == "SessionID"))
                        {
                            //Update Cookie values
                            workingCookie.Value = "";
                            workingCookie.Expires = DateTime.Now.AddDays(-2d);
                            workingCookie.Domain = ".hilton.com";

                            //Add Cookie in Response collection
                            Response.Cookies.Add(workingCookie);
                        }
                    }
                    Response.Redirect("~/", false);
                }
                else
                {
                    ((PageBase)Page).ClearSession(SessionConstants.User);
                    ((PageBase)Page).ClearSession(SessionConstants.AdminMenu);
                    ((PageBase)Page).ClearSession(SessionConstants.WebMenu);
                    ((PageBase)Page).OnQLogin(string.Empty);
                }
            }
            catch (ThreadAbortException)
            { }
        }

        /// <summary>
        /// Triggered on initialization of page
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        /// Triggered when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                User loggedInUser = (User)Session[SessionConstants.User];
                if (!IsPostBack)
                {
                    adminLinkButton.Visible = false;
                    barImage.Visible = false;
                    

                    if (loggedInUser != null)
                    {
                        bool isAdminMenu = false;
                        userNameLabel.Text = "Welcome, " + loggedInUser.FirstName + " " + loggedInUser.LastName;
                        loginLinkButton.Text = "Logout";

                        if (loggedInUser.UserTypeId == (int)UserTypeEnum.Administrator)
                        {
                            adminLinkButton.Visible = true;
                            barImage.Visible = true;
                        }
                        if (loggedInUser.UserTypeId == (int)UserTypeEnum.Partner)
                        {
                            if (loggedInUser.AssociatedPartners.Count > 1)
                            {
                                userPartnersDropDown.Visible = true;
                                lblCompany.Visible = true;
                                userPartnersDropDown.DataSource = loggedInUser.AssociatedPartners;
                                userPartnersDropDown.DataValueField = "PartnerId";
                                userPartnersDropDown.DataTextField = "CompanyDescription";
                                userPartnersDropDown.DataBind();
                            }
                            else
                            {
                                userPartnersDropDown.Visible = false;
                                lblCompany.Visible = false;
                            }

                        }
                        if (!string.IsNullOrWhiteSpace(loggedInUser.Company))
                        {
                            userPartnersDropDown.SelectedValue = loggedInUser.Company;
                        }
                        GetMenu(loggedInUser.UserTypeId, isAdminMenu, loggedInUser.UserId);

                        //Session Timeout code
                        //webconfig --SessionTimeoutPopup--58 min
                        string ShowPopupTime = System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutPopup"];
                        Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                        SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                        String sessionTimeout = section.Timeout.TotalMinutes.ToString();
                        hidSessionTimeOut.Value = sessionTimeout; //60 mins // 3 min
                        hidShowPopupTime.Value = ShowPopupTime;  //58 mins // 3 min
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "SessionTimeoutCheck", "SessionTimeoutCheck();", true);
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "StartTimer", "StartTimer();", true);
                    }
                    else
                    {
                        GetMenu((int)UserTypeEnum.Visitor, false);
                    }
                }
                else
                {
                    if (loggedInUser != null)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "SessionTimeoutCheck", "SessionTimeoutCheck();", true);
                    }
                }

                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (ThreadAbortException)
            {
            }

        }


        /// <summary>
        //gets the menu list
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="adminMenu"></param>
        private void GetMenu(int userTypeId, bool adminMenu, int userId = 0)
        {
            IList<UserPermissions> userPermissions;
            _securityManager = SecurityManager.Instance;

            IList<UserPermissions> userPermCollection = ((PageBase)Page).GetSession<IList<UserPermissions>>(SessionConstants.WebMenu);
            if (userPermCollection != null && userPermCollection.Count > 0)
            {
                userPermissions = userPermCollection;
            }
            else
            {
                userPermissions = _securityManager.GetMenus(userTypeId, adminMenu, userId);
                ((PageBase)Page).SetSession<IList<UserPermissions>>(SessionConstants.WebMenu, userPermissions);
            }

            User user = ((PageBase)Page).GetSession<User>(SessionConstants.User);

            // Remove Construction Report Permission
            if (user != null && user.PartnershipType == (int)PartnershipTypeEnum.Partner)
            {
                UserPermissions userConstReportPermission = userPermissions.Where(x => x.MenuId == (int)MenuEnum.ConstructionReport).FirstOrDefault();
                
                if (userConstReportPermission != null)
                {
                    ((PageBase)Page).SetSession<UserPermissions>(SessionConstants.ConstructionReportMenu, userConstReportPermission);
                    userPermissions.Remove(userConstReportPermission);
                    ((PageBase)Page).SetSession<IList<UserPermissions>>(SessionConstants.WebMenu, userPermissions);
                }
            }
            else if(user != null && user.PartnershipType == (int)PartnershipTypeEnum.GoldPartner)
            {
                UserPermissions userConstReportPermission = userPermissions.Where(x => x.MenuId == (int)MenuEnum.ConstructionReport).FirstOrDefault();
                if (userConstReportPermission == null)
                {
                    UserPermissions userConstReport = ((PageBase)Page).GetSession<UserPermissions>(SessionConstants.ConstructionReportMenu);
                    if (userConstReport != null)
                    {
                        userPermissions.Add(userConstReport);
                        ((PageBase)Page).SetSession<IList<UserPermissions>>(SessionConstants.WebMenu, userPermissions);
                        ((PageBase)Page).ClearSession(SessionConstants.ConstructionReportMenu);
                    }
                }
            }

            List<UserPermissions> menuPermissions = userPermissions.Where(p => p.ParentMenuId == 0).OrderBy(x => x.DisplayDirection).ToList();

            if (menuBar.Items.Count > 0)
            {
                int count = menuBar.Items.Count;
                for (int i = 0; i < count; i++)
                {
                    menuBar.Items.RemoveAt(0);
                }
            }

            foreach (UserPermissions userPermission in menuPermissions)
            {
                MenuItem menuItem = new MenuItem(userPermission.MenuContent.ToString(CultureInfo.InvariantCulture), userPermission.MenuId.ToString(CultureInfo.InvariantCulture), "", string.IsNullOrWhiteSpace(userPermission.Command.ToString(CultureInfo.InvariantCulture)) ? "javascript:void(0);" : userPermission.Command.ToString(CultureInfo.InvariantCulture));
                menuBar.Items.Add(menuItem);
            }

            var userSubMenus = (from usrSubMenus in userPermissions
                                where usrSubMenus.ParentMenuId != 0
                                select usrSubMenus).OrderBy(x => x.Sequence);

            foreach (UserPermissions userSubMenuPermission in userSubMenus)
            {
                MenuItem menuItem = new MenuItem(userSubMenuPermission.MenuContent.ToString(CultureInfo.InvariantCulture), userSubMenuPermission.ParentMenuId.ToString(CultureInfo.InvariantCulture), "", userSubMenuPermission.Command.ToString(CultureInfo.InvariantCulture));
                menuBar.FindItem(userSubMenuPermission.ParentMenuId.ToString(CultureInfo.InvariantCulture)).ChildItems.Add(menuItem);
            }
        }

        protected void userPartnersDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {

            User loggedInUser = (User)Session[SessionConstants.User];
            if (loggedInUser != null)
            {
                loggedInUser.Company = userPartnersDropDown.SelectedValue;
                loggedInUser.PartnershipType = loggedInUser.AssociatedPartners.Where(x => x.PartnerId == loggedInUser.Company).FirstOrDefault().PartnershipTypeId;
                loggedInUser.IsPartnershipExpired = loggedInUser.AssociatedPartners.Where(x => x.PartnerId == loggedInUser.Company).FirstOrDefault().IsPartnershipExpired;
                ((PageBase)Page).SetSession<User>(SessionConstants.User, loggedInUser);
                GetMenu(loggedInUser.UserTypeId, false, loggedInUser.UserId);
                Response.Redirect("~/Home.aspx?action=IndexChanged", false);
            }
        }
    }
}