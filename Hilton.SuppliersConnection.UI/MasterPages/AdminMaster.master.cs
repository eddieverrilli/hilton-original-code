﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.UI.BaseClasses;
using System.Configuration;
using System.Web.Configuration;

namespace Hilton.SuppliersConnection.UI
{
    public partial class AdminMaster : System.Web.UI.MasterPage
    {
        private ISecurityManager _securityManager;

        /// <summary>
        /// Triggers when menu item is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AdminMenu_MenuItemClick(Object sender, MenuEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(e.Item.Value))
                {
                    string url = ResolveUrl(e.Item.Value);
                    string pageUrl = Request.RawUrl;

                    pageUrl = ((PageBase)Page).SetPageUrl(pageUrl);

                    if (pageUrl.Equals(url, StringComparison.InvariantCultureIgnoreCase))
                    {
                        int index = pageUrl.LastIndexOf("/", StringComparison.InvariantCulture);
                        string uiName = pageUrl.Substring(index + 1);
                        switch (uiName)
                        {
                            case UIConstants.UserListUI:
                                ((PageBase)Page).ClearSession(SessionConstants.UserSearch);
                                break;

                            case UIConstants.TransactionListUI:
                                ((PageBase)Page).ClearSession(SessionConstants.TransactionSearch);
                                break;

                            case UIConstants.ProjectTemplateListUI:
                                ((PageBase)Page).ClearSession(SessionConstants.TemplateSearch);
                                break;

                            case UIConstants.ProjectListUI:
                                ((PageBase)Page).ClearSession(SessionConstants.ProjectSearch);
                                break;

                            case UIConstants.ProductListUI:
                                ((PageBase)Page).ClearSession(SessionConstants.ProductSearch);
                                break;

                            case UIConstants.PartnerListUI:
                                ((PageBase)Page).ClearSession(SessionConstants.PartnerSearch);
                                break;
                        }
                    }
                    else
                    {
                        // Clean the product and transaction session when user is coming from PartnerList or detail page
                        int index = pageUrl.LastIndexOf("/", StringComparison.InvariantCulture);
                        string uiName = pageUrl.Substring(index + 1);
                        switch (uiName)
                        {
                            case UIConstants.PartnerListUI:
                                ((PageBase)Page).ClearSession(SessionConstants.PartnerSearch);
                                ((PageBase)Page).ClearSession(SessionConstants.ProductSearch);
                                ((PageBase)Page).ClearSession(SessionConstants.TransactionSearch);
                                ((PageBase)Page).ClearSession(SessionConstants.PartnerId);
                                break;

                            case UIConstants.ProjectListUI:
                                ((PageBase)Page).ClearSession(SessionConstants.ProductImageBytes);
                                break;

                            case UIConstants.ProductListUI:
                            case UIConstants.ProductDetailsUI:
                                ((PageBase)Page).ClearSession(SessionConstants.UserId);
                                break;
                        }
                    }

                    //Generic session clear
                    ((PageBase)Page).ClearSession(SessionConstants.TemplateDetailSearch);
                    Response.Redirect(e.Item.Value, true);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        ///triggers when logout button is clicked. the session of the current user is ended
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void LogoutLinkButton_Click(Object sender, EventArgs e)
        {
            try
            {
                if (logoutLinkButton.Text == "Logout")
                {
                    Session.Abandon();
                    HttpCookie workingCookie;
                    int cookiesCount = Request.Cookies.Count;
                    for (int i = 0; i < cookiesCount; i++)
                    {
                        workingCookie = Request.Cookies[i];

                        if (Server.HtmlEncode(workingCookie.Name) == "Lobbyopentoken" || (Server.HtmlEncode(workingCookie.Name) == "SessionID"))
                        {
                            //Update Cookie values
                            workingCookie.Value = "";
                            workingCookie.Expires = DateTime.Now.AddDays(-2d);
                            workingCookie.Domain = ".hilton.com";

                            //Add Cookie in Response collection
                            Response.Cookies.Add(workingCookie);
                        }
                    }
                    Response.Redirect("~/", false);
                }
                else
                {
                    ((PageBase)Page).OnQLogin(string.Empty);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        public string GoogleAnalyticsId
        {
            get { return ((PageBase)Page).GoogleAnalyticsId; }
        }
        /// <summary>
        ///triggered when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (Session[SessionConstants.User] != null)
                {
                    User loggedInUser = new User();
                    loggedInUser = (User)Session[SessionConstants.User];
                    bool isAdminMenu = true;

                    GetAdminMenu(loggedInUser.UserTypeId, isAdminMenu);

                    userNameLabel.Text = loggedInUser.FirstName + " " + loggedInUser.LastName;

                    //Session Timeout code
                    //webconfig --SessionTimeoutPopup--58 min
                    string ShowPopupTime = System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutPopup"];
                    Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                    SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                    String sessionTimeout = section.Timeout.TotalMinutes.ToString();
                    hidSessionTimeOut.Value = sessionTimeout; //60 mins // 3 min
                    hidShowPopupTime.Value = ShowPopupTime;  //58 mins // 3 min

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "SessionTimeoutCheck", "SessionTimeoutCheck();", true);
                }
                
                //Bind GoogleAnalyticsId
                Page.Header.DataBind();  
                
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        /// gets the menu for admin
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="adminMenu"></param>
        private void GetAdminMenu(int userTypeId, bool adminMenu)
        {
            IList<UserPermissions> userPermissions = null;
            _securityManager = SecurityManager.Instance;

            IList<UserPermissions> userPermCollection = ((PageBase)Page).GetSession<IList<UserPermissions>>(SessionConstants.AdminMenu);
            if (userPermCollection != null && userPermCollection.Count > 0)
            {
                userPermissions = userPermCollection;
            }
            else
            {
                User user = ((PageBase)Page).GetSession<User>(SessionConstants.User);
                if (user != null)
                {
                    userPermissions = _securityManager.GetMenus(userTypeId, adminMenu, user.UserId);
                    ((PageBase)Page).SetSession<IList<UserPermissions>>(SessionConstants.AdminMenu, userPermissions);
                }
            }
            if (userPermissions != null)
            {
                List<UserPermissions> menuPermissions = userPermissions.Where(p => p.ParentMenuId == 0).OrderBy(x => x.DisplayDirection).ToList();

                if (adminMenuBar.Items.Count > 0)
                {
                    int count = adminMenuBar.Items.Count;
                    for (int i = 0; i < count; i++)
                    {
                        adminMenuBar.Items.RemoveAt(0);
                    }
                }
                foreach (UserPermissions userPermission in menuPermissions)
                {
                    adminMenuBar.Items.Add(new MenuItem(userPermission.MenuContent.ToString(CultureInfo.InvariantCulture), userPermission.Command.ToString(CultureInfo.InvariantCulture), "", string.IsNullOrWhiteSpace(userPermission.Command.ToString(CultureInfo.InvariantCulture)) ? "javascript:void(0);" : userPermission.Command.ToString(CultureInfo.InvariantCulture)));
                }
            
            }
        }
    }
}