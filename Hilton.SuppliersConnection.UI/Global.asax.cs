﻿using System;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Hilton.SuppliersConnection.UI
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (ex is HttpUnhandledException && ex.InnerException != null)
            {
                ex = ex.InnerException;
            }

            Exception errorToThrow;

            if (ExceptionPolicy.HandleException(ex, "AllExceptionsPolicy", out errorToThrow))
            {
                Response.Redirect("~/ErrorPage.aspx");
            }
            else
            {
                Server.ClearError();
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Fires when the session is started
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Session_Start(object sender, EventArgs e)
        {


            if (System.Configuration.ConfigurationManager.AppSettings["UseSSL"].ToLower() == "true")
            {

                if (Response.Cookies.Count > 0)
                {
                    foreach (string s in Response.Cookies.AllKeys)
                    {
                        if (s.ToLower() == "asp.net_sessionid")
                        {
                            Response.Cookies[s].Secure = true;
                        }
                    }
                }

                if (!Request.IsSecureConnection)
                {
                   
                    Response.Redirect(Request.Url.ToString().Replace("http:", "https:"), false);
                }

               
            }
        }
    }
}