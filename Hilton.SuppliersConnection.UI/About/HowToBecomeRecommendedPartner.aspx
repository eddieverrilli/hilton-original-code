﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HowToBecomeRecommendedPartner.aspx.cs"
    ViewStateMode="Inherit" MasterPageFile="~/MasterPages/WebMaster.master" Inherits="Hilton.SuppliersConnection.UI.HowToBecomeRecommendedPartner" %>

<asp:Content ID="contentContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="linkbtnAboutSupplierConnection" runat="server" CBID="892" Text="About Suppliers’ Connection"></asp:HyperLink>
        <span>/</span> <span>
            <asp:Label ID="breadCrumLabel" runat="server" Text="How do I Become a Recommended Partner?"></asp:Label>
        </span>
    </div>
    <div class="main-content">
        <h2>
            <asp:Label ID="headerLabel" runat="server" Text="How do I Become a Recommended Partner?"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="headerDescLabel" runat="server" CBID="901" Text=""></asp:Label>
        </p>
    </div>
    <div class="main">
        <div class="inner-content">
            <h3>
                <asp:Label ID="sectionHeaderLabel" runat="server" CBID="16" Text="How do I become a Recommended Partner"></asp:Label>
            </h3>
            <div class="middle-curve">
                <asp:Label ID="howToBecomePartnerBodyLabel" runat="server" CBID="902" Text=""></asp:Label>
            </div>
        </div>
    </div>
    <div class="sidebar" style="display: none">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="sidePanelHeaderLabel" runat="server" Text="About Suppliers' Connection"></asp:Label></h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="whatIsSuppliersConnectionHyperLink" NavigateUrl="~/About/AboutSupplierConnection.aspx"
                            runat="server" Text="What is Suppliers' Connection?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="howToUseSuppliersConnectionHyperLink" NavigateUrl="~/About/HowToUseSuppliersConnection.aspx"
                            runat="server" Text="How to Use Suppliers' Connection?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="getAnAccountHyperLink" NavigateUrl="~/About/GetAnAccount.aspx"
                            runat="server" Text="How do I Get an Account?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="howToBecomeRecommendedPartnerHyperLink" class="active" runat="server"
                            Text="How do I Become a Recommended Partner?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="contactUsHyperLink" NavigateUrl="~/About/ContactUs.aspx" runat="server"
                            CBID="17" Text="Contact Us"></asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>