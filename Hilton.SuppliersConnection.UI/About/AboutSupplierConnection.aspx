﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" ViewStateMode="Inherit"
    AutoEventWireup="True" CodeBehind="AboutSupplierConnection.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.AboutSupplierConnection" %>

<asp:Content ID="contentContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="aboutSupplierConnectionHyperLink" runat="server" CBID="892" Text="About Suppliers’ Connection"></asp:HyperLink>
        <span>/</span> <span>
            <asp:Label ID="breadCrumLabel" runat="server" Text="What is Suppliers’ Connection?"></asp:Label>
        </span>
    </div>
    <div class="main-content">
        <h2>
            <asp:Label ID="headerLabel" runat="server" Text="What is Suppliers’ Connection?"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="headerDescLabel" runat="server" CBID="894" Text=""></asp:Label>
        </p>
    </div>
    <div class="main">
        <div class="inner-content">
            <h3>
                <asp:Label ID="sectionHeaderLabel" runat="server" CBID="895" Text="What Suppliers' Connection Can Do for You"></asp:Label></h3>
            <div class="middle-curve">
                <asp:Label ID="aboutSupplierConectionBodyLabel" runat="server" CBID="896" Text=""></asp:Label>
            </div>
        </div>
    </div>
    <div class="sidebar" style="display: none">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="sidePanelHeaderLabel" runat="server" Text="About Suppliers' Connection"></asp:Label></h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="whatIsSuppliersConnectionHyperLink" runat="server" class="active"
                            Text="What is Suppliers' Connection?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="howToUseSuppliersConnectionHyperLink" NavigateUrl="~/About/HowToUseSuppliersConnection.aspx"
                            runat="server" Text="How to Use Suppliers' Connection?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="getAnAccountHyperLink" NavigateUrl="~/About/GetAnAccount.aspx"
                            runat="server" Text="How do I Get an Account?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="howToBecomeRecommendedPartnerHyperLink" NavigateUrl="~/About/HowToBecomeRecommendedPartner.aspx"
                            runat="server" Text="How do I Become a Recommended Partner?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="contactUsHyperLink" NavigateUrl="~/About/ContactUs.aspx" runat="server"
                            CBID="17" Text="Contact Us"></asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
