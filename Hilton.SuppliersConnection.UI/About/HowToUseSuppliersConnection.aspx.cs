﻿using System;
using System.Globalization;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class HowToUseSuppliersConnection : PageBase
    {
        /// <summary>
        /// Invoked on OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                this.Title = ResourceUtility.GetLocalizedString(14, culture, "How to Use Suppliers' Connection");
                headerLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(14, culture, headerLabel.Text));
                breadCrumLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(14, culture, breadCrumLabel.Text));
                headerLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(14, culture, headerLabel.Text));
                sidePanelHeaderLabel.Text = ResourceUtility.GetLocalizedString(1, culture, sidePanelHeaderLabel.Text).ToUpper();
                whatIsSuppliersConnectionHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(13, culture, whatIsSuppliersConnectionHyperLink.Text));
                howToUseSuppliersConnectionHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(14, culture, howToUseSuppliersConnectionHyperLink.Text));
                getAnAccountHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(15, culture, getAnAccountHyperLink.Text));
                howToBecomeRecommendedPartnerHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0}", ResourceUtility.GetLocalizedString(16, culture, howToBecomeRecommendedPartnerHyperLink.Text));

                base.OnInit(e);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Invoked on Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}