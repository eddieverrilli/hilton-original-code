﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditProjectUserPopup.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Account.AddEditProjectUserPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../Styles/iframe-modal.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/modal.css" />
    <title></title>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="addCategoryScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        $(window).ready(function () {
            $('.cancelbox').click(function () {
                window.parent.closeIframe();
                return false;

            });
        })

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }

    </script>
    <div id="add-edit" class="modal">
        <h2>
            <asp:Label ID="pageHeader" runat="server"></asp:Label></h2>
        <div class="content">
            <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="resultMessageDiv" runat="server">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div>
                <div class="row">
                    <div class="label">
                        <asp:Label ID="chooseUserLabel" CBID="294" runat="server" Text="Choose User"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="chooseUserDropDownList" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="UserDropDown_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="label">
                        <asp:Label ID="firstNameLabel" CBID="105" runat="server" Text="First Name"></asp:Label>
                    </div>
                    <div class="input textbox mandatory">
                        <asp:TextBox runat="server" ID="firstNameTextBox"></asp:TextBox></div>
                    <div style="margin-left: 160px;">
                        <asp:RequiredFieldValidator ID="firstNameValidator" SetFocusOnError="true" VMTI="1"
                            runat="server" ErrorMessage="Please enter first name" ControlToValidate="firstNameTextBox"
                            CssClass="errorText" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="firstNameRegularExpressionValidator" VMTI="2"
                            SetFocusOnError="true" runat="server" ControlToValidate="firstNameTextBox" ErrorMessage="Please enter valid name"
                            CssClass="errorText" ValidationGroup="Save" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="label">
                        <asp:Label ID="lastNameLabel" CBID="117" runat="server" Text="Last Name"></asp:Label>
                    </div>
                    <div class="input textbox mandatory">
                        <asp:TextBox runat="server" ID="lastNameTextBox"></asp:TextBox></div>
                    <div style="margin-left: 160px;">
                        <asp:RequiredFieldValidator ID="lastNameValidator" runat="server" VMTI="3" ControlToValidate="lastNametextBox"
                            ErrorMessage="Please enter last name" SetFocusOnError="true" CssClass="errorText"
                            Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="lastNameRegularExpressionValidator" SetFocusOnError="true"
                            VMTI="4" runat="server" ControlToValidate="lastNametextBox" ErrorMessage="Please enter valid name"
                            CssClass="errorText" ValidationGroup="Save" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="label">
                        <asp:Label ID="emailAddressLabel" CBID="380" runat="server" Text="Email Address"></asp:Label>
                    </div>
                    <div class="input textbox mandatory">
                        <asp:TextBox runat="server" ID="emailAddressTextBox"></asp:TextBox></div>
                    <div style="margin-left: 160px;">
                        <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" SetFocusOnError="true"
                            VMTI="11" runat="server" ErrorMessage="Correct the format" CssClass="errorText"
                            Display="Dynamic" ControlToValidate="emailAddressTextBox" ValidationGroup="Save"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="requiredEmailValidator" runat="server" SetFocusOnError="true"
                            VMTI="10" ControlToValidate="emailAddressTextBox" ErrorMessage="Please enter email"
                            CssClass="errorText" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="label">
                        <asp:Label ID="roleLabel" CBID="628" runat="server" Text="Role"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="userRoleDropDown" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row select-combo">
                    <div class="label">
                        <asp:Label ID="Label1" CBID="584" runat="server" Text="receive Initial partner communications?"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:RadioButton ID="partnerCommunicationYesRadioButton" GroupName="RecieveCommunication"
                            runat="server" Text="Yes" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="partnerCommunicationNoRadioButton" GroupName="RecieveCommunication"
                            runat="server" Text="No" />
                    </div>
                </div>
            </div>
            <div class="form-btn">
                <asp:LinkButton ID="removeFromProjectLink" runat="server" CssClass="remove-project"
                    ValidationGroup="Save" OnClick="RemoveFromProjectLink_Click" Text="remove from project"
                    CBID="616"></asp:LinkButton>
                <asp:LinkButton ID="TB_closeWindowButton" runat="server" CBID="271" CssClass="cancelbox"
                    Text="CANCEL"></asp:LinkButton>
                <asp:LinkButton ID="saveLink" runat="server" ValidationGroup="Save" CssClass="thankyou simplemodal-overlay"
                    OnClick="SaveLink_Click" CBID="637" Text="SAVE"></asp:LinkButton></div>
            <div class="clear">
                &nbsp;</div>
        </div>
    </div>
    </form>
</body>
</html>