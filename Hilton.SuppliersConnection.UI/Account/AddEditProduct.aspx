﻿<%@ Page Title="" Language="C#" AutoEventWireup="True" MasterPageFile="~/MasterPages/WebMaster.master"
    CodeBehind="AddEditProduct.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.Account.AddEditProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript" src="../Scripts/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../Scripts/custominputs.js"></script>
    <style>
        .ui-dialog-buttonpane.ui-widget-content
        {
            border: none;
            text-align: center;
            margin: 0 auto;
        }
        .ui-dialog-buttonset button
        {
            background: url("../Images/buttons.gif") repeat-x scroll 0 -76px transparent !important;
            border: 1px solid #CECECE !important;
            color: #827672 !important;
            margin-left: 10px;
            margin-top: 0;
            padding: 5px 15px !important;
            text-transform: uppercase;
            border-radius: .3em;
            box-shadow: 0 1px 2px rgba(0,0,0,0.2);
            line-height: 16px;
            cursor: pointer;
            width: 80px !important;
        }
        .ui-dialog
        {
            z-index: 200;
            width: 465px !important;
            position: fixed;
            padding: 0 0 5px 0;
            border: none;
            border-radius: 0;
        }
        .ui-icon-alert
        {
            display: none;
        }
        .ui-dialog-content.ui-widget-content
        {
            display: block;
            width: auto;
            min-height: 50px !important; /* max-height: none; */ /* height: 0px; */
            border: none;
            height: 50px;
            padding: 20px;
        }
        .ui-widget-overlay
        {
            z-index: 99;
            background: #000;
            position: fixed;
        }
        .ui-dialog-titlebar .ui-button-text
        {
            display: none;
        }
        .ui-widget-header button
        {
            width: 29px !important;
            float: right;
            margin-bottom: 0;
            background: url(../Images/cross-img.gif) no-repeat 100% 0 !important;
            border: none !important;
            height: 20px;
            margin-top: 6px;
        }
        .ui-icon-closethick
        {
            visibility: hidden;
        }
        #dialog-confirm, #ProdDelSuccessMsg
        {
            width: auto !important;
            color: #686868;
        }
        .ui-dialog-titlebar.ui-widget-header
        {
            background: url(../Images/popup-header.gif) repeat-x 0 0 !important;
            height: 30px;
            border: none;
            border-radius: 0px;
        }
          .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus
        {
        	cursor: pointer;
        }
    </style>
    <script src="../Scripts/jquery-ui_popup.js"></script>
    <script src="../Scripts/jquery.characterlimiter.js" type="text/javascript"></script>
    <script type="text/javascript">



        function ShowDialog() {
            $mydialog = $("#dialog-confirm").dialog({
                resizable: false,
                height: 140,
                modal: true,
                closeOnEscape: false,
                buttons: {
                    "Yes": function () {
                        $(this).dialog("close");
                        callback(true);
                    },
                    "No": function () {
                        $(this).dialog("close");
                        callback(false);
                    }
                }
            });
        }

        function callback(value) {
            if (value) {
                var prodID = $('#hidProductID').val();
                CallServerMethod_remProd(prodID);
            }
            return value;
        }

        function CallServerMethod_remProd(prodID) {
            $.ajax({
                type: "POST",
                url: "AddEditProduct.aspx/removePrododuct",
                data: '{"prodIDHid":' + prodID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = data.d;
                    if (result == "sucess") {
                        ShowProdDelSuccessMsg();
                    }
                    else if (result == "ErrInProdDel") {
                        $('#cphContent_resultMessageDiv')[0].innerHTML = "There was an error while removing the product. Please try again.";
                    }
                    else {
                        $('#cphContent_resultMessageDiv')[0].innerHTML = "An application error occurred. Please try again. If the problem persists, contact Administrator.";
                    }
                },
                error: function (e) {
                    $('#cphContent_resultMessageDiv')[0].innerHTML = "An application error occurred. Please try again. If the problem persists, contact Administrator.";
                }
            });
        }


        function ShowProdDelSuccessMsg() {
            $mydialog = $("#ProdDelSuccessMsg").dialog({
                resizable: false,
                height: 140,
                modal: true,
                closeOnEscape: false,
                buttons: {
                    "Ok": function () {
                        $(this).dialog("close");
                        window.location = "MyProducts.aspx";
                    }
                }
            });
             $('#ProdDelSuccessMsg').prev().find('button').hide()
        }

        $(document).ready(function () {
            SetFileInputStyles();
            $('textarea#descriptionTextBox').limiter(2000, $('#remainingCharacters'));
            $("#dialog-confirm").hide();
            $("#ProdDelSuccessMsg").hide();
        });

        function SetFileInputStyles() {
            $("input.customInput").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 63
            });

            $("input.customInput1").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 63
            });
        }

        function AjaxFileUpload(control, fileType) {
            debugger;
            if (fileType == 'image') {
                var imageName = $('#ImageUpload')[0].value.toString().substring($('#ImageUpload')[0].value.toString().lastIndexOf('\\') + 1, $('#ImageUpload')[0].value.length);
                $('#<%= imageNameHiddenField.ClientID %>').val(imageName);
            }
            else {
                var pdfName = $('#specSheetFileUpload')[0].value.toString().substring($('#specSheetFileUpload')[0].value.toString().lastIndexOf('\\') + 1, $('#specSheetFileUpload')[0].value.length);
                $('#<%= pdfNameHiddenField.ClientID %>').val(pdfName);
            }
            if (fileType == 'image')
                $('#loading').attr('source', fileType);
            else
                $('#loading1').attr('source', fileType);

            $("#loading")
                .ajaxStart(function (obj) {
                    if ($(this).attr('source') == 'image') {
                        $(this).show();
                        $(this).attr('source', '');
                    }

                })
                .ajaxComplete(function (obj) {
                    $(this).hide();
                });

            $("#loading1")
                .ajaxStart(function (obj) {
                    if ($(this).attr('source') == 'pdf') {
                        $(this).show();
                        $(this).attr('source', '');
                    }

                })
                .ajaxComplete(function (obj) {
                    $(this).hide();
                });

            $.ajaxFileUpload
                    (
                        {
                            url: '../Handlers/FileUpload.ashx?fileType=' + fileType,
                            secureuri: false,
                            fileElementId: control,
                            dataType: 'json',
                            data: { name: 'login', id: 'id' },
                            success: function (data, status) {

                                if (status == 'success' && data.responseText != null && data.responseText == 'uploaded') {
                                    SetFileLink(fileType);

                                    if (fileType == 'image') {
                                        $.ajax({
                                            type: "POST",
                                            url: "../Handlers/FileUpload.ashx?fileType=" + fileType + "&op=show",
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (data, img) {
                                                //debugger;

                                                $('#<%= productImage.ClientID %>').attr('src', "data:image/png;base64," + data);
                                            },
                                            error: function (data, status, e) {

                                            }
                                        });

                                    }

                                }
                                else {
                                    ValidateFile(fileType, data.responseText);
                                }

                            },
                            error: function (data, status, e) {

                            }
                        }
                    )

            return false;

        }

        function disabler(event) {
            event.preventDefault();
            return false;
        }

        function SetFileLink(fileType) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text('');
                    $('#imageErrSpan').hide();
                    $('#<%= imageLinkButton.ClientID %>').text($('#<%= imageNameHiddenField.ClientID %>').val());
                    $('#<%= imageLinkButton.ClientID %>').bind('click', disabler);
                    if ($('#imageNameHiddenField.ClientID').val() != '') {
                        $('#<%= removeImageLinkButton.ClientID %>').show();
                    }
                    break;
                case 'pdf':
                    $('#pdfErrSpan').text('');
                    $('#pdfErrSpan').hide();
                    $('#<%= specSheetPdfLinkButton.ClientID %>').text($('#<%= pdfNameHiddenField.ClientID %>').val());
                    $('#<%= specSheetPdfLinkButton.ClientID %>').bind('click', disabler);
                    if ($('#pdfNameHiddenField.ClientID').val() != '') {
                        $('#<%= removePdfLinkButton.ClientID %>').show();
                    }
                    break;

            }
        }

        function ValidateFile(fileType, msg) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text(msg);
                    $('#imageErrSpan').show();
                    $('#<%= removeImageLinkButton.ClientID %>').hide();
                    $('#<%= imageNameHiddenField.ClientID %>').val("");
                    $('#<%= imageLinkButton.ClientID %>').text("");
                    $('#<%= productImage.ClientID %>').attr('src', '../Images/NoProduct.PNG');
                    break;
                case 'pdf':
                    $('#pdfErrSpan').text(msg);
                    $('#pdfErrSpan').show();
                    $('#<%= removePdfLinkButton.ClientID %>').hide();
                    $('#<%= pdfNameHiddenField.ClientID %>').val("");
                    $('#<%= specSheetPdfLinkButton.ClientID %>').text("");
                    break;
            }

        }

        function ProcessApply() {
            var Inputs = document.getElementsByTagName("input");
            var selectedCategories = '';
            for (var n = 0; n < Inputs.length; ++n) {
                if (Inputs[n].type == 'checkbox' && Inputs[n].checked) {
                    selectedCategories = selectedCategories + Inputs[n].id + ";";
                }
            }

            if (selectedCategories == '')
                document.getElementById('<%=selectedCategoriesHiddenField.ClientID %>').value = "0";
            else
                document.getElementById('<%=selectedCategoriesHiddenField.ClientID %>').value = selectedCategories;

            return true;
        }

        function SelectClickedButton(id, divId) {
            //var linkButtonId = "cphContent_categoryDivisionsRepeater_" + id;
            //var divStandardsId = "cphContent_categoryDivisionsRepeater_" + divId;
            //  alert("abc");
            var linkButtonId = id;
            var divStandardsId = divId;
            //            alert(linkbutton.innerText);
            var linkbutton = document.getElementById(linkButtonId);
            var divStandards = document.getElementById(divStandardsId);
            if (linkbutton.innerText == "Hide Standards") {
                linkbutton.innerText = "Show Standards";
                divStandards.style.display = "none";
            }
            else {
                linkbutton.innerText = "Hide Standards";
                divStandards.style.display = "block";
            }
            return false;
        }

        function checkSelectedCheckbox() {
            //  alert("adf");
            var selectedIds = document.getElementById('<%=selectedCategoriesHiddenField.ClientID %>').value;
            // alert()
            var selectCheckbox = selectedIds.split(";");

            var Inputs = document.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n) {

                if (Inputs[n].type == 'checkbox') {
                    for (var i = 0; i < selectCheckbox.length; i++) {
                        //                        if (Inputs[n].id.substring(0, Inputs[n].id.length - 2) == selectCheckbox[i]) {
                        //                            Inputs[n].checked = true;
                        if (Inputs[n].id == selectCheckbox[i]) {
                            Inputs[n].checked = true;
                        }
                    }
                }
            }
            $('#ImageUpload')[0].disabled = true;
            $('#specSheetFileUpload')[0].disabled = true;
            return true;
        }

        function more_lessItems() {
            $('td.abc').each(function () {
                var max = 1
                if ($(this).find("li").length > max) {
                    var count = $(this).find("li").length;
                    var num = count - 2;
                    if (num != 0) {
                        $(this).find('li:gt(1)').hide();
                        var list = $(this).find('li:gt(1)');
                        $(this).append('<span class="more">+' + num + ' More</span>');
                        $(this).append('<span href="" class="less">Less</span>');
                        $('.more').click(function () {
                            $(this).next().show();
                            $(this).parent().find('li:gt(1)').show()
                            $(this).hide();
                        });
                        $('.less').click(function () {
                            $(this).prev().show();
                            $(this).parent().find('li:gt(1)').hide()
                            $(this).hide();
                        });
                    }
                }
            });
        }

        function InitializeScript() {
            //   alert("init");
            $('.accordion-panel .accordion:first h3').addClass('active');
            $('.accordion-panel .accordion h3').siblings().hide();
            $('.accordion-panel .accordion .panel1:first .panel2').hide();
            $('.accordion-panel .accordion .panel1:first .panel3:first').hide();
            $('.accordion-panel .accordion .description').hide();
            //$('.accordion-panel .accordion:first .panel').show();
            $('.accordion-panel .accordion h3:first').siblings().show();
            $('.accordion-panel .accordion h3').click(function () {
                $(this).toggleClass('active');
                $(this).siblings().slideToggle();

            });
            $('.accordion-panel .accordion h4').click(function () {

                $(this).toggleClass('active');
                $(this).siblings('.panel2').slideToggle();
            });
            $('.accordion-panel .accordion h5').click(function () {
                $(this).toggleClass('active');
                $(this).next().slideToggle();
            });

        }

        $(window).ready(function () {
            InitializeScript();
            more_lessItems();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="myProdsHyperLink" CBID="446" Text="My Products" runat="server"
            NavigateUrl="~/Account/MyProducts.aspx">
        </asp:HyperLink><span>/</span>
        <asp:Label ID="titleLabel" runat="server" CBID="93" Text="Request Product"></asp:Label></div>
    <div class="main-content">
        <asp:Button runat="server" ID="remProductLinkButton" CssClass="btn4" OnClientClick="if ( ! ShowDialog()) return false;"
            meta:resourcekey="BtnUserDeleteResource1" CBID="613" Text="REMOVE PRODUCT" />
        <h2>
            <asp:Label ID="submitHeaderLabel" runat="server"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="bodyLabel" runat="server" CBID="52" Text=""></asp:Label>
        </p>
    </div>
    <div class="main">
        <div class="become-partner addedit-product">
            <div class="accordion">
                <h3 class="none">
                    <asp:Label ID="categoryHeaderLabel" runat="server" CBID="289" Text="Category, Brand, Region and Country">
                    </asp:Label>
                </h3>
                <div class="search-result addedit-padtop">
                    <p style="padding: 1px;">
                        <asp:Label ID="addEditProdIntroLabel" runat="server" CssClass="addEdit-pad" CBID="1124"
                            Text="Please select a category, brand, region and country from the drop down boxes and then select the Filter button.">
                        </asp:Label>
                    </p>
                    <div class="label" style="padding: 1px 1px 1px 1px;">
                        <asp:Label ID="filterLabel" runat="server" CBID="1017" Text="Filter:"></asp:Label>
                    </div>
                    <asp:UpdatePanel ID="dropDownUpdatePanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <div>
                                <asp:DropDownList ID="categoriesDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="CategoriesDropDown_SelectedIndexChanged"
                                    AutoPostBack="true" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="brandDropDown" ViewStateMode="Enabled" AutoPostBack="false"
                                    runat="server">
                                </asp:DropDownList>
                                <%-- <asp:DropDownList ID="propertyTypeDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="PropertyTypeDropDown_SelectedIndexChanged"
                                    AutoPostBack="true" visible="false"  runat="server">
                                </asp:DropDownList>--%>
                                <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged"
                                    AutoPostBack="true" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled" runat="server" AutoPostBack="false">
                                </asp:DropDownList>
                                <br />
                                <div style="width: 250px; margin-left: -0.5px;">
                                    <asp:PlaceHolder ID="parentCategoryContainer" ViewStateMode="Enabled" runat="server">
                                    </asp:PlaceHolder>
                                </div>
                            </div>
                            <div class="go" style="float: right; padding: 0px 36px 0px 0px">
                                <asp:Button ID="filterButton" runat="server" Text="FILTER" OnClick="FilterButton_Click"
                                    CBID="1017" />
                            </div>
                            <asp:HiddenField ID="leafCategoryHdn" runat="server" Value="0" />
                            <asp:HiddenField ID="lastSelectedCategoryDropDown" runat="server" Value="0" />
                            <div class="clear">
                            </div>
                            <div id="searchResultDiv" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="categoryDetailsUpdatePanel" UpdateMode="Conditional" runat="server">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="filterButton" EventName="Click" />
                    </Triggers>
                    <ContentTemplate>
                        <div class="accordion-panel">
                            <div id="divPropertyListDetails" runat="server">
                                <asp:Repeater ID="categoryDivisionsRepeater" runat="server" OnItemDataBound="CategoryDivisionsRepeater_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="accordion">
                                            <div class="property-list-detail" runat="server">
                                                <div id="categoryListDivision" class="accordion" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <asp:HiddenField ID="selectedCategoriesHiddenField" ViewStateMode="Enabled" runat="server"
                                Value="0" />
                            <asp:HiddenField ID="contactIdHiddenField" ViewStateMode="Enabled" runat="server"
                                Value="0" />
                            <div class="clear">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="accordion">
                <h3 class="none">
                    <asp:Label ID="productDetailsLabel" runat="server" CBID="529" Text="Product Details">
                    </asp:Label>
                </h3>
                <div class="contact-form">
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="productNameLabel" runat="server" CBID="535" Text="Product Name"></asp:Label></div>
                        <div class="input">
                            <asp:TextBox ID="productNameTextBox" runat="server" MaxLength="256" CssClass="mandatory">
                            </asp:TextBox>
                        </div>
                        <div class="validation">
                            <asp:RequiredFieldValidator ID="productNameRequiredFieldValidator" SetFocusOnError="true"
                                VMTI="21" runat="server" CssClass="errorText" ControlToValidate="productNameTextBox"
                                ValidationGroup="UpdateProductContact" ViewStateMode="Enabled" Display="Dynamic"
                                ErrorMessage="Product Name is Required.">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="productSKULabel" runat="server" CBID="671" Text="SKU Number"></asp:Label></div>
                        <div class="input">
                            <asp:TextBox ID="productSKUTextBox" MaxLength="128" runat="server">
                            </asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <asp:Label ID="specSheetPDFLabel" runat="server" Text="Spec Sheet PDF" CBID="676"
                                CssClass="label"></asp:Label>
                        </div>
                        <asp:UpdatePanel ID="specSheetUploadUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="input image">
                                    <div class="fileinputs">
                                        <input type="file" size="5px" id="specSheetFileUpload" class="customInput1 customStyle noborder"
                                            name="productPDF" onchange="return AjaxFileUpload('specSheetFileUpload','pdf');" />
                                    </div>
                                    <div class="imgsrc">
                                        <img id="loading1" src="../Images/async.gif" alt="" style="display: none" />
                                        <asp:LinkButton ID="specSheetPdfLinkButton" runat="server" ViewStateMode="Enabled"
                                            OnClick="SpecSheetPdfLinkButton_Click" CssClass="errorText" CausesValidation="false"></asp:LinkButton>
                                        <asp:LinkButton ID="removePdfLinkButton" OnClick="RemovePdfLinkButton_Click" runat="server"
                                            Style="display: none; margin-left: 15px" CssClass="errorText" CausesValidation="false"
                                            Text="X" CBID="752"></asp:LinkButton>
                                    </div>
                                    <span id="pdfErrSpan" class="errorText" style="display: none"></span>
                                    <input type="hidden" id="pdfNameHiddenField" runat="server" value="" />
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="specSheetPdfLinkButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-spec">
                        <asp:Label ID="pdfSpecificationLabel" runat="server" Text="Spec sheet must be PDF file with a maximum size of 10 MB."
                            CBID="1126"></asp:Label></div>
                    <div class="row">
                        <div>
                            <asp:Label ID="productImageLabel" runat="server" Text="Product Image" CBID="532"
                                CssClass="label"></asp:Label></div>
                        <asp:UpdatePanel ID="imageUploadUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="input image">
                                    <p class="imageload">
                                        <img id="loading" src="../Images/async.gif" alt="" class="loadingimg" />
                                        <asp:Image ID="productImage" runat="server" CssClass="pic imagestyle" AlternateText="" /></p>
                                    <div class="fileinputs">
                                        <input type="file" size="5px" id="ImageUpload" class="customInput customStyle noborder"
                                            name="productImage1" onchange="return AjaxFileUpload('ImageUpload','image');" />
                                    </div>
                                    <div class="imgsrc">
                                        <asp:LinkButton ID="imageLinkButton" CssClass="errorText pad-left6" runat="server"
                                            ViewStateMode="Enabled" CausesValidation="false" OnClick="ImageLinkButton_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="removeImageLinkButton" OnClick="RemoveImageLinkButton_Click"
                                            runat="server" Style="display: none; margin-left: 15px" CssClass="errorText"
                                            CausesValidation="false" Text="X" CBID="753"></asp:LinkButton>
                                    </div>
                                    <span id="imageErrSpan" class="errorText" style="display: none"></span>
                                    <input type="hidden" id="imageNameHiddenField" runat="server" value="" />
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="imageLinkButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-spec">
                        <asp:Label ID="companyLogoSizeDescription" runat="server" Text="Images should be JPG or PNG, resized to 235x235 pixels"
                            CBID="1123"></asp:Label></div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="productWebsite" CBID="539" runat="server" Text="Product Website"></asp:Label></div>
                        <div class="input textbox2">
                            <asp:TextBox ID="websiteTextBox" MaxLength="1000" runat="server">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ID="websiteRegularExpressionValidator" SetFocusOnError="true"
                                VMTI="20" runat="server" ErrorMessage="Correct the format" ValidationGroup="UpdateProductContact"
                                CssClass="errorText" Display="Dynamic" ViewStateMode="Enabled" ControlToValidate="websiteTextBox">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="descriptionLabel" runat="server" CBID="527" Text="Description"></asp:Label></div>
                        <div class="input textarea">
                            <asp:TextBox ID="descriptionTextBox" runat="server" ClientIDMode="Static" TextMode="MultiLine"
                                Width="279px">
                            </asp:TextBox>
                            <div class="errorText">
                                <asp:Label runat="server" ID="remainingCharacters" ClientIDMode="Static"></asp:Label>
                                <asp:Label runat="server" ID="remainingCharacterLabel" Text="characters remaining"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <h3 class="none">
                    <asp:Label ID="contactInformationLabel" runat="server" CBID="330" Text="Contact Information">
                    </asp:Label>
                </h3>
                <div class="contact-form">
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="firstNameLabel" runat="server" CBID="102" Text="First Name"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:TextBox ID="firstNameTextBox" runat="server" CssClass="mandatory" MaxLength="128">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="firstNameRequiredField" ViewStateMode="Enabled" SetFocusOnError="true"
                                VMTI="1" runat="server" ErrorMessage="Please provide first name" ValidationGroup="UpdateProductContact"
                                ControlToValidate="firstNameTextBox" CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ViewStateMode="Enabled" ID="firstNameRegularExpressionValidator"
                                SetFocusOnError="true" VMTI="2" runat="server" ControlToValidate="firstNameTextBox"
                                ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="UpdateProductContact"
                                Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="lastNameLabel" runat="server" CBID="114" Text="Last Name"></asp:Label></div>
                        <div class="input">
                            <asp:TextBox ID="lastNameTextBox" runat="server" CssClass="mandatory" MaxLength="128">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="lastNameRequiredField" SetFocusOnError="true"
                                VMTI="3" runat="server" ErrorMessage="Please provide last name" ValidationGroup="UpdateProductContact"
                                ControlToValidate="lastNameTextBox" CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ViewStateMode="Enabled" ID="lastNameRegularExpressionValidator"
                                SetFocusOnError="true" VMTI="4" runat="server" ControlToValidate="lastNameTextBox"
                                ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="UpdateProductContact"
                                Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="titleContactLabel" runat="server" CBID="709"></asp:Label></div>
                        <div class="input">
                            <asp:TextBox runat="server" MaxLength="128" ID="titleContactTextBox">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ViewStateMode="Enabled" ID="titleContactRegularExpressionValidator"
                                SetFocusOnError="true" VMTI="5" runat="server" ControlToValidate="titleContactTextBox"
                                ErrorMessage="Please enter valid title" CssClass="errorText" ValidationGroup="UpdateProductContact"
                                Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="emailLabel" runat="server" CBID="387" Text="Email Address"></asp:Label></div>
                        <div class="input">
                            <asp:TextBox ID="emailTextBox" runat="server" CssClass="mandatory" MaxLength="128">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="emailRequiredField" ViewStateMode="Enabled" SetFocusOnError="true"
                                VMTI="10" runat="server" ErrorMessage="Please provide Email" ValidationGroup="UpdateProductContact"
                                ControlToValidate="emailTextBox" CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" ViewStateMode="Enabled"
                                SetFocusOnError="true" VMTI="11" runat="server" CssClass="errorText" ErrorMessage="Correct the format"
                                ControlToValidate="emailTextBox" ValidationGroup="UpdateProductContact">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="phoneLabel" runat="server" CBID="127" Text="Phone"></asp:Label></div>
                        <div class="input">
                            <asp:TextBox ID="phoneTextBox" runat="server" CssClass="mandatory" MaxLength="32">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="phoneRequiredField" runat="server" ViewStateMode="Enabled"
                                SetFocusOnError="true" VMTI="6" ErrorMessage="Please provide Phone Number" ValidationGroup="UpdateProductContact"
                                ControlToValidate="phoneTextBox" CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="phoneRegularExpressionValidator" ViewStateMode="Enabled"
                                VMTI="7" CssClass="errorText" ControlToValidate="phoneTextBox" ValidationGroup="UpdateProductContact"
                                SetFocusOnError="true" runat="server">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="faxLabel" runat="server" CBID="144" Text="Fax"></asp:Label></div>
                        <div class="input">
                            <asp:TextBox ID="faxTextBox" runat="server" CssClass="mandatory" MaxLength="32">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="faxRequiredField" ViewStateMode="Enabled" SetFocusOnError="true"
                                VMTI="8" runat="server" ErrorMessage="Please provide fax number" ValidationGroup="UpdateProductContact"
                                ControlToValidate="faxTextBox" CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="faxRegularExpressionValidator" SetFocusOnError="true"
                                ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="faxTextBox"
                                ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="UpdateProductContact"
                                Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear">
            &nbsp;</div>
        <asp:UpdatePanel ID="addeditProductUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="box1">
                    <div id="resultMessageDiv" runat="server">
                    </div>
                    <div class="go" style="float: right;">
                        <asp:LinkButton ID="cancelLinkButton" runat="server" ViewStateMode="Enabled" Text="CANCEL"
                            class="gradient-btn" OnClick="cancelLinkButton_Click" CausesValidation="false"></asp:LinkButton>
                        <asp:LinkButton CssClass="btn-one" ID="submitLinkButton" runat="server" OnClientClick="ProcessApply();"
                            OnClick="submitLinkButton_Click" Text="SUBMIT PRODUCT" ViewStateMode="Enabled"
                            ValidationGroup="UpdateProductContact"></asp:LinkButton></div>
                    <div class="clear">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="MY ACCOUNT" CBID="96" runat="server"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li id="constructionReportListItem" runat="server">
                        <asp:HyperLink ID="constructionReportHyperLink" CBID="11" Text="Construction Report"
                            NavigateUrl="~/Account/LeadsAndConstructionReport.aspx" runat="server">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="approvedCategoriesHyperLink" CBID="232" Text="Approved Categories"
                            runat="server">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myProductsHyperLink" CBID="446" Text="My Products" runat="server"
                            NavigateUrl="~/Account/MyProducts.aspx" class="active">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myAccountHyperLink" CBID="58" Text="Account Status" runat="server"
                            NavigateUrl="~/Account/AccountStatus.aspx">
                        </asp:HyperLink></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
    <div id="dialog-confirm">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
            Are you sure you want to remove this product from all Brands regions and countries?</p>
    </div>
    <div id="ProdDelSuccessMsg">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
            Product has been removed from all Brands, Regions and Countries.</p>
    </div>
    <asp:HiddenField ID="hidProductID" runat="server" ClientIDMode="Static" />
</asp:Content>
