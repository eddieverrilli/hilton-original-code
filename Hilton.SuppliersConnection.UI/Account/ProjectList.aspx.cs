﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class MyAccountProjectList : PageBase
    {
        private IOwnerManager _ownerManager;

        /// <summary>
        /// Validate the user, get manager reference and set the grid header
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            ValidateUserAccess((int)MenuEnum.MyProjects);

            _ownerManager = OwnerManager.Instance;
            this.Title = ResourceUtility.GetLocalizedString(96, culture, "My Projects");

            //Setting grid header localized text
            SetGridViewHeaderText();
        }

        /// <summary>
        /// Set the grid view headers
        /// </summary>
        private void SetGridViewHeaderText()
        {
            activeProjectsGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(353, culture, "Date Added");
            activeProjectsGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(559, culture, "Property");
            activeProjectsGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(551, culture, "Project Type");
            activeProjectsGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(236, culture, "Assigned Users");
            activeProjectsGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(499, culture, "Partners Selected ");
        }

        /// <summary>
        /// Bind the data to the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                resultMessageDiv.Attributes.Remove("class");
                resultMessageDiv.InnerText = string.Empty;
                MyProjectsSearch myProjectsSearch;
                myProjectsSearch = PopulateSearchCriteria();

                if (!IsPostBack)
                {
                    User user = GetSession<User>(SessionConstants.User);
                  
                    myProjectsSearch.SortExpression = UIConstants.ProjectAddedDate;
                    myProjectsSearch.SortDirection = UIConstants.AscAbbreviation;
                    SetViewState(ViewStateConstants.SortExpression, UIConstants.ProjectAddedDate);
                    SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                    SearchMyProjects(myProjectsSearch);
                }

                if (GetSession<string>(SessionConstants.TemplateOperationResult) != null)
                {
                    ConfigureResultMessage(resultMessageDiv, "message success", GetSession<string>(SessionConstants.TemplateOperationResult));
                    ClearSession(SessionConstants.TemplateOperationResult);
                }
                resultDivUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Search my projects
        /// </summary>
        /// <param name="myProjectsSearch"></param>
        private void SearchMyProjects(MyProjectsSearch myProjectsSearch)
        {
            IList<Project> projectCollection = GetProjectData(myProjectsSearch);
            if (projectCollection.Count == 0)
            {
                ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoProjectsFoundForUser, culture));
            }
            else
            {
                SetViewState<MyProjectsSearch>(ViewStateConstants.MyProjectsViewState, myProjectsSearch);
                BindData(projectCollection);
            }
        }

        /// <summary>
        /// Populate the search criteria
        /// </summary>
        /// <returns></returns>
        private MyProjectsSearch PopulateSearchCriteria()
        {
            User user = GetSession<User>(SessionConstants.User);
            MyProjectsSearch myProjectsSearch = new MyProjectsSearch()
            {
                UserId = user.UserId
            };
            return myProjectsSearch;
        }

        /// <summary>
        /// Get the project data
        /// </summary>
        /// <param name="myProjectSearch"></param>
        /// <returns></returns>
        private IList<Project> GetProjectData(MyProjectsSearch myProjectSearch)
        {
            IList<Project> projectCollection = _ownerManager.GetProjectListForOwner(myProjectSearch);
            return projectCollection;
        }

        /// <summary>
        /// Bind the data to project grid view
        /// </summary>
        /// <param name="projectCollection"></param>
        private void BindData(IList<Project> projectCollection)
        {
            activeProjectsGridView.DataSource = projectCollection;
            activeProjectsGridView.DataBind();
        }

        /// <summary>
        /// Bind assigned user data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AssignedUsersRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            try
            {
                int? projectId = -1;
                if (args != null && args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
                {
                    projectId = ((User)(args.Item.DataItem)).ProjectId;
                    SetViewState(ViewStateConstants.CurrentProjectId, projectId);

                    if (!((((User)(args.Item.DataItem)).UserTypeDescription.ToString(CultureInfo.InvariantCulture).ToLower(CultureInfo.InvariantCulture)).Equals("owner", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        HyperLink userNameHyperLink = args.Item.FindControl("userNameHyperLink") as HyperLink;
                        userNameHyperLink.Text = ((User)(args.Item.DataItem)).Name;
                        if (isConsultantHiddenField.Value == "1")
                        {
                            userNameHyperLink.Attributes.Remove("class");
                            userNameHyperLink.Attributes.Add("class", "nonedit");
                        }
                        else
                        {
                            userNameHyperLink.Attributes.Remove("class");
                            userNameHyperLink.Attributes.Add("class", "edit thickbox");
                            userNameHyperLink.Text = ((User)(args.Item.DataItem)).Name;
                            userNameHyperLink.NavigateUrl = "AddEditProjectUserPopup.aspx?keepThis=true&mode=edit&project=" + projectId.ToString() + "&user=" + ((User)(args.Item.DataItem)).UserId.ToString(CultureInfo.InvariantCulture) + "&TB_iframe=true&height=480&width=400";
                        }
                        if (!((User)(args.Item.DataItem)).ReceivePartnerCommunication)
                        {
                            var atDiv = (HtmlGenericControl)args.Item.FindControl("atUserName");
                            atDiv.Attributes.Remove("class");
                            atDiv.Attributes.Add("class", "without-at");
                        }
                    }
                    else
                    {
                        Literal userNameLiteral = args.Item.FindControl("userNameLiteral") as Literal;
                        userNameLiteral.Text = ((User)(args.Item.DataItem)).Name;
                    }
                    Literal statusLiteral = args.Item.FindControl("statusLiteral") as Literal;
                    statusLiteral.Text = ((User)(args.Item.DataItem)).UserStatus;
                    Literal roleLiteral = args.Item.FindControl("roleLiteral") as Literal;
                    roleLiteral.Text = ((User)(args.Item.DataItem)).UserRole;
                }
                if (args != null && args.Item.ItemType == ListItemType.Footer)
                {
                    HyperLink addUserHyperLink = args.Item.FindControl("addUserHyperLink") as HyperLink;
                    if (isConsultantHiddenField.Value == "1")
                    {
                        addUserHyperLink.Visible = false;
                    }
                    else
                    {
                        addUserHyperLink.NavigateUrl = "AddEditProjectUserPopup.aspx?keepThis=true&mode=add&project=" + GetViewState(ViewStateConstants.CurrentProjectId).ToString() + "&user=-1&TB_iframe=true&height=480&width=400";
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ActiveProjectsGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            MyProjectsSearch myProjectsSearch = GetViewState<MyProjectsSearch>(ViewStateConstants.MyProjectsViewState);
            myProjectsSearch.SortExpression = sortExpression;
            myProjectsSearch.SortDirection = sortDirection;
            SearchMyProjects(myProjectsSearch);
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ActiveProjectsGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will return the sort colum index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in activeProjectsGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return activeProjectsGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("ProjectList.aspx") || Request.UrlReferrer.ToString().Contains("ProjectList.aspx")))
                    {
                        ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                        if (field.SortExpression == contentSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, contentSearch.SortExpression);
                            if (contentSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return activeProjectsGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// Bind the data to active projects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ActiveProjectsGridView_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton chooseVendorsLinkButton = (LinkButton)args.Row.FindControl("chooseVendorsLinkButton");
                    ScriptManager.GetCurrent(this).RegisterPostBackControl(chooseVendorsLinkButton);

                    Label brand = args.Row.FindControl("propertyBrandLabel") as Label;
                    brand.Text = ((Project)(args.Row.DataItem)).Property.BrandDescription;

                    Label propertyName = args.Row.FindControl("propertyNameLabel") as Label;
                    propertyName.Text = ((Project)(args.Row.DataItem)).Property.FacilityUniqueName;

                    Label propertyAddressOneLabel = args.Row.FindControl("propertyAddressOneLabel") as Label;
                    propertyAddressOneLabel.Text = ((Project)(args.Row.DataItem)).Property.Address;

                    Label propertyAddressTwoLabel = args.Row.FindControl("propertyAddressTwoLabel") as Label;

                    propertyAddressTwoLabel.Text = string.IsNullOrWhiteSpace(((Project)(args.Row.DataItem)).Property.Address2) ? string.Empty : ((Project)(args.Row.DataItem)).Property.Address2 + "<br />";

                    Label propertyCityLabel = args.Row.FindControl("propertyCityLabel") as Label;
                    propertyCityLabel.Text = ((Project)(args.Row.DataItem)).Property.City;

                    Label propertyStateLabel = args.Row.FindControl("propertyStateLabel") as Label;
                    propertyStateLabel.Text = ((Project)(args.Row.DataItem)).Property.State;

                    Label zipCodeLabel = args.Row.FindControl("zipCodeLabel") as Label;
                    zipCodeLabel.Text = ((Project)(args.Row.DataItem)).Property.ZipCode;

                    Label countryName = args.Row.FindControl("countryNameLabel") as Label;
                    countryName.Text = ((Project)(args.Row.DataItem)).Property.Country;

                    Repeater assignedUsersRepeater = args.Row.FindControl("assignedUsersRepeater") as Repeater;
                    //Find the owners to enable or disable user addition and removal
                    User user = GetSession<User>(SessionConstants.User);
                    var owner = (((Project)(args.Row.DataItem)).AssociatedUserCollection.Where(p => p.UserTypeDescription.ToLower() == "owner"));
                    if (owner != null && owner.Any(p => p.UserId == user.UserId))
                        isConsultantHiddenField.Value = "0";
                    else
                        isConsultantHiddenField.Value = "1";
                    assignedUsersRepeater.DataSource = ((Project)(args.Row.DataItem)).AssociatedUserCollection;
                    assignedUsersRepeater.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Navigate to project profile page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chooseVendorsLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
                Context.Items[UIConstants.ProjectId] = Convert.ToString(activeProjectsGridView.DataKeys[row.RowIndex][0], CultureInfo.InstalledUICulture);
                Server.Transfer("~/Account/ProjectProfile.aspx");
            }
            catch (ThreadAbortException)
            { }
        }
    }
}