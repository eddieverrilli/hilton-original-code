﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class AddEditRegionCountries : PageBase
    {
        private IPartnerManager _partnerManager;

        /// <summary>
        /// Invoked at OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                ValidateUserAccess((int)MenuEnum.ApprovedCategories);
                bool isConstructionReportAccessible = ValidateMenuAuthorization((int)MenuEnum.ConstructionReport);
                if (!isConstructionReportAccessible)
                {
                    constructionReportListItem.Visible = false;
                }
                else
                {
                    constructionReportListItem.Visible = true;
                }
                _partnerManager = PartnerManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(91, culture, "Edit Regions/Countries for Approved Categories");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


        /// <summary>
        /// Invoked at Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            resultMessageDiv.Attributes.Remove("class");
            resultMessageDiv.InnerText = string.Empty;

            if (!Page.IsPostBack)
            {
                try
                {
                    if (Session["RegionCountryRequestId"] != null)
                    {
                        RegionCountryRequestId.Value = Session["RegionCountryRequestId"].ToString();
                    }
                    SetLabelValues(RegionCountryRequestId.Value);
                    divbuttons.Visible = false;
                    PopulateDropDownData();
                }
                catch (Exception genericException)
                {
                    UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }
                chooseCategoryUpdatePanel.Update();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(chooseCategoryUpdatePanel, this.GetType(), "SetMultiSelectDropDowns", "SetMultiSelectDropDowns();", true);
                ScriptManager.RegisterClientScriptBlock(categoriesResultUpdatePanel, categoriesResultUpdatePanel.GetType(), "InitializeScript", "InitializeScript();", true);
                ScriptManager.RegisterClientScriptBlock(categoriesResultUpdatePanel, this.GetType(), "moreLess", "more_lessItems()", true);

            }
        }

        private void SetLabelValues(string RegionCountryRequestId)
        {
            switch (RegionCountryRequestId)
            {
                case "1":
                    lblBrCumRegion.Text = "Add Regions/Countries";
                    titleLabel.Text = "Add Regions/Countries";
                    categoryHeaderLabel.Text = "Add Regions/Countries for Approved Categories";
                    lblmaintext.Text = "add";
                    addCategoryLinkButton.Visible = true;
                    deletCategoryLinkButton.Visible = false;
                    break;
                case "2":
                    lblBrCumRegion.Text = "Delete Regions/Countries";
                    titleLabel.Text = "Delete Regions/Countries";
                    categoryHeaderLabel.Text = "Delete Regions/Countries for Approved Categories";
                    lblmaintext.Text = "delete";
                    addCategoryLinkButton.Visible = false;
                    deletCategoryLinkButton.Visible = true;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Populate the static drop downs
        /// </summary>
        private void PopulateDropDownData()
        {
            User user = GetSession<User>(SessionConstants.User);
            int partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

            IList<PartnerCategoryDetails> partnerCategories = _partnerManager.GetCategoryBrandMappingForPartner(partnerId);

            SetViewState<IList<PartnerCategoryDetails>>(ViewStateConstants.CategoriesViewState, partnerCategories);

            if (partnerCategories != null && partnerCategories.Count > 0)
            {

                List<int> allDistinctCategories = partnerCategories.ToList().Select(x => x.CategoryId).Distinct().OrderBy(z => z).ToList();
                bool showMultiSelect = true;
                allDistinctCategories.ForEach(cat =>
                {
                    if (showMultiSelect)
                    {
                        List<PartnerCategoryDetails> partnerCategoriesRows = partnerCategories.ToList().Where(y => y.CategoryId == cat).ToList();

                        partnerCategoriesRows.ForEach(z =>
                        {
                            z.Countries.ToList().ForEach(cr =>
                            {

                                allDistinctCategories.Where(c => c != cat).ToList().ForEach(otherCat =>
                                    {
                                        if (showMultiSelect)
                                        {
                                            List<PartnerCategoryDetails> nextPartnerCategories = partnerCategories.ToList().Where(b => b.CategoryId == otherCat).ToList();


                                            var moveFurther = nextPartnerCategories.Any(zz => zz.BrandId == z.BrandId &&

                                               zz.Countries.ToList().Any(zzc => zzc.CountryId == cr.CountryId && zzc.RegionId == cr.RegionId)

                                                );

                                            if (!moveFurther)
                                            {
                                                showMultiSelect = moveFurther;
                                            }

                                        }

                                    });
                            }
                                );
                        });
                    }

                });

                bool getDropDownMode = showMultiSelect;

                IList<PartnerCategoryDetails> categories = partnerCategories.GroupBy(c => c.CategoryId).Select(group => group.First()).ToList();
                if (showMultiSelect)
                {
                    hdnSelectDropdownMode.Value = "1";
                    singleBrandDropDown.Visible = false;
                    singleCategoryDropDown.Visible = false;
                    BindCategoryDropDown(categories, categoryDropDown);
                    selectedParentCategories.Text = string.Empty;
                    brandCollection.Value = string.Join(",", partnerCategories.Select(p => p.BrandId).Distinct());
                    PopulateCategoryRegionDropDown();
                    OptCountryGroup.Enabled = false;
                }
                else
                {
                    hdnSelectDropdownMode.Value = "0";
                    BindCategoryDropDown(categories, singleCategoryDropDown);
                    singleCategoryDropDown.Items.Insert(0, new ListItem("Select Category", "0"));
                    singleBrandDropDown.Items.Insert(0, new ListItem("Select Brand", "0"));
                    singleBrandDropDown.Enabled = false;
                    singleBrandDropDown.CssClass = "drpstatedisabled";
                    categoryRegionDropDown.Enabled = false;
                    OptCountryGroup.Enabled = false;
                    categoryDropDown.Visible = false;
                    optgroup.Visible = false;
                }
            }
        }

        /// <summary>
        /// Invoked at SingleCategoryDropDown SelectedIndex Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SingleCategoryDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (singleCategoryDropDown.SelectedIndex > 0)
                {
                    string catid = singleCategoryDropDown.SelectedValue.ToString();
                    IList<PartnerCategoryDetails> partnerCategories = GetViewState<IList<PartnerCategoryDetails>>(ViewStateConstants.CategoriesViewState);
                    List<PartnerCategoryDetails> partnercategoryDetails = new List<PartnerCategoryDetails>();

                    partnercategoryDetails = partnerCategories.Where(item => item.CategoryId == Convert.ToInt32(catid)).Distinct().ToList<PartnerCategoryDetails>();

                    singleBrandDropDown.DataSource = partnercategoryDetails.OrderBy(p => p.BrandDescription);
                    singleBrandDropDown.DataTextField = UIConstants.BrandDescription;
                    singleBrandDropDown.DataValueField = UIConstants.BrandId;
                    singleBrandDropDown.DataBind();
                    singleBrandDropDown.Items.Insert(0, new ListItem("Select Brand", "0"));
                    singleBrandDropDown.Enabled = true;
                    singleBrandDropDown.CssClass = string.Empty;
                }
                else
                {
                    singleBrandDropDown.Items.Clear();
                    singleBrandDropDown.Items.Insert(0, new ListItem("Select Brand", "0"));
                    singleBrandDropDown.Enabled = false;
                    singleBrandDropDown.CssClass = "drpstatedisabled";
                }
                selectedRegions.Text = string.Empty;
                categoryRegionDropDown.Items.Clear();
                categoryRegionDropDown.Enabled = false;
                countryCollection.Value = string.Empty;
                OptCountryGroup.Items.Clear();
                OptCountryGroup.Enabled = false;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at SingleBrandDropDown SelectedIndex Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SingleBrandDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (singleCategoryDropDown.SelectedIndex > 0 && singleBrandDropDown.SelectedIndex > 0)
                {
                    string catid = singleCategoryDropDown.SelectedValue.ToString();
                    string brandid = singleBrandDropDown.SelectedValue.ToString();
                    IList<PartnerCategoryDetails> partnerCategories = GetViewState<IList<PartnerCategoryDetails>>(ViewStateConstants.CategoriesViewState);
                    List<PartnerCategoryDetails> partnerApprovedcategoryDetailsForSelectedCategoryandBrand = new List<PartnerCategoryDetails>();

                    //Step 1 - Get partner approved contries and region for selected category and brand
                    partnerApprovedcategoryDetailsForSelectedCategoryandBrand = partnerCategories.Where(item => item.CategoryId == Convert.ToInt32(catid) && item.BrandId == Convert.ToInt32(brandid)).Distinct().ToList<PartnerCategoryDetails>();

                    // Step 2 -- Get Master Country List for filtering
                    List<DropDownItem> countries = PageBase.GetStaticDropDownDataSource(DropDownConstants.CountryDropDown).ToList();

                    // Step 3 -- Get the distinct list of approved countries and regions
                    List<DropDownItem> approvedCountriesAndRegions = new List<DropDownItem>();

                    partnerApprovedcategoryDetailsForSelectedCategoryandBrand.ForEach(x =>
                    {
                        List<Country> countriesRegionCollection = x.Countries.ToList();

                        countriesRegionCollection.ForEach(y =>
                        {
                            approvedCountriesAndRegions.Add(new DropDownItem
                            {
                                DataValueField = y.CountryId.ToString(),
                                ParentDataField = y.RegionId.ToString()
                            });
                        });
                    });

                    if (RegionCountryRequestId.Value == "1")
                    {
                        //Add Mode -- Show only unapproved list of countries and regions

                        // Step 4 -- Remove approved countries and regions from master country/region list
                        approvedCountriesAndRegions.ForEach(z =>
                        {
                            countries.Remove(countries.Where(a => a.DataValueField == z.DataValueField && a.ParentDataField == z.ParentDataField).FirstOrDefault());
                        });


                        // Save filtered country region collection in view state for use in region selection for contry population
                        SetViewState<List<DropDownItem>>(ViewStateConstants.FilteredCountryRegion, countries);

                        // Step 5 -- Bind distinct Regions from remaining data
                        List<DropDownItem> regionMaster = new List<DropDownItem>();

                        regionMaster = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown).ToList();

                        List<DropDownItem> filteredRegionMaster = (from rm in regionMaster join c in countries on rm.DataValueField equals c.ParentDataField select rm).Distinct().ToList();

                        categoryRegionDropDown.Enabled = true;
                        categoryRegionDropDown.DataSource = filteredRegionMaster;// GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
                        categoryRegionDropDown.DataTextField = "DataTextField";
                        categoryRegionDropDown.DataValueField = "DataValueField";
                        categoryRegionDropDown.DataBind();
                        countryCollection.Value = string.Join(",", countries.Select(p => p.DataValueField).Distinct());
                    }
                    else
                    {
                        //Delete Mode  -- Show only approved countries and regions  
                        List<DropDownItem> regionAllMaster = new List<DropDownItem>();
                        regionAllMaster = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown).ToList();


                        List<DropDownItem> countriesMaster = PageBase.GetStaticDropDownDataSource(DropDownConstants.CountryDropDown).ToList();
                        List<DropDownItem> approvedCountriesMaster = (from cm in countriesMaster join r in approvedCountriesAndRegions on cm.DataValueField equals r.DataValueField select cm).Distinct().ToList();

                        // Save filtered country region collection in view state for use in region selection for contry population
                        SetViewState<List<DropDownItem>>(ViewStateConstants.FilteredCountryRegion, approvedCountriesMaster);


                        List<DropDownItem> approvedFilteredRegionMaster = (from rm in regionAllMaster join r in approvedCountriesAndRegions on rm.DataValueField equals r.ParentDataField select rm).Distinct().ToList();

                        //Binding Region dropdown
                        categoryRegionDropDown.Enabled = true;
                        categoryRegionDropDown.DataSource = approvedFilteredRegionMaster;
                        categoryRegionDropDown.DataTextField = "DataTextField";
                        categoryRegionDropDown.DataValueField = "DataValueField";
                        categoryRegionDropDown.DataBind();
                        countryCollection.Value = string.Join(",", countries.Select(p => p.DataValueField).Distinct());

                    }
                }
                else
                {
                    selectedRegions.Text = string.Empty;
                    categoryRegionDropDown.Items.Clear();
                    categoryRegionDropDown.Enabled = false;
                    countryCollection.Value = string.Empty;
                    OptCountryGroup.Items.Clear();
                    OptCountryGroup.Enabled = false;
                }



            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        /// Bind the category drop down
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        private void BindCategoryDropDown(IList<PartnerCategoryDetails> categories, DropDownList sourceDropDown)
        {
            sourceDropDown.DataSource = categories.OrderBy(p => p.CategoryDisplayName);
            sourceDropDown.DataTextField = UIConstants.CategoryDisplayName;
            sourceDropDown.DataValueField = UIConstants.CategoryId;
            sourceDropDown.DataBind();
        }

        /// <summary>
        /// Populate the region drop down
        /// </summary>
        private void PopulateCategoryRegionDropDown()
        {
            // Working in showMultiselect true scenario
            IList<PartnerCategoryDetails> partnerCategories = GetViewState<IList<PartnerCategoryDetails>>(ViewStateConstants.CategoriesViewState);

            int oneCategoryId = partnerCategories.ToList().Select(x => x.CategoryId).FirstOrDefault();

            List<PartnerCategoryDetails> partnerCategoryRow = partnerCategories.ToList().Where(y => y.CategoryId == oneCategoryId).ToList();

            //  Get Master Country List for filtering
            List<DropDownItem> countries = PageBase.GetStaticDropDownDataSource(DropDownConstants.CountryDropDown).ToList();
            if (RegionCountryRequestId.Value == "1")
            {

                partnerCategoryRow.ForEach(z =>
                {
                    int brandid = z.BrandId;

                    z.Countries.ToList().ForEach(cr =>
                    {
                        int countryid = cr.CountryId;
                        int regionid = cr.RegionId;

                        List<PartnerCategoryDetails> nextPartnerBrands = partnerCategories.ToList().Where(b => b.CategoryId == oneCategoryId && b.BrandId != brandid).ToList();
                        bool removeCommonCountryRegion = true;
                        nextPartnerBrands.ForEach(bcr =>
                        {
                            var combinationExists = bcr.Countries.ToList().Any(zzc => zzc.CountryId == cr.CountryId && zzc.RegionId == cr.RegionId);

                            if (!combinationExists)
                            {
                                removeCommonCountryRegion = false;
                            }


                        });


                        if (removeCommonCountryRegion)
                        {
                            countries.Remove(countries.Where(a => a.DataValueField == countryid.ToString() && a.ParentDataField == regionid.ToString()).FirstOrDefault());
                        }

                    });
                });


                // Save filtered country region collection in view state for use in region selection for contry population
                SetViewState<List<DropDownItem>>(ViewStateConstants.FilteredCountryRegion, countries);

                // Step 5 -- Bind distinct Regions from remaining data
                List<DropDownItem> regionMaster = new List<DropDownItem>();

                regionMaster = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown).ToList();

                List<DropDownItem> filteredRegionMaster = (from rm in regionMaster join c in countries on rm.DataValueField equals c.ParentDataField select rm).Distinct().ToList();

                categoryRegionDropDown.DataSource = filteredRegionMaster;//GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
                categoryRegionDropDown.DataTextField = "DataTextField";
                categoryRegionDropDown.DataValueField = "DataValueField";
                categoryRegionDropDown.DataBind();

                countryCollection.Value = string.Join(",", countries.Select(p => p.DataValueField).Distinct());
            }
            else
            {
                // Delete - Show all the approved countries and regions
                List<DropDownItem> regionAllMaster = new List<DropDownItem>();
                regionAllMaster = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown).ToList();
                List<DropDownItem> countriesMaster = PageBase.GetStaticDropDownDataSource(DropDownConstants.CountryDropDown).ToList();

                List<DropDownItem> approvedCountriesAndRegions = new List<DropDownItem>();

                partnerCategoryRow.ForEach(x =>
                {
                    x.Countries.ToList().ForEach(y =>
                    {
                        approvedCountriesAndRegions.Add(new DropDownItem
                        {
                            DataValueField = y.CountryId.ToString(),
                            ParentDataField = y.RegionId.ToString()
                        });
                    });
                });


                List<DropDownItem> approvedCountriesMaster = (from cm in countriesMaster join r in approvedCountriesAndRegions on cm.DataValueField equals r.DataValueField select cm).Distinct().ToList();

                // Save filtered country region collection in view state for use in region selection for contry population
                SetViewState<List<DropDownItem>>(ViewStateConstants.FilteredCountryRegion, approvedCountriesMaster);

                List<DropDownItem> approvedFilteredRegionMaster = (from rm in regionAllMaster join r in approvedCountriesAndRegions on rm.DataValueField equals r.ParentDataField select rm).Distinct().ToList();

                categoryRegionDropDown.DataSource = approvedFilteredRegionMaster;
                categoryRegionDropDown.DataTextField = "DataTextField";
                categoryRegionDropDown.DataValueField = "DataValueField";
                categoryRegionDropDown.DataBind();

                countryCollection.Value = string.Join(",", countries.Select(p => p.DataValueField).Distinct());
            }
        }

        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedRegions_TextChanged(Object sender, EventArgs e)
        {
            try
            {

                string[] regions = selectedRegions.Text.Split(',');
                if (regions.Count() > 0)
                {
                    OptCountryGroup.Enabled = true;
                    List<DropDownItem> countries = GetViewState<List<DropDownItem>>(ViewStateConstants.FilteredCountryRegion);
                    countries = countries.Where(item => regions.Contains(item.ParentDataField)).ToList<DropDownItem>();
                    countryCollection.Value = string.Join(",", countries.Select(p => p.DataValueField).Distinct());
                }

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                categoriesResultUpdatePanel.Update();
            }
        }


        /// <summary>
        /// Add rgion/countries
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void addCategoryLinkButton_Click(object sender, EventArgs e)
        {
            CreateUpdatedRegionCountryList(true);
        }

        /// <summary>
        /// Delete region/country
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void deletCategoryLinkButton_Click(object sender, EventArgs e)
        {
            CreateUpdatedRegionCountryList(false);
        }


        /// <summary>
        /// create Category details updated by partner to add/delete region/country
        /// </summary>
        /// <param name="isAddCounrties"></param>
        private void CreateUpdatedRegionCountryList(bool isAddCounrties)
        {
            IList<PartnerCategoryDetails> partnerCategories = GetViewState<IList<PartnerCategoryDetails>>(ViewStateConstants.CategoriesViewState);
            List<PartnerCategoryDetails> partnercategoryDetails = new List<PartnerCategoryDetails>();
            bool noCountryAdded = true;

            if (hdnSelectDropdownMode.Value == "0")
            {
                if (singleCategoryDropDown.SelectedIndex == 0)
                {
                    selectedParentCategories.Text = string.Empty;
                }
                else
                {
                    selectedParentCategories.Text = singleCategoryDropDown.SelectedValue;
                }

                if (singleCategoryDropDown.SelectedIndex == 0)
                {
                    selectedBrands.Text = string.Empty;
                }
                else
                {
                    selectedBrands.Text = singleBrandDropDown.SelectedValue;
                }
            }


            if (selectedParentCategories.Text == string.Empty || selectedParentCategories.Text.Contains("undefined") || selectedParentCategories.Text.Contains("null") || selectedBrands.Text == string.Empty || selectedBrands.Text.Contains("undefined") || selectedRegions.Text == string.Empty || selectedCountries.Text == string.Empty)
            {
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SelectAlteastOne, culture));

                categoriesResultUpdatePanel.Update();
                return;
            }

            foreach (string categoryId in selectedParentCategories.Text.Split(','))
            {

                foreach (string brandid in selectedBrands.Text.Split(','))
                {
                    PartnerCategoryDetails partnerApprovedCategory = partnerCategories.FirstOrDefault(pc => pc.CategoryId == Convert.ToInt32(categoryId) && pc.BrandId == Convert.ToInt32(brandid));

                    if (partnerApprovedCategory != null) // invalid combination of category and brand
                    {
                        PartnerCategoryDetails categoryDetails = new PartnerCategoryDetails();

                        categoryDetails.CategoryId = Convert.ToInt32(categoryId);
                        categoryDetails.CategoryDisplayName = partnerCategories.FirstOrDefault(cat => cat.CategoryId == Convert.ToInt32(categoryId)).CategoryDisplayName;


                        categoryDetails.BrandId = Convert.ToInt32(brandid);
                        categoryDetails.BrandDescription = partnerCategories.FirstOrDefault(brand => brand.BrandId == Convert.ToInt32(brandid)).BrandDescription;

                        IList<DropDownItem> allCountries = GetStaticDropDownDataSource(DropDownConstants.CountryDropDown);
                        List<Country> countries = new List<Country>();


                        foreach (string countryid in selectedCountries.Text.Split(','))
                        {
                            bool isCountryAlreadyApproved = partnerApprovedCategory.Countries.FirstOrDefault(c => c.CountryId == Convert.ToInt32(countryid)) != null ? true : false;

                            bool isCountryToBeAdded = false;

                            //Add countries
                            if (isAddCounrties && !isCountryAlreadyApproved)
                                isCountryToBeAdded = true;
                            //Delete countries
                            if (!isAddCounrties && isCountryAlreadyApproved)
                                isCountryToBeAdded = true;

                            if (isCountryToBeAdded)
                            {
                                DropDownItem country = allCountries.FirstOrDefault(c => c.DataValueField == countryid);

                                countries.Add(new Country
                                {
                                    CountryId = Convert.ToInt32(country.DataValueField),
                                    CountryName = country.DataTextField,
                                    RegionId = Convert.ToInt32(country.ParentDataField)
                                });
                            }

                        }
                        categoryDetails.Countries = countries;

                        if (countries.Count != 0)
                        {
                            noCountryAdded = false;
                            bool isAllCountrySelected = false;
                            int countCountriesToBeDeleted = 0;
                            if (!isAddCounrties) //check if all countries are being deleted
                            {

                                foreach (Country country in partnerApprovedCategory.Countries)
                                {
                                    if (countries.Exists(c => c.CountryId == country.CountryId))
                                    {
                                        countCountriesToBeDeleted++;

                                    }
                                }

                            }

                            isAllCountrySelected = partnerApprovedCategory.Countries.Count == countCountriesToBeDeleted ? true : false;

                            if (!isAllCountrySelected)
                            {
                                IList<DropDownItem> allregions = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
                                List<Region> regionsDesc = new List<Region>();
                                var regions = countries.GroupBy(item => item.RegionId).Select(grp => grp.First()).ToList();
                                foreach (var item in regions)
                                {
                                    DropDownItem region = allregions.FirstOrDefault(c => c.DataValueField == Convert.ToString(item.RegionId));
                                    regionsDesc.Add(new Region
                                    {
                                        RegionId = Convert.ToInt32(region.DataValueField),
                                        RegionDescription = region.DataTextField,
                                    });
                                }
                                categoryDetails.Regions = regionsDesc;

                                partnercategoryDetails.Add(categoryDetails);
                            }
                        }
                    }

                }

            }
            // In deleting, all selected countries are not approved.
            if (partnercategoryDetails.Count == 0 && !isAddCounrties && noCountryAdded)
            {
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SelectApprovedRegionCountry, culture));

                categoriesResultUpdatePanel.Update();
            }//In deleting partner selected all approved countries to delete.
            else if (partnercategoryDetails.Count == 0 && !isAddCounrties && !noCountryAdded) // delete all region/country
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "ShowAllDeleteMessage", "ShowAllDeleteMessage();", true);
            }//In adding all the region counries are already approved
            else if (partnercategoryDetails.Count == 0 && isAddCounrties) // Add region/country already exists
            {
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SelectNotApprovedRegionCountry, culture));

                categoriesResultUpdatePanel.Update();
            }
            else
            {
                approvedCategoriesGridView.DataSource = partnercategoryDetails;
                approvedCategoriesGridView.DataBind();
                SetViewState<IList<PartnerCategoryDetails>>(ViewStateConstants.CategoriesViewState, partnercategoryDetails);
                if (isAddCounrties)
                    SetViewState<string>(ViewStateConstants.AddOrDelete, "1");
                else
                    SetViewState<string>(ViewStateConstants.AddOrDelete, "0");
                AddDeleteDiv.Visible = false;
                // EnableDisableControls(false);
                if (isAddCounrties)
                    addDeleteMessage.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.AddRegionCountry, culture);
                else
                    addDeleteMessage.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.DeleteRegionCountry, culture);

                divbuttons.Visible = true;
            }

        }

        /// <summary>
        /// Save changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                User user = GetSession<User>(SessionConstants.User);
                int partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

                IList<PartnerCategoryDetails> partnerCategoryDetails = GetViewState<IList<PartnerCategoryDetails>>(ViewStateConstants.CategoriesViewState);


                PartnerCategories partnerCategories = new PartnerCategories();
                string flag = GetViewState<string>(ViewStateConstants.AddOrDelete);
                partnerCategories.UpdateFlag = flag == "1" ? true : false;
                partnerCategories.PartnerCategoriesDetails = partnerCategoryDetails;
                partnerCategories.PartnerId = partnerId;
                partnerCategories.CategoryId = partnerCategoryDetails[0].CategoryId;

                int result = _partnerManager.UpdatePartnerRegionCountries(partnerCategories, user.UserId);
                if (result > 0)
                    Response.Redirect("~/Account/ApprovedCategories.aspx?all=1", false);
                else
                {

                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerUpdateFailure, culture));

                }

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));

            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Account/AddEditRegionCountries.aspx", false);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));

            }
        }

    }
}
