﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="ProjectList.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.MyAccountProjectList" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <script type="text/javascript" src="../Scripts/thickbox.js"></script>
    <script type="text/javascript">
        function closeIframe() {
            $('.close').click();
        }
    </script>
    <div class="breadcrumbs">
        <asp:HyperLink ID="myAccountMainPageHyperLink" runat="server" CBID="96" Text="My Account"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="myProjectsLabel" CBID="60" Text="My Projects" runat="server"></asp:Label>
    </div>
    <div class="main-content">
        <h2>
            <asp:Label ID="myProjectsHeaderLabel" CBID="60" Text="My Projects" runat="server"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="myProjectsBodyLabel" CBID="61" Text="" runat="server"></asp:Label></p>
        <br />
        <div id="resultMessageDiv" runat="server">
        </div>
    </div>
    <div class="main">
        <div class="project-list">
            <div class="accordion">
                <h3>
                    <asp:Label ID="activeProjectLabel" CBID="445" runat="server" Text="My Active Projects"></asp:Label>
                </h3>
                <asp:UpdatePanel ID="resultDivUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="Div1" runat="server">
                        </div>
                        <div class="panel3">
                            <asp:GridView ID="activeProjectsGridView" GridLines="None" ViewStateMode="Enabled"
                                runat="server" AutoGenerateColumns="false" Width="100%" OnSorting="ActiveProjectsGridView_Sorting"
                                DataKeyNames="ProjectId" OnRowCreated="ActiveProjectsGridView_RowCreated" OnRowDataBound="ActiveProjectsGridView_RowDataBound"
                                AllowSorting="true">
                                <Columns>
                                    <asp:BoundField HeaderText="DATE ADDED" SortExpression="ProjectAddedDate" DataField="ProjectAddedDate"
                                        DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-CssClass="col1" />
                                    <asp:TemplateField ItemStyle-CssClass="col2" SortExpression="BrandName" HeaderText="PROPERTY">
                                        <ItemTemplate>
                                            <asp:Label ID="propertyBrandLabel" Font-Bold="true" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyNameLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyAddressOneLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyAddressTwoLabel" runat="server" />
                                            <asp:Label ID="propertyCityLabel" runat="server" />
                                            <span>,</span>
                                            <asp:Label ID="propertyStateLabel" runat="server" />
                                            <asp:Label ID="zipCodeLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="countryNameLabel" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="PROJECT TYPE" SortExpression="ProjectTypeDescription"
                                        DataField="ProjectTypeDescription" ItemStyle-CssClass="col3" />
                                    <asp:TemplateField HeaderText="ASSIGNED USERS" HeaderStyle-CssClass="col4" ItemStyle-CssClass="col4">
                                        <ItemTemplate>
                                            <asp:Repeater ID="assignedUsersRepeater" runat="server" OnItemDataBound="AssignedUsersRepeater_ItemDataBound">
                                                <ItemTemplate>
                                                    <div class="at" runat="server" id="atUserName">
                                                        <span class="edit-pencil">
                                                            <asp:HyperLink ID="userNameHyperLink" runat="server" />
                                                            <asp:Literal ID="userNameLiteral" runat="server" />
                                                            (<asp:Literal ID="statusLiteral" runat="server"></asp:Literal>) </span>
                                                        <br />
                                                        <span>
                                                            <asp:Literal ID="roleLiteral" runat="server"></asp:Literal></span>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div class="add">
                                                        <asp:HyperLink ID="addUserHyperLink" runat="server" Text="Add User" class="thickbox" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PARTNERS SELECTED" ItemStyle-CssClass="col5" SortExpression="ProjectConfiguration">
                                        <ItemTemplate>
                                            <div class="go">
                                                <asp:LinkButton ID="chooseVendorsLinkButton" runat="server" OnClick="chooseVendorsLinkButton_Click"> CHOOSE VENDORS<br />AND PRODUCTS</asp:LinkButton>
                                            </div>
                                            <%# DataBinder.Eval(Container.DataItem, "ConfigPercent")%>
                                            %
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="isConsultantHiddenField" runat="server" Value="0" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box">
            <h3>
                <asp:Label ID="myAccountSideBarLabel" CBID="1036" runat="server" Text="MY ACCOUNT"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="myProjectHyperLink" CssClass="active" runat="server" NavigateUrl=""
                            Text="My Projects"></asp:HyperLink></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>