﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ApprovedCategories : PageBase
    {
        private IPartnerManager _partnerManager;
        private IProductManager _productManager;

        
        /// <summary>
        ///  Register for post backs
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            brands.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(optgroup, p.DataValueField)));
            base.Render(writer);
        }

        /// <summary>
        /// Invoked at ApprovedCategoriesRepeater ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ApprovedCategoriesRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RepeaterItem repeaterItem = e.Item;
                PartnerCategories result = (PartnerCategories)e.Item.DataItem;
                if (((repeaterItem.ItemType == ListItemType.Item) || (repeaterItem.ItemType == ListItemType.AlternatingItem)) && repeaterItem.ItemType != ListItemType.Header)
                {
                    GridView categoriesGridView = new GridView();
                    categoriesGridView = (GridView)repeaterItem.FindControl("approvedCategoriesGridView");

                    categoriesGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(254, culture, "BRAND");
                    categoriesGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(592, culture, "REGION");
                    categoriesGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(0, culture, "COUNTRY");
                    categoriesGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(678, culture, "STANDARDS");

                    categoriesGridView.DataSource = ((PartnerCategories)repeaterItem.DataItem).PartnerCategoriesDetails;
                    categoriesGridView.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Row Data Bound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void categoriesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow gr = e.Row;
                    PartnerCategoryDetails result = (PartnerCategoryDetails)e.Row.DataItem;

                    Repeater repeaterStandards = new Repeater();
                    repeaterStandards = (Repeater)(gr.FindControl("repeaterStandards"));
                    repeaterStandards.DataSource = result.CategoryStandard;
                    repeaterStandards.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at StandardsRepeater ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void StandardRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RepeaterItem repeaterItem = e.Item;
                if (repeaterItem.DataItem != null)
                {
                    CategoryStandard standard = (CategoryStandard)e.Item.DataItem;
                    if (standard.StandardDescription == "-")
                    {
                        var listStandard = e.Item.FindControl("listStandard") as HtmlGenericControl;
                        if (listStandard != null)
                        {
                            listStandard.Style.Add("list-style", "none");
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at GoLinkButton Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoLinkButton_Click(object sender, EventArgs e)
        {
            User user = GetSession<User>(SessionConstants.User);

            try
            {
                if (string.IsNullOrWhiteSpace(selectedBrands.Text))
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", "Please select brand");
                }
                else
                {
                    SearchCriteria searchCriteria = BuildSearchCriteria();
                    searchCriteria.PartnerId = user.Company;
                    IList<PartnerCategories> partnerList = _partnerManager.GetApprovedPartnerCategory(searchCriteria);
                    if (partnerList.Count > 0)
                    {
                        approvedCategoriesRepeater.DataSource = partnerList;
                        approvedCategoriesRepeater.DataBind();
                    }
                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.NoApprovedCategories, culture));
                    }
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);                
                ValidateUserAccess((int)MenuEnum.ApprovedCategories);

                bool isConstructionReportAccessible = ValidateMenuAuthorization((int)MenuEnum.ConstructionReport);
                if (!isConstructionReportAccessible)
                    constructionReportListItem.Visible = false;
                else
                    constructionReportListItem.Visible = true;

                _productManager = ProductManager.Instance;
                _partnerManager = PartnerManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(91, culture, "Approved Categories");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int partnerId;
            if (!Page.IsPostBack)
            {
                try
                {


                    User user = GetSession<User>(SessionConstants.User);
                    partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

                    IList<SearchFilters> searchFilterData = _productManager.GetSearchFilterItems(partnerId);
                    SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilterData);

                    rightPanelHeaderLabel.Text = ResourceUtility.GetLocalizedString(4, culture, "MY ACCOUNT").ToUpper();
                    PopulateDropDownData();

                    //return from Edit Region/countries
                    if (Request.QueryString["all"] != null)
                    {
                        selectedBrands.Text = brandCollection.Value;

                        ScriptManager.RegisterClientScriptBlock(categoriesResultUpdatePanel, categoriesResultUpdatePanel.GetType(), "InitializeScript", "InitializeScript();", true);
                        ScriptManager.RegisterClientScriptBlock(categoriesResultUpdatePanel, this.GetType(), "moreLess", "more_lessItems()", true);
                        ScriptManager.RegisterClientScriptBlock(approvedCategoriesUpdatePanel, this.GetType(), "getItems", "getItems();", true);

                        GoLinkButton_Click(null, null);


                    }
                }
                catch (Exception genericException)
                {
                    UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }


            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(categoriesResultUpdatePanel, categoriesResultUpdatePanel.GetType(), "InitializeScript", "InitializeScript();", true);
                ScriptManager.RegisterClientScriptBlock(categoriesResultUpdatePanel, this.GetType(), "moreLess", "more_lessItems()", true);
                ScriptManager.RegisterClientScriptBlock(approvedCategoriesUpdatePanel, this.GetType(), "getItems", "getItems();", true);
            }
        }

        /// <summary>
        /// Invoked at RegionDropDown SelectedIndex Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] regions = { regionDropDown.SelectedValue };
                BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), 0, true, regions);

            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Builds The Search Criteria
        /// </summary>
        /// <returns></returns>
        private SearchCriteria BuildSearchCriteria()
        {
            SearchCriteria searchCriteria = new SearchCriteria()
            {
                BrandId = selectedBrands.Text,
                RegionId = (regionDropDown.SelectedIndex == 0) ? string.Empty : regionDropDown.SelectedValue,
                CountryId = (countryDropDown.SelectedIndex == 0) ? string.Empty : countryDropDown.SelectedValue,
            };
            return searchCriteria;
        }

        /// <summary>
        /// Populated Drop Down data
        /// </summary>
        private void PopulateDropDownData()
        {
            IList<SearchFilters> searchFilters = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);

            var brandId = Helper.GetSearchFilterDropDownData(DropDownConstants.BrandDropDown, searchFilters).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false);
            brandCollection.Value = string.Join(",", brandId.ToList().Select(p => p.DataValueField));

            BindDropDown(searchFilters, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(220, culture), null);
            BindDropDown(searchFilters, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1301, culture, "All Countries"), null);
        }


    }
}