﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Account
{
    public partial class AddEditProjectUserPopup : PageBase
    {
        private IOwnerManager _ownerManager;
        private IProjectManager _projectManager;

        /// <summary>
        /// Bind the data to control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int currentProjectId = Convert.ToInt32(Request.QueryString["project"], CultureInfo.InvariantCulture);
                var userCollection = _projectManager.GetProjectConsultants(currentProjectId);
                var mode = Request.QueryString["mode"].ToString(CultureInfo.InvariantCulture);
                int currentUserId = Convert.ToInt32(Request.QueryString["user"], CultureInfo.InvariantCulture);

                SetViewState(ViewStateConstants.CurrentProjectId, (int?)currentProjectId);
                SetViewState<string>(ViewStateConstants.PageMode, mode);
                SetViewState<IList<User>>(ViewStateConstants.ConsultantUsers, userCollection);
                SetViewState(ViewStateConstants.CurrentUserId, (int?)currentUserId);

                if (mode.Equals(UIConstants.Edit.ToLowerInvariant(), StringComparison.InvariantCulture))
                {
                    removeFromProjectLink.Visible = true;
                    chooseUserDropDownList.Enabled = false;
                }
                else
                {
                    chooseUserDropDownList.Enabled = true;
                    removeFromProjectLink.Visible = false;
                }
                if (!IsPostBack)
                {
                    PopulateDropDownData();

                    SetRegularExpressions();

                    if (mode.Equals(UIConstants.Edit.ToLowerInvariant(), StringComparison.InvariantCulture))
                    {
                        pageHeader.Text = "Edit Project User";
                        SetViewState(ViewStateConstants.AddOrEdit, UIConstants.Edit);
                    }
                    else
                    {
                        pageHeader.Text = "Add Project User";
                        SetViewState(ViewStateConstants.AddOrEdit, UIConstants.Add);
                    }
                    BindUserDropDown(userCollection);
                    PopulateCurrentConsultantDetails(currentUserId);
                }
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will set the validaion expression for regular expression validator based on culture
        /// </summary>
        private void SetRegularExpressions()
        {
            SetValidatorRegularExpressions(firstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
        }

        /// <summary>
        /// This method will populate the drop down data
        /// </summary>
        protected void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.ConsultantRoleDropDown, userRoleDropDown, string.Empty, null);
        }

        /// <summary>
        /// Get the manager instance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            _ownerManager = OwnerManager.Instance;
            _projectManager = ProjectManager.Instance;
        }

        /// <summary>
        /// Populate the current consultant details
        /// </summary>
        /// <param name="userId"></param>
        private void PopulateCurrentConsultantDetails(int userId)
        {
            try
            {
                if (userId != -1)
                {
                    User currentlySelectedConsultant = new User();
                    var userCollection = GetViewState<IList<User>>(ViewStateConstants.ConsultantUsers) ?? new List<User>();
                    if (userCollection.Count(p => p.UserId == userId) > 1)
                    {
                        if (userCollection.Count(p => p.UserId == userId
                            && p.ProjectId == Convert.ToInt32(GetViewState(ViewStateConstants.CurrentProjectId), CultureInfo.InvariantCulture)) == 1)
                        {
                            currentlySelectedConsultant = userCollection.FirstOrDefault(p => p.UserId == userId
                                && p.ProjectId == Convert.ToInt32(GetViewState(ViewStateConstants.CurrentProjectId), CultureInfo.InvariantCulture));

                            userRoleDropDown.SelectedValue = currentlySelectedConsultant.UserRoleId.ToString();
                            PopulateReceiveCommunicationRadioButton(currentlySelectedConsultant.ReceivePartnerCommunication);
                        }
                        else
                        {
                            currentlySelectedConsultant = userCollection.FirstOrDefault(p => p.UserId == userId);
                            userRoleDropDown.SelectedValue = 1.ToString(CultureInfo.InvariantCulture);
                            PopulateReceiveCommunicationRadioButton(true);
                        }
                    }
                    else
                    {
                        currentlySelectedConsultant = userCollection.FirstOrDefault(p => p.UserId == userId);

                        userRoleDropDown.SelectedValue = currentlySelectedConsultant.UserRoleId.ToString();
                        PopulateReceiveCommunicationRadioButton(currentlySelectedConsultant.ReceivePartnerCommunication);
                    }
                    firstNameTextBox.Text = currentlySelectedConsultant.FirstName;
                    lastNameTextBox.Text = currentlySelectedConsultant.LastName;
                    emailAddressTextBox.Text = currentlySelectedConsultant.Email;
                    chooseUserDropDownList.SelectedValue = userId.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    chooseUserDropDownList.SelectedIndex = 0;
                    firstNameTextBox.Text = string.Empty;
                    lastNameTextBox.Text = string.Empty;
                    emailAddressTextBox.Text = string.Empty;
                    userRoleDropDown.SelectedValue = 1.ToString(CultureInfo.InvariantCulture);
                    PopulateReceiveCommunicationRadioButton(true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Populate Receive Communication RadioButton
        /// </summary>
        /// <param name="receivePartnerCommunication"></param>
        private void PopulateReceiveCommunicationRadioButton(bool receivePartnerCommunication)
        {
            try
            {
                if (receivePartnerCommunication)
                {
                    partnerCommunicationYesRadioButton.Checked = true;
                    partnerCommunicationNoRadioButton.Checked = false;
                }
                else
                {
                    partnerCommunicationYesRadioButton.Checked = false;
                    partnerCommunicationNoRadioButton.Checked = true;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Create user entity
        /// </summary>
        /// <returns></returns>
        private User CreateUserEntity()//tbs standards mapping
        {
            User newUser = null;
            try
            {
                newUser = new User();
                newUser.ProjectId = Convert.ToInt32(GetViewState(ViewStateConstants.CurrentProjectId), CultureInfo.InvariantCulture); //currentProjectId;

                if (GetViewState<string>(ViewStateConstants.AddOrEdit) == UIConstants.Edit)
                {
                    newUser.UserId = Convert.ToInt32(chooseUserDropDownList.SelectedValue, CultureInfo.InvariantCulture);
                }
                newUser.FirstName = firstNameTextBox.Text.Trim();
                newUser.LastName = lastNameTextBox.Text.Trim();
                newUser.Email = emailAddressTextBox.Text.Trim();
                newUser.UserRoleId = Convert.ToInt32(userRoleDropDown.SelectedValue, CultureInfo.InvariantCulture);
                newUser.ReceivePartnerCommunication = partnerCommunicationYesRadioButton.Checked ? true : false;
                newUser.UpdatedByUserId = GetSession<User>(SessionConstants.User).UserId;
                newUser.Culture = Convert.ToString(culture, CultureInfo.InvariantCulture);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            return newUser;
        }

        /// <summary>
        /// Bind the user drop down
        /// </summary>
        /// <param name="userCollection"></param>
        private void BindUserDropDown(IList<User> userCollection)
        {
            try
            {
                chooseUserDropDownList.DataSource = userCollection.Select(p => new { p.Name, p.UserId }).Distinct();
                chooseUserDropDownList.DataTextField = "Name";
                chooseUserDropDownList.DataValueField = "UserId";
                chooseUserDropDownList.DataBind();
                chooseUserDropDownList.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1254, culture, "Add New"), "-1"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Populate the current consultant details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UserDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList chooseUserDropDown = sender as DropDownList;
                var selectedValue = Convert.ToInt32(chooseUserDropDown.SelectedValue.ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
                PopulateCurrentConsultantDetails(selectedValue);
                var userDetails = GetViewState<IList<User>>(ViewStateConstants.ConsultantUsers) ?? new List<User>();
                if (selectedValue != -1)
                {
                    firstNameTextBox.Enabled = false;
                    lastNameTextBox.Enabled = false;
                    emailAddressTextBox.Enabled = false;
                }
                else
                {
                    firstNameTextBox.Enabled = true;
                    lastNameTextBox.Enabled = true;
                    emailAddressTextBox.Enabled = true;
                }

                if (selectedValue == -1 || !userDetails.Any(p => p.UserId == selectedValue
                    && p.ProjectId == Convert.ToInt32(GetViewState(ViewStateConstants.CurrentProjectId), CultureInfo.InvariantCulture)))
                {
                    SetViewState(ViewStateConstants.AddOrEdit, UIConstants.Add);
                }
                else
                {
                    SetViewState(ViewStateConstants.AddOrEdit, UIConstants.Edit);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Save the changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveLink_Click(object sender, EventArgs e)
        {
            try
            {
                int result;

                if (GetViewState<string>(ViewStateConstants.AddOrEdit) == UIConstants.Add)
                {
                    result = _ownerManager.AddProjectUser(CreateUserEntity());
                    if (result > 0)
                    {
                        SetSession<string>(SessionConstants.TemplateOperationResult, ConfigurationStore.GetApplicationMessages(MessageConstants.ProjectConsultantSuccessfullyAdded, culture));
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                    }
                    else if (result == -1)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                    }
                    else if (result == -2)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserTypeIsNotOwnerOrConsultant, culture));
                    }
                    else if (result == -3)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserProjectMappingExists, culture));
                    }
                }
                else
                {
                    result = _ownerManager.UpdateProjectUser(CreateUserEntity());
                    if (result == 0)
                    {
                        SetSession<string>(SessionConstants.TemplateOperationResult, ConfigurationStore.GetApplicationMessages(MessageConstants.ProjectConsultantSuccessfullyAdded, culture));
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                    }
                    else if (result == -1)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                    }
                    else if (result == 2)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserWithUpdatedDetailsExists, culture));
                    }
                }
                resultUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Remove from project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveFromProjectLink_Click(object sender, EventArgs e)
        {
            try
            {
                int result;
                result = _ownerManager.RemoveProjectConsultantAssociation(CreateUserEntity());
                if (result == 0)
                {
                    SetSession<string>(SessionConstants.TemplateOperationResult, ConfigurationStore.GetApplicationMessages(MessageConstants.ProjectConsultantAssociationRemoved, culture));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                }
                else if (result == -1)
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }
                else if (result == 2)
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserProjectMappingError, culture));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}