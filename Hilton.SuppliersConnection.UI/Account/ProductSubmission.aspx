﻿<%@ Page Title="" Language="C#" AutoEventWireup="True" MasterPageFile="~/MasterPages/WebMaster.master"
    CodeBehind="ProductSubmission.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.Account.ProductSubmission" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="myProdsHyperLink" CBID="446" Text="My Products" runat="server"
            NavigateUrl="~/Account/MyProducts.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="titleLabel" runat="server" CBID="93" Text="Product Request"></asp:Label></div>
    <div class="main-content">
        <h2>
            <asp:Label ID="thankyouLabel" runat="server" CBID="1030" Text="Thank You for Your Submission"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="descriptionLabel" runat="server" CBID="1031" Text="This is dummy text for product submission"></asp:Label></p>
    </div>
    <div class="main">
        <div class="box1 thankyou submission">
            <h2>
                <asp:Label ID="nextStepsLabel" runat="server" CBID="1032" Text="Next Steps…"></asp:Label>
            </h2>
            <ul class="nostyle thankyouspace">
                <li><span class="number">1</span>
                    <p>
                        <asp:Label ID="contentLabel1" runat="server" CBID="1033" Text="Step 1 Dummy Text"></asp:Label></p>
                </li>
                <li><span class="number">2</span>
                    <p>
                        <asp:Label ID="contentLabel2" runat="server" CBID="1034" Text="Step 2 Dummy Text"></asp:Label></p>
                </li>
                <li><span class="number">3</span>
                    <p>
                        <asp:Label ID="contentLabel3" runat="server" CBID="1035" Text="Step 3 Dummy Text"></asp:Label></p>
                </li>
            </ul>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="myAccountLabel" runat="server" CBID="96" Text="MY ACCOUNT"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="constructionReportHyperLink" CBID="11" NavigateUrl="~/Account/LeadsAndConstructionReport.aspx"
                            runat="server">CONSTRUCTION REPORT</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="approvedCategoriesHyperLink" CBID="232" runat="server" NavigateUrl="~/Account/ApprovedCategories.aspx">APPROVED CATEGORIES</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myProductsHyperLink" runat="server" CBID="446" class="active"
                            NavigateUrl="~/Account/MyProducts.aspx">MY PRODUCTS</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myAccountHyperLink" runat="server" CBID="58" NavigateUrl="~/Account/AccountStatus.aspx">MY ACCOUNT</asp:HyperLink></li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>