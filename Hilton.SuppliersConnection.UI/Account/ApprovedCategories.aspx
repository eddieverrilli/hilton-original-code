﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="ApprovedCategories.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ApprovedCategories" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script type="text/javascript" src="../Scripts/thickbox.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            getItems();
            InitializeScript();
            more_lessItems();
        });
        function InitializeScript() {

            $('.accordion:first h3, .accordion:first .panel1:first h4, .accordion:first .panel1:first .panel2:first h5').addClass('active');
            $('.accordion:first h3').siblings().show();
            $('.accordion:first .panel1:first .panel2').show();
            $('.accordion:first .panel1:first .panel3:first').show();
            $('.accordion h3').click(function () {
                $(this).toggleClass('active');
                $(this).siblings('.panel').slideToggle();
            });
            $('.accordion h4').siblings('.panel1').toggle();
            $('.accordion h4').click(function () {

                $(this).toggleClass('active');
                $(this).siblings('.panel1').slideToggle();
            });
            $('.accordion h5').click(function () {
                $(this).toggleClass('active');
                $(this).next().slideToggle();
            });

        }

        function more_lessText() {
            $(function () {
                $('.excerpt').wrapInner("<span></span>");
                $('.excerpt').each(function () {
                    $(this).html(formatWords($(this).html(), 50));

                    $(this).children().children('.more_text').hide();

                }).click(function () {
                    var more_text = $(this).children().children('.more_text');
                    var more_link = $(this).children('a.more_link');

                    if (more_text.hasClass('hide')) {
                        more_text.show();
                        more_link.html('Hide');
                        more_text.removeClass('hide');
                    } else {
                        more_text.hide();
                        more_link.html('More');
                        more_text.addClass('hide');
                    }
                    return false;
                });
            });

            function formatWords(sentence, show) {
                var words = sentence.split(' ');
                var new_sentence = '';
                for (i = 0; i < words.length; i++) {
                    if (i <= show) {
                        new_sentence += words[i] + ' ';
                    } else {
                        if (i == (show + 1)) new_sentence += '<span class="more_text hide">';
                        new_sentence += words[i] + ' ';
                        if (words[i + 1] == null) new_sentence += '</span></br><a href="#" class="more_link">More...</a>';
                    }
                }
                return new_sentence;
            }
        }

        function more_lessItems() {
            $('td.col4').each(function () {
                var max = 1
                if ($(this).find("li").length > max) {
                    var count = $(this).find("li").length;
                    var num = count - 2;
                    if (num != 0) {
                        $(this).find('li:gt(1)').hide();
                        var list = $(this).find('li:gt(1)');
                        $(this).append('<span class="more">+' + num + ' More</span>');
                        $(this).append('<span href="" class="less">Less</span>');
                        $('.more').click(function () {
                            $(this).next().show();
                            $(this).parent().find('li:gt(1)').show()
                            $(this).hide();
                        });
                        $('.less').click(function () {
                            $(this).prev().show();
                            $(this).parent().find('li:gt(1)').hide()
                            $(this).hide();
                        });
                    }
                }
            });
        }
        function ConfirmAddDeleteRegionCountry() {
            $('#<%=EditRegionCountryLink.ClientID%>').attr('href', 'AddEditRegionCountryPopup.aspx?keepThis=true&TB_iframe=true&height=300&width=500');
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:LinkButton ID="myAccountHyperLink" CBID="96" Text="My Account" runat="server"></asp:LinkButton>
        <span>/</span>
        <asp:Label ID="myApprovedCategoriesLabel" CBID="232" Text="Approved Categories" runat="server"></asp:Label>
        <span></span>
    </div>
    <div class="main-content">
        <div class="flR go">
            <asp:UpdatePanel ID="regionCountryUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:LinkButton ID="EditRegionCountryLink" runat="server" OnClientClick="ConfirmAddDeleteRegionCountry()"
                        class="thickbox btn copytemplate" CommandArgument="none" CommandName="AddCategory"
                        Text="Click here to Edit Region/Country"></asp:LinkButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <h2>
            <asp:Label ID="titleLabel" runat="server" CBID="47" Text="Approved Categories across Brands & Regions"></asp:Label>
        </h2>
        <p>
            <asp:Label ID="approvedCategoriesContentLabel" CBID="48" runat="server"></asp:Label></p>
    </div>
    <div class="main">
        <div class="approved-category">
            <div class="accordion">
                <asp:UpdatePanel ID="approvedCategoriesUpdatePanel" runat="server">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="selectedBrands" EventName="TextChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <h3>
                            <asp:Label ID="approvedCategoriesLabel" runat="server" CBID="232"></asp:Label></h3>
                        <div class="search-result">
                            <div class="label">
                                <asp:Label ID="filterLabel" runat="server" CBID="771"></asp:Label></div>
                            <div>
                                <div class="brands">
                                    <asp:DropDownList runat="server" ID="optgroup" ViewStateMode="Enabled" ClientIDMode="Static"
                                        multiple="multiple">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="isCheckAll" runat="server" ClientIDMode="Static" />
                                    <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display: none;"></asp:TextBox>
                                </div>
                                <br></br>
                                <div class="region-with-margin">
                                    <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled" AutoPostBack="false"
                                        runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="go">
                                <asp:LinkButton ID="goLinkButton" runat="server" CBID="399" OnClick="GoLinkButton_Click">Go</asp:LinkButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="panel">
                    <asp:UpdatePanel ID="categoriesResultUpdatePanel" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="goLinkButton" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <div id="resultMessageDiv" runat="server">
                            </div>
                            <asp:Repeater ID="approvedCategoriesRepeater" runat="server" OnItemDataBound="ApprovedCategoriesRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <div class="panel">
                                        <h4>
                                            <asp:Label class="acc-open tg" Text="Open" CBID="474" runat="server"></asp:Label><asp:Label
                                                class="acc-close tg" Text="Close" CBID="1118" runat="server"></asp:Label>
                                            <%# DataBinder.Eval(Container.DataItem,"CategoryDisplayName")%>
                                        </h4>
                                        <div class="panel1">
                                            <asp:GridView ID="approvedCategoriesGridView" runat="server" OnRowDataBound="categoriesGridView_RowDataBound"
                                                AlternatingRowStyle-CssClass="odd" GridLines="None" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:ImageField HeaderText="BRAND" ControlStyle-CssClass="pic imagestyle" DataImageUrlField="BrandImageURL"
                                                        ItemStyle-CssClass="col1" HeaderStyle-CssClass="col1" />
                                                    <%--<asp:BoundField HeaderText="PROP. TYPE" DataField="PropertyTypeDescription" ControlStyle-CssClass="col2"
                                                        HeaderStyle-CssClass="col2" />--%>
                                                    <asp:BoundField HeaderText="REGION" DataField="RegionDescription" ItemStyle-CssClass="col2"
                                                        HeaderStyle-CssClass="col2" />
                                                    <asp:TemplateField HeaderText="COUNTRIES">
                                                        <ItemTemplate>
                                                            <itemstyle cssclass="col3"></itemstyle>
                                                            <ul>
                                                                <asp:Repeater ID="repeaterCountries" runat="server" DataSource='<%# Eval("Countries") %>'>
                                                                    <ItemTemplate>
                                                                        <li id="listStandard" runat="server">
                                                                            <%# Eval("CountryName")%><br />
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="col4"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="STANDARDS">
                                                        <ItemTemplate>
                                                            <itemstyle cssclass="col4"></itemstyle>
                                                            <ul>
                                                                <asp:Repeater ID="repeaterStandards" OnItemDataBound="StandardRepeater_ItemDataBound"
                                                                    ViewStateMode="Disabled" runat="server">
                                                                    <ItemTemplate>
                                                                        <li id="listStandard" runat="server">
                                                                            <%# DataBinder.Eval(Container.DataItem,"StandardDescription") %>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="col4"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeaderLabel" Text="MY ACCOUNT" runat="server"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li id="constructionReportListItem" runat="server">
                        <asp:HyperLink ID="constructionReportHyperLink" CBID="11" Text="Construction Report"
                            NavigateUrl="~/Account/LeadsAndConstructionReport.aspx" runat="server"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="approvedCategoriesHyperLink" CBID="232" Text="Approved Categories"
                            runat="server" class="active"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myProductsHyperLink" CBID="446" Text="My Products" runat="server"
                            NavigateUrl="~/Account/MyProducts.aspx"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myAccountStatusHyperLink" CBID="58" Text="Account Status" runat="server"
                            NavigateUrl="~/Account/AccountStatus.aspx"></asp:HyperLink></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>
