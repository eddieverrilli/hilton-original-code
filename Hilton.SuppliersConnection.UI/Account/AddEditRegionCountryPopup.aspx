﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditRegionCountryPopup.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Account.AddEditRegionCountryPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../Styles/iframe-modal.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/modal.css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server" target="_parent">
    <asp:ScriptManager ID="addCategoryScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <div id="add-edit" class="modal">
        <h2>
            <asp:Label ID="pageHeader" runat="server"></asp:Label></h2>
        <div class="content">
            <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="resultMessageDiv" runat="server">
                    </div>
                    <div>
                        <p>
                            Do you wish to Add or Delete Regions and Countries for your Approved Categories?
                        </p>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="form-btn" style="text-align: center">
                <asp:LinkButton ID="btnAddCat" runat="server" OnClick="AddRegionCountry" CssClass="remove-project"
                    Text="ADD"></asp:LinkButton>
                <asp:LinkButton ID="btnDeleteCat" runat="server" OnClick="DeleteRegionCountry" CssClass="remove-project"
                    Text="DELETE"></asp:LinkButton>
            </div>
            <div class="clear">
                &nbsp;</div>
        </div>
    </div>
    </form>
</body>
</html>
