﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="PartnerProductOptions.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.PartnerProductOptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
    <script src="../Scripts/thickbox.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ForceCall() {
            tb_init('a.thickbox');
            imgLoader = new Image();
            imgLoader.src = tb_pathToImage;

        }

        function more_lessItems() {
            $('td.col2').each(function () {
                var max = 1
                if ($(this).find("li").length > max) {
                    var count = $(this).find("li").length;
                    var num = count - 2;
                    if (num != 0) {
                        $(this).find('li:gt(1)').hide();
                        var list = $(this).find('li:gt(1)');
                        $(this).append('<span class="more">+' + num + ' More</span>');
                        $(this).append('<span href="" class="less">Less</span>');
                        $('.more').click(function () {
                            $(this).next().show();
                            $(this).parent().find('li:gt(1)').show()
                            $(this).hide();
                        });
                        $('.less').click(function () {
                            $(this).prev().show();
                            $(this).parent().find('li:gt(1)').hide()
                            $(this).hide();
                        });
                    }
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="myAccountMainPageHyperLink" runat="server" CBID="96" NavigateUrl="~/Account/ProjectList.aspx"
            Text="My Account"></asp:HyperLink>
        <span>/</span>
        <asp:HyperLink ID="myProjectHyperLink" runat="server" Text="My Projects" CBID="1037"
            NavigateUrl="~/Account/ProjectList.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:LinkButton ID="projProfileLinkButton" runat="server" OnClick="projProfileLinkButton_Click"
            CBID="1021" Text="Project Profile"></asp:LinkButton>
        <span>/</span>
        <asp:Label ID="productOptionLabel" runat="server" CBID="1022" Text="Product Options"></asp:Label>
    </div>
    <div class="main-content">
        <h2>
            <asp:Label ID="partnerProductOptionTitle" runat="server" CBID="98"></asp:Label></h2>
        <p>
            <asp:Label ID="partnerProductOptionIntroLabel" runat="server" CBID="1023" Text="Please select one or more partners below. Partners will be notified of your selection.">
            </asp:Label></p>
    </div>
    <div class="main">
        <asp:UpdatePanel ID="partnerProdDetailsUpdatePanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div class="my-account">
                    <div class="accordion">
                        <h3>
                            <asp:Label ID="choosePartnerProductLabel" runat="server" CBID="293"></asp:Label></h3>
                        <div class="panel3">
                            <table width="100%" cellpadding="0" cellspacing="0" class="partnerpro">
                                <tr>
                                    <td class="col1">
                                        <p class="title">
                                            <asp:Label ID="categoryLabel" ViewStateMode="Enabled" runat="server"></asp:Label>
                                        </p>
                                    </td>
                                    <td class="col2">
                                        <p class="title">
                                            <asp:Label ID="brandStandardsLabel" ViewStateMode="Enabled" runat="server" CBID="266"></asp:Label></p>
                                        <div class="standards">
                                            <p>
                                                <ul>
                                                    <asp:Repeater ID="standardsRepeater" ViewStateMode="Enabled" runat="server">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:Label ID="standardNumberLabel" Text='<%# DataBinder.Eval(Container.DataItem, "StandardNumber")%>'
                                                                    ToolTip='<%# DataBinder.Eval(Container.DataItem, "StandardDescription")%>' ViewStateMode="Enabled"
                                                                    runat="server"></asp:Label>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </p>
                                        </div>
                                    </td>
                                    <td class="col3">
                                        <p class="title">
                                            <asp:Label ID="contactLabel" runat="server" ViewStateMode="Enabled" CBID="322"></asp:Label></p>
                                        <p>
                                            <asp:Image ID="straightLineImage" runat="server" ViewStateMode="Enabled" Width="218"
                                                Height="1" ImageUrl="~/Images/dots1.gif" />
                                        </p>
                                        <div class="name">
                                            <p>
                                                <asp:Label ID="contactInfoLabel" ViewStateMode="Enabled" runat="server"></asp:Label>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView BorderStyle="None" ID="grdPartnerProduct" runat="server" ViewStateMode="Enabled"
                                AutoGenerateColumns="false" AlternatingRowStyle-CssClass="odd" class="grid1"
                                GridLines="None" Width="100%" OnRowDataBound="grdPartnerProduct_RowDataBound"
                                OnRowCommand="grdPartnerProduct_RowCommand" DataKeyNames="ProductId,PartnerId,IsSelected,IsOther,CatId">
                                <Columns>
                                    <asp:TemplateField ItemStyle-CssClass="col-1">
                                        <ItemTemplate>
                                            <asp:Image ID="ImgProduct" Width="48" Height="48" class="product" runat="server" />
                                            <div class="details">
                                                <div class="title">
                                                    <h6>
                                                        <asp:Label ID="partnerLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"PartnerName")%>'></asp:Label>
                                                    </h6>
                                                    <span class="feedback">
                                                        <asp:HyperLink ID="imageFeedbackPopupHyperlink" runat="server" class="submit-feedback"
                                                            Visible="false"><img src="../Images/product/comment-icon.png" alt="" width="14" height="13" /></asp:HyperLink></span>
                                                </div>
                                                <p>
                                                    <asp:Label ID="detailLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ProductNumber")%>'></asp:Label></p>
                                                <p>
                                                    <asp:Image ID="ImgpdfIcon" runat="server" ImageUrl='~/Images/product/Images/pdf-icon.gif'
                                                        Width="11" Height="11" class="product" Visible="false" />
                                                    &nbsp;
                                                    <asp:LinkButton ID="downLoadSpecSheetLinkButton" runat="server" CommandName="DownloadPDF"
                                                        Visible="false">Download Spec Sheet</asp:LinkButton>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="col-2">
                                        <ItemTemplate>
                                            <asp:Button ControlStyle-CssClass="btn2" runat="server" ID="btnSelection" CommandName="Select" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="col-3">
                                        <ItemTemplate>
                                            <asp:Image ID="checkedImage" runat="server" AlternateText="" />
                                            <asp:Label ID="checkedStatusLabel" runat="server" CssClass="selectedText" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div id="resultMessageDiv" runat="server">
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3" class="row-field">
                                        <p>
                                            <asp:Label ID="nonAppProdModalLabel" runat="server" CBID="1025" Text="Don’t see a product you want?"></asp:Label>
                                            <asp:HyperLink ID="nonApprovedProdHyperlink" runat="server" class="non-approved-product"
                                                ViewStateMode="Enabled" Text="Identify a non-approved product" CBID="1026"></asp:HyperLink>
                                            .</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="divPaging" runat="server" class="paging">
                        <asp:LinkButton ID="prevLinkButton" runat="server" ViewStateMode="Enabled" class="prev"
                            OnClick="prevLinkButton_Click"></asp:LinkButton>
                        <asp:LinkButton ID="nextLinkButton" runat="server" ViewStateMode="Enabled" class="next"
                            OnClick="nextLinkButton_Click"></asp:LinkButton>
                        <div class="center">
                            <p>
                                <asp:Label ID="pageCategoryLabel" Text="CATEGORY" CBID="0" runat="server"></asp:Label></p>
                            <p>
                                <span>
                                    <asp:Label ID="startPage" ViewStateMode="Enabled" runat="server"></asp:Label>
                                </span>
                                <asp:Label ID="ofLabel" Text="of" runat="server"></asp:Label>
                                <span>
                                    <asp:Label ID="endPage" ViewStateMode="Enabled" runat="server"></asp:Label>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="box1">
            <asp:UpdatePanel ID="bottonConfigurationUpdatePanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="per">
                        <asp:Chart ID="configurationChart1" runat="server" Height="60px" Width="55px">
                            <Titles>
                                <asp:Title ShadowOffset="3" Name="Title1" />
                            </Titles>
                            <Legends>
                                <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                    LegendStyle="Row" />
                            </Legends>
                            <Series>
                                <asp:Series Name="Default" />
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                            </ChartAreas>
                        </asp:Chart>
                        <div class="per-detail">
                            <h4>
                                <asp:Label ID="percentPartnerSelLabel" runat="server"></asp:Label>
                                <span>%</span></h4>
                            <p>
                                <asp:Label ID="partnerSelLabel" CBID="500" Text="PARTNERS SELECTED" runat="server" /></p>
                        </div>
                    </div>
                    <div class="detail">
                        <h3>
                            <asp:Label ID="configIncompleteLabel" runat="server"></asp:Label></h3>
                        <p>
                            <asp:Label ID="configIncompleteMsgLabel" runat="server" />
                        </p>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box">
            <h3>
                <asp:Label ID="myProjectLabel" CBID="1027" Text="MY PROJECTS" CssClass="upper" runat="server"></asp:Label></h3>
            <div class="middle">
                <ul class="nostyle">
                    <li>
                        <asp:HyperLink ID="myProjectRightPanelHyperLink" runat="server" class="active" CBID="1037"
                            Text="My Projects" NavigateUrl="~/Account/ProjectList.aspx"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:LinkButton ID="projectProfileRightPanelLinkButton" runat="server" class="active"
                            OnClick="projProfileLinkButton_Click" CBID="1021" Text="Project Profile"></asp:LinkButton>
                        <ul class="nostyle">
                            <li>
                                <asp:HyperLink ID="productOptionsHyperLink" runat="server" CBID="1022" Text="Product Options"
                                    class="active"></asp:HyperLink>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box">
            <div class="title">
                <h3>
                    <asp:Label ID="partnerSelectionLabel" CBID="496" Text="PARTNER SELECTION" runat="server">
                    </asp:Label></h3>
            </div>
            <div class="content" style="background: #fff;">
                <asp:UpdatePanel ID="rightPanelUpdatePanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="per">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Chart ID="configurationChart" runat="server" Height="80px" Width="65px">
                                            <Titles>
                                                <asp:Title ShadowOffset="3" Name="Title1" />
                                            </Titles>
                                            <Legends>
                                                <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                                    LegendStyle="Row" />
                                            </Legends>
                                            <Series>
                                                <asp:Series Name="Default" />
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                                            </ChartAreas>
                                        </asp:Chart>
                                    </td>
                                </tr>
                            </table>
                            <div class="per-detail">
                                <h4>
                                    <asp:Label ID="percentLabel" runat="server"></asp:Label>
                                    <asp:Label ID="percentSignLabel" runat="server" Text="%"></asp:Label>
                                </h4>
                                <p>
                                    <asp:Label ID="partnerSelectedLabel" Text="PARTNERS SELECTED" CBID="1028" runat="server">
                                    </asp:Label></p>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="detail">
                    <h4>
                        <asp:Label runat="server" ID="sidebarBrandNameLabel"></asp:Label></h4>
                    <div>
                        <asp:Label runat="server" ID="sidebarfacilityNameLabel"></asp:Label><br />
                        <asp:Label runat="server" ID="sidebarAddressLabel"></asp:Label><br />
                        <div id="address2Div" runat="server" visible="false">
                            <asp:Label runat="server" ID="sidebarAddress2Label"></asp:Label>
                        </div>
                        <asp:Label runat="server" ID="sidebarCityLabel"></asp:Label>,
                        <asp:Label runat="server" ID="sidebarStateLabel"></asp:Label>
                        <asp:Label runat="server" ID="sidebarZipCodeLabel"></asp:Label>
                        <br />
                        <asp:Label runat="server" ID="sidebarCountryNameLabel"></asp:Label>
                    </div>
                </div>
                <div class="border">
                    &nbsp;
                </div>
                <div class="return">
                    <asp:LinkButton ID="backToPropertyLabel" runat="server" OnClick="backToPropertyLabel_Click"
                        CBID="1029" Text="Return to Property Configuration"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;
    </div>
</asp:Content>