﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI.Account
{
    public partial class AddEditRegionCountryPopup : PageBase
    {
        
        /// <summary>
        /// Get the manager instance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);           
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddRegionCountry(object sender, EventArgs e)
        {
            Session.Remove("RegionCountryRequestId");
            SetSession<string>("RegionCountryRequestId", "1");
            Response.Redirect("AddEditRegionCountries.aspx", true);
        }

        protected void DeleteRegionCountry(object sender, EventArgs e)
        {
            Session.Remove("RegionCountryRequestId");
            SetSession<string>("RegionCountryRequestId", "2");
            Response.Redirect("AddEditRegionCountries.aspx", true);
        }
    }
}