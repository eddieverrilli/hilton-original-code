﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="LeadsAndConstructionReport.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.LeadsAndConstructionReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <script type="text/javascript" src="../Scripts/thickbox.js"></script>
    <div class="breadcrumbs">
        <asp:HyperLink ID="HyperLink1" CBID="96" Text="My Account" runat="server"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="constReportLabel" CBID="11" runat="server" Text="Construction Report"></asp:Label><span></span>
    </div>
    <div class="main-content">
        <h2>
            <asp:Label ID="PageTileLabel" CBID="55" runat="server" Text="Leads & Construction Report"></asp:Label></h2>
        <p>
            <asp:Label ID="leadsAndConstrcutionContentLabel" runat="server" CBID="56"></asp:Label></p>
    </div>
    <div class="main">
        <div id="resultMessageDiv" runat="server">
        </div>
        <div class="lead-construction">
            <div class="accordion">
                <h3>
                    <asp:Label ID="hotLeadsLabel" runat="server" CBID="418" Text="Hot Leads"></asp:Label>
                </h3>
                <asp:UpdatePanel ID="hotLeadConstructionReportUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel3">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="eligible" colspan="6">
                                        <asp:Label ID="headerLabel" runat="server" CBID="1018" Text="Where your product is being actively considered for a project"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="mg-right">
                                            <div id="hotLeadDiv" runat="server">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <%--gridview for hot leads--%>
                            <asp:GridView ID="GridViewHotLeads" runat="server" OnRowDataBound="GridViewHotLeads_OnRowDataBound"
                                OnSorting="GridViewHotLeads_Sorting" AutoGenerateColumns="false" GridLines="None"
                                DataKeyNames="ProductId" OnRowCreated="GridViewHotLeads_RowCreated" AllowSorting="true">
                                <Columns>
                                    <asp:BoundField HeaderText="" DataField="AddedDate" DataFormatString="{0:dd-MMM-yyyy}"
                                        SortExpression="ProjectAddedDate" ItemStyle-CssClass="col1" />
                                    <asp:BoundField HeaderText="" SortExpression="Category" DataField="CategoryDisplayName"
                                        ItemStyle-CssClass="col2" />
                                    <asp:BoundField HeaderText="PRODUCT" SortExpression="ProductName" DataField="ProductName"
                                        ItemStyle-CssClass="col3" />
                                    <asp:BoundField SortExpression="ProjectTypeDescription" DataField="ProjectTypeName"
                                        ItemStyle-CssClass="col4" />
                                    <asp:TemplateField ItemStyle-CssClass="col5" HeaderText="PROPERTY" SortExpression="BrandName">
                                        <ItemTemplate>
                                            <asp:Label ID="propertyBrandLabel" Font-Bold="true" runat="server" />
                                            <br />
                                            <asp:Label ID="facilityNameLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyAddressOneLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyAddressTwoLabel" Visible="false" runat="server" />
                                            <asp:Label ID="propertyCityLabel" runat="server" />
                                            <span>,</span>
                                            <asp:Label ID="propertyStateLabel" runat="server" />
                                            <asp:Label ID="zipCodeLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyCountryLabel" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="col6" HeaderText="" SortExpression="MessageDate">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="ContactRepHyperLink" runat="server" CBID="334" Text="CONTACT REP"
                                                CssClass="thickbox"></asp:HyperLink>
                                            <asp:Label ID="ContactRepLabel" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="divHotLeadPaging" runat="server" class="paging">
                            <asp:LinkButton ID="hotLeadPrevLinkButton" runat="server" class="prev" ViewStateMode="Enabled"
                                OnClick="HotLeadPrevLinkButton_Click"></asp:LinkButton>
                            <asp:LinkButton ID="hotLeadNextLinkButton" runat="server" class="next" ViewStateMode="Enabled"
                                OnClick="HotLeadNextLinkButton_Click"></asp:LinkButton>
                            <div class="center">
                                <p>
                                    PAGE</p>
                                <p>
                                    <span>
                                        <asp:Label ID="hotLeadStartPage" runat="server" ViewStateMode="Enabled">1</asp:Label>
                                    </span>of <span>
                                        <asp:Label ID="hotLeadEndPage" runat="server" ViewStateMode="Enabled"></asp:Label>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="accordion">
                <h3>
                    <asp:Label ID="warmLeadsLabel" runat="server" CBID="743" Text="Warm Leads"></asp:Label></h3>
                <asp:UpdatePanel ID="warmLeadConstructionReportUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel3">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="eligible" colspan="6">
                                        <asp:Label ID="warmLeadsHeaderLabel" runat="server" CBID="1019" Text="Projects for which your
        products are eligible"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="mg-right">
                                            <div id="warmLeadDiv" runat="server">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView ID="GridViewWarmLeads" runat="server" OnSorting="GridViewWarmLeads_Sorting"
                                OnRowDataBound="GridViewWarmLeads_OnRowDataBound" DataKeyNames="ProductId" OnRowCreated="GridViewWarmLeads_RowCreated"
                                AutoGenerateColumns="false" GridLines="None" AllowSorting="true">
                                <Columns>
                                    <asp:BoundField HeaderText="" DataField="AddedDate" SortExpression="ProjectAddedDate"
                                        DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-CssClass="col1" />
                                    <asp:BoundField HeaderText="" SortExpression="Category" DataField="CategoryDisplayName"
                                        ItemStyle-CssClass="col2" />
                                    <asp:BoundField HeaderText="" SortExpression="ProductName" DataField="ProductName"
                                        ItemStyle-CssClass="col3" />
                                    <asp:BoundField HeaderText="" SortExpression="ProjectTypeDescription" DataField="ProjectTypeName"
                                        ItemStyle-CssClass="col4" />
                                    <asp:TemplateField ItemStyle-CssClass="col5" SortExpression="BrandName" HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="propertyBrandLabel" Font-Bold="true" runat="server" />
                                            <br />
                                            <asp:Label ID="facilityNameLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyAddressOneLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyAddressTwoLabel" Visible="false" runat="server" />
                                            <asp:Label ID="propertyCityLabel" runat="server" />
                                            <span>,</span>
                                            <asp:Label ID="propertyStateLabel" runat="server" />
                                            <asp:Label ID="zipCodeLabel" runat="server" />
                                            <br />
                                            <asp:Label ID="propertyCountryLabel" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="col6" SortExpression="MessageDate" HeaderText="">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="ContactRepHyperLink" runat="server" CBID="334" CssClass="thickbox"
                                                Text="Contact Rep"></asp:HyperLink>
                                            <asp:Label ID="ContactRepLabel" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="divWarmLeadPaging" runat="server" class="paging">
                            <asp:LinkButton ID="warmLeadPrevLinkButton" runat="server" class="prev" ViewStateMode="Enabled"
                                OnClick="WarmLeadPrevLinkButton_Click"></asp:LinkButton>
                            <asp:LinkButton ID="warmLeadNextLinkButton" runat="server" class="next" ViewStateMode="Enabled"
                                OnClick="WarmLeadNextLinkButton_Click"></asp:LinkButton>
                            <div class="center">
                                <p>
                                    PAGE</p>
                                <p>
                                    <span>
                                        <asp:Label ID="warmLeadStartPage" runat="server" ViewStateMode="Enabled">1</asp:Label>
                                    </span>of <span>
                                        <asp:Label ID="warmLeadEndPage" runat="server" ViewStateMode="Enabled"></asp:Label>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="accordion">
                <h3>
                    <asp:Label ID="constructionReportLabel" runat="server" CBID="317"></asp:Label></h3>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel3">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="eligible" colspan="6">
                                        <asp:Label ID="constructionReportHeaderLabel" runat="server" CBID="1111" Text="Downl2oad a list of all new builds along with property
        contact information"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="eligible" colspan="6">
                                        <asp:LinkButton ID="downloadSpreadSheetLinkButton" CBID="374" runat="server" OnClick="DownloadSpreadSheet_ClickLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="downloadSpreadSheetLinkButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="MY ACCOUNT" runat="server"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="constructionReportHyperLink" CBID="11" Text="Construction Report"
                            runat="server" class="active"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="spprovedCategoriesHyperLink" runat="server" CBID="232" Text="Approved Categories"
                            NavigateUrl="~/Account/ApprovedCategories.aspx"></asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="myProductsHyperLink" runat="server" CBID="446" Text="My Products"
                            NavigateUrl="~/Account/MyProducts.aspx"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myAccountHyperLink" runat="server" CBID="58" Text="Account Status"
                            NavigateUrl="~/Account/AccountStatus.aspx"></asp:HyperLink></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>