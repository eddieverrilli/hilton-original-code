﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="AccountStatus.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.AccountStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript" src="../Scripts/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../Scripts/custominputs.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.Imageupload.js"></script>
    <script src="../Scripts/jquery.characterlimiter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            SetFileInputStyles();
            $('textarea#companyDescriptionTextBox').limiter(2000, $('#remainingCharacters'));
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:LinkButton ID="aboutSupplierConnectionLinkButton" CBID="96" Text="My Account"
            runat="server">
        </asp:LinkButton>
        <span>/</span>
        <asp:Label ID="myProductsLabel" CBID="58" Text="Account Status" runat="server"></asp:Label>
        <span></span>
    </div>
    <div class="main-content">
        <h2>
            <asp:Label ID="myAccntHeaderLabel" CBID="58" Text="Account Status" runat="server"></asp:Label></h2>
        <p>
            <asp:Label ID="myAccntIntroLabel" CBID="59" Text="" runat="server"></asp:Label></p>
    </div>
    <div class="main">
        <div class="become-partner account-status">
            <div class="box1">
                <div class="box1-inner">
                    <div class="lft">
                        <h3>
                            <asp:Label ID="myAccntCompanyNameLabel" CBID="135" runat="server" Text="Company Name">
                            </asp:Label>
                        </h3>
                        <p>
                            <asp:Label ID="companyNameLabel" ViewStateMode="Enabled" runat="server"></asp:Label></p>
                    </div>
                    <asp:HiddenField ID="partnerIdHiddenField" runat="server" />
                    <div class="rgt">
                        <ul>
                            <li><span id="partnershipLevel" viewstatemode="Enabled" runat="server"></span>
                                <div>
                                    <asp:Label ID="hiltonWorldWideLabel" CBID="1013" Text="Hilton Worldwide" runat="server"></asp:Label>
                                    <p>
                                        <asp:Label ID="recommendedPartnerLabel" ViewStateMode="Enabled" runat="server" Text="">
                                        </asp:Label>
                                    </p>
                                </div>
                            </li>
                            <li><span class="calender"></span>
                                <div>
                                    <asp:Label ID="accountTermLabel" runat="server" CBID="163" Text="Account Term"></asp:Label>
                                    <p>
                                        <asp:Literal ID="activationDateLiteral" ViewStateMode="Enabled" runat="server"><</asp:Literal>
                                        —
                                        <asp:Literal ID="expirationDateLiteral" ViewStateMode="Enabled" runat="server"></asp:Literal></p>
                                </div>
                            </li>
                        </ul>
                        <asp:UpdatePanel ID="AccountStatusUpdatePanel" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="go" style="margin-top: -80px;">
                                    <asp:Button ID="upgradePartnershipLinkButton" ToolTip="When upgrading to Gold Partner level, your account term will renew once your payment has been processed. Refunds will not be issued for the remaining balance of your previous account term. "
                                        runat="server" ViewStateMode="Enabled" CBID="0" Text="Upgrade to Gold Level Partner"
                                        OnClick="UpgradePartnershipLinkButton_Click" />
                                </div>
                                <div class="go">
                                    <asp:Button ID="renewButton" runat="server" ViewStateMode="Enabled" CBID="617" Text="RENEW"
                                        OnClick="RenewLinkButton_Click" />
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="renewButton" />
                                <asp:PostBackTrigger ControlID="upgradePartnershipLinkButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <h3 class="none">
                    <asp:Label ID="companyInformationLabel" runat="server" CBID="138" Text="Company Information">
                    </asp:Label>
                </h3>
                <div class="contact-form">
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="compNameLabel" runat="server" CBID="135" Text="Company Name"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:TextBox ID="companyNameTextBox" runat="server" ViewStateMode="Enabled" MaxLength="256"
                                ReadOnly="true">
                            </asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="companyLogoLabel" runat="server" Text="" CBID="991"></asp:Label></div>
                        <asp:UpdatePanel ID="imageUploadUpdatePanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="input image form-1">
                                    <p class="imageload">
                                        <img id="loading" src="../Images/async.gif" class="loadingimg" />
                                        <asp:Image ID="companyLogo" runat="server" ClientIDMode="Static" CssClass="pic imagestyle" /></p>
                                    <div class="fileinputs">
                                        <input type="file" size="5px" id="imageUpload" class="customInput customStyle noborder"
                                            name="companyLogo1" onchange="return ajaxFileUpload('imageUpload','image');" />
                                    </div>
                                    <div class="imgsrc">
                                        <asp:LinkButton ID="imageLinkButton" ClientIDMode="Static" CssClass="errorText" runat="server"
                                            ViewStateMode="Enabled" CausesValidation="false" OnClick="ImageLinkButton_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="removeImageLinkButton" OnClick="RemoveImageLinkButton_Click"
                                            ClientIDMode="Static" runat="server" Style="display: none; margin-left: 15px"
                                            CausesValidation="false" CssClass="errorText" Text="X" CBID="753"></asp:LinkButton>
                                    </div>
                                    <span id="imageErrSpan" class="errorText" style="display: none"></span>
                                    <input type="hidden" id="imageNameHiddenField" clientidmode="Static" runat="server"
                                        value="" />
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="imageLinkButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="clear">
                        </div>
                        <div class="form-spec">
                            <asp:Label ID="companyLogoSizeDescription" runat="server" Text="Images should be JPG or PNG, resized to 235x235 pixels"
                                CBID="1129"></asp:Label></div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="addressLabel" runat="server" CBID="304" Text="Address"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox runat="server" CssClass="mandatory" ID="addressTextBox" MaxLength="256">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="companyAddressRequiredField" ViewStateMode="Enabled"
                                SetFocusOnError="true" VMTI="13" runat="server" ErrorMessage="Please provide company address"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="addressTextBox" CssClass="errorText"
                                Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <br />
                            <asp:TextBox runat="server" ID="addressLine2textBox" MaxLength="256">
                            </asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="countryLabel" runat="server" CBID="345" Text="Country"></asp:Label>
                        </div>
                        <asp:UpdatePanel ID="dropDownUpdatePanel" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <div class="input select1">
                                    <asp:DropDownList runat="server" ID="countryDropDown" ViewStateMode="Enabled" AutoPostBack="true"
                                        OnSelectedIndexChanged="CompanyInfoCountryDropDown_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="countryRequiredField" VMTI="14"
                                        SetFocusOnError="true" runat="server" ErrorMessage="Please select a country"
                                        ValidationGroup="UpdateAccountStatus" ControlToValidate="countryDropDown" InitialValue="0"
                                        CssClass="errorText" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="cityLabel" runat="server" CBID="296" Text="City"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox runat="server" ID="cityTextBox" MaxLength="128" CssClass="mandatory">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="CityRequiredField" SetFocusOnError="true"
                                VMTI="15" runat="server" ErrorMessage="Please provide city" ValidationGroup="UpdateAccountStatus"
                                ControlToValidate="cityTextBox" CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="stateLabel" runat="server" CBID="689" Text="State"></asp:Label>
                        </div>
                        <asp:UpdatePanel ID="stateDropDownUpdatePanel" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <div class="input select2">
                                    <asp:DropDownList runat="server" ViewStateMode="Enabled" ID="stateDropDown" 
                                         Width="160px">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="stateRequiredFieldValidator"
                                        SetFocusOnError="true" runat="server" ErrorMessage="Please provide state" ValidationGroup="UpdateAccountStatus"
                                        InitialValue="0" ControlToValidate="stateDropDown" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="countryDropDown" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="zipCodeLabel" runat="server" CBID="762" Text="Zip Code"></asp:Label>
                        </div>
                        <asp:UpdatePanel ID="zipCodeUpdatePanel" ViewStateMode="Enabled" UpdateMode="Conditional"
                            runat="server">
                            <ContentTemplate>
                                <div id="zipTextBoxDiv" runat="server" class="input textbox">
                                    <asp:TextBox ID="zipTextBox" MaxLength="10" runat="server">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="zipRequiredFieldValidator"
                                        runat="server" SetFocusOnError="true" ErrorMessage="Please provide postal code"
                                        ValidationGroup="UpdateAccountStatus" ControlToValidate="zipTextBox" CssClass="errorText"
                                        Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="zipCodeRegularExpressionValidator" ViewStateMode="Enabled"
                                        ErrorMessage="Please correct the format" CssClass="errorText" ControlToValidate="zipTextBox"
                                        ValidationGroup="UpdateAccountStatus" SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="countryDropDown" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="companyDescription" runat="server" CBID="0" Text="Company Description"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox runat="server" TextMode="MultiLine" ClientIDMode="Static" ID="companyDescriptionTextBox">
                            </asp:TextBox>
                        </div>
                        <div class="errorText" style="margin-top: -2%; margin-left: 20%">
                            <asp:Label runat="server" ID="remainingCharacters" ClientIDMode="Static"></asp:Label>
                            <asp:Label runat="server" ID="remainingCharacterLabel" Text="characters remaining"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="websiteLabel" runat="server" CBID="744" Text="Website"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox ID="websiteTextBox" MaxLength="1000" runat="server">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ViewStateMode="Enabled" ID="websiteRegularExpressionValidator"
                                SetFocusOnError="true" VMTI="20" runat="server" ErrorMessage="Correct the format"
                                ValidationGroup="UpdateAccountStatus" CssClass="errorText" Display="Dynamic"
                                ControlToValidate="websiteTextBox">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <h3 class="none">
                    <asp:Label ID="contactInformationLabel" runat="server" CBID="328" Text="Contact Information">
                    </asp:Label>
                </h3>
                <div class="contact-form">
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="firstNameLabel" runat="server" CBID="104" Text="First Name"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox ID="firstNameTextBox" runat="server" MaxLength="128" CssClass="mandatory">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="firstNameRequiredField" SetFocusOnError="true"
                                VMTI="1" runat="server" ErrorMessage="Please provide first name" ValidationGroup="UpdateAccountStatus"
                                ControlToValidate="firstNameTextBox" CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ViewStateMode="Enabled" ID="firstNameRegularExpressionValidator"
                                SetFocusOnError="true" VMTI="2" runat="server" ControlToValidate="firstNameTextBox"
                                ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="lastNameLabel" runat="server" CBID="116" Text="Last Name"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox ID="lastNameTextBox" runat="server" MaxLength="128" CssClass="mandatory">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="lastNameequiredField" SetFocusOnError="true"
                                VMTI="3" runat="server" ErrorMessage="Please provide last name" ValidationGroup="UpdateAccountStatus"
                                ControlToValidate="lastNameTextBox" CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="lastNameRegularExpressionValidator" ViewStateMode="Enabled"
                                SetFocusOnError="true" runat="server" ControlToValidate="lastNameTextBox" ErrorMessage="Please enter valid name"
                                VMTI="4" CssClass="errorText" ValidationGroup="UpdateAccountStatus" Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="titlePrimaryContactLabel" runat="server" CBID="0" Text="Title"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox ID="titlePrimaryContactTextBox" runat="server" MaxLength="256">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ID="titlePrimaryRegularExpressionValidator" ViewStateMode="Enabled"
                                SetFocusOnError="true" runat="server" ControlToValidate="titlePrimaryContactTextBox"
                                ErrorMessage="Please enter valid title" VMTI="5" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="phoneLabel" runat="server" CBID="130" Text="Phone"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox runat="server" ID="phoneTextBox" MaxLength="32" CssClass="mandatory">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="phoneNumberRequiredField"
                                SetFocusOnError="true" VMTI="6" runat="server" ErrorMessage="Please provide phone number"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="phoneTextBox" CssClass="errorText"
                                Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ViewStateMode="Enabled" ID="phoneRegularExpressionValidator"
                                VMTI="7" CssClass="errorText" ControlToValidate="phoneTextBox" ValidationGroup="UpdateAccountStatus"
                                SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="faxLabel" runat="server" CBID="146" Text="Fax"></asp:Label>
                        </div>
                        <div class="input textbox2">
                            <asp:TextBox ID="faxTextBox" runat="server" MaxLength="32" CssClass="mandatory">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="faxRequiredField" VMTI="8"
                                SetFocusOnError="true" runat="server" ErrorMessage="Please provide fax number"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="faxTextBox" CssClass="errorText"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="faxRegularExpressionValidator" SetFocusOnError="true"
                                ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="faxTextBox"
                                ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="emailLabel" runat="server" CBID="379" Text="Email Address"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox ID="emailAddressTextBox" runat="server" CssClass="mandatory" MaxLength="128">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ViewStateMode="Enabled" ID="emailAddressRequiredField"
                                SetFocusOnError="true" VMTI="10" runat="server" ErrorMessage="Please provide Email address"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="emailAddressTextBox"
                                CssClass="errorText" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" runat="server"
                                SetFocusOnError="true" VMTI="11" ErrorMessage="Correct the format" CssClass="errorText"
                                Display="Dynamic" ControlToValidate="emailAddressTextBox" ViewStateMode="Enabled"
                                ValidationGroup="UpdateAccountStatus">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <h3 class="none">
                    <asp:Label ID="secondayContactLabel" runat="server" CBID="0" Text="Secondary Contact"></asp:Label></h3>
                <div class="contact-form">
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="secondaryContactFirstNameLabel" runat="server" CBID="0" Text="First Name"></asp:Label></div>
                        <div class="input textbox">
                            <asp:TextBox ID="secondaryContactFirstNameTextbox" CssClass="mandatory" runat="server"
                                MaxLength="128"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="secondaryContactFirstNameRequiredFieldValidator"
                                SetFocusOnError="true" VMTI="1" runat="server" ViewStateMode="Enabled" ErrorMessage="Please provide first name"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="secondaryContactFirstNameTextbox"
                                CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="secondaryContactFirstNamerRegularExpressionValidator"
                                VMTI="2" SetFocusOnError="true" runat="server" ViewStateMode="Enabled" ControlToValidate="secondaryContactFirstNameTextbox"
                                ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="secondaryContactLastNameLabel" runat="server" CBID="0" Text="Last Name"></asp:Label></div>
                        <div class="input textbox">
                            <asp:TextBox ID="secondaryContactLastNameTextbox" CssClass="mandatory" runat="server"
                                MaxLength="128"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="secondaryContactLastNameRequiredFieldValidator" VMTI="3"
                                ViewStateMode="Enabled" SetFocusOnError="true" runat="server" ErrorMessage="Please provide last name"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="secondaryContactLastNameTextbox"
                                CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="secondaryContactLastNameRegularExpressionValidator"
                                SetFocusOnError="true" VMTI="4" ViewStateMode="Enabled" runat="server" ControlToValidate="secondaryContactLastNameTextbox"
                                ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="secondaryContactTitleLabel" runat="server" CBID="0" Text="Title"></asp:Label>
                        </div>
                        <div class="input textbox">
                            <asp:TextBox ID="secondaryContactTitleTextBox" runat="server" MaxLength="256">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ID="SecondaryContactTitleRegularExpressionValidator"
                                ViewStateMode="Enabled" SetFocusOnError="true" runat="server" ControlToValidate="secondaryContactTitleTextBox"
                                ErrorMessage="Please enter valid title" VMTI="5" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="secondaryContactPhoneLabel" runat="server" CBID="0" Text="Phone Number"></asp:Label></div>
                        <div class="input textbox">
                            <asp:TextBox ID="secondaryContactPhoneTextbox" CssClass="mandatory" runat="server"
                                MaxLength="32"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="secondaryContactPhoneRequiredFieldValidator" runat="server"
                                VMTI="6" SetFocusOnError="true" ViewStateMode="Enabled" ErrorMessage="Please provide phone number"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="secondaryContactPhoneTextbox"
                                CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="secondaryContactPhoneRegularExpressionValidator"
                                VMTI="7" CssClass="errorText" ViewStateMode="Enabled" ControlToValidate="secondaryContactPhoneTextbox"
                                ValidationGroup="UpdateAccountStatus" SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="secondaryContactFaxLabel" runat="server" CBID="0" Text="Fax Number"></asp:Label></div>
                        <div class="input textbox2">
                            <asp:TextBox ID="secondaryContactFaxTextbox" CssClass="mandatory" runat="server"
                                MaxLength="32"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="secondaryContactFaxRequiredFieldValidator" VMTI="8"
                                SetFocusOnError="true" runat="server" ViewStateMode="Enabled" ErrorMessage="Please provide fax number"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="secondaryContactFaxTextbox"
                                CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="secondaryContactFaxRegularExpressionValidator"
                                SetFocusOnError="true" ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="secondaryContactFaxTextbox"
                                ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="secondaryContactEmailLabel" runat="server" CBID="0" Text="Email Address"></asp:Label></div>
                        <div class="input textbox">
                            <asp:TextBox ID="secondaryContactEmailTextbox" CssClass="mandatory" runat="server"
                                MaxLength="128"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="secondaryContactEmailRequiredFieldValidator" VMTI="10"
                                SetFocusOnError="true" runat="server" ViewStateMode="Enabled" ErrorMessage="Please provide email"
                                ValidationGroup="UpdateAccountStatus" ControlToValidate="secondaryContactEmailTextbox"
                                CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="secondaryContactEmailRegularExpressionValidator"
                                SetFocusOnError="true" ViewStateMode="Enabled" VMTI="11" runat="server" ErrorMessage="Correct the format"
                                ControlToValidate="secondaryContactEmailTextbox" CssClass="errorText" ValidationGroup="UpdateAccountStatus"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="regionDropDownUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="accordion">
                        <h3>
                            <asp:Label ID="regionalContactLabel" runat="server" CBID="0" Text="Country Contact"></asp:Label></h3>
                        <div class="contact-form">
                            <div class="row">
                                <div class="label">
                                    Country</div>
                                <div class="input select1">
                                    <asp:DropDownList ID="regionCountryDropDown" ViewStateMode="Enabled" runat="server" OnSelectedIndexChanged="RegionCountryDropDown_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="regionalFirstNameLabel" runat="server" CBID="0" Text="First Name"></asp:Label></div>
                                <div runat="server" id="firstNameDiv" class="input textbox">
                                    <asp:TextBox ID="regionalFirstNameTextBox" CssClass="mandatory" runat="server" MaxLength="128"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regionalFirstNameRegularExpressionValidator"
                                        ViewStateMode="Enabled" runat="server" ControlToValidate="regionalFirstNameTextBox"
                                        VMTI="2" SetFocusOnError="true" ErrorMessage="Please enter valid name" CssClass="errorText"
                                        ValidationGroup="UpdateAccountStatus" Display="Dynamic"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="regionalLastNameLabel" runat="server" CBID="0" Text="Last Name"></asp:Label></div>
                                <div runat="server" id="lastNameDiv" class="input textbox">
                                    <asp:TextBox ID="regionalLastNameTextBox" CssClass="mandatory" runat="server" MaxLength="128"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regionalLastNameRegularExpressionValidator" VMTI="4"
                                        SetFocusOnError="true" runat="server" ViewStateMode="Enabled" ControlToValidate="regionalLastNameTextBox"
                                        ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="regionalTitleLabel" runat="server" CBID="0" Text="Title"></asp:Label></div>
                                <div runat="server" id="titleDiv" class="input textbox">
                                    <asp:TextBox ID="regionalTitleTextBox" runat="server" MaxLength="128"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regionalTitleRegularExpressionValidator" VMTI="5"
                                        SetFocusOnError="true" runat="server" ViewStateMode="Enabled" ControlToValidate="regionalTitleTextBox"
                                        ErrorMessage="Please enter valid title" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="regionalPhoneNumberLabel" runat="server" CBID="0" Text="Phone Number"></asp:Label></div>
                                <div runat="server" id="phoneDiv" class="input textbox">
                                    <asp:TextBox ID="regionalPhoneTextBox" CssClass="mandatory" MaxLength="32" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="phoneRegionalRegularExpressionValidator" ViewStateMode="Enabled"
                                        VMTI="7" CssClass="errorText" ControlToValidate="regionalPhoneTextBox" ValidationGroup="UpdateAccountStatus"
                                        SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="regionalFaxNumberLabel" runat="server" CBID="0" Text="Fax Number"></asp:Label></div>
                                <div runat="server" id="faxDiv" class="input textbox2">
                                    <asp:TextBox ID="regionalFaxTextBox" CssClass="mandatory" MaxLength="32" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="faxRegionalRegularExpressionValidator" ViewStateMode="Enabled"
                                        SetFocusOnError="true" VMTI="9" runat="server" ControlToValidate="regionalFaxTextBox"
                                        ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="UpdateAccountStatus"
                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="regionalEmailAddressLabel" runat="server" CBID="0" Text="Email Address"></asp:Label></div>
                                <div runat="server" id="emailDiv" class="input textbox">
                                    <asp:TextBox ID="regionalEmailTextBox" runat="server" CssClass="mandatory" MaxLength="128"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regionalEmailRegularExpressionValidator" ViewStateMode="Enabled"
                                        SetFocusOnError="true" VMTI="11" runat="server" ErrorMessage="Correct the format"
                                        CssClass="errorText" ControlToValidate="regionalEmailTextBox" ValidationGroup="UpdateAccountStatus"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="box1 cnt">
                <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                        <asp:Label ID="regionalContactErrorMessageLabel" CssClass="errorText" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="buttonUpdatePanel" ViewStateMode="Enabled" UpdateMode="Conditional"
                    runat="server">
                    <ContentTemplate>
                        <div class="go">
                            <asp:Button ID="cancelButton" runat="server" CBID="270" Text="CANCEL" OnClick="CancelButton_Click" />
                            <asp:Button ID="saveButton" runat="server" CBID="636" Text="SAVE" OnClick="SaveButton_Click"
                                ValidationGroup="UpdateAccountStatus" />
                        </div>
                        <asp:CustomValidator ID="savePartnerCustomValidator" runat="server" ValidationGroup="UpdateAccountStatus"
                            CssClass="errorText" Display="Dynamic" OnServerValidate="SavePartnerValidator_ServerValidate"></asp:CustomValidator>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="clear">
                    &nbsp;</div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="MY ACCOUNT" CBID="96" runat="server"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li id="constructionReportListItem" runat="server">
                        <asp:HyperLink ID="constReportHyperLink" CBID="11" Text="Construction Report" NavigateUrl="~/Account/LeadsAndConstructionReport.aspx"
                            runat="server">
                        </asp:HyperLink>
                    </li>
                    <li id="approvedCategoriesListItem" runat="server">
                        <asp:HyperLink ID="approvedCategoriesHyperLink" runat="server" CBID="232" Text="Approved Categories"
                            NavigateUrl="~/Account/ApprovedCategories.aspx">
                        </asp:HyperLink>
                    </li>
                    <li id="myProductsListItem" runat="server">
                        <asp:HyperLink ID="myProductsHyperLink" runat="server" CBID="446" Text="My Products"
                            NavigateUrl="~/Account/MyProducts.aspx">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myAccountHyperLink" runat="server" class="active" CBID="58" Text="Account Status"
                            NavigateUrl="~/Account/AccountStatus.aspx">
                        </asp:HyperLink></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>
