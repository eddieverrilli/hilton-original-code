﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    EnableEventValidation="false" CodeBehind="AddEditRegionCountries.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.AddEditRegionCountries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script type="text/javascript" src="../Scripts/SetCountriesDropDown.js"></script>
    <script type="text/javascript" src="../Scripts/mutiselect.category.dropdowns.js"></script>
    <script type="text/javascript" src="../Scripts/multiselect.admin.region.and.country.js"></script>
    <style type="text/css">
        .drpstatedisabled
        {
            opacity: .35;
            filter: Alpha(Opacity=35);
            background-image: none;
            background-color: inherit;
        }
    </style>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            SetMultiSelectDropDowns();
        });


        function ShowAllDeleteMessage() {

            alert('Each brand and category must have at least one Region and Country.');
            window.location.href = location.href;
        }




        function SetMultiSelectDropDowns() {
            getItems();
            getCountryItems();
            isAllUnChecked = 1;
            isAllRegionUnChecked = 1;
            isAllCountryUnChecked = 1;
            SetcategoryDropDownEditRegionCountry();
            SetRegionAndCountryDropDown($('#categoryRegionDropDown'), $('#categoryCountryDropDown'), $('#selectedRegions'), $('#selectedCountries'));
            $("#optgroup").multiselect({
                position: {
                    my: 'left bottom',
                    at: 'left top'
                }
            });

            $("#OptCountryGroup").multiselect({
                position: {
                    my: 'left bottom',
                    at: 'left top'
                }
            });
        }

        function InitializeScript() {

            $('.accordion:first h3, .accordion:first .panel1:first h4, .accordion:first .panel1:first .panel2:first h5').addClass('active');
            $('.accordion:first h3').siblings().show();
            $('.accordion:first .panel1:first .panel2').show();
            $('.accordion:first .panel1:first .panel3:first').show();
            $('.accordion h3').click(function () {
                $(this).toggleClass('active');
                $(this).siblings('.panel').slideToggle();
            });
            $('.accordion h4').siblings('.panel1').toggle();
            $('.accordion h4').click(function () {

                $(this).toggleClass('active');
                $(this).siblings('.panel1').slideToggle();
            });
            $('.accordion h5').click(function () {
                $(this).toggleClass('active');
                $(this).next().slideToggle();
            });

        }

        function more_lessText() {
            $(function () {
                $('.excerpt').wrapInner("<span></span>");
                $('.excerpt').each(function () {
                    $(this).html(formatWords($(this).html(), 50));

                    $(this).children().children('.more_text').hide();

                }).click(function () {
                    var more_text = $(this).children().children('.more_text');
                    var more_link = $(this).children('a.more_link');

                    if (more_text.hasClass('hide')) {
                        more_text.show();
                        more_link.html('Hide');
                        more_text.removeClass('hide');
                    } else {
                        more_text.hide();
                        more_link.html('More');
                        more_text.addClass('hide');
                    }
                    return false;
                });
            });
            function formatWords(sentence, show) {
                var words = sentence.split(' ');
                var new_sentence = '';
                for (i = 0; i < words.length; i++) {
                    if (i <= show) {
                        new_sentence += words[i] + ' ';
                    } else {
                        if (i == (show + 1)) new_sentence += '<span class="more_text hide">';
                        new_sentence += words[i] + ' ';
                        if (words[i + 1] == null) new_sentence += '</span></br><a href="#" class="more_link">More...</a>';
                    }
                }
                return new_sentence;
            }
        }

        function more_lessItems() {
            $('td.col4').each(function () {
                var max = 1
                if ($(this).find("li").length > max) {
                    var count = $(this).find("li").length;
                    var num = count - 2;
                    if (num != 0) {
                        $(this).find('li:gt(1)').hide();
                        var list = $(this).find('li:gt(1)');
                        $(this).append('<span class="more">+' + num + ' More</span>');
                        $(this).append('<span href="" class="less">Less</span>');
                        $('.more').click(function () {
                            $(this).next().show();
                            $(this).parent().find('li:gt(1)').show()
                            $(this).hide();
                        });
                        $('.less').click(function () {
                            $(this).prev().show();
                            $(this).parent().find('li:gt(1)').hide()
                            $(this).hide();
                        });
                    }
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="myProdsHyperLink" Text="Approved Categories" runat="server" NavigateUrl="~/Account/ApprovedCategories.aspx">
        </asp:HyperLink><span>/</span>
        <asp:Label ID="lblBrCumRegion" runat="server" Text="Edit Regions/Countries"></asp:Label></div>
    <div class="main-content">
        <h2>
            <asp:Label ID="titleLabel" runat="server" Text="Edit Regions/Countries">
            </asp:Label>
        </h2>
    </div>
    <div class="main">
        <div class="approved-category">
            <div class="accordion">
                <h3 class="none">
                    <asp:Label ID="categoryHeaderLabel" runat="server" Text="Edit Regions/Countries for Approved Categories">
                    </asp:Label>
                </h3>
                <div>
                    <asp:HyperLink ID="HyperLink1" Text="<< Return to Approved Categories" Style="margin-left: 5px;"
                        Font-Size="Larger" Font-Bold="true" runat="server" NavigateUrl="~/Account/ApprovedCategories.aspx">
                    </asp:HyperLink>
                </div>
                <br />
                <asp:UpdatePanel ID="chooseCategoryUpdatePanel" runat="server" ViewStateMode="Enabled"
                    UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="selectedRegions" EventName="TextChanged" />
                        <asp:AsyncPostBackTrigger ControlID="addCategoryLinkButton" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="deletCategoryLinkButton" EventName="Click" />
                    </Triggers>
                    <ContentTemplate>
                        <asp:HiddenField ID="RegionCountryRequestId" runat="server" />
                        <asp:HiddenField ID="hdnSelectDropdownMode" runat="server" />
                        <div class="search-result " id="AddDeleteDiv" runat="server" style="padding-top: 0px;">
                            <p style="margin-left: 5px;">
                                You may only
                                <asp:Label ID="lblmaintext" Text="edit" runat="server"></asp:Label>
                                regions/countries for Categories & Brands for which you have been previously approved.</p>
                            <div class="flL" style="padding: 5px;">
                                <asp:DropDownList ID="singleCategoryDropDown" ClientIDMode="Static" runat="server"
                                    Style="width: 160px" ViewStateMode="Enabled" AutoPostBack="true" OnSelectedIndexChanged="SingleCategoryDropDown_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:DropDownList ID="categoryDropDown" ClientIDMode="Static" runat="server" ViewStateMode="Enabled"
                                    multiple="multiple">
                                </asp:DropDownList>
                                <asp:TextBox runat="server" ID="selectedParentCategories" ClientIDMode="Static" Style="display: none;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="categoriesDropDownRequiredFieldValidator" InitialValue="0"
                                    VMTI="0" runat="server" ErrorMessage="Please Select a category." ControlToValidate="selectedParentCategories"
                                    Display="Dynamic" ValidationGroup="dropdown" CssClass="errorText"></asp:RequiredFieldValidator>
                            </div>
                            <div class="flL" style="padding: 5px; margin-left: 5px;">
                                <asp:DropDownList ID="singleBrandDropDown" ClientIDMode="Static" AutoPostBack="true"
                                    Style="width: 160px" runat="server" OnSelectedIndexChanged="SingleBrandDropDown_SelectedIndexChanged"
                                    ViewStateMode="Enabled">
                                </asp:DropDownList>
                                <asp:DropDownList ID="optgroup" runat="server" class="list-5" multiple="multiple"
                                    ClientIDMode="Static">
                                </asp:DropDownList>
                                <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="isCheckAll" runat="server" Value="0" ClientIDMode="Static" />
                                <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display: none;"
                                    AutoPostBack="false"></asp:TextBox>
                                <div class="warning">
                                    <asp:RequiredFieldValidator ID="brandValidator" Enabled="false" VMTI="31" SetFocusOnError="true"
                                        runat="server" ErrorMessage="Please select a brand" ValidationGroup="AddCategory"
                                        ControlToValidate="optgroup" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="flL area-dropdown list-c" style="padding: 5px;">
                                <asp:DropDownList ID="categoryRegionDropDown" runat="server" class="list-6" multiple="multiple"
                                    ClientIDMode="Static">
                                </asp:DropDownList>
                                <asp:HiddenField ID="isAllRegionChecked" runat="server" ClientIDMode="Static" Value="1" />
                                <asp:TextBox runat="server" ID="selectedRegions" ClientIDMode="Static" Style="display: none;"
                                    AutoPostBack="true" OnTextChanged="SelectedRegions_TextChanged"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="regionRequiredFieldValidator" Enabled="false" runat="server"
                                    VMTI="30" SetFocusOnError="true" ErrorMessage="Please select a region" ValidationGroup="AddCategory"
                                    ControlToValidate="categoryRegionDropDown" InitialValue="0" CssClass="errorText"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="flL" style="padding: 5px;">
                                <asp:DropDownList ID="OptCountryGroup" runat="server" class="list-5" multiple="multiple"
                                    ClientIDMode="Static">
                                </asp:DropDownList>
                                <asp:HiddenField ID="countryCollection" runat="server" ClientIDMode="Static" />
                                <asp:TextBox runat="server" ID="selectedCountries" ClientIDMode="Static" Style="display: none;"
                                    AutoPostBack="false"></asp:TextBox>
                            </div>
                            <input type="hidden" runat="server" id="AddDeleteHidden" />
                            <div style="float: right; padding: 0px 36px 0px 0px">
                                <div class="go">
                                    <asp:LinkButton ID="addCategoryLinkButton" runat="server" ClientIDMode="Static" class="go"
                                        Text="Continue" ValidationGroup="AddCategory" CBID="0" OnClick="addCategoryLinkButton_Click"></asp:LinkButton></div>
                                <div class="go">
                                    <asp:LinkButton ID="deletCategoryLinkButton" runat="server" ClientIDMode="Static"
                                        class="go" Text="Continue" ValidationGroup="AddCategory" CBID="0" OnClick="deletCategoryLinkButton_Click"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="panel">
                    <asp:UpdatePanel ID="categoriesResultUpdatePanel" ViewStateMode="Enabled" runat="server"
                        UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="addCategoryLinkButton" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="deletCategoryLinkButton" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <p style="padding-left: 10px;">
                                <b><span id="addDeleteMessage" runat="server"></span></b>
                            </p>
                            <div style="margin-left: 10px; margin-right: 10px;">
                                <asp:GridView ID="approvedCategoriesGridView" runat="server" AlternatingRowStyle-CssClass="odd"
                                    GridLines="None" AutoGenerateColumns="false">
                                    <HeaderStyle CssClass="grid-header" />
                                    <Columns>
                                        <asp:BoundField HeaderText="CATEGORY" DataField="CategoryDisplayName" ItemStyle-CssClass="col4"
                                            HeaderStyle-CssClass="col4" />
                                        <asp:BoundField HeaderText="BRAND" DataField="BrandDescription" ItemStyle-CssClass="col4"
                                            HeaderStyle-CssClass="col4" />
                                        <asp:TemplateField HeaderText="REGION">
                                            <ItemTemplate>
                                                <itemstyle cssclass="col4"></itemstyle>
                                                <ul>
                                                    <asp:Repeater ID="repeaterregions" runat="server" DataSource='<%# Eval("Regions") %>'>
                                                        <ItemTemplate>
                                                            <li id="listStandard" runat="server">
                                                                <%# Eval("RegionDescription")%><br />
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="col4"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="COUNTRIES">
                                            <ItemTemplate>
                                                <itemstyle cssclass="col4"></itemstyle>
                                                <ul>
                                                    <asp:Repeater ID="repeaterCountries" runat="server" DataSource='<%# Eval("Countries") %>'>
                                                        <ItemTemplate>
                                                            <li id="listStandard" runat="server">
                                                                <%# Eval("CountryName")%><br />
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="col4"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="search-result">
                                <div id="resultMessageDiv" runat="server">
                                </div>
                                <div id="divbuttons" runat="server" style="background-color: White; padding-top: 10px;
                                    padding-bottom: 10px;">
                                    <span style="padding: 10px; vertical-align: middle;">Are you sure you want to save the
                                        updates? Select Save to continue or Cancel to cancel changes.</span>
                                    <div style="float: right; padding: 0px 5px 0px 0px;">
                                        <div class="go" style="margin-right: 10px">
                                            <asp:LinkButton ID="SaveButton" CBID="644" Text="Save" runat="server" OnClick="SaveButton_Click"></asp:LinkButton></div>
                                        <div class="go" style="padding-right: 10px">
                                            <asp:LinkButton ID="CancelButton" ClientIDMode="Static" Text="Cancel" runat="server"
                                                OnClick="CancelButton_Click"></asp:LinkButton></div>
                                    </div>
                                    <div class="clear">
                                        &nbsp;</div>
                                </div>
                            </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="sidebar">
            <div class="sidebar-box" id="rightsidebar-wrapper">
                <h3>
                    <asp:Label ID="rightPanelHeaderLabel" Text="MY ACCOUNT" runat="server"></asp:Label>
                </h3>
                <div class="middle">
                    <ul>
                        <li id="constructionReportListItem" runat="server">
                            <asp:HyperLink ID="constructionReportHyperLink" CBID="11" Text="Construction Report"
                                NavigateUrl="~/Account/LeadsAndConstructionReport.aspx" runat="server"></asp:HyperLink>
                        </li>
                        <li>
                            <asp:HyperLink ID="approvedCategoriesHyperLink" CBID="232" Text="Approved Categories"
                                runat="server" class="active"></asp:HyperLink>
                        </li>
                        <li>
                            <asp:HyperLink ID="myProductsHyperLink" CBID="446" Text="My Products" runat="server"
                                NavigateUrl="~/Account/MyProducts.aspx"></asp:HyperLink>
                        </li>
                        <li>
                            <asp:HyperLink ID="myAccountStatusHyperLink" CBID="58" Text="Account Status" runat="server"
                                NavigateUrl="~/Account/AccountStatus.aspx"></asp:HyperLink></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clear">
            &nbsp;</div>
    </div>
</asp:Content>
