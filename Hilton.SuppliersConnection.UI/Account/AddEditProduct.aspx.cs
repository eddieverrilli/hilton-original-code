﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Account
{
    public partial class AddEditProduct : PageBase
    {
        private IPartnerManager _partnerManager;
        private IProductManager _productManager;
        private ProjectTemplate _projectTemplate;

        /// <summary>
        /// Invoked at OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            ValidateUserAccess((int)MenuEnum.MyProducts);
            bool isConstructionReportAccessible = ValidateMenuAuthorization((int)MenuEnum.ConstructionReport);
            if (!isConstructionReportAccessible)
                constructionReportListItem.Visible = false;
            else
                constructionReportListItem.Visible = true;

            _partnerManager = PartnerManager.Instance;
            _productManager = ProductManager.Instance;

            this.Title = ResourceUtility.GetLocalizedString(93, culture, "Submit Product");

            IList<Category> categories = PartnerManager.Instance.GetCategories;

            SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categories);
            if (parentCategoryContainer == null)
                parentCategoryContainer = new PlaceHolder();

            RecreateControls("categoriesDropDown", "DropDownList");
        }

        /// <summary>
        /// Invoked at Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // int partnerId;
            try
            {
                //   User user = GetSession<User>(SessionConstants.User);
                // partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

                resultMessageDiv.Attributes.Remove("class");
                resultMessageDiv.InnerText = string.Empty;

                if (!Page.IsPostBack)
                {
                    PopulateDropDownData();

                    SetRegularExpressions();

                    SetPartnerContactData();

                    SetPageSettings();
                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(categoryDetailsUpdatePanel, this.GetType(), "moreLess", "more_lessItems()", true);
                    ScriptManager.RegisterStartupScript(categoryDetailsUpdatePanel, categoryDetailsUpdatePanel.GetType(), "InitializeScript", "InitializeScript();", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        private void SetRegularExpressions()
        {
            // Regular Expression for Name
            SetValidatorRegularExpressions(firstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(titleContactRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);
            SetValidatorRegularExpressions(phoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(faxRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
            // Regular Expression for website
            SetValidatorRegularExpressions(websiteRegularExpressionValidator, (int)RegularExpressionTypeEnum.WebsiteValidator);

            // Regular Expression for website
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
        }

        /// <summary>
        /// Invoked at submitLinkButton Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submitLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (GetViewState<string>(UIConstants.Mode) == UIConstants.Edit)
                {
                    UpdateProductContactOnReview(Convert.ToInt32(GetViewState<string>(ViewStateConstants.ProductViewState), CultureInfo.InvariantCulture));
                }
                else
                {
                    string[] checkedCategories = selectedCategoriesHiddenField.Value.ToString(CultureInfo.InvariantCulture).Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    if (checkedCategories.Count() == 1 && checkedCategories[0].Equals("0", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.NoCategorySelected, culture));
                        User user = GetSession<User>(SessionConstants.User);

                        _projectTemplate = _partnerManager.GetCategoriesForSubmitProduct(Convert.ToInt32(GetViewState(ViewStateConstants.SelectedCategory), CultureInfo.InvariantCulture), Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture), 0, Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture), Convert.ToInt32(countryDropDown.SelectedValue, CultureInfo.InvariantCulture), Convert.ToInt32(user.Company, CultureInfo.InvariantCulture));
                        IList<Category> rootCategoryList = (_projectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
                        categoryDivisionsRepeater.DataSource = rootCategoryList;
                        categoryDivisionsRepeater.DataBind();
                        productImage.ImageUrl = Session[SessionConstants.ProductImageBytes] != null ? "data:image/jpg;base64," + Convert.ToBase64String(((byte[])Session[SessionConstants.ProductImageBytes]).ConvertToThumbnail()) : "../Images/NoProduct.PNG";
                        imageLinkButton.Text = imageNameHiddenField.Value;

                        return;
                    }

                    int result = _productManager.AddProductForReview(GenerateProduct(), GenerateProductAssignedSpecs(checkedCategories));

                    if (result > 0)
                    {
                        Response.Redirect("ProductSubmission.aspx", false);
                    }

                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.ProductNotAdded, culture));
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at cancelLinkButton Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("MyProducts.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Updates Product Contact
        /// </summary>
        /// <param name="productId"></param>
        private void UpdateProductContactOnReview(int productId)
        {
            try
            {
                Product product = new Product()
                {
                    ProductId = productId,
                    ProductName = productNameTextBox.Text.Trim(),
                    Contact = new Contact()
                    {
                        FirstName = firstNameTextBox.Text.Trim(),
                        LastName = lastNameTextBox.Text.Trim(),
                        Title = titleContactTextBox.Text.Trim(),
                        Email = emailTextBox.Text.Trim(),
                        Phone = phoneTextBox.Text.Trim(),
                        Fax = faxTextBox.Text.Trim()
                    }
                };

                int result = _productManager.UpdateProductForReview(product);

                if (result >= 0)
                {
                    ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.ProductSuccessfullyUpdated, culture));
                    SetSession<string>(SessionConstants.ProductId, result.ToString());
                    SetPageSettings();
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Sets the log-in user Partner Contact Data
        /// </summary>
        private void SetPartnerContactData()
        {
            try
            {
                User user = GetSession<User>(SessionConstants.User);
                if (user != null)
                {
                    Contact partnerContact = _partnerManager.GetPartnerContactDetails(Convert.ToInt32(user.Company, CultureInfo.InvariantCulture));
                    contactIdHiddenField.Value = partnerContact.ContactId;
                    firstNameTextBox.Text = partnerContact.FirstName;
                    lastNameTextBox.Text = partnerContact.LastName;
                    titleContactTextBox.Text = partnerContact.Title;
                    emailTextBox.Text = partnerContact.Email;
                    phoneTextBox.Text = partnerContact.Phone;
                    faxTextBox.Text = partnerContact.Fax;
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
            }
        }

        /// <summary>
        /// Gethers Products Specifications
        /// </summary>
        /// <returns></returns>
        private Product GenerateProduct()
        {
            Product product = new Product()
            {
                ProductName = productNameTextBox.Text.Trim(),
                ProductSku = productSKUTextBox.Text.Trim(),
                ProductImage = Session[SessionConstants.ProductImageBytes] != null ? (byte[])Session[SessionConstants.ProductImageBytes] : null,
                ProductThumbnailImage = Session[SessionConstants.ProductImageBytes] != null ? ((byte[])Session[SessionConstants.ProductImageBytes]).ConvertToThumbnail() : null,
                SpecSheetPdf = Session[SessionConstants.ProductSpecSheetPdf] != null ? (byte[])Session[SessionConstants.ProductSpecSheetPdf] : null,
                PartnerId = Convert.ToInt32(GetSession<User>(SessionConstants.User).Company, CultureInfo.InvariantCulture),
                SpecSheetPdfName = pdfNameHiddenField.Value,
                ImageName = imageNameHiddenField.Value,
                Description = descriptionTextBox.Text.Trim(),
                Website = websiteTextBox.Text.Trim(),
                ContactId = string.IsNullOrEmpty(contactIdHiddenField.Value) ? 0 : Convert.ToInt32(contactIdHiddenField.Value, CultureInfo.InvariantCulture),
                Contact = new Contact()
                {
                    FirstName = firstNameTextBox.Text.Trim(),
                    LastName = lastNameTextBox.Text.Trim(),
                    Title = titleContactTextBox.Text.Trim(),
                    Email = emailTextBox.Text.Trim(),
                    Phone = phoneTextBox.Text.Trim(),
                    Fax = faxTextBox.Text.Trim()
                },
                StatusId = (int)ProductStatusEnum.Pending,
                RequestedById = GetSession<User>(SessionConstants.User).UserId,
                BrandStandards = string.Empty,
                Culture = Convert.ToString(culture, CultureInfo.InvariantCulture)
            };

            return product;
        }

        /// <summary>
        /// Invoked at CategoriesDropDown SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoriesDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProcessDropDownItemChange(DropDownConstants.CategoriesDropDown, brandDropDown, regionDropDown, null, categoriesDropDown);

                OnSelectedIndexChanged(sender, e);
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }



        /// <summary>
        ///
        /// </summary>
        /// <param name="totalCategoryDropDownCount"></param>
        private void ProcessCategories(int totalCategoryDropDownCount = 0)
        {
            try
            {
                int existingDropDownCount;
                if (totalCategoryDropDownCount == 0)
                    existingDropDownCount = FindOccurence("categoriesDropDown");
                else
                    existingDropDownCount = totalCategoryDropDownCount;

                IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);

                string parentSelectedValue;
                DropDownList parentCategoryDropDown;
                for (int dropdownCount = 1; dropdownCount <= existingDropDownCount; dropdownCount++)
                {
                    DropDownList subCategoryDropDown = null;
                    if (dropdownCount == 1)
                    {
                        parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown");
                        parentSelectedValue = parentCategoryDropDown.SelectedValue;
                        if (existingDropDownCount == 1)
                            subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount + 1).ToString(CultureInfo.InvariantCulture));
                    }
                    else if (dropdownCount == 2)
                    {
                        parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown");
                        parentSelectedValue = parentCategoryDropDown.SelectedValue;
                        subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount - 1).ToString(CultureInfo.InvariantCulture));
                        parentSelectedValue = parentCategoryDropDown.SelectedValue;
                        subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
                    }

                    if (subCategoryDropDown != null)
                    {
                        string selectedValue = subCategoryDropDown.SelectedValue;
                        if (existingDropDownCount == 1)
                            FindPopulateDropDowns(parentSelectedValue, "categoriesDropDown-" + (dropdownCount + 1).ToString(CultureInfo.InvariantCulture));
                        else
                            FindPopulateDropDowns(parentSelectedValue, "categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
                        if (Convert.ToInt16(selectedValue) > 0)
                            subCategoryDropDown.SelectedValue = selectedValue;

                        IList<SearchFilters> filteredAllResultCollection = Helper.GetFilteredCollection(filteredHierarchyCBRTData, Convert.ToInt32(brandDropDown.SelectedValue), Convert.ToInt32(regionDropDown.SelectedValue), 0);

                        IList<ListItem> dropDownDirtyListItem = new List<ListItem>();
                        foreach (ListItem dropDownListItem in subCategoryDropDown.Items)
                        {
                            if (Convert.ToInt16(dropDownListItem.Value) > 0)
                            {
                                IList<SearchFilters> filteredCollection = Helper.GetFilteredCollection(filteredHierarchyCBRTData, Convert.ToInt32(brandDropDown.SelectedValue), Convert.ToInt32(regionDropDown.SelectedValue), 0, Convert.ToInt32(dropDownListItem.Value));

                                if (filteredCollection.Count == 0)
                                    dropDownDirtyListItem.Add(dropDownListItem);
                            }
                        }

                        if (dropDownDirtyListItem.Count > 0)
                            dropDownDirtyListItem.ToList().ForEach(x => subCategoryDropDown.Items.Remove(x));
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at RegionDropDown SelectedIndex Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList lastCategoryDropDown;
                //To reset the country already selected
                countryDropDown.SelectedIndex = -1;
                if (parentCategoryContainer.Controls.Count > 0)
                {
                    lastCategoryDropDown = (DropDownList)FindControl(parentCategoryContainer, lastSelectedCategoryDropDown.Value);
                    IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
                    IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
                    Helper.ProcessDropDownItemChangeForCountry(filteredHierarchyCBRTData, culture, DropDownConstants.RegionDropDown, brandDropDown, regionDropDown, countryDropDown, null, categoriesDropDown, (lastCategoryDropDown == null) ? 0 : Convert.ToInt16(lastCategoryDropDown.Text), searchFiltersData);
                    ProcessCategories();
                }
                else
                {
                    ProcessDropDownItemChangeForCountry(DropDownConstants.RegionDropDown, brandDropDown, regionDropDown, countryDropDown, null, categoriesDropDown);
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at remProductLinkButton Click
        /// </summary>
        /// <param name="senderm"></param>
        /// <param name="e"></param>
        //protected void remProductLinkButton_Click(object senderm, EventArgs e)
        //{
        //    try
        //    {
        //        int productId = Convert.ToInt32(GetViewState<string>(ViewStateConstants.ProductViewState), CultureInfo.InvariantCulture);

        //        int resultId = _productManager.RemoveProduct(productId);

        //        if (resultId == 0)
        //        {
        //            Response.Redirect("MyProducts.aspx", false);
        //        }
        //        else
        //        {
        //            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.ProductNotRemoved, culture));
        //        }
        //    }
        //    catch (ThreadAbortException)
        //    {
        //    }
        //    catch (Exception exception)
        //    {
        //        UserInterfaceExceptionHandler.HandleExcetion(ref exception);
        //        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        //    }
        //}


        [System.Web.Services.WebMethod]
        public static string removePrododuct(string prodIDHid)
        {
            string returnVal = "";
            AddEditProduct objAddEditProduct = new AddEditProduct();
            try
            {
                 IProductManager _productManager;
                _productManager = ProductManager.Instance;
                System.Web.UI.Page ObjPage = new System.Web.UI.Page();
                int productId = Convert.ToInt32(prodIDHid);
                
                int resultId = _productManager.RemoveProduct(productId);

                if (resultId == 0)
                {
                    returnVal = "sucess";
                    return returnVal;
                    //Response.Redirect("MyProducts.aspx", false);
                }
                else
                {
                    returnVal = "ErrInProdDel";
                    return returnVal;
                }
            }
            catch (ThreadAbortException)
            {
                returnVal = "ErrGeneral";
                return returnVal;
            }
            catch (Exception exception)
            {
                returnVal = "ErrGeneral";
                return returnVal;
            }
        }

        /// <summary>
        /// Invoked at FilterButton Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FilterButton_Click(object sender, EventArgs e)
        {
            int partnerId = 0;

            try
            {
                User user = GetSession<User>(SessionConstants.User);
                partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

                _projectTemplate = _partnerManager.GetCategoriesForSubmitProduct(Convert.ToInt32(leafCategoryHdn.Value), Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture), 0, Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture), Convert.ToInt32(countryDropDown.SelectedValue, CultureInfo.InvariantCulture), partnerId);
                IList<Category> rootCategoryList = (_projectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
                categoryDivisionsRepeater.DataSource = rootCategoryList;
                categoryDivisionsRepeater.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Invoked at CategoryDivisionsRepeater ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryDivisionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl categoryDivision = (HtmlGenericControl)e.Item.FindControl("categoryListDivision");
                    Panel subcategoryPanel = new Panel();
                    if ((Convert.ToInt32(((Entities.Category)(e.Item.DataItem)).ParentCategoryId)) == -1 && !(((Entities.Category)(e.Item.DataItem)).IsNewProductsReq).Value)
                    {
                        //if (((Entities.Category)(e.Item.DataItem)).IsLeaf)
                        //{
                        //    subcategoryPanel = CreateMidLevelCategoryDivision((Entities.Category)(e.Item.DataItem));
                        //    //                        Panel pnlSubCategory = new Panel() // COMMENTED BY CODEIT.RIGHT;
                        //    categoryDivision.Controls.Add(subcategoryPanel);
                        //}

                        //else
                        //{
                        subcategoryPanel = CreateMidLevelCategoryDivision((Entities.Category)(e.Item.DataItem));
                        //                        Panel pnlSubCategory = new Panel() // COMMENTED BY CODEIT.RIGHT;
                        categoryDivision.Controls.Add(subcategoryPanel);
                        //}
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Generates Product Assigned Specifications
        /// </summary>
        /// <param name="checkedCategories"></param>
        /// <returns></returns>
        private IList<ProductCategory> GenerateProductAssignedSpecs(string[] checkedCategories)
        {
            ProductCategory productCategory;
            ProductAssignedSpecifications productAssignedSpecifications;
            //ProductPropertyType productPropertyType;
            IList<ProductCategory> productCategoryCollection = new List<ProductCategory>();
            IList<ProductAssignedSpecifications> productAssignedSpecificationsCollection;
            //IList<ProductPropertyType> productPropertyTypeCollection;

            foreach (string categoryItem in checkedCategories)
            {
                //   string removeString = "cphContent_categoryDivisionsRepeater";
                //   string[] checkedCategoriesDetails = categoryItem.Remove(0, removeString.Length + 1).Split(new Char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string[] checkedCategoriesDetails = categoryItem.Split('~');
                int categoryId = Convert.ToInt32(checkedCategoriesDetails[0], CultureInfo.InvariantCulture);
                string categoryDescription = checkedCategoriesDetails[1];
                int brandId = Convert.ToInt32(checkedCategoriesDetails[2], CultureInfo.InvariantCulture);
                string brandName = checkedCategoriesDetails[3];

                int regionId = Convert.ToInt32(checkedCategoriesDetails[4], CultureInfo.InvariantCulture);
                string regionName = checkedCategoriesDetails[5];
                int countryId = Convert.ToInt32(checkedCategoriesDetails[6], CultureInfo.InvariantCulture);
                string countryName = checkedCategoriesDetails[7];
                var productCategoryColl = productCategoryCollection.Where(x => x.CategoryId.Equals(categoryId));
                if (productCategoryColl.Count() == 0)
                {
                    productCategory = new ProductCategory();
                    productCategory.CategoryId = Convert.ToInt32(categoryId);
                    productCategory.CategoryName = Convert.ToString(categoryDescription, CultureInfo.InvariantCulture);

                    productAssignedSpecifications = new ProductAssignedSpecifications(); ;
                    productAssignedSpecifications.BrandId = Convert.ToInt32(brandId);
                    productAssignedSpecifications.BrandDescription = Convert.ToString(brandName, CultureInfo.InvariantCulture);
                    productAssignedSpecifications.RegionId = Convert.ToInt32(regionId);
                    productAssignedSpecifications.RegionDescription = Convert.ToString(regionName, CultureInfo.InvariantCulture);
                    productAssignedSpecifications.CountryId = Convert.ToInt32(countryId);
                    productAssignedSpecifications.CountryDescription = Convert.ToString(countryName, CultureInfo.InvariantCulture);

                    productAssignedSpecificationsCollection = new List<ProductAssignedSpecifications>();
                    productAssignedSpecificationsCollection.Add(productAssignedSpecifications);

                    productCategory.ProductAssignedSpecifications = productAssignedSpecificationsCollection;
                    productCategoryCollection.Add(productCategory);
                }
                else
                {
                    var productCategoriesCollection = productCategoryCollection.Where(x => x.CategoryId.Equals(categoryId)).ToList();
                    if (productCategoriesCollection.Count > 0)
                    {
                        productCategoriesCollection.ForEach(x =>
                        {
                            var productCategoryDetailCollection = x.ProductAssignedSpecifications.Where(z => z.BrandId.Equals(brandId) && z.RegionId.Equals(regionId) && z.CountryId.Equals(countryId));
                            if (productCategoryDetailCollection.Count() == 0)
                            {
                                productAssignedSpecifications = new ProductAssignedSpecifications();
                                productAssignedSpecifications.BrandId = brandId;
                                productAssignedSpecifications.BrandDescription = brandName;
                                productAssignedSpecifications.RegionId = regionId;
                                productAssignedSpecifications.RegionDescription = regionName;
                                productAssignedSpecifications.CountryId = Convert.ToInt32(countryId);
                                productAssignedSpecifications.CountryDescription = countryName;

                                x.ProductAssignedSpecifications.Add(productAssignedSpecifications);
                            }

                        });
                    }
                }
            }

            return productCategoryCollection;
        }

        /// <summary>
        /// Binds the Data to Drop Down
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            categoryDropDown.DataSource = categoryList;
            categoryDropDown.DataTextField = "CategoryName";
            categoryDropDown.DataValueField = "CategoryId";
            categoryDropDown.DataBind();
        }

        /// <summary>
        /// Finds Occurence of control
        /// </summary>
        /// <param name="substr"></param>
        /// <returns></returns>
        private int FindOccurence(string substr)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int count = 0;
            for (int i = 0; i < ctrls.Length; i++)
            {
                if (ctrls[i].Contains(substr + "-") && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Populate Dropdowns
        /// </summary>
        /// <param name="parentCategoryDropDown"></param>
        /// <param name="selectedValue"></param>
        private void PopulateDropDown(DropDownList parentCategoryDropDown, string selectedValue)
        {
            parentCategoryDropDown.Items.Clear();
            IList<Category> categoryList = new List<Category>();
            if (selectedValue.Equals(String.Empty))
            {
                categoryList = GetRootLevelCategories();
            }
            else if (selectedValue != "0")
            {
                categoryList = GetChildCategories(selectedValue);
            }
            if (categoryList != null)
            {
                BindData(parentCategoryDropDown, categoryList);
            }
            parentCategoryDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(223, culture), "0"));
        }

        /// <summary>
        /// Populates Drop Down Data
        /// </summary>
        private void PopulateDropDownData()
        {
            int partnerId;
            User user = GetSession<User>(SessionConstants.User);
            partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);
            CategoriesFilterHierarchy searchFilters;
            IList<Category> categoryItems;
            if (GetSession(UIConstants.Mode) != null && GetSession<string>(UIConstants.Mode) == UIConstants.Edit)
            {
                categoryItems = _partnerManager.GetAddEditProductActiveCategories(partnerId, (int)ModeEnum.Edit);
                searchFilters = _partnerManager.GetSubmitProductSearchFilterData(partnerId, (int)ModeEnum.Edit);
            }
            else
            {
                categoryItems = _partnerManager.GetAddEditProductActiveCategories(partnerId, (int)ModeEnum.Add);
                searchFilters = _partnerManager.GetSubmitProductSearchFilterData(partnerId, (int)ModeEnum.Add);
            }

            // SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilters);
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilters.FilteredCBRTData);
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData, searchFilters.FilteredHierarchyCBRTData);

            SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categoryItems);
            BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), null);
            BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1210, culture, "All Categories"), null);
            BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(1191, culture, "All Brands"), null);
            //BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(1197, culture, "All Types"), null);
            BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1206, culture, "All Regions"), null);
        }

        /// <summary>
        /// Find populated DropDowns
        /// </summary>
        /// <param name="selectedCategoryId"></param>
        /// <param name="id"></param>
        private void FindPopulateDropDowns(string selectedCategoryId, string id)
        {
            DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, id);
            PopulateDropDown(newDropDown, selectedCategoryId);
        }

        /// <summary>
        /// List Child Categories
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {

            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

            IList<Category> categoryList = (categories.AsEnumerable().
                                           Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture))).ToList());

            //To put logic to filter per selected BRT
            IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
            int brandSelectedValue = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int regionSelectedValue = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture);

            var filterdCatgories = categoryList.Join(filteredHierarchyCBRTData, c => c.CategoryId, fh => fh.CatId, (categ, filterHier) => new { categ, filterHier })
            .Where(item => ((regionSelectedValue == 0) ? true : item.filterHier.RegionId.Equals(regionSelectedValue))
                                                                                                     && ((brandSelectedValue == 0) ? true : item.filterHier.BrandId.Equals(brandSelectedValue))).Select(y => y.categ).Distinct().ToList();

            return filterdCatgories;
        }

        /// <summary>
        /// Gets Root level Category
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetRootLevelCategories()
        {
            IList<Category> rootCategoryList = (GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState).AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
            return rootCategoryList;
        }

        /// <summary>
        /// Get categories from view state
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(int currentSelectedCategoryId)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            Category category = (Category)(categories.Where(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId)).Single());
            return category;
        }

        /// <summary>
        /// Recreate Dynamic Controls
        /// </summary>
        /// <param name="ctrlPrefix"></param>
        /// <param name="ctrlType"></param>
        private void RecreateControls(string ctrlPrefix, string ctrlType)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int cnt = FindOccurence(ctrlPrefix);
            if (cnt > 0)
            {
                for (int k = 1; k <= cnt; k++)
                {
                    for (int i = 0; i < ctrls.Length; i++)
                    {
                        if (ctrls[i].Contains(ctrlPrefix + "-" + (k + 1).ToString(CultureInfo.InvariantCulture)) && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                        {
                            string ctrlID = string.Empty;
                            string[] subCtrls = ctrls[i].Split('-');

                            for (int l = 0; l < subCtrls.Length; l++)
                            {
                                if (subCtrls[l].Contains("categoriesDropDown"))
                                {
                                    ctrlID = subCtrls[l + 1].Substring(0, 1);
                                }
                            }

                            ctrlID = "categoriesDropDown-" + ctrlID;

                            if (ctrlType == "DropDownList")
                            {
                                CreateDropDownList(ctrlID);
                            }
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates a New Drop Down List
        /// </summary>
        /// <param name="ID"></param>
        protected void CreateDropDownList(string ID)
        {
            DropDownList parentCategoryDropDown = new DropDownList();
            parentCategoryDropDown.ID = ID;
            parentCategoryDropDown.AutoPostBack = true;
            parentCategoryDropDown.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);

            parentCategoryContainer.Controls.Add(parentCategoryDropDown);
        }

        /// <summary>
        /// Fires On Change Index
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);

            DropDownList ddl = (DropDownList)sender;
            string id = ddl.ID;
            int existingDropDownCount = FindOccurence("categoriesDropDown");
            if (!id.Contains("-"))
            {
                id = id + "-1";
            }
            string[] idSequence = id.Split('-');

            int currentSelectedCategoryId = Convert.ToInt32(ddl.SelectedValue.ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);

            for (int dropdownCount = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1; dropdownCount <= existingDropDownCount + 1; dropdownCount++)
            {
                DropDownList rowDivision = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
                parentCategoryContainer.Controls.Remove(rowDivision);
            }
            if (currentSelectedCategoryId != 0)
            {
                SetViewState(ViewStateConstants.SelectedCategory, (int?)currentSelectedCategoryId);
                Category currentSelectedCategory = ReadCategory(currentSelectedCategoryId);
                if (!(currentSelectedCategory.IsLeaf))
                {
                    IList<Category> childCategoryList = GetChildCategories(currentSelectedCategoryId.ToString());
                    if (childCategoryList.Count() > 0)
                    {
                        int nextID = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1;

                        CreateDropDownList("categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
                        FindPopulateDropDowns(currentSelectedCategoryId.ToString(), "categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
                    }
                }
            }
            if (ddl.SelectedIndex == 0)
            {
                if (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) > 1)
                {
                    string parentDropDownId;
                    if (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) == 2)
                    {
                        parentDropDownId = "categoriesDropDown";
                    }
                    else
                    {
                        parentDropDownId = "categoriesDropDown-" + (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) - 1).ToString(CultureInfo.InvariantCulture);
                    }

                    DropDownList parentDropDown = (DropDownList)parentCategoryContainer.FindControl(parentDropDownId);
                    int parentDropDownValue = Convert.ToInt32(parentDropDown.SelectedValue, CultureInfo.InvariantCulture);
                    SetViewState(ViewStateConstants.SelectedCategory, (int?)parentDropDownValue);
                    leafCategoryHdn.Value = parentDropDownValue.ToString();
                    lastSelectedCategoryDropDown.Value = parentDropDownId;
                    Helper.ProcessDropDownItemChange(filteredHierarchyCBRTData, culture, DropDownConstants.CategoriesDropDown, brandDropDown, regionDropDown, null, parentDropDown);
                }
                else
                {
                    int categoriesDropDownValue = Convert.ToInt32(categoriesDropDown.SelectedValue, CultureInfo.InvariantCulture);
                    SetViewState(ViewStateConstants.SelectedCategory, (int?)categoriesDropDownValue);
                    leafCategoryHdn.Value = categoriesDropDown.SelectedValue;
                    lastSelectedCategoryDropDown.Value = "categoriesDropDown";
                }
            }
            else
            {
                SetViewState(ViewStateConstants.SelectedCategory, (int?)currentSelectedCategoryId);
                leafCategoryHdn.Value = currentSelectedCategoryId.ToString();
                lastSelectedCategoryDropDown.Value = ddl.ID;
                Helper.ProcessDropDownItemChange(filteredHierarchyCBRTData, culture, DropDownConstants.CategoriesDropDown, brandDropDown, regionDropDown, null, ddl);
            }
        }

        /// <summary>
        /// Creating Levels for Categories
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>

        private Panel CreateMidLevelCategoryDivision(Category category)
        {
            //   _repeaterRowsCount += 1;
            Panel categoryPanel = new Panel();
            string categoryName = category.CategoryName;
            string categoryId = category.CategoryId.ToString(CultureInfo.InvariantCulture);

            categoryPanel.ID = "panel" + categoryId;
            HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H3");
            headingCategoryName.InnerText = categoryName;

            Label spanOpen = new Label();
            spanOpen.Attributes.Add("class", "acc-open tg");
            spanOpen.Text = ResourceUtility.GetLocalizedString(1182, culture, "Open");
            headingCategoryName.Controls.Add(spanOpen);

            Label spanClose = new Label();
            spanClose.Attributes.Add("class", "acc-close tg");
            spanClose.Text = ResourceUtility.GetLocalizedString(1183, culture, "Close");
            headingCategoryName.Controls.Add(spanClose);
            categoryPanel.Controls.Add(headingCategoryName);

            IEnumerable<Category> subCategoryCollection = _projectTemplate.Categories.Where(p => (p.RootParentId == Convert.ToInt32(categoryId, CultureInfo.InvariantCulture)));

            if (subCategoryCollection.Count() >= 1)
            {
                int sameSubCategoryId = 0;
                int sameSubCategoryList = 1;
                foreach (Category subcategory in subCategoryCollection)
                {
                    Panel subcategoryPanel = new Panel();

                    Category secondLevelParent = _projectTemplate.Categories.Where(p => p.CategoryId == subcategory.ParentCategoryId).Single();

                    if (secondLevelParent.ParentCategoryId == -1)
                        subcategoryPanel.Attributes.Add("class", "panel");

                    string subcategoryName = subcategory.CategoryName;
                    string subcategoryLevel = subcategory.Level.ToString(CultureInfo.InvariantCulture);
                    string subcategoryId = subcategory.CategoryId.ToString(CultureInfo.InvariantCulture);
                    if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                    {
                        subcategoryPanel.ID = "panel" + subcategoryId + "_" + sameSubCategoryList;
                        sameSubCategoryList = sameSubCategoryList + 1;
                    }
                    else
                    {
                        subcategoryPanel.ID = "panel" + subcategoryId;
                        sameSubCategoryList = 1;
                    }
                    int marginFromLeft = (Convert.ToInt32(subcategoryLevel, CultureInfo.InvariantCulture) - 1) * 30;

                    if (subcategory.IsLeaf)
                    {
                        HtmlTable categoryDetailsTable = new HtmlTable();
                        categoryDetailsTable.Attributes.Add("class", "cat-table");
                        categoryDetailsTable.CellPadding = 0;
                        categoryDetailsTable.CellSpacing = 0;
                        HtmlTableRow row = new HtmlTableRow();
                        HtmlTableCell cell;

                        HtmlGenericControl headingApply = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        HtmlGenericControl applyBorder = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        HtmlGenericControl divStandards = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");

                        divStandards.ViewStateMode = ViewStateMode.Enabled;
                        divStandards.ClientIDMode = ClientIDMode.Static;

                        if (subcategory.IsNewProductsReq.HasValue && subcategory.IsNewProductsReq.Value)
                        {
                            for (int colCount = 1; colCount <= 5; colCount++)
                            {
                                cell = new HtmlTableCell();

                                if (colCount == 1)
                                {
                                    CheckBox opportunityCheckBox = new CheckBox();
                                    opportunityCheckBox.ClientIDMode = ClientIDMode.Static;
                                    var id = new StringBuilder();
                                    id.Append(subcategory.CategoryId);
                                    id.Append("~");
                                    id.Append(subcategory.CategoryName);
                                    id.Append("~");
                                    id.Append(subcategory.BrandId);
                                    id.Append("~");
                                    id.Append(subcategory.BrandName);
                                    id.Append("~");
                                    id.Append(subcategory.RegionId);
                                    id.Append("~");
                                    id.Append(subcategory.RegionName);
                                    id.Append("~");
                                    id.Append(subcategory.CountryId);
                                    id.Append("~");
                                    id.Append(subcategory.CountryName);
                                    opportunityCheckBox.ID = Convert.ToString(id, CultureInfo.InvariantCulture);

                                    opportunityCheckBox.Attributes.Add("runat", "server");
                                    if (GetSession(UIConstants.Mode) != null && GetSession(UIConstants.Mode).ToString() == UIConstants.Edit)
                                        opportunityCheckBox.Enabled = false;
                                    cell.Attributes.Add("class", "col1");
                                    cell.Controls.Add(opportunityCheckBox);
                                }
                                else if (colCount == 2)
                                {
                                    Label brandLabel = new Label();
                                    cell.Controls.Add(brandLabel);
                                    if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                                    {
                                        brandLabel.ID = "lblBrand_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                        sameSubCategoryList = sameSubCategoryList + 1;
                                    }
                                    else
                                    {
                                        brandLabel.ID = "lblBrand_" + subcategory.CategoryId;
                                        sameSubCategoryList = 1;
                                    }
                                    brandLabel.Text = subcategory.BrandName;
                                    brandLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col2");
                                }

                                else if (colCount == 3)
                                {
                                    Label regionLabel = new Label();
                                    cell.Controls.Add(regionLabel);
                                    if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                                    {
                                        regionLabel.ID = "lblRegion_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                        sameSubCategoryList = sameSubCategoryList + 1;
                                    }
                                    else
                                    {
                                        regionLabel.ID = "lblRegion_" + subcategory.CategoryId;
                                        sameSubCategoryList = 1;
                                    }

                                    regionLabel.Text = subcategory.RegionName;
                                    regionLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col3");
                                }
                                else if (colCount == 4)
                                {
                                    Label countryLabel = new Label();
                                    cell.Controls.Add(countryLabel);
                                    if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                                    {
                                        countryLabel.ID = "lblCountry_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                        sameSubCategoryList = sameSubCategoryList + 1;
                                    }
                                    else
                                    {
                                        countryLabel.ID = "lblCountry_" + subcategory.CategoryId;
                                        sameSubCategoryList = 1;
                                    }

                                    countryLabel.Text = subcategory.CountryName;
                                    countryLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col4");
                                }
                                else if (colCount == 5)
                                {
                                    if (!string.IsNullOrEmpty(subcategory.StandardDescription))
                                    {
                                        LinkButton standardsLinkButton = new LinkButton();
                                        standardsLinkButton.ClientIDMode = ClientIDMode.Static;
                                        standardsLinkButton.ViewStateMode = ViewStateMode.Enabled;
                                        if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                                        {
                                            standardsLinkButton.ID = "lnkShow_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                            divStandards.ID = "divStandards_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                            sameSubCategoryList = sameSubCategoryList + 1;
                                        }
                                        else
                                        {
                                            standardsLinkButton.ID = "lnkShow_" + subcategory.CategoryId;
                                            divStandards.ID = "divStandards_" + subcategory.CategoryId;
                                            sameSubCategoryList = 1;
                                        }

                                        standardsLinkButton.Text = ResourceUtility.GetLocalizedString(670, culture);
                                        standardsLinkButton.Font.Bold = true;
                                        //Create event name
                                        var eventName = new StringBuilder();
                                        eventName.Append("SelectClickedButton('");
                                        eventName.Append(standardsLinkButton.ID);
                                        eventName.Append("','");
                                        eventName.Append(divStandards.ID);
                                        eventName.Append("');return false;");
                                        standardsLinkButton.OnClientClick = Convert.ToString(eventName, CultureInfo.InvariantCulture);

                                        cell.Controls.Add(standardsLinkButton);

                                        divStandards.InnerHtml = "<table><tr><td class='abc'><ul>" + Server.HtmlDecode(subcategory.StandardDescription) + "</ul></td></tr></table>";
                                        cell.Attributes.Add("class", "col5");
                                    }
                                }
                                row.Cells.Add(cell);
                            }

                            categoryDetailsTable.Rows.Add(row);

                            divStandards.Attributes.Add("class", "description");
                            if (subcategory.CategoryId != 0 && subcategory.CategoryId != sameSubCategoryId)
                            {
                                HtmlGenericControl headingSubCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H6");
                                headingSubCategoryName.InnerText = subcategoryName;
                                headingSubCategoryName.Style.Add("padding-left", marginFromLeft + "px");
                                subcategoryPanel.Controls.Add(headingSubCategoryName);

                                sameSubCategoryId = subcategory.CategoryId;
                                headingApply.InnerHtml = ResourceUtility.GetLocalizedString(229, culture);
                                headingApply.Attributes.Add("class", "apply");

                                applyBorder.Style.Add("border-bottom", "1px dotted");
                                subcategoryPanel.Controls.Add(headingApply);
                                subcategoryPanel.Controls.Add(applyBorder);
                            }
                        }

                        subcategoryPanel.Controls.Add(categoryDetailsTable);
                        subcategoryPanel.Controls.Add(divStandards);
                    }
                    else
                    {
                        HtmlGenericControl headingSubCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H6");
                        headingSubCategoryName.InnerText = subcategoryName;
                        headingSubCategoryName.Style.Add("padding-left", marginFromLeft + "px");
                        subcategoryPanel.Controls.Add(headingSubCategoryName);
                    }

                    if (categoryPanel.ID.ToString(CultureInfo.InvariantCulture) == "panel" + subcategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))

                        categoryPanel.Controls.Add(subcategoryPanel);

                    else //last;
                    {
                        Panel pnlParent = new Panel();
                        foreach (Control p in categoryPanel.Controls)
                        {
                            if (p is Panel)
                            {
                                pnlParent = (Panel)FindControl(categoryPanel, "panel" + subcategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                if (pnlParent != null)
                                    pnlParent.Controls.Add(subcategoryPanel);
                            }
                        }
                    }
                }
            }
            else
            {
                if (category.IsLeaf)
                {
                    HtmlTable categoryDetailsTable = new HtmlTable();
                    categoryDetailsTable.Attributes.Add("class", "cat-table");
                    categoryDetailsTable.CellPadding = 0;
                    categoryDetailsTable.CellSpacing = 0;

                    IEnumerable<Category> sameParentAndLeafCategoryCollection = _projectTemplate.Categories.Where(p => (p.CategoryId == Convert.ToInt32(categoryId, CultureInfo.InvariantCulture) && p.ParentCategoryId == -1 && p.IsLeaf && p.BrandName != string.Empty));

                    HtmlTableRow row;// = new HtmlTableRow();
                    HtmlTableCell cell;

                    HtmlGenericControl headingApply = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    HtmlGenericControl applyBorder = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    HtmlGenericControl divStandards = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    divStandards.ClientIDMode = ClientIDMode.Static;
                    int panelIndex = 0;

                    foreach (Category category1 in sameParentAndLeafCategoryCollection)
                    {
                        panelIndex += 1;
                        Panel subcategoryPanel1 = new Panel();
                        subcategoryPanel1.ID = "panel" + category1.CategoryId + "_" + panelIndex;

                        if (category1.IsNewProductsReq.HasValue && category1.IsNewProductsReq.Value)
                        {
                            row = new HtmlTableRow();
                            for (int colCount = 1; colCount <= 6; colCount++)
                            {
                                cell = new HtmlTableCell();

                                if (colCount == 1)
                                {
                                    CheckBox opportunityCheckBox = new CheckBox();
                                    opportunityCheckBox.ClientIDMode = ClientIDMode.Static;
                                    //Create the check box id
                                    var id = new StringBuilder();
                                    id.Append(category1.CategoryId);
                                    id.Append("~");
                                    id.Append(category1.CategoryName);
                                    id.Append("~");
                                    id.Append(category1.BrandId);
                                    id.Append("~");
                                    id.Append(category1.BrandName);
                                    id.Append("~");
                                    id.Append(category1.RegionId);
                                    id.Append("~");
                                    id.Append(category1.RegionName);

                                    opportunityCheckBox.ID = Convert.ToString(id, CultureInfo.InvariantCulture);

                                    opportunityCheckBox.Attributes.Add("runat", "server");
                                    //cell.Style.Add("width", "10%");
                                    if (GetSession(UIConstants.Mode) != null && GetSession(UIConstants.Mode).ToString() == UIConstants.Edit)
                                        opportunityCheckBox.Enabled = false;
                                    cell.Controls.Add(opportunityCheckBox);
                                    cell.Attributes.Add("class", "col1");
                                }
                                else if (colCount == 2)
                                {
                                    Label brandLabel = new Label();
                                    cell.Controls.Add(brandLabel);
                                    brandLabel.ID = "lblBrand_" + category1.CategoryId;
                                    brandLabel.Text = category1.BrandName;
                                    brandLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col2");
                                }

                                else if (colCount == 3)
                                {
                                    Label regionLabel = new Label();
                                    cell.Controls.Add(regionLabel);
                                    regionLabel.ID = "lblRegion_" + category1.CategoryId;
                                    regionLabel.Text = category1.RegionName;
                                    regionLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col3");
                                }
                                else if (colCount == 4)
                                {
                                    Label countryLabel = new Label();
                                    cell.Controls.Add(countryLabel);
                                    countryLabel.ID = "lblCountry_" + category1.CategoryId;
                                    countryLabel.Text = category1.CountryName;
                                    countryLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col3");
                                }
                                else if (colCount == 5)
                                {
                                    if (!string.IsNullOrEmpty(category1.StandardDescription))
                                    {
                                        LinkButton standardsLinkButton = new LinkButton();
                                        standardsLinkButton.ClientIDMode = ClientIDMode.Static;

                                        standardsLinkButton.ID = "lnkShow_" + category1.CategoryId;
                                        divStandards.ID = "divStandards_" + category1.CategoryId;

                                        cell.Controls.Add(standardsLinkButton);
                                        standardsLinkButton.Text = ResourceUtility.GetLocalizedString(670, culture);
                                        standardsLinkButton.Font.Bold = true;

                                        var eventName = new StringBuilder();
                                        eventName.Append("SelectClickedButton('");
                                        eventName.Append(standardsLinkButton.ID);

                                        eventName.Append("','");
                                        eventName.Append(divStandards.ID);

                                        eventName.Append("');return false");

                                        standardsLinkButton.OnClientClick = Convert.ToString(eventName, CultureInfo.InvariantCulture);

                                        divStandards.InnerHtml = "<table><tr><td class='abc'><ul>" + Server.HtmlDecode(category1.StandardDescription) + "</ul></td></tr></table>";
                                        cell.Attributes.Add("class", "col5");
                                    }
                                }

                                row.Cells.Add(cell);
                            }

                            categoryDetailsTable.Rows.Add(row);

                            divStandards.Attributes.Add("class", "description");
                            headingApply.InnerHtml = ResourceUtility.GetLocalizedString(226, culture);
                            headingApply.Attributes.Add("class", "apply");
                            applyBorder.Style.Add("border-bottom", "1px dotted");
                            subcategoryPanel1.Controls.Add(headingApply);
                            subcategoryPanel1.Controls.Add(applyBorder);
                            subcategoryPanel1.Controls.Add(categoryDetailsTable);
                            subcategoryPanel1.Controls.Add(divStandards);
                        }
                        else
                        {
                            categoryPanel.Controls.Remove(headingCategoryName);
                        }
                        categoryPanel.Controls.Add(subcategoryPanel1);
                    }
                }
            }

            return categoryPanel;
        }


        /// <summary>
        /// Sets The Page level Settings
        /// </summary>
        private void SetPageSettings()
        {
            if (GetSession(UIConstants.Mode) != null && GetSession<string>(UIConstants.Mode) == UIConstants.Edit)
            {
                SetViewState(UIConstants.Mode, UIConstants.Edit);

                int productId = Convert.ToInt32(GetSession(SessionConstants.ProductId), CultureInfo.InvariantCulture);
                SetViewState(ViewStateConstants.ProductViewState, productId.ToString(CultureInfo.InvariantCulture));

                ClearSession(SessionConstants.ProductId);

                PopulateProductDetails(productId);

                submitLinkButton.Text = ResourceUtility.GetLocalizedString(1122, culture, "Update Product");

                SetLabelText(submitHeaderLabel, ResourceUtility.GetLocalizedString(1014, culture, "View Product"));
                SetLabelText(titleLabel, ResourceUtility.GetLocalizedString(1014, culture, "View Product"));
                hidProductID.Value = GetViewState<string>(ViewStateConstants.ProductViewState);
            }
            else
            {
                submitLinkButton.Text = ResourceUtility.GetLocalizedString(702, culture, "Submit Product");
                SetViewState(UIConstants.Mode, UIConstants.Add);
                remProductLinkButton.Visible = false;
                ClearSession(SessionConstants.ProductSearch);
                SetLabelText(submitHeaderLabel, ResourceUtility.GetLocalizedString(1015, culture, "Submit Product For Review"));
                SetLabelText(titleLabel, ResourceUtility.GetLocalizedString(93, culture, "Request Product"));
            }
        }

        /// <summary>
        /// Populate Product Details
        /// </summary>
        /// <param name="productId"></param>
        private void PopulateProductDetails(int productId)
        {
            User user = GetSession<User>(SessionConstants.User);
            Product product = _productManager.GetProductDetailsForReview(productId);

            SetTextBoxText(productNameTextBox, product.ProductName);
            SetTextBoxText(productSKUTextBox, product.ProductSku);
            productImage.ImageUrl = string.IsNullOrEmpty(product.ImageName) ? "../Images/NoProduct.PNG" : product.ProductThumbnailImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(product.ProductThumbnailImage) : "../Images/NoProduct.PNG";
            specSheetPdfLinkButton.Text = product.SpecSheetPdfName;
            imageLinkButton.Text = product.ImageName;

            SetTextBoxText(websiteTextBox, product.Website);
            SetTextBoxText(descriptionTextBox, product.Description);

            //Make Field ReadOnly
            //  productNameTextBox.ReadOnly = true;
            productSKUTextBox.ReadOnly = true;
            websiteTextBox.ReadOnly = true;
            descriptionTextBox.ReadOnly = true;
            brandDropDown.Enabled = false;
            categoriesDropDown.Enabled = false;
            regionDropDown.Enabled = false;
            countryDropDown.Enabled = false;
            //propertyTypeDropDown.Enabled = false;
            filterButton.Enabled = false;

            //Editable Fields
            SetTextBoxText(firstNameTextBox, product.Contact.FirstName);
            SetTextBoxText(lastNameTextBox, product.Contact.LastName);
            SetTextBoxText(titleContactTextBox, product.Contact.Title);
            SetTextBoxText(emailTextBox, product.Contact.Email);
            SetTextBoxText(phoneTextBox, product.Contact.Phone);
            SetTextBoxText(faxTextBox, product.Contact.Fax);
            //Editable Fields

            pdfNameHiddenField.Value = product.SpecSheetPdfName;
            imageNameHiddenField.Value = product.ImageName;

            IList<Category> rootCategoryCollection = new List<Category>();
            IList<ProductCategory> productCategoryCollection = _productManager.GetProductAssignedSpecifications(productId);
            ArrayList checkBoxListIds = new ArrayList();
            string allCheckedCategories = string.Empty;
            string leafCategoryName = string.Empty;
            Int16 pageMode;

            if (GetSession(UIConstants.Mode) != null && GetSession<string>(UIConstants.Mode) == UIConstants.Edit)
            {
                pageMode = (int)ModeEnum.Edit;
            }
            else
            {
                pageMode = (int)ModeEnum.Add;
            }
            IList<Category> categoryCollection = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

            int catId = 0;

            foreach (ProductCategory pc in productCategoryCollection)
            {
                IList<ProductAssignedSpecifications> productSpecs = pc.ProductAssignedSpecifications;

                foreach (ProductAssignedSpecifications pas in productSpecs)
                {
                    int brandId = pas.BrandId;
                    int regionId = pas.RegionId;
                    int countryId = pas.CountryId;
                    int categoryId = pas.CategoryId;
                    //var propTypes = pas.PropertyTypes.ToList();

                    //propTypes.ForEach(x =>
                    //{
                    //    if (catId != x.CategoryId)
                    //    {
                            ProjectTemplate prjTemplate = _partnerManager.GetCategoriesForSubmittedProducts(categoryId, brandId, 0, regionId, countryId, Convert.ToInt32(user.Company, CultureInfo.InvariantCulture), pageMode);
                            var rootCategoryList = (prjTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();

                            rootCategoryList.ForEach(c =>
                            {
                                if (rootCategoryCollection.Where(i => i.CategoryId.Equals(c.CategoryId)).Count() == 0)
                                    rootCategoryCollection.Add(c);
                            });
                            var category = categoryCollection.Where(c => c.CategoryId.Equals(categoryId)).FirstOrDefault();
                            leafCategoryName = (category != null) ? category.CategoryName : string.Empty;
                           // catId = x.CategoryId;
                        //}
                        checkBoxListIds.Add(Convert.ToString(categoryId, CultureInfo.InvariantCulture) + "~" + leafCategoryName + "~" + Convert.ToString(brandId, CultureInfo.InvariantCulture) + "~" + pas.BrandDescription + "~" + Convert.ToString(regionId, CultureInfo.InvariantCulture) + "~" + pas.RegionDescription + "~" + Convert.ToString(countryId, CultureInfo.InvariantCulture) + "~" + pas.CountryDescription);
                    //});
                }
            }

            _projectTemplate = _partnerManager.GetCategoriesForSubmittedProducts(0, 0, 0, 0, 0, Convert.ToInt32(user.Company, CultureInfo.InvariantCulture), pageMode);
            categoryDivisionsRepeater.DataSource = rootCategoryCollection;
            categoryDivisionsRepeater.DataBind();

            String[] strArr = (String[])checkBoxListIds.ToArray(typeof(string));

            allCheckedCategories = string.Join(";", strArr);
            selectedCategoriesHiddenField.Value = allCheckedCategories;

            Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "checkSelectedCheckbox", "checkSelectedCheckbox()", true);
        }


        

        /// <summary>
        /// To Remove uploaded Image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageLinkButton.Text = string.Empty;
                imageNameHiddenField.Value = string.Empty;
                productImage.ImageUrl = "../Images/NoProduct.PNG";
                ClearSession(SessionConstants.ProductImageBytes);
                removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "SpecSheetScript1", "SetFileInputStyles();", true);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Process file download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            string extension = string.Empty;

            switch (fileType)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "image":
                    string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    int upperBound = tempString.GetUpperBound(0);
                    contentType = "image/" + tempString[upperBound];
                    break;
                default:
                    contentType = "";
                    break;
            };

            Response.Buffer = true;
            Response.Charset = "";

            Response.ClearHeaders();
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
            Response.BinaryWrite(fileBytes);
            Response.Flush();
            Response.Clear();
            Response.End();
        }

        /// <summary>
        /// To get the product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SpecSheetPdfLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (GetViewState<string>(UIConstants.Mode) == UIConstants.Edit)
                {
                    Product product = _productManager.GetProductDetails(Convert.ToInt32(GetViewState<string>(ViewStateConstants.ProductViewState)));
                    ProcessFileDownload("pdf", product.SpecSheetPdf, product.SpecSheetPdfName);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            ScriptManager.RegisterClientScriptBlock(specSheetUploadUpdatePanel, specSheetUploadUpdatePanel.GetType(), "SpecSheetScript", "SetFileInputStyles();", true);
        }

        /// <summary>
        /// To get product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (GetViewState<string>(UIConstants.Mode) == UIConstants.Edit)
                {
                    Product product = _productManager.GetProductImage(Convert.ToInt32(GetViewState<string>(ViewStateConstants.ProductViewState)));
                    ProcessFileDownload("image", product.ProductImage, product.ImageName);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "SpecSheetScript1", "SetFileInputStyles();", true);
        }

        /// <summary>
        /// To Remove uploaded PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemovePdfLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                pdfNameHiddenField.Value = string.Empty;
                specSheetPdfLinkButton.Text = string.Empty;
                ClearSession(SessionConstants.ProductSpecSheetPdf);
                specSheetPdfLinkButton.Text = "";
                removePdfLinkButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                ScriptManager.RegisterClientScriptBlock(specSheetUploadUpdatePanel, specSheetUploadUpdatePanel.GetType(), "SpecSheetScript", "SetFileInputStyles();", true);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Event on Click Standards
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnClickStandards(object sender, EventArgs e)
        {
            LinkButton buttonLinkButton = (LinkButton)sender;
            string ID = buttonLinkButton.ID;
            if (ID.Contains("showLinkButton"))
            {
                LinkButton hideLinkButton = (LinkButton)Page.FindControl("hideStandardsLinkButton");
                hideLinkButton.Style.Add("display", "none");
            }
            else if (ID.Contains("hideLinkButton"))
            {
                LinkButton showLinkButton = (LinkButton)Page.FindControl("showStandardsLinkButton");
                showLinkButton.Style.Add("display", "none");
            }
        }

        /// <summary>
        /// Create Category Hierarchy Structure
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        private string CreateCategoryHierarchyStructure(int categoryId)
        {
            string categoryHierarchy = string.Empty;
            IList<Category> categoryCollection = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            var category = categoryCollection.Where(c => c.CategoryId.Equals(categoryId)).FirstOrDefault();
            if (category != null)
            {
                categoryHierarchy = category.CategoryName;
            }
            return categoryHierarchy;
        }

    }
}