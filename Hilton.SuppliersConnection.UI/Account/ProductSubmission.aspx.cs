﻿using System;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Account
{
    public partial class ProductSubmission : PageBase
    {
        /// <summary>
        /// Invoked at OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            ValidateUserAccess((int)MenuEnum.MyProducts);

            this.Title = ResourceUtility.GetLocalizedString(94, culture, "Request Product");
        }
    }
}