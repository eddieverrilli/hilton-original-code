﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI; 
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class MyProducts : PageBase
    {
        private IProductManager _productManager;
        private IPartnerManager _partnerManager;
        private IHelperManager _helperManager;


        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedBrands_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                var brands = sender as TextBox;
                //brandCollection.Value = ProcessDropDownItemChange(DropDownConstants.BrandDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Register for post backs
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            brands.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(optgroup, p.DataValueField)));
            var brandId = brands;
            brandCollection.Value = string.Join(",", brandId.ToList().Select(p => p.DataValueField));
            base.Render(writer);
        }
        /// <summary>
        /// Get the instance of the manager class
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs args)
        {
            try
            {
                base.OnInit(args);

                ValidateUserAccess((int)MenuEnum.MyProducts);
                bool isConstructionReportAccessible = ValidateMenuAuthorization((int)MenuEnum.ConstructionReport);
                if (!isConstructionReportAccessible)
                    constructionReportListItem.Visible = false;
                else
                    constructionReportListItem.Visible = true;

                _productManager = ProductManager.Instance;
                _partnerManager = PartnerManager.Instance;
                _helperManager = HelperManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(92, culture, "My Products");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at GridViewMyProductDetails RowDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridViewMyProductDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow productRow = e.Row;
                if (productRow.RowType == DataControlRowType.DataRow)
                {
                    Product product = (Product)productRow.DataItem;
                    ImageButton imageButton = (ImageButton)productRow.FindControl("productImage");
                    string[] tempString = product.ImageName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    string contentType;
                    if (tempString.Length > 0)
                    {
                        int upperBound = tempString.GetUpperBound(0);
                        contentType = "data:image/" + tempString[upperBound] + ";base64";
                    }
                    else
                    {
                        contentType = "data:image/png;base64";
                    }
                    imageButton.ImageUrl = (!string.IsNullOrEmpty(product.ImageName) && product.ProductThumbnailImage != null) ? contentType + "," + Convert.ToBase64String(product.ProductThumbnailImage) : "../Images/noproduct.png";

                    if (!string.IsNullOrEmpty(product.ImageName) && product.ProductThumbnailImage != null)
                    {
                        imageButton.OnClientClick = string.Format("$(this).DownloadImage({{productId:'{0}'}});return false;", product.ProductId);
                    }
                    else
                    {
                        imageButton.OnClientClick = "return false;";
                        imageButton.Visible = false;
                    }

                    if (product.SpecSheetPdfName != null && !String.IsNullOrEmpty(product.SpecSheetPdfName))
                    {
                        Image imgpdfIcon = e.Row.FindControl("ImgpdfIcon") as Image;
                        imgpdfIcon.Visible = true;

                        LinkButton downLoadSpecSheetLinkButton = e.Row.FindControl("downLoadSpecSheetLinkButton") as LinkButton;
                        downLoadSpecSheetLinkButton.Visible = true;
                        downLoadSpecSheetLinkButton.OnClientClick = string.Format("$(this).DownloadImage({{productId:'{0}',mode:'{1}'}});return false;", product.ProductId, "1");
                      
                    }

                    LinkButton editButton = (LinkButton)(productRow.FindControl("editLinkButton"));

                    if (product.StatusDescription.ToLower().Equals(ProductStatusEnum.Approved.ToString().ToLower(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        editButton.Visible = true;
                    }
                    else
                    {
                        editButton.Visible = false;
                    }
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
            }
        }

        /// <summary>
        ///  Grid Row Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridViewMyProductDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DownloadPDF", StringComparison.InvariantCultureIgnoreCase))
                {
                    int productId = Convert.ToInt32(e.CommandArgument, CultureInfo.InvariantCulture);
                    Product productPDF = _helperManager.GetProductPdf(productId);
                    string fileName = productPDF.SpecSheetPdfName;
                    byte[] fileBytes = productPDF.SpecSheetPdf;
                    ProcessFileDownload("pdf", fileBytes, fileName); 
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Process file download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            string extension = string.Empty;

            switch (fileType)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "image":
                    string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    int upperBound = tempString.GetUpperBound(0);
                    contentType = "image/" + tempString[upperBound];
                    break;
                default:
                    contentType = "";
                    break;
            };

            Response.Buffer = true;
            Response.Charset = "";
            Response.ClearHeaders();
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
            Response.BinaryWrite(fileBytes);
            Response.Flush();
            Response.Clear();
            Response.End();
        }

        /// <summary>
        /// Downloads Product Image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProductImage_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton imgButton = (ImageButton)sender;
                Product product = _productManager.GetProductImage(Convert.ToInt32(imgButton.CommandArgument));
                if (!string.IsNullOrEmpty(product.ImageName))
                    ProcessFileDownload("image", product.ProductImage, product.ImageName);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at GridViewMyProducts RowDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridViewMyProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow gr = e.Row;
                    GridView grdProductDetails = new GridView();
                    GridView grdProductPropType = new GridView();
                    grdProductDetails = (GridView)(gr.FindControl("productdetailGridView"));
                    //grdProductPropType = (GridView)(gr.FindControl("gridPropertyType"));
                    Image brandImage = (Image)gr.FindControl("brandImage");
                    brandImage.ImageUrl = ((ProductAssignedSpecifications)gr.DataItem).BrandImageUrl;

                    grdProductDetails.DataSource = ((ProductAssignedSpecifications)gr.DataItem).Product;
                    grdProductDetails.DataBind();
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
            }
        }

        /// <summary>
        /// Invoked at editLinkButton Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void editLinkButton_Click(object sender, EventArgs args)
        {
            LinkButton lb = (LinkButton)sender;
            SearchCriteria searchCriteria = BuildSearchCriteria();
            searchCriteria.PartnerId = Convert.ToString(GetSession<User>(SessionConstants.User).Company, CultureInfo.InvariantCulture);

            try
            {
                if (args != null)
                {
                    SetSession<SearchCriteria>(SessionConstants.ProductSearch, searchCriteria);
                    SetSession<string>(SessionConstants.ProductId, lb.CommandArgument.ToString(CultureInfo.InvariantCulture));
                    SetSession<string>(UIConstants.Mode, UIConstants.Edit);
                    Response.Redirect("~/Account/AddEditProduct.aspx", false);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at the page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = GetSession<User>(SessionConstants.User);
            try
            {
                resultMessageDiv.Attributes.Remove("class");
                resultMessageDiv.InnerText = "";

                if (!Page.IsPostBack)
                {
                    ClearSession(SessionConstants.ProductId);
                    ClearSession(UIConstants.Mode);

                    rightPanelHeader.Text = ResourceUtility.GetLocalizedString(4, culture, "MY ACCOUNT").ToUpper();

                    IList<SearchFilters> searchFilterData = _partnerManager.GetMyProductSearchFilterData(Convert.ToInt32(user.Company, CultureInfo.InvariantCulture));
                    SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilterData);

                    SetSubmitButtonVisibility(Convert.ToInt32(user.Company, CultureInfo.InvariantCulture));

                    PopulateDropDownData();

                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("AddEditProduct.aspx")))
                    {
                        if (GetSession<SearchCriteria>(SessionConstants.ProductSearch) != null)
                        {
                            SearchCriteria searchCriteria = GetSession<SearchCriteria>(SessionConstants.ProductSearch);
                            IList<ProductCategory> productCategory = _productManager.GetApprovedPartnerProducts(searchCriteria);
                            if (productCategory.Count > 0)
                            {
                                repeaterCategoryList.DataSource = productCategory;
                                repeaterCategoryList.DataBind();
                            }

                            selectedBrands.Text = searchCriteria.BrandId;
                            brandCollection.Value = searchCriteria.BrandCollection;
                          
                            regionDropDown.SelectedValue = searchCriteria.RegionId;
                           
                            ClearSession(SessionConstants.ProductSearch);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeScript1", "InitializeScript();", true);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(productResultUpdatePanel, productResultUpdatePanel.GetType(), "InitializeScript", "InitializeScript();", true);
                    ScriptManager.RegisterClientScriptBlock(myproductsfilterUpdatePanel, this.GetType(), "getItems", "getItems();", true);
                }

                this.Title = ResourceUtility.GetLocalizedString(446, culture, "My Products");
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Sets SubmitButton Visibility
        /// </summary>
        /// <param name="partnerId"></param>
        private void SetSubmitButtonVisibility(int partnerId)
        {
            sumbitNewProductHyperLink.Visible = Convert.ToBoolean((_partnerManager.GetPartnerAccessibilityOnSubmitProduct(partnerId) > 0));
        }

      
        /// <summary>
        /// Invoked at RegionDropDown SelectedIndex Changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                countryDropDown.SelectedIndex = -1;
                string[] regionValues = { regionDropDown.SelectedValue };
                BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1301, culture, "All Countries"), 0, true, regionValues);
              }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at repeaterCategoryList Item DataBound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repeaterCategoryList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RepeaterItem item = e.Item;
                if (((item.ItemType == ListItemType.Item) ||
                    (item.ItemType == ListItemType.AlternatingItem)) && item.ItemType != ListItemType.Header)
                {
                    GridView grd = new GridView();
                    grd = (GridView)(item.FindControl("gridviewMyProducts"));
                    grd.DataSource = ((ProductCategory)item.DataItem).ProductAssignedSpecifications;
                    grd.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(255, culture, "BRAND");
                    //grd.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(566, culture, "Property Type");
                    grd.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(593, culture, "REGION");
                    grd.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(0, culture, "COUNTRIES");
                    grd.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(542, culture, "PRODUCTS");
                    grd.DataBind();
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
            }
        }

        /// <summary>
        /// Invoked at click of GoLinkButton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(selectedBrands.Text))
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", "Please select brand");
                }
                else
                {
                    SearchCriteria searchCriteria = BuildSearchCriteria();
                    searchCriteria.PartnerId = GetSession<User>(SessionConstants.User).Company;

                    IList<ProductCategory> productCategory = _productManager.GetApprovedPartnerProducts(searchCriteria);
                    if (productCategory.Count > 0)
                    {
                        repeaterCategoryList.DataSource = productCategory;
                        repeaterCategoryList.DataBind();
                    }
                    else
                    {
                        repeaterCategoryList.DataSource = null;
                        repeaterCategoryList.DataBind();
                        ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoRecordsFound, culture));
                    }
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.NoRecordsFound, culture));
            }
        }

        /// <summary>
        /// Populates Drop Down Data
        /// </summary>
        private void PopulateDropDownData()
        {
            IList<SearchFilters> searchFilters = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
            BindDropDown(searchFilters, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(202, culture), null);
            BindDropDown(searchFilters, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1301, culture, "All Countries"), null);
        }

        /// <summary>
        /// Builds Search Criteria
        /// </summary>
        /// <returns></returns>
        private SearchCriteria BuildSearchCriteria()
        {
            SearchCriteria searchCriteria = new SearchCriteria()
            {
                BrandId = selectedBrands.Text,
                BrandCollection = brandCollection.Value,
                RegionId = (regionDropDown.SelectedIndex == 0) ? string.Empty : regionDropDown.SelectedValue,
                CountryId = (countryDropDown.SelectedIndex == 0) ? string.Empty : countryDropDown.SelectedValue,
               
            };
            return searchCriteria;
        }
    }
}