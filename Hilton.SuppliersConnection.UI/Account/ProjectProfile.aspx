﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True" EnableEventValidation="false"
    CodeBehind="ProjectProfile.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.Account.ProjectProfile" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="myAccountMainPageHyperLink" runat="server" CBID="96" NavigateUrl="~/Account/ProjectList.aspx"
            Text="My Account"></asp:HyperLink>
        <span>/</span>
        <asp:HyperLink ID="myProjectHyperLink" runat="server" Text="My Projects" CBID="1037"
            NavigateUrl="~/Account/ProjectList.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="projectProfileTitleLabel" Text="Project Profile" CBID="97" runat="server"></asp:Label>
    </div>
    <div class="main-content">
        <h2>
            <asp:Label ID="pageIntroLabel" Text="Choose partners to outfit your property." CBID="63"
                runat="server"></asp:Label>
        </h2>
        <table border="0" cellpadding="0" cellspacing="0" class="table1">
            <tr>
                <td width="136">
                    <asp:Label runat="server" ID="dateCreatedLabel" CBID="354" Text="DATE CREATED:"></asp:Label>
                </td>
                <td width="70">
                    <asp:Label runat="server" ID="createdDateLabel"></asp:Label>
                </td>
                <td width="46">
                </td>
                <td width="89">
                    <asp:Label ID="propertyLabel" CBID="560" Text="PROPERTY:" runat="server"></asp:Label>
                </td>
                <td width="239" rowspan="2">
                    <asp:Label Font-Bold="true" runat="server" ID="brandNameLabel"></asp:Label><br />
                    <asp:Label runat="server" ID="propertyNameLabel"></asp:Label><br />
                    <asp:Label runat="server" ID="addressLabel"></asp:Label>
                    <div id="address2Div" runat="server" visible="false">
                        <asp:Label runat="server" ID="address2Label"></asp:Label><br />
                    </div>
                    <asp:Label runat="server" ID="cityLabel"></asp:Label>,
                    <asp:Label runat="server" ID="stateLabel"></asp:Label>
                    <asp:Label runat="server" ID="zipCodeLabel"></asp:Label><br />
                    <asp:Label runat="server" ID="countryLabel"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="projTypeLabel" CBID="552" Text="PROJECT TYPE:" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="projectTypeLabel"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div class="btn3">
            <asp:LinkButton ID="DownloadSelPartnerLinkButton" CssClass="btn1 flR" runat="server"
                OnClick="DownloadSelectedPartnerLinkButton_Click" Text="Download selected partners"
                CBID="375"></asp:LinkButton>
            <asp:LinkButton ID="DownloadAllPartnerLinkButton" CssClass="btn1 flR" runat="server"
                OnClick="DownloadAllPartnerLinkButton_Click" Text="download all partner options"
                CBID="373"></asp:LinkButton>
        </div>
    </div>
    <div class="main">
        <div class="property-list-detail">
            <asp:Repeater ID="categoryDivisionsRepeater" OnItemCommand="CategoryRepeater_RowCommand"
                runat="server" OnItemDataBound="CategoryDivisionsRepeater_ItemDataBound">
                <ItemTemplate>
                    <div id="categoryListDivision" class="accordion" runat="server">
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="box1">
            <div class="per">
                <asp:Chart ID="chart2" runat="server" Height="60px" Width="55px">
                    <Titles>
                        <asp:Title ShadowOffset="3" Name="Title1" />
                    </Titles>
                    <Legends>
                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                            LegendStyle="Row" />
                    </Legends>
                    <Series>
                        <asp:Series Name="Default" />
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="chartArea1" BorderWidth="0" />
                    </ChartAreas>
                </asp:Chart>
                <div class="per-detail">
                    <h4>
                        <asp:Label ID="percentLabel" runat="server"></asp:Label>
                        <span>%</span></h4>
                    <p>
                        <asp:Label ID="partnerSelLabel" CBID="500" Text="PARTNERS SELECTED" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
            <div class="detail">
                <h3>
                    <asp:Label ID="configIncompleteLabel" runat="server"></asp:Label></h3>
                <p>
                    <asp:Label ID="configIncompleteMsgLabel" runat="server" />
                </p>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div id="rightsidebar-wrapper">
            <div class="sidebar-box">
                <h3>
                    <asp:Label ID="myProjectLabel" CBID="1037" CssClass="upper" Text="MY PROJECTS" runat="server"></asp:Label></h3>
                <div class="middle">
                    <ul>
                        <li>
                            <asp:HyperLink ID="myProjectsRightPanelHyperLink" runat="server" class="active" CBID="1037"
                                Text="My Projects" NavigateUrl="~/Account/ProjectList.aspx"></asp:HyperLink>
                            <ul>
                                <li>
                                    <asp:HyperLink ID="projectProfileHyperLink" runat="server" CBID="97" Text="Project Profile"
                                        class="active"></asp:HyperLink>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box">
                <div class="title">
                    <h3>
                        <asp:Label ID="partnerSelectionLabel" CBID="496" Text="PARTNER SELECTION" runat="server"></asp:Label>
                    </h3>
                </div>
                <div class="content" style="background: #fff;">
                    <div class="per">
                        <asp:Chart ID="chart1" runat="server" Height="80px" Width="65px">
                            <Titles>
                                <asp:Title ShadowOffset="2" Name="Title1" />
                            </Titles>
                            <Legends>
                                <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                    LegendStyle="Row" />
                            </Legends>
                            <Series>
                                <asp:Series Name="Default" />
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="chartArea1" BorderWidth="0" />
                            </ChartAreas>
                        </asp:Chart>
                        <div class="per-detail">
                            <h4>
                                <asp:Label ID="sidebarPercentLabel" runat="server"></asp:Label>
                                <span>%</span></h4>
                            <p>
                                <asp:Label ID="partnerSelectedLabel" Text="PARTNERS SELECTED" CBID="500" runat="server"></asp:Label></p>
                        </div>
                    </div>
                    <div class="detail">
                        <h4>
                            <asp:Label runat="server" ID="sidebarBrandNameLabel"></asp:Label></h4>
                        <div>
                            <asp:Label runat="server" ID="sidebarfacilityNameLabel"></asp:Label><br />
                            <asp:Label runat="server" ID="sidebarAddressLabel"></asp:Label><br />
                            <div id="sidebarAddress2Div" runat="server" visible="false">
                                <asp:Label runat="server" ID="sidebarAddress2Label"></asp:Label><br />
                            </div>
                            <asp:Label runat="server" ID="sidebarCityLabel"></asp:Label>,
                            <asp:Label runat="server" ID="sidebarStateLabel"></asp:Label>
                            <asp:Label runat="server" ID="sidebarZipCodeLabel"></asp:Label>
                            <br />
                            <asp:Label runat="server" ID="sidebarCountryNameLabel"></asp:Label>
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
    <script type="text/javascript">
        $('.panel div div').hide();
        $('.prod-div div, .tb-col3, .tb-col3 div').show();
        $('.accordion h3').click(function () {
            $(this).toggleClass('active');
            $(this).siblings('.panel').slideToggle();
        });

        $('.panel h4').click(function () {
            $(this).toggleClass('active');
            $(this).siblings().slideToggle();

        });
    </script>
</asp:Content>