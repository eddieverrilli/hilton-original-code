﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI
{
    public partial class NonApprovedProductDialog : PageBase
    {
        private IOwnerManager _ownerManager;

        /// <summary>
        /// Invoked on page initialization
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _ownerManager = OwnerManager.Instance;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                categoryLabel.Text = "for " + Convert.ToString(Request.QueryString["categoryHeirarchy"], CultureInfo.InvariantCulture);
                
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Send_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    int projectId = Convert.ToInt32(Request.QueryString["projectId"], CultureInfo.InvariantCulture);
                    int categoryId = Convert.ToInt32(Request.QueryString["categoryId"], CultureInfo.InvariantCulture);
                    PartnerProdOptionSearch searchCriteria = new PartnerProdOptionSearch();
                    searchCriteria.CategoryId = categoryId;
                    searchCriteria.ProjectId = projectId;
                    SetViewState<PartnerProdOptionSearch>(ViewStateConstants.PartnerProdOptionViewState, searchCriteria);

                    string productName = productNameTextBox.Text.Trim();
                    string manufacturer = manufacturerTextBox.Text.Trim();
                    string modelNumber = modelNoTextBox.Text.Trim();
                    string reason = reasonTextBox.Text.Trim();
                    string specSheetName = string.Empty;
                    byte[] specSheet = null;
                    if (specSheetPdfUpload.HasFile)
                    {
                        specSheetName = Convert.ToString(specSheetPdfUpload.FileName, CultureInfo.InvariantCulture);
                        specSheet = specSheetPdfUpload.FileBytes;
                    }

                    _ownerManager.SaveNonApprovedProducts(projectId, productName, manufacturer, modelNumber, reason, categoryId, specSheetName, specSheet, GetSession<User>(SessionConstants.User).UserId);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidatePdfFileSize(object source, ServerValidateEventArgs args)
        {
            int fileMaxSize = int.Parse(ConfigurationStore.GetAppSetting(AppSettingConstants.PdfFileMaxSize), CultureInfo.InvariantCulture);
            if (specSheetPdfUpload.HasFile)
            {
                if (specSheetPdfUpload.PostedFile.ContentLength <= fileMaxSize * 1024 * 1024)
                    args.IsValid = true;
                else
                {
                    args.IsValid = false;
                    pdfUploadvalidator.ErrorMessage = ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidImageSize, culture) + "  " + fileMaxSize.ToString(CultureInfo.InvariantCulture) + "MB";
                }
            }
            else
                args.IsValid = true;
        }

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void SpecSheetPdfLinkButton_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //Product product = _productManager.GetProductDetails(73);
        //        //ProcessFileDownload("pdf", product.SpecSheetPdf, product.SpecSheetPdfName);
        //    }
        //    catch (Exception exception)
        //    {
        //        UserInterfaceExceptionHandler.HandleExcetion(ref exception);
        //        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        //    }
        //}

        //  COMMENTED BY CODEIT.RIGHT
        //        /// <summary>
        //        ///
        //        /// </summary>
        //        /// <param name="fileType"></param>
        //        /// <param name="fileBytes"></param>
        //        /// <param name="fileName"></param>
        //        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        //        {
        //            string contentType = string.Empty;
        //            switch (fileType)
        //            {
        //                case "pdf":
        //                    contentType = "application/pdf";
        //                    break;
        //
        //                case "image":
        //                    contentType = "image/jpeg";
        //                    break;
        //                default:
        //                    contentType = "";
        //                    break;
        //            };
        //
        //            Response.Buffer = true;
        //            Response.Charset = "";
        //            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        //            Response.ContentType = contentType;
        //            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
        //            Response.BinaryWrite(fileBytes);
        //            Response.Flush();
        //            Response.End();
        //        }
    }
}