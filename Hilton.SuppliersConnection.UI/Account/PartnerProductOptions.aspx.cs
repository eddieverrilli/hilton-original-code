﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class PartnerProductOptions : PageBase
    {
        private IOwnerManager _ownerManager;

        private int projectId;

        /// <summary>
        ///triggers when Page is initiated
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs args)
        {
            try
            {
                base.OnInit(args);

                ValidateUserAccess((int)MenuEnum.MyProjects);

                _ownerManager = OwnerManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(98, culture, "Partner / Product Options");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///triggers when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Master.Page.ClientScript.IsStartupScriptRegistered("alert"))
                {
                    Master.Page.ClientScript.RegisterStartupScript
                        (this.GetType(), "alert", "more_lessItems();", true);
                }

                IList<Tuple<int, int, bool>> partnerProductLeafCatCollection = GetSession<IList<Tuple<int, int, bool>>>(SessionConstants.PartnerProfileLeafCategory);
                PartnerProdOptionSearch partnerProdSession = GetSession<PartnerProdOptionSearch>(SessionConstants.PartnerProdOptionSearch);
                projectId = partnerProdSession.ProjectId;
                int categoryId = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item1;
                divPaging.Visible = true;
                if (!Page.IsPostBack)
                {
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                 
                    if (categoryId > 0)
                        BindGrid(categoryId);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(partnerProdDetailsUpdatePanel, this.GetType(), "moreLess", "more_lessItems()", true);
                    ScriptManager.RegisterClientScriptBlock(partnerProdDetailsUpdatePanel, this.GetType(), "ForceCall", "ForceCall();", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);
                string percent = _ownerManager.GetSCConfigurationPercent(projectId);
                if (string.IsNullOrWhiteSpace(percent))
                    percent = "0";

                percentLabel.Text = percent;
                percentPartnerSelLabel.Text = percent;

                double[] yValues = new double[2];
                yValues[0] = Convert.ToDouble(percent, CultureInfo.InvariantCulture);
                yValues[1] = 100 - Convert.ToDouble(percent, CultureInfo.InvariantCulture);
                PlotPieChart(configurationChart, yValues);
                PlotPieChart(configurationChart1, yValues);
                bottonConfigurationUpdatePanel.Update();
                rightPanelUpdatePanel.Update();

                if (percent == "100")
                {
                    configIncompleteLabel.Text = ResourceUtility.GetLocalizedString(1086, culture, "CONFIGURATION COMPLETE");
                    configIncompleteMsgLabel.Text = ResourceUtility.GetLocalizedString(1087, culture, "Your property configuration is complete.");
                }
                else
                {
                    configIncompleteLabel.Text = ResourceUtility.GetLocalizedString(1085, culture, "CONFIGURATION INCOMPLETE");
                    configIncompleteMsgLabel.Text = ResourceUtility.GetLocalizedString(1088, culture, "Your property configuration is incomplete. Please complete all sections above to continue. You can save your progress and return later.");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///plots the pie chart as per project configuration
        /// </summary>
        /// <param name="chart"></param>
        /// <param name="yValues"></param>
        /// <returns></returns>
        private void PlotPieChart(Chart chart, double[] yValues)
        {
            string[] xValues = { "Configured", "Not-Configured", };
            chart.Series["Default"].Points.DataBindXY(xValues, yValues);
            chart.Series["Default"].Points[0].Color = Color.SlateGray;
            chart.Series["Default"].Points[1].Color = Color.LightGray;
            chart.Series["Default"].ChartType = SeriesChartType.Pie;
            chart.Series["Default"]["PieLabelStyle"] = "Disabled";
            chart.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            chart.Legends[0].Enabled = false;
        }

        /// <summary>
        ///binds the grid with the data collection
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="projectId"></param>
        private void BindGrid(int categoryId)
        {
            resultMessageDiv.InnerText = string.Empty;
            resultMessageDiv.Attributes.Remove("class");

            PartnerProductOption partnerProdOptionDetails = _ownerManager.GetPartnerProductOptionDetails(categoryId, projectId, Convert.ToInt32(GetSession<User>(SessionConstants.User).UserId));
            IList<Tuple<int, int, bool>> partnerProductLeafCatCollection = GetSession<IList<Tuple<int, int, bool>>>(SessionConstants.PartnerProfileLeafCategory);

            if (partnerProdOptionDetails != null && partnerProdOptionDetails.CategoryStandard != null && partnerProdOptionDetails.CategoryStandard.Count > 0)
            {
                brandStandardsLabel.Visible = true;
                standardsRepeater.DataSource = partnerProdOptionDetails.CategoryStandard;
                standardsRepeater.DataBind();
            }
            else
            {
                brandStandardsLabel.Visible = false;
                standardsRepeater.DataSource = null;
                standardsRepeater.DataBind();
            }

            if (partnerProdOptionDetails != null && partnerProdOptionDetails.Contact != null && !string.IsNullOrWhiteSpace(partnerProdOptionDetails.Contact.FirstName))
            {
                contactLabel.Visible = true;
                contactInfoLabel.Visible = true;
                contactInfoLabel.Text = partnerProdOptionDetails.Contact.FirstName.ToString(CultureInfo.InvariantCulture);
                straightLineImage.Visible = true;
            }
            else
            {
                contactLabel.Visible = false;
                contactInfoLabel.Visible = false;
                contactInfoLabel.Text = string.Empty;
                straightLineImage.Visible = false;
            }

            categoryLabel.Text = partnerProdOptionDetails.CategoryHeirarchy.ToString(CultureInfo.InvariantCulture);

            PartnerProdOptionSearch searchCriteria = new PartnerProdOptionSearch();
            searchCriteria.CategoryId = categoryId;
            searchCriteria.ProjectId = projectId;
            int totalRecordCount = 0;

            SetViewState<PartnerProdOptionSearch>(ViewStateConstants.PartnerProdOptionViewState, searchCriteria);

            IList<PartnerProductOption> partnerProdOption = _ownerManager.GetPartnerProductOption(searchCriteria, ref totalRecordCount);

       
            if (totalRecordCount > 0)
            {
                partnerProdOption.ToList().ForEach(x => x.CategoryHeirarchy = partnerProdOptionDetails.CategoryHeirarchy.ToString(CultureInfo.InvariantCulture));

                grdPartnerProduct.DataSource = partnerProdOption;
                grdPartnerProduct.DataBind();

                divPaging.Visible = true;
                startPage.Text = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item2.ToString();
                endPage.Text = partnerProductLeafCatCollection.Count.ToString();

                if (partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item2.Equals(1))
                    prevLinkButton.Visible = false;
                else
                    prevLinkButton.Visible = true;

                if (partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item2.Equals(partnerProductLeafCatCollection.Count))
                    nextLinkButton.Visible = false;
                else
                    nextLinkButton.Visible = true;
              
            }
            else
            {
                resultMessageDiv.Attributes.Add("class", "message notification");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoApprovedPartnerProduct, culture);

                grdPartnerProduct.DataSource = null;
                grdPartnerProduct.DataBind();
                startPage.Text = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item2.ToString();
                endPage.Text = partnerProductLeafCatCollection.Count.ToString();

                if (partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item2.Equals(1))
                    prevLinkButton.Visible = false;
                else
                    prevLinkButton.Visible = true;

                if (partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item2.Equals(partnerProductLeafCatCollection.Count))
                    nextLinkButton.Visible = false;
                else
                    nextLinkButton.Visible = true;
            }
            partnerProdDetailsUpdatePanel.Update();

            nonApprovedProdHyperlink.NavigateUrl = "~/Account/NonApprovedProductDialog.aspx?keepThis=true&categoryId=" + categoryId.ToString(CultureInfo.InvariantCulture) + "&projectId=" + projectId.ToString(CultureInfo.InvariantCulture) + "&categoryHeirarchy=" + partnerProdOptionDetails.CategoryHeirarchy.ToString(CultureInfo.InvariantCulture) + "&TB_iframe=true&height=540&width=800";
            nonApprovedProdHyperlink.Attributes.Add("class", "thickbox");
            sidebarBrandNameLabel.Text = partnerProdOptionDetails.BrandName;
            sidebarfacilityNameLabel.Text = partnerProdOptionDetails.FacilityName;
            sidebarAddressLabel.Text = partnerProdOptionDetails.Address;
            if (!string.IsNullOrWhiteSpace(partnerProdOptionDetails.Address2))
            {
                address2Div.Visible = true;
                sidebarAddress2Label.Text = partnerProdOptionDetails.Address2;
            }
            sidebarCityLabel.Text = partnerProdOptionDetails.City;
            sidebarStateLabel.Text = partnerProdOptionDetails.State;
            sidebarZipCodeLabel.Text = partnerProdOptionDetails.ZipCode.ToString(CultureInfo.InvariantCulture);
            sidebarCountryNameLabel.Text = partnerProdOptionDetails.Country;
        }

        /// <summary>
        ///triggered when each row is bound.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdPartnerProduct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.WebControls.Image imgProduct = e.Row.FindControl("ImgProduct") as System.Web.UI.WebControls.Image;
                    if (((PartnerProductOption)(e.Row.DataItem)).ProductImage != null && ((PartnerProductOption)(e.Row.DataItem)).ProductImage.Length > 0)
                    {
                        imgProduct.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(((PartnerProductOption)(e.Row.DataItem)).ProductImage);
                    }
                    else
                    {
                        imgProduct.Visible = false;
                    }
        
                    if (((PartnerProductOption)(e.Row.DataItem)).IsOther == false)
                    {
                        HyperLink imageFeedbackPopupHyperlink = e.Row.FindControl("imageFeedbackPopupHyperlink") as HyperLink;
                        if (((PartnerProductOption)(e.Row.DataItem)).ProductName != null)
                        {
                            imageFeedbackPopupHyperlink.NavigateUrl = "~/Partners/SubmitFeedback.aspx?keepThis=true&partnerName=" + ((PartnerProductOption)(e.Row.DataItem)).PartnerName.ToString(CultureInfo.InvariantCulture) + "&productName=" + ((PartnerProductOption)(e.Row.DataItem)).ProductName.ToString(CultureInfo.InvariantCulture) + "&skuNumber=" + ((PartnerProductOption)(e.Row.DataItem)).ProductNumber.ToString(CultureInfo.InvariantCulture) + "&contactEmail=" + ((PartnerProductOption)(e.Row.DataItem)).ProductContactEmailAddress.ToString(CultureInfo.InvariantCulture) + "&templateId=6" + "&partnerId=" + ((PartnerProductOption)(e.Row.DataItem)).PartnerId + "&productId=" + ((PartnerProductOption)(e.Row.DataItem)).ProductId + "&senderId=1&toFirstName=" + ((PartnerProductOption)(e.Row.DataItem)).ToFirstName + "&toLastName=" + ((PartnerProductOption)(e.Row.DataItem)).ToLastName + "&categoryHeirarchy=" + ((PartnerProductOption)(e.Row.DataItem)).CategoryHeirarchy + "&TB_iframe=true&height=440&width=800";
                        }
                        else
                        {
                            imageFeedbackPopupHyperlink.NavigateUrl = "~/Partners/SubmitFeedback.aspx?keepThis=true&partnerName=" + ((PartnerProductOption)(e.Row.DataItem)).PartnerName.ToString(CultureInfo.InvariantCulture) + "&contactEmail=" + ((PartnerProductOption)(e.Row.DataItem)).ProductContactEmailAddress.ToString(CultureInfo.InvariantCulture) + "&templateId=6" + "&partnerId=" + ((PartnerProductOption)(e.Row.DataItem)).PartnerId + "&productId=" + ((PartnerProductOption)(e.Row.DataItem)).ProductId + "&senderId=1&toFirstName=" + ((PartnerProductOption)(e.Row.DataItem)).ToFirstName + "&toLastName=" + ((PartnerProductOption)(e.Row.DataItem)).ToLastName + "&categoryHeirarchy=" + ((PartnerProductOption)(e.Row.DataItem)).CategoryHeirarchy + "&TB_iframe=true&height=440&width=800";
                        }
                        imageFeedbackPopupHyperlink.Attributes.Add("class", "thickbox");
                        imageFeedbackPopupHyperlink.Visible = true;

                        if (((PartnerProductOption)(e.Row.DataItem)).SpecSheetPdfName != null && !String.IsNullOrEmpty(((PartnerProductOption)(e.Row.DataItem)).SpecSheetPdfName))
                        {
                            System.Web.UI.WebControls.Image imgpdfIcon = e.Row.FindControl("ImgpdfIcon") as System.Web.UI.WebControls.Image;
                            imgpdfIcon.Visible = true;

                            LinkButton downLoadSpecSheetLinkButton = e.Row.FindControl("downLoadSpecSheetLinkButton") as LinkButton;
                            downLoadSpecSheetLinkButton.Visible = true;
                            ScriptManager.GetCurrent(this).RegisterPostBackControl(downLoadSpecSheetLinkButton);
                        }
                    }
                    Button btnSelection = e.Row.FindControl("btnSelection") as Button;
                    Label checkedStatusLabel = e.Row.FindControl("checkedStatusLabel") as Label;
                    System.Web.UI.WebControls.Image selectedCheckBoxImage = e.Row.FindControl("checkedImage") as System.Web.UI.WebControls.Image;

                    if (((PartnerProductOption)(e.Row.DataItem)).IsSelected == true && ((PartnerProductOption)(e.Row.DataItem)).IsOther == false)
                    {
                        btnSelection.Text = UIConstants.RemoveSelection;
                        selectedCheckBoxImage.ImageUrl = "../Images/selected-image.png";
                        checkedStatusLabel.Text = ResourceUtility.GetLocalizedString(1024, culture, "Selected");
                    }

                    else if (((PartnerProductOption)(e.Row.DataItem)).IsSelected == false && ((PartnerProductOption)(e.Row.DataItem)).IsOther == false)
                    {
                        btnSelection.Text = UIConstants.SelectPartnerSelection;
                        selectedCheckBoxImage.ImageUrl = "../Images/non-selected-img.png";
                        checkedStatusLabel.Text = string.Empty;
                    }
                    else if (((PartnerProductOption)(e.Row.DataItem)).IsOther == true)
                    {
                        btnSelection.Text = UIConstants.RemoveSelection;
                        selectedCheckBoxImage.ImageUrl = "../Images/selected-image.png";
                        checkedStatusLabel.Text = ResourceUtility.GetLocalizedString(1024, culture, "Selected");
                        if (((PartnerProductOption)(e.Row.DataItem)).SpecSheetPdfName != null && !String.IsNullOrEmpty(((PartnerProductOption)(e.Row.DataItem)).SpecSheetPdfName))
                        {
                            System.Web.UI.WebControls.Image imgpdfIcon = e.Row.FindControl("ImgpdfIcon") as System.Web.UI.WebControls.Image;
                            imgpdfIcon.Visible = true;

                            LinkButton lnkDownLoadSpecSheet = e.Row.FindControl("downLoadSpecSheetLinkButton") as LinkButton;
                            lnkDownLoadSpecSheet.Visible = true;
                            ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkDownLoadSpecSheet);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdPartnerProduct_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                int productId = Convert.ToInt32(grdPartnerProduct.DataKeys[row.RowIndex][0], CultureInfo.InvariantCulture);
                int partnerId = Convert.ToInt32(grdPartnerProduct.DataKeys[row.RowIndex][1], CultureInfo.InvariantCulture);
                bool isSelected = Convert.ToBoolean(grdPartnerProduct.DataKeys[row.RowIndex][2], CultureInfo.InvariantCulture);
                bool isOther = Convert.ToBoolean(grdPartnerProduct.DataKeys[row.RowIndex][3], CultureInfo.InvariantCulture);
                int catId = Convert.ToInt32(grdPartnerProduct.DataKeys[row.RowIndex][4], CultureInfo.InvariantCulture);

                if (e.CommandName.Equals("Select", StringComparison.InvariantCultureIgnoreCase))
                {
                    _ownerManager.UpdateProductSelection(productId, isSelected, isOther, partnerId, projectId, catId, GetSession<User>(SessionConstants.User).UserId);

                    divPaging.Visible = true;
                    PartnerProdOptionSearch searchCriteria = GetViewState<PartnerProdOptionSearch>(ViewStateConstants.PartnerProdOptionViewState);

                    int totalRecordCount = 0;
                    IList<PartnerProductOption> partnerProdOption = _ownerManager.GetPartnerProductOption(searchCriteria, ref totalRecordCount);

                    if (partnerProdOption.Count == 0)
                    {
                        resultMessageDiv.Attributes.Add("class", "message notification");
                        resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoApprovedPartnerProduct, culture);
                    }

                    grdPartnerProduct.DataSource = partnerProdOption;
                    grdPartnerProduct.DataBind();
                }
                else if (e.CommandName.Equals("DownloadPDF", StringComparison.InvariantCultureIgnoreCase))
                {
                    PartnerProductOption productPDF = _ownerManager.GetProductPdf(productId, isOther);
                    string fileName = productPDF.SpecSheetPdfName;
                    byte[] fileBytes = productPDF.SpecSheetPdf;
                    string contentType = "application/pdf";

                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ClearHeaders();
                    Response.ContentType = contentType;
                    Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                    Response.BinaryWrite(fileBytes);
                    Response.Flush();
                    Response.Clear();
                    Response.End();
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Triggered when previous button is linked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void prevLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                divPaging.Visible = true;

                IList<Tuple<int, int, bool>> partnerProductLeafCatCollection = GetSession<IList<Tuple<int, int, bool>>>(SessionConstants.PartnerProfileLeafCategory);

                Tuple<int, int, bool> tempTuple = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true));
                int prevCategoryId = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item2.Equals(tempTuple.Item2 - 1)).Item1;
                Tuple<int, int, bool> tempTuple1 = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item1.Equals(prevCategoryId));

                partnerProductLeafCatCollection.Add(new Tuple<int, int, bool>(tempTuple.Item1, tempTuple.Item2, false));
                partnerProductLeafCatCollection.Add(new Tuple<int, int, bool>(tempTuple1.Item1, tempTuple1.Item2, true));
                partnerProductLeafCatCollection.Remove(tempTuple);
                partnerProductLeafCatCollection.Remove(tempTuple1);
                tempTuple = null;
                tempTuple1 = null;

                ClearSession(SessionConstants.PartnerProfileLeafCategory);
                SetSession<IList<Tuple<int, int, bool>>>(SessionConstants.PartnerProfileLeafCategory, partnerProductLeafCatCollection);

                BindGrid(prevCategoryId);

                if (partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item2.Equals(1))
                    prevLinkButton.Visible = false;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Triggered when next button is linked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void nextLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                divPaging.Visible = true;
                PartnerProdOptionSearch partnerProdSession = GetSession<PartnerProdOptionSearch>(SessionConstants.PartnerProdOptionSearch);
                IList<Tuple<int, int, bool>> partnerProductLeafCatCollection = GetSession<IList<Tuple<int, int, bool>>>(SessionConstants.PartnerProfileLeafCategory);

                Tuple<int, int, bool> tempTuple = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true));
                int nextCategoryId = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item2.Equals(tempTuple.Item2 + 1)).Item1;
                Tuple<int, int, bool> tempTuple1 = partnerProductLeafCatCollection.SingleOrDefault(a => a.Item1.Equals(nextCategoryId));

                partnerProductLeafCatCollection.Add(new Tuple<int, int, bool>(tempTuple.Item1, tempTuple.Item2, false));
                partnerProductLeafCatCollection.Add(new Tuple<int, int, bool>(tempTuple1.Item1, tempTuple1.Item2, true));
                partnerProductLeafCatCollection.Remove(tempTuple);
                partnerProductLeafCatCollection.Remove(tempTuple1);
                tempTuple = null;
                tempTuple1 = null;

                ClearSession(SessionConstants.PartnerProfileLeafCategory);
                SetSession<IList<Tuple<int, int, bool>>>(SessionConstants.PartnerProfileLeafCategory, partnerProductLeafCatCollection);

                BindGrid(nextCategoryId);

                if (partnerProductLeafCatCollection.SingleOrDefault(a => a.Item3.Equals(true)).Item2.Equals(partnerProductLeafCatCollection.Count))
                    nextLinkButton.Visible = false;

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///Triggered when Back To Property button is linked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void backToPropertyLabel_Click(object sender, EventArgs e)
        {
            try
            {
                Context.Items[UIConstants.ProjectId] = projectId;
                Server.Transfer("~/Account/ProjectProfile.aspx", true);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Triggered when project profile button is linked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void projProfileLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Context.Items[UIConstants.ProjectId] = projectId;
                Server.Transfer("~/Account/ProjectProfile.aspx", true);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}