﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Account
{
    public partial class ProjectProfile : PageBase
    {
        private IOwnerManager _ownerManager;
        private IList<Tuple<int, int, bool>> _partnerProfileLeafCatCollection;
        private int sequenceId = 0;

        private Entities.ProjectDetail projectProfileDetails;

        /// <summary>
        /// Fires OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                ValidateUserAccess((int)MenuEnum.MyProjects);
                //Get the instance of the manager class
                _ownerManager = OwnerManager.Instance;
                _partnerProfileLeafCatCollection = new List<Tuple<int, int, bool>>();
                this.Title = ResourceUtility.GetLocalizedString(97, culture, "Property Profile");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Fires PageLoad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int projectId;
                if (!IsPostBack)
                {
                    sequenceId = 0;
                    projectId = Convert.ToInt32(Context.Items[UIConstants.ProjectId], CultureInfo.InvariantCulture);
                    SetViewState<string>(ViewStateConstants.ProjectId, projectId.ToString(CultureInfo.InvariantCulture));
                }
                else
                {
                    projectId = Convert.ToInt32(GetViewState<string>(ViewStateConstants.ProjectId), CultureInfo.InvariantCulture);
                }

                projectProfileDetails = _ownerManager.GetProjectProfile(projectId, Convert.ToInt32(GetSession<User>(SessionConstants.User).UserId));
                PopulatePropertyInfo(projectProfileDetails);
                categoryDivisionsRepeater.DataSource = projectProfileDetails.ProjectCategoriesDetails.Where(p => p.ParentCategoryId == -1).OrderBy(p => p.SequenceId);
                categoryDivisionsRepeater.DataBind();

                string percent = _ownerManager.GetSCConfigurationPercent(projectProfileDetails.ProjectId);
                if (string.IsNullOrWhiteSpace(percent))
                    percent = "0";

                percentLabel.Text = percent;
                sidebarPercentLabel.Text = percent;

                double[] yValues = new double[2];
                yValues[0] = Convert.ToDouble(percent, CultureInfo.InvariantCulture);
                yValues[1] = 100 - Convert.ToDouble(percent, CultureInfo.InvariantCulture);
                PlotPieChart(chart1, yValues);
                PlotPieChart(chart2, yValues);

                if (percent == "100")
                {
                    configIncompleteLabel.Text = ResourceUtility.GetLocalizedString(1084, culture, "CONFIGURATION COMPLETE");
                    configIncompleteMsgLabel.Text = ResourceUtility.GetLocalizedString(1089, culture, "Your property configuration is complete.");
                }
                else
                {
                    configIncompleteLabel.Text = ResourceUtility.GetLocalizedString(314, culture, "CONFIGURATION INCOMPLETE");
                    configIncompleteMsgLabel.Text = ResourceUtility.GetLocalizedString(1090, culture, "Your property configuration is incomplete. Please complete all sections above to continue. You can save your progress and return later.");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Plot the pie chart
        /// </summary>
        /// <param name="chart"></param>
        /// <param name="yValues"></param>
        /// <returns></returns>
        private void PlotPieChart(Chart chart, double[] yValues)
        {
            string[] xValues = { "Configured", "Not-Configured", };
            chart.Series["Default"].Points.DataBindXY(xValues, yValues);
            chart.Series["Default"].Points[0].Color = Color.SlateGray;
            chart.Series["Default"].Points[1].Color = Color.LightGray;
            chart.Series["Default"].ChartType = SeriesChartType.Pie;
            chart.Series["Default"]["PieLabelStyle"] = "Disabled";
            chart.ChartAreas["chartArea1"].Area3DStyle.Enable3D = false;
            chart.Legends[0].Enabled = false;
        }

        /// <summary>
        /// Populate the property info
        /// </summary>
        /// <param name="projectDetails"></param>
        private void PopulatePropertyInfo(Entities.ProjectDetail projectDetails)
        {
            createdDateLabel.Text = projectDetails.CreatedDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            projectTypeLabel.Text = projectDetails.ProjectTypeName;
            brandNameLabel.Text = projectDetails.BrandName;
            propertyNameLabel.Text = projectDetails.FacilityName;
            addressLabel.Text = projectDetails.Address;
            if (!string.IsNullOrWhiteSpace(projectDetails.Address2))
            {
                sidebarAddress2Div.Visible = true;
                address2Label.Text = projectDetails.Address2;
            }
            cityLabel.Text = projectDetails.City;
            stateLabel.Text = projectDetails.State;
            zipCodeLabel.Text = projectDetails.ZipCode.ToString(CultureInfo.InvariantCulture);
            countryLabel.Text = projectDetails.Country;
            //Side bar address
            sidebarBrandNameLabel.Text = projectDetails.BrandName;
            sidebarfacilityNameLabel.Text = projectDetails.FacilityName;
            sidebarAddressLabel.Text = projectDetails.Address;
            if (!string.IsNullOrWhiteSpace(projectDetails.Address2))
            {
                address2Div.Visible = true;
                sidebarAddress2Label.Text = projectDetails.Address2;
            }
            sidebarCityLabel.Text = projectDetails.City;
            sidebarStateLabel.Text = projectDetails.State;
            sidebarZipCodeLabel.Text = projectDetails.ZipCode.ToString(CultureInfo.InvariantCulture);
            sidebarCountryNameLabel.Text = projectDetails.Country;
        }

        /// <summary>
        /// Create category structure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryDivisionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e != null)
                {
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {
                        HtmlGenericControl categoryDivision = (HtmlGenericControl)e.Item.FindControl("categoryListDivision");
                        var dataItem = (Entities.ProjectConfigDetails)(e.Item.DataItem);
                        if (dataItem.ParentCategoryId == -1)
                        {
                            // Create parent category
                            Panel categoryPanel = new Panel();
                            HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H3");
                            System.Web.UI.WebControls.Image selectedCheckBoxImage = new System.Web.UI.WebControls.Image();
                      
                            //Total leaf child that should have product mapped
                            var leafChilds = projectProfileDetails.ProjectCategoriesDetails.Where(x => x.RootParentId == dataItem.CategoryId && x.IsRequired == true && x.IsLeaf == true).Count();
                            var productMappedLeafChilds = projectProfileDetails.ProjectCategoriesDetails.Where(x => x.RootParentId == dataItem.CategoryId && x.IsRequired == true && x.IsLeaf == true && x.HasProducts == true).Count();
                            if (leafChilds == productMappedLeafChilds)
                            {
                                selectedCheckBoxImage.ImageUrl = "../Images/selected-image.png";
                            }
                            else
                            {
                                selectedCheckBoxImage.ImageUrl = "../Images/non-selected-img.png";
                            }

                            Literal parentCategoryName = new Literal() { Text = dataItem.CategoryName };

                            selectedCheckBoxImage.CssClass = "checkedImage";
                            headingCategoryName.Controls.Add(selectedCheckBoxImage);

                            var childCategoriesCount = projectProfileDetails.ProjectCategoriesDetails.Where(x => x.RootParentId == dataItem.CategoryId).Count();

                            if (childCategoriesCount > 0)
                            {
                                headingCategoryName.Attributes.Add("class", "H3Text");
                            }
                            else
                            {
                                headingCategoryName.Attributes.Add("class", "H3Text nobtn");
                            }

                            headingCategoryName.Controls.Add(parentCategoryName);

                            Label spanOpen = new Label();
                            spanOpen.Attributes.Add("class", "acc-open tg");
                            spanOpen.Text = ResourceUtility.GetLocalizedString(79, culture, "Open");
                            headingCategoryName.Controls.Add(spanOpen);

                            Label spanClose = new Label();
                            spanClose.Attributes.Add("class", "acc-close tg");
                            spanClose.Text = ResourceUtility.GetLocalizedString(1116, culture, "Close");
                            headingCategoryName.Controls.Add(spanClose);

                            categoryPanel.Controls.Add(headingCategoryName);

                            Panel categoriesExceptRoot = new Panel();
                            categoriesExceptRoot.Attributes.Add("class", "panel");
                            categoriesExceptRoot.ID = "panel_" + dataItem.CategoryId;
                            IEnumerable<ProjectConfigDetails> projCatDetails = projectProfileDetails.ProjectCategoriesDetails.Where(p => p.RootParentId == dataItem.CategoryId).OrderBy(p => p.CategoryLevel).ThenBy(p => p.SequenceId);

                            foreach (var subCategory in projCatDetails)
                            {
                                if (subCategory.IsLeaf == false)
                                {
                                    Panel subCategoryPanel = new Panel();
                                    subCategoryPanel.ID = "panel_" + subCategory.CategoryId;
                                    HtmlGenericControl midCategoryControl = null;
                                    if (projCatDetails.Count(p => p.ParentCategoryId == subCategory.CategoryId) > 0)
                                        midCategoryControl = new HtmlGenericControl("H4");
                                    else
                                        midCategoryControl = new HtmlGenericControl("H5");
                                    midCategoryControl.Style.Add("padding-left", (25 + subCategory.CategoryLevel * 10).ToString(CultureInfo.InvariantCulture) + "px");

                                    midCategoryControl.InnerText = subCategory.CategoryName + " ";

                                    Label midCategorySpanOpen = new Label();
                                    midCategorySpanOpen.Attributes.Add("class", "acc-open tg");
                                    midCategorySpanOpen.Text = ResourceUtility.GetLocalizedString(79, culture, "Open");
                                    midCategoryControl.Controls.Add(midCategorySpanOpen);

                                    Label midCategorySpanClose = new Label();
                                    midCategorySpanClose.Attributes.Add("class", "acc-close tg");
                                    midCategorySpanClose.Text = ResourceUtility.GetLocalizedString(1116, culture, "Close");
                                    midCategoryControl.Controls.Add(midCategorySpanClose);

                                    if (!string.IsNullOrWhiteSpace(subCategory.CategoryDescription))
                                    {
                                        System.Web.UI.WebControls.Image infoImage = new System.Web.UI.WebControls.Image() { ImageUrl = "../Images/itool.png", Width = 13, Height = 13, ToolTip = subCategory.CategoryDescription };
                                        midCategoryControl.Controls.Add(infoImage);
                                    }

                                    subCategoryPanel.Controls.Add(midCategoryControl);

                                    if (categoriesExceptRoot.ID.ToString(CultureInfo.InvariantCulture) == "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))
                                    {
                                        categoriesExceptRoot.Controls.Add(subCategoryPanel);
                                    }
                                    else //last;
                                    {
                                        Panel parentPanel = new Panel();
                                        foreach (Control p in categoriesExceptRoot.Controls)
                                        {
                                            if (p is Panel)
                                            {
                                                parentPanel = (Panel)FindControl(categoriesExceptRoot, "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                                if (parentPanel != null)
                                                    parentPanel.Controls.Add(subCategoryPanel);
                                            }
                                        }
                                     }
                                }
                                else if (subCategory.IsLeaf)
                                {
                                    sequenceId++;
                                    _partnerProfileLeafCatCollection.Add(new Tuple<int, int, bool>(subCategory.CategoryId, sequenceId, false));

                                    using (Panel subPanelDivision = new Panel())
                                    {
                                        subPanelDivision.Attributes.Add("class", "tabel2 table2-overlay");
                                        subPanelDivision.ID = "panel_" + subCategory.CategoryId;
                                        HtmlGenericControl leafCategoryHeader = new HtmlGenericControl();

                                        leafCategoryHeader.Attributes.Add("class", "tb-col1");
                                        Literal midSubCategoryLiteral = new Literal() { Text = subCategory.CategoryName + " " };
                                        leafCategoryHeader.Controls.Add(midSubCategoryLiteral);

                                        if (!string.IsNullOrWhiteSpace(subCategory.CategoryDescription))
                                        {
                                            System.Web.UI.WebControls.Image infoImage = new System.Web.UI.WebControls.Image() { ImageUrl = "../Images/itool.png", Width = 13, Height = 13, ToolTip = subCategory.CategoryDescription };
                                            leafCategoryHeader.Controls.Add(infoImage);
                                        }

                                        HtmlGenericControl leafCategoryRequired = new HtmlGenericControl();
                                        leafCategoryRequired.Attributes.Add("class", "tb-col2");
                                        Literal requiredLiteral = new Literal() { Text = subCategory.IsRequired == true ? UIConstants.Required : UIConstants.Optional };
                                        leafCategoryRequired.Controls.Add(requiredLiteral);

                                        HtmlGenericControl chooseLinkButtonControl = null;
                                        HtmlGenericControl editLinkButtonControl = null;
                                        GridView productsGrid = null;
                                        HtmlGenericControl productGridDiv = new HtmlGenericControl("DIV");
                                        if (subCategory.LeafCategoryDetails.PartnerProductDetails.Count > 0)
                                        {
                                            productGridDiv.Attributes.Add("class", "tb-col3");
                                            productsGrid = new GridView();

                                            productsGrid.CssClass = "no-border";
                                            productsGrid.AlternatingRowStyle.CssClass = "alternate";

                                            productsGrid.ShowHeader = false;
                                            productsGrid.AutoGenerateColumns = false;

                                            BoundField productNameBoundField = new BoundField();
                                            productNameBoundField.DataField = "ProductName";
                                            productsGrid.Columns.Add(productNameBoundField);

                                            BoundField skuNumberBoundField = new BoundField();
                                            skuNumberBoundField.DataField = "SKUNumber";
                                            productsGrid.Columns.Add(skuNumberBoundField);

                                            productsGrid.RowDataBound += new GridViewRowEventHandler(ProdcutsGrid_RowDataBound);

                                            productsGrid.DataSource = subCategory.LeafCategoryDetails.PartnerProductDetails;
                                            productsGrid.DataBind();

                                            productGridDiv.Controls.Add(productsGrid);

                                            editLinkButtonControl = new HtmlGenericControl();
                                            editLinkButtonControl.Attributes.Add("class", "tb-col5");
                                            LinkButton editLinkButton = new LinkButton() { Text = ResourceUtility.GetLocalizedString(774, culture, UIConstants.Edit), CssClass = "edit" };
                                            editLinkButton.CommandArgument = subCategory.CategoryId + "," + subCategory.ProjectId;
                                            editLinkButtonControl.Controls.Add(editLinkButton);
                                        }
                                        else
                                        {
                                            chooseLinkButtonControl = new HtmlGenericControl();
                                            chooseLinkButtonControl.Attributes.Add("class", "tb-col3");
                                            LinkButton chooseLinkButton = new LinkButton() { Text = ResourceUtility.GetLocalizedString(292, culture, UIConstants.Choose), CssClass = "link" };
                                            chooseLinkButton.CommandArgument = subCategory.CategoryId + "," + subCategory.ProjectId;
                                            chooseLinkButtonControl.Controls.Add(chooseLinkButton);
                                        }

                                        subPanelDivision.Controls.Add(leafCategoryHeader);
                                        subPanelDivision.Controls.Add(leafCategoryRequired);
                                        if (subCategory.LeafCategoryDetails.PartnerProductDetails.Count > 0)
                                        {
                                            subPanelDivision.Controls.Add(productGridDiv);
                                            subPanelDivision.Controls.Add(editLinkButtonControl);
                                        }
                                        else
                                        {
                                            subPanelDivision.Controls.Add(chooseLinkButtonControl);
                                        }

                                        if (categoriesExceptRoot.ID.ToString(CultureInfo.InvariantCulture) == "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))
                                        {
                                            categoriesExceptRoot.Controls.Add(subPanelDivision);
                                        }
                                        else //last;
                                        {
                                            Panel pnlParent = new Panel();
                                            pnlParent.Attributes.Add("class", "tb-col3");
                                            foreach (Control p in categoriesExceptRoot.Controls)
                                            {
                                                if (p is Panel)
                                                {
                                                    pnlParent = (Panel)FindControl(categoriesExceptRoot, "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                                    if (pnlParent != null)
                                                        pnlParent.Controls.Add(subPanelDivision);
                                                }
                                            }

                                            //Panel pnlParent = new Panel();
                                            //pnlParent.Attributes.Add("class", "tb-col3");
                                            //pnlParent = (Panel)FindControl(categoriesExceptRoot, "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                            //pnlParent.Controls.Add(subPanelDivision);
                                        }
                                    }
                                }
                            }
                            categoryPanel.Controls.Add(categoriesExceptRoot);
                            categoryDivision.Controls.Add(categoryPanel);

                            categoryPanel.Dispose();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Add the prodcuts to grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProdcutsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var dataItem = e.Row.DataItem as ProjectConfigPartnerProduct;
                    if (string.IsNullOrWhiteSpace(dataItem.ProductName))
                    {
                        e.Row.Cells[0].Text = dataItem.PartnerName;
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Dispose the controls recursively
        /// </summary>
        /// <param name="control"></param>
        private void Dispose(Control control)
        {
            for (int count = 0; count < control.Controls.Count; count++)
            {
                if (control.Controls[count].Controls.Count > 0)
                {
                    Dispose(control.Controls[count]);
                }
                else
                {
                    control.Dispose();
                }
            }
        }

        /// <summary>
        /// Fires Category Row Command Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void CategoryRepeater_RowCommand(object sender, CommandEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string[] paramerters = args.CommandArgument.ToString().Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    int categoryId = int.Parse(paramerters[0], CultureInfo.InvariantCulture);
                    int projectId = int.Parse(paramerters[1], CultureInfo.InvariantCulture);

                    PartnerProdOptionSearch searchCriteria = new PartnerProdOptionSearch();
                    searchCriteria.CategoryId = categoryId;
                    searchCriteria.ProjectId = projectId;
                    SetSession<PartnerProdOptionSearch>(SessionConstants.PartnerProdOptionSearch, searchCriteria);

                    Tuple<int, int, bool> tempTuple = _partnerProfileLeafCatCollection.SingleOrDefault(a => a.Item1.Equals(categoryId));
                    _partnerProfileLeafCatCollection.Add(new Tuple<int, int, bool>(tempTuple.Item1, tempTuple.Item2, true));
                    _partnerProfileLeafCatCollection.Remove(tempTuple);
                    tempTuple = null;
                    SetSession<IList<Tuple<int, int, bool>>>(SessionConstants.PartnerProfileLeafCategory, _partnerProfileLeafCatCollection);
                    Response.Redirect("~/Account/PartnerProductOptions.aspx", false);
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Get the level specific categories
        /// </summary>
        /// <param name="projectProfileDeatilsDetailsDataSet"></param>
        private void WriteHeaders()
        {
            Response.Write("Categories");
            Response.Write("\t");
            Response.Write("Item");
            Response.Write("\t");
            Response.Write("Brand");
            Response.Write("\t");
            Response.Write("Product");
            Response.Write("\t");
            Response.Write("Model #");
            Response.Write("\n");
        }

        /// <summary>
        /// Get level specific categories
        /// </summary>
        /// <param name="projectTemplateDetailsDataSet"></param>
        private void GetLevelSpecificCategories(DataSet projectTemplateDetailsDataSet)
        {
            WriteCategoryDetails(projectTemplateDetailsDataSet.Tables[0]);
            WriteHeaders();
            DataRow[] projectTemplateDetailsDataRow = projectTemplateDetailsDataSet.Tables[1].Select("parentCategory=-1").ToArray();
            DataTable levelSpecificCategoryDetails = null;
            if (projectTemplateDetailsDataRow.GetLength(0) > 0)
            {
                levelSpecificCategoryDetails = projectTemplateDetailsDataRow.CopyToDataTable();
            }
            foreach (DataRow categoryDetailsRow in levelSpecificCategoryDetails.Rows)
            {
                Response.Write(categoryDetailsRow["CategoryName"].ToString() + "\t");
                Response.Write("\n");
                if ((bool)categoryDetailsRow["HasProducts"] && (bool)categoryDetailsRow["IsLeaf"])
                {
                    WriteProducts(projectTemplateDetailsDataSet.Tables[2], categoryDetailsRow["CategoryId"].ToString());
                }
                ExportToExcel(projectTemplateDetailsDataSet, categoryDetailsRow["CategoryId"].ToString());
            }
        }

        /// <summary>
        /// Export to excel for a category id
        /// </summary>
        /// <param name="categoryCollection"></param>
        /// <param name="categoryId"></param>
        private void ExportToExcel(DataSet categoryCollection, string categoryId)
        {
            DataTable levelSpecificCategoryTable = null;
            DataRow[] levelSpecificCategoryRowCollection = categoryCollection.Tables[1].Select("parentCategory=" + "'" + categoryId + "'").OrderBy(p => p["SequenceId"]).ToArray(); ;
            if (levelSpecificCategoryRowCollection.GetLength(0) > 0)
            {
                levelSpecificCategoryTable = levelSpecificCategoryRowCollection.CopyToDataTable();
                foreach (DataRow categoryDetailsRow in levelSpecificCategoryTable.Rows)
                {
                    if ((bool)categoryDetailsRow["IsLeaf"])
                    {
                        Response.Write("\t");
                        Response.Write(categoryDetailsRow["CategoryName"].ToString() + "\t");
                    }
                    else
                    {
                        Response.Write(categoryDetailsRow["CategoryName"].ToString() + "\t");
                    }
                    if (categoryDetailsRow["HasProducts"].ToString() == "True")
                        WriteProducts(categoryCollection.Tables[2], categoryDetailsRow["CategoryId"].ToString());
                    else
                        Response.Write("\n");
                    ExportToExcel(categoryCollection, categoryDetailsRow["CategoryId"].ToString());
                }
            }
        }

        /// <summary>
        /// Write the category details in excel
        /// </summary>
        /// <param name="levelSpecificCategoryTable"></param>
        private void WriteCategoryDetails(DataTable levelSpecificCategoryTable)
        {
            foreach (DataRow categoryDetailsRow in levelSpecificCategoryTable.Rows)
            {
                Response.Write(categoryDetailsRow["BrandName"].ToString() + "\n");
                Response.Write(categoryDetailsRow["FacilityUniqueName"].ToString() + "\n");
                Response.Write(categoryDetailsRow["ProjectTypeName"].ToString() + "\n");
                Response.Write(categoryDetailsRow["Address"].ToString() + "\n");
                if (!string.IsNullOrWhiteSpace(categoryDetailsRow["Address2"].ToString()))
                {
                    Response.Write(categoryDetailsRow["Address2"].ToString() + "\n");
                }
                Response.Write(categoryDetailsRow["City"].ToString() + ", ");
                Response.Write(categoryDetailsRow["State"].ToString() + " ");
                Response.Write(categoryDetailsRow["ZipCode"].ToString() + "\n");
                Response.Write(categoryDetailsRow["Country"].ToString() + "\n");

                Response.Write("\n");
                Response.Write("\n");
            }
        }

        /// <summary>
        /// Write the products into the excel
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="catId"></param>
        private void WriteProducts(DataTable dt, string catId)
        {
            DataTable productCollection = null;
            DataRow[] partnerProductDataset = dt.Select("catId=" + "'" + catId + "'");
            if (partnerProductDataset.GetLength(0) > 0)
            {
                productCollection = partnerProductDataset.CopyToDataTable();
                foreach (DataRow dr in productCollection.Rows)
                {
                    Response.Write(dr["PartnerName"].ToString() + "\t");
                    Response.Write(dr["ProductName"].ToString() + "\t");
                    Response.Write(dr["SKUNumber"].ToString() + "\t");
                    Response.Write("\n");
                    if (productCollection.Rows.IndexOf(dr) != productCollection.Rows.Count - 1)
                    {
                        Response.Write("\t");
                        Response.Write("\t");
                    }
                }
            }
            else
            {
                Response.Write("\n");
            }
        }

        /// <summary>
        /// Export the data to excel
        /// </summary>
        /// <param name="projectProfileDetailsDataSet"></param>
        private void ExportToExcel(DataSet projectProfileDetailsDataSet)
        {
            try
            {
                string attach = UIConstants.ProjectProfileExcelFileName;
                Response.ClearContent();
                Response.AddHeader("content-disposition", attach);
                Response.ContentType = UIConstants.ContentType;
                if (projectProfileDetailsDataSet.Tables != null)
                {
                    GetLevelSpecificCategories(projectProfileDetailsDataSet);
                    Response.End();
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Export all partner data to excel sheet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DownloadAllPartnerLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int projectId = Convert.ToInt32(GetViewState<string>(ViewStateConstants.ProjectId), CultureInfo.InvariantCulture);
                DataSet projectTemplateDataSet = _ownerManager.ExportAllPartnerOption(projectId);
                ExportToExcel(projectTemplateDataSet);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Export all partner data to excel sheet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DownloadSelectedPartnerLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int projectId = Convert.ToInt32(GetViewState<string>(ViewStateConstants.ProjectId), CultureInfo.InvariantCulture);
                DataSet projectTemplateDataSet = _ownerManager.ExportSelectedPartners(projectId, Convert.ToInt32(GetSession<User>(SessionConstants.User).UserId));
                ExportToExcel(projectTemplateDataSet);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }
    }
}