﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="MyProducts.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.MyProducts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
    <script src="../Scripts/jquery.downloader.js" type="text/javascript"></script>
   <script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            getItems();
        });
        function InitializeScript() {

            $('.accordion:first h3, .accordion:first .panel1:first h4, .accordion:first .panel1:first .panel2:first h5').addClass('active');
            $('.accordion:first h3').siblings().show(); 
            $('.accordion:first .panel1:first .panel2').show();
            $('.accordion:first .panel1:first .panel3:first').show();
            $('.accordion h3').click(function () {
                $(this).toggleClass('active');
                $(this).siblings('.panel').slideToggle();
            });
            $('.accordion h4').siblings('.panel1').toggle();
            $('.accordion h4').click(function () {

                $(this).toggleClass('active');
                $(this).siblings('.panel1').slideToggle();
            });
            $('.accordion h5').click(function () {
                $(this).toggleClass('active');
                $(this).next().slideToggle();
            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="myAccountHyperLink" CBID="96" Text="My Account" runat="server"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="myProductsLabel" CBID="446" runat="server" Text="My Products"></asp:Label>
        <span></span>
    </div>
    <div class="main-content">
        <div class="flR go">
            <asp:HyperLink ID="sumbitNewProductHyperLink" runat="server" CBID="705" Text="SUBMIT NEW PRODUCT FOR REVIEW"
                NavigateUrl="~/Account/AddEditProduct.aspx"></asp:HyperLink>
        </div>
        <h2>
            <asp:Label ID="appHiltonProdsLabel" CBID="49" runat="server" Text="
            My Approved Hilton Products"></asp:Label>
        </h2>
        <p>
            <asp:Label runat="server" ID="approvedContentLabel" CBID="50" Text=""></asp:Label>
        </p>
    </div>
    <div class="main">
        <div class="my-product">
            <div class="accordion">
                <asp:UpdatePanel ID="myproductsfilterUpdatePanel" runat="server">
                 <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="selectedBrands" EventName="TextChanged" />
                </Triggers>
                    <ContentTemplate>
                        <h3>
                            <asp:Label ID="myProdSectionHeaderLabel" CBID="446" runat="server" Text="My Products"></asp:Label>
                        </h3>
                        <div class="search-result">
                            <div class="label">
                                <asp:Label ID="filterLabel" CBID="772" runat="server" Text="Filter: "></asp:Label>
                            </div>
                            <div>
                                <div class="brands">
                                     <asp:DropDownList runat="server" ID="optgroup"  ViewStateMode="Enabled" ClientIDMode="Static"  multiple="multiple">         
                                         </asp:DropDownList>
                                         <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                                         <asp:HiddenField ID="isCheckAll" runat="server" ClientIDMode="Static" />
                                         <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display:none;" ></asp:TextBox>
                                    <%--<asp:DropDownList ID="propertyTypeDropDown" runat="server" ViewStateMode="Enabled"
                                        AutoPostBack="true" OnSelectedIndexChanged="PropertyTypeDropDown_SelectedIndexChanged">
                                    </asp:DropDownList>--%>
                                </div>
                                <br />
                             
                                <div class="region-with-margin">
                                    <asp:DropDownList ID="regionDropDown" runat="server" ViewStateMode="Enabled" AutoPostBack="true"
                                        OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled" 
                                            AutoPostBack="false" runat="server">
                                        </asp:DropDownList>
                                </div>
                          </div>
                            <div class="go">
                                <asp:LinkButton ID="goLinkButton" runat="server" CBID="400" ViewStateMode="Enabled"
                                    Text="Go" OnClick="GoLinkButton_Click"></asp:LinkButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="panel">
                    <asp:UpdatePanel ID="productResultUpdatePanel" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="goLinkButton" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <div id="resultMessageDiv" runat="server">
                            </div>
                            <asp:Repeater ID="repeaterCategoryList" runat="server" ViewStateMode="Enabled" OnItemDataBound="repeaterCategoryList_ItemDataBound">
                                <ItemTemplate>
                                    <div class="panel">
                                        <h4 class="myproducttoggle">
                                            <asp:Label ID="Label1" class="acc-open tg" Text="Open" CBID="475" runat="server"></asp:Label><asp:Label
                                                ID="Label2" class="acc-close tg" Text="Close" CBID="1119" runat="server"></asp:Label>
                                            <%# DataBinder.Eval(Container.DataItem, "CategoryName")%>
                                        </h4>
                                        <div class="panel1">
                                            <asp:GridView ID="gridviewMyProducts" runat="server" GridLines="None" AutoGenerateColumns="false"
                                                Width="100%" OnRowDataBound="GridViewMyProducts_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="BRAND">
                                                        <ItemTemplate>
                                                            <asp:Image ID="brandImage" CssClass="pic imagestyle" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                   <%-- <asp:TemplateField HeaderText="PROP. TYPE">
                                                        <ItemTemplate>
                                                            <div class="nostyle">
                                                                <asp:GridView ID="gridPropertyType" runat="server" GridLines="None" AutoGenerateColumns="false"
                                                                    ShowHeader="false">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="PROP. TYPE" DataField="PropertyTypeDescription" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField HeaderText="REGION" DataField="RegionDescription" ItemStyle-CssClass="col3"
                                                        HeaderStyle-CssClass="col3" />
                                                       

                                                         <asp:TemplateField HeaderText="COUNTRIES" ItemStyle-CssClass="col3" HeaderStyle-CssClass="col3">
                                                                    <ItemTemplate>
                                                                    <itemstyle cssclass="col3"></itemstyle>
                                                                        <ul>
                                                                    <asp:Repeater ID="repeaterCountries" runat="server" DataSource='<%# Eval("Countries") %>'>
                                                                    <ItemTemplate>
                                                                     <li id="listStandard" runat="server">
                                                                        <%# Eval("CountryName")%><br />
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                 </ItemTemplate>
                                                                 <ItemStyle CssClass="col3"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-CssClass="col4" HeaderStyle-CssClass="col4" HeaderText="PRODUCTS">
                                                        <ItemTemplate>
                                                            <asp:GridView ID="productdetailGridView" runat="server" GridLines="None" OnRowDataBound="GridViewMyProductDetails_RowDataBound"
                                                                AutoGenerateColumns="false" AlternatingRowStyle-CssClass="odd" ShowHeader="false" OnRowCommand="GridViewMyProductDetails_RowCommand">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-CssClass="col4-col1">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="productImage" CssClass="pic imagestyle" runat="server" OnClientClick="javascript: return false;"
                                                                                OnClick="ProductImage_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductId") %>' />
                                                                            <h6>
                                                                                <%# DataBinder.Eval(Container.DataItem, "ProductName")%>
                                                                            </h6>
                                                                            <p>
                                                                                <%# string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "ProductSKU").ToString())? "":DataBinder.Eval(Container.DataItem, "ProductSKU") + "," %>
                                                                                <%# DataBinder.Eval(Container.DataItem, "PartnerName")%></p>
                                                                            <p>
                                                                                <asp:Image ID="ImgpdfIcon" runat="server" ImageUrl='~/Images/product/Images/pdf-icon.gif'
                                                                                    Width="11" Height="11" class="product" Visible="false" />
                                                                               <asp:LinkButton ID="downLoadSpecSheetLinkButton" runat="server" CssClass="DownloadSpecSheet" 
                                                                                    Visible="false">Download Spec Sheet</asp:LinkButton>
                                                                             </p>
                                                                            </td>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-CssClass="col4-col2">
                                                                        <ItemTemplate>
                                                                            <p>
                                                                                <asp:Label ID="approvedLabel" runat="server" CssClass="col4-col2" Text='<%# DataBinder.Eval(Container.DataItem, "StatusDescription") %>'></asp:Label></p>
                                                                            <asp:LinkButton ID="editLinkButton" runat="server" Visible="false" CssClass="edit"
                                                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductId") %>' Text="edit"
                                                                                OnClick="editLinkButton_Click"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="MY ACCOUNT" runat="server"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li id="constructionReportListItem" runat="server">
                        <asp:HyperLink ID="constructionReportHyperLink" CBID="11" Text="Construction Report"
                            NavigateUrl="~/Account/LeadsAndConstructionReport.aspx" runat="server"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="approvedCategoriesHyperLink" runat="server" CBID="232" Text="Approved Categories"
                            NavigateUrl="~/Account/ApprovedCategories.aspx"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myProductsHyperLink" runat="server" class="active" CBID="446"
                            Text="My Products"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="myAccountStatusHyperLink" runat="server" CBID="58" Text="Account Status"
                            NavigateUrl="~/Account/AccountStatus.aspx"></asp:HyperLink></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>