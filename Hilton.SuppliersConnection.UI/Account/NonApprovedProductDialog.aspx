﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NonApprovedProductDialog.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.NonApprovedProductDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%-- <link href="../Styles/main.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Styles/modal.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body
        {
            background: none;
        }
    </style>
    <title></title>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        $(window).ready(function () {
            $('.cancel').click(function () {
                window.parent.closeIframe();
                return false;

            });
        })

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }

        function ValidatePdfExtension(source, args) {
            //debugger;
            var fileUploadPath = document.getElementById('<%= specSheetPdfUpload.ClientID %>').value;

            if (fileUploadPath == '')
            // No file Selected
                args.IsValid = true;
            else {
                var extension = fileUploadPath.substring(fileUploadPath.lastIndexOf('.') + 1);
                if (extension == "pdf") {

                    var filePathLink = document.getElementById('<%= specSheetPdfNameLabel.ClientID%>');

                    if (filePathLink != null) {
                        filePathLink.innerHTML = fileUploadPath.substring(fileUploadPath.lastIndexOf('\\') + 1);
                        filePathLink.disabled = true;

                    }

                    args.IsValid = true; //valid File Type
                }
                else
                    args.IsValid = false;  // Invalid File Type

            }

        }

    </script>
    <div id="non-approved-product" class="modal">
        <a href="#" class="cross simplemodal-overlay"></a>
        <h2>
            <asp:Label ID="nonApprvdProdLabel" runat="server" CBID="369" Text="Non Approved Product">
            </asp:Label></h2>
        <div class="content">
            <h3>
                <asp:Label ID="chooseNonApprvdProdLabel" runat="server" Text="Choose a Non-Approved Product">
                </asp:Label>
            </h3>
            <h4>
                <asp:Label ID="categoryLabel" runat="server"></asp:Label>
            </h4>
            <p>
                <asp:Label ID="descriptionLabel" runat="server" Text="This is dummy text" CBID="1020"></asp:Label></p>
            <ul class="approved-product">
                <li><span class="label">
                    <asp:Label ID="productNameLabel" runat="server" CBID="537" Text="Product Name" CssClass="label">
                    </asp:Label>
                </span><span class="input">
                    <asp:TextBox ID="productNameTextBox" MaxLength="128" runat="server" CssClass="mandatory">
                    </asp:TextBox>
                </span>
                    <br />
                    <br />
                    <asp:RequiredFieldValidator ID="prodNameVaildator" SetFocusOnError="true" VMTI="21"
                        runat="server"  CssClass="errorText"  ControlToValidate="productNameTextBox" ValidationGroup="ValidationGp2"
                        ErrorMessage="Please enter Product Name">
                    </asp:RequiredFieldValidator>
                </li>
                <li><span class="label">
                    <asp:Label ID="manufacturerLabel" runat="server" CBID="432" Text="Manufacturer" CssClass="label">
                    </asp:Label>
                </span><span class="input">
                    <asp:TextBox ID="manufacturerTextBox" runat="server" MaxLength="128" CssClass="mandatory">
                    </asp:TextBox>
                </span>
                    <br />
                    <br />
                    <asp:RequiredFieldValidator ID="manufacturerValidator" SetFocusOnError="true" VMTI="24"
                        runat="server"  CssClass="errorText"  ControlToValidate="manufacturerTextBox" ValidationGroup="ValidationGp2"
                        ErrorMessage="Please enter Manufacturer">
                    </asp:RequiredFieldValidator>
                </li>
                <li><span class="label">
                    <asp:Label ID="modelNumberLabel" runat="server" CBID="443" Text="Model Number" CssClass="label">
                    </asp:Label>
                </span><span class="input">
                    <asp:TextBox ID="modelNoTextBox" MaxLength="128" runat="server">
                    </asp:TextBox>
                </span></li>
                <li><span class="label">
                    <asp:Label ID="reasonLabel" runat="server" CBID="583" Text="Reason" CssClass="label"></asp:Label>
                </span><span class="input">
                    <asp:TextBox ID="reasonTextBox" runat="server" MaxLength="512" TextMode="MultiLine"
                        CssClass="mandatory">
                    </asp:TextBox>
                </span></li>
                <li><span class="label"></span>
                    <asp:RequiredFieldValidator ID="reasonValidator" ValidationGroup="ValidationGp2"
                        SetFocusOnError="true" VMTI="25" runat="server"  CssClass="errorText" ControlToValidate="reasonTextBox"
                        ErrorMessage="Please enter Reason">
                    </asp:RequiredFieldValidator>
                </li>
                <li><span class="label">
                    <asp:Label ID="specSheetPDFLabel" runat="server" Text="Spec Sheet PDF" CBID="676"
                        CssClass="label"></asp:Label>
                </span>
                    <asp:UpdatePanel ID="specSheetUploadUpdatePanel" runat="server" RenderMode="Inline"
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <span class="input">
                                <asp:FileUpload ID="specSheetPdfUpload" runat="server" /><br />
                                <asp:Label ID="pdfSpecificationLabel" runat="server" Text="Spec sheet must be PDF file with a maximum size of 10 MB."
                                    CBID="1126"></asp:Label><br />
                                <asp:CustomValidator ID="pdfUploadvalidator" runat="server" ControlToValidate="specSheetPdfUpload"
                                    Display="Dynamic" ClientValidationFunction="ValidatePdfExtension" CssClass="errorText"
                                    ValidationGroup="ValidationGp2" ValidateEmptyText="true" OnServerValidate="ValidatePdfFileSize"
                                    ErrorMessage="Please Select correct extention or correct size of file"></asp:CustomValidator>
                                <asp:Label ID="specSheetPdfNameLabel" runat="server" ViewStateMode="Enabled"></asp:Label>
                            </span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </li>
            </ul>
            <div id="resultMessageDiv" runat="server">
            </div>
            <div class="form-btn">
                <asp:HyperLink ID="cancelLinkButton" runat="server" CBID="272" CssClass="cancel">
                </asp:HyperLink>
                <asp:LinkButton ID="sendLinkButton" runat="server" CBID="703" CssClass="thankyou simplemodal-overlay"
                    ValidationGroup="ValidationGp2" OnClick="Send_Click">
                </asp:LinkButton>
            </div>
            <div class="clear">
                &nbsp;</div>
        </div>
    </div>
    </form>
</body>
</html>