﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class AccountStatus : PageBase
    {
        private IPartnerManager _partnerManager;

        /// <summary>
        /// Initialize the partner manager instance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
              
                ValidateUserAccess((int)MenuEnum.AccountStatus);

                if (!IsPostBack)
                {
                    SetRightPanelPermissionAccess();
                }
                _partnerManager = PartnerManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(96, culture, "My Account");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Set the right panel permission
        /// </summary>
        private void SetRightPanelPermissionAccess()
        {
            if (!ValidateMenuAuthorization((int)MenuEnum.ConstructionReport))
                constructionReportListItem.Visible = false;
            else
                constructionReportListItem.Visible = true;

            if (!ValidateMenuAuthorization((int)MenuEnum.ApprovedCategories))
                approvedCategoriesListItem.Visible = false;
            else
                approvedCategoriesListItem.Visible = true;

            if (!ValidateMenuAuthorization((int)MenuEnum.MyProducts))
                myProductsListItem.Visible = false;
            else
                myProductsListItem.Visible = true;
        }

        /// <summary>
        /// Bind the data to the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                resultMessageDiv.Attributes.Remove("class");
                resultMessageDiv.InnerText = string.Empty;
                

                if (!IsPostBack)
                {
                    this.Title = ResourceUtility.GetLocalizedString(58, culture, "Account Status");
                    rightPanelHeader.Text = ResourceUtility.GetLocalizedString(4, culture, "MY ACCOUNT").ToUpper(CultureInfo.InvariantCulture);
                    renewButton.Visible = false;
                    upgradePartnershipLinkButton.Visible = false;

                    PopulateDropDownData();

                    SetRegularExpressions();

                    SetRegionalContactAsReadOnly();

                    User user = GetSession<User>(SessionConstants.User);
                    SetHiddenFieldValue(partnerIdHiddenField, user.Company.ToString(CultureInfo.InvariantCulture));
                    var accountStatusDetails = _partnerManager.GetPartnerAccountStatus(Convert.ToInt32(user.Company, CultureInfo.InvariantCulture), Convert.ToInt32(user.UserId, CultureInfo.InvariantCulture));
                    PopulateAccountStatusDetails(accountStatusDetails);
                    //Set the visibility of renew button
                    if (accountStatusDetails.PartnershipExpirationDate.HasValue)
                    {
                        int renewButtonVisibilityDays = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.RenewButtonDisplayInterval),CultureInfo.InvariantCulture);
                        if (accountStatusDetails.PartnershipExpirationDate.Value.AddDays(-renewButtonVisibilityDays).Date <= System.DateTime.Today)
                        {
                            renewButton.Visible = true;
                        }

                    }
                    //Get the partnership type description
                    if (accountStatusDetails.PartnershipTypeDesc == Convert.ToString(PartnershipTypeEnum.GoldPartner, CultureInfo.InvariantCulture))
                        recommendedPartnerLabel.Text = ResourceUtility.GetLocalizedString(408, culture, "Gold-Level Recommended Partner");
                    if (accountStatusDetails.PartnershipTypeDesc == Convert.ToString(PartnershipTypeEnum.Partner, CultureInfo.InvariantCulture))
                    {
                        recommendedPartnerLabel.Text = ResourceUtility.GetLocalizedString(891, culture, "Partner");
                        upgradePartnershipLinkButton.Visible = true;
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// This method will set the validation expression for regular expression validator based on culture
        /// </summary>
        private void SetRegularExpressions()
        {
            // Regular Expression for primary contact
            SetValidatorRegularExpressions(firstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(phoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(zipCodeRegularExpressionValidator, (int)RegularExpressionTypeEnum.ZipCodeValidator);
            SetValidatorRegularExpressions(faxRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
            SetValidatorRegularExpressions(titlePrimaryRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);

            // Regular Expression for regional contact
            SetValidatorRegularExpressions(regionalFirstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(regionalLastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(phoneRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(regionalEmailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
            SetValidatorRegularExpressions(regionalTitleRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);

            // Regular Expression for secondary contact
            SetValidatorRegularExpressions(secondaryContactFirstNamerRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(secondaryContactLastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(secondaryContactPhoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(secondaryContactEmailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(secondaryContactFaxRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
            SetValidatorRegularExpressions(SecondaryContactTitleRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);
            
            // Regular Expression for website
            SetValidatorRegularExpressions(websiteRegularExpressionValidator, (int)RegularExpressionTypeEnum.WebsiteValidator);

            // Regular Expression for website
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
        }

        /// <summary>
        /// Populate the static drop downs
        /// </summary>
        private void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1240, culture, "Select a Country"), null);
            BindStaticDropDown(DropDownConstants.StateDropDown, stateDropDown, null, null);
        }


        /// <summary>
        /// Set the state of state drop down and zip code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompanyInfoCountryDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList country = (DropDownList)sender;
                if (country.SelectedItem.Value != DropDownConstants.UnitedStates)
                {
                    stateDropDown.SelectedValue = null;
                    if (stateDropDown.Items.Count > 0)
                    {
                        stateDropDown.Items.RemoveAt(0);
                    }
                    phoneTextBox.MaxLength = 32;
                    faxTextBox.MaxLength = 32;
                    SetValidatorRegularExpressions(phoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
                    SetValidatorRegularExpressions(faxRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
                    stateDropDown.Items.Insert(0, new ListItem(" ", DropDownConstants.DefaultValue));
                    stateDropDown.Enabled = false;
                    zipTextBox.ReadOnly = false;
                    SetTextBoxText(zipTextBox, null);
                    zipTextBox.Enabled = true;
                    zipTextBoxDiv.Attributes.Remove("class");
                    zipTextBoxDiv.Attributes.Add("class", "input textbox3 ");
                    zipRequiredFieldValidator.Enabled = false;
                    zipCodeRegularExpressionValidator.Enabled = false;
                    stateRequiredFieldValidator.Enabled = false;
                }
                else
                {
                    phoneTextBox.MaxLength = 10;
                    faxTextBox.MaxLength = 10;
                    SetValidatorRegularExpressions(phoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidatorUS);
                    SetValidatorRegularExpressions(faxRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidatorUS);
                    if (stateDropDown.Items.Count > 0)
                    {
                        stateDropDown.Items.RemoveAt(0);
                    }
                    stateDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1245, culture, "Select a State"), DropDownConstants.DefaultValue));
                    stateDropDown.Enabled = true;
                    zipTextBox.ReadOnly = false;
                    zipTextBox.Enabled = true;
                    zipTextBoxDiv.Attributes.Remove("class");
                    zipTextBoxDiv.Attributes.Add("class", "input textbox3 ");
                    zipRequiredFieldValidator.Enabled = true;
                    zipCodeRegularExpressionValidator.Enabled = true;
                    stateRequiredFieldValidator.Enabled = true;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Save the details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    User user = GetSession<User>(SessionConstants.User);
                    var accountStatus = new Entities.AccountStatus()
                    {
                        HiltonId = Convert.ToString(user.HiltonUserId, CultureInfo.InvariantCulture),
                        PartnerId = partnerIdHiddenField.Value,
                        FirstName = firstNameTextBox.Text,
                        LastName = lastNameTextBox.Text,
                        CompanyAddress1 = addressTextBox.Text,
                        CompanyAddress2 = addressLine2textBox.Text,
                        CountryId = Convert.ToInt32(countryDropDown.SelectedItem.Value, CultureInfo.InvariantCulture),
                        City = cityTextBox.Text,
                        StateId = stateDropDown.SelectedItem != null ? Convert.ToInt32(stateDropDown.SelectedItem.Value, CultureInfo.InvariantCulture) : 0,
                        ZipCode = zipTextBox.Text.Trim(),
                        Phone = phoneTextBox.Text,
                        Fax = faxTextBox.Text,
                        Email = emailAddressTextBox.Text,
                        CompanyDescription = companyDescriptionTextBox.Text,
                        WebSite = websiteTextBox.Text,
                        UserId = user.UserId,
                        CompanyLogoImage = Session[SessionConstants.ProductImageBytes] != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? (byte[])Session[SessionConstants.ProductImageBytes] : null,
                        CompanyLogoThumbnailImage = Session[SessionConstants.ProductImageBytes] != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? ((byte[])Session[SessionConstants.ProductImageBytes]).ConvertToThumbnail() : null,
                        CompanyLogoName = imageNameHiddenField.Value,
                    };
                    accountStatus.Contacts = new List<Contact>();
                    accountStatus.Contacts.Add(new Contact()
                    {
                        ContactTypeId = (int)ContactTypeEnum.Primary,
                        ContactId = GetViewState<string>(ViewStateConstants.PrimaryContactId),
                        FirstName = firstNameTextBox.Text,
                        LastName = lastNameTextBox.Text,
                        Title = titlePrimaryContactTextBox.Text,
                        Phone = phoneTextBox.Text,
                        Fax = faxTextBox.Text,
                        Email = emailAddressTextBox.Text
                    });
                    accountStatus.Contacts.Add(new Contact()
                    {
                        ContactTypeId = (int)ContactTypeEnum.Secondary,
                        ContactId = GetViewState<string>(ViewStateConstants.SecondaryContactId),
                        FirstName = secondaryContactFirstNameTextbox.Text,
                        LastName = secondaryContactLastNameTextbox.Text,
                        Title = secondaryContactTitleTextBox.Text,
                        Phone = secondaryContactPhoneTextbox.Text,
                        Fax = secondaryContactFaxTextbox.Text,
                        Email = secondaryContactEmailTextbox.Text
                    });
                    var regionalContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                    
                    foreach (Contact contact in regionalContacts.Where(p => !(string.IsNullOrWhiteSpace(p.FirstName))))
                    {
                        accountStatus.Contacts.Add(new Contact()
                        {
                            ContactTypeId = (int)ContactTypeEnum.Regional,
                            ContactId = contact.ContactId,
                            FirstName = contact.FirstName,
                            LastName = contact.LastName,
                            Title=contact.Title,
                            Phone = contact.Phone,
                            Fax = contact.Fax,
                            Email = contact.Email,
                            CountryId = contact.CountryId,
                            CountryName = contact.CountryName,
                            RegionId = contact.RegionId
                        });
                    }

                    int result = -1;
                    result = _partnerManager.UpdatePartnerAccountStatus(accountStatus);
                    if (result == -1)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.AccountStatusUpdateFailure, culture));
                    }
                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.AccountStatusUpdateSuccess, culture));
                    }
                }
                resultUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Perform the cancel button operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Account/AccountStatus.aspx", false);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Renew the partnership
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RenewLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                string applicationPaymentUrl = string.Empty;

                User user = GetSession<User>(SessionConstants.User);
                Context.Items[UIConstants.PartnerId] = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

                //TO - Do - To be uncommented after CR for Renew Application
                //Server.Transfer("~/Partners/BecomeRecommendedPartner.aspx",false);

                Server.Transfer("~/Activate/ProcessPaymentRequest.aspx?Code=" + GetViewState<string>(ViewStateConstants.PaymentCode), false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Upgrade the partnership
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpgradePartnershipLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                //string applicationPaymentUrl = string.Empty;

                //User user = GetSession<User>(SessionConstants.User);
                //Context.Items[UIConstants.PartnerId] = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

                //TO - Do - To be uncommented after CR for Renew Application
                //Server.Transfer("~/Partners/BecomeRecommendedPartner.aspx",false);

                Server.Transfer("~/Activate/ProcessPaymentRequest.aspx?Code=" + GetViewState<string>(ViewStateConstants.PaymentCode) + "&UpgradePartner=" + PaymentGatewayConstants.UpgradePartner, false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Populate the account status details
        /// </summary>
        /// <param name="accountStatus"></param>
        private void PopulateAccountStatusDetails(Entities.AccountStatus accountStatus)
        {
            companyNameLabel.Text = accountStatus.CompanyName;

            companyNameTextBox.Text = accountStatus.CompanyName;
            addressTextBox.Text = accountStatus.CompanyAddress1;
            addressLine2textBox.Text = accountStatus.CompanyAddress2;

            PopulateContactInfo(accountStatus.Contacts,accountStatus.Countries);
            PopulateCountries(accountStatus.Countries);

            cityTextBox.Text = accountStatus.City;
            zipTextBox.Text = accountStatus.ZipCode;
            companyDescriptionTextBox.Text = accountStatus.CompanyDescription;
            websiteTextBox.Text = accountStatus.WebSite;
            activationDateLiteral.Text = accountStatus.PartnershipActiveDate.HasValue ? accountStatus.PartnershipActiveDate.Value.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) : string.Empty;
            expirationDateLiteral.Text = accountStatus.PartnershipExpirationDate.HasValue ? accountStatus.PartnershipExpirationDate.Value.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) : string.Empty;

            if (countryDropDown.Items.FindByValue(accountStatus.CountryId.ToString(CultureInfo.InvariantCulture)) != null)
            {
                countryDropDown.Items.FindByValue(accountStatus.CountryId.ToString(CultureInfo.InvariantCulture)).Selected = true;
            }
            if (stateDropDown.Items.FindByValue(accountStatus.StateId.ToString(CultureInfo.InvariantCulture)) != null)
            {
                stateDropDown.Items.FindByValue(accountStatus.StateId.ToString(CultureInfo.InvariantCulture)).Selected = true;
            }

            if (countryDropDown.SelectedValue != DropDownConstants.UnitedStates)
            {
                stateDropDown.Items.Insert(0, new ListItem(" ", DropDownConstants.DefaultValue));
                stateDropDown.Enabled = false;
                zipTextBox.ReadOnly = false;
                zipTextBox.Enabled = true;
                zipTextBoxDiv.Attributes.Add("class", "input small ");
                zipTextBox.Attributes.Add("class", "mandatory");
                zipRequiredFieldValidator.Enabled = true;
                zipCodeRegularExpressionValidator.Enabled = true;
                stateRequiredFieldValidator.Enabled = false;
            }
            else
            {
                stateDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1245, culture, "Select a State"), DropDownConstants.DefaultValue));
                stateDropDown.Enabled = true;
                zipTextBox.ReadOnly = false;
                zipTextBox.Enabled = true;
                zipTextBoxDiv.Attributes.Clear();
                zipTextBoxDiv.Attributes.Add("class", "input small ");
                zipTextBox.Attributes.Add("class", "mandatory");
                zipRequiredFieldValidator.Enabled = true;
                zipCodeRegularExpressionValidator.Enabled = true;
                stateRequiredFieldValidator.Enabled = true;
            }
            //Set the visibility of required partnership type
            if (accountStatus.PartnershipTypeId == (int)PartnershipTypeEnum.GoldPartner)
            {
                partnershipLevel.Attributes.Clear();
                partnershipLevel.Attributes.Add("class", "account-gold-level");
                recommendedPartnerLabel.Text = ResourceUtility.GetLocalizedString(408, culture, "Gold-Level Recommended Partner");
            }
            else if (accountStatus.PartnershipTypeId == (int)PartnershipTypeEnum.Partner)
            {
                partnershipLevel.Attributes.Clear();
                partnershipLevel.Attributes.Add("class", "account-normal-level");
                ResourceUtility.GetLocalizedString(891, culture, "Partner");
            }

            companyLogo.ImageUrl = accountStatus.CompanyLogoImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(accountStatus.CompanyLogoImage.ConvertToThumbnail()) : "../Images/NoProduct.PNG";
            SetSession<byte[]>(SessionConstants.ProductImageBytes, accountStatus.CompanyLogoImage);
            imageLinkButton.Text = accountStatus.CompanyLogoName;
            imageNameHiddenField.Value = accountStatus.CompanyLogoName;
            if (!string.IsNullOrEmpty(accountStatus.CompanyLogoName))
                removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "inline");

            SetViewState<string>(ViewStateConstants.PaymentCode, accountStatus.PaymentCode);
        }

        

        /// <summary>
        /// To get product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Partner partner = _partnerManager.GetPartnerImage(Convert.ToInt32(partnerIdHiddenField.Value,CultureInfo.InvariantCulture));
                ProcessImageDownload(partner.CompanyLogoImage, partner.CompanyLogoName);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "SpecSheetScript1", "SetFileInputStyles();", true);
        }

        /// <summary>
        /// Process the image download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessImageDownload(byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            if (fileBytes != null && !string.IsNullOrWhiteSpace(fileName))
            {
                string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                int upperBound = tempString.GetUpperBound(0);
                contentType = "image/" + tempString[upperBound];
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.ContentType = contentType;
                Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
                Response.BinaryWrite(fileBytes);
                Response.Flush();
                Response.End();
            }
        }

        /// <summary>
        /// Remove image link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageLinkButton.Text = string.Empty;
                imageNameHiddenField.Value = string.Empty;
                companyLogo.ImageUrl = "../Images/NoProduct.PNG";
                ClearSession(SessionConstants.ProductImageBytes);
                removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "ImageUpload", "SetFileInputStyles();", true);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Populate the regional contacts as per selected region
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionCountryDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList country = sender as DropDownList;
                int previousSelectedCountry = Convert.ToInt32(GetViewState<string>(ViewStateConstants.PreviousSelectedCountry), CultureInfo.InvariantCulture);
                SetViewState<string>(ViewStateConstants.PreviousSelectedCountry, country.SelectedItem.Value);

                var regionalContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                if (previousSelectedCountry != 0)
                {

                    var contactToUpdate = regionalContacts.Where(p => p.CountryId == previousSelectedCountry).FirstOrDefault();
                    contactToUpdate.FirstName = regionalFirstNameTextBox.Text;
                    contactToUpdate.LastName = regionalLastNameTextBox.Text;
                    contactToUpdate.Title = regionalTitleTextBox.Text;
                    contactToUpdate.Phone = regionalPhoneTextBox.Text;
                    contactToUpdate.Fax = regionalFaxTextBox.Text;
                    contactToUpdate.Email = regionalEmailTextBox.Text;
                }
                if (country.SelectedItem.Value == DropDownConstants.DefaultValue)
                {
                    SetRegionalContactAsReadOnly();
                }
                else
                {
                    if (regionalContacts.Any(p => p.CountryId == Convert.ToInt32(country.SelectedItem.Value, CultureInfo.InvariantCulture)))
                    {
                        foreach (Contact contact in regionalContacts)
                        {
                            if (country.SelectedItem.Value == contact.CountryId.ToString(CultureInfo.InvariantCulture))
                            {
                                SetTextBoxText(regionalFirstNameTextBox, contact.FirstName);
                                SetTextBoxText(regionalLastNameTextBox, contact.LastName);
                                SetTextBoxText(regionalTitleTextBox, contact.Title);
                                SetTextBoxText(regionalPhoneTextBox, contact.Phone);
                                SetTextBoxText(regionalFaxTextBox, contact.Fax);
                                SetTextBoxText(regionalEmailTextBox, contact.Email);
                                break;
                            }
                        }
                    }
                    else
                    {
                        SetTextBoxText(regionalFirstNameTextBox, null);
                        SetTextBoxText(regionalLastNameTextBox, null);
                        SetTextBoxText(regionalTitleTextBox, null);
                        SetTextBoxText(regionalPhoneTextBox, null);
                        SetTextBoxText(regionalFaxTextBox, null);
                        SetTextBoxText(regionalEmailTextBox, null);
                    }
                }
                if (country.SelectedItem.Value == DropDownConstants.UnitedStates)
                {
                    regionalFaxTextBox.MaxLength = 32;   //changed as per test case 29089 -release 2016.01
                    regionalPhoneTextBox.MaxLength = 32;
                    SetValidatorRegularExpressions(phoneRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidatorUS);
                    SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidatorUS);

                }
                else
                {
                    regionalFaxTextBox.MaxLength = 32;
                    regionalPhoneTextBox.MaxLength = 32;
                    SetValidatorRegularExpressions(phoneRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
                    SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Set the state for regional contact controls state as read only
        /// </summary>
        private void SetRegionalContactAsReadOnly()
        {
            SetTextBoxText(regionalFirstNameTextBox, null);
            SetTextBoxText(regionalLastNameTextBox, null);
            SetTextBoxText(regionalPhoneTextBox, null);
            SetTextBoxText(regionalFaxTextBox, null);
            SetTextBoxText(regionalEmailTextBox, null);

            regionalFirstNameTextBox.Enabled = false;
            firstNameDiv.Attributes.Add("class", "input textbox readonly");

            regionalLastNameTextBox.Enabled = false;
            lastNameDiv.Attributes.Add("class", "input textbox readonly");

            regionalPhoneTextBox.Enabled = false;
            phoneDiv.Attributes.Add("class", "input textbox readonly");

            regionalFaxTextBox.Enabled = false;
            faxDiv.Attributes.Add("class", "input textbox2 readonly");

            regionalEmailTextBox.Enabled = false;
            emailDiv.Attributes.Add("class", "input textbox readonly");
        }

        /// <summary>
        /// Populate the contact info
        /// </summary>
        /// <param name="contacts"></param>
        /// //private void PopulateContactInfo(IList<Contact> contacts, IList<Region> regions)
        private void PopulateContactInfo(IList<Contact> contacts, IList<Country> countries)
        {
            IList<Contact> regionalContacts = new List<Contact>();
           
            foreach (var contact in contacts)
            {
                if (contact.ContactTypeId == (int)ContactTypeEnum.Primary)
                {
                    SetViewState<string>(ViewStateConstants.PrimaryContactId, contact.ContactId);
                    SetTextBoxText(firstNameTextBox, contact.FirstName);
                    SetTextBoxText(lastNameTextBox, contact.LastName);
                    SetTextBoxText(titlePrimaryContactTextBox, contact.Title);
                    SetTextBoxText(phoneTextBox, contact.Phone);
                    SetTextBoxText(faxTextBox, contact.Fax);
                    SetTextBoxText(emailAddressTextBox, contact.Email);
                }
                else if (contact.ContactTypeId == (int)ContactTypeEnum.Secondary)
                {
                    SetViewState<string>(ViewStateConstants.SecondaryContactId, contact.ContactId);
                    SetTextBoxText(secondaryContactFirstNameTextbox, contact.FirstName);
                    SetTextBoxText(secondaryContactLastNameTextbox, contact.LastName);
                    SetTextBoxText(secondaryContactTitleTextBox, contact.Title);
                    SetTextBoxText(secondaryContactPhoneTextbox, contact.Phone);
                    SetTextBoxText(secondaryContactFaxTextbox, contact.Fax);
                    SetTextBoxText(secondaryContactEmailTextbox, contact.Email);
                }
                else if (contact.ContactTypeId == (int)ContactTypeEnum.Regional)
                {
                    regionalContacts.Add(contact);
                }
            }

            foreach (var country in countries)
            {
                if (regionalContacts.Any(p => p.CountryId != country.CountryId))
                {
                    regionalContacts.Add(new Contact
                    {
                        CountryId = country.CountryId,
                        CountryName = country.CountryName,
                        RegionId = country.RegionId
                    });
                }
            }
            SetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState, regionalContacts);
        }
        //private void PopulatrRegions(IList<Region> regions)
        private void PopulateCountries(IList<Country> countries)
        {
            regionCountryDropDown.DataSource = countries;
            regionCountryDropDown.DataTextField = "CountryName";
            regionCountryDropDown.DataValueField = "CountryId";
            regionCountryDropDown.DataBind();
            SetViewState<string>(ViewStateConstants.PreviousSelectedCountry, "0");

            var regionalContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
            foreach (var country in countries)
            {
                if (regionalContacts.Count == 0 || regionalContacts.Any(p => p.CountryId != country.CountryId))
                {
                    regionalContacts.Add(new Contact
                    {
                        CountryId = country.CountryId,
                        CountryName = country.CountryName,
                        RegionId = country.RegionId
                    });
                }
            }

            SetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState, regionalContacts);
            SetViewState<IList<Country>>(ViewStateConstants.RegionalContactDropDownViewState, countries);
            regionCountryDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1252, culture, "Select"), DropDownConstants.DefaultValue));

        }

        /// <summary>
        /// Validate the fields required for saving the partner details
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void SavePartnerValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (regionCountryDropDown.SelectedItem.Value != DropDownConstants.DefaultValue)
                {
                    var regionalContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                    var contactToUpdate = regionalContacts.Where(p => p.CountryId.ToString(CultureInfo.InvariantCulture) == regionCountryDropDown.SelectedItem.Value).FirstOrDefault();
                    contactToUpdate.FirstName = regionalFirstNameTextBox.Text;
                    contactToUpdate.LastName = regionalLastNameTextBox.Text;
                    contactToUpdate.Title = regionalTitleTextBox.Text;
                    contactToUpdate.Phone = regionalPhoneTextBox.Text;
                    contactToUpdate.Fax = regionalFaxTextBox.Text;
                    contactToUpdate.Email = regionalEmailTextBox.Text;
                }
               
                var regionalContact = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();

                foreach (var contact in regionalContact)
                {
                    if ((string.IsNullOrWhiteSpace(contact.FirstName) && string.IsNullOrWhiteSpace(contact.LastName)
                        && string.IsNullOrWhiteSpace(contact.Email) && string.IsNullOrWhiteSpace(contact.Phone)
                        && string.IsNullOrWhiteSpace(contact.Fax)) && string.IsNullOrWhiteSpace(contact.Title))
                    {
                        continue;
                    }
                    else if ((string.IsNullOrWhiteSpace(contact.FirstName) || string.IsNullOrWhiteSpace(contact.LastName)
                        || string.IsNullOrWhiteSpace(contact.Email) || string.IsNullOrWhiteSpace(contact.Phone)
                        || string.IsNullOrWhiteSpace(contact.Fax)))
                    {
                        args.IsValid = false;
                        var regionalContactDropDown = GetViewState<IList<Country>>(ViewStateConstants.RegionalContactDropDownViewState) ?? new List<Country>();
                        if (string.IsNullOrWhiteSpace(regionalContactErrorMessageLabel.Text))
                        {
                            regionalContactErrorMessageLabel.Text = string.Format(CultureInfo.InvariantCulture, ConfigurationStore.GetApplicationMessages(MessageConstants.RegionalContactErrorMessage, culture),
                              regionalContactDropDown.Where(p => p.CountryId == contact.CountryId).FirstOrDefault().CountryName);
                        }
                        else
                        {
                            regionalContactErrorMessageLabel.Text = Server.HtmlEncode(regionalContactErrorMessageLabel.Text) + "<br />" + Server.HtmlEncode(string.Format(CultureInfo.InvariantCulture, ConfigurationStore.GetApplicationMessages(MessageConstants.RegionalContactErrorMessage, culture),
                                regionalContactDropDown.Where(p => p.CountryId == contact.CountryId).FirstOrDefault().CountryName));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        
        }
    }
}