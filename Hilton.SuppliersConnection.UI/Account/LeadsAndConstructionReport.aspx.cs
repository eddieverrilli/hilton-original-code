﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class LeadsAndConstructionReport : PageBase
    {
        private IPartnerManager _partnerManager;
        private IHelperManager _helperManager;

        /// <summary>
        /// Invoked at OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                ValidateUserAccess((int)MenuEnum.ConstructionReport);

                User user = GetSession<User>(SessionConstants.User);

                if (user != null && user.PartnershipType != (int)PartnershipTypeEnum.GoldPartner)
                {
                    Response.Redirect("~/AccessDenied.aspx", false);
                }

                _partnerManager = PartnerManager.Instance;
                _helperManager = HelperManager.Instance;
                this.Title = ResourceUtility.GetLocalizedString(95, culture, "Leads & Construction Report");

                SetGridViewHeaderText();
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// .NET will refuse to accept "unknown" post backs for security reasons.
        /// Because of this we have to register all possible callbacks.
        /// This must be done in Render, hence the override for selecting row on mouse click
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            Page.ClientScript.RegisterForEventValidation(
                    new System.Web.UI.PostBackOptions(
                        warmLeadPrevLinkButton));

            Page.ClientScript.RegisterForEventValidation(
                   new System.Web.UI.PostBackOptions(
                       warmLeadNextLinkButton));

            Page.ClientScript.RegisterForEventValidation(
                   new System.Web.UI.PostBackOptions(
                       hotLeadPrevLinkButton));

            Page.ClientScript.RegisterForEventValidation(
                   new System.Web.UI.PostBackOptions(
                       hotLeadNextLinkButton));

            Page.ClientScript.RegisterForEventValidation(
                          new System.Web.UI.PostBackOptions(
                              GridViewWarmLeads, "Sort$ProjectAddedDate"));

            Page.ClientScript.RegisterForEventValidation(
                          new System.Web.UI.PostBackOptions(
                              GridViewWarmLeads, "Sort$Category"));

            Page.ClientScript.RegisterForEventValidation(
                    new System.Web.UI.PostBackOptions(
                        GridViewWarmLeads, "Sort$ProductName"));

            Page.ClientScript.RegisterForEventValidation(
                        new System.Web.UI.PostBackOptions(
                            GridViewWarmLeads, "Sort$ProjectTypeDescription"));

            Page.ClientScript.RegisterForEventValidation(
                        new System.Web.UI.PostBackOptions(
                            GridViewWarmLeads, "Sort$BrandName"));

            Page.ClientScript.RegisterForEventValidation(
                          new System.Web.UI.PostBackOptions(
                              GridViewWarmLeads, "Sort$MessageDate"));

            Page.ClientScript.RegisterForEventValidation(
                         new System.Web.UI.PostBackOptions(
                             GridViewHotLeads, "Sort$ProjectAddedDate"));

            Page.ClientScript.RegisterForEventValidation(
                          new System.Web.UI.PostBackOptions(
                              GridViewHotLeads, "Sort$Category"));

            Page.ClientScript.RegisterForEventValidation(
                    new System.Web.UI.PostBackOptions(
                        GridViewHotLeads, "Sort$ProductName"));

            Page.ClientScript.RegisterForEventValidation(
                        new System.Web.UI.PostBackOptions(
                            GridViewHotLeads, "Sort$ProjectTypeDescription"));

            Page.ClientScript.RegisterForEventValidation(
                        new System.Web.UI.PostBackOptions(
                            GridViewHotLeads, "Sort$BrandName"));

            Page.ClientScript.RegisterForEventValidation(
                          new System.Web.UI.PostBackOptions(
                              GridViewHotLeads, "Sort$MessageDate"));

           
            // Do the standard rendering stuff
            base.Render(writer);
        }

        /// <summary>
        /// Invoked at Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int partnerId;

            try
            {
                User user = GetSession<User>(SessionConstants.User);
                partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);
                divHotLeadPaging.Visible = false;
                divWarmLeadPaging.Visible = false;
                if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("ContactOwner.aspx")))
                    {
                        MaintainPageState();
                    }
                    else
                    {
                        ConstructionReportSearch constReportSearch = new ConstructionReportSearch()
                        {
                            PartnerId = partnerId,
                            SortExpression = UIConstants.Default,
                            SortDirection = UIConstants.AscAbbreviation,
                            PageIndex = 0,
                            PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.ConstructionReportMaxPageSize), CultureInfo.InvariantCulture)
                        };

                        SetViewState(ViewStateConstants.WarmLeadSortExpression, UIConstants.Default);
                        SetViewState(ViewStateConstants.HotLeadSortExpression, UIConstants.Default);
                        SetViewState(ViewStateConstants.WarmLeadSortDirection, SortDirection.Ascending);
                        SetViewState(ViewStateConstants.HotLeadSortDirection, SortDirection.Ascending);
              
                        var hotLeadsCollection = _partnerManager.GetPartnerHotLeads(constReportSearch);

                        if (hotLeadsCollection.Item2 == 0)
                        {
                            ConfigureResultMessage(hotLeadDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoHotLeadsFound, culture));
                        }
                        else
                        {
                            divHotLeadPaging.Visible = true;
                            hotLeadPrevLinkButton.Enabled = false;
                            hotLeadStartPage.Text = "1";
                            hotLeadEndPage.Text = (Math.Ceiling((Double)hotLeadsCollection.Item2 / constReportSearch.PageSize)).ToString(CultureInfo.InvariantCulture);
                            constReportSearch.EndPage = hotLeadEndPage.Text;

                            if (Convert.ToInt16(hotLeadEndPage.Text, CultureInfo.InvariantCulture) == 1)
                            {
                                hotLeadNextLinkButton.Enabled = false;
                            }
                            else
                                hotLeadNextLinkButton.Enabled = true;

                            if (hotLeadStartPage.Text == hotLeadEndPage.Text)
                            {
                                divHotLeadPaging.Visible = false;
                            }
                            GridViewHotLeads.DataSource = hotLeadsCollection.Item1;
                            GridViewHotLeads.DataBind();
                        }
                        SetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportHotLead, constReportSearch);

                        rightPanelHeader.Text = ResourceUtility.GetLocalizedString(4, culture, "MY ACCOUNT").ToUpper();

                        // Bind The Warm Leads Grid

                       constReportSearch = new ConstructionReportSearch()
                        {
                            PartnerId = partnerId,
                            SortExpression = UIConstants.Default,
                            SortDirection = UIConstants.AscAbbreviation,
                            PageIndex = 0,
                            PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.ConstructionReportMaxPageSize), CultureInfo.InvariantCulture)
                        };
                        var warmLeadCollection = _partnerManager.GetPartnerWarmLeads(constReportSearch);

                        if (warmLeadCollection.Item2 == 0)
                        {
                            ConfigureResultMessage(warmLeadDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoWarmLeadsFound, culture));
                        }

                        else
                        {
                            divWarmLeadPaging.Visible = true;
                            warmLeadPrevLinkButton.Enabled = false;
                            warmLeadStartPage.Text = "1";
                            warmLeadEndPage.Text = (Math.Ceiling((Double)warmLeadCollection.Item2 / constReportSearch.PageSize)).ToString(CultureInfo.InvariantCulture);
                            constReportSearch.EndPage = warmLeadEndPage.Text;
                            if (Convert.ToInt16(warmLeadEndPage.Text, CultureInfo.InvariantCulture) == 1)
                            {
                                warmLeadNextLinkButton.Enabled = false;
                            }
                            else
                                warmLeadNextLinkButton.Enabled = true;

                            if (warmLeadStartPage.Text == warmLeadEndPage.Text)
                            {
                                divWarmLeadPaging.Visible = false;
                            }

                            GridViewWarmLeads.DataSource = warmLeadCollection.Item1;
                            GridViewWarmLeads.DataBind();
                        }
                        SetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportWarmLead, constReportSearch);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(hotLeadConstructionReportUpdatePanel, hotLeadConstructionReportUpdatePanel.GetType(), "PopUpScript", "tb_init('a.thickbox')", true);
                    ScriptManager.RegisterClientScriptBlock(warmLeadConstructionReportUpdatePanel, warmLeadConstructionReportUpdatePanel.GetType(), "PopUpScript", "tb_init('a.thickbox')", true);
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        private void MaintainPageState()
        {
           ConstructionReportSearch hotLeadConstReportSearch = GetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportHotLead);
            SetViewState(ViewStateConstants.HotLeadSortExpression, hotLeadConstReportSearch.SortExpression);
            if (hotLeadConstReportSearch.SortDirection == UIConstants.AscAbbreviation)
                SetViewState(ViewStateConstants.HotLeadSortDirection, SortDirection.Ascending);
            else
                SetViewState(ViewStateConstants.HotLeadSortDirection, SortDirection.Descending);
            BindHotLeadGrid(hotLeadConstReportSearch);

            ConstructionReportSearch warmLeadConstReportSearch = GetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportWarmLead);
            SetViewState(ViewStateConstants.WarmLeadSortExpression, warmLeadConstReportSearch.SortExpression);
            if (warmLeadConstReportSearch.SortDirection == UIConstants.AscAbbreviation)
                SetViewState(ViewStateConstants.WarmLeadSortDirection, SortDirection.Ascending);
            else
                SetViewState(ViewStateConstants.WarmLeadSortDirection, SortDirection.Descending);
            BindWarmLeadGrid(warmLeadConstReportSearch);
        }

        /// <summary>
        /// Sets GridView Header Text from CBID
        /// </summary>
        private void SetGridViewHeaderText()
        {
            GridViewWarmLeads.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(196, culture);
            GridViewWarmLeads.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(286, culture);
            GridViewWarmLeads.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(522, culture);
            GridViewWarmLeads.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(549, culture);
            GridViewWarmLeads.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(558, culture);
            GridViewWarmLeads.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(321, culture);

            GridViewHotLeads.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(350, culture);
            GridViewHotLeads.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(285, culture);
            GridViewHotLeads.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(522, culture);
            GridViewHotLeads.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(549, culture);
            GridViewHotLeads.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(558, culture);
            GridViewHotLeads.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(321, culture);
        }

        /// <summary>
        /// To go back to previous page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HotLeadPrevLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                divHotLeadPaging.Visible = true;

                hotLeadNextLinkButton.Enabled = true;

                User user = GetSession<User>(SessionConstants.User);
                int partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

                ConstructionReportSearch constReportSearch = GetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportHotLead);
                constReportSearch.PageIndex -= 1;

                var hotLeadsCollection = _partnerManager.GetPartnerHotLeads(constReportSearch);

                if (hotLeadsCollection.Item2 == 0)
                {
                    ConfigureResultMessage(hotLeadDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoHotLeadsFound, culture));
                }
                else
                {
                    GridViewHotLeads.DataSource = hotLeadsCollection.Item1;
                    GridViewHotLeads.DataBind();
                }

                hotLeadStartPage.Text = (constReportSearch.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                if (Convert.ToInt16(hotLeadStartPage.Text, CultureInfo.InvariantCulture) == 1)
                {
                    hotLeadPrevLinkButton.Enabled = false;
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To go to next page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HotLeadNextLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                divHotLeadPaging.Visible = true;
                hotLeadPrevLinkButton.Enabled = true;
                ConstructionReportSearch constReportSearch = GetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportHotLead);
                constReportSearch.PageIndex += 1;

                var hotLeadsCollection = _partnerManager.GetPartnerHotLeads(constReportSearch);

                if (hotLeadsCollection.Item2 == 0)
                {
                    ConfigureResultMessage(hotLeadDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoHotLeadsFound, culture));
                }
                else
                {
                    GridViewHotLeads.DataSource = hotLeadsCollection.Item1;
                    GridViewHotLeads.DataBind();
                }

                if (Convert.ToInt16(hotLeadEndPage.Text, CultureInfo.InvariantCulture) == constReportSearch.PageIndex + 1)
                {
                    hotLeadNextLinkButton.Enabled = false;
                }
                hotLeadStartPage.Text = (constReportSearch.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                SetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportHotLead, constReportSearch);
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To go back to previous page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void WarmLeadPrevLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                divWarmLeadPaging.Visible = true;
                warmLeadNextLinkButton.Enabled = true;
                User user = GetSession<User>(SessionConstants.User);
                int partnerId = Convert.ToInt32(user.Company, CultureInfo.InvariantCulture);

                ConstructionReportSearch constReportSearch = GetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportWarmLead);
                constReportSearch.PageIndex -= 1;

                var warmLeadCollection = _partnerManager.GetPartnerWarmLeads(constReportSearch);

                if (warmLeadCollection.Item2 == 0)
                {
                    ConfigureResultMessage(warmLeadDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoWarmLeadsFound, culture));
                }

                else
                {
                    GridViewWarmLeads.DataSource = warmLeadCollection.Item1;
                    GridViewWarmLeads.DataBind();
                }
      
                warmLeadStartPage.Text = (constReportSearch.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                if (Convert.ToInt16(warmLeadStartPage.Text, CultureInfo.InvariantCulture) == 1)
                {
                    warmLeadPrevLinkButton.Enabled = false;
                }
                SetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportWarmLead, constReportSearch);
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To go to next page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void WarmLeadNextLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                divWarmLeadPaging.Visible = true;
                warmLeadPrevLinkButton.Enabled = true;
                ConstructionReportSearch constReportSearch = GetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportWarmLead);
                constReportSearch.PageIndex += 1;

                var warmLeadCollection = _partnerManager.GetPartnerWarmLeads(constReportSearch);

                if (warmLeadCollection.Item2 == 0)
                {
                    ConfigureResultMessage(warmLeadDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoWarmLeadsFound, culture));
                }

                else
                {
                    GridViewWarmLeads.DataSource = warmLeadCollection.Item1;
                    GridViewWarmLeads.DataBind();
                }

                if (Convert.ToInt16(warmLeadEndPage.Text, CultureInfo.InvariantCulture) == constReportSearch.PageIndex + 1)
                {
                    warmLeadNextLinkButton.Enabled = false;
                }
                warmLeadStartPage.Text = (constReportSearch.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                SetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportWarmLead, constReportSearch);
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Bind Partner Hot Leads
        /// </summary>
        /// <param name="partnerId"></param>
        private void BindHotLeadGrid(ConstructionReportSearch constReportSearch)
        {
            SetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportHotLead, constReportSearch);
            var hotLeadsCollection = _partnerManager.GetPartnerHotLeads(constReportSearch);

            if (hotLeadsCollection.Item2 == 0)
            {
                ConfigureResultMessage(hotLeadDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoHotLeadsFound, culture));
            }
            else
            {
                GridViewHotLeads.DataSource = hotLeadsCollection.Item1;
                GridViewHotLeads.DataBind();

                if (constReportSearch.PageIndex != 0)
                {
                    divHotLeadPaging.Visible = true;
                    hotLeadPrevLinkButton.Enabled = true;
                    if (Convert.ToInt16(constReportSearch.EndPage, CultureInfo.InvariantCulture) == (constReportSearch.PageIndex + 1))
                    {
                        hotLeadNextLinkButton.Enabled = false;
                    }
                    hotLeadStartPage.Text = (constReportSearch.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                    hotLeadEndPage.Text = (constReportSearch.EndPage).ToString(CultureInfo.InvariantCulture);

                    if (hotLeadStartPage.Text == hotLeadEndPage.Text)
                    {
                        divHotLeadPaging.Visible = false;
                    }
                }
                else if (constReportSearch.PageIndex == 0)
                {
                    divHotLeadPaging.Visible = true;
                    hotLeadPrevLinkButton.Enabled = false;
                    hotLeadStartPage.Text = "1";
                    hotLeadEndPage.Text = (Math.Ceiling((Double)hotLeadsCollection.Item2 / constReportSearch.PageSize)).ToString(CultureInfo.InvariantCulture);
                    if (Convert.ToInt16(hotLeadEndPage.Text, CultureInfo.InvariantCulture) == 1)
                    {
                        hotLeadNextLinkButton.Enabled = false;
                    }
                    else
                        hotLeadNextLinkButton.Enabled = true;

                    if (hotLeadStartPage.Text == hotLeadEndPage.Text)
                    {
                        divHotLeadPaging.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// Bind Partner Warm Lead Grid
        /// </summary>
        /// <param name="myProjectSearchCriteria"></param>
        private void BindWarmLeadGrid(ConstructionReportSearch constReportSearch)
        {
            SetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportWarmLead, constReportSearch);
            var warmLeadCollection = _partnerManager.GetPartnerWarmLeads(constReportSearch);

            if (warmLeadCollection.Item2 == 0)
            {
                ConfigureResultMessage(warmLeadDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoWarmLeadsFound, culture));
            }

            else
            {
                GridViewWarmLeads.DataSource = warmLeadCollection.Item1;
                GridViewWarmLeads.DataBind();

                if (constReportSearch.PageIndex != 0)
                {
                    divWarmLeadPaging.Visible = true;
                    warmLeadPrevLinkButton.Enabled = true;
                    if (Convert.ToInt16(constReportSearch.EndPage, CultureInfo.InvariantCulture) == (constReportSearch.PageIndex + 1))
                    {
                        warmLeadNextLinkButton.Enabled = false;
                    }
                    warmLeadStartPage.Text = (constReportSearch.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                    warmLeadEndPage.Text = (constReportSearch.EndPage).ToString(CultureInfo.InvariantCulture);

                    if (warmLeadStartPage.Text == warmLeadEndPage.Text)
                    {
                        divWarmLeadPaging.Visible = false;
                    }
                }
                else if (constReportSearch.PageIndex == 0)
                {
                    divWarmLeadPaging.Visible = true;
                    warmLeadPrevLinkButton.Enabled = false;
                    warmLeadStartPage.Text = "1";
                    warmLeadEndPage.Text = (Math.Ceiling((Double)warmLeadCollection.Item2 / constReportSearch.PageSize)).ToString(CultureInfo.InvariantCulture);
                    if (Convert.ToInt16(warmLeadEndPage.Text, CultureInfo.InvariantCulture) == 1)
                    {
                        warmLeadNextLinkButton.Enabled = false;
                    }
                    else
                        warmLeadNextLinkButton.Enabled = true;

                    if (warmLeadStartPage.Text == warmLeadEndPage.Text)
                    {
                        divWarmLeadPaging.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// This property return the sort direction structure
        /// </summary>
        protected SortDirection GridViewHotLeadSortDirection
        {
            get
            {
                if (GetViewState(ViewStateConstants.HotLeadSortDirection) == null)
                    SetViewState(ViewStateConstants.HotLeadSortDirection, SortDirection.Ascending);
                return (SortDirection)GetViewState(ViewStateConstants.HotLeadSortDirection);
            }
            set
            {
                SetViewState(ViewStateConstants.HotLeadSortDirection, value);
            }
        }

        /// <summary>
        /// This property return the sort direction structure
        /// </summary>
        protected SortDirection GridViewWarmLeadSortDirection
        {
            get
            {
                if (GetViewState(ViewStateConstants.WarmLeadSortDirection) == null)
                    SetViewState(ViewStateConstants.WarmLeadSortDirection, SortDirection.Ascending);
                return (SortDirection)GetViewState(ViewStateConstants.WarmLeadSortDirection);
            }
            set
            {
                SetViewState(ViewStateConstants.WarmLeadSortDirection, value);
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GridViewWarmLeads_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.WarmLeadSortExpression) != null && GetViewState<string>(ViewStateConstants.WarmLeadSortExpression) != sortExpression)
                    {
                        GridViewWarmLeadSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.WarmLeadSortExpression, sortExpression);

                    if (GridViewWarmLeadSortDirection == SortDirection.Ascending)
                    {
                        GridViewWarmLeadSortDirection = SortDirection.Descending;
                        SortWarmLeadGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewWarmLeadSortDirection = SortDirection.Ascending;
                        SortWarmLeadGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GridViewHotLeads_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.HotLeadSortExpression) != null && GetViewState<string>(ViewStateConstants.HotLeadSortExpression) != sortExpression)
                    {
                        GridViewHotLeadSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.HotLeadSortExpression, sortExpression);

                    if (GridViewHotLeadSortDirection == SortDirection.Ascending)
                    {
                        GridViewHotLeadSortDirection = SortDirection.Descending;
                        SortHotLeadGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewHotLeadSortDirection = SortDirection.Ascending;
                        SortHotLeadGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortWarmLeadGridView(string sortExpression, string sortDirection)
        {
            try
            {
                ConstructionReportSearch constReportSearch = GetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportWarmLead);
                constReportSearch.SortExpression = sortExpression;
                constReportSearch.SortDirection = sortDirection;
                constReportSearch.PageIndex = 0;
                BindWarmLeadGrid(constReportSearch);
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortHotLeadGridView(string sortExpression, string sortDirection)
        {
            try
            {
                ConstructionReportSearch constReportSearch = GetSession<ConstructionReportSearch>(SessionConstants.ConstructionReportHotLead);
                constReportSearch.SortExpression = sortExpression;
                constReportSearch.SortDirection = sortDirection;
                constReportSearch.PageIndex = 0;
                BindHotLeadGrid(constReportSearch);
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GridViewWarmLeads_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetWarmLeadSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddWarmLeadSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Add sorting image on sort column
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="HeaderRow"></param>
        protected void AddWarmLeadSortImage(int columnIndex, TableRow headerRow)
        {
            if (headerRow != null)
            {
                if (GetViewState<object>(ViewStateConstants.WarmLeadSortDirection).ToString() == UIConstants.Ascending)
                {
                    headerRow.Cells[columnIndex].Attributes.Add("class", "sort-a");
                }
                else if (GetViewState<object>(ViewStateConstants.WarmLeadSortDirection).ToString() == UIConstants.Descending)
                {
                    headerRow.Cells[columnIndex].Attributes.Add("class", "sort-d");
                }
            }
        }

        /// <summary>
        /// Add sorting image on sort column
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="HeaderRow"></param>
        protected void AddHotLeadSortImage(int columnIndex, TableRow headerRow)
        {
            if (headerRow != null)
            {
                if (GetViewState<object>(ViewStateConstants.HotLeadSortDirection).ToString() == UIConstants.Ascending)
                {
                    headerRow.Cells[columnIndex].Attributes.Add("class", "sort-a");
                }
                else if (GetViewState<object>(ViewStateConstants.HotLeadSortDirection).ToString() == UIConstants.Descending)
                {
                    headerRow.Cells[columnIndex].Attributes.Add("class", "sort-d");
                }
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GridViewHotLeads_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetHotLeadSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddHotLeadSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will return the sort column index
        /// </summary>
        /// <returns></returns>
        private int GetWarmLeadSortColumnIndex()
        {
            foreach (DataControlField field in GridViewWarmLeads.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.WarmLeadSortExpression))
                {
                    return GridViewWarmLeads.Columns.IndexOf(field);
                }
            }
            return -1;
        }

        /// <summary>
        /// This method will return the sort column index
        /// </summary>
        /// <returns></returns>
        private int GetHotLeadSortColumnIndex()
        {
            foreach (DataControlField field in GridViewHotLeads.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.HotLeadSortExpression))
                {
                    return GridViewHotLeads.Columns.IndexOf(field);
                }
            }
            return -1;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GridViewHotLeads_OnRowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    string toFirstName = string.Empty;
                    string toLastName = string.Empty;

                    Label brand = args.Row.FindControl("propertyBrandLabel") as Label;
                    brand.Text = ((HotLead)(args.Row.DataItem)).BrandName;

                    Label facilityName = args.Row.FindControl("facilityNameLabel") as Label;
                    facilityName.Text = ((HotLead)(args.Row.DataItem)).FacilityName;

                    Label propertyAddressOneLabel = args.Row.FindControl("propertyAddressOneLabel") as Label;
                    if (!string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).Address))
                        propertyAddressOneLabel.Text = ((HotLead)(args.Row.DataItem)).Address;

                    Label propertyAddressTwoLabel = args.Row.FindControl("propertyAddressTwoLabel") as Label;
                    if (!string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).Address2))
                    {
                        propertyAddressTwoLabel.Visible = true;
                        propertyAddressTwoLabel.Text = ((HotLead)(args.Row.DataItem)).Address2 + "<br />";
                    }

                    Label propertyCityLabel = args.Row.FindControl("propertyCityLabel") as Label;
                    propertyCityLabel.Text = ((HotLead)(args.Row.DataItem)).CityName;

                    Label propertyStateLabel = args.Row.FindControl("propertyStateLabel") as Label;
                    propertyStateLabel.Text = ((HotLead)(args.Row.DataItem)).StateAbbreviation;

                    Label zipCodeLabel = args.Row.FindControl("zipCodeLabel") as Label;
                    if (!string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).ZipCode))
                        zipCodeLabel.Text = " " + ((HotLead)(args.Row.DataItem)).ZipCode;

                    Label countryLabel = args.Row.FindControl("propertyCountryLabel") as Label;
                    if (!string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).CountryName))
                        countryLabel.Text = ((HotLead)(args.Row.DataItem)).CountryName;

                    string address = ((HotLead)(args.Row.DataItem)).BrandName.Trim() + " " + ((HotLead)(args.Row.DataItem)).FacilityName.Trim() + " " + ((HotLead)(args.Row.DataItem)).Address.Trim() + " ";
                    address += !string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).Address2) ? ((HotLead)(args.Row.DataItem)).Address2.Trim() + " " : " ";
                    address += !string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).CityName) ? ((HotLead)(args.Row.DataItem)).CityName.Trim() + " " : " ";
                    address += !string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).StateAbbreviation) ? ((HotLead)(args.Row.DataItem)).StateAbbreviation.Trim() + " " : " ";
                    address += !string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).ZipCode) ? ((HotLead)(args.Row.DataItem)).ZipCode.Trim() + " " : string.Empty;
                    address += !string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).CountryName) ? ((HotLead)(args.Row.DataItem)).CountryName.Trim() : string.Empty;

                    string[] projectManagerName = ((HotLead)(args.Row.DataItem)).ProjectManager.Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
                    if (projectManagerName.Count() == 2)
                    {
                        toFirstName = projectManagerName[0];
                        toLastName = projectManagerName[1];
                    }
                    else if (projectManagerName.Count() == 1)
                    {
                        toFirstName = projectManagerName[0];
                        toLastName = string.Empty;
                    }
                    HyperLink contactRepHyperLink = args.Row.FindControl("ContactRepHyperLink") as HyperLink;
                    Label ContactRepLabel = args.Row.FindControl("ContactRepLabel") as Label;
                    if (string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).RepEmailAddress))
                    {
                        contactRepHyperLink.Visible = false;
                    }

                    if (string.IsNullOrWhiteSpace(((HotLead)(args.Row.DataItem)).RepContactDate))
                    {
                        ContactRepLabel.Visible = false;
                        contactRepHyperLink.NavigateUrl = "~/Projects/ContactOwner.aspx?keepThis=true&ce=" + ((HotLead)(args.Row.DataItem)).RepEmailAddress + "&ad=" + Server.UrlEncode(address) + "&ca=" + Server.UrlEncode(((HotLead)(args.Row.DataItem)).CategoryDisplayName) + "&b=" + ((HotLead)(args.Row.DataItem)).BrandId + "&r=" + ((HotLead)(args.Row.DataItem)).RegionId + "&p=" + ((HotLead)(args.Row.DataItem)).PropertyTypeId + "&f=" + Server.UrlEncode(toFirstName) + "&l=" + Server.UrlEncode(toLastName) + "&pj=" + ((HotLead)(args.Row.DataItem)).ProjectId + "&c=" + ((HotLead)(args.Row.DataItem)).CategoryId + "&pr=" + ((HotLead)(args.Row.DataItem)).ProductId + "&TB_iframe=true&height=440&width=780";
                    }
                    else
                    {
                        contactRepHyperLink.Visible = false;
                        ContactRepLabel.Text = ResourceUtility.GetLocalizedString(619, culture, "REP CONTACTED").ToUpper() + "<br>" + Convert.ToDateTime(((HotLead)(args.Row.DataItem)).RepContactDate).ToString("dd-MMM-yyyy");
                    }
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void DownloadSpreadSheet_ClickLinkButton(object sender, EventArgs args)
        {
            try
            {
                int partnerId = 0;
                User user = GetSession<User>(SessionConstants.User);
                if (user != null && !string.IsNullOrEmpty(user.Company))
                {
                    partnerId = Convert.ToInt32(user.Company);
                }
                DataSet constReportDataSet = _partnerManager.GetConstructionReportDataSet(partnerId);

                ExportToExcel(constReportDataSet);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Exports Construction Reports Data into Excel Format
        /// </summary>
        /// <param name="constReportDataSet"></param>
        private void ExportToExcel(DataSet constReportDataSet)
        {
            try
            {
                string attach = UIConstants.ConstructionReportExcelFileName;
                IList<ReportColumns> reportColumnCollection = null;

                Response.ClearContent();
                Response.AddHeader("content-disposition", attach);
                Response.ContentType = UIConstants.ContentType;
                reportColumnCollection = _helperManager.GetReportColumnDetails((int)ReportEnum.ConstructionReport);

                StringBuilder constReportStringBuilder = new StringBuilder();
                constReportStringBuilder.Append("<html><head><title></title></head><body>");
                constReportStringBuilder.AppendFormat("<table border = '1' cellpadding = '2'>");
                constReportStringBuilder.Append("<tr style = 'width: 100%'>");
                constReportStringBuilder.AppendFormat("<td colspan = '{0}'><b>Projects in Design and Under Construction</b></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.AppendFormat("<tr><td colspan = '{0}'></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.AppendFormat("<tr><td colspan = '{1}'>Report Date: {0}</td></tr>", DateTime.Now.ToLongDateString(), reportColumnCollection.Count);
                constReportStringBuilder.AppendFormat("<tr><td colspan = '{0}'></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.Append("<tr>");

                foreach (ReportColumns reportColumn in reportColumnCollection)
                {
                    constReportStringBuilder.AppendFormat("<td style ='width: {1}px; background-color: {2};vertical-align: {3};text-align: {4};height: {5};color: {7}';display:{6}><b>{0}</b></td>"
                                                    , reportColumn.ColumnName, reportColumn.Width, reportColumn.HeaderBgColor, reportColumn.HeaderVAlign, reportColumn.HeaderHAlign, (reportColumn.AutoFit) ? "100%" : reportColumn.Height + "px",
                                                     reportColumn.Visible, reportColumn.HeaderTextColor);
                }

                constReportStringBuilder.Append("</tr>");

                if (constReportDataSet.Tables != null)
                {
                    foreach (DataRow reportRow in constReportDataSet.Tables[0].Rows)
                    {
                        constReportStringBuilder.Append("<tr>");

                        for (int i = 0; i < constReportDataSet.Tables[0].Columns.Count; i++)
                        {
                            ReportColumns reportColumn = reportColumnCollection[i];
                            constReportStringBuilder.AppendFormat("<td style ='width: {1}px; background-color: {2};vertical-align: {3};text-align: {4};height: {5}';display:{6}>{0}</td>",
                                reportRow[i], reportColumn.Width, reportColumn.CellColor, reportColumn.VAlign, reportColumn.HAlign, (reportColumn.AutoFit) ? "100%" : reportColumn.Height + "px",
                                 reportColumn.Visible);

                        }

                        constReportStringBuilder.Append("</tr>");
                    }
                }
                constReportStringBuilder.Append("</table></body></html>");

                Response.Write(constReportStringBuilder.ToString());

                Response.End();
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GridViewWarmLeads_OnRowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    string toFirstName = string.Empty;
                    string toLastName = string.Empty;

                    Label brand = args.Row.FindControl("propertyBrandLabel") as Label;
                    brand.Text = ((WarmLead)(args.Row.DataItem)).BrandName;

                    Label facilityName = args.Row.FindControl("facilityNameLabel") as Label;
                    facilityName.Text = ((WarmLead)(args.Row.DataItem)).FacilityName;

                    Label propertyAddressOneLabel = args.Row.FindControl("propertyAddressOneLabel") as Label;
                    if (!string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).Address))
                        propertyAddressOneLabel.Text = ((WarmLead)(args.Row.DataItem)).Address;

                    Label propertyAddressTwoLabel = args.Row.FindControl("propertyAddressTwoLabel") as Label;
                    if (!string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).Address2))
                    {
                        propertyAddressTwoLabel.Visible = true;
                        propertyAddressTwoLabel.Text = ((WarmLead)(args.Row.DataItem)).Address2 + "<br />";
                    }

                    Label propertyCityLabel = args.Row.FindControl("propertyCityLabel") as Label;
                    propertyCityLabel.Text = ((WarmLead)(args.Row.DataItem)).CityName;

                    Label propertyStateLabel = args.Row.FindControl("propertyStateLabel") as Label;
                    propertyStateLabel.Text = ((WarmLead)(args.Row.DataItem)).StateAbbreviation;

                    Label zipCodeLabel = args.Row.FindControl("zipCodeLabel") as Label;
                    if (!string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).ZipCode))
                        zipCodeLabel.Text = ((WarmLead)(args.Row.DataItem)).ZipCode;

                    Label countryLabel = args.Row.FindControl("propertyCountryLabel") as Label;
                    if (!string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).CountryName))
                        countryLabel.Text = ((WarmLead)(args.Row.DataItem)).CountryName;

                    string address = ((WarmLead)(args.Row.DataItem)).BrandName.Trim() + " " + ((WarmLead)(args.Row.DataItem)).FacilityName.Trim() + " " + ((WarmLead)(args.Row.DataItem)).Address.Trim() + " ";
                    address += !string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).Address2) ? ((WarmLead)(args.Row.DataItem)).Address2.Trim() + " " : " ";
                    address += !string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).CityName) ? ((WarmLead)(args.Row.DataItem)).CityName.Trim() + ", " : " ";
                    address += !string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).StateAbbreviation) ? ((WarmLead)(args.Row.DataItem)).StateAbbreviation.Trim() + " " : " ";
                    address += !string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).ZipCode) ? ((WarmLead)(args.Row.DataItem)).ZipCode.Trim() + " " : string.Empty;
                    address += !string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).CountryName) ? ((WarmLead)(args.Row.DataItem)).CountryName.Trim() : string.Empty;

                    string[] projectManagerName = ((WarmLead)(args.Row.DataItem)).ProjectManager.Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
                    if (projectManagerName.Count() == 2)
                    {
                        toFirstName = projectManagerName[0];
                        toLastName = projectManagerName[1];
                    }
                    else if (projectManagerName.Count() == 1)
                    {
                        toFirstName = projectManagerName[0];
                        toLastName = string.Empty;
                    }
                    HyperLink contactRepHyperLink = args.Row.FindControl("ContactRepHyperLink") as HyperLink;
                    Label ContactRepLabel = args.Row.FindControl("ContactRepLabel") as Label;

                    if (string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).RepEmailAddress))
                    {
                        contactRepHyperLink.Visible = false;
                    }
                    if (string.IsNullOrWhiteSpace(((WarmLead)(args.Row.DataItem)).RepContactDate))
                    {
                        ContactRepLabel.Visible = false;
                       contactRepHyperLink.NavigateUrl = "~/Projects/ContactOwner.aspx?keepThis=true&ce=" + ((WarmLead)(args.Row.DataItem)).RepEmailAddress + "&ad=" + Server.UrlEncode(address) + "&ca=" + Server.UrlEncode(((WarmLead)(args.Row.DataItem)).CategoryDisplayName) + "&b=" + ((WarmLead)(args.Row.DataItem)).BrandId + "&r=" + ((WarmLead)(args.Row.DataItem)).RegionId + "&p=" + ((WarmLead)(args.Row.DataItem)).PropertyTypeId + "&f=" + Server.UrlEncode(toFirstName) + "&l=" + Server.UrlEncode(toLastName) + "&pj=" + ((WarmLead)(args.Row.DataItem)).ProjectId + "&c=" + ((WarmLead)(args.Row.DataItem)).CategoryId + "&pr=" + ((WarmLead)(args.Row.DataItem)).ProductId + "&TB_iframe=true&height=440&width=780";
                    }
                    else
                    {
                        contactRepHyperLink.Visible = false;
                        ContactRepLabel.Text = ResourceUtility.GetLocalizedString(619, culture, "REP CONTACTED").ToUpper() + "<br>" + Convert.ToDateTime(((WarmLead)(args.Row.DataItem)).RepContactDate).ToString("dd-MMM-yyyy");
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}