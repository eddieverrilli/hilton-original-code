﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class EmailAlert : PageBase
    {
        private IMessageManager _messageManager;
        private Partner partner = new Partner();

        private IPartnerManager _partnerManager;
        private User currentUser;

        /// <summary>
        /// Invoked on Init
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _partnerManager = PartnerManager.Instance;
                _messageManager = MessageManager.Instance;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Bind the data to control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                currentUser = (User)GetSession<User>(SessionConstants.User);
                if (currentUser != null)
                {
                    informationUserNameLabel.Text = currentUser.FirstName + " " + currentUser.LastName;
                    feedbackEmailLinkButton.Text = currentUser.Email;

                    if (!Page.IsPostBack)
                    {
                        string contactEmail = Convert.ToString(Request.QueryString["contactEmail"], CultureInfo.InvariantCulture);
                        contactEmailLabel.Text = contactEmail;
                        string contactId = Convert.ToString(Request.QueryString["contactId"], CultureInfo.InvariantCulture);
                        contactIdHiddenValue.Value = contactId;
                        string contactTypeId = Convert.ToString(Request.QueryString["contactTypeId"], CultureInfo.InvariantCulture);
                        contactTypeIdHiddenValue.Value = contactTypeId;
                        string regionId = Convert.ToString(Request.QueryString["regionId"], CultureInfo.InvariantCulture);
                        regionIdHiddenValue.Value = regionId;
                        string toFirstName = Convert.ToString(Request.QueryString["toFirstName"], CultureInfo.InvariantCulture);
                        firstNameHiddenValue.Value = toFirstName;
                        string toLastName = Convert.ToString(Request.QueryString["toLastName"], CultureInfo.InvariantCulture);
                        lastNameHiddenValue.Value = toLastName;

                        partnerIdFromQueryStringHidden.Value = Convert.ToString(Request.QueryString["partnerId"], CultureInfo.InvariantCulture);
                        PopulateDropDownData(Convert.ToInt32(partnerIdFromQueryStringHidden.Value, CultureInfo.InvariantCulture));
                    }
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "showConfirmationMessage", "Close()", true);
                }
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Populate Drop Down Data
        /// </summary>
        /// <param name="partnerId"></param>
        private void PopulateDropDownData(int partnerId)
        {
            IList<SearchFilters> searchFilters = _partnerManager.GetPartnerSearchFilterItems(partnerId);
            BindPageDropDown(searchFilters, DropDownConstants.ContactDropDown, contactDropDownList, ResourceUtility.GetLocalizedString(1248, culture, "Choose a Contact"), Convert.ToInt32(regionIdHiddenValue.Value, CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// BindPageDropDown
        /// </summary>
        /// <param name="searchFiltersData"></param>
        /// <param name="dropDownType"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        /// <param name="selectedValue"></param>
        private void BindPageDropDown(IList<SearchFilters> searchFiltersData, string dropDownType, DropDownList sourceDropDown, string defaultText, int? selectedValue)
        {
            sourceDropDown.DataSource = Helper.GetSearchFilterDropDownData(dropDownType, searchFiltersData).Where(p => (string.IsNullOrWhiteSpace(p.DataTextField) == false));
            sourceDropDown.DataTextField = "DataTextField";
            sourceDropDown.DataValueField = "DataValueField";
            sourceDropDown.DataBind();
            if (selectedValue.HasValue)
                sourceDropDown.SelectedValue = selectedValue.ToString();
        }

        /// <summary>
        /// Drop Down Change Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownChange(object sender, EventArgs e)
        {
            try
            {
                partner = GetSession<Partner>(SessionConstants.PartnerProfile);
                Contact contactInfo1 = partner.Contacts.Where(p => p.RegionId == Convert.ToInt32(contactDropDownList.SelectedValue, CultureInfo.InvariantCulture)).FirstOrDefault();

                contactEmailLabel.Text = contactInfo1.Email;
                regionIdHiddenValue.Value = contactInfo1.RegionId.ToString();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Email Send Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Send_Click(object sender, EventArgs e)
        {
            try
            {
                currentUser = (User)GetSession<User>(SessionConstants.User);
                if (currentUser != null)
                {
                    Message message = new Message();
                    message.SenderId = currentUser.UserId;
                    message.ToFirstName = firstNameHiddenValue.Value;
                    message.ToLastName = lastNameHiddenValue.Value;
                    message.PartnerId = partnerIdFromQueryStringHidden.Value;

                    if (sendCopyToYouCheckBox.Checked)
                    {
                        message.IsCopyToSelf = true;
                    }
                    else
                    {
                        message.IsCopyToSelf = false;
                    }
                    message.ToEmail = contactEmailLabel.Text;
                    message.RegionId = Convert.ToInt32(regionIdHiddenValue.Value, CultureInfo.InvariantCulture);
                    message.Details = commentsTextBox.Text;
                    message.SenderType = "O";
                    message.StatusId = (int)MessageStatusEnum.New;
                    message.BrandId = Convert.ToInt32(contactDropDownList.SelectedValue, CultureInfo.InvariantCulture);
                    message.TemplateId = 2;
                    message.culture = Convert.ToString(culture, CultureInfo.InvariantCulture);
                    if (_messageManager.InsertMessage(message))
                    {
                        Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "showConfirmationMessage", "ShowConfirm()", true);
                    }
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SessionExpired, culture));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}