﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;
using System.Web.Services;
namespace Hilton.SuppliersConnection.UI
{
    public partial class RecommendedPartners : PageBase
    {
        private IPartnerManager _partnerManager;

        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedBrands_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                //DropDownList lastCategoryDropDown;
                //if (parentCategoryContainer.Controls.Count > 0)
                //{
                //    lastCategoryDropDown = (DropDownList)FindControl(parentCategoryContainer, lastSelectedCategoryDropDown.Value);
                //    IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData); var brands = (selectedBrands.Text == null || selectedBrands.Text == DropDownConstants.DefaultValue) ? null : selectedBrands.Text.Split(',').Select(item => int.Parse(item));
                //    IList<SearchFilters> filteredCollection = searchFiltersData.Where(item => (brands == null ? true : brands.Contains(item.BrandId))).ToList<SearchFilters>();
                //    string selectedCategoryId = categoriesDropDown.SelectedValue;
                //    BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), Convert.ToInt32(categoriesDropDown.SelectedValue));

                //    ProcessCategories();
                //}
                //else
                //{
                   IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData); var brands = (selectedBrands.Text == null || selectedBrands.Text == DropDownConstants.DefaultValue) ? null : selectedBrands.Text.Split(',').Select(item => int.Parse(item));
                   IList<SearchFilters> filteredCollection = searchFiltersData.Where(item => (brands == null ? true : brands.Contains(item.BrandId))).ToList<SearchFilters>();

                    var selectedCategoryIds = filteredCollection.Select(p => p.CatId).ToList();

                    IList<Category> categoriesInViewState = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
                    IList<Category> categories = (from Q in categoriesInViewState
                                                  where selectedCategoryIds.Contains(Q.CategoryId)
                                                  select Q).ToList();
                    MakeParentCategoriesMenuItems(categories);
                    ScriptManager.RegisterClientScriptBlock(dropDownUpdatePanel, this.GetType(), "resetMenu", "resetMenu()", true);
                    //BindDropDown(filteredCollection, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(1209, culture, "All Categories"), 0);
                //}
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedRegions_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                string[] regions = selectedRegions.Text.Split(',');
                BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, string.Empty, 0, true, regions);
                if (categoryMenu.Controls.Count == 0)
                {
                    IList<Category> categoriesInViewState = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
                    MakeParentCategoriesMenuItems(categoriesInViewState);
                    ScriptManager.RegisterClientScriptBlock(dropDownUpdatePanel, this.GetType(), "resetMenu", "resetMenu()", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                dropDownUpdatePanel.Update();
            }
        }

        protected void SpecialCategoryTxtBox_TextChanged(object sender, EventArgs e)
        {
            IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
            IList<SearchFilters> brandsCollection = filteredHierarchyCBRTData.Where(item => item.CatId == Convert.ToInt32(leafCategoryHdn.Value)).ToList<SearchFilters>();
            brandCollection.Value = string.Join(",", brandsCollection.Select(p => p.BrandId).Distinct());
            if (leafCategoryHdn.Value == "0")
            {
                IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
                brandCollection.Value = string.Join(",", brands.Select(p => p.DataValueField).Distinct());
                selectedBrands.Text = brandCollection.Value;
            } 
        }

        /// <summary>
        ///  Register for post backs
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            brands.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(optgroup, p.DataValueField)));
            base.Render(writer);
        }


      

        /// <summary>
        /// To bind the data to category drop down
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            try
            {
                categoryDropDown.DataSource = categoryList;
                categoryDropDown.DataTextField = "CategoryName";
                categoryDropDown.DataValueField = "CategoryId";
                categoryDropDown.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Drop down selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void CategoriesDropDown_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        OnSelectedIndexChanged(sender, e);
        //    }
        //    catch (Exception exception)
        //    {
        //        UserInterfaceExceptionHandler.HandleExcetion(ref exception);
        //        ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        //    }
        //}

        /// <summary>
        /// To create dynamic drop downs
        /// </summary>
        /// <param name="ID"></param>
        //protected void CreateDropDownList(string ID)
        //{
        //    try
        //    {
        //        DropDownList parentCategoryDropDown = new DropDownList();
        //        parentCategoryDropDown.ID = ID;
        //        parentCategoryDropDown.AutoPostBack = true;
        //        parentCategoryDropDown.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);
        //        parentCategoryContainer.Controls.Add(parentCategoryDropDown);
        //    }
        //    catch (Exception exception)
        //    {
        //        UserInterfaceExceptionHandler.HandleExcetion(ref exception);
        //        ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        //    }
        //}

        /// <summary>
        /// To populate the Search Criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoButton_Click(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrWhiteSpace(selectedBrands.Text))
                {
                    recommendedPartnersGridView.Visible = false;
                    ConfigureResultMessage(searchResultDiv, "message error", "Please select brand");
                    updPnlPartner.Update();
                }
                else if (string.IsNullOrWhiteSpace(selectedRegions.Text))
                {
                    recommendedPartnersGridView.Visible = false;
                    ConfigureResultMessage(searchResultDiv, "message error", "Please select region");
                    updPnlPartner.Update();
                }
                else if (string.IsNullOrWhiteSpace(selectedCountries.Text))
                {
                    recommendedPartnersGridView.Visible = false;
                    ConfigureResultMessage(searchResultDiv, "message error", "Please select country");
                    updPnlPartner.Update();
                }
                else
                {
                    recommendedPartnersGridView.Visible = true;
                    PartnerSearch searchCriteria = new PartnerSearch();
                    searchCriteria.CategoryId = leafCategoryHdn.Value;
                    searchCriteria.BrandId = selectedBrands.Text;
                    searchCriteria.BrandCollection = brandCollection.Value;
                    searchCriteria.CountryId = selectedCountries.Text;
                    searchCriteria.RegionId = selectedRegions.Text;
                    searchCriteria.RegionCollection = regionCollection.Value;

                    searchCriteria.PageIndex = 0;
                    searchCriteria.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                    searchCriteria.SortExpression = UIConstants.CompanyName; ;
                    searchCriteria.SortDirection = UIConstants.AscAbbreviation;
                    SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                    SetViewState(ViewStateConstants.SortExpression, UIConstants.CompanyName);

                    //Save criteria in session for back button functionality
                    SetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch, searchCriteria);

                    //Clear Hilton worldwide details session on go click
                    ClearSession(SessionConstants.HiltonWorldwideData);

                    int totalRecordCount = 0;
                    IList<PartnerSearchResult> partnerSearchResult = _partnerManager.GetPartnerSearchResult(searchCriteria, ref totalRecordCount);
                    recommendedPartnersGridView.DataSource = partnerSearchResult;
                    recommendedPartnersGridView.DataBind();

                    if (totalRecordCount > 0)
                    {
                        divPaging.Visible = true;
                        prevLinkButton.Enabled = false;
                        startPage.Text = "1";
                        searchCriteria.StartPage = startPage.Text;
                        endPage.Text = (Math.Ceiling((Double)totalRecordCount / searchCriteria.PageSize)).ToString(CultureInfo.InvariantCulture);
                        searchCriteria.EndPage = endPage.Text;
                        if (Convert.ToInt16(endPage.Text, CultureInfo.InvariantCulture) == 1)
                        {
                            nextLinkButton.Enabled = false;
                        }
                        else
                            nextLinkButton.Enabled = true;
                    }
                    else
                    {
                        ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoPartnersFoundForCriteria, culture));
                    }
                    updPnlPartner.Update();

                    //string allLevelCategories = string.Empty;
                    //allLevelCategories = categoriesDropDown.SelectedValue;
                    //for (int i = 1; i < parentCategoryContainer.Controls.Count + 1; i++)
                    //{
                    //    DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, "categoriesDropDown-" + (i + 1).ToString(CultureInfo.InvariantCulture));
                    //    allLevelCategories += "~" + newDropDown.SelectedValue;
                    //}
                    //SetSession<string>(SessionConstants.AllCascadeCategoryIds, allLevelCategories);
                    ScriptManager.RegisterClientScriptBlock(dropDownUpdatePanel, this.GetType(), "setCategoryTextBoxVal", "setCategoryTextBoxVal()", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To go to next page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void nextLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                divPaging.Visible = true;
                PartnerSearch searchCriteria = GetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch);
                searchCriteria.PageIndex += 1;
                prevLinkButton.Enabled = true;
                int totalRecordCount = 0;
                IList<PartnerSearchResult> partnerSearchResult = _partnerManager.GetPartnerSearchResult(searchCriteria, ref totalRecordCount);
                recommendedPartnersGridView.DataSource = partnerSearchResult;
                recommendedPartnersGridView.DataBind();
                if (Convert.ToInt16(endPage.Text, CultureInfo.InvariantCulture) == searchCriteria.PageIndex + 1)
                {
                    nextLinkButton.Enabled = false;
                }
                startPage.Text = (searchCriteria.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                SetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch, searchCriteria);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on OnInit
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs args)
        {
            try
            {
                base.OnInit(args);
                _partnerManager = PartnerManager.Instance;
                bool isBecomePartnerAccessible = ValidateMenuAuthorization((int)MenuEnum.BecomeRPartner);
                if (!isBecomePartnerAccessible)
                    becomePartnerListItem.Visible = false;
                else
                    becomePartnerListItem.Visible = true;
                this.Title = ResourceUtility.GetLocalizedString(82, culture, "Browse Recommended Partners");
                SetGridViewHeaderText();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// On drop down Selected Index Changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);

        //        DropDownList ddl = (DropDownList)sender;
        //        string id = ddl.ID;
        //        int existingDropDownCount = FindOccurence("categoriesDropDown");

        //        if (!id.Contains("-"))
        //        {
        //            id = id + "-1";
        //        }
        //        string[] idSequence = id.Split('-');

        //        string currentSelectedCategoryId = ddl.SelectedValue.ToString(CultureInfo.InvariantCulture);

        //        for (int dropdownCount = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1; dropdownCount <= existingDropDownCount + 1; dropdownCount++)
        //        {
        //            DropDownList rowDivision = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
        //            parentCategoryContainer.Controls.Remove(rowDivision);
        //        }

        //        if (currentSelectedCategoryId != "0")
        //        {
        //            Category currentSelectedCategory = ReadCategory(currentSelectedCategoryId);
        //            if (!(currentSelectedCategory.IsLeaf))
        //            {
        //                IList<Category> childCategoryList = GetChildCategories(currentSelectedCategoryId);
        //                if (childCategoryList.Count() > 0)
        //                {
        //                    int nextID = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1;

        //                    CreateDropDownList("categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
        //                    FindPopulateDropDowns(currentSelectedCategoryId, "categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
        //                }
        //            }
        //        }

        //        if (ddl.SelectedIndex == 0)
        //        {
        //            if (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) > 1)
        //            {
        //                int previousSequence = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) - 1;
        //                DropDownList ddlLast;
        //                if (previousSequence > 1)
        //                {
        //                    ddlLast = (DropDownList)dropDownUpdatePanel.FindControl(idSequence[0] + "-" + (previousSequence).ToString(CultureInfo.InvariantCulture));
        //                }
        //                else
        //                {
        //                    ddlLast = (DropDownList)dropDownUpdatePanel.FindControl("categoriesDropDown");
        //                }

        //                categoryHdn.Value = ddlLast.SelectedValue;
        //                leafCategoryHdn.Value = ddlLast.SelectedValue;
        //                lastSelectedCategoryDropDown.Value = ddlLast.ID;
        //                IList<SearchFilters> brandsCollection = filteredHierarchyCBRTData.Where(item => item.CatId == Convert.ToInt32(ddlLast.SelectedValue)).ToList<SearchFilters>();
        //                brandCollection.Value = string.Join(",", brandsCollection.Select(p => p.BrandId).Distinct());
        //            }
        //            else
        //            {
        //                categoryHdn.Value = "0";
        //                leafCategoryHdn.Value = "0";
        //                lastSelectedCategoryDropDown.Value = "categoriesDropDown";
        //                IList<SearchFilters> filterSearchData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
        //                var brandId = Helper.GetSearchFilterDropDownData(DropDownConstants.BrandDropDown, filterSearchData).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false);
        //                brandCollection.Value = string.Join(",", brandId.ToList().Select(p => p.DataValueField));
        //                selectedBrands.Text = string.Empty;

        //            }
        //        }
        //        else
        //        {
        //            categoryHdn.Value = ddl.SelectedValue;
        //            leafCategoryHdn.Value = ddl.SelectedValue;
        //            lastSelectedCategoryDropDown.Value = ddl.ID;
        //            IList<SearchFilters> brandsCollection = filteredHierarchyCBRTData.Where(item => item.CatId == Convert.ToInt32(ddl.SelectedValue)).ToList<SearchFilters>();
        //            brandCollection.Value = string.Join(",", brandsCollection.Select(p => p.BrandId).Distinct());
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        UserInterfaceExceptionHandler.HandleExcetion(ref exception);
        //        ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        //    }
        //}

        /// <summary
        /// Invoked on Init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                //if (parentCategoryContainer == null)
                //    parentCategoryContainer = new PlaceHolder();
                //RecreateControls("categoriesDropDown", "DropDownList");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                divPaging.Visible = false;
                searchResultDiv.Attributes.Remove("class");
                searchResultDiv.InnerText = string.Empty;

                if (!Master.Page.ClientScript.IsStartupScriptRegistered("alert"))
                {
                    Master.Page.ClientScript.RegisterStartupScript
                        (this.GetType(), "alert", "more_lessItems();", true);
                }

                if (!Page.IsPostBack)
                {
                    User user = GetSession<User>(SessionConstants.User);
                    if (user == null)
                        isLogOnUserHiddenField.Value = "0";
                    else
                        userTypeHiddenField.Value = Convert.ToString(user.UserTypeId);

                    rightPanelHeader.Text = ResourceUtility.GetLocalizedString(3, culture, "Recommended Partners").ToUpper();

                    PopulateDropDownData();
                    IList<Category> categoryItems = _partnerManager.GetRecommendedVendorsActiveCategories;
                    SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categoryItems);
                    MakeParentCategoriesMenuItems(categoryItems);

                    HiltonWorldwide hiltonWorldWidedata = GetSession<HiltonWorldwide>(SessionConstants.HiltonWorldwideData);
                    if (hiltonWorldWidedata != null)
                    {
                        SetHiltonWorldwideFilters(hiltonWorldWidedata);
                    }

                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("PartnerProfile.aspx")))
                    {
                        MaintainPageState();
                    }
                    else
                    {
                        ClearSession(SessionConstants.FilterCriteria);
                        ClearSession(SessionConstants.AllCascadeCategoryIds);
                        ClearSession(SessionConstants.ProductImageBytes);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(updPnlPartner, this.GetType(), "moreLesswords", "more_lessText()", true);
                    ScriptManager.RegisterClientScriptBlock(updPnlPartner, this.GetType(), "moreLess", "more_lessItems()", true);
                    ScriptManager.RegisterClientScriptBlock(updPnlPartner, this.GetType(), "ForceCall", "forceCall();", true);
                    ScriptManager.RegisterClientScriptBlock(updPnlPartner, this.GetType(), "forceHover", "forceHover();", true);
                    ScriptManager.RegisterClientScriptBlock(dropDownUpdatePanel, this.GetType(), "getItems", "getItems();", true);
                    ScriptManager.RegisterClientScriptBlock(dropDownUpdatePanel, this.GetType(), "SetRegionAndCountryDropDown", "SetRegionAndCountryDropDown();", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To set filters
        /// </summary>
        /// <param name="hiltonWorldWidedata"></param>
        private void SetHiltonWorldwideFilters(HiltonWorldwide hiltonWorldWidedata)
        {
            if (!string.IsNullOrEmpty(hiltonWorldWidedata.BrandId))
            {
                selectedBrands.Text = hiltonWorldWidedata.BrandId;
                brandCollection.Value = hiltonWorldWidedata.BrandCollection;
            }

        }

        /// <summary>
        /// To maintain the page state
        /// </summary>
        private void MaintainPageState()
        {
            PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch);
            if (partnerSearch != null)
            {
                // PolulateSearchCriteria(partnerSearch);
                SearchPartners(partnerSearch);
            }
        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="partnerSearch"></param>
        private void SetDropDownsSelectedValue(PartnerSearch partnerSearch)
        {
            selectedBrands.Text = partnerSearch.BrandId;
            brandCollection.Value = partnerSearch.BrandCollection;

            selectedRegions.Text = partnerSearch.RegionId;
            regionCollection.Value = partnerSearch.RegionCollection;

            selectedCountries.Text = partnerSearch.CountryId;
            //if (!string.IsNullOrEmpty(partnerSearch.PropertyTypeId) && Convert.ToInt16(partnerSearch.PropertyTypeId) > 0)
            //{
            //    propertyTypeDropDown.SelectedValue = partnerSearch.PropertyTypeId;
            //}
        }

        /// <summary>
        /// To Populate categories cascading
        /// </summary>
        //private void PopulateCascadeCategories()
        //{
        //    string allCascadeCategoryIds = GetSession<string>(SessionConstants.AllCascadeCategoryIds);
        //    if (!String.IsNullOrEmpty(allCascadeCategoryIds))
        //    {
        //        string[] temp = allCascadeCategoryIds.Split('~');
        //        for (int i = 0; i < temp.Length; i++)
        //        {
        //            if (i == 0)
        //            {
        //                if (Convert.ToInt16(temp[i]) > 0)
        //                {
        //                    categoriesDropDown.SelectedValue = temp[i];
        //                    categoryHdn.Value = temp[i];
        //                }
        //            }
        //            if (temp[i] != "0")
        //            {
        //                Category currentSelectedCategory = ReadCategory(temp[i]);
        //                if (!(currentSelectedCategory.IsLeaf))
        //                {
        //                    IList<Category> childCategoryList = GetChildCategories(temp[i]);
        //                    if (childCategoryList.Count() > 0)
        //                    {
        //                        int nextID = Convert.ToInt32(i + 1, CultureInfo.InvariantCulture) + 1;

        //                        CreateDropDownList("categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
        //                        FindPopulateDropDowns(temp[i], "categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
        //                        DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, "categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
        //                        if (i + 1 != temp.Length)
        //                        {
        //                            if (Convert.ToInt16(temp[i + 1]) > 0)
        //                            {
        //                                newDropDown.SelectedValue = temp[i + 1];
        //                                leafCategoryHdn.Value = temp[i + 1];
        //                                lastSelectedCategoryDropDown.Value = newDropDown.ID;
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// To search the Partners
        /// </summary>
        /// <param name="partnerSearch"></param>
        private void SearchPartners(PartnerSearch partnerSearch)
        {
            int totalRecordCount = 0;
            IList<PartnerSearchResult> partnerSearchResult = _partnerManager.GetPartnerSearchResult(partnerSearch, ref totalRecordCount);
            recommendedPartnersGridView.DataSource = partnerSearchResult;
            recommendedPartnersGridView.DataBind();

            if (totalRecordCount > 0)
            {
                if (partnerSearch.PageIndex != 0)
                {
                    divPaging.Visible = true;
                    prevLinkButton.Enabled = true;
                    if (Convert.ToInt16(partnerSearch.EndPage, CultureInfo.InvariantCulture) == (partnerSearch.PageIndex + 1))
                    {
                        nextLinkButton.Enabled = false;
                    }
                    startPage.Text = (partnerSearch.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                    endPage.Text = (partnerSearch.EndPage).ToString(CultureInfo.InvariantCulture);
                }
                else if (partnerSearch.PageIndex == 0)
                {
                    divPaging.Visible = true;
                    prevLinkButton.Enabled = false;
                    startPage.Text = "1";
                    endPage.Text = (Math.Ceiling((Double)totalRecordCount / partnerSearch.PageSize)).ToString(CultureInfo.InvariantCulture);
                    if (Convert.ToInt16(endPage.Text, CultureInfo.InvariantCulture) == 1)
                    {
                        nextLinkButton.Enabled = false;
                    }
                    else
                        nextLinkButton.Enabled = true;
                }
            }

            ////Save criteria in session for back button functionality
            SetSession<PartnerSearch>(SessionConstants.PartnerSearch, partnerSearch);
        }

        /// <summary>
        /// To go back to previous page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void prevLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                divPaging.Visible = true;
                PartnerSearch searchCriteria = GetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch);
                searchCriteria.PageIndex -= 1;
                nextLinkButton.Enabled = true;
                int totalRecordCount = 0;
                IList<PartnerSearchResult> partnerSearchResult = _partnerManager.GetPartnerSearchResult(searchCriteria, ref totalRecordCount);
                recommendedPartnersGridView.DataSource = partnerSearchResult;
                recommendedPartnersGridView.DataBind();
                startPage.Text = (searchCriteria.PageIndex + 1).ToString(CultureInfo.InvariantCulture);
                if (Convert.ToInt16(startPage.Text, CultureInfo.InvariantCulture) == 1)
                {
                    prevLinkButton.Enabled = false;
                }
                SetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch, searchCriteria);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


        /// <summary>
        /// Row Data Bound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RecommendedPartners_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow gr = e.Row;
                    PartnerSearchResult result = (PartnerSearchResult)e.Row.DataItem;

                    if (isLogOnUserHiddenField.Value != "0")
                    {
                        if (result.Partner.CompanyLogoImage != null)
                        {
                            Image logoCompany = new Image();
                            logoCompany = (Image)(gr.FindControl("partnerImg"));
                            logoCompany.Visible = false;

                            ImageButton logoCompanyImageButton = new ImageButton();
                            logoCompanyImageButton = (ImageButton)(gr.FindControl("partnerImgButton"));
                            logoCompanyImageButton.Visible = true;
                            logoCompanyImageButton.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(result.Partner.CompanyLogoImage);
                            logoCompanyImageButton.CommandName = "viewPartnerProfile";
                            logoCompanyImageButton.CommandArgument = result.PartnerId.ToString();
                        }
                        else
                        {
                            Image logoCompany = new Image();
                            logoCompany = (Image)(gr.FindControl("partnerImg"));
                            logoCompany.ImageUrl = "../Images/NoProduct.PNG";
                            logoCompany.Visible = false;
                        }

                        Label vendorLabel = new Label();
                        vendorLabel = (Label)(gr.FindControl("vendorLabel"));
                        vendorLabel.Visible = false;

                        LinkButton vendorLinkButton = new LinkButton();
                        vendorLinkButton = (LinkButton)(gr.FindControl("vendorLinkButton"));
                        vendorLinkButton.Visible = true;
                        vendorLinkButton.CommandName = "viewPartnerProfile";
                        vendorLinkButton.CommandArgument = result.PartnerId.ToString();
                    }
                    else
                    {
                        if (result.Partner.CompanyLogoImage != null)
                        {
                            Image logoCompany = new Image();
                            logoCompany = (Image)(gr.FindControl("partnerImg"));
                            logoCompany.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(result.Partner.CompanyLogoImage);
                        }
                        else
                        {
                            Image logoCompany = new Image();
                            logoCompany = (Image)(gr.FindControl("partnerImg"));
                            logoCompany.ImageUrl = "../Images/NoProduct.PNG";
                            logoCompany.Visible = false;
                        }

                        LinkButton vendorLinkButton = new LinkButton();
                        vendorLinkButton = (LinkButton)(gr.FindControl("vendorLinkButton"));
                        vendorLinkButton.Visible = false;
                    }

                    Image imgSam = new Image();
                    imgSam = (Image)(gr.FindControl("samImg"));

                    if (isLogOnUserHiddenField.Value != "0" && userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Owner)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Administrator)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Corporate)))
                    {
                        HyperLink hprlnkComment = new HyperLink();
                        hprlnkComment = (HyperLink)(gr.FindControl("hprlnkComment"));
                        hprlnkComment.NavigateUrl = "~/Partners/SubmitFeedback.aspx?keepThis=true&partnerName=" + HttpUtility.UrlEncode(result.Partner.CompanyName) + "&partnerId=" + result.PartnerId + "&toFirstName=" + result.Contact.FirstName + "&toLastName=" + result.Contact.LastName + "&toEmail=" + result.Contact.Email + "&TB_iframe=true&height=440&width=800";
                    }
                    else
                    {
                        HyperLink hprlnkComment = new HyperLink();
                        hprlnkComment = (HyperLink)(gr.FindControl("hprlnkComment"));
                        hprlnkComment.Visible = false;
                    }

                    if (result.Partner.IsSAM)
                    {
                        imgSam.Visible = true;
                        imgSam.ToolTip = ResourceUtility.GetLocalizedString(1082, culture, "SAM Partner");
                    }
                    else imgSam.Visible = false;

                    Repeater repeaterRegion = new Repeater();
                    repeaterRegion = (Repeater)(gr.FindControl("repeaterRegions"));

                    repeaterRegion.DataSource = result.Region.OrderBy(x => x.RegionDescription);
                    repeaterRegion.DataBind();

                    Repeater repeaterCountry = new Repeater();
                    repeaterCountry = (Repeater)(gr.FindControl("repeaterCountries"));

                    repeaterCountry.DataSource = result.Country.OrderBy(x => x.CountryName);
                    repeaterCountry.DataBind();

                    Repeater repeaterBrand = new Repeater();
                    repeaterBrand = (Repeater)(gr.FindControl("repeaterBrands"));

                    repeaterBrand.DataSource = result.Brands;
                    repeaterBrand.DataBind();

                    Repeater repeaterCategories = new Repeater();
                    repeaterCategories = (Repeater)(gr.FindControl("repeaterCategories"));
                    repeaterCategories.DataSource = result.Categories.OrderBy(x => x.CategoryName);
                    repeaterCategories.DataBind();

                    Literal contactLtl = new Literal();
                    contactLtl = (Literal)(gr.FindControl("contactLtl"));
                    contactLtl.Text = "<span>" + result.Contact.FirstName + "  " + result.Contact.LastName + @"</span>" +
                                      @"<br \><span>" + result.Contact.Title + @"</span>" +
                        (string.IsNullOrWhiteSpace(result.Contact.Email) ? string.Empty : @"<br \><span><br/> <a href='mailto:" + result.Contact.Email + "'>" + result.Contact.Email + "</a>" + @"</span>") +
                          (string.IsNullOrWhiteSpace(result.Contact.Phone) ? string.Empty : @"<br \><span>" + result.Contact.Phone + @"</span><br>");

                    HtmlAnchor webSiteLink = (HtmlAnchor)e.Row.FindControl("webSiteLink");

                    webSiteLink.InnerText = (string.IsNullOrWhiteSpace(result.Partner.WebSiteLink) ? string.Empty : result.Partner.WebSiteLink);
                    webSiteLink.HRef = (string.IsNullOrWhiteSpace(result.Partner.WebSiteLink) ? string.Empty : result.Partner.WebSiteLink);

                    if (result.Partner.WebSiteLink.Contains("http://") || result.Partner.WebSiteLink.Contains("https://"))
                        webSiteLink.HRef = result.Partner.WebSiteLink;
                    else
                        webSiteLink.HRef = "http://" + result.Partner.WebSiteLink;





                    if (isLogOnUserHiddenField.Value == "0")
                    {
                        LinkButton viewProfileProducts = (LinkButton)gr.FindControl("lnkViewProfileProducts");
                        viewProfileProducts.Visible = false;
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Row Command event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RecommendedPartners_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "viewPartnerProfile")
                {
                    SetSession<string>(SessionConstants.PartnerId, e.CommandArgument.ToString());
                    Response.Redirect("~/Partners/PartnerProfile.aspx");
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void RecommendedPartners_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void RecommendedPartners_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Process Categories
        /// </summary>
        /// <param name="totalCategoryDropDownCount"></param>
        //private void ProcessCategories(int totalCategoryDropDownCount = 0)
        //{
        //    try
        //    {
        //        int existingDropDownCount;
        //        if (totalCategoryDropDownCount == 0)
        //            existingDropDownCount = FindOccurence("categoriesDropDown");
        //        else
        //            existingDropDownCount = totalCategoryDropDownCount;

        //        IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);

        //        string parentSelectedValue;
        //        DropDownList parentCategoryDropDown;
        //        for (int dropdownCount = 1; dropdownCount <= existingDropDownCount; dropdownCount++)
        //        {
        //            DropDownList subCategoryDropDown = null;
        //            if (dropdownCount == 1)
        //            {
        //                parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown");
        //                parentSelectedValue = parentCategoryDropDown.SelectedValue;
        //                if (existingDropDownCount == 1)
        //                    subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount + 1).ToString(CultureInfo.InvariantCulture));
        //            }
        //            else if (dropdownCount == 2)
        //            {
        //                parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown");
        //                parentSelectedValue = parentCategoryDropDown.SelectedValue;
        //                subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
        //            }
        //            else
        //            {
        //                parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount - 1).ToString(CultureInfo.InvariantCulture));
        //                parentSelectedValue = parentCategoryDropDown.SelectedValue;
        //                subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
        //            }

        //            if (subCategoryDropDown != null)
        //            {
        //                string selectedValue = subCategoryDropDown.SelectedValue;
        //                if (existingDropDownCount == 1)
        //                    FindPopulateDropDowns(parentSelectedValue, "categoriesDropDown-" + (dropdownCount + 1).ToString(CultureInfo.InvariantCulture));
        //                else
        //                    FindPopulateDropDowns(parentSelectedValue, "categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
        //                if (Convert.ToInt16(selectedValue) > 0)
        //                    subCategoryDropDown.SelectedValue = selectedValue;

        //                IList<ListItem> dropDownDirtyListItem = new List<ListItem>();
        //                var brands = (selectedBrands.Text == null || selectedBrands.Text == DropDownConstants.DefaultValue) ? null : selectedBrands.Text.Split(',').Select(item => int.Parse(item));

        //                foreach (ListItem dropDownListItem in subCategoryDropDown.Items)
        //                {
        //                    if (Convert.ToInt16(dropDownListItem.Value) > 0)
        //                    {
        //                        int? categorySelectedValue = Convert.ToInt32(dropDownListItem.Value);
        //                        IList<SearchFilters> filteredCollection = filteredHierarchyCBRTData.Where(item =>
        //                                                                                              (brands == null ? true : brands.Contains(item.BrandId))
        //                                                                                            && ((categorySelectedValue == null || categorySelectedValue == 0) ? true : item.CatId.Equals(categorySelectedValue))).ToList<SearchFilters>();

        //                        if (filteredCollection.Count == 0)
        //                            dropDownDirtyListItem.Add(dropDownListItem);
        //                    }
        //                }

        //                if (dropDownDirtyListItem.Count > 0)
        //                    dropDownDirtyListItem.ToList().ForEach(x => subCategoryDropDown.Items.Remove(x));
        //            }
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        UserInterfaceExceptionHandler.HandleExcetion(ref exception);
        //        ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
        //    }
        //}

        /// <summary>
        /// To find the occurrance
        /// </summary>
        /// <param name="substr"></param>
        /// <returns></returns>
        private int FindOccurence(string substr)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int count = 0;
            for (int i = 0; i < ctrls.Length; i++)
            {
                if (ctrls[i].Contains(substr + "-") && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// To find populated drop downs
        /// </summary>
        /// <param name="selectedCategoryId"></param>
        /// <param name="id"></param>
        //private void FindPopulateDropDowns(string selectedCategoryId, string id)
        //{
        //    DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, id);
        //    PopulateDropDown(newDropDown, selectedCategoryId);
        //}

        /// <summary>
        /// To get the child categories
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

            IList<Category> categoryList = (categories.AsEnumerable().
                                           Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture))).ToList());
            return categoryList;
        }

        /// <summary>
        /// To get root level categories
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetRootLevelCategories()
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            IList<Category> rootCategoryList = (categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
            return rootCategoryList;
        }

        /// <summary>
        /// To populate Drop down
        /// </summary>
        /// <param name="parentCategoryDropDown"></param>
        /// <param name="selectedValue"></param>
        private void PopulateDropDown(DropDownList categoryDropDown, string selectedValue)
        {
            categoryDropDown.Items.Clear();
            IList<Category> categoryList = new List<Category>();
            if (String.IsNullOrEmpty(selectedValue))
            {
                categoryList = GetRootLevelCategories();
            }
            else if (selectedValue != "0")
            {
                categoryList = GetChildCategories(selectedValue);
            }
            if (categoryList != null)
            {
                BindData(categoryDropDown, categoryList);
            }
            categoryDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(223, culture), "0"));
        }

        /// <summary>
        /// To Populate Drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            CategoriesFilterHierarchy searchFilters = _partnerManager.GetSearchFilterDataForVendor();
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilters.FilteredCBRTData);
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData, searchFilters.FilteredHierarchyCBRTData);


            IList<Category> categoryItems = _partnerManager.GetRecommendedVendorsActiveCategories;
            SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categoryItems);

            //BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.CategoriesDropDown, categoriesDropDown, ResourceUtility.GetLocalizedString(209, culture), null);

            var brandId = Helper.GetSearchFilterDropDownData(DropDownConstants.BrandDropDown, searchFilters.FilteredCBRTData).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false);
            brandCollection.Value = string.Join(",", brandId.ToList().Select(p => p.DataValueField));

            SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categoryItems);
            //categoriesDropDown.DataSource = categoryItems.Where(x => x.IsLeaf == false && x.ParentCategoryId == -1).OrderBy(x => x.CategoryName);
            //categoriesDropDown.DataTextField = "CategoryName";
            //categoriesDropDown.DataValueField = "CategoryId";
            //categoriesDropDown.DataBind();
            //categoriesDropDown.Items.Insert(0, new ListItem { Text = ResourceUtility.GetLocalizedString(209, culture), Value = "0", });

            BindStaticDropDown(DropDownConstants.RegionDropDown, regionDropDown, string.Empty, 0);
            BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, string.Empty, 0);


        }

        //[System.Web.Services.WebMethod]
        //public static void ChangeBrand_OnCategoryChange(int catIDHid)
        //{
        //    PageBase objPageBase = new PageBase();
        //    IList<SearchFilters> filteredHierarchyCBRTData = objPageBase.GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
        //    IList<SearchFilters> brandsCollection = filteredHierarchyCBRTData.Where(item => item.CatId == Convert.ToInt32(catIDHid)).ToList<SearchFilters>();
        //        //    brandCollection.Value = string.Join(",", brandsCollection.Select(p => p.BrandId).Distinct());
            
        //}

        /// <summary>
        /// To make cascaded dropdown list
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        public void MakeParentCategoriesMenuItems(IList<Category> categoryItems)
        {
            try
            {
                categoryItems.Where(x =>  x.ParentCategoryId == -1).OrderBy(x => x.CategoryName);
                categoryMenu.Controls.Clear();
                if (categoryMenu != null)
                {
                    categoryMenu.Controls.Add(SpecialCategoryTxtBox);

                    HtmlGenericControl ulParent = new HtmlGenericControl("ul");
                    ulParent.Attributes.Add("class", "nav closedMenu");
                    categoryMenu.Controls.Add(ulParent);

                    if (categoryTextBoxHdn.Value != "All Categories" && categoryTextBoxHdn.Value != "")
                    {
                        String strSelectAllCat = ResourceUtility.GetLocalizedString(209, culture);
                        HtmlGenericControl liAllCat = new HtmlGenericControl("li");
                        liAllCat.Attributes.Add("data-level", "1");
                        liAllCat.Attributes.Add("class", "dropdown1");
                        ulParent.Controls.Add(liAllCat);
                        HtmlGenericControl anchorAllCat = new HtmlGenericControl("a");
                        anchorAllCat.Attributes.Add("href", "#");
                        anchorAllCat.InnerText = strSelectAllCat;
                        liAllCat.Controls.Add(anchorAllCat);
                    }                

                    foreach (Category parentCategory in categoryItems.Where(x =>  x.ParentCategoryId == -1).OrderBy(x => x.CategoryName))
                    {
                        String parentCategoryID = parentCategory.CategoryId.ToString();
                        HtmlGenericControl liParent = new HtmlGenericControl("li");
                        liParent.Attributes.Add("data-level", "1");
                        liParent.Attributes.Add("value", parentCategory.CategoryId.ToString());
                        if (!(parentCategory.IsLeaf))
                        {
                            liParent.Attributes.Add("class", "dropdown");
                        }
                        if (parentCategory.IsLeaf && parentCategory.ParentCategoryId == -1)
                        {
                            liParent.Attributes.Add("class", "dropdown1");
                        }
                        ulParent.Controls.Add(liParent);

                        HtmlGenericControl anchor = new HtmlGenericControl("a");
                        anchor.Attributes.Add("href", "#");
                        anchor.InnerText = parentCategory.CategoryName;
                        liParent.Controls.Add(anchor);

                        if (!(parentCategory.IsLeaf))
                        {
                            MakeFirstChildCategoriesMenuItems(parentCategoryID, liParent, 2);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

       

        public void MakeFirstChildCategoriesMenuItems(string parentCategoryID, HtmlGenericControl liParent, int DataLevel)
        {
            try
            {
                IList<Category> childCategoryList = GetChildCategories(parentCategoryID);
                if (childCategoryList.Count() > 0)
                {
                    HtmlGenericControl ulChild = new HtmlGenericControl("ul");
                    liParent.Controls.Add(ulChild);

                    HtmlGenericControl liAllCat = new HtmlGenericControl("li");
                    liAllCat.Attributes.Add("data-level", "2");
                    liAllCat.Attributes.Add("value", parentCategoryID);
                    ulChild.Controls.Add(liAllCat);
                    HtmlGenericControl anchorAllCat = new HtmlGenericControl("a");
                    anchorAllCat.Attributes.Add("href", "#");
                    anchorAllCat.InnerText = ResourceUtility.GetLocalizedString(223, culture); ;
                    liAllCat.Controls.Add(anchorAllCat);

                    foreach (Category childCategory in childCategoryList)
                    {
                        HtmlGenericControl liChild = new HtmlGenericControl("li");
                        liChild.Attributes.Add("data-level", DataLevel.ToString());
                        liChild.Attributes.Add("value", childCategory.CategoryId.ToString());
                        if (!(childCategory.IsLeaf))
                        {
                            liChild.Attributes.Add("class", "dropdown");
                        }
                        ulChild.Controls.Add(liChild);

                        HtmlGenericControl anchorChild = new HtmlGenericControl("a");
                        anchorChild.Attributes.Add("href", "#");
                        anchorChild.InnerText = childCategory.CategoryName;
                        liChild.Controls.Add(anchorChild);

                        if (!(childCategory.IsLeaf))
                        {
                            MakeSecondChildCategoriesMenuItems(childCategory.CategoryId.ToString(), liChild, 3);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        public void MakeSecondChildCategoriesMenuItems(string parentCategoryID, HtmlGenericControl liParent, int DataLevel)
        {
            try
            {
                IList<Category> childCategoryList = GetChildCategories(parentCategoryID);
                if (childCategoryList.Count() > 0)
                {
                    HtmlGenericControl ulChild = new HtmlGenericControl("ul");
                    liParent.Controls.Add(ulChild);

                    HtmlGenericControl liAllCat = new HtmlGenericControl("li");
                    liAllCat.Attributes.Add("data-level", "2");
                    liAllCat.Attributes.Add("value", parentCategoryID);
                    ulChild.Controls.Add(liAllCat);
                    HtmlGenericControl anchorAllCat = new HtmlGenericControl("a");
                    anchorAllCat.Attributes.Add("href", "#");
                    anchorAllCat.InnerText = ResourceUtility.GetLocalizedString(223, culture); ;
                    liAllCat.Controls.Add(anchorAllCat);

                    foreach (Category childCategory in childCategoryList)
                    {
                        HtmlGenericControl liChild = new HtmlGenericControl("li");
                        liChild.Attributes.Add("data-level", DataLevel.ToString());
                        liChild.Attributes.Add("value", childCategory.CategoryId.ToString());
                        //if (!(childCategory.IsLeaf))
                        //{
                        //    liChild.Attributes.Add("class", "dropdown");
                        //}
                        ulChild.Controls.Add(liChild);

                        HtmlGenericControl anchorChild = new HtmlGenericControl("a");
                        anchorChild.Attributes.Add("href", "#");
                        anchorChild.InnerText = childCategory.CategoryName;
                        liChild.Controls.Add(anchorChild);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(searchResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To get categories from View state
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(string currentSelectedCategoryId)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            Category category = (Category)(categories.Where(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)).Single());
            return category;
        }

        

        /// <summary>
        /// To set header text from content block
        /// </summary>
        private void SetGridViewHeaderText()
        {
            recommendedPartnersGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(492, culture, "Partner Name").ReplaceEmptyString();
            recommendedPartnersGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(277, culture, "Categories");
            recommendedPartnersGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(606, culture, "Regions");
            recommendedPartnersGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(307, culture, "Countries");
            recommendedPartnersGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(267, culture, "Brands");
            // recommendedPartnersGridView.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(575, culture, "Property Types");
            recommendedPartnersGridView.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(320, culture, "Contact");
        }

        /// <summary>
        /// This method will return the sort column index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in recommendedPartnersGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return recommendedPartnersGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch);
                    if (field.SortExpression == partnerSearch.SortExpression)
                    {
                        SetViewState<string>(ViewStateConstants.SortExpression, partnerSearch.SortExpression);
                        if (partnerSearch.SortDirection == UIConstants.AscAbbreviation)
                            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                        else
                            SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                        return recommendedPartnersGridView.Columns.IndexOf(field);
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch);
            partnerSearch.SortExpression = sortExpression;
            partnerSearch.SortDirection = sortDirection;
            partnerSearch.PageIndex = 0;
            SearchPartners(partnerSearch);
        }
        [WebMethod]
        public static List<Search> SearchAutoComplete(string name_startsWith)
        {
            PageBase basepageobject = new PageBase();
            IList<Search> searchList = null;
            searchList = (IList<Search>)basepageobject.GetSession(ViewStateConstants.MultiSearch);
            if (searchList == null)
            {
                searchList = PartnerManager.SearchAutoComplete(name_startsWith);
                basepageobject.SetSession<IList<Search>>(ViewStateConstants.MultiSearch, searchList);
            }
            else
            {
                searchList = (IList<Search>)basepageobject.GetSession(ViewStateConstants.MultiSearch);
            }

            return searchList.Where(a => a.name.ToUpper().Contains(name_startsWith.ToUpper())).ToList();

        }
        protected void btn_Click(object sender, EventArgs e)
        {
            recommendedPartnersGridView.Visible = true;
            string test = log.InnerHtml.ToString();
            string category = string.Empty, brands = string.Empty, country = string.Empty, region = string.Empty, propertyType = string.Empty, partners = string.Empty, primaryContact = string.Empty, secondaryContact = string.Empty, regionContact = string.Empty, brandSegment = string.Empty, states = string.Empty;
            if (!string.IsNullOrEmpty(hdCountry.Value))
            {
                string[] abc = hdCountry.Value.Split(new Char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in abc)
                {
                    if (item.Contains("Categories"))
                    {
                        category = item.Substring(item.IndexOf(",,")).ToString();
                    }
                    if (item.Contains("Brands"))
                    {
                        brands = item.Substring(item.IndexOf(",,")).ToString();
                    }
                    if (item.Contains("Countries"))
                    {
                        country = item.Substring(item.IndexOf(",,")).ToString();
                    }
                    if (item.Contains("Regions"))
                    {
                        region = item.Substring(item.IndexOf(",,")).ToString();
                    }

                    if (item.Contains("Partners"))
                    {
                        partners = item.Substring(item.IndexOf(",,")).ToString();
                    }
                    if (item.Contains("PrimaryContacts"))
                    {
                        primaryContact = item.Substring(item.IndexOf(",,")).ToString();
                    }
                    else if (item.Contains("SecondaryContacts"))
                    {
                        secondaryContact = item.Substring(item.IndexOf(",,")).ToString();
                    }
                    if (item.Contains("RegionalContacts"))
                    {
                        regionContact = item.Substring(item.IndexOf(",,")).ToString();
                    }
                    if (item.Contains("BrandSegments"))
                    {
                        brandSegment = item.Substring(item.IndexOf(",,")).ToString();
                    }
                    if (item.Contains("States"))
                    {
                        states = item.Substring(item.IndexOf(",,")).ToString();
                    }

                }
            }
            if (category.Length == 0)
            {
                category = Convert.ToString(0);
            }
            if (brands.Length == 0)
            {
                brands = Convert.ToString(0);
            }
            if (country.Length == 0)
            {
                country = Convert.ToString(0);
            }
            if (region.Length == 0)
            {
                region = Convert.ToString(0);
            }

            if (partners.Length == 0)
            {
                partners = Convert.ToString(0);
            }
            if (primaryContact.Length == 0)
            {
                primaryContact = Convert.ToString(0);
            }
            else if (secondaryContact.Length == 0)
            {
                secondaryContact = Convert.ToString(0);
            }
            if (regionContact.Length == 0)
            {
                regionContact = Convert.ToString(0);
            }
            if (brandSegment.Length == 0)
            {
                brandSegment = Convert.ToString(0);
            }
            if (states.Length == 0)
            {
                states = Convert.ToString(0);
            }
            PartnerSearch searchCriteria = new PartnerSearch();
            searchCriteria.CategoryId = category.ToString();
            searchCriteria.BrandId = brands.ToString();
            searchCriteria.CountryId = country.ToString();
            // searchCriteria.PropertyTypeId = propertyType.ToString();
            searchCriteria.RegionId = region.ToString();
            searchCriteria.PartnerId = partners.ToString();
            searchCriteria.primary_contactId = primaryContact.ToString();
            searchCriteria.secondary_contactId = secondaryContact.ToString();
            searchCriteria.regional_contactId = regionContact.ToString();
            searchCriteria.brandSegmentId = brandSegment.ToString();
            searchCriteria.states = states.ToString();
            searchCriteria.PageIndex = 0;
            searchCriteria.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            searchCriteria.SortExpression = UIConstants.CompanyName; ;
            searchCriteria.SortDirection = UIConstants.AscAbbreviation;
            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
            SetViewState(ViewStateConstants.SortExpression, UIConstants.CompanyName);

            //Save criteria in session for back button functionality
            SetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch, searchCriteria);

            //Clear Hilton worldwide details session on go click
            ClearSession(SessionConstants.HiltonWorldwideData);

            int totalRecordCount = 0;
            IList<PartnerSearchResult> partnerSearchResult = _partnerManager.GetPartnerSearchResult(searchCriteria, ref totalRecordCount);
            recommendedPartnersGridView.DataSource = partnerSearchResult;
            recommendedPartnersGridView.DataBind();

            if (totalRecordCount > 0)
            {
                divPaging.Visible = true;
                prevLinkButton.Enabled = false;
                startPage.Text = "1";
                searchCriteria.StartPage = startPage.Text;
                endPage.Text = (Math.Ceiling((Double)totalRecordCount / searchCriteria.PageSize)).ToString(CultureInfo.InvariantCulture);
                searchCriteria.EndPage = endPage.Text;
                if (Convert.ToInt16(endPage.Text, CultureInfo.InvariantCulture) == 1)
                {
                    nextLinkButton.Enabled = false;
                }
                else
                    nextLinkButton.Enabled = true;
            }
            else
            {
                ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoPartnersFoundForCriteria, culture));
            }
            updPnlPartner.Update();

            //string allLevelCategories = string.Empty;
            //allLevelCategories = categoriesDropDown.SelectedValue;
            //for (int i = 1; i < parentCategoryContainer.Controls.Count + 1; i++)
            //{
            //    DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, "categoriesDropDown-" + (i + 1).ToString(CultureInfo.InvariantCulture));
            //    allLevelCategories += "~" + newDropDown.SelectedValue;
            //}
            //SetSession<string>(SessionConstants.AllCascadeCategoryIds, allLevelCategories);
        }



      
    }
}