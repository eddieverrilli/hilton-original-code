﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="PendingCheckPayment.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.PendingCheckPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
    <h2>
        <asp:Label ID="userNameLabel" Text="" EnableViewState="true" runat="server"></asp:Label>
    </h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="recommendedPartnersHyperLink" CBID="3" Text="Recommended Partners"
            NavigateUrl="~/Partners/RecommendedPartners.aspx" runat="server"></asp:HyperLink>
        <span>/</span> <span>
            <asp:Label ID="myAccountLabel" runat="server" CBID="96" Text="My Account"></asp:Label>
        </span>
    </div>
    <div class="main">
        <div class="check-payment">
            <div class="box1">
                <p>
                    <asp:Label ID="pageHeaderLabel" runat="server" CBID="1072" Text="PAYMENT FORM"></asp:Label></p>
                <h2>
                    <asp:Label ID="pageTitleLabel" runat="server" CBID="89" Text="Pending Check Payment"></asp:Label></h2>
                <p>
                    <asp:Label ID="pageBodyTopLabel" runat="server" Text="You've chosen to send payment via check."
                        CBID="44"></asp:Label></p>
                <h3>
                    Mail a check for
                    <asp:Label ID="amountLabel" runat="server"></asp:Label>
                    to:</h3>
                <p>
                    <asp:Label ID="processingCenterAddressLabel" runat="server" CBID="1071" Text="Sherri Litano, Manager of Vendor Relations,Hilton Worldwide,755 Crossover Lane, Building A, Memphis, TN 38117"></asp:Label>
                </p>
                <p>
                    <asp:Label ID="pageBodyBottomLabel" runat="server" CBID="1073" Text="Once Hilton Worldwide receives and processes your check, your account will be activated."></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="sidePanelHeaderLabel" runat="server" CBID="949" Text="Recommended Partners"></asp:Label>
            </h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="activateHyperLink" runat="server" class="active" CBID="1080" Text="ACTIVATE"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="receiptHyperLink" runat="server" CBID="1081" Text="RECEIPT"></asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>