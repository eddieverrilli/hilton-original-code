﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;
using System.Reflection;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ApplicationDetails : PageBase
    {
        private IPartnerManager _partnerManager;
        private PartnerApplication _partnerApplication = new PartnerApplication();

        /// <summary>
        /// Invoked on Init
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                if (GetSession<User>(SessionConstants.User) != null)
                {
                    ClearSession(SessionConstants.ApplicationDetails);
                    ClearSession(SessionConstants.FilterCriteria);
                    ClearSession(SessionConstants.AppliedOpportunityDetails);
                    ClearSession(SessionConstants.EditPartnerApplication);
                    ClearSession(SessionConstants.AllCascadeCategoryIds);
                    ClearSession(SessionConstants.ProductImageBytes);
                    Response.Redirect("~/");
                }
                _partnerManager = PartnerManager.Instance;
                this.Title = ResourceUtility.GetLocalizedString(85, culture, "Apply to Become a Partner");
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    var applicationDetails = GetSession<PartnerApplication>(SessionConstants.ApplicationDetails);
                    if (applicationDetails != null)
                    {
                        var appliedOpportunities = GetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails);
                        if (appliedOpportunities != null)
                        {
                            var updatedAppliedOpportunities = appliedOpportunities.AppliedParntershipOpportunities;
                            var oldAppliedOpportunities = applicationDetails.AppliedParntershipOpportunities;
                            updatedAppliedOpportunities.ToList().ForEach(p =>
                            {
                                var oldOpportunities = oldAppliedOpportunities.ToList().FirstOrDefault(d => d.CountryId == p.CountryId && d.BrandId==p.BrandId);
                                if (oldOpportunities == null)
                                {
                                    applicationDetails.AppliedParntershipOpportunities.Add(new AppliedPartnershipOpportunity() { RegionId = p.RegionId,CountryId=p.CountryId,BrandId=p.BrandId,
                                    BrandName=p.BrandName,CountryName=p.CountryName,RegionName=p.RegionName });
                                }
                            });

                            oldAppliedOpportunities.ToList().ForEach(j =>
                                {
                                    if (!updatedAppliedOpportunities.Any(v => v.CountryId == j.CountryId && v.BrandId==j.BrandId))
                                    {
                                        applicationDetails.AppliedParntershipOpportunities.Remove(j);
                                    }
                                }
                            );

                            var oldContact = applicationDetails.Contacts;

                            updatedAppliedOpportunities.ToList().ForEach(p =>
                            {
                                var oldSessionContacts = oldContact.Where(x => x.ContactTypeId == (int)ContactTypeEnum.Regional).ToList().FirstOrDefault(d => d.CountryId == p.CountryId);
                                if (oldSessionContacts == null)
                                {
                                    applicationDetails.Contacts.Add(new Contact() { RegionName = p.RegionName, RegionId = p.RegionId, CountryId=p.CountryId,CountryName=p.CountryName, ContactTypeId = (int)ContactTypeEnum.Regional });
                                }
                            });

                            oldContact.Where(x => x.ContactTypeId == (int)ContactTypeEnum.Regional).ToList().ForEach(y =>
                            {
                                if (!updatedAppliedOpportunities.Any(t => t.CountryId == y.CountryId))
                                {
                                    applicationDetails.Contacts.Remove(y);
                                }
                            });
                        }
                        _partnerApplication = applicationDetails;

                        regularPartnerRadioButton.Checked = true;

                        if (_partnerApplication.PartnershipType == (int)PartnershipTypeEnum.GoldPartner)
                        {
                            regularPartnerRadioButton.Checked = false;
                            goldLevelPartnerRadioButton.Checked = true;
                        }
                        else if (_partnerApplication.PartnershipType == (int)PartnershipTypeEnum.Partner)
                        {
                            goldLevelPartnerRadioButton.Checked = false;
                            regularPartnerRadioButton.Checked = true;
                        }

                        PopulateDropDownData();
                        PopulatePartnershipAmount();

                        partnerIdHiddenField.Value = Convert.ToString(_partnerApplication.PartnerId, CultureInfo.InvariantCulture);
                        companyNameTextBox.Text = _partnerApplication.CompanyName;
                        companyAddressLine1TextBox.Text = _partnerApplication.CompanyAddress1;
                        companyAddressLine2TextBox.Text = _partnerApplication.CompanyAddress2;
                        countryDropDownList.SelectedValue = _partnerApplication.Country;
                        cityTextBox.Text = _partnerApplication.City;
                        stateDropDownList.SelectedValue = _partnerApplication.State;
                        zipCodeTextBox.Text = _partnerApplication.ZipCode;
                        if (_partnerApplication.Country != DropDownConstants.UnitedStates)
                        {
                            stateDropDownList.Enabled = false;
                            stateRequiredFieldValidator.Enabled = false;
                            zipCodeTextBox.ReadOnly = false;
                            zipCodeTextBox.Enabled = true;

                            zipCodeTextBox.Attributes.Remove("class");
                            zipCodeTextBox.Attributes.Add("class", "input textbox3 small readonly");
                            zipCodeRequiredFieldValidator.Enabled = false;
                            //zipCodeRangeValidator.Enabled = false;
                            zipCodeRegularExpressionValidator.Enabled = false;
                        }
                        else
                        {
                            stateDropDownList.Enabled = true;
                            zipCodeTextBox.ReadOnly = false;
                            zipCodeTextBox.Enabled = true;
                            zipCodeTextBox.Attributes.Remove("class");
                            zipCodeTextBox.Attributes.Add("class", "input textbox3 small mandatory");
                            zipCodeRequiredFieldValidator.Enabled = true;
                            //zipCodeRangeValidator.Enabled = true;
                            zipCodeRegularExpressionValidator.Enabled = true;
                            stateRequiredFieldValidator.Enabled = true;
                        }
                        companyDescriptionTextBox.Text = _partnerApplication.CompanyDescription;
                        companyLogoImage.ImageUrl = _partnerApplication.CompanyLogoImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(_partnerApplication.CompanyLogoImage) : "../Images/NoProduct.png";
                        webSiteTextBox.Text = _partnerApplication.WebSiteLink;
                        imageLinkButton.Text = _partnerApplication.CompanyLogoName;
                        imageNameHiddenField.Value = _partnerApplication.CompanyLogoName;
                        if (!string.IsNullOrEmpty(_partnerApplication.CompanyLogoName))
                            removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "inline");

                        Contact primaryContactInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId == (int)ContactTypeEnum.Primary).FirstOrDefault();
                        Contact secondaryContactInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId == (int)ContactTypeEnum.Secondary).FirstOrDefault();
                        IEnumerable<Contact> regionalContactsInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId != (int)ContactTypeEnum.Primary && p.ContactTypeId != (int)ContactTypeEnum.Secondary);
                        //var regions = (from region in _partnerApplication.AppliedParntershipOpportunities
                        //               select new { region.CountryId, region.CountryName, region.RegionId, region.RegionName }).Distinct();
                        //IList<Contact> regionalContactsInfo = new List<Contact>();
                        //foreach (var contact in regions)
                        //{
                        //    regionalContactsInfo.Add(new Contact()
                        //    {
                        //        RegionId = contact.RegionId,
                        //        RegionName = contact.RegionName,
                        //        CountryId = contact.CountryId,
                        //        CountryName = contact.CountryName
                        //    });
                        //}
                        //Primary contact
                        primaryContactIdHiddenField.Value = primaryContactInfo.ContactId;
                        firstNamePrimaryContactTextBox.Text = primaryContactInfo.FirstName;
                        lastNamePrimaryContactTextBox.Text = primaryContactInfo.LastName;
                        titlePrimaryContactTextBox.Text = primaryContactInfo.Title;
                        phonePrimaryContactTextBox.Text = primaryContactInfo.Phone;
                        faxPrimaryContactTextBox.Text = primaryContactInfo.Fax;
                        emailAddressPrimaryContactTextBox.Text = primaryContactInfo.Email;

                        //Secondary Contact
                        //if (secondaryContactInfo != null)
                        //{
                        secondaryContactIdHiddenField.Value = secondaryContactInfo.ContactId;
                        firstNameSecondaryContactTextBox.Text = secondaryContactInfo.FirstName;
                        lastNameSecondaryContactTextBox.Text = secondaryContactInfo.LastName;
                        titleSecondaryContactTextBox.Text = secondaryContactInfo.Title;
                        phoneSecondaryContactTextBox.Text = secondaryContactInfo.Phone;
                        faxSecondaryContactTextBox.Text = secondaryContactInfo.Fax;
                        emailAddressSecondaryContactTextBox.Text = secondaryContactInfo.Email;
                        //}

                        //Regional Contact
                      
                        regionContactRepeater.DataSource = regionalContactsInfo;
                        regionContactRepeater.DataBind();
                    }
                    else
                    {
                        _partnerApplication = GetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails);

                        PopulateDropDownData();
                        PopulatePartnershipAmount();

                        if (_partnerApplication.PartnerId == 0)
                        {
                            regularPartnerRadioButton.Checked = true;
                        }
                        else
                        {
                            if (_partnerApplication.PartnershipType == (int)PartnershipTypeEnum.GoldPartner)
                            {
                                goldLevelPartnerRadioButton.Checked = true;
                            }
                            else
                            {
                                regularPartnerRadioButton.Checked = true;
                            }
                        }

                        partnerIdHiddenField.Value = Convert.ToString(_partnerApplication.PartnerId, CultureInfo.InvariantCulture);
                        companyNameTextBox.Text = _partnerApplication.CompanyName;
                        companyAddressLine1TextBox.Text = _partnerApplication.CompanyAddress1;
                        companyAddressLine2TextBox.Text = _partnerApplication.CompanyAddress2;
                        countryDropDownList.SelectedValue = _partnerApplication.Country;
                        cityTextBox.Text = _partnerApplication.City;
                        stateDropDownList.SelectedValue = _partnerApplication.State;
                        if (countryDropDownList.SelectedValue == null || countryDropDownList.SelectedValue == DropDownConstants.DefaultValue)
                        {
                            stateDropDownList.SelectedValue = null;
                            stateDropDownList.Items.RemoveAt(0);
                            stateDropDownList.Items.Insert(0, new ListItem(" ", DropDownConstants.DefaultValue));
                            stateDropDownList.Enabled = false;
                            zipCodeTextBox.ReadOnly = true;
                            zipCodeTextBox.Text = null;
                            zipCodeTextBox.Enabled = false;
                            zipCodeTextBox.Attributes.Remove("class");
                            zipCodeTextBox.Attributes.Add("class", "input textbox3 small readonly");
                            zipCodeRequiredFieldValidator.Enabled = false;
                            stateRequiredFieldValidator.Enabled = false;
                        }
                        zipCodeTextBox.Text = _partnerApplication.ZipCode;
                        companyDescriptionTextBox.Text = _partnerApplication.CompanyDescription;
                        companyLogoImage.ImageUrl = _partnerApplication.CompanyLogoImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(_partnerApplication.CompanyLogoImage) : "../Images/NoProduct.PNG";
                        webSiteTextBox.Text = _partnerApplication.WebSiteLink;
                        imageLinkButton.Text = _partnerApplication.CompanyLogoName;
                        imageNameHiddenField.Value = _partnerApplication.CompanyLogoName;
                        if (!string.IsNullOrEmpty(_partnerApplication.CompanyLogoName))
                            removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "inline");

                        if (_partnerApplication.Contacts != null)
                        {
                            Contact primaryContactInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId == 1).FirstOrDefault();

                            primaryContactIdHiddenField.Value = primaryContactInfo.ContactId;
                            firstNamePrimaryContactTextBox.Text = primaryContactInfo.FirstName;
                            lastNamePrimaryContactTextBox.Text = primaryContactInfo.LastName;
                            titlePrimaryContactTextBox.Text = primaryContactInfo.Title;
                            phonePrimaryContactTextBox.Text = primaryContactInfo.Phone;
                            faxPrimaryContactTextBox.Text = primaryContactInfo.Fax;
                            emailAddressPrimaryContactTextBox.Text = primaryContactInfo.Email;

                            Contact secondaryContactInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId == 2).FirstOrDefault();

                            secondaryContactIdHiddenField.Value = secondaryContactInfo.ContactId;
                            firstNameSecondaryContactTextBox.Text = secondaryContactInfo.FirstName;
                            lastNameSecondaryContactTextBox.Text = secondaryContactInfo.LastName;
                            titleSecondaryContactTextBox.Text = secondaryContactInfo.Title;
                            phoneSecondaryContactTextBox.Text = secondaryContactInfo.Phone;
                            faxSecondaryContactTextBox.Text = secondaryContactInfo.Fax;
                            emailAddressSecondaryContactTextBox.Text = secondaryContactInfo.Email;

                            countryDropDownList.SelectedValue = _partnerApplication.Country;
                            stateDropDownList.SelectedValue = _partnerApplication.State;
                        }
                        var regions = (from region in _partnerApplication.AppliedParntershipOpportunities
                                       select new { region.CountryId, region.CountryName,region.RegionId,region.RegionName }).Distinct();
                        IList<Contact> regionalContacts = new List<Contact>();
                        foreach (var contact in regions)
                        {
                            regionalContacts.Add(new Contact()
                            {
                                RegionId = contact.RegionId,
                                RegionName = contact.RegionName,
                                CountryId=contact.CountryId,
                                CountryName=contact.CountryName
                            });
                        }

                        regionContactRepeater.DataSource = regionalContacts;
                        regionContactRepeater.DataBind();
                    }

                    SetRegularExpressions();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will set the validation expression for regular expression validator based on culture
        /// </summary>
        private void SetRegularExpressions()
        {
            // Regular Expression for Name
            SetValidatorRegularExpressions(firstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(titlePrimaryRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);
            SetValidatorRegularExpressions(phonePrimaryContactRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(faxPrimaryRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);

            SetValidatorRegularExpressions(firstNameSecContactRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameSecContactRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(titleSecondaryContactRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);
            SetValidatorRegularExpressions(phoneSecondaryRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(secondaryEmailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(faxSecondaryRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);

            // Regular Expression for website
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(zipCodeRegularExpressionValidator, (int)RegularExpressionTypeEnum.ZipCodeValidator);
            SetValidatorRegularExpressions(webSiteRegularExpressionValidator, (int)RegularExpressionTypeEnum.WebsiteValidator);
            
        }

        /// <summary>
        /// Invoked on ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionContactRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item != null && e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    var item = e.Item;
                    Label lbl = (Label)e.Item.FindControl("countryNameLabel");
                    TextBox phoneRegionalContactTextBox = (TextBox)item.FindControl("phoneRegionalContactTextBox");
                    TextBox faxRegionalContactTextBox = (TextBox)item.FindControl("faxRegionalContactTextBox");
                    RegularExpressionValidator phoneRegionalRegularExpressionValidator = (RegularExpressionValidator)item.FindControl("phoneRegionalRegularExpressionValidator");
                    RegularExpressionValidator faxRegionalRegularExpressionValidator = (RegularExpressionValidator)item.FindControl("faxRegionalRegularExpressionValidator");
                    if (lbl.Text.Equals("United States", StringComparison.InvariantCultureIgnoreCase))
                    {
                        phoneRegionalContactTextBox.MaxLength = 32;
                        faxRegionalContactTextBox.MaxLength = 32;
                        SetValidatorRegularExpressions(phoneRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidatorUS);
                        SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidatorUS);
                    }
                    else
                    {
                        phoneRegionalContactTextBox.MaxLength = 32;
                        faxRegionalContactTextBox.MaxLength = 32;
                        SetValidatorRegularExpressions(phoneRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
                         SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
                    }

                    RegularExpressionValidator firstNameRegContactRequiredFieldValidator = (RegularExpressionValidator)item.FindControl("firstNameRegContactRegularExpressionValidator");
                    RegularExpressionValidator lastNameRegContactRequiredFieldValidator = (RegularExpressionValidator)item.FindControl("lastNameRegContactRegularExpressionValidator");
                    RegularExpressionValidator titleRegContactRequiredFieldValidator = (RegularExpressionValidator)item.FindControl("titleRegContactRegularExpressionValidator");
                    RegularExpressionValidator regionalEmailRegularExpressionValidator = (RegularExpressionValidator)item.FindControl("regionalEmailRegularExpressionValidator");
                   

                    SetValidatorRegularExpressions(firstNameRegContactRequiredFieldValidator, (int)RegularExpressionTypeEnum.NameValidator);
                    SetValidatorRegularExpressions(lastNameRegContactRequiredFieldValidator, (int)RegularExpressionTypeEnum.NameValidator);
                    SetValidatorRegularExpressions(titleRegContactRequiredFieldValidator, (int)RegularExpressionTypeEnum.TitleValidator);
                    SetValidatorRegularExpressions(regionalEmailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
                    

                    if (_partnerApplication.Contacts != null)
                    {
                        IEnumerable<Contact> regionalContactsInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId != 1 && p.ContactTypeId != 2);

                        Label countryIdLabel = (Label)item.FindControl("countryIdLabel");
                        var contact = regionalContactsInfo.Where(p => p.CountryId == Convert.ToInt32(countryIdLabel.Text, CultureInfo.InvariantCulture)).FirstOrDefault();
                        if (contact != null)
                        {
                            CheckBox sameAsPrimaryCheckBox = (CheckBox)item.FindControl("sameAsPrimaryCheckBox");
                            HiddenField regionalContactIdHiddenField = (HiddenField)item.FindControl("regionalContactIdHiddenField");
                            TextBox firstNameRegionalContactTextBox = (TextBox)item.FindControl("firstNameRegionalContactTextBox");
                            TextBox lastNameRegionalContactTextBox = (TextBox)item.FindControl("lastNameRegionalContactTextBox");
                            TextBox titleRegionalContactTextBox = (TextBox)item.FindControl("titleRegionalContactTextBox");
                             phoneRegionalContactTextBox = (TextBox)item.FindControl("phoneRegionalContactTextBox");
                             faxRegionalContactTextBox = (TextBox)item.FindControl("faxRegionalContactTextBox");
                            TextBox emailRegionalContactTextBox = (TextBox)item.FindControl("emailRegionalContactTextBox");

                            if (contact.IsSameAsPrimary)
                            {
                                sameAsPrimaryCheckBox.Checked = true;
                                firstNameRegionalContactTextBox.Enabled = false;
                                lastNameRegionalContactTextBox.Enabled = false;
                                titleRegionalContactTextBox.Enabled = false;
                                phoneRegionalContactTextBox.Enabled = false;
                                faxRegionalContactTextBox.Enabled = false;
                                emailRegionalContactTextBox.Enabled = false;
                                firstNameRegionalContactTextBox.Text = string.Empty;
                                lastNameRegionalContactTextBox.Text = string.Empty;
                                titleRegionalContactTextBox.Text = string.Empty;
                                phoneRegionalContactTextBox.Text = string.Empty;
                                faxRegionalContactTextBox.Text = string.Empty;
                                emailRegionalContactTextBox.Text = string.Empty;
                            }
                            else
                            {
                                regionalContactIdHiddenField.Value = contact.ContactId;
                                firstNameRegionalContactTextBox.Text = contact.FirstName;
                                lastNameRegionalContactTextBox.Text = contact.LastName;
                                titleRegionalContactTextBox.Text = contact.Title;
                                phoneRegionalContactTextBox.Text = contact.Phone;
                                faxRegionalContactTextBox.Text = contact.Fax;
                                emailRegionalContactTextBox.Text = contact.Email;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Country Dropdown Selected Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList country = (DropDownList)sender;
                if (country.SelectedItem.Value != DropDownConstants.UnitedStates)
                {
                   stateDropDownList.SelectedValue = null;
                   stateDropDownList.Items.RemoveAt(0);
                   stateDropDownList.Items.Insert(0, new ListItem(" ", DropDownConstants.DefaultValue));
                   stateDropDownList.Enabled = false;
                   stateRequiredFieldValidator.Enabled = false;
                   zipCodeTextBox.ReadOnly = false;
                   zipCodeTextBox.Enabled = true;
                   zipCodeTextBox.Attributes.Remove("class");
                   zipCodeTextBox.Attributes.Add("class", "input textbox3 small mandatory");
                   zipCodeRequiredFieldValidator.Enabled = true;
                }
                else
                {
                    stateDropDownList.Items.RemoveAt(0);
                    stateDropDownList.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1244, culture, "Select a State"), DropDownConstants.DefaultValue));
                    stateDropDownList.Enabled = true;
                    stateRequiredFieldValidator.Enabled = true;
                    zipCodeTextBox.ReadOnly = false;
                    zipCodeTextBox.Enabled = true;
                    zipCodeTextBox.Attributes.Remove("class");
                    zipCodeTextBox.Attributes.Add("class", "input textbox3 small mandatory");
                    zipCodeRequiredFieldValidator.Enabled = true;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Populate Dropdowns
        /// </summary>
        private void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDownList, ResourceUtility.GetLocalizedString(1239, culture, "Select a Country"), null);
            BindStaticDropDown(DropDownConstants.StateDropDown, stateDropDownList, null, null);
            stateDropDownList.Items.Insert(0, new ListItem(" ", DropDownConstants.DefaultValue));
        }

        /// <summary>
        /// Populate partnership Amount
        /// </summary>
        private void PopulatePartnershipAmount()
        {
            DataSet amountDetailsDataSet = _partnerManager.GetPartnershipAmount;

            string goldAmountAfterFormatting = Convert.ToString(amountDetailsDataSet.Tables[0].Rows[0][0], CultureInfo.InvariantCulture);

            SetViewState(ViewStateConstants.GoldAmount, goldAmountAfterFormatting);

            if (goldAmountAfterFormatting.Length >= 7)
            {
                goldAmountAfterFormatting = "$" + goldAmountAfterFormatting.Insert(goldAmountAfterFormatting.Length - 6, ",");
            }
            else
            {
                goldAmountAfterFormatting = "$" + goldAmountAfterFormatting;
            }

            string regularAmountAfterFormatting = Convert.ToString(amountDetailsDataSet.Tables[0].Rows[0][1], CultureInfo.InvariantCulture);

            SetViewState(ViewStateConstants.RegularAmount, regularAmountAfterFormatting);
            if (regularAmountAfterFormatting.Length >= 7)
            {
                regularAmountAfterFormatting = "$" + regularAmountAfterFormatting.Insert(regularAmountAfterFormatting.Length - 6, ",");
            }
            else
            {
                regularAmountAfterFormatting = "$" + regularAmountAfterFormatting;
            }
            goldPartnershipAmountLabel.Text = goldAmountAfterFormatting;
            regularPartnershipAmountLabel.Text = regularAmountAfterFormatting;
        }

        /// <summary>
        /// Checkbox Checked Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SameAsPrimaryCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                try
                {
                    if (((CheckBox)sender).Checked)
                    {
                        foreach (RepeaterItem item in regionContactRepeater.Items)
                        {
                            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                            {
                                CheckBox sameAsPrimaryCheckBox = (CheckBox)item.FindControl("sameAsPrimaryCheckBox");
                                if (sameAsPrimaryCheckBox.Checked)
                                {
                                    TextBox firstNameRegionalContactTextBox = (TextBox)item.FindControl("firstNameRegionalContactTextBox");
                                    TextBox lastNameRegionalContactTextBox = (TextBox)item.FindControl("lastNameRegionalContactTextBox");
                                    TextBox titleRegionalContactTextBox = (TextBox)item.FindControl("titleRegionalContactTextBox");
                                    TextBox phoneRegionalContactTextBox = (TextBox)item.FindControl("phoneRegionalContactTextBox");
                                    TextBox faxRegionalContactTextBox = (TextBox)item.FindControl("faxRegionalContactTextBox");
                                    TextBox emailRegionalContactTextBox = (TextBox)item.FindControl("emailRegionalContactTextBox");

                                    firstNameRegionalContactTextBox.Text = string.Empty;
                                    firstNameRegionalContactTextBox.Enabled = false;
                                    lastNameRegionalContactTextBox.Text = string.Empty;
                                    lastNameRegionalContactTextBox.Enabled = false;
                                    titleRegionalContactTextBox.Text = string.Empty;
                                    titleRegionalContactTextBox.Enabled = false;
                                    phoneRegionalContactTextBox.Text = string.Empty;
                                    phoneRegionalContactTextBox.Enabled = false;
                                    faxRegionalContactTextBox.Text = string.Empty;
                                    faxRegionalContactTextBox.Enabled = false;
                                    emailRegionalContactTextBox.Text = string.Empty;
                                    emailRegionalContactTextBox.Enabled = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (RepeaterItem item in regionContactRepeater.Items)
                        {
                            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                            {
                                CheckBox sameAsPrimaryCheckbox = (CheckBox)item.FindControl("sameAsPrimaryCheckBox");
                                if (!sameAsPrimaryCheckbox.Checked)
                                {
                                    TextBox firstNameRegionalContactTextBox = (TextBox)item.FindControl("firstNameRegionalContactTextBox");
                                    TextBox lastNameRegionalContactTextBox = (TextBox)item.FindControl("lastNameRegionalContactTextBox");
                                    TextBox titleRegionalContactTextBox = (TextBox)item.FindControl("titleRegionalContactTextBox");
                                    TextBox phoneRegionalContactTextBox = (TextBox)item.FindControl("phoneRegionalContactTextBox");
                                    TextBox faxRegionalContactTextBox = (TextBox)item.FindControl("faxRegionalContactTextBox");
                                    TextBox emailRegionalContactTextBox = (TextBox)item.FindControl("emailRegionalContactTextBox");

                                    RegularExpressionValidator firstNameRegContactRequiredFieldValidator = (RegularExpressionValidator)item.FindControl("firstNameRegContactRegularExpressionValidator");
                                    RegularExpressionValidator lastNameRegContactRequiredFieldValidator = (RegularExpressionValidator)item.FindControl("lastNameRegContactRegularExpressionValidator");
                                    RegularExpressionValidator titleRegContactRequiredFieldValidator = (RegularExpressionValidator)item.FindControl("titleRegContactRegularExpressionValidator");
                                    RegularExpressionValidator regionalEmailRegularExpressionValidator = (RegularExpressionValidator)item.FindControl("regionalEmailRegularExpressionValidator");
                                    RegularExpressionValidator faxRegionalRegularExpressionValidator = (RegularExpressionValidator)item.FindControl("faxRegionalRegularExpressionValidator");

                                    SetValidatorRegularExpressions(firstNameRegContactRequiredFieldValidator, (int)RegularExpressionTypeEnum.NameValidator);
                                    SetValidatorRegularExpressions(lastNameRegContactRequiredFieldValidator, (int)RegularExpressionTypeEnum.NameValidator);
                                    SetValidatorRegularExpressions(titleRegContactRequiredFieldValidator, (int)RegularExpressionTypeEnum.TitleValidator);
                                    SetValidatorRegularExpressions(regionalEmailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
                                    SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);

                                    firstNameRegionalContactTextBox.Enabled = true;
                                    lastNameRegionalContactTextBox.Enabled = true;
                                    titleRegionalContactTextBox.Enabled = true;
                                    phoneRegionalContactTextBox.Enabled = true;
                                    faxRegionalContactTextBox.Enabled = true;
                                    emailRegionalContactTextBox.Enabled = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }
            }
        }

        /// <summary>
        /// Submit Data to next page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContinueApplicationData(object sender, EventArgs e)
        {
            try
            {
                int result = 0;
                result = _partnerManager.CheckDuplicatePartner(companyNameTextBox.Text);
                if (result == -1)
                {
                    if (GetSession<byte[]>(SessionConstants.ProductImageBytes) != null)
                    {
                        companyLogoImage.ImageUrl = GetSession<byte[]>(SessionConstants.ProductImageBytes) != null ? "data:image/jpg;base64," + Convert.ToBase64String(((byte[])GetSession<byte[]>(SessionConstants.ProductImageBytes)).ConvertToThumbnail()) : string.Empty;
                        imageLinkButton.Text = imageNameHiddenField.Value;
                        removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "inline");
                    }
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateCompanyNameErrorMessage, culture));
                }
                else
                {
                    _partnerApplication = GetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails); ;

                    if (goldLevelPartnerRadioButton.Checked)
                    {
                        _partnerApplication.PartnershipType = (int)PartnershipTypeEnum.GoldPartner;
                        _partnerApplication.PartnerShipAmount = (float)Convert.ToDouble(GetViewState<object>(ViewStateConstants.GoldAmount), CultureInfo.InvariantCulture);
                    }
                    else if (regularPartnerRadioButton.Checked)
                    {
                        _partnerApplication.PartnershipType = (int)PartnershipTypeEnum.Partner;

                        _partnerApplication.PartnerShipAmount = (float)Convert.ToDouble(GetViewState<object>(ViewStateConstants.RegularAmount), CultureInfo.InvariantCulture);
                    }

                    _partnerApplication.PartnerId = Convert.ToInt32(partnerIdHiddenField.Value, CultureInfo.InvariantCulture);
                    _partnerApplication.CompanyName = companyNameTextBox.Text;
                    _partnerApplication.CompanyAddress1 = companyAddressLine1TextBox.Text;
                    _partnerApplication.CompanyAddress2 = companyAddressLine2TextBox.Text;
                    if (String.IsNullOrEmpty(imageNameHiddenField.Value))
                    {
                        _partnerApplication.CompanyLogoName = _partnerApplication.CompanyLogoName;
                        _partnerApplication.CompanyLogoImage = _partnerApplication.CompanyLogoImage;
                    }
                    else
                    {
                        _partnerApplication.CompanyLogoName = imageNameHiddenField.Value;
                        _partnerApplication.CompanyLogoImage = Session[SessionConstants.ProductImageBytes] != null ? (byte[])Session[SessionConstants.ProductImageBytes] : null; //null;//GetImageBytes();
                        _partnerApplication.CompanyLogoImageThumbnail = Session[SessionConstants.ProductImageBytes] != null ? ((byte[])Session[SessionConstants.ProductImageBytes]).ConvertToThumbnail() : null;
                    }
                    _partnerApplication.WebSiteLink = webSiteTextBox.Text;
                    _partnerApplication.Country = countryDropDownList.SelectedValue;
                    _partnerApplication.CountryName = countryDropDownList.SelectedItem.Text;
                    _partnerApplication.City = cityTextBox.Text;
                    _partnerApplication.State = stateDropDownList.SelectedValue;
                    _partnerApplication.StateName = stateDropDownList.SelectedItem.Text;
                    _partnerApplication.ZipCode = zipCodeTextBox.Text.Trim();
                    _partnerApplication.CompanyDescription = companyDescriptionTextBox.Text;

                    _partnerApplication.Contacts = new List<Contact>();

                    Contact primaryContact = new Contact()
                    {
                        ContactId = primaryContactIdHiddenField.Value,
                        FirstName = firstNamePrimaryContactTextBox.Text,
                        LastName = lastNamePrimaryContactTextBox.Text,
                        Title = titlePrimaryContactTextBox.Text,
                        Phone = phonePrimaryContactTextBox.Text,
                        Fax = faxPrimaryContactTextBox.Text,
                        Email = emailAddressPrimaryContactTextBox.Text,
                        ContactTypeId = (Int32)ContactTypeEnum.Primary,
                        RegionId = 0
                    };

                    _partnerApplication.Contacts.Add(primaryContact);

                    Contact secondaryContact = new Contact()
                    {
                        ContactId = secondaryContactIdHiddenField.Value,
                        FirstName = firstNameSecondaryContactTextBox.Text,
                        LastName = lastNameSecondaryContactTextBox.Text,
                        Title = titleSecondaryContactTextBox.Text,
                        Phone = phoneSecondaryContactTextBox.Text,
                        Fax = faxSecondaryContactTextBox.Text,
                        Email = emailAddressSecondaryContactTextBox.Text,
                        ContactTypeId = (Int32)ContactTypeEnum.Secondary,
                        RegionId = 0,
                    };

                    
                    _partnerApplication.Contacts.Add(secondaryContact);
                   
                    foreach (RepeaterItem item in regionContactRepeater.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            HiddenField regionalContactIdHiddenField = (HiddenField)item.FindControl("regionalContactIdHiddenField");
                            TextBox firstNameRegionalContactTextBox = (TextBox)item.FindControl("firstNameRegionalContactTextBox");
                            TextBox lastNameRegionalContactTextBox = (TextBox)item.FindControl("lastNameRegionalContactTextBox");
                            TextBox titleRegionalContactTextBox = (TextBox)item.FindControl("titleRegionalContactTextBox");
                            TextBox phoneRegionalContactTextBox = (TextBox)item.FindControl("phoneRegionalContactTextBox");
                            TextBox faxRegionalContactTextBox = (TextBox)item.FindControl("faxRegionalContactTextBox");
                            TextBox emailRegionalContactTextBox = (TextBox)item.FindControl("emailRegionalContactTextBox");

                            Label regionNameLabel = (Label)item.FindControl("regionNameLabel");
                            Label regionIdLabel = (Label)item.FindControl("regionIdLabel");
                            Label countryNameLabel = (Label)item.FindControl("countryNameLabel");
                            Label countryIdLabel = (Label)item.FindControl("countryIdLabel");
                            CheckBox sameAsPrimaryCheckBox = (CheckBox)item.FindControl("sameAsPrimaryCheckBox");
                            bool isSameAsPrimary;
                            Contact regionalContacts;
                            if (sameAsPrimaryCheckBox.Checked)
                            {
                                isSameAsPrimary = Convert.ToBoolean(1);
                                regionalContacts = new Contact
                                {
                                    ContactId = regionalContactIdHiddenField.Value,
                                    FirstName = firstNamePrimaryContactTextBox.Text,
                                    LastName = lastNamePrimaryContactTextBox.Text,
                                    Title = titlePrimaryContactTextBox.Text,
                                    Phone = phonePrimaryContactTextBox.Text,
                                    Fax = faxPrimaryContactTextBox.Text,
                                    Email = emailAddressPrimaryContactTextBox.Text,
                                    ContactTypeId = (Int32)ContactTypeEnum.Regional,
                                    RegionId = Convert.ToInt32(regionIdLabel.Text, CultureInfo.InvariantCulture),
                                    RegionName = regionNameLabel.Text,
                                    CountryId = Convert.ToInt32(countryIdLabel.Text, CultureInfo.InvariantCulture),
                                    CountryName = countryNameLabel.Text,
                                    IsSameAsPrimary = Convert.ToBoolean(isSameAsPrimary)
                                };
                            }
                            else
                            {
                                isSameAsPrimary = Convert.ToBoolean(0);
                                regionalContacts = new Contact
                                {
                                    ContactId = regionalContactIdHiddenField.Value,
                                    FirstName = firstNameRegionalContactTextBox.Text,
                                    LastName = lastNameRegionalContactTextBox.Text,
                                    Title = titleRegionalContactTextBox.Text,
                                    Phone = phoneRegionalContactTextBox.Text,
                                    Fax = faxRegionalContactTextBox.Text,
                                    Email = emailRegionalContactTextBox.Text,
                                    ContactTypeId = (Int32)ContactTypeEnum.Regional,
                                    RegionId = Convert.ToInt32(regionIdLabel.Text, CultureInfo.InvariantCulture),
                                    RegionName = regionNameLabel.Text,
                                    CountryId = Convert.ToInt32(countryIdLabel.Text, CultureInfo.InvariantCulture),
                                    CountryName = countryNameLabel.Text,
                                    IsSameAsPrimary = Convert.ToBoolean(isSameAsPrimary)
                                };
                            }
                            _partnerApplication.Contacts.Add(regionalContacts);
                        }
                    }

                    SetSession<PartnerApplication>(SessionConstants.ApplicationDetails, _partnerApplication);
                    Response.Redirect("ReviewApplication.aspx");
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Remove the uploaded image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageLinkButton.Text = string.Empty;
                imageNameHiddenField.Value = string.Empty;
                companyLogoImage.ImageUrl = "../Images/NoProduct.PNG";
                ClearSession(SessionConstants.ProductImageBytes);
                removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "ImageUpload", "SetFileInputStyles();", true);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}