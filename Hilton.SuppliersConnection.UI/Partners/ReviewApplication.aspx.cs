﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ReviewApplication : PageBase
    {
        private IPartnerManager _partnerManager;
        private PartnerApplication _partnerApplication = new PartnerApplication();

        /// <summary>
        /// Validate the admin permission
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void TermsAndCondition_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (!termsAgreementCheckBox.Checked)
                {
                    ConfigureResultMessage(termsAndConditionsMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.AcceptTermAndCondition, culture));
                    args.IsValid = false;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Init
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    ClearSession(SessionConstants.ApplicationDetails);
                    ClearSession(SessionConstants.FilterCriteria);
                    ClearSession(SessionConstants.AppliedOpportunityDetails);
                    ClearSession(SessionConstants.EditPartnerApplication);
                    ClearSession(SessionConstants.AllCascadeCategoryIds);
                    ClearSession(SessionConstants.ProductImageBytes);
                    Response.Redirect("~/", true);
                }
                _partnerManager = PartnerManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(86, culture, "Apply to Become a Partner");

                SetGridViewHeaderText();
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Repeater ItemCommand
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionContactRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e != null && e.CommandName == "Edit"  )
                {
                    var appliedOpportunities = GetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails);

                    Server.Transfer("ApplicationDetails.aspx", false);
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    _partnerApplication = GetSession<PartnerApplication>(SessionConstants.ApplicationDetails);
                    TermsAndConditions termsAndCondition;
                    termsAndCondition = GetSession<TermsAndConditions>(SessionConstants.TermsAndCondition);

                    if (termsAndCondition != null)
                    {
                        termsAndConditionDiv.Visible = true;
                        termAndConditionTextBox.Text = termsAndCondition.TermsAndConditionDesc.ToString();
                        termsAndConditionVersionHiddenField.Value = termsAndCondition.VersionId.ToString();
                    }
                    else
                    {
                        termsAndConditionDiv.Visible = false;
                        termCustomerValidator.Enabled = false;
                    }

                    if (_partnerApplication.PartnershipType == (int)PartnershipTypeEnum.GoldPartner)
                    {
                        goldBlockTableRow.Style.Add("display", "block");
                        silverBlockTableRow.Style.Add("display", "none");

                        string goldAmountAfterFormatting = _partnerApplication.PartnerShipAmount.ToString("c", culture);

                        goldPartnershipAmountLabel.Text = goldAmountAfterFormatting;
                    }
                    else if (_partnerApplication.PartnershipType == (int)PartnershipTypeEnum.Partner)
                    {
                        goldBlockTableRow.Style.Add("display", "none");
                        silverBlockTableRow.Style.Add("display", "block");

                        string regularAmountAfterFormatting = _partnerApplication.PartnerShipAmount.ToString("c", culture);

                        regularPartnershipAmountLabel.Text = regularAmountAfterFormatting;
                    }

                    partnerIdHiddenField.Value = Convert.ToString(_partnerApplication.PartnerId, CultureInfo.InvariantCulture);
                    companyNameLabel.Text = _partnerApplication.CompanyName;
               
                    companyLogoImage.ImageUrl = _partnerApplication.CompanyLogoImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(_partnerApplication.CompanyLogoImageThumbnail) : "../Images/NoProduct.png";

                    webSiteLinkLabel.Text = _partnerApplication.WebSiteLink;
                    companyAddressLabel.Text = _partnerApplication.CompanyAddress1 + " " + _partnerApplication.CompanyAddress2;
                    if (_partnerApplication.Country == "0")
                        countryLabel.Text = "";
                    else
                        countryLabel.Text = _partnerApplication.CountryName;

                    cityLabel.Text = _partnerApplication.City;
                    if (_partnerApplication.State == "0")
                        stateLabel.Text = "";
                    else
                        stateLabel.Text = _partnerApplication.StateName;

                    zipCodeLabel.Text = _partnerApplication.ZipCode;
                    companyDescriptionLabel.Text = _partnerApplication.CompanyDescription;

                    Contact primaryContactInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId == (int)ContactTypeEnum.Primary).FirstOrDefault();
                    Contact secondaryContactInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId == (int)ContactTypeEnum.Secondary).FirstOrDefault();
                    IEnumerable<Contact> regionalContactsInfo = _partnerApplication.Contacts.Where(p => p.ContactTypeId != (int)ContactTypeEnum.Primary && p.ContactTypeId != (int)ContactTypeEnum.Secondary);

                    IEnumerable<AppliedPartnershipOpportunity> applied = _partnerApplication.AppliedParntershipOpportunities.AsEnumerable();

                    //Primary contact
                    primaryContactIdHiddenField.Value = primaryContactInfo.ContactId;
                    firstNamePrimaryContactLabel.Text = primaryContactInfo.FirstName;
                    lastNamePrimaryContactLabel.Text = primaryContactInfo.LastName;
                    titlePrimaryContactLabel.Text = primaryContactInfo.Title;
                    phonePrimaryContactLabel.Text = primaryContactInfo.Phone;
                    faxPrimaryContactLabel.Text = primaryContactInfo.Fax;
                    emailAddressPrimaryContactLabel.Text = primaryContactInfo.Email;

                    //Secondary Contact
                    secondaryContactIdHiddenField.Value = secondaryContactInfo.ContactId;
                    firstNameSecondaryContactLabel.Text = secondaryContactInfo.FirstName;
                    lastNameSecondaryContactLabel.Text = secondaryContactInfo.LastName;
                    titleSecondaryContactLabel.Text = secondaryContactInfo.Title;
                    phoneSecondaryContactLabel.Text = secondaryContactInfo.Phone;
                    faxSecondaryContactLabel.Text = secondaryContactInfo.Fax;
                    emailSecondaryContactLabel.Text = secondaryContactInfo.Email;
                   
                    //Regional Contact
                    regionContactRepeater.DataSource = regionalContactsInfo;
                    regionContactRepeater.DataBind();

                    selectedCategoriesGridView.DataSource = applied;
                    selectedCategoriesGridView.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid Row Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedCategoriesGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e != null && e.CommandName == "edit")
                {
                    Response.Redirect("BecomeRecommendedPartner.aspx");
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Edit Contact
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditPrimaryContactButton_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("ApplicationDetails.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Edit Company
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditCompanyInformationButton_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("ApplicationDetails.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Edit Category Info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditCategoryInfo(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("BecomeRecommendedPartner.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Edit Partner Info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditPartnerTypeButton_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("ApplicationDetails.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Submit Data to DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubmitApplicationButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    PartnerApplication partnerApplication = GetSession<PartnerApplication>(SessionConstants.ApplicationDetails);

                    if (termsAndConditionDiv.Visible)
                    {
                        partnerApplication.TermsAndCondition = new TermsAndConditions();
                        partnerApplication.TermsAndCondition.TermsAndConditionDesc = termAndConditionTextBox.Text;
                        partnerApplication.TermsAndCondition.VersionId = Convert.ToInt16(termsAndConditionVersionHiddenField.Value);
                        partnerApplication.TermsAndCondition.IPAddress = string.Empty;

                         User currentUser = GetSession<User>(SessionConstants.User);
                        if (currentUser !=null)
                        {
                            partnerApplication.UserId = currentUser.UserId; 
                        }
                    }

                    if (partnerApplication.PartnerId == 0)
                    {
                        _partnerManager.SubmitPartnerApplication(partnerApplication);
                    }
                    else
                    {
                        _partnerManager.UpdatePartnerApplication(partnerApplication); //To update
                    }

                    SetSession<PartnerApplication>(SessionConstants.PrintPartnerApplication, partnerApplication);

                    Response.Redirect("ThankYouNextSteps.aspx");
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Get Headers from Content Block
        /// </summary>
        private void SetGridViewHeaderText()
        {
            selectedCategoriesGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(280, culture, "CATEGORY");
            selectedCategoriesGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(589, culture, "REGIONS");
            selectedCategoriesGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(1302, culture, "COUNTRY");
            selectedCategoriesGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(251, culture, "BRAND");
           // selectedCategoriesGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(562, culture, "TYPE");
        }
    }
}