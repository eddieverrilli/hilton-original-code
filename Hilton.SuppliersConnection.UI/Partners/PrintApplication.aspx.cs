﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class PrintApplication : PageBase
    {
        private IPartnerManager _partnerManager;
        private PartnerApplication partnerApplication;
        private const string PageUrl = "~/Partners/PrintApplication.aspx";

        /// <summary>
        /// To open a pop up
        /// </summary>
        public static void Open()
        {
            try
            {
                PageBase page = HttpContext.Current.Handler as PageBase;
                page.OpenDialog(page.ResolveUrl(PageUrl), "Popup", 820, 400);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Invoked on PreRender
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);
                SupressPageHeader();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Init
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _partnerManager = PartnerManager.Instance;
                partnerApplication = new PartnerApplication();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<PartnerApplication>(SessionConstants.PrintPartnerApplication) != null)
                {
                    partnerApplication = GetSession<PartnerApplication>(SessionConstants.PrintPartnerApplication);

                    if (partnerApplication.PartnershipType == (int)PartnershipTypeEnum.GoldPartner)
                    {
                        goldBlockTableRow.Style.Add("display", "block");
                        silverBlockTableRow.Style.Add("display", "none");
                        goldPartnerImageIcon.ImageUrl = "../Images/goldprt.gif";

                        goldPartnershipAmountLabel.Text = partnerApplication.PartnerShipAmount.ToString("c", culture);
                    }
                    else if (partnerApplication.PartnershipType == (int)PartnershipTypeEnum.Partner)
                    {
                        goldBlockTableRow.Style.Add("display", "none");
                        silverBlockTableRow.Style.Add("display", "block");
                        partnerImageIcon.ImageUrl = "../Images/regularprt.gif";
                        regularPartnershipAmountLabel.Text = partnerApplication.PartnerShipAmount.ToString("c", culture);
                    }

                    companyNameLabel.Text = partnerApplication.CompanyName;
                    companyLogoImage.ImageUrl = partnerApplication.CompanyLogoImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(partnerApplication.CompanyLogoImage.ConvertToThumbnail()) : "../Images/NoProduct.png";
                    webSiteLinkLabel.Text = partnerApplication.WebSiteLink;
                    companyAddressLabel.Text = partnerApplication.CompanyAddress1 + " " + partnerApplication.CompanyAddress2;
                    countryLabel.Text = partnerApplication.CountryName;
                    cityLabel.Text = partnerApplication.City;
                    stateLabel.Text = partnerApplication.StateName;
                    zipCodeLabel.Text = partnerApplication.ZipCode;
                    companyDescriptionLabel.Text = partnerApplication.CompanyDescription;

                    Contact primaryContactInfo = partnerApplication.Contacts.Where(p => p.ContactTypeId == 1).FirstOrDefault();
                    Contact secondaryContactInfo = partnerApplication.Contacts.Where(p => p.ContactTypeId == 2).FirstOrDefault();
                    IEnumerable<Contact> regionalContactsInfo = partnerApplication.Contacts.Where(p => p.ContactTypeId != 1 && p.ContactTypeId != 2);

                    IEnumerable<AppliedPartnershipOpportunity> applied = partnerApplication.AppliedParntershipOpportunities.AsEnumerable();

                    //Primary contact
                    primaryContactIdHiddenField.Value = primaryContactInfo.ContactId;
                    firstNamePrimaryContactLabel.Text = primaryContactInfo.FirstName;
                    lastNamePrimaryContactLabel.Text = primaryContactInfo.LastName;
                    titlePrimaryContactLabel.Text = primaryContactInfo.Title;
                    phonePrimaryContactLabel.Text = primaryContactInfo.Phone;
                    faxPrimaryContactLabel.Text = primaryContactInfo.Fax;
                    emailAddressPrimaryContactLabel.Text = primaryContactInfo.Email;

                    //Secondary Contact
                    secondaryContactIdHiddenField.Value = secondaryContactInfo.ContactId;
                    firstNameSecondaryContactLabel.Text = secondaryContactInfo.FirstName;
                    lastNameSecondaryContactLabel.Text = secondaryContactInfo.LastName;
                    titleSecondaryContactLabel.Text = secondaryContactInfo.Title;
                    phoneSecondaryContactLabel.Text = secondaryContactInfo.Phone;
                    faxSecondaryContact.Text = secondaryContactInfo.Fax;
                    emailSecondaryContactLabel.Text = secondaryContactInfo.Email;
                  
                    //Regional Contact
                    regionContactRepeater.DataSource = regionalContactsInfo;
                    regionContactRepeater.DataBind();

                    selectedCategoriesGridView.DataSource = applied;
                    selectedCategoriesGridView.DataBind();
                }
              
            }

            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

      
    }
}