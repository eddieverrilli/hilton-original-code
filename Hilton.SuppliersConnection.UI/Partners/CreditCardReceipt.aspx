﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="true"
    CodeBehind="CreditCardReceipt.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.CreditCardReceipt" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="content">
        <div class="wrapper">
            <div class="breadcrumbs">
                <asp:HyperLink ID="recommendedPartnersHyperLink" CBID="3" Text="Recommended Partners"
                    NavigateUrl="~/Partners/RecommendedPartners.aspx" runat="server"></asp:HyperLink>
                <span>/</span>
                <asp:HyperLink ID="becomePartnerHyperLink" runat="server" CBID="37" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"
                    Text="Become a Partner"></asp:HyperLink>
                <span>/</span> <span>
                    <asp:Label ID="becomePartnerLabel" runat="server" CBID="37" Text="Become a Partner"></asp:Label></span>
            </div>
            <div class="main-content">
                <h2>
                    <asp:Label ID="creditCardReceiptTitleLabel" runat="server" CBID="90"></asp:Label></h2>
                <p>
                    <asp:Label ID="creditCardReceiptBodyLabel" runat="server" Text="Congratulations, your account has been activated. Please record the information below."></asp:Label>
                </p>
                <br />
                <div id="resultMessageDiv" runat="server">
                </div>
            </div>
            <div class="main" runat="server" clientidmode="Static" id="mainDiv">
                <div class="recipt">
                    <div class="accordion account-type">
                        <h3>
                            <asp:Label ID="transactionSummaryLabel" runat="server" CBID="728"></asp:Label></h3>
                        <div class="panel3">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="cell">
                                        <asp:Label ID="transactionDateTitleLabel" Text="TRANSACTION DATE" CBID="726" runat="server"></asp:Label>
                                    </td>
                                    <td class="cell">
                                        <asp:Label ID="transactionIdTitleLabel" Text="TRANSACTION ID" CBID="727" runat="server"></asp:Label>
                                    </td>
                                    <td class="cell">
                                        <asp:Label ID="transactionAmountTitleLabel" Text="AMOUNT PAID" CBID="225" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="cell">
                                        <asp:Label ID="transactionDateLabel" Text="" runat="server" class="large"></asp:Label>
                                    </td>
                                    <td class="cell">
                                        <asp:Label ID="transactionIdLabel" Text="" runat="server" class="large"></asp:Label>
                                    </td>
                                    <td class="cell">
                                        <asp:Label ID="transactionAmountLabel" Text="" runat="server" class="large"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border" colspan="5">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="accordion account-type">
                        <h3>
                            <asp:Label ID="accountTypeLabel" runat="server" CBID="225"></asp:Label></h3>
                        <div class="panel3">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="col2">
                                        <div runat="server" id="goldPartnerDiv">
                                            <span class="gold-level"></span>
                                            <h6>
                                                <asp:Label ID="goldLevelPartnerLabel" runat="server" CBID="412"></asp:Label></h6>
                                            <p>
                                                <asp:Label ID="goldLevelPartnerDescriptionLabel" runat="server" CBID="989" Text=""></asp:Label></p>
                                        </div>
                                        <div runat="server" id="partnerDiv">
                                            <span class="normal-level"></span>
                                            <h6>
                                                <asp:Label ID="regularPartnerLabel" runat="server" CBID="611"></asp:Label></h6>
                                            <p>
                                                <asp:Label ID="regularPartnerDescription" runat="server" CBID="990"></asp:Label></p>
                                        </div>
                                    </td>
                                    <td class="col3">
                                        <h6>
                                            <asp:Label ID="amountLabel" runat="server" Text=""></asp:Label></h6>
                                        <p>
                                            <asp:Label ID="perYearLabel" runat="server" CBID="511"></asp:Label></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border" colspan="3">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="information-forms">
                        <div class="contact-form">
                            <h3>
                                <asp:Label ID="contactInformationLabel" runat="server" CBID="326"></asp:Label></h3>
                            <div class="content">
                                <ul>
                                    <li>
                                        <asp:Label ID="firstNamePrimaryLabel" runat="server" Text="First Name" CBID="101"
                                            CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="firstNamePrimaryTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="lastNamePrimaryLabel" runat="server" Text="Last Name" CBID="113" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="lastNamePrimaryTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="titlePrimaryLabel" runat="server" Text="Title" CBID="712" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="titlePrimaryTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="phonePrimaryLabel" runat="server" Text="Phone" CBID="126" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="phonePrimaryTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="faxPrimaryLabel" runat="server" Text="Fax" CBID="143" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="faxPrimaryTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="emailPrimaryLabel" runat="server" Text="Email Address" CBID="377"
                                            CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="emailPrimaryTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                </ul>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                        <div class="contact-form flR">
                            <h3>
                                <asp:Label ID="billingInformationLabel" runat="server" CBID="249"></asp:Label></h3>
                            <div class="content">
                                <ul>
                                    <li>
                                        <asp:Label ID="firstNameBillingLabel" runat="server" Text="First Name" CBID="101"
                                            CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="firstNameBillingTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="lastNameBillingLabel" runat="server" Text="Last Name" CBID="113" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="lastNameBillingTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="titleBillingLable" runat="server" Text="Title" CBID="712" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="titleBillingTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="phoneBillingLabel" runat="server" Text="Phone" CBID="126" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="phoneBillingTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="faxBillingLabel" runat="server" Text="Fax" CBID="143" CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="faxBillingTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="emailBillingLabel" runat="server" Text="Email Address" CBID="386"
                                            CssClass="label"></asp:Label>
                                        <div class="input">
                                            <asp:TextBox ID="emailBillingTextBox" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </li>
                                </ul>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box1">
                        <h3>
                            <asp:Label ID="paymentInformationLabel" runat="server" CBID="502"></asp:Label></h3>
                        <div class="content">
                            <asp:Label ID="paymentTypeLabel" runat="server" CBID="506" Text="PAYMENT TYPE" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="paymentType" runat="server" Text="Credit Card" class="type" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar">
                <div id="rightsidebar-wrapper">
                    <div class="sidebar-box">
                        <h3>
                            <asp:Label ID="sidePanelHeaderLabel" runat="server" CBID="949" Text="Recommended Partners"></asp:Label>
                        </h3>
                        <div class="middle">
                            <ul>
                                <li>
                                    <asp:HyperLink ID="activateHyperLink" runat="server" class="active" CBID="1062" Text="ACTIVATE"></asp:HyperLink>
                                </li>
                                <li>
                                    <asp:HyperLink ID="receiptHyperLink" runat="server" CBID="1063" Text="RECEIPT"></asp:HyperLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="box">
                        <div class="title">
                            <h3>
                                <img src="../Images/help-icon.gif" alt="" width="13" height="15" />
                                NEED HELP?
                            </h3>
                        </div>
                        <div class="content">
                            <h5>
                                <asp:Label ID="customerSupportLabel" runat="server" CBID="1064" Text="Contact Account Support:"></asp:Label>
                            </h5>
                            <p class="email">
                                <asp:HyperLink ID="customerSupportEmailHyperLink" runat="server" CBID="1065" Text="info@hilton.com"></asp:HyperLink>
                            </p>
                            <p class="phone">
                                <asp:HyperLink ID="customerSupportPhoneHyperLink" runat="server" CBID="1066" Text=""></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear">
                &nbsp;</div>
        </div>
    </div>
</asp:Content>