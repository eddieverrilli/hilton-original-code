﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="BecomeRecommendedPartner.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.BecomeRecommendedPartner" %>

<asp:Content ID="content1" ContentPlaceHolderID="cphHeader" runat="server">
  
    <script src="../Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.watermarkinput.js" type="text/javascript"></script>
    <style type="text/css">
        .icon-cancel
        {
            width: 11px;
            height: 11px;
            float: left;
            background-position: center 0;
            background-image: url(image/close.png);
            cursor: pointer;
        }
        .icon-cancel:hover
        {
            background-position: center -11px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            $("#<%= hdn.ClientID %>").val("");
            $("#<%=textCategory.ClientID %>").Watermark("Please type Category");
            $("#<%=textCategory.ClientID %>").autocomplete({
                delay: 0,
                minLength: 3,
                source: function (request, response) {
                    var parameters = { categoryName_startsWith: request.term };
                    var json_data = JSON.stringify(parameters);
                    jQuery.support.cors = true;
                    $.ajax({
                        url: "BecomeRecommendedPartner.aspx/GetActiveCategories",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: json_data,
                        async: true,
                        crossDomain: true,
                        error: function (xhr, textStatus, errorThrown) {
                            $("#<%=textCategory.ClientID %>").val("");
                        },
                        failure: function (data) {
                            alert(data);
                        },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.CategoryName,
                                    id: item.CategoryId
                                }

                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    $("#<%= hdn.ClientID %>").val(ui.item.id);
                }
            });
        }  
    </script>
    <script type="text/javascript">

        function ProcessApply() {
            var Inputs = document.getElementsByTagName("input");
            var selectedCategories = '';
            for (var n = 0; n < Inputs.length; ++n) {
                if (Inputs[n].type == 'checkbox' && Inputs[n].checked) {
                    selectedCategories = selectedCategories + Inputs[n].id + ";";
                }
            }
            //      alert(selectedCategories);
            if (selectedCategories == '')
                document.getElementById('<%=selectedCategoriesHiddenField.ClientID %>').value = "0";
            else
                document.getElementById('<%=selectedCategoriesHiddenField.ClientID %>').value = selectedCategories;

            return true;
        }

        function SelectClickedButton(id, divId) {
            //alert("clicked");
            // var linkButtonId = "cphContent_categoryDivisionsRepeater_" + id;
            // var divStandardsId = "cphContent_categoryDivisionsRepeater_" + divId;
            var linkButtonId = id;
            var divStandardsId = divId;
            var linkbutton = document.getElementById(linkButtonId);
            var divStandards = document.getElementById(divStandardsId);
            //alert(linkbutton);
            //alert(divStandards);

            if (linkbutton.innerText == "Hide Standards") {
                linkbutton.innerText = "Show Standards";
                divStandards.style.display = "none";
            }
            else {
                linkbutton.innerText = "Hide Standards";
                divStandards.style.display = "block";
            }
            return false;
        }

        function CheckSelectedCheckbox() {

            var selectedIds = document.getElementById('<%=selectedCategoriesHiddenField.ClientID %>').value;
            //   alert(selectedIds);
            var selectCheckbox = selectedIds.split(";");

            var Inputs = document.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n) {

                if (Inputs[n].type == 'checkbox') {
                    for (var i = 0; i < selectCheckbox.length; i++) {
                        if (Inputs[n].id == selectCheckbox[i]) {
                            Inputs[n].checked = true;
                        }
                    }
                }
            }
            return true;
        }

        function more_lessItems() {
            $('td.abc').each(function () {
                var max = 1
                if ($(this).find("li").length > max) {
                    var count = $(this).find("li").length;
                    var num = count - 2;
                    if (num != 0) {
                        $(this).find('li:gt(1)').hide();
                        var list = $(this).find('li:gt(1)');
                        $(this).append('<span class="more">+' + num + ' More</span>');
                        $(this).append('<span href="" class="less">Less</span>');
                        $('.more').click(function () {
                            $(this).next().show();
                            $(this).parent().find('li:gt(1)').show()
                            $(this).hide();
                        });
                        $('.less').click(function () {
                            $(this).prev().show();
                            $(this).parent().find('li:gt(1)').hide()
                            $(this).hide();
                        });
                    }
                }
            });
        }

        function InitializeScript() {
            //$('.accordion-panel .accordion h3, .accordion-panel .accordion .panel1:first h4, .accordion-panel .accordion .panel1:first .panel2:first h5').addClass('active');
            //  $('.accordion-panel .accordion .description').hide();
            // alert("initialize");
            $('.accordion-panel .accordion:first h3').addClass('active');
            $('.accordion-panel .accordion h3').siblings().hide();
            $('.accordion-panel .accordion .panel1:first .panel2').hide();
            $('.accordion-panel .accordion .panel1:first .panel3:first').hide();
            $('.accordion-panel .accordion .description').hide();
            //$('.accordion-panel .accordion:first .panel').show();
            $('.accordion-panel .accordion h3:first').siblings().show();
            $('.accordion-panel .accordion h3').click(function () {
                $(this).toggleClass('active');
                $(this).siblings().slideToggle();

            });
            $('.accordion-panel .accordion h4').click(function () {

                $(this).toggleClass('active');
                $(this).siblings('.panel2').slideToggle();
            });
            $('.accordion-panel .accordion h5').click(function () {
                $(this).toggleClass('active');
                $(this).next().slideToggle();
            });

        }

        $(window).ready(function () {
            InitializeScript()
        });

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="recommendedPartnersHyperLink" CBID="3" Text="Recommended Partners"
            NavigateUrl="~/Partners/RecommendedPartners.aspx" runat="server"></asp:HyperLink>
        <span>/</span>
        <asp:HyperLink ID="becomePartnerHyperLink" runat="server" CBID="37" Text="Become a Partner"></asp:HyperLink>
        <span>/</span> <span>
            <asp:Label ID="chooseCategoriesLabel" runat="server" Text="Choose Categories" CBID="1069"></asp:Label></span>
    </div>
    <br />
    <div class="main">
        <div class="become-partner">
            <div class="box1">
                <div class="steps">
                    <asp:HyperLink ID="becomeRecomendedPartnerStepHyperlink" runat="server" class="active"></asp:HyperLink>
                    <asp:HyperLink ID="applicationDeatilStepHyperLink" runat="server" class=""></asp:HyperLink>
                    <asp:HyperLink ID="reviewApplicationStepHyperLink" runat="server" class=""></asp:HyperLink>
                    <asp:HyperLink ID="thankYouHyperStepLink" runat="server" class=""></asp:HyperLink>
                </div>
                <h2>
                    <asp:Label ID="becomeAPartnerTitleLabel" runat="server" CBID="84"></asp:Label></h2>
                <p>
                    <asp:Label ID="becomeAPartnerDescriptionLabel" runat="server" CBID="38"></asp:Label>
                </p>
            </div>
            <asp:UpdatePanel ID="dropDownUpdatePanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="accordion">
                        <h3 class="none">
                            <asp:Label ID="currentPartnershipOpportunitiesLabel" runat="server" CBID="348" Text="Apply for Current Partnership Opportunities"></asp:Label>
                        </h3>
                        <div class="search-result padtop">
                            <p class="partOpp-padleft" style="padding: 1px;">
                                <asp:Label ID="currentPartnershipOpportunitiesIntroLabel" runat="server" CBID="1125"
                                    Text="Please select a category, brand, region and country from the drop down boxes or type category and then select the corresponding button"></asp:Label>
                            </p>
                            <div>
                                <div class="label flL">
                                    <asp:Label ID="filterLabel" runat="server" Text="Filter:" CBID="770"></asp:Label></div>
                                <div class="flL" style="width: 500px">
                                    <div class="flL" style="margin-bottom: 2px;">
                                        <asp:DropDownList ID="categoriesDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="CategoriesDropDown_SelectedIndexChanged"
                                            AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="categoriesDropDownRequiredFieldValidator" InitialValue="0"
                                            VMTI="0" runat="server" ErrorMessage="Please Select a category." ControlToValidate="categoriesDropDown"
                                            Display="Dynamic" ValidationGroup="dropdown" CssClass="errorText"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="flL">
                                        <asp:DropDownList ID="brandsDropDown" ViewStateMode="Enabled" AutoPostBack="true"
                                            OnSelectedIndexChanged="BrandsDropDown_SelectedIndexChanged" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <%-- <div class="flL">  <asp:DropDownList ID="propertyTypeDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="PropertyTypeDropDown_SelectedIndexChanged"
                                    AutoPostBack="true" runat="server">
                                </asp:DropDownList></div>--%>
                                </div>
                                <br />
                                <div class="flL">
                                    <div style="width: 250px; margin-left: 90px; margin-bottom: 2px;">
                                        <asp:PlaceHolder ID="parentCategoryContainer" ViewStateMode="Enabled" runat="server">
                                        </asp:PlaceHolder>
                                    </div>
                                </div>
                                <div style="margin-left: 90px; margin-bottom: 2px;">
                                    <div class="flL">
                                        <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged"
                                            AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                        <br />
                                     <%--   <asp:RequiredFieldValidator ID="regionDropDownRequiredFieldValidator" runat="server"
                                            InitialValue="0" VMTI="0" ErrorMessage="Please Select a Region." ControlToValidate="regionDropDown"
                                            Display="Dynamic" ValidationGroup="dropdown" CssClass="errorText"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="flL">
                                        <asp:DropDownList ID="CountryDropDown" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="go flL" style="width: 10px">
                                        <asp:Button ID="filterButton" runat="server" Text="FILTER" ValidationGroup="dropdown"
                                            CBID="770" OnClick="FilterPartnershipOpportunities" />
                                    </div>
                                </div>
                            </div>
                            <div align="left" style="margin-left: 6px">
                                <%--<asp:Label ID="labelCategories"  class="label" runat="server" Text="SELECT CATEGORIES:"></asp:Label>--%>
                                <asp:TextBox ID="textCategory" runat="server"></asp:TextBox>
                                <asp:Button ID="buttonSearch" runat="server" class="serch" Text="Search" OnClick="buttonSearch_Click"
                                    ValidationGroup="category" />
                                <br />
                                <div class="ReqfieldBecomepartner">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCatetgory" runat="server" ErrorMessage="Please type a category to search"
                                        ControlToValidate="textCategory" Display="Dynamic" ValidationGroup="category"
                                        CssClass="errorText" InitialValue="Please type Category"></asp:RequiredFieldValidator>
                                    <asp:HiddenField ID="hdn" runat="server" />
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField ID="leafCategoryHdn" runat="server" Value="0" />
                        <asp:HiddenField ID="lastSelectedCategoryDropDown" runat="server" Value="0" />
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;
                    </div>
                    <div id="resultMessageDiv" runat="server">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="categoryDetailsUpdatePanel" ViewStateMode="Enabled" UpdateMode="Conditional"
                runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="filterButton" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="buttonSearch" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <div class="accordion-panel">
                        <div id="divPropertyListDetails" runat="server">
                            <asp:Repeater ID="categoryDivisionsRepeater" runat="server" ViewStateMode="Enabled"
                                OnItemDataBound="CategoryDivisionsRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <div class="accordion">
                                        <div class="property-list-detail">
                                            <div id="categoryListDivision" class="accordion" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <asp:HiddenField ID="hiddenLinkButtonId" runat="server" Value="0" />
                        <div class="clear">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="continueUpdatePanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="box1">
                        <div id="selectACategorydiv" runat="server">
                        </div>
                        <div class="go">
                            <asp:Label ID="nextStepLabel" runat="server" CBID="228" Text="NEXT STEP: APPLICATION DETAILS"></asp:Label>
                            <asp:Button ID="continueToApplicationDetailsButton" runat="server" Text="CONTINUE"
                                OnClientClick="return ProcessApply();" CBID="227" OnClick="ContinueToApplicationDetailsButton_Click" />
                            <asp:HiddenField ID="selectedCategoriesHiddenField" runat="server" Value="0" />
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="filterButton" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--Code for side bar--%>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="Recommended Partners" runat="server"></asp:Label></h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="browsePartnersHyperLink" runat="server" CBID="32" Text="Browse Partners"
                            NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="becomeAPartnerHyperLink" class="active" CBID="84" Text="Become a Partner"
                            runat="server"></asp:HyperLink>
                        <ul>
                            <li>
                                <asp:HyperLink ID="chooseCategoriesHyperLink" runat="server" class="active" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"
                                    CBID="1057" Text="Step 1: Choose Categories"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="completeApplicationHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/ApplicationDetails.aspx"
                                    CBID="1058" Text="Step 2: Complete Application"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="reviewApplicationHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/ReviewApplication.aspx"
                                    CBID="1059" Text="Step 3: Review Application"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="thankYouHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/ThankYouNextSteps.aspx"
                                    CBID="1060" Text="Step 4: Thank You"></asp:HyperLink>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>
