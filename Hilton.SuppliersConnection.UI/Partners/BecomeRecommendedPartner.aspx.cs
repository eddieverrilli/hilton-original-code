﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;
using System.Web.Services;

namespace Hilton.SuppliersConnection.UI
{
    public partial class BecomeRecommendedPartner : PageBase
    {
        private IPartnerManager _partnerManager;
        //   private int _repeaterRowsCount = 0;

        private ProjectTemplate _projectTemplate;
        private FilterCriteria _filterCriteria = new FilterCriteria();

        /// <summary>
        /// Get the description of content block for specified title
        /// </summary>
        /// <param name="categoryName_startsWith"></param>
        /// <returns> IList<Category> </returns>
        [WebMethod]
        public static IList<Category> GetActiveCategories(string categoryName_startsWith)
        {
            IList<Category> categories = HelperManager.Instance.GetActiveCategories.ToList();
            IList<Category> searchCategorylist = new List<Category>();
            foreach (Category c in categories)
            {
                searchCategorylist.Add(new Category() { CategoryName = c.CategoryName, CategoryId = c.CategoryId });
            }
            return searchCategorylist.Where(a => a.CategoryName.ToUpper().Contains(categoryName_startsWith.ToUpper())).ToList();
        }

        /// <summary>
        /// Invoked on Init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (parentCategoryContainer == null)
                    parentCategoryContainer = new PlaceHolder();
                RecreateControls("categoriesDropDown", "DropDownList");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    ArrayList checkBoxListIds = new ArrayList();
                    string allCheckedCategories;
                    String[] strArr;
                    rightPanelHeader.Text = ResourceUtility.GetLocalizedString(3, culture, "Recommended Partners").ToUpper();

                    PopulateDropDownData();

                    PartnerApplication partnerApplication = new PartnerApplication();
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("AccountStatus.aspx")) && (Context.Items[UIConstants.PartnerId] != null))
                    {
                        partnerApplication = _partnerManager.GetRenewAccountDetails((Convert.ToInt32(Context.Items[UIConstants.PartnerId], CultureInfo.InvariantCulture)));

                        _projectTemplate = _partnerManager.GetCategoriesForBecomePartner(0, 0, 0, 0);
                        IList<Category> rootCategoryList = (_projectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
                        categoryDivisionsRepeater.DataSource = rootCategoryList;
                        categoryDivisionsRepeater.DataBind();
                        SetSession<PartnerApplication>(SessionConstants.EditPartnerApplication, partnerApplication);
                        foreach (AppliedPartnershipOpportunity p in partnerApplication.AppliedParntershipOpportunities)
                        {
                            checkBoxListIds.Add(CreateCheckBoxListIds(p));
                        }
                        strArr = (String[])checkBoxListIds.ToArray(typeof(string));

                        allCheckedCategories = string.Join(";", strArr);
                        selectedCategoriesHiddenField.Value = allCheckedCategories;
                        Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "checkSelectedCheckbox", "CheckSelectedCheckbox()", true);
                    }
                    else if (Request.UrlReferrer != null && ((Request.UrlReferrer.ToString().Contains("ApplicationDetails.aspx") || Request.UrlReferrer.ToString().Contains("ReviewApplication.aspx"))))
                    {
                        FilterCriteria filterCriteria = GetSession<FilterCriteria>(SessionConstants.FilterCriteria);

                        PolulateSearchCriteria(filterCriteria);

                        if (GetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails) != null)
                        {
                            partnerApplication = GetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails);

                            _projectTemplate = _partnerManager.GetCategoriesForBecomePartner(Convert.ToInt32(filterCriteria.CategoryId, CultureInfo.InvariantCulture), Convert.ToInt32(filterCriteria.BrandId, CultureInfo.InvariantCulture), Convert.ToInt32(filterCriteria.RegionId, CultureInfo.InvariantCulture), Convert.ToInt32(filterCriteria.CountryId, CultureInfo.InvariantCulture));
                            IList<Category> rootCategoryList = (_projectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
                            categoryDivisionsRepeater.DataSource = rootCategoryList;
                            categoryDivisionsRepeater.DataBind();

                            partnerApplication.AppliedParntershipOpportunities.ToList().ForEach(p =>
                            {
                                checkBoxListIds.Add(CreateCheckBoxListIds(p));
                            });

                            strArr = (String[])checkBoxListIds.ToArray(typeof(string));

                            allCheckedCategories = string.Join(";", strArr);
                            selectedCategoriesHiddenField.Value = allCheckedCategories;

                            Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "checkSelectedCheckbox", "CheckSelectedCheckbox()", true);
                        }
                    }
                    else
                    {
                        ClearSession(SessionConstants.ApplicationDetails);
                        ClearSession(SessionConstants.TermsAndCondition);
                        ClearSession(SessionConstants.FilterCriteria);
                        ClearSession(SessionConstants.AppliedOpportunityDetails);
                        ClearSession(SessionConstants.EditPartnerApplication);
                        ClearSession(SessionConstants.AllCascadeCategoryIds);
                        ClearSession(SessionConstants.ProductImageBytes);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(categoryDetailsUpdatePanel, this.GetType(), "moreLess", "more_lessItems()", true);
                    ScriptManager.RegisterClientScriptBlock(categoryDetailsUpdatePanel, this.GetType(), "InitializeScript", "InitializeScript();", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="partnerSearch"></param>
        private void SetDropDownsSelectedValue(FilterCriteria filterCriteria)
        {
            if (Convert.ToInt16(filterCriteria.RegionId) > 0)
            {
                regionDropDown.SelectedValue = filterCriteria.RegionId.ToString();
            }
            if (Convert.ToInt16(filterCriteria.BrandId) > 0)
            {
                brandsDropDown.SelectedValue = filterCriteria.BrandId.ToString();
            }
        
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="filterCriteria"></param>
        private void PolulateSearchCriteria(FilterCriteria filterCriteria)
        {
            if (Convert.ToInt16(filterCriteria.CategoryId) > 0)
            {
                PopulateCascadeCategories();
                SetDropDownsSelectedValue(filterCriteria);
                ProcessDropDownItemChange(DropDownConstants.CategoriesDropDown, brandsDropDown, regionDropDown, null, categoriesDropDown);
                ProcessCategories(parentCategoryContainer.Controls.Count + 1);
            }

            if (Convert.ToInt16(filterCriteria.RegionId) > 0)
            {
                SetDropDownsSelectedValue(filterCriteria);
                DropDownList lastCategoryDropDown;
                if (parentCategoryContainer.Controls.Count > 0)
                {
                    lastCategoryDropDown = (DropDownList)FindControl(parentCategoryContainer, lastSelectedCategoryDropDown.Value);
                    IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
                    IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
                    Helper.ProcessDropDownItemChange(filteredHierarchyCBRTData, culture, DropDownConstants.RegionDropDown, brandsDropDown, regionDropDown, null, categoriesDropDown, (lastCategoryDropDown == null) ? 0 : Convert.ToInt16(lastCategoryDropDown.Text), searchFiltersData);

                    ProcessCategories(parentCategoryContainer.Controls.Count + 1);
                }
                else
                {
                    ProcessDropDownItemChange(DropDownConstants.RegionDropDown, brandsDropDown, regionDropDown, null, categoriesDropDown);
                }
            }

            if (Convert.ToInt16(filterCriteria.BrandId) > 0)
            {
                SetDropDownsSelectedValue(filterCriteria);
                DropDownList lastCategoryDropDown;
                if (parentCategoryContainer.Controls.Count > 0)
                {
                    lastCategoryDropDown = (DropDownList)FindControl(parentCategoryContainer, lastSelectedCategoryDropDown.Value);
                    IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
                    IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
                    Helper.ProcessDropDownItemChange(filteredHierarchyCBRTData, culture, DropDownConstants.BrandDropDown, brandsDropDown, regionDropDown, null, categoriesDropDown, (lastCategoryDropDown == null) ? 0 : Convert.ToInt16(lastCategoryDropDown.Text), searchFiltersData);
                    ProcessCategories(parentCategoryContainer.Controls.Count + 1);
                }
                else
                {
                    ProcessDropDownItemChange(DropDownConstants.BrandDropDown, brandsDropDown, regionDropDown, null, categoriesDropDown);
                }
            }

        }

        /// <summary>
        /// To Populate categories cascading
        /// </summary>
        private void PopulateCascadeCategories()
        {
            string allCascadeCategoryIds = GetSession<string>(SessionConstants.AllCascadeCategoryIds);
            if (!String.IsNullOrEmpty(allCascadeCategoryIds))
            {
                string[] temp = allCascadeCategoryIds.Split('~');
                for (int i = 0; i < temp.Length; i++)
                {
                    if (i == 0)
                    {
                        if (Convert.ToInt16(temp[i]) > 0)
                        {
                            categoriesDropDown.SelectedValue = temp[i];
                            SetViewState(ViewStateConstants.SelectedCategory, temp[i]);
                        }
                    }
                    if (temp[i] != "0")
                    {
                        Category currentSelectedCategory = ReadCategory(temp[i]);
                        if (!(currentSelectedCategory.IsLeaf))
                        {
                            IList<Category> childCategoryList = GetChildCategories(temp[i]);
                            if (childCategoryList.Count() > 0)
                            {
                                int nextID = Convert.ToInt32(i + 1, CultureInfo.InvariantCulture) + 1;

                                CreateDropDownList("categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
                                FindPopulateDropDowns(temp[i], "categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
                                DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, "categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
                                if (i + 1 != temp.Length)
                                {
                                    if (Convert.ToInt16(temp[i + 1]) > 0)
                                    {
                                        leafCategoryHdn.Value = temp[i + 1];
                                        newDropDown.SelectedValue = temp[i + 1];
                                        lastSelectedCategoryDropDown.Value = newDropDown.ID;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// To get categories from Viewstate
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(string currentSelectedCategoryId)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            Category category = (Category)(categories.Where(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)).Single());
            return category;
        }

        /// <summary>
        /// Create check box list ids
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private string CreateCheckBoxListIds(AppliedPartnershipOpportunity p)
        {
            var id = new StringBuilder();
            id.Append(p.CategoryId);
            id.Append("~");
            id.Append(p.CategoryDescription);
            id.Append("~");
            id.Append(p.BrandId);
            id.Append("~");
            id.Append(p.BrandName);
            id.Append("~");
            id.Append(p.RegionId);
            id.Append("~");
            id.Append(p.RegionName);

            return Convert.ToString(id, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Bind Category Data
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        private void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            categoryDropDown.DataSource = categoryList;
            categoryDropDown.DataTextField = "CategoryName";
            categoryDropDown.DataValueField = "CategoryId";
            categoryDropDown.DataBind();
        }

        /// <summary>
        /// Brand Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BrandsDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList lastCategoryDropDown;
                if (parentCategoryContainer.Controls.Count > 0)
                {
                    lastCategoryDropDown = (DropDownList)FindControl(parentCategoryContainer, lastSelectedCategoryDropDown.Value);
                    IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
                    IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
                    //For Country Change
                    Helper.ProcessDropDownItemChangeForCountry(filteredHierarchyCBRTData, culture, DropDownConstants.BrandDropDown, brandsDropDown, regionDropDown, CountryDropDown, null, categoriesDropDown, (lastCategoryDropDown == null) ? 0 : Convert.ToInt16(lastCategoryDropDown.Text), searchFiltersData);
                    ProcessCategories();

                }
                else
                {
                    ProcessDropDownItemChangeForCountry(DropDownConstants.BrandDropDown, brandsDropDown, regionDropDown, CountryDropDown, null, categoriesDropDown);
                }
                SetDropdownDefaultText();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Category Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoriesDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProcessDropDownItemChangeForCountry(DropDownConstants.CategoriesDropDown, brandsDropDown, regionDropDown, CountryDropDown, null, categoriesDropDown);
                SelectedIndexChanged(sender, e);

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Repeater ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryDivisionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item != null && e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl categoryDivision = (HtmlGenericControl)e.Item.FindControl("categoryListDivision");

                    Panel subcategoryPanel = new Panel();
                    //Take the root categories and process those. In processing the complete hierarchy will get build in create category division method
                    if ((Convert.ToInt32(((Entities.Category)(e.Item.DataItem)).ParentCategoryId)) == -1 && !(((Entities.Category)(e.Item.DataItem)).IsNewPartnersRequired).Value)
                    {
                        subcategoryPanel = CreateCategoryDivision((Entities.Category)(e.Item.DataItem));
                        categoryDivision.Controls.Add(subcategoryPanel);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Submit Data to Next Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContinueToApplicationDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {

                PartnerApplication partnerApplication = new PartnerApplication();

                if (GetSession<PartnerApplication>(SessionConstants.EditPartnerApplication) != null)
                {
                    partnerApplication = GetSession<PartnerApplication>(SessionConstants.EditPartnerApplication);
                    ClearSession(SessionConstants.EditPartnerApplication);
                }
                else if (GetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails) != null)
                {
                    partnerApplication = GetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails);
                }

                partnerApplication.AppliedParntershipOpportunities = new List<AppliedPartnershipOpportunity>();

                string checkedCategoriesValues = selectedCategoriesHiddenField.Value;
                string[] checkedCategories = checkedCategoriesValues.Split(';');

                for (int checkedCategoriesCount = 0; checkedCategoriesCount < checkedCategories.Length - 1; checkedCategoriesCount++)
                {
                    string[] checkedCategoriesDetails = checkedCategories[checkedCategoriesCount].Split('~');

                    AppliedPartnershipOpportunity partnershipCategories = new AppliedPartnershipOpportunity()
                    {
                        CategoryId = Convert.ToInt32(checkedCategoriesDetails[0], CultureInfo.InvariantCulture),
                        CategoryDescription = checkedCategoriesDetails[1],
                        BrandId = Convert.ToInt32(checkedCategoriesDetails[2], CultureInfo.InvariantCulture),
                        BrandName = checkedCategoriesDetails[3],
                        //PropertyTypeId = Convert.ToInt32(checkedCategoriesDetails[4], CultureInfo.InvariantCulture),
                        //PropertyTypeName = checkedCategoriesDetails[5],
                        RegionId = Convert.ToInt32(checkedCategoriesDetails[4], CultureInfo.InvariantCulture),
                        RegionName = checkedCategoriesDetails[5],
                        CountryId = Convert.ToInt32(checkedCategoriesDetails[6], CultureInfo.InvariantCulture),
                        CountryName = checkedCategoriesDetails[7]
                    };

                    partnerApplication.AppliedParntershipOpportunities.Add(partnershipCategories);
                }

                SetSession<PartnerApplication>(SessionConstants.AppliedOpportunityDetails, partnerApplication);

                if (partnerApplication.AppliedParntershipOpportunities.Count > 0)
                {
                    Response.Redirect("ApplicationDetails.aspx");
                }
                else
                {
                    selectACategorydiv.Style.Add("display", "block");
                    ConfigureResultMessage(selectACategorydiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SelectOnePartnershipOpportunity, culture));
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Dynamic DropDown creation
        /// </summary>
        /// <param name="id"></param>
        private void CreateDropDownList(string id)
        {
            DropDownList parentCategoryDropDown = new DropDownList();
            parentCategoryDropDown.ID = id;
            parentCategoryDropDown.AutoPostBack = true;
            parentCategoryDropDown.SelectedIndexChanged += new EventHandler(SelectedIndexChanged);

            parentCategoryContainer.Controls.Add(parentCategoryDropDown);
        }

        /// <summary>
        /// Filter Result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FilterPartnershipOpportunities(object sender, EventArgs e)
        {
            try
            {
                FilterCriteria filterCriteria = new FilterCriteria()
                {
                    CategoryId = Convert.ToInt16(leafCategoryHdn.Value),
                    BrandId = Convert.ToInt32(brandsDropDown.SelectedValue, CultureInfo.InvariantCulture),
                    //   PropertyTypeId = Convert.ToInt32(propertyTypeDropDown.SelectedValue, CultureInfo.InvariantCulture),
                    RegionId = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture),
                    CountryId = Convert.ToInt32(CountryDropDown.SelectedValue, CultureInfo.InvariantCulture),
                };

                SetSession<FilterCriteria>(SessionConstants.FilterCriteria, filterCriteria);

                string allLevelCategories = string.Empty;
                allLevelCategories = categoriesDropDown.SelectedValue;
                for (int i = 1; i < parentCategoryContainer.Controls.Count + 1; i++)
                {
                    DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, "categoriesDropDown-" + (i + 1).ToString(CultureInfo.InvariantCulture));
                    allLevelCategories += "~" + newDropDown.SelectedValue;
                }
                SetSession<string>(SessionConstants.AllCascadeCategoryIds, allLevelCategories);

                _projectTemplate = _partnerManager.GetCategoriesForBecomePartner(Convert.ToInt32(leafCategoryHdn.Value), Convert.ToInt32(filterCriteria.BrandId, CultureInfo.InvariantCulture), Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture), Convert.ToInt32(CountryDropDown.SelectedValue, CultureInfo.InvariantCulture));

                if (_projectTemplate.TermsAndCondition.VersionId != 0)
                {
                    SetSession<TermsAndConditions>(SessionConstants.TermsAndCondition, new TermsAndConditions() { VersionId = _projectTemplate.TermsAndCondition.VersionId, TermsAndConditionDesc = _projectTemplate.TermsAndCondition.TermsAndConditionDesc });
                }

                IList<Category> rootCategoryList = (_projectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();

                // Only bind the root categories with the repeater and the repeater code will internally manage the sub level creation
                if (rootCategoryList != null && rootCategoryList.Count != 0)
                {
                    categoryDivisionsRepeater.DataSource = rootCategoryList;
                    categoryDivisionsRepeater.DataBind();
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoRecordsFound, culture));
                }

                selectACategorydiv.Style.Add("display", "none");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                if (GetSession<User>(SessionConstants.User) != null)
                {
                    ClearSession(SessionConstants.ApplicationDetails);
                    ClearSession(SessionConstants.TermsAndCondition);
                    ClearSession(SessionConstants.FilterCriteria);
                    ClearSession(SessionConstants.AppliedOpportunityDetails);
                    ClearSession(SessionConstants.EditPartnerApplication);
                    ClearSession(SessionConstants.AllCascadeCategoryIds);
                    ClearSession(SessionConstants.ProductImageBytes);
                    Response.Redirect("~/");
                }
                _partnerManager = PartnerManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(84, culture, "Become a Partner");
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Dynamic Dropdown Selected Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);

            DropDownList ddl = (DropDownList)sender;
            string id = ddl.ID;
            int existingDropDownCount = FindOccurence("categoriesDropDown");
            if (!id.Contains("-"))
            {
                id = id + "-1";
            }
            string[] idSequence = id.Split('-');

            int currentSelectedCategoryId = Convert.ToInt32(ddl.SelectedValue.ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            for (int dropdownCount = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1; dropdownCount <= existingDropDownCount + 1; dropdownCount++)
            {
                DropDownList rowDivision = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
                parentCategoryContainer.Controls.Remove(rowDivision);
            }
            if (currentSelectedCategoryId != 0)
            {
                Category currentSelectedCategory = ReadCategory(currentSelectedCategoryId);
                if (!(currentSelectedCategory.IsLeaf))
                {
                    IList<Category> childCategoryList = GetChildCategories(currentSelectedCategoryId.ToString());
                    if (childCategoryList.Count() > 0)
                    {
                        int nextId = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1;

                        CreateDropDownList("categoriesDropDown-" + nextId.ToString(CultureInfo.InvariantCulture));
                        FindPopulateDropDowns(Convert.ToString(currentSelectedCategoryId), "categoriesDropDown-" + nextId.ToString(CultureInfo.InvariantCulture));
                    }
                }
            }
            if (ddl.SelectedIndex == 0)
            {
                if (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) > 1)
                {
                    string parentDropDownId;
                    if (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) == 2)
                    {
                        parentDropDownId = "categoriesDropDown";
                    }
                    else
                    {
                        parentDropDownId = "categoriesDropDown-" + (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) - 1).ToString(CultureInfo.InvariantCulture);
                    }

                    DropDownList parentDropDown = (DropDownList)parentCategoryContainer.FindControl(parentDropDownId);
                    int parentDropDownValue = Convert.ToInt32(parentDropDown.SelectedValue, CultureInfo.InvariantCulture);
                    SetViewState(ViewStateConstants.SelectedCategory, (int?)parentDropDownValue);
                    leafCategoryHdn.Value = parentDropDownValue.ToString();
                    lastSelectedCategoryDropDown.Value = parentDropDownId;
                    Helper.ProcessDropDownItemChange(filteredHierarchyCBRTData, culture, DropDownConstants.CategoriesDropDown, brandsDropDown, regionDropDown, null, parentDropDown);
                }
                else
                {
                    int categoriesDropDownValue = Convert.ToInt32(categoriesDropDown.SelectedValue, CultureInfo.InvariantCulture);
                    SetViewState(ViewStateConstants.SelectedCategory, (int?)categoriesDropDownValue);
                    leafCategoryHdn.Value = categoriesDropDown.SelectedValue;
                    lastSelectedCategoryDropDown.Value = "categoriesDropDown";
                }
            }
            else
            {
                SetViewState(ViewStateConstants.SelectedCategory, (int?)currentSelectedCategoryId);
                leafCategoryHdn.Value = currentSelectedCategoryId.ToString();
                lastSelectedCategoryDropDown.Value = ddl.ID;
                Helper.ProcessDropDownItemChange(filteredHierarchyCBRTData, culture, DropDownConstants.CategoriesDropDown, brandsDropDown, regionDropDown, null, ddl);
            }
            SetDropdownDefaultText();
        }

        

        /// <summary>
        /// Region DropDown Selected Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList lastCategoryDropDown;
                //To reset the country already selected
                CountryDropDown.SelectedIndex = -1;
                if (parentCategoryContainer.Controls.Count > 0)
                {
                    lastCategoryDropDown = (DropDownList)FindControl(parentCategoryContainer, lastSelectedCategoryDropDown.Value);
                    IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
                    IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
                    //Helper.ProcessDropDownItemChange(filteredHierarchyCBRTData, culture, DropDownConstants.RegionDropDown, brandsDropDown, regionDropDown, propertyTypeDropDown, categoriesDropDown, (lastCategoryDropDown == null) ? 0 : Convert.ToInt16(lastCategoryDropDown.Text), searchFiltersData);
                    //For Country Change
                    Helper.ProcessDropDownItemChangeForCountry(filteredHierarchyCBRTData, culture, DropDownConstants.RegionDropDown, brandsDropDown, regionDropDown, CountryDropDown, null, categoriesDropDown, (lastCategoryDropDown == null) ? 0 : Convert.ToInt16(lastCategoryDropDown.Text), searchFiltersData);
                    ProcessCategories();
                }
                else
                {
                    ProcessDropDownItemChangeForCountry(DropDownConstants.RegionDropDown, brandsDropDown, regionDropDown, CountryDropDown, null, categoriesDropDown);
                }
                SetDropdownDefaultText();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }




        /// <summary>
        /// Creating Levels for Categories
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        private Panel CreateCategoryDivision(Category category)
        {
            Panel categoryPanel = new Panel();
            string categoryName = category.CategoryName;
            string categoryId = category.CategoryId.ToString(CultureInfo.InvariantCulture);

            categoryPanel.ID = "panel" + categoryId;
            HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H3");
            headingCategoryName.InnerText = categoryName;
            Label spanOpen = new Label();
            spanOpen.Attributes.Add("class", "acc-open tg");
            spanOpen.Text = ResourceUtility.GetLocalizedString(473, culture, "Open");
            headingCategoryName.Controls.Add(spanOpen);

            Label spanClose = new Label();
            spanClose.Attributes.Add("class", "acc-close tg");
            spanClose.Text = ResourceUtility.GetLocalizedString(1117, culture, "Close");
            headingCategoryName.Controls.Add(spanClose);
            categoryPanel.Controls.Add(headingCategoryName);

            IEnumerable<Category> subCategoryCollection = _projectTemplate.Categories.Where(p => (p.RootParentId == Convert.ToInt32(categoryId, CultureInfo.InvariantCulture)));

            if (subCategoryCollection.Count() >= 1)
            {
                int sameSubCategoryId = 0;
                int sameSubCategoryList = 1;
                foreach (Category subcategory in subCategoryCollection)
                {
                    Panel subcategoryPanel = new Panel();

                    Category secondLevelParent = _projectTemplate.Categories.Where(p => p.CategoryId == subcategory.ParentCategoryId).Single();

                    if (secondLevelParent.ParentCategoryId == -1)
                        subcategoryPanel.Attributes.Add("class", "panel");

                    string subcategoryName = subcategory.CategoryName;
                    string subcategoryLevel = subcategory.Level.ToString(CultureInfo.InvariantCulture);
                    string subcategoryId = subcategory.CategoryId.ToString(CultureInfo.InvariantCulture);
                    if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                    {
                        subcategoryPanel.ID = "panel" + subcategoryId + "_" + sameSubCategoryList;
                        sameSubCategoryList = sameSubCategoryList + 1;
                    }
                    else
                    {
                        subcategoryPanel.ID = "panel" + subcategoryId;
                        sameSubCategoryList = 1;
                    }
                    int marginFromLeft = (Convert.ToInt32(subcategoryLevel, CultureInfo.InvariantCulture) - 1) * 30;

                    if (subcategory.IsLeaf)
                    {
                        HtmlTable categoryDetailsTable = new HtmlTable();
                        categoryDetailsTable.Attributes.Add("class", "cat-table");
                        categoryDetailsTable.CellPadding = 0;
                        categoryDetailsTable.CellSpacing = 0;
                        HtmlTableRow row = new HtmlTableRow();
                        HtmlTableCell cell;

                        HtmlGenericControl headingApply = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        HtmlGenericControl applyBorder = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        HtmlGenericControl divStandards = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        divStandards.ViewStateMode = ViewStateMode.Enabled;
                        divStandards.ClientIDMode = ClientIDMode.Static;

                        if (subcategory.IsNewPartnersRequired.HasValue && subcategory.IsNewPartnersRequired.Value)
                        {
                            for (int colCount = 1; colCount <= 5; colCount++)
                            {
                                cell = new HtmlTableCell();

                                if (colCount == 1)
                                {
                                    CheckBox opportunityCheckBox = new CheckBox();
                                    opportunityCheckBox.ClientIDMode = ClientIDMode.Static;
                                    var id = new StringBuilder();
                                    id.Append(subcategory.CategoryId);
                                    id.Append("~");
                                    id.Append(subcategory.CategoryName);
                                    id.Append("~");
                                    id.Append(subcategory.BrandId);
                                    id.Append("~");
                                    id.Append(subcategory.BrandName);
                                    //id.Append("~");
                                    //id.Append(subcategory.PropertyTypeId);
                                    //id.Append("~");
                                    //id.Append(subcategory.PropertyTypeName);
                                    id.Append("~");
                                    id.Append(subcategory.RegionId);
                                    id.Append("~");
                                    id.Append(subcategory.RegionName);
                                    id.Append("~");
                                    id.Append(subcategory.CountryId);
                                    id.Append("~");
                                    id.Append(subcategory.CountryName);
                                    opportunityCheckBox.ID = Convert.ToString(id, CultureInfo.InvariantCulture);

                                    opportunityCheckBox.Attributes.Add("runat", "server");
                                    cell.Attributes.Add("class", "col1");
                                    cell.Controls.Add(opportunityCheckBox);
                                }
                                else if (colCount == 2)
                                {
                                    Label brandLabel = new Label();
                                    cell.Controls.Add(brandLabel);
                                    if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                                    {
                                        brandLabel.ID = "lblBrand_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                        sameSubCategoryList = sameSubCategoryList + 1;
                                    }
                                    else
                                    {
                                        brandLabel.ID = "lblBrand_" + subcategory.CategoryId;
                                        sameSubCategoryList = 1;
                                    }
                                    brandLabel.Text = subcategory.BrandName;
                                    brandLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col2");
                                }
                                
                                else if (colCount == 3)
                                {
                                    Label regionLabel = new Label();
                                    cell.Controls.Add(regionLabel);
                                    if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                                    {
                                        regionLabel.ID = "lblRegion_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                        sameSubCategoryList = sameSubCategoryList + 1;
                                    }
                                    else
                                    {
                                        regionLabel.ID = "lblRegion_" + subcategory.CategoryId;
                                        sameSubCategoryList = 1;
                                    }

                                    regionLabel.Text = subcategory.RegionName;
                                    regionLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col3");

                                }
                                else if (colCount == 4)
                                {
                                    /////////////////// For Country///////////////
                                    Label countryLabel = new Label();
                                    cell.Controls.Add(countryLabel);
                                    if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                                    {
                                        countryLabel.ID = "lblCountry_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                        sameSubCategoryList = sameSubCategoryList + 1;
                                    }
                                    else
                                    {
                                        countryLabel.ID = "lblCountry_" + subcategory.CategoryId;
                                        sameSubCategoryList = 1;
                                    }

                                    countryLabel.Text = subcategory.CountryName;
                                    countryLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col3");

                                }
                                else if (colCount == 5)
                                {
                                    if (!string.IsNullOrEmpty(subcategory.StandardDescription))
                                    {
                                        LinkButton standardsLinkButton = new LinkButton();
                                        standardsLinkButton.ClientIDMode = ClientIDMode.Static;
                                        standardsLinkButton.ViewStateMode = ViewStateMode.Enabled;
                                        if (sameSubCategoryId != 0 && sameSubCategoryId == subcategory.CategoryId)
                                        {
                                            standardsLinkButton.ID = "lnkShow_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                            divStandards.ID = "divStandards_" + subcategory.CategoryId + "_" + sameSubCategoryList;
                                            sameSubCategoryList = sameSubCategoryList + 1;
                                        }
                                        else
                                        {
                                            standardsLinkButton.ID = "lnkShow_" + subcategory.CategoryId;
                                            divStandards.ID = "divStandards_" + subcategory.CategoryId;
                                            sameSubCategoryList = 1;
                                        }

                                        standardsLinkButton.Text = ResourceUtility.GetLocalizedString(669, culture);
                                        standardsLinkButton.Font.Bold = true;
                                        //Create event name
                                        var eventName = new StringBuilder();
                                        eventName.Append("SelectClickedButton('");
                                        eventName.Append(standardsLinkButton.ID);
                                        eventName.Append("','");
                                        eventName.Append(divStandards.ID);
                                        eventName.Append("');return false");
                                        standardsLinkButton.OnClientClick = Convert.ToString(eventName, CultureInfo.InvariantCulture);

                                        cell.Controls.Add(standardsLinkButton);

                                        divStandards.InnerHtml = "<table><tr><td class='abc'><ul>" + Server.HtmlDecode(subcategory.StandardDescription) + "</ul></td></tr></table>";
                                        cell.Attributes.Add("class", "col5");
                                    }
                                }
                                row.Cells.Add(cell);
                            }

                            categoryDetailsTable.Rows.Add(row);

                            divStandards.Attributes.Add("class", "description");
                            if (subcategory.CategoryId != 0 && subcategory.CategoryId != sameSubCategoryId)
                            {
                                HtmlGenericControl headingSubCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H6");
                                headingSubCategoryName.InnerText = subcategoryName;
                                headingSubCategoryName.Style.Add("padding-left", marginFromLeft + "px");
                                subcategoryPanel.Controls.Add(headingSubCategoryName);

                                sameSubCategoryId = subcategory.CategoryId;
                                headingApply.InnerHtml = ResourceUtility.GetLocalizedString(229, culture);
                                headingApply.Attributes.Add("class", "apply");

                                applyBorder.Style.Add("border-bottom", "1px dotted");
                                subcategoryPanel.Controls.Add(headingApply);
                                subcategoryPanel.Controls.Add(applyBorder);
                            }
                        }

                        subcategoryPanel.Controls.Add(categoryDetailsTable);
                        subcategoryPanel.Controls.Add(divStandards);
                    }
                    else
                    {
                        HtmlGenericControl headingSubCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H6");
                        headingSubCategoryName.InnerText = subcategoryName;
                        headingSubCategoryName.Style.Add("padding-left", marginFromLeft + "px");
                        subcategoryPanel.Controls.Add(headingSubCategoryName);
                    }

                    if (categoryPanel.ID.ToString(CultureInfo.InvariantCulture) == "panel" + subcategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))

                        categoryPanel.Controls.Add(subcategoryPanel);

                    else //last;
                    {
                        Panel pnlParent = new Panel();
                        foreach (Control p in categoryPanel.Controls)
                        {
                            if (p is Panel)
                            {
                                pnlParent = (Panel)FindControl(categoryPanel, "panel" + subcategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                if (pnlParent != null)
                                    pnlParent.Controls.Add(subcategoryPanel);
                            }
                        }
                    }
                }
            }
            else
            {
                if (category.IsLeaf)
                {
                    HtmlTable categoryDetailsTable = new HtmlTable();
                    categoryDetailsTable.Attributes.Add("class", "cat-table");
                    categoryDetailsTable.CellPadding = 0;
                    categoryDetailsTable.CellSpacing = 0;

                    IEnumerable<Category> sameParentAndLeafCategoryCollection = _projectTemplate.Categories.Where(p => (p.CategoryId == Convert.ToInt32(categoryId, CultureInfo.InvariantCulture) && p.ParentCategoryId == -1 && p.IsLeaf && p.BrandName != string.Empty));

                    HtmlTableRow row;// = new HtmlTableRow();
                    HtmlTableCell cell;

                    HtmlGenericControl headingApply = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    HtmlGenericControl applyBorder = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    HtmlGenericControl divStandards = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    divStandards.ClientIDMode = ClientIDMode.Static;
                    int panelIndex = 0;
                    foreach (Category category1 in sameParentAndLeafCategoryCollection)
                    {
                        panelIndex += 1;
                        Panel subcategoryPanel1 = new Panel();
                        subcategoryPanel1.ID = "panel" + category1.CategoryId + "_" + panelIndex;
                        if (category1.IsNewPartnersRequired.HasValue && category1.IsNewPartnersRequired.Value)
                        {
                            row = new HtmlTableRow();
                            for (int colCount = 1; colCount <= 4; colCount++)
                            {
                                cell = new HtmlTableCell();

                                if (colCount == 1)
                                {
                                    CheckBox opportunityCheckBox = new CheckBox();
                                    opportunityCheckBox.ClientIDMode = ClientIDMode.Static;
                                    //Create the check box id
                                    var id = new StringBuilder();
                                    id.Append(category1.CategoryId);
                                    id.Append("~");
                                    id.Append(category1.CategoryName);
                                    id.Append("~");
                                    id.Append(category1.BrandId);
                                    id.Append("~");
                                    id.Append(category1.BrandName);
                                    //id.Append("~");
                                    //id.Append(category1.PropertyTypeId);
                                    //id.Append("~");
                                    //id.Append(category1.PropertyTypeName);
                                    id.Append("~");
                                    id.Append(category1.RegionId);
                                    id.Append("~");
                                    id.Append(category1.RegionName);
                                    opportunityCheckBox.ID = Convert.ToString(id, CultureInfo.InvariantCulture);
                                    opportunityCheckBox.Attributes.Add("runat", "server");
                                    cell.Controls.Add(opportunityCheckBox);
                                    cell.Attributes.Add("class", "col1");
                                }
                                else if (colCount == 2)
                                {
                                    Label brandLabel = new Label();
                                    cell.Controls.Add(brandLabel);
                                    brandLabel.ID = "lblBrand_" + category1.CategoryId;
                                    brandLabel.Text = category1.BrandName;
                                    brandLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col2");
                                }
                               
                                else if (colCount == 3)
                                {
                                    Label regionLabel = new Label();
                                    cell.Controls.Add(regionLabel);
                                    regionLabel.ID = "lblRegion_" + category1.CategoryId;
                                    regionLabel.Text = category1.RegionName;
                                    regionLabel.Font.Bold = true;
                                    cell.Attributes.Add("class", "col3");
                                }
                                else if (colCount == 4)
                                {
                                    if (!string.IsNullOrEmpty(category1.StandardDescription))
                                    {
                                        LinkButton standardsLinkButton = new LinkButton();
                                        standardsLinkButton.ClientIDMode = ClientIDMode.Static;
                                        standardsLinkButton.ID = "lnkShow_" + category1.CategoryId;
                                        divStandards.ID = "divStandards_" + category1.CategoryId;

                                        cell.Controls.Add(standardsLinkButton);
                                        standardsLinkButton.Text = ResourceUtility.GetLocalizedString(669, culture);
                                        standardsLinkButton.Font.Bold = true;

                                        var eventName = new StringBuilder();
                                        eventName.Append("SelectClickedButton('");
                                        eventName.Append(standardsLinkButton.ID);
                                        eventName.Append("','");
                                        eventName.Append(divStandards.ID);
                                        eventName.Append("');return false");

                                        standardsLinkButton.OnClientClick = Convert.ToString(eventName, CultureInfo.InvariantCulture);
                                        divStandards.InnerHtml = "<table><tr><td class='abc'><ul>" + Server.HtmlDecode(category1.StandardDescription) + "</ul></td></tr></table>";
                                        cell.Attributes.Add("class", "col5");
                                    }
                                }

                                row.Cells.Add(cell);
                            }

                            categoryDetailsTable.Rows.Add(row);

                            divStandards.Attributes.Add("class", "description");
                            headingApply.InnerHtml = ResourceUtility.GetLocalizedString(229, culture);
                            headingApply.Attributes.Add("class", "apply");
                            applyBorder.Style.Add("border-bottom", "1px dotted");
                            subcategoryPanel1.Controls.Add(headingApply);
                            subcategoryPanel1.Controls.Add(applyBorder);
                            subcategoryPanel1.Controls.Add(categoryDetailsTable);
                            subcategoryPanel1.Controls.Add(divStandards);
                        }
                        else
                        {
                            categoryPanel.Controls.Remove(headingCategoryName);
                        }
                        categoryPanel.Controls.Add(subcategoryPanel1);
                    }
                }
            }
            return categoryPanel;
        }

        /// <summary>
        /// Find Control Occurrence
        /// </summary>
        /// <param name="substr"></param>
        /// <returns></returns>
        private int FindOccurence(string substr)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int count = 0;
            for (int i = 0; i < ctrls.Length; i++)
            {
                if (ctrls[i].Contains(substr + "-") && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Find populated DropDowns
        /// </summary>
        /// <param name="selectedCategoryId"></param>
        /// <param name="id"></param>
        private void FindPopulateDropDowns(string selectedCategoryId, string id)
        {
            DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, id);
            PopulateDropDown(newDropDown, selectedCategoryId);
        }

        /// <summary>
        /// List Child Categories
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

            IList<Category> categoryList = (categories.AsEnumerable().
                                           Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture))).ToList());

            //To put logic to filter per selected BRT
            IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);
            int brandSelectedValue = Convert.ToInt32(brandsDropDown.SelectedValue, CultureInfo.InvariantCulture);
            int regionSelectedValue = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture);
          
            var filterdCatgories = categoryList.Join(filteredHierarchyCBRTData, c => c.CategoryId, fh => fh.CatId, (categ, filterHier) => new { categ, filterHier })
                                    .Where(item => ((regionSelectedValue == 0) ? true : item.filterHier.RegionId.Equals(regionSelectedValue))
                                                                                                     && ((brandSelectedValue == 0) ? true : item.filterHier.BrandId.Equals(brandSelectedValue))
                                                                                                     ).Select(y => y.categ).Distinct().ToList();

            return filterdCatgories;
        }

        /// <summary>
        /// List Root Categories
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetRootLevelCategories()
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            IList<Category> rootCategoryList = (categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
            return rootCategoryList;
        }

        /// <summary>
        /// Populate Dropdowns
        /// </summary>
        /// <param name="parentCategoryDropDown"></param>
        /// <param name="selectedValue"></param>
        private void PopulateDropDown(DropDownList parentCategoryDropDown, string selectedValue)
        {
            parentCategoryDropDown.Items.Clear();
            IList<Category> categoryList = new List<Category>();
            if (selectedValue.Equals(String.Empty))
            {
                categoryList = GetRootLevelCategories();
            }
            else if (selectedValue != "0")
            {
                categoryList = GetChildCategories(selectedValue);
            }
            if (categoryList != null)
            {
                BindData(parentCategoryDropDown, categoryList);
            }
            parentCategoryDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(223, culture), "0"));
        }

        /// <summary>
        /// Populate Dropdown data
        /// </summary>
        private void PopulateDropDownData()
        {

            CategoriesFilterHierarchy searchFilters = _partnerManager.GetPartnershipOpportunitiesSearchFilterItems();
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilters.FilteredCBRTData);
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData, searchFilters.FilteredHierarchyCBRTData);

            IList<Category> categoryItems = _partnerManager.GetBecomePartnerCategories;
            SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categoryItems);
            BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.CountryDropDown, CountryDropDown, ResourceUtility.GetLocalizedString(1212, culture), null);
            BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.CategoriesDropDown, categoriesDropDown, "Select Category", null);
            BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(204, culture), null);
            BindDropDown(searchFilters.FilteredCBRTData, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(219, culture), null);
        }

        /// <summary>
        /// Get Categories from view state
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(int currentSelectedCategoryId)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            Category category = (Category)(categories.Where(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)).Single());
            return category;
        }

        /// <summary>
        /// Recreate Controls
        /// </summary>
        /// <param name="ctrlPrefix"></param>
        /// <param name="ctrlType"></param>
        private void RecreateControls(string ctrlPrefix, string ctrlType)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int cnt = FindOccurence(ctrlPrefix);
            if (cnt > 0)
            {
                for (int k = 1; k <= cnt; k++)
                {
                    for (int i = 0; i < ctrls.Length; i++)
                    {
                        if (ctrls[i].Contains(ctrlPrefix + "-" + (k + 1).ToString(CultureInfo.InvariantCulture)) && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                        {
                            string ctrlID = string.Empty;
                            string[] subCtrls = ctrls[i].Split('-');

                            for (int l = 0; l < subCtrls.Length; l++)
                            {
                                if (subCtrls[l].Contains("categoriesDropDown"))
                                {
                                    ctrlID = subCtrls[l + 1].Substring(0, 1);
                                }
                            }

                            ctrlID = "categoriesDropDown-" + ctrlID;

                            if (ctrlType == "DropDownList")
                            {
                                CreateDropDownList(ctrlID);
                            }
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="totalCategoryDropDownCount"></param>
        private void ProcessCategories(int totalCategoryDropDownCount = 0)
        {
            try
            {
                int existingDropDownCount;
                if (totalCategoryDropDownCount == 0)
                    existingDropDownCount = FindOccurence("categoriesDropDown");
                else
                    existingDropDownCount = totalCategoryDropDownCount;

                IList<SearchFilters> filteredHierarchyCBRTData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterHierarchySearchData);

                string parentSelectedValue;
                DropDownList parentCategoryDropDown;
                for (int dropdownCount = 1; dropdownCount <= existingDropDownCount; dropdownCount++)
                {
                    DropDownList subCategoryDropDown = null;
                    if (dropdownCount == 1)
                    {
                        parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown");
                        parentSelectedValue = parentCategoryDropDown.SelectedValue;
                        if (existingDropDownCount == 1)
                            subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount + 1).ToString(CultureInfo.InvariantCulture));
                    }
                    else if (dropdownCount == 2)
                    {
                        parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown");
                        parentSelectedValue = parentCategoryDropDown.SelectedValue;
                        subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        parentCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount - 1).ToString(CultureInfo.InvariantCulture));
                        parentSelectedValue = parentCategoryDropDown.SelectedValue;
                        subCategoryDropDown = (DropDownList)parentCategoryContainer.FindControl("categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
                    }

                    if (subCategoryDropDown != null)
                    {
                        string selectedValue = subCategoryDropDown.SelectedValue;
                        if (existingDropDownCount == 1)
                            FindPopulateDropDowns(parentSelectedValue, "categoriesDropDown-" + (dropdownCount + 1).ToString(CultureInfo.InvariantCulture));
                        else
                            FindPopulateDropDowns(parentSelectedValue, "categoriesDropDown-" + (dropdownCount).ToString(CultureInfo.InvariantCulture));
                        if (Convert.ToInt16(selectedValue) > 0)
                            subCategoryDropDown.SelectedValue = selectedValue;

                      
                        IList<ListItem> dropDownDirtyListItem = new List<ListItem>();
                        foreach (ListItem dropDownListItem in subCategoryDropDown.Items)
                        {
                            if (Convert.ToInt16(dropDownListItem.Value) > 0)
                            {
                                IList<SearchFilters> filteredCollection = Helper.GetFilteredCollection(filteredHierarchyCBRTData, Convert.ToInt32(brandsDropDown.SelectedValue), Convert.ToInt32(regionDropDown.SelectedValue), 0, Convert.ToInt32(dropDownListItem.Value));

                                if (filteredCollection.Count == 0)
                                    dropDownDirtyListItem.Add(dropDownListItem);
                            }
                        }

                        if (dropDownDirtyListItem.Count > 0)
                            dropDownDirtyListItem.ToList().ForEach(x => subCategoryDropDown.Items.Remove(x));
                    }
                }
                SetDropdownDefaultText();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }
        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            if (hdn.Value.ToString().Length > 0)
            {
                try
                {

                    FilterCriteria filterCriteria = new FilterCriteria()
                    {
                        CategoryId = Convert.ToInt16(hdn.Value),
                    };

                    SetSession<FilterCriteria>(SessionConstants.FilterCriteria, filterCriteria);

                    string allLevelCategories = string.Empty;
                    allLevelCategories = categoriesDropDown.SelectedValue;
                    for (int i = 1; i < parentCategoryContainer.Controls.Count + 1; i++)
                    {
                        DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, "categoriesDropDown-" + (i + 1).ToString(CultureInfo.InvariantCulture));
                        allLevelCategories += "~" + newDropDown.SelectedValue;
                    }
                    SetSession<string>(SessionConstants.AllCascadeCategoryIds, allLevelCategories);

                    _projectTemplate = _partnerManager.GetCategoriesForBecomePartner(Convert.ToInt32(hdn.Value), Convert.ToInt32(0, CultureInfo.InvariantCulture), Convert.ToInt32(0, CultureInfo.InvariantCulture), Convert.ToInt32(0, CultureInfo.InvariantCulture));

                    if (_projectTemplate.TermsAndCondition.VersionId != 0)
                    {
                        SetSession<TermsAndConditions>(SessionConstants.TermsAndCondition, new TermsAndConditions() { VersionId = _projectTemplate.TermsAndCondition.VersionId, TermsAndConditionDesc = _projectTemplate.TermsAndCondition.TermsAndConditionDesc });
                    }

                    IList<Category> rootCategoryList = (_projectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();

                    // Only bind the root categories with the repeater and the repeater code will internally manage the sub level creation
                    if (rootCategoryList != null && rootCategoryList.Count != 0)
                    {
                        categoryDivisionsRepeater.DataSource = rootCategoryList;
                        categoryDivisionsRepeater.DataBind();
                    }
                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoRecordsFound, culture));
                    }
                    selectACategorydiv.Style.Add("display", "none");
                    categoryDetailsUpdatePanel.Update();

                }
                catch (Exception exception)
                {
                    UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }
            }
            else
            {
                ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoRecordsFound, culture));
            }

        }

        private void SetDropdownDefaultText()
        {
            categoriesDropDown.Items[0].Text = "Select Category";
           // regionDropDown.Items[0].Text = "Select Region";

        }
    }
}