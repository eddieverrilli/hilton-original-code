﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class PartnerProfile : PageBase
    {
        private Partner partner = new Partner();
        private ProjectTemplate projectTemplate;
        private Panel productPanel;
        private Panel brandRegionPropertyPanel;
        private Panel categoryPanel;
        private int partnerId;
        private int sameCategoryId = 0;
        private int sameRankRow = 0;
        private string sameBrandPropertyAndRegionId = string.Empty;

        private IPartnerManager _partnerManager;
        private IProductManager _productManager;
        private IHelperManager _helperManager;
        private User loggedInUser;

        /// <summary>
        /// Invoked On Init
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                User user = GetSession<User>(SessionConstants.User);
                if (IsSessionTimedout())
                {
                    ClearSession(SessionConstants.RecommendedPartnerSearch);
                }
                else if (user == null)
                {
                    ClearSession(SessionConstants.RecommendedPartnerSearch);
                    Response.Redirect("~/AccessDenied.aspx", false);
                }
                _partnerManager = PartnerManager.Instance;
                _productManager = ProductManager.Instance;
                _helperManager = HelperManager.Instance;

                bool isBecomePartnerAccessible = ValidateMenuAuthorization((int)MenuEnum.BecomeRPartner);
                if (!isBecomePartnerAccessible)
                    becomePartnerListItem.Visible = false;
                else
                    becomePartnerListItem.Visible = true;

                this.Title = ResourceUtility.GetLocalizedString(83, culture, "Partner Profile");

                LinkButton hprlnkLoginLogout = new LinkButton();
                hprlnkLoginLogout = (LinkButton)(this.Master.FindControl("loginLinkButton"));
                hprlnkLoginLogout.Text = "Logout";

                backToListHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "‹‹ {0}", ResourceUtility.GetLocalizedString(122, culture, "Back To List"));
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                loggedInUser = (User)Session[SessionConstants.User];

                userTypeHiddenField.Value = Convert.ToString(loggedInUser.UserTypeId);

                partnerId = Convert.ToInt32(GetSession<string>(SessionConstants.PartnerId), CultureInfo.InvariantCulture);

                if (!Page.IsPostBack || string.IsNullOrEmpty(Request["__EVENTTARGET"]))//|| (!IsPostBack && !string.IsNullOrEmpty(Request["__EVENTTARGET"]) && Request["__EVENTTARGET"].ToString().Contains("categoryProductsRepeater")))
                {

                    PartnerSearch searchCriteriaFromRecommendedPartnerPg = GetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch);
                    Tuple<IList<SearchFilters>, Partner> partnerData = null;
                    if (searchCriteriaFromRecommendedPartnerPg != null)
                    {
                        partnerData = _partnerManager.GetPartnerProfileDetails(partnerId, searchCriteriaFromRecommendedPartnerPg.BrandId, searchCriteriaFromRecommendedPartnerPg.PropertyTypeId, searchCriteriaFromRecommendedPartnerPg.RegionId, searchCriteriaFromRecommendedPartnerPg.CountryId, searchCriteriaFromRecommendedPartnerPg.CategoryId);
                    }
                    else
                    {
                        partnerData = _partnerManager.GetPartnerProfileDetails(partnerId, "0", "0", "0", "0", "0");
                    }

                    //Tuple<IList<SearchFilters>, Partner> partnerData = _partnerManager.GetPartnerProfileDetails(partnerId);
                    if (partnerData != null)
                    {
                        PopulateDropDownData(partnerData.Item1);
                    }

                    partner = partnerData.Item2;
                    if (partner != null)
                    {
                        logoImage.ImageUrl = partner.CompanyLogoImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(partner.CompanyLogoImage) : "../Images/NoProduct.PNG";
                        if (partner.CompanyLogoImage == null)
                        {
                            logoImage.Visible = false;
                        }
                        rightPanelHeader.Text = ResourceUtility.GetLocalizedString(3, culture, "Recommended Partners").ToUpper();

                        vendorNameLabel.Text = partner.CompanyName;
                        descriptionLabel.Text = partner.CompanyDescription;

                        if (partner.IsSAM)
                        {
                            hiltonIconImage.Visible = true;
                            vendorLabel.Visible = true;
                        }
                        else
                        {
                            hiltonIconImage.Visible = false;
                            vendorLabel.Visible = false;
                        }

                        // SetSession(SessionConstants.PartnerContacts, partner.Contacts);
                        SetSession(SessionConstants.PartnerProfile, partner);

                        Contact primaryContactInfo = partner.Contacts.Where(p => p.ContactTypeId == 1).FirstOrDefault();

                        if (primaryContactInfo != null)
                        {
                            nameLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0} {1}", primaryContactInfo.FirstName, primaryContactInfo.LastName);// primaryContactInfo.FirstName + " " + primaryContactInfo.LastName;

                            if (!string.IsNullOrEmpty(partner.CompanyAddress2))
                            {
                                addressLabel.Text = string.Concat("<br/>", partner.CompanyAddress1, "<br/>", partner.CompanyAddress2, "<br/>");
                                //address2Label.Text = partner.CompanyAddress2;//string.Format("&nbsp;{0}, <br />", partner.CompanyAddress2);
                            }
                            else
                            {
                                addressLabel.Text = string.Concat("<br/>", partner.CompanyAddress1, "<br/>"); // string.Format("{0}, <br />", partner.CompanyAddress1);
                            }

                            if (!string.IsNullOrEmpty(partner.City))
                            {
                                cityStateZipLabel.Text = partner.City;
                            }

                            if (!string.IsNullOrEmpty(partner.State))
                            {
                                if (string.IsNullOrEmpty(cityStateZipLabel.Text))
                                    cityStateZipLabel.Text = partner.State;
                                else
                                    cityStateZipLabel.Text = string.Concat(cityStateZipLabel.Text, ", ", partner.State);
                            }

                            if (!string.IsNullOrEmpty(partner.ZipCode))
                            {

                                if (partner.ZipCode != "0")
                                    cityStateZipLabel.Text = string.Concat(cityStateZipLabel.Text, "<br/>", partner.ZipCode, "<br/>");
                            }

                            if (!string.IsNullOrEmpty(partner.Country))
                            {
                                if (!string.IsNullOrEmpty(partner.ZipCode))
                                    countryLabel.Text = string.Format("{0}<br />", partner.Country);
                                else
                                    countryLabel.Text = string.Format("<br/>{0}<br />", partner.Country);
                            }

                            if (userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Owner)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Consultant)))
                            {
                                emailHyperLink.Text = primaryContactInfo.Email;
                                emaillink.Visible = false;
                            }
                            else
                            {
                                emaillink.Visible = true;
                                emailHyperLink.Visible = false;
                                emaillink.HRef = "mailto:" + primaryContactInfo.Email;
                                emaillink.InnerText = primaryContactInfo.Email;

                                if (string.IsNullOrEmpty(primaryContactInfo.Title))
                                {
                                    TitleLabel.Visible = false;
                                    addressLabel.Text = addressLabel.Text.Remove(0, 5); //remove<br>
                                }
                                else
                                {
                                    TitleLabel.Visible = Visible;
                                    TitleLabel.Text = primaryContactInfo.Title;

                                }
                            }
                            phoneLabel.Text = primaryContactInfo.Phone;
                            webSiteLink.InnerText = primaryContactInfo.Website;
                            if (primaryContactInfo.Website.Contains("http://") || primaryContactInfo.Website.Contains("https://"))
                                webSiteLink.HRef = primaryContactInfo.Website;
                            else
                                webSiteLink.HRef = "http://" + primaryContactInfo.Website;
                            contactIdHiddenValue.Value = primaryContactInfo.ContactId;
                            //contactTypeIdHiddenValue.Value = primaryContactInfo.ContactTypeIdIndex.ToString(CultureInfo.InvariantCulture);
                            toFirstNameHiddenValue.Value = primaryContactInfo.FirstName.ToString(CultureInfo.InvariantCulture);
                            toLastNameHiddenValue.Value = primaryContactInfo.LastName.ToString(CultureInfo.CurrentCulture);
                        }

                        if (userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Owner)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Administrator)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Consultant)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Corporate)))
                        {
                            commentIconImage.NavigateUrl = "~/Partners/SubmitFeedback.aspx?keepThis=true&partnerName=" + HttpUtility.UrlEncode(vendorNameLabel.Text) + "&partnerId=" + partnerId + "&toFirstName=" + toFirstNameHiddenValue.Value + "&toLastName=" + toLastNameHiddenValue.Value + "&toEmail=" + emailHyperLink.Text + "&TB_iframe=true&height=440&width=800";
                        }
                        else
                        {
                            commentIconImage.Visible = false;
                        }
                        selectedLnkEmailHiddenValue.Value = emailHyperLink.Text;
                        partnerIdHiddenValue.Value = Convert.ToString(partnerId, CultureInfo.InvariantCulture);
                        regionIdHiddenValue.Value = "0";
                    }

                    //projectTemplate = _partnerManager.GetPartnerAppCatAndProducts(partnerId);
                    projectTemplate = GetGetPartnerAppCatAndProducts();
                    IList<PartnerProductTrial> partnerProductTrial = (projectTemplate.PartnerProductTrial.AsEnumerable()).ToList();
                    if (partnerProductTrial.Count != 0)
                    {
                        categoryProductsRepeater.DataSource = partnerProductTrial;
                        categoryProductsRepeater.DataBind();
                    }
                    else
                    {
                        ConfigureResultMessage(resultCategoriesDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoRecordsFound, culture));
                    }
                }
                else if (IsPostBack && !string.IsNullOrEmpty(Request["__EVENTTARGET"]) && Request["__EVENTTARGET"].ToString().Contains("categoryProductsRepeater"))
                {

                    projectTemplate = GetGetPartnerAppCatAndProducts();

                    IList<PartnerProductTrial> partnerProductTrial = (projectTemplate.PartnerProductTrial.AsEnumerable()).ToList();

                    if (partnerProductTrial != null && partnerProductTrial.Count > 0)
                    {
                        categoryProductsRepeater.DataSource = partnerProductTrial;
                        categoryProductsRepeater.DataBind();
                    }
                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoRecordsFound, culture));
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(contactUpdatePanel, this.GetType(), "ForceCall", "ForceCall();", true);
                    ScriptManager.RegisterClientScriptBlock(categoriesResultUpdatePanel, categoriesResultUpdatePanel.GetType(), "forceHover", "ForceHover();", true);
                    ScriptManager.RegisterClientScriptBlock(categoriesResultUpdatePanel, categoriesResultUpdatePanel.GetType(), "ForceCall", "ForceCall();", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Render
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Go LinkButton Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void GoLinkButton_Click(object sender, EventArgs e)
        {

            try
            {

                projectTemplate = GetGetPartnerAppCatAndProducts();


                IList<PartnerProductTrial> partnerProductTrial = (projectTemplate.PartnerProductTrial.AsEnumerable()).ToList();

                if (partnerProductTrial != null && partnerProductTrial.Count > 0)
                {
                    categoryProductsRepeater.DataSource = partnerProductTrial;
                    categoryProductsRepeater.DataBind();
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoRecordsFound, culture));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// ItemDataBound of repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void CategoryProductsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e != null && e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl categoryDivision = (HtmlGenericControl)e.Item.FindControl("categoryProductsDivision");
                    Panel subcategoryPanel = new Panel();

                    subcategoryPanel = CreateCategoryProductDivision((Entities.PartnerProductTrial)(e.Item.DataItem));
                    categoryDivision.Controls.Add(subcategoryPanel);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked to Create CategoryProduct Divisions
        /// </summary>
        /// <param name="partnerProductTrial"></param>
        /// <returns></returns>
        private Panel CreateCategoryProductDivision(PartnerProductTrial partnerProductTrial)
        {

            string categoryName = partnerProductTrial.CategoryDisplayName;
            string categoryId = partnerProductTrial.CatId.ToString(CultureInfo.InvariantCulture);

            if (sameCategoryId != partnerProductTrial.CatId)
            {
                categoryPanel = new Panel();
                categoryPanel.ID = "panel" + categoryId;
                HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H3");
                headingCategoryName.InnerText = categoryName;
                sameCategoryId = partnerProductTrial.CatId;
                categoryPanel.Controls.Add(headingCategoryName);
            }

            HtmlTable headingSubDetailsTable = new HtmlTable();
            HtmlTable productsDetailsTable = new HtmlTable();

            HtmlTableRow headingSubDetailsRow = new HtmlTableRow();
            HtmlTableRow row = new HtmlTableRow();

            HtmlTableCell headingSubDetailsCell = new HtmlTableCell();
            HtmlTableCell cell;

            HtmlGenericControl headingSubDetails = new System.Web.UI.HtmlControls.HtmlGenericControl("H5");

            if (partnerProductTrial.ProductId > 0)
                headingSubDetails.Attributes.Add("class", "panel1");
            else
                headingSubDetails.Attributes.Add("class", "nostyle");

            string brandName = partnerProductTrial.BrandName.ToString(CultureInfo.InvariantCulture);
            //string propertyTypeName = partnerProductTrial.PropertyTypeName.ToString(CultureInfo.InvariantCulture);
            string regionName = partnerProductTrial.RegionName.ToString(CultureInfo.InvariantCulture);
            string countryName = partnerProductTrial.CountryName.ToString(CultureInfo.InvariantCulture);

            for (int headingSubDetailsCount = 1; headingSubDetailsCount <= 4; headingSubDetailsCount++)
            {
                if (headingSubDetailsCount == 1)
                {
                    Label brandNameLabel = new Label();
                    brandNameLabel.Text = brandName;
                    brandNameLabel.Attributes.Add("class", "brand");
                    headingSubDetailsCell.Controls.Add(brandNameLabel);
                }

                else if (headingSubDetailsCount == 3)
                {
                    Label regionNameLabel = new Label();
                    regionNameLabel.Text = regionName;
                    regionNameLabel.Attributes.Add("class", "country");
                    headingSubDetailsCell.Controls.Add(regionNameLabel);
                }
                else if (headingSubDetailsCount == 4)
                {
                    Label countryNameLabel = new Label();
                    countryNameLabel.Text = countryName;
                    headingSubDetailsCell.Controls.Add(countryNameLabel);
                }
                headingSubDetailsRow.Cells.Add(headingSubDetailsCell);
            }

            headingSubDetailsTable.Rows.Add(headingSubDetailsRow);
            headingSubDetails.Controls.Add(headingSubDetailsTable);

            if (sameRankRow != partnerProductTrial.Row)
            {
                productPanel = new Panel();
                brandRegionPropertyPanel = new Panel();
                brandRegionPropertyPanel.Controls.Add(headingSubDetails);
            }

            if (partnerProductTrial.ProductId != 0)
            {
                for (int colCount = 1; colCount <= 4; colCount++)
                {
                    cell = new HtmlTableCell();
                    if (colCount == 1)
                    {
                        if (partnerProductTrial.CompanyLogo != null)
                        {
                            Image companyLogo = new Image();
                            companyLogo.ImageUrl = partnerProductTrial.CompanyLogo != null ? "data:image/jpg;base64," + Convert.ToBase64String(partnerProductTrial.CompanyLogo) : "../Images/NoProduct.PNG";
                            cell.Attributes.Add("class", "col1");
                            companyLogo.Style.Add("float", "none");
                            companyLogo.CssClass = "pic imagestyle";
                            cell.Controls.Add(companyLogo);
                        }

                        HtmlGenericControl paraControl = new System.Web.UI.HtmlControls.HtmlGenericControl("p");
                        Label partnerName = new Label();
                        cell.Attributes.Add("class", "col1");
                        partnerName.Text = partnerProductTrial.CompanyName;
                        paraControl.Controls.Add(partnerName);
                        cell.Controls.Add(paraControl);
                    }
                    else if (colCount == 2)
                    {
                        ImageButton productImageButton = new ImageButton();

                        string[] tempString = partnerProductTrial.ImageName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                        string contentType;
                        if (tempString.Length > 0)
                        {
                            int upperBound = tempString.GetUpperBound(0);
                            contentType = "data:image/" + tempString[upperBound] + ";base64";
                        }
                        else
                        {
                            contentType = "data:image/png;base64";
                        }
                        productImageButton.ImageUrl = (partnerProductTrial.ProductImage != null && !string.IsNullOrEmpty(partnerProductTrial.ImageName)) ? contentType + "," + Convert.ToBase64String(partnerProductTrial.ProductImage) : "../Images/noproduct.png";
                        if (partnerProductTrial.ProductImage != null && !string.IsNullOrEmpty(partnerProductTrial.ImageName))
                        {
                            productImageButton.OnClientClick = string.Format("$(this).DownloadImage({{productId:'{0}'}});return false;", partnerProductTrial.ProductId);
                        }
                        else
                        {
                            productImageButton.OnClientClick = "return false;";
                            productImageButton.Visible = false;
                        }

                        cell.Attributes.Add("class", "col2");
                        productImageButton.Style.Add("width", "50px");
                        productImageButton.Style.Add("height", "50px");
                        productImageButton.CssClass = "pic";
                        cell.Controls.Add(productImageButton);
                    }
                    else if (colCount == 3)
                    {
                        Label productName = new Label();
                        productName.Text = partnerProductTrial.ProductName;
                        HtmlGenericControl headingproduction = new System.Web.UI.HtmlControls.HtmlGenericControl("H2");
                        headingproduction.Controls.Add(productName);

                        Label skuNumber = new Label();
                        if (!string.IsNullOrWhiteSpace(partnerProductTrial.SkuNumber.ToString(CultureInfo.InvariantCulture)))
                            skuNumber.Text = "SKU# " + partnerProductTrial.SkuNumber.ToString(CultureInfo.InvariantCulture);
                        else
                            skuNumber.Text = partnerProductTrial.SkuNumber.ToString(CultureInfo.InvariantCulture);

                        HtmlGenericControl div = new HtmlGenericControl("div");
                        div.Attributes.Add("class", "comment more");
                        Literal productDescription = new Literal();
                        if (!string.IsNullOrWhiteSpace(partnerProductTrial.ProductDescription))
                            productDescription.Text = partnerProductTrial.ProductDescription.ToString(CultureInfo.InvariantCulture);
                        else
                            productDescription.Text = partnerProductTrial.ProductDescription.ToString(CultureInfo.InvariantCulture);

                        div.Controls.Add(productDescription);
                        cell.Attributes.Add("class", "col3");
                        cell.Controls.Add(headingproduction);
                        cell.Controls.Add(skuNumber);
                        cell.Controls.Add(div);


                        if (!string.IsNullOrWhiteSpace(partnerProductTrial.SpecSheetPDFName))
                        {
                            HtmlGenericControl donwloadSection = new HtmlGenericControl("p");

                            Image pdfIconImage = new Image()
                            {
                                ImageUrl = @"~/Images/product/Images/pdf-icon.gif",
                                Width = 11,
                                Height = 11,
                                CssClass = "product"

                            };

                            LinkButton downLoadSpecSheetLinkButton = new LinkButton()
                            {
                                OnClientClick = string.Format("$(this).DownloadImage({{productId:'{0}',mode:'{1}'}});return false;", partnerProductTrial.ProductId, "1"),
                                Text = "Download Spec Sheet",
                                CssClass = "downloadPDFButton",
                            };

                            ScriptManager.GetCurrent(this).RegisterPostBackControl(downLoadSpecSheetLinkButton);

                            donwloadSection.Controls.Add(pdfIconImage);
                            donwloadSection.Controls.Add(downLoadSpecSheetLinkButton);
                            cell.Controls.Add(donwloadSection);

                        }

                    }
                    else if (colCount == 4)
                    {
                        if (userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Owner)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Administrator)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Consultant)) || userTypeHiddenField.Value == Convert.ToString(Convert.ToInt32(UserTypeEnum.Corporate)))
                        {
                            Literal comment = new Literal();
                            comment.Text = @"<span class='comment'>
                                                <a class='thickbox' href='/Partners/SubmitFeedback.aspx?keepThis=true&partnerName=" + HttpUtility.UrlEncode(vendorNameLabel.Text) + @"&productName=" + partnerProductTrial.ProductName.ToString(CultureInfo.InvariantCulture) + @"&skuNumber=" + partnerProductTrial.SkuNumber.ToString(CultureInfo.InvariantCulture) + @"&partnerId=" + partnerId + @"&productId=" + partnerProductTrial.ProductId + @"&toFirstName=" + partnerProductTrial.FirstName + @"&toLastName=" + partnerProductTrial.LastName + @"&toEmail=" + partnerProductTrial.Email + @"&TB_iframe=true&height=440&width=800'>
                                                    <img width='14' height='13' src='/images/product/comment-icon.png'>

                                                </a>
                                                <span class='tooltip'>
                                                    <img width='110' height='19' src='/images/product/submit-feedback.png'>
                                                </span>
                                            </span>
                                        ";

                            cell.Attributes.Add("class", "col4");
                            cell.Controls.Add(comment);
                        }
                    }

                    row.Cells.Add(cell);
                }

                productsDetailsTable.Rows.Add(row);

                productPanel.Controls.Add(productsDetailsTable);

                if (sameRankRow == partnerProductTrial.Row)
                {
                    int brandRegionTypePanelControlCount = brandRegionPropertyPanel.Controls.Count - 1;
                    brandRegionPropertyPanel.Controls.RemoveAt(brandRegionTypePanelControlCount);
                }
                else
                {
                    sameRankRow = partnerProductTrial.Row;
                }
                brandRegionPropertyPanel.Controls.Add(productPanel);
            }


            categoryPanel.Controls.Add(brandRegionPropertyPanel);

            return categoryPanel;
        }



        /// <summary>
        ///  Grid Row Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryProductsRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DownloadPDF", StringComparison.InvariantCultureIgnoreCase))
                {
                    int productId = Convert.ToInt32(e.CommandArgument, CultureInfo.InvariantCulture);
                    Product productPDF = _helperManager.GetProductPdf(productId);
                    string fileName = productPDF.SpecSheetPdfName;
                    byte[] fileBytes = productPDF.SpecSheetPdf;
                    ProcessFileDownload("pdf", fileBytes, fileName);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Process file download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            string extension = string.Empty;

            switch (fileType)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "image":
                    string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    int upperBound = tempString.GetUpperBound(0);
                    contentType = "image/" + tempString[upperBound];
                    break;
                default:
                    contentType = "";
                    break;
            };

            Response.Buffer = true;
            Response.Charset = "";
            Response.ClearHeaders();
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
            Response.BinaryWrite(fileBytes);
            Response.Flush();
            Response.Clear();
            Response.End();
        }

        /// <summary>
        /// Drop down selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] regions = { regionDropDown.SelectedValue };
                BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), 0, true, regions);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on DropDown selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownChange(object sender, EventArgs e)
        {
            try
            {
                partner = GetSession<Partner>(SessionConstants.PartnerProfile);

                Contact contactInfo1 = partner.Contacts.Where(p => p.CountryId == Convert.ToInt32(contactDropDown.SelectedValue, CultureInfo.InvariantCulture)).FirstOrDefault();


                nameLabel.Text = contactInfo1.FirstName + " " + contactInfo1.LastName;
                if (!string.IsNullOrEmpty(contactInfo1.Address2))
                {
                    addressLabel.Text = string.Concat("<br/>", contactInfo1.Address1, "<br/>", contactInfo1.Address2, "<br/>");
                    //address2Label.Text = contactInfo1.Address2; //string.Format("&nbsp;{0}, <br />", contactInfo1.Address2);
                }
                else
                {
                    addressLabel.Text = addressLabel.Text = string.Concat("<br/>", contactInfo1.Address1, "<br/>"); // string.Format("{0}, <br />", contactInfo1.Address1);
                }
                if (loggedInUser.UserTypeId == Convert.ToInt16(UserTypeEnum.Owner) || loggedInUser.UserTypeId == Convert.ToInt16(UserTypeEnum.Consultant))
                {
                    emailHyperLink.Text = contactInfo1.Email;
                    emaillink.Visible = false;
                }
                else
                {
                    emaillink.Visible = true;
                    emailHyperLink.Visible = false;
                    emaillink.HRef = "mailto:" + contactInfo1.Email;
                    emaillink.InnerText = contactInfo1.Email;
                    //  emailText.Text = contactInfo1.Email;
                    if (string.IsNullOrEmpty(contactInfo1.Title))
                    {
                        TitleLabel.Visible = false;
                        addressLabel.Text = addressLabel.Text.Remove(0, 5); //remove<br>
                    }
                    else
                    {
                        TitleLabel.Visible = Visible;
                        TitleLabel.Text = contactInfo1.Title;
                    }
                }
                phoneLabel.Text = contactInfo1.Phone;
                contactIdHiddenValue.Value = contactInfo1.ContactId;



                if (contactInfo1.ContactTypeId == 3)
                {
                    //countryLabel.Text = contactInfo1.CountryName;
                    if (!string.IsNullOrEmpty(contactInfo1.CountryName))
                    {
                        if (!string.IsNullOrEmpty(partner.ZipCode))
                            countryLabel.Text = string.Format("{0}<br />", contactInfo1.CountryName);
                        else
                            countryLabel.Text = string.Format("<br/>{0}<br />", contactInfo1.CountryName);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(partner.ZipCode))
                        countryLabel.Text = string.Format("{0}<br />", partner.Country);
                    else
                        countryLabel.Text = string.Format("<br/>{0}<br />", partner.Country);
                }
                //contactTypeIdHiddenValue.Value = contactInfo1.ContactTypeIdIndex.ToString(CultureInfo.InvariantCulture);
                toFirstNameHiddenValue.Value = contactInfo1.FirstName.ToString(CultureInfo.InvariantCulture);
                toLastNameHiddenValue.Value = contactInfo1.LastName.ToString(CultureInfo.InvariantCulture);
                selectedLnkEmailHiddenValue.Value = emailHyperLink.Text;
                partnerIdHiddenValue.Value = Convert.ToString(partnerId, CultureInfo.InvariantCulture);
                regionIdHiddenValue.Value = Convert.ToString(contactInfo1.RegionId, CultureInfo.InvariantCulture);

                logoImage.ImageUrl = partner.CompanyLogoImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(partner.CompanyLogoImage) : "../Images/NoProduct.PNG";
                rightPanelHeader.Text = ResourceUtility.GetLocalizedString(3, culture, "Recommended Partners").ToUpper();

                vendorNameLabel.Text = partner.CompanyName;
                if (loggedInUser.UserTypeId == Convert.ToInt16(UserTypeEnum.Owner) || loggedInUser.UserTypeId == Convert.ToInt16(UserTypeEnum.Consultant) || loggedInUser.UserTypeId == Convert.ToInt16(UserTypeEnum.Administrator) || loggedInUser.UserTypeId == Convert.ToInt16(Convert.ToInt32(UserTypeEnum.Corporate)))
                {
                    commentIconImage.NavigateUrl = "~/Partners/SubmitFeedback.aspx?keepThis=true&partnerName=" + HttpUtility.UrlEncode(vendorNameLabel.Text) + "&partnerId=" + partnerId + "&toFirstName=" + toFirstNameHiddenValue.Value + "&toLastName=" + toLastNameHiddenValue.Value + "&toEmail=" + emailHyperLink.Text + "&TB_iframe=true&height=440&width=800";
                }
                else
                {
                    commentIconImage.Visible = false;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked to populate Drop Downs
        /// </summary>
        public void PopulateDropDownData(IList<SearchFilters> searchFilters)
        {
            try
            {
                SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilters);
                BindDropDown(searchFilters, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), null);
                BindDropDown(searchFilters, DropDownConstants.ContactDropDownpartnerProfile, contactDropDown, null, null);
                BindDropDown(searchFilters, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(203, culture), null);
                BindDropDown(searchFilters, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(218, culture), null);

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked to Bind Drop Downs
        /// </summary>
        /// <param name="searchFiltersData"></param>
        /// <param name="dropDownType"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        /// <param name="selectedValue"></param>

        private void BindPageDropDown(IList<SearchFilters> searchFiltersData, string dropDownType, DropDownList sourceDropDown, string defaultText, int? selectedValue)
        {
            sourceDropDown.DataSource = Helper.GetSearchFilterDropDownData(dropDownType, searchFiltersData).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false);
            sourceDropDown.DataTextField = "DataTextField";
            sourceDropDown.DataValueField = "DataValueField";
            sourceDropDown.DataBind();
            if (!string.IsNullOrWhiteSpace(defaultText))
                sourceDropDown.Items.Insert(0, new ListItem(defaultText, "0"));
            if (selectedValue.HasValue)
                sourceDropDown.SelectedValue = selectedValue.ToString();
        }

        private ProjectTemplate GetGetPartnerAppCatAndProducts()
        {
            PartnerSearch searchCriteriaFromRecommendedPartnerPg = GetSession<PartnerSearch>(SessionConstants.RecommendedPartnerSearch);

            string brand = string.Empty;
            //  string PropertyType = string.Empty;
            string region = string.Empty;
            string country = string.Empty;
            string category = string.Empty;

            if (searchCriteriaFromRecommendedPartnerPg != null)
            {

                brand = brandDropDown.SelectedValue == "0" ? searchCriteriaFromRecommendedPartnerPg.BrandId : brandDropDown.SelectedValue;
                region = regionDropDown.SelectedValue == "0" ? searchCriteriaFromRecommendedPartnerPg.RegionId : regionDropDown.SelectedValue;
                country = countryDropDown.SelectedValue == "0" ? searchCriteriaFromRecommendedPartnerPg.CountryId : countryDropDown.SelectedValue;
                category = searchCriteriaFromRecommendedPartnerPg.CategoryId;
            }
            else
            {
                brand = "0";
                region = "0";
                country = "0";
                category = "0";
            }


            return _partnerManager.GetPartnerAppCatAndProducts(partnerId, brand, "0", region, country, category);
        }
    }
}
