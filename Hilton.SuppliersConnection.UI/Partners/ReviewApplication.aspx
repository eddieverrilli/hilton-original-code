﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    ValidateRequest="false" CodeBehind="ReviewApplication.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ReviewApplication" %>

<asp:Content ID="content2" ContentPlaceHolderID="cphHead" runat="server">
    <script src="../Scripts/jquery-tinymce.js" type="text/javascript"></script>
    <script src="../Scripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
    <script type="text/javascript">

        // Initializes all textareas with the tinymce class
        $(document).ready(function () {

            //  var versionValue = $('select#cphDetails_termsAndConditionDropDown option:selected').val();
            //alert(versionValue)
            //  if (versionValue == "0") {

            tinyMCE.init(
        {
            script_url: '../Scripts/tiny_mce/tiny_mce.js',
            mode: "exact",
            readonly: "1",
            elements: "ctl00$cphContent$termAndConditionTextBox",
            plugins: "lists,spellchecker,inlinepopups,preview,table,contextmenu,paste,directionality,fullscreen,",

            // Theme options
            theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,sub,sup,|,hr,|,removeformat,|,charmap,",
            theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,|,code,|,insertdate,inserttime,preview,visualaid,|,fullscreen,",
            theme_advanced_buttons3: "tablecontrols,",
            theme_advanced_buttons4: "ltr,rtl,|,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_disable: "code",
            theme_advanced_statusbar_location: "bottom",
            theme_advanced_resizing: false,
            visualblocks_default_state: true,

            // Schema is HTML5 instead of default HTML4
            schema: "html5",

            // End container block element when pressing enter inside an empty block
            end_container_on_empty_block: true,

            // Skin options
            skin: "o2k7",
            skin_variant: "silver",
            oninit: CustomOnInit,
            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "js/template_list.js",
            external_link_list_url: "js/link_list.js",
            external_image_list_url: "js/image_list.js",
            media_external_list_url: "js/media_list.js",

            // HTML5 formats
            style_formats: [
                { title: 'h1', block: 'h1' },
                { title: 'h2', block: 'h2' },
                { title: 'h3', block: 'h3' },
                { title: 'h4', block: 'h4' },
                { title: 'h5', block: 'h5' },
                { title: 'h6', block: 'h6' },
                { title: 'p', block: 'p' },
                { title: 'div', block: 'div' },
                { title: 'pre', block: 'pre' },
                { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
                { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
                { title: 'blockquote', block: 'blockquote', wrapper: true },
                { title: 'hgroup', block: 'hgroup', wrapper: true },
                { title: 'aside', block: 'aside', wrapper: true },
                { title: 'figure', block: 'figure', wrapper: true }
        ]

        });
            //tinyMCE.activeEditor.getBody().setAttribute('contenteditable', false);

            function CustomOnInit() {
                var versionValue = $('select#cphDetails_termsAndConditionDropDown option:selected').val();
                //  tinyMCE.activeEditor.settings.readonly = true;

                //      alert(versionValue)
                if (versionValue == "0") {
                    tinyMCE.activeEditor.getBody().setAttribute('contenteditable', true);
                    //   tinyMCE.get(tinyMCE.activeEditor.editorId).settings.readonly = true;
                    // tinyMCE.activeEditor.getBody().setAttribute('readonly', '1');
                    //   tinyMCE.activeEditor.setAttribute('readonly', "1");
                    //tinyMCE.get(tinyMCE.activeEditor.editorId).attr('readonly', true);
                }
                else {
                    tinyMCE.activeEditor.getBody().setAttribute('contenteditable', false);
                    tinyMCE.activeEditor.getBody().style.backgroundColor = "lightgray";
                    //    tinyMCE.get(tinyMCE.activeEditor.editorId).settings.readonly = true;
                    //tinyMCE.activeEditor.getBody().setAttribute('readonly', '1');
                    //   tinyMCE.activeEditor.setAttribute('readonly', "1");
                    // tinyMCE.get(tinyMCE.activeEditor.editorId).attr('readonly', true);
                    // tinyMCE.activeEditor.getBody().setAttribute('contenteditable', false);
                }
            }
        });

        function ReInitializeTinyMCE() {

            tinyMCE.idCounter = 0;
            tinyMCE.execCommand('mceRemoveControl', true, 'cphContent_termAndConditionTextBox');

            tinyMCE.init(
            {
                script_url: '../Scripts/tiny_mce/tiny_mce.js',
                mode: "exact",
                readonly: "1",
                elements: "ctl00$cphContent$termAndConditionTextBox",
                theme: "advanced",
                plugins: "lists,spellchecker,inlinepopups,preview,table,contextmenu,paste,directionality,fullscreen,",

                // Theme options
                theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,sub,sup,|,hr,removeformat,|,charmap,",
                theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,|,code,|,insertdate,inserttime,preview,visualaid,|,fullscreen,",
                theme_advanced_buttons3: "tablecontrols,",
                theme_advanced_buttons4: "ltr,rtl,|,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_disable: "code",
                theme_advanced_resizing: false,
                visualblocks_default_state: true,

                // Schema is HTML5 instead of default HTML4
                schema: "html5",

                // End container block element when pressing enter inside an empty block
                end_container_on_empty_block: true,

                // Skin options
                skin: "o2k7",
                skin_variant: "silver",

                // Drop lists for link/image/media/template dialogs
                template_external_list_url: "js/template_list.js",
                external_link_list_url: "js/link_list.js",
                external_image_list_url: "js/image_list.js",
                media_external_list_url: "js/media_list.js",

                // HTML5 formats
                style_formats: [
                { title: 'h1', block: 'h1' },
                { title: 'h2', block: 'h2' },
                { title: 'h3', block: 'h3' },
                { title: 'h4', block: 'h4' },
                { title: 'h5', block: 'h5' },
                { title: 'h6', block: 'h6' },
                { title: 'p', block: 'p' },
                { title: 'div', block: 'div' },
                { title: 'pre', block: 'pre' },
                { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
                { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
                { title: 'blockquote', block: 'blockquote', wrapper: true },
                { title: 'hgroup', block: 'hgroup', wrapper: true },
                { title: 'aside', block: 'aside', wrapper: true },
                { title: 'figure', block: 'figure', wrapper: true }
        ]

            });

            var versionValue = $('select#cphDetails_termsAndConditionDropDown option:selected').val();

            if (versionValue == "0") {
                tinyMCE.get(tinyMCE.activeEditor.editorId).getBody().setAttribute('contenteditable', 'true');
            }
            else {
                tinyMCE.get(tinyMCE.activeEditor.editorId).getBody().setAttribute('contenteditable', 'false');
                tinyMCE.activeEditor.getBody().style.backgroundColor = "lightgray";
            }

        }

        function UpdateTextArea() {
            tinyMCE.triggerSave(false, true);
        }

    </script>
</asp:Content>
<asp:Content ID="applicationContent" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="recommendedPartnersHyperLink" runat="server" CBID="1077" Text="Recommended Partners"
            NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:HyperLink ID="becomePartnerBreadcrumHyperLink" runat="server" CBID="37" Text="Become a Partner"
            NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="reviewApplicationHeaderLabel" CBID="1078" Text="Review Application"
            runat="server" />
    </div>
    <div class="main-content">
    </div>
    <div id="resultMessageDiv" runat="server">
    </div>
    <br />
    <div class="main">
        <div class="box1">
            <div class="steps">
                <asp:HyperLink ID="becomeRecomendedPartnerStepHyperlink" runat="server" class="done"></asp:HyperLink>
                <asp:HyperLink ID="applicationDeatilStepHyperLink" runat="server" class="done"></asp:HyperLink>
                <asp:HyperLink ID="reviewApplicationStepHyperLink" runat="server" class="active"></asp:HyperLink>
                <asp:HyperLink ID="thankYouHyperStepLink" runat="server" class=""></asp:HyperLink>
            </div>
            <h2>
                <asp:Label ID="reviewApplicationLabel" runat="server" CBID="86"></asp:Label></h2>
            <p>
                <asp:Label ID="reviewApplicationIntroLabel" runat="server" Text="" CBID="40"></asp:Label>
            </p>
        </div>
        <div class="review-application">
            <div class="accordion">
                <h3>
                    <asp:Label ID="selectedCategoriesLabel" runat="server" CBID="659"></asp:Label></h3>
                <div class="panel3">
                    <asp:GridView ID="selectedCategoriesGridView" GridLines="None" runat="server" AutoGenerateColumns="false">
                        <RowStyle BorderStyle="None" />
                        <Columns>
                            <asp:TemplateField ItemStyle-CssClass="col1" HeaderStyle-CssClass="col1" HeaderText="CATEGORY">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "CategoryDescription")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col2" HeaderStyle-CssClass="col2" HeaderText="REGIONS">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "RegionName") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField ItemStyle-CssClass="col3" HeaderStyle-CssClass="col3" HeaderText="COUNTRY">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "CountryName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col4" HeaderStyle-CssClass="col4" HeaderText="BRAND">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "BrandName") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                    </asp:GridView>
                    <div class="edit-btn">
                        <div class="input">
                            <div class="go">
                                <asp:Button ID="editCategories" CommandName="Edit" CBID="780" OnClick="EditCategoryInfo"
                                    runat="server" Text="EDIT" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <h3>
                    <asp:Label ID="partnerInformationLabel" runat="server" CBID="491"></asp:Label></h3>
                <div class="panel1">
                    <h4>
                        <asp:Label ID="accountTypeLabel" runat="server" CBID="164"></asp:Label></h4>
                    <table>
                        <tr id="goldBlockTableRow" style="display: none;" runat="server">
                            <td class="col2 wid">
                                <div>
                                    <span class="gold-level"></span>
                                    <h6>
                                        <asp:Label ID="goldLevelPartnerLabel" runat="server" CBID="977" Text="Gold-Level Partner"></asp:Label></h6>
                                    <p>
                                        <asp:Label ID="goldPartnerDescription" runat="server" CBID="976" Text="Silver-Level benefits, plus access to Hilton’s Construction Report and Lead Generation."></asp:Label></p>
                                </div>
                            </td>
                            <td class="col3">
                                <h6>
                                    <asp:Label ID="goldPartnershipAmountLabel" runat="server"></asp:Label>
                                </h6>
                                <p>
                                    <asp:Label ID="perYearGoldLabel" runat="server" CBID="509"></asp:Label></p>
                            </td>
                        </tr>
                        <tr id="silverBlockTableRow" style="display: none;" runat="server">
                            <td class="col2 wid">
                                <div>
                                    <span class="normal-level"></span>
                                    <h6>
                                        <asp:Label ID="regularPartnerLabel" runat="server" CBID="978" Text="Partner"></asp:Label></h6>
                                    <p>
                                        <asp:Label ID="rgularPartnerDescription" runat="server" CBID="979" Text="Listing on Suppliers’ Connection"></asp:Label></p>
                                </div>
                            </td>
                            <td class="col3 ">
                                <h6>
                                    <asp:Label ID="regularPartnershipAmountLabel" runat="server"></asp:Label>
                                </h6>
                                <p>
                                    <asp:Label ID="perYearRegularLabel" runat="server" CBID="509"></asp:Label></p>
                            </td>
                        </tr>
                    </table>
                    <div class="go">
                        <asp:Button ID="editPartnerTypeButton" runat="server" CBID="780" OnClick="EditPartnerTypeButton_Click"
                            Text="EDIT" /></div>
                    <div class="clear">
                        &nbsp;</div>
                    <h4>
                        <asp:Label ID="companyInformation" runat="server" CBID="137" Text="COMPANY INFORMATION"></asp:Label></h4>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="companyNameStaticLabel" runat="server" CBId="134" Text="Company Name"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="companyNameLabel" runat="server"></asp:Label>
                            <asp:HiddenField ID="partnerIdHiddenField" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="companyLogoLabel" runat="server" CBID="980" Text="Company Logo"></asp:Label></div>
                        <div class="input">
                            <asp:Image ID="companyLogoImage" CssClass="pic imagestyle" ImageUrl="~/Images/company-logo.gif"
                                runat="server" /></div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="companyAddressStaticLabel" runat="server" CBId="303"></asp:Label></div>
                        <div class="input">
                            <p>
                                <asp:Label ID="companyAddressLabel" runat="server"></asp:Label></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="webSiteLinkStaticLabel" runat="server" Text="WEB SITE" CBID="0"></asp:Label></div>
                        <div class="input">
                            <p>
                                <asp:Label ID="webSiteLinkLabel" runat="server"></asp:Label></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="countryStaticLabel" runat="server" CBId="307"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="countryLabel" runat="server"></asp:Label></div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="cityStaticLabel" runat="server" CBId="306"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="cityLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="stateStaticLabel" runat="server" CBId="311"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="stateLabel" runat="server"></asp:Label></div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="zipCodeStaticLabel" runat="server" CBId="312"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="zipCodeLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="companyDescriptionStaticLabel" runat="server" Text="Company description"
                                CBID="981"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="companyDescriptionLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row editbtn">
                        <div class="label">
                            &nbsp;</div>
                        <div class="input">
                            <div class="go">
                                <asp:Button ID="editCompanyInformationButton" runat="server" CBID="780" OnClick="EditCompanyInformationButton_Click"
                                    Text="EDIT" />
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="panel1">
                    <h4>
                        <asp:Label ID="contactInformationLabel" runat="server" Text="Contact Information"
                            CBID="324"></asp:Label></h4>
                    <h6 class="pad20">
                        <asp:Label ID="primaryContactLabel" runat="server" CBID="519"></asp:Label></h6>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="firstNamePrimaryContactStaticLabel" CBID="99" runat="server"></asp:Label></div>
                        <div class="input">
                            <asp:HiddenField ID="primaryContactIdHiddenField" runat="server" />
                            <asp:Label ID="firstNamePrimaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="lastNamePrimaryContactStaticLabel" runat="server" CBID="111"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="lastNamePrimaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="titleStaticPrimaryContactLabel" runat="server" CBID="710"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="titlePrimaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="phonePrimaryContactStaticLabel" runat="server" CBID="124"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="phonePrimaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="faxPrimaryContactStaticLabel" runat="server" CBID="141"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="faxPrimaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="emailAddressPrimaryContactStaticLabel" runat="server" CBId="384"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="emailAddressPrimaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row editbtn">
                        <div class="label">
                            &nbsp;</div>
                        <div class="input">
                            <div class="go">
                                <asp:Button ID="editPrimaryContactButton" CBID="780" runat="server" OnClick="EditPrimaryContactButton_Click"
                                    Text="EDIT" />
                            </div>
                        </div>
                    </div>
                    <div class="row border">
                        <img width="638" height="1" src="../Images/dotted-bg1.gif" alt="" />
                    </div>
                    <h6 class="pad20">
                        <asp:Label ID="secondaryContactLabel" runat="server" Text="Secondary Contact" CBID="982"></asp:Label></h6>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="firstNameSecondaryContactStaticLabel" runat="server" CBID="99"></asp:Label></div>
                        <div class="input">
                            <asp:HiddenField ID="secondaryContactIdHiddenField" runat="server" />
                            <asp:Label ID="firstNameSecondaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="lastNameSecondaryContactStaticLabel" runat="server" CBID="111"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="lastNameSecondaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="titleSecondaryContactStaticLabel" runat="server" CBID="710"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="titleSecondaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="phoneSecondaryContactStaticLabel" runat="server" CBID="124"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="phoneSecondaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="faxSecondaryContactStaticLabel" runat="server" CBID="141"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="faxSecondaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="emailSecondaryContactStaticLabel" runat="server" CBID="384"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="emailSecondaryContactLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row editbtn">
                        <div class="label">
                            &nbsp;</div>
                        <div class="input">
                            <div class="go">
                                <asp:Button ID="editSecondaryContactButton" runat="server" CBID="780" OnClick="EditPrimaryContactButton_Click"
                                    Text="EDIT" />
                            </div>
                        </div>
                    </div>
                    <div class="row border">
                        <img width="638" height="1" src="../Images/dotted-bg1.gif" alt="" /></div>
                    <asp:PlaceHolder ID="placeHolder1" runat="server"></asp:PlaceHolder>
                    <asp:Repeater ID="regionContactRepeater" ViewStateMode="Enabled" OnItemCommand="RegionContactRepeater_ItemCommand"
                        runat="server">
                        <ItemTemplate>
                            <h6 class="pad20">
                                <asp:Label ID="regionNameLabel" Text='<%# DataBinder.Eval(Container.DataItem, "CountryName")%>'
                                    runat="server"></asp:Label>
                                <asp:Label ID="regionIdLabel" Text='<%# DataBinder.Eval(Container.DataItem, "CountryId")%>'
                                    Visible="false" runat="server"></asp:Label>
                            </h6>
                            <div class="row">
                                <div class="label">
                                    <asp:HiddenField ID="regionalContactIdHiddenField" runat="server" />
                                    <asp:Label ID="firstNameRegionalContactLabel" Text="FIRST NAME" CBID="99" runat="server"></asp:Label></div>
                                <div class="input ">
                                    <asp:Label ID="firstNameInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName")%>'></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="lastNameRegionalContactLabel" Text="LAST NAME" CBID="111" runat="server"></asp:Label></div>
                                <div class="input ">
                                    <asp:Label ID="lastNameInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastName")%>'></asp:Label></div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="titleRegionalContactLabel" Text="TITLE" CBID="710" runat="server"></asp:Label></div>
                                <div class="input">
                                    <asp:Label ID="titleInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>'></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="phoneRegionalContactLabel" Text="PHONE" CBID="124" runat="server"></asp:Label></div>
                                <div class="input textbox-four">
                                    <asp:Label ID="phoneInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Phone")%>'></asp:Label></div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="faxRegionalContactLabel" Text="FAX" CBID="141" runat="server"></asp:Label></div>
                                <div class="input textbox-four">
                                    <asp:Label runat="server" ID="faxInputRegionalContactLabel" Text='<%# DataBinder.Eval(Container.DataItem, "Fax")%>'></asp:Label></div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="emailRegionalContactLabel" Text="EMAIL ADDRESS" CBID="384" runat="server"></asp:Label></div>
                                <div class="input textbox-one">
                                    <asp:Label ID="emailInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Email")%>'></asp:Label></div>
                            </div>
                            <div class="row editbtn">
                                <div class="label">
                                    &nbsp;</div>
                                <div class="input">
                                    <div class="go">
                                        <asp:Button ID="editRegionContactsButton" CommandName="Edit" CBID="780" runat="server"
                                            Text="EDIT" />
                                    </div>
                                </div>
                            </div>
                            <div class="row border">
                                <img width="638" height="1" src="../Images/dotted-bg1.gif" alt="" /></div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div runat="server" id="termsAndConditionDiv" viewstatemode="Enabled">
                        <h4>
                            <asp:Label ID="Label1" runat="server" CBID="0" Text="Terms And Conditions"></asp:Label></h4>
                        <asp:UpdatePanel ID="termsAndConditionUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="editor1">
                                    <asp:TextBox ID="termAndConditionTextBox" runat="server" Rows="10" CssClass="widthTC graybg"
                                        TextMode="MultiLine" />
                                    <asp:HiddenField ID="termsAndConditionVersionHiddenField" runat="server" ViewStateMode="Enabled" />
                                    <br />
                                    <asp:CheckBox ID="termsAgreementCheckBox" runat="server" ViewStateMode="Enabled"
                                        CssClass="chkbox" ValidationGroup="Agree" CBID="0" Text=" I Agree with Terms and Condition" />
                                    <asp:CustomValidator ID="termCustomerValidator" CssClass="errorText" Display="Dynamic"
                                        ViewStateMode="Enabled" runat="server" ValidationGroup="ReviewApplication" OnServerValidate="TermsAndCondition_ServerValidate"></asp:CustomValidator>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="submitApplicationButton" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <br />
                            <div id="termsAndConditionsMessageDiv" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="box1">
                <div class="go">
                    <asp:Button ID="submitApplicationButton" runat="server" Text="SUBMIT APPLICATION"
                        ValidationGroup="ReviewApplication" CBID="704" OnClick="SubmitApplicationButton_Click" />
                </div>
                <div class="clear">
                    &nbsp;</div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="Recommended Partners" runat="server"></asp:Label></h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="browsePartnersHyperLink" runat="server" CBID="32" Text="Browse Partners"
                            NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="becomePartnerHyperLink" class="active" CBID="84" Text="Become a Partner"
                            runat="server"></asp:HyperLink>
                        <ul>
                            <li>
                                <asp:HyperLink ID="chooseCategoriesHyperLink" runat="server" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"
                                    CBID="983" Text="Step 1: Choose Categories"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="completeApplicationHyperLink" runat="server" NavigateUrl="~/Partners/ApplicationDetails.aspx"
                                    CBID="984" Text="Step 2: Complete Application"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="reviewApplicationHyperLink" runat="server" class="active" NavigateUrl="~/Partners/ReviewApplication.aspx"
                                    CBID="985" Text="Step 3: Review Application">
                                </asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="thankYouHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/ThankYouNextSteps.aspx"
                                    CBID="986" Text="Step 4: Thank You"></asp:HyperLink>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>