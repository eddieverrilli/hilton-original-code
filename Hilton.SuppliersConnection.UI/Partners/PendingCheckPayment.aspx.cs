﻿using System;
using System.Globalization;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class PendingCheckPayment : PageBase
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                this.Title = ResourceUtility.GetLocalizedString(89, culture, "Pending Check Payment");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //User loggedInUser = new User();
                //loggedInUser = GetSession<User>(SessionConstants.User);

                //userNameLabel.Text = "Welcome, " + loggedInUser.FirstName + " " + loggedInUser.LastName;

                if (Context.Items[UIConstants.CheckPaymentAmount] != null)
                {
                    amountLabel.Text = Convert.ToDecimal(Context.Items[UIConstants.CheckPaymentAmount], CultureInfo.InvariantCulture).ToString("C", new CultureInfo("en-US"));
                }
            }
        }
    }
}