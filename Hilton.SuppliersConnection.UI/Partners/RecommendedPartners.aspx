﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    ViewStateMode="Inherit" CodeBehind="RecommendedPartners.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.RecommendedPartners" %>
        
<%@ Register Src="~/Controls/FeedbackPopup.ascx" TagName="FeedbackPopup" TagPrefix="uc1" %>
<asp:Content ID="content2" ContentPlaceHolderID="cphContent" runat="server">


    <script src="../Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="../Scripts/MultiSearch.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.watermarkinput.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/thickbox.js"></script>    
    <script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script type="text/javascript" src="../Scripts/multiselect.region.and.countries.js"></script>
       <%-- <script src="../Scripts/dropdownScript.js" type="text/javascript"></script>--%>

        <style type="text/css">
        .icon-cancel
        {
            width: 11px;
            height: 11px;
            float: left;
            
            background-image: url(image/close.png);
            cursor: pointer;
        }        
        .ui-autocomplete-category
        {
            font-weight: bold;
            padding: .2em .4em;
            margin: .8em 0 .2em;
            line-height: 1.5;
        }
    </style>

    <script language="javascript" type="text/javascript">
        (function (e) {
            "use strict";

            function i(t, n) {
                this.opts = e.extend({
                    handleKeys: !0,
                    scrollEventKeys: [32, 33, 34, 35, 36, 37, 38, 39, 40]
                }, n);
                this.$container = t;
                this.$document = e(document);
                this.disable()
            }
            var t, n = function (e) {
                for (var t = 0; t < this.opts.scrollEventKeys.length; t++)
                    if (e.keyCode === this.opts.scrollEventKeys[t]) {
                        e.preventDefault();
                        return
                    }
            },
        r = function (e) {
            e.preventDefault()
        };
            i.prototype = {
                disable: function () {
                    var e = this;
                    e.$container.on("mousewheel.UserScrollDisabler DOMMouseScroll.UserScrollDisabler touchmove.UserScrollDisabler", r);
                    e.opts.handleKeys && e.$document.on("keydown.UserScrollDisabler", function (t) {
                        n.call(e, t)
                    })
                },
                undo: function () {
                    var e = this;
                    e.$container.off(".UserScrollDisabler");
                    e.opts.handleKeys && e.$document.off(".UserScrollDisabler")
                }
            };
            e.fn.disablescroll = function (e) {
                !t && (typeof e == "object" || !e) ? t = new i(this, e) : t && t[e] ? t[e].call(t) : t && t.disable.call(t)
            }
        })(jQuery);

       $(function () {
                $('.special').hover(function () {
                    var thisText = $(this).val();
                    if (thisText !== '') {
                        $('.tooltipText').html(thisText);
                        $('.tooltip').css('top', $('.special').offset().top - 40)
                        $('.tooltip').css('left', $('.special').offset().left)
                        $('.tooltip').show();
                    }
                },
	        function () {
	            $('.tooltip').hide();
	            $('.tooltipText').html('');
	        });

            $('.dropdown').hover(function () {
                var thisOffsetTop = $(this).offset().top - $(window).scrollTop(),
			thisAHeight = $(this).find('ul:first>li').length * 30,
			windowHeight = $(window).height();

                if (windowHeight < thisAHeight + thisOffsetTop + 30) {
                    if (thisOffsetTop - thisAHeight >= 0) {
                        if ($(this).attr('data-level') == 2) {
                            $(this).find('a').next().css('bottom', 0);
                            $(this).find('a').next().css('top', 'auto');
                        }
                        else {
                            $(this).find('a').next().css('bottom', windowHeight - $(this).offset().top - 30)
                            $(this).find('a').next().css('top', 'auto')
                        }
                    }
                    else {
                        if ($(this).attr('data-level') == 2) {
                            $(this).find('a').next().css('top', -thisOffsetTop);
                        } else {
                            $(this).find('a').next().css('top', '0');
                        }
                    }
                }
                else {
                    if ($(this).attr('data-level') == 2) {
                        $(this).find('a').next().css('top', 0);
                    } else {
                        $(this).find('a').next().css('top', $(this).offset().top);
                    }
                }


                if ($(this).attr('data-level') == 1) {
                    $(this).find('a').next().css('left', $(this).offset().left + $(this).width());
                } else
                    $(this).find('a').next().css('left', $(this).width());
      
            })
            $('.nav ul, .dropdown, .nav ul li, .nav ul ul li').hover(function () {
                $('.nav.openMenu').disablescroll();
            });
            $('.nav ul, .dropdown').mouseout(function () {
                $('.nav.openMenu').disablescroll("undo");
            });
        });


        $(function (e) {
            $('body').click(function (e) {
                if ($(e.target).hasClass('special')) {
                    if ($(e.target).next('.nav').hasClass('closedMenu')) {
                        $(e.target).next('.nav').removeClass('closedMenu').addClass('openMenu').show();
                    }
                    else
                        if ($(e.target).next('.nav').hasClass('openMenu')) {
                            $(e.target).next('.nav').removeClass('openMenu').addClass('closedMenu').hide();
                        }
                }
                else {
                    if ($('.nav').hasClass('openMenu')) {
                        //if($(this).attr('child-node')){
                        $('.nav').removeClass('openMenu').addClass('closedMenu').hide();
                        //}
                    }
                }
            });

            $('.nav li').click(function (e) {
                if (!$(this).attr('child-node')) {
                    var dataLevel = $(this).attr('data-level');
                    switch (dataLevel) {
                        case "1":
                            var selectedText = $(this).find('a:first').text();
                            $(this).parents('.nav').prev('.special').val(selectedText);
                            $('#cphContent_leafCategoryHdn').val($(this).context.value);
                            $('#cphContent_categoryTextBoxHdn').val(selectedText);
                            $("#cphContent_SpecialCategoryTxtBox").trigger('change');
                            break;
                        case "2":
                            var selectedText = $(this).parent('ul').parent('li').find('a:first').text() + ' :: ' + $(this).find('a:first').text();
                            $(this).parents('.nav').prev('.special').val(selectedText);
                            $('#cphContent_leafCategoryHdn').val($(this).context.value);
                            $('#cphContent_categoryTextBoxHdn').val(selectedText);
                            $("#cphContent_SpecialCategoryTxtBox").trigger('change');
                            break;
                        case "3":
                            var selectedText = $(this).parent('ul').parent('li').parent().parent().find('a:first').text() + ' :: ' + $(this).parent('ul').parent('li').find('a:first').text() + ' :: ' + $(this).text();
                            $(this).parents('.nav').prev('.special').val(selectedText);
                            $('#cphContent_leafCategoryHdn').val($(this).context.value);
                            $('#cphContent_categoryTextBoxHdn').val(selectedText);
                            $("#cphContent_SpecialCategoryTxtBox").trigger('change');
                            break;
                    }
                    //  CallServerSideMethod($(this).context.value);
                    if ($('.nav').hasClass('openMenu')) {
                        $('.nav').removeClass('openMenu').addClass('closedMenu').hide();
                    }
                    e.stopPropagation();
                } e.stopPropagation();
                $("#SpecialCategoryTxtBox").val('Whatever').trigger('change');
                return true;
            }); 
        });


        function resetMenu() {
            $(function (e) {
                $('.nav li').click(function (e) {
                    if (!$(this).attr('child-node')) {
                        var dataLevel = $(this).attr('data-level');
                        switch (dataLevel) {
                            case "1":
                                var selectedText = $(this).find('a:first').text();
                                $(this).parents('.nav').prev('.special').val(selectedText);
                                $('#cphContent_leafCategoryHdn').val($(this).context.value);
                                $('#cphContent_categoryTextBoxHdn').val(selectedText);
                                $("#cphContent_SpecialCategoryTxtBox").trigger('change');
                                break;
                            case "2":
                                var selectedText = $(this).parent('ul').parent('li').find('a:first').text() + ' :: ' + $(this).find('a:first').text();
                                $(this).parents('.nav').prev('.special').val(selectedText);
                                $('#cphContent_leafCategoryHdn').val($(this).context.value);
                                $('#cphContent_categoryTextBoxHdn').val(selectedText);
                                $("#cphContent_SpecialCategoryTxtBox").trigger('change');
                                break;
                            case "3":
                                var selectedText = $(this).parent('ul').parent('li').parent().parent().find('a:first').text() + ' :: ' + $(this).parent('ul').parent('li').find('a:first').text() + ' :: ' + $(this).text();
                                $(this).parents('.nav').prev('.special').val(selectedText);
                                $('#cphContent_leafCategoryHdn').val($(this).context.value);
                                $('#cphContent_categoryTextBoxHdn').val(selectedText);
                                $("#cphContent_SpecialCategoryTxtBox").trigger('change');
                                break;
                        }
                       // CallServerSideMethod($(this).context.value);
                        if ($('.nav').hasClass('openMenu')) {
                            $('.nav').removeClass('openMenu').addClass('closedMenu').hide();
                        }
                        e.stopPropagation();
                    } e.stopPropagation();
                    
                    return true;
                });
            });

            $(function () {
                $('.dropdown').hover(function () {
                    var thisOffsetTop = $(this).offset().top - $(window).scrollTop(),
			thisAHeight = $(this).find('ul:first>li').length * 30,
			windowHeight = $(window).height();

                    if (windowHeight < thisAHeight + thisOffsetTop + 30) {
                        if (thisOffsetTop - thisAHeight >= 0) {
                            if ($(this).attr('data-level') == 2) {
                                $(this).find('a').next().css('bottom', 0);
                                $(this).find('a').next().css('top', 'auto');
                            }
                            else {
                                $(this).find('a').next().css('bottom', windowHeight - $(this).offset().top - 30)
                                $(this).find('a').next().css('top', 'auto')
                            }
                        }
                        else {
                            if ($(this).attr('data-level') == 2) {
                                $(this).find('a').next().css('top', -thisOffsetTop);
                            } else {
                                $(this).find('a').next().css('top', '0');
                            }
                        }
                    }
                    else {
                        if ($(this).attr('data-level') == 2) {
                            $(this).find('a').next().css('top', 0);
                        } else {
                            $(this).find('a').next().css('top', $(this).offset().top);
                        }
                    }


                    if ($(this).attr('data-level') == 1) {
                        $(this).find('a').next().css('left', $(this).offset().left + $(this).width());
                    } else
                        $(this).find('a').next().css('left', $(this).width());

                })
                $('.nav ul, .dropdown, .nav ul li, .nav ul ul li').hover(function () {
                    $('.nav.openMenu').disablescroll();
                });
                $('.nav ul, .dropdown').mouseout(function () {
                    $('.nav.openMenu').disablescroll("undo");
                });
            });

            $('.special').hover(function () {
                var thisText = $(this).val();
                if (thisText !== '') {
                    $('.tooltipText').html(thisText);
                    $('.tooltip').css('top', $('.special').offset().top - 40)
                    $('.tooltip').css('left', $('.special').offset().left)
                    $('.tooltip').show();
                }
            },
	        function () {
	            $('.tooltip').hide();
	            $('.tooltipText').html('');
	        });
        } 
      
        function setCategoryTextBoxVal() {
            $('.special').val($('#cphContent_categoryTextBoxHdn').val());
        }


        function more_lessText() {
            $(function () {
                $('.excerpt').wrapInner("<span></span>");
                $('.excerpt').each(function () {
                    $(this).html(formatWords($(this).html(), 3));

                    $(this).children().children('.more_text').hide();

                }).click(function () {
                    var more_text = $(this).children().children('.more_text');
                    var more_link = $(this).children('a.more_link');

                    if (more_text.hasClass('hide')) {
                        more_text.show();
                        more_link.html('Hide');
                        more_text.removeClass('hide');
                    } else {
                        more_text.hide();
                        more_link.html('More');
                        more_text.addClass('hide');
                    }
                    return false;
                });
            });

            function formatWords(sentence, show) {
                var words = sentence.split(' ');
                var new_sentence = '';
                for (i = 0; i < words.length; i++) {
                    if (i <= show) {
                        new_sentence += words[i] + ' ';
                    } else {
                        if (i == (show + 1)) new_sentence += '<span class="more_text hide">';
                        new_sentence += words[i] + ' ';
                        if (words[i + 1] == null) new_sentence += '</span></br><a href="#" class="more_link">More...</a>';
                    }
                }
                return new_sentence;
            }
        }

        function more_lessItems() {
            $('td.col3, td.col4, td.col5, td.col2').each(function () {
                var max = 1
                if ($(this).find("li").length > max) {
                    var count = $(this).find("li").length;
                    var num = count - 2;
                    if (num != 0) {
                        $(this).find('li:gt(1)').hide();
                        var list = $(this).find('li:gt(1)');
                        $(this).append('<span class="more">+' + num + ' More</span>');
                        $(this).append('<span href="" class="less">Less</span>');
                        $('.more').click(function () {
                            $(this).next().show();
                            $(this).parent().find('li:gt(1)').show()
                            $(this).hide();
                        });
                        $('.less').click(function () {
                            $(this).prev().show();
                            $(this).parent().find('li:gt(1)').hide()
                            $(this).hide();
                        });
                    }
                }
            });
        }

        function forceCall() {
            tb_init('a.thickbox');
            imgLoader = new Image();
            imgLoader.src = tb_pathToImage;
        }

        function forceHover() {
            $('.comment').hover(function () {
                $(this).children('.tooltip').show();
            },
                function () {
                    $(this).children('.tooltip').hide();
                });
        }
        $(document).ready(function () {
            isAllCountryChecked = 1;
            getItems();
            SetRegionAndCountryDropDown();
            $('#cphContent_categoryTextBoxHdn').val("All Categories");
        });

        function CleanSearchItems() {
            $('#log').children().remove();
        }

        function UpdateSessionTimeout() {
            SessionTimeoutCheck();
        }


        window.CallParent = function () {
            SessionTimeoutCheck();
        }  
    </script>
    
    
    <div class="breadcrumbs">
        <asp:HyperLink ID="recommendedPartnerBreadCrumHyperLink" runat="server" CBID="3"
            Text="Recommended Partners"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="myProductsLabel" CBID="32" Text="Browse Partners" runat="server"></asp:Label>
        <span></span>
    </div>
    <div class="main-content paddingTop">
        <asp:UpdatePanel ID="dropDownUpdatePanel" runat="server" UpdateMode="Conditional">
        <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="selectedBrands" EventName="TextChanged" />
                     <asp:AsyncPostBackTrigger ControlID="selectedRegions" EventName="TextChanged" />
                      <asp:AsyncPostBackTrigger ControlID="SpecialCategoryTxtBox" EventName="TextChanged" />
                </Triggers>
            <ContentTemplate>
            <div class="wrap">
                <div class="search-result part1">
                       <h2>
            <asp:Label ID="browseRecommendedPartners" runat="server" CBID="32" Text="Browse Partner"></asp:Label></h2>
        <p>
            <asp:Label ID="desciptionLabel" runat="server" CBID="33" Text=""></asp:Label></p>
            <div id="cascadedDdl"  runat="server">
            </div>
                
                    <div class="category">
                      <asp:Panel ID="categoryMenu" runat="server">
                      <asp:TextBox runat ="server" ID="SpecialCategoryTxtBox" CssClass="special" 
                               AutoPostBack="true" OnTextChanged="SpecialCategoryTxtBox_TextChanged"  Text="All Categories"></asp:TextBox>
                      </asp:Panel>
                        <%--<asp:DropDownList ID="categoriesDropDown" runat="server" ViewStateMode="Enabled"
                            AutoPostBack="True" OnSelectedIndexChanged="CategoriesDropDown_SelectedIndexChanged">
                            <asp:ListItem>All Categories</asp:ListItem>
                        </asp:DropDownList>
                        <div style="width: 250px; margin-left: -0.5px;">
                            <asp:PlaceHolder ID="parentCategoryContainer" ViewStateMode="Enabled" runat="server">
                            </asp:PlaceHolder>--%>
                        </div>
                    <div class="brands">
                       <asp:DropDownList runat="server" ID="optgroup"  ViewStateMode="Enabled" ClientIDMode="Static"  multiple="multiple">         
                        </asp:DropDownList>
                        <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="isCheckAll" runat="server" ClientIDMode="Static" />
                        <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display:none;" AutoPostBack="true" OnTextChanged="SelectedBrands_TextChanged"></asp:TextBox>
                        
                    </div>
                    <div class="region">
                        <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" 
                             multiple="multiple" runat="server" ClientIDMode="Static">
                        </asp:DropDownList>
                        <asp:HiddenField ID="regionCollection" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="isAllRegionChecked" runat="server" ClientIDMode="Static" Value="1" />
                        <asp:TextBox runat="server" ID="selectedRegions" ClientIDMode="Static" Style="display:none;" AutoPostBack="true" OnTextChanged="SelectedRegions_TextChanged"></asp:TextBox>
                        <br />
                        <div class="no-margin"><asp:DropDownList  ViewStateMode="Enabled" ID="countryDropDown" multiple="multiple" ClientIDMode="Static"
                            runat="server">
                        </asp:DropDownList></div>
                         <asp:TextBox runat="server" ID="selectedCountries"  ClientIDMode="Static" Style="display:none;" ></asp:TextBox>
                         
                    </div>
                    <div class="go" style="float: right;margin-right:114px;margin-top:5px;">
                        <asp:LinkButton ID="goButton" runat="server" CBID="397" OnClick="GoButton_Click" OnClientClick="javascript:CleanSearchItems();"
                            Text="Go"></asp:LinkButton>
                    </div>
                  
                </div>              
                </div>
                <asp:HiddenField ID="categoryHdn" runat="server" Value="0" />
                <asp:HiddenField ID="leafCategoryHdn" runat="server" Value="0" />
                <asp:HiddenField ID="categoryTextBoxHdn" runat="server"  />
                <asp:HiddenField ID="lastSelectedCategoryDropDown" runat="server" Value="0" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="sidebar browsepartner-sidebar style1">
        <div class="sidebar-box nofloat cntnt" id="Div1">
            <h3>
                <asp:Label ID="Label1" Text="SMART SEARCH" runat="server"></asp:Label></h3>
            <div class="middle">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="ui-widget padding">                
                <%--<label for="search">
                    Search:
                </label>--%>
                <input id="search" class="search">
                <img src="../Images/btn_Question.png" title="Enter your text to search on Partner Category, Brand Segment, Brand, Property Type, Region, Country, Company Name, State and/or Contact First Name or Last Name. You may enter and select multiple search options.
Click the ‘Go’ button to execute your search.
" style="display: inline;padding-top: 0;position: relative;top: 5px;" />
                <asp:Button ID="btn" CssClass="goStyle" runat="server" Text="Go" OnClick="btn_Click" Style="height: 27px" onClientClick="if(!fn_getSelectedvales()) return false"    />
            </div>
           <%-- <div class="ui-widget" id="Selectedvalue">
            </div>--%>            
            <asp:HiddenField ID="hd" runat="server" />
            <asp:HiddenField ID="hdCountry" runat="server" ClientIDMode="Static" />
        </ContentTemplate>
        </asp:UpdatePanel>
        <div class="ui-widget clr" style="margin-top: 1em;padding:2px; font-family: Arial">
        <%--Result:--%>
        <div id="log" style="min-height: 150px; width: 215px;" runat="server" clientidmode="Static">            
        </div>
    </div>
</div>
        </div>
    </div>

   
   <%-- <div id="categoryMenu" class="main browsePartner-main one" runat="server">
  </div>--%>
        <asp:UpdatePanel ID="updPnlPartner" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="recommended-partners">
                    <div class="accordion">
                        <h3>
                            <asp:Label ID="recommendedPArtners" runat="server" CBID="586" Text="Recommended Partners"></asp:Label></h3>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="panel3">
                            <asp:GridView ID="recommendedPartnersGridView" ViewStateMode="Enabled" AutoGenerateColumns="False"
                                runat="server" AlternatingRowStyle-CssClass="odd" BorderStyle="None" GridLines="None"
                                Width="100%" OnRowDataBound="RecommendedPartners_RowDataBound" OnRowCommand="RecommendedPartners_RowCommand"
                                OnRowCreated="RecommendedPartners_RowCreated" OnSorting="RecommendedPartners_Sorting"
                                AllowSorting="True">
                                <AlternatingRowStyle CssClass="odd"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField ItemStyle-CssClass="col1" HeaderText="Partner Name" SortExpression="CompanyName">
                                        <ItemTemplate>
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td style="width: 99%">
                                                            <p>
                                                                <asp:Image ID="partnerImg" ViewStateMode="Disabled" CssClass="imagestyle" runat="server" />
                                                                <asp:ImageButton ID="partnerImgButton" ViewStateMode="Enabled" Visible="false" CssClass="imagestyle"
                                                                    runat="server" />
                                                            </p>
                                                            <p>
                                                                <asp:Label ID="vendorLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Partner.CompanyName") %>'></asp:Label>
                                                                <asp:LinkButton ID="vendorLinkButton" CssClass="wrap-text" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem,"Partner.CompanyName") %>'></asp:LinkButton>
                                                                &nbsp;</p>
                                                        </td>
                                                        <td>
                                                            <span class="cmt submit-feedback comment">
                                                                <asp:HyperLink ID="hprlnkComment" CssClass="thickbox" runat="server"><img  src="../Images/product/comment-icon.png" alt="" width="14" height="13" /></asp:HyperLink>
                                                                <span class="tooltip">
                                                                    <img src="../Images/product/submit-feedback.png" alt="" width="110" height="19" />
                                                                </span></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p>
                                                    <asp:Label ID="isSamLabel" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem,"Partner.IsSAM") %>'></asp:Label>
                                                    <asp:Image ID="samImg" runat="server" ImageUrl="~/Images/hilton-icon.gif" Height="17"
                                                        Width="17" />
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="col1"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Categories" ItemStyle-CssClass="col2" SortExpression="CategoryName">
                                        <ItemTemplate>
                                            <ul>
                                                <asp:Repeater ID="repeaterCategories" ViewStateMode="Disabled" runat="server">
                                                    <ItemTemplate>
                                                        <li>
                                                            <%# DataBinder.Eval(Container.DataItem, "CategoryName")%></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="col2"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Regions" ItemStyle-CssClass="col3" SortExpression="RegionDescription">
                                        <ItemTemplate>
                                            <ul>
                                                <asp:Repeater ID="repeaterRegions" ViewStateMode="Disabled" runat="server">
                                                    <ItemTemplate>
                                                        <li>
                                                            <%# DataBinder.Eval(Container.DataItem, "RegionDescription")%></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="col3"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Countries"  SortExpression="CountryName">
                                        <ItemTemplate>
                                            <ul>
                                                <asp:Repeater ID="repeaterCountries" ViewStateMode="Disabled" runat="server">
                                                    <ItemTemplate>
                                                        <li>
                                                            <%# DataBinder.Eval(Container.DataItem, "CountryName")%></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="col3"></ItemStyle>
                                        <ItemStyle ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Brands" ItemStyle-CssClass="col4" SortExpression="BrandDescription">
                                        <ItemTemplate>
                                            <itemstyle cssclass="col4"></itemstyle>
                                            <ul>
                                                <asp:Repeater ID="repeaterBrands" runat="server">
                                                    <ItemTemplate>
                                                        <li>
                                                            <%# DataBinder.Eval(Container.DataItem, "BrandDescription")%></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="col4"></ItemStyle>
                                    </asp:TemplateField>
                                  <%--  <asp:TemplateField HeaderText="Property Types" ItemStyle-CssClass="col5" SortExpression="PropertyTypeDescription">
                                        <ItemTemplate>
                                            <itemstyle cssclass="col5"></itemstyle>
                                            <ul>
                                                <asp:Repeater ID="repeaterTypes" ViewStateMode="Disabled" runat="server">
                                                    <ItemTemplate>
                                                        <li>
                                                            <%# DataBinder.Eval(Container.DataItem, "PropertyTypeDescription") %></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="col5"></ItemStyle>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Contact" ItemStyle-CssClass="col5" SortExpression="Contacts">
                                        <ItemTemplate>
                                            <p>
                                               <table style="border-top-style:none;border-width:0px;border-style:none;padding-right:1px;">
                                               <tr style="padding-left:0;">
                                                   <td style="padding-left:0;border-top-style:none;border-width:0px;border-style:none;">
                                                   <asp:Literal ID="contactLtl"  runat="server"></asp:Literal>
                                                   </td>
                                                   <td style="padding-left:0;border-top-style:none;border-width:0px;border-style:none;">
                                                     <asp:LinkButton ID="lnkViewProfileProducts" CBID="742" runat="server" CommandName="viewPartnerProfile"
                                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PartnerId") %>'></asp:LinkButton>
                                                   </td>
                                               </tr>
                                              </table>  
                                            <br/>
                                              <a class="wrap-text" ID="webSiteLink" target="_blank" runat="server"></a>
                                            </p>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="col5"></ItemStyle>
                                    </asp:TemplateField>
                                   </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="isLogOnUserHiddenField" runat="server" Value="1" />
                        </div>
                    </div>
                    <div id="divPaging" runat="server" class="paging">
                        <asp:LinkButton ID="prevLinkButton" runat="server" class="prev" ViewStateMode="Enabled"
                            OnClick="prevLinkButton_Click"></asp:LinkButton>
                        <asp:LinkButton ID="nextLinkButton" runat="server" class="next" ViewStateMode="Enabled"
                            OnClick="nextLinkButton_Click"></asp:LinkButton>
                        <div class="center">
                            <p>
                                PAGE</p>
                            <p>
                                <span>
                                    <asp:Label ID="startPage" runat="server" ViewStateMode="Enabled">1</asp:Label>
                                </span>of <span>
                                    <asp:Label ID="endPage" runat="server" ViewStateMode="Enabled"></asp:Label>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="userTypeHiddenField" runat="server" />
            <div class="sidebar browsepartner-sidebar">
        <div class="sidebar-box nofloat" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="Recommended Partners" runat="server"></asp:Label></h3>
            <div class="middle style2">
                <ul>
                    <li>
                        <asp:HyperLink ID="hplnkBrowsePartners" runat="server" CBID="32" Text="Browse Partners"
                            class="active"></asp:HyperLink>
                    </li>
                    <li id="becomePartnerListItem" runat="server">
                        <asp:HyperLink ID="hplnkBecomePartner" runat="server" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx">Become a Partner</asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
 <%--   </div>--%>
 <div class='tooltip'>
	<div class='tooltipText'></div>
	<div class='arrowInv'></div>
</div>
    <div class="clear">
        &nbsp;</div>
    <uc1:FeedbackPopup ID="FeedbackPopup1" runat="server" />

</asp:Content>