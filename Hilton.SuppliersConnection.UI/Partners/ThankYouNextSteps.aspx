﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="ThankYouNextSteps.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ThankYouNextSteps" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var _isDebug;
        function OpenPopup(url, name, width, height, toolbars, scrollbars, status, menubar, resizable) {

            var nScreenWidth = window.screen.width;
            var nScreenHeight = window.screen.height;
            var sWindowsLocationText = "";

            if (nScreenWidth != "" && nScreenHeight != "") {
                sWindowsLocationText = ",left=" + (nScreenWidth - width) / 2;
                sWindowsLocationText += ",top=" + (nScreenHeight - height) / 2;  // Special to remove space for status bar
            }

            if (_isDebug == true) {
                status = "Yes";
                resizable = "Yes";
            }

            var parameters = "width=" + width + ",height=" + height + ",toolbars=" + toolbars + ",scrollbars=" + scrollbars + ",status=" + status + ",menubar=" + menubar + ",resizable=" + resizable + sWindowsLocationText;

            var myPopup = window.open(url, name, parameters);
            return myPopup;
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="recommendedPartnersHyperLink" runat="server" CBID="3" Text="Recommended Partners"
            NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:HyperLink ID="becomePartnerBreadcrumHyperLink" runat="server" CBID="37" Text="Become a Partner"
            NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="thankYouHeaderLabel" CBID="1083" Text="Thank You" runat="server" />
    </div>
    <div class="main-content">
    </div>
    <div id="resultMessageDiv" runat="server">
    </div>
    <asp:UpdatePanel ID="printUpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="main">
                <div class="box1 thankyou">
                    <div class="steps">
                        <asp:HyperLink ID="becomeRecomendedPartnerStepHyperlink" runat="server" class="done"></asp:HyperLink>
                        <asp:HyperLink ID="applicationDeatilStepHyperLink" runat="server" class="done"></asp:HyperLink>
                        <asp:HyperLink ID="reviewApplicationStepHyperLink" runat="server" class="done"></asp:HyperLink>
                        <asp:HyperLink ID="thankYouHyperStepLink" runat="server" class="active"></asp:HyperLink>
                    </div>
                    <h4>
                        <asp:Label ID="reviewApplicationLabel" runat="server" CBID="86"></asp:Label></h4>
                    <h2>
                        <asp:Label ID="thankYouLabel" runat="server" CBId="1002" Text="Thank You for Your Request"></asp:Label></h2>
                    <p>
                        <asp:Label ID="thankYouBodyLabel" Text="" runat="server" CBID="1003"></asp:Label>
                    </p>
                    <h2>
                        <asp:Label ID="nextStepsLabel" runat="server" Text="Next Steps…" CBID="1004"></asp:Label></h2>
                    <ul class="nostyle">
                        <li><span class="number">1</span>
                            <h3>
                                <asp:Label ID="firstStepHighlightLabel" runat="server" Text="Print and mail a copy of the application along with the following list of required criteria:"
                                    CBID="1005"></asp:Label></h3>
                            <p class="go">
                                <asp:LinkButton ID="printLinkButton" CBId="521" Text="PRINT" runat="server"></asp:LinkButton>
                            </p>
                        </li>
                    </ul>
                    <p>
                        <asp:Label ID="firstStepTextLabel" runat="server" CBID="1008" CssClass="NextStepsMargin"
                            Text="Hilton Partner Processing Center 1234 Hilton St. 1-01-333 Houston, TX 78900"></asp:Label></p>
                    <ul class="nostyle">
                        <li><span class="number">2</span>
                            <h3>
                                <asp:Label ID="secondStepHighlightLabel" runat="server" Text="Please forward all submittals via mail to the address listed below:"
                                    CBID="1006"></asp:Label></h3>
                        </li>
                    </ul>
                    <p class="NextStepsMargin">
                        <asp:Label ID="secondStepTextLabel" runat="server" CBID="1009" Text=""></asp:Label></p>
                    <ul class="nostyle">
                        <li><span class="number">3</span>
                            <h3>
                                <asp:Label ID="thirdStepHighlightLabel" runat="server" CBID="1007" Text="The review process may take up to six weeks from receipt of the mailed application and criteria. After your application has been reviewed by various internal product consultants, you will be notified of its status and next steps via email."></asp:Label></h3>
                            <p>
                        </li>
                    </ul>
                    <asp:Label ID="thirdStepTextLabel" runat="server" Text=""></asp:Label></p>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" CBID="3" Text="Recommended Partners" runat="server"></asp:Label></h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="browsePartnersHyperLink" runat="server" CBID="32" Text="Browse Partners"
                            NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="becomePartnerHyperLink" class="active" CBID="84" Text="Become a Partner"
                            runat="server"></asp:HyperLink>
                        <ul>
                            <li>
                                <asp:HyperLink ID="chooseCategoriesHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"
                                    CBID="983" Text="Step 1: Choose Categories"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="completeApplicationHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/ApplicationDetails.aspx"
                                    CBID="984" Text="Step 2: Complete Application"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="reviewApplicationHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/ReviewApplication.aspx"
                                    CBID="985" Text="Step 3: Review Application"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="thankYouHyperLink" runat="server" class="active" NavigateUrl="~/Partners/ThankYouNextSteps.aspx"
                                    CBID="986" Text="Step 4: Thank You"></asp:HyperLink>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>