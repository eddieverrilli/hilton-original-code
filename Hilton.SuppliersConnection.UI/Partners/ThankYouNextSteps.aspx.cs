﻿using System;
using System.Globalization;
using System.Threading;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ThankYouNextSteps : PageBase
    {
        /// <summary>
        /// Invoked on OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    ClearSession(SessionConstants.PrintPartnerApplication);
                    ClearSession(SessionConstants.ApplicationDetails);
                    ClearSession(SessionConstants.FilterCriteria);
                    ClearSession(SessionConstants.AppliedOpportunityDetails);
                    ClearSession(SessionConstants.EditPartnerApplication);
                    ClearSession(SessionConstants.AllCascadeCategoryIds);
                    ClearSession(SessionConstants.ProductImageBytes);
                    Response.Redirect("~/");
                }
                this.Title = ResourceUtility.GetLocalizedString(87, culture, "Apply to Become a Partner");
                base.OnInit(e);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ClearSession(SessionConstants.ApplicationDetails);
                ClearSession(SessionConstants.FilterCriteria);
                ClearSession(SessionConstants.AppliedOpportunityDetails);
                ClearSession(SessionConstants.EditPartnerApplication);
                ClearSession(SessionConstants.AllCascadeCategoryIds);
                ClearSession(SessionConstants.ProductImageBytes);

                string jsToolbar = "No";
                string jsMenubar = "No";
                string jsScrollbars = "Yes";
                string jsStatus = "Yes";
                string jsResizable = "Yes";
                string name = "Popup";
                int width = 820;
                int height = 400;

                string url = ResolveUrl("~/Partners/PrintApplication.aspx");

                printLinkButton.Attributes.Add("onClick", "javascript:OpenPopup(\"" + url + "\",\"" + name + "\",\"" + width.ToString(CultureInfo.InvariantCulture) + "\",\"" + height.ToString(CultureInfo.InvariantCulture) + "\",\"" + jsToolbar + "\",\"" + jsScrollbars + "\",\"" + jsStatus + "\",\"" + jsMenubar + "\",\"" + jsResizable + "\");");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}