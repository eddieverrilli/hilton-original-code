﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="PartnerProfile.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.PartnerProfile"
    ViewStateMode="Enabled" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
    <link type="text/css" href='../Styles/moreless.css' rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/thickbox.js"></script>
    <script src="../Scripts/jquery.downloader.js" type="text/javascript"></script>
    <script type="text/javascript">


        function pageLoad() {
            var showChar = 200;
            var ellipsestext = "...";
            var moretext = "more";
            var lesstext = "less";
            $('.more').each(function () {
                var content = $(this).html();
                if (content.length > showChar) {
                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar - 1, content.length - showChar + 1);
                    //var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                    //var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span><span class="morecontent" ><span>' + h + '</span>';
                    var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span><span class="morecontent" ><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="lesslink">less</a>';
                    $(this).html(html);
                    $('.morecontent').hide();
                    $('.morelink').addClass("more"); 
                }
            });
            $(".morelink").click(function () {
                if ($(this).hasClass("more")) {
                    $(this).removeClass("more");
                    $(this).addClass("less");
                    $(this).parent().find('.morecontent').show();
                    $(this).html(moretext);
                    $(this).parent().find('.moreellipses').hide();

                }
                return false;
            });
            $(".lesslink").click(function () {
                $(this).parent().hide();
                $(this).parent().parent().find('.morelink').removeClass("less");
                $(this).parent().parent().find('.morelink').addClass("more");
                $(this).parent().parent().find('.moreellipses').show();
                return false;
            });
        }

       
       


</script>
<script type="text/javascript">
        function ForceCall() {
            tb_init('a.thickbox');
            imgLoader = new Image();
            imgLoader.src = tb_pathToImage;

            $('.comment').hover(function () {
                $(this).children('.tooltip').show();
            },

            function () {
                $(this).children('.tooltip').hide();
            });

        }
        $(window).ready(function () {
            $('.accordion h5').next().hide();
            $('.accordion h5').click(function () {
                $(this).toggleClass('active');
                $(this).next().slideToggle();
            });
        });

        function ForceHover() {
            $('.comment').hover(function () {
                $(this).children('.tooltip').show();
            },

            function () {
                $(this).children('.tooltip').hide();
            });

            $('.accordion h5').next().hide();
            $('.accordion h5').click(function () {
                $(this).toggleClass('active');
                $(this).next().slideToggle();
            });
        }

        function SetHref() {
            var contactEmail = $('#<%=selectedLnkEmailHiddenValue.ClientID%>').val();
            var partnerId = $('#<%=partnerIdHiddenValue.ClientID%>').val();
            var contactId = $('#<%=contactIdHiddenValue.ClientID%>').val();
            var contactTypeId = $('#<%=contactTypeIdHiddenValue.ClientID%>').val();
            var regionId = $('#<%=regionIdHiddenValue.ClientID%>').val();
            var toFirstName = $('#<%=toFirstNameHiddenValue.ClientID %>').val();
            var toLastName = $('#<%=toLastNameHiddenValue.ClientID %>').val();
            $('#<%=emailHyperLink.ClientID%>').attr('href', '/Partners/EmailAlert.aspx?keepThis=true&contactEmail=' + contactEmail.toString() + '&partnerId=' + partnerId.toString() + '&contactId=' + contactId.toString() + '&contactTypeId=' + contactTypeId.toString() + '&regionId=' + regionId.toString() + '&toFirstName=' + toFirstName.toString() + '&toLastName=' + toLastName.toString() + '&TB_iframe=true&height=440&width=780');
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="recommededPartersHyperLink" runat="server" CBID="3" Text="Recommended Partners"
            NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:HyperLink ID="browsePartnerHyperLink" runat="server" CBID="32" Text="Browse Partners"
            NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
        <span>/</span> <span>
            <asp:Label ID="myProductsLabel" CBID="83" Text="Partner Profile" runat="server"></asp:Label></span></div>
    <div class="main-content">
        <h2>
            <asp:Label ID="partnerPforileLabel" runat="server" CBID="83"></asp:Label></h2>
        <div class="back-list">
            <asp:HyperLink ID="backToListHyperLink" Text="<< Back To List" runat="server" NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
        </div>
        <div>
        </div>
        <div class="clear">
            &nbsp;</div>
        <asp:UpdatePanel ID="contactUpdatePanel" ViewStateMode="Enabled" runat="server" UpdateMode="Conditional"
            class="contactdetails">
            <ContentTemplate>
                <div class="profile-header">
                    <asp:Image ID="logoImage" runat="server" ViewStateMode="Enabled" CssClass="pic imagestyle" />
                    <h4>
                        <asp:Label ID="vendorNameLabel" runat="server"></asp:Label><span class="cmt submit-feedback comment">
                            <asp:HyperLink ID="commentIconImage" runat="server" ViewStateMode="Enabled" CssClass="thickbox"
                                ImageUrl="../../Images/product/comment-icon.png"></asp:HyperLink>
                            <span class="tooltip">
                                <img src="../Images/product/submit-feedback.png" alt="" width="110" height="19" />
                            </span></span>
                    </h4>
                    <p>
                        <asp:Image ID="hiltonIconImage" ToolTip="Hilton IsSAM Image" ViewStateMode="Enabled"
                            runat="server" ImageUrl="../../Images/hilton-icon.gif" />&nbsp;
                        <asp:Label ID="vendorLabel" Text="Hilton SAM Vendor" CssClass="aligntop" ViewStateMode="Enabled"
                            runat="server"></asp:Label></p>
                    <div class="content">
                        <p>
                            <asp:Label ID="descriptionLabel" runat="server" ViewStateMode="Enabled"></asp:Label></p>
                    </div>
                </div>
                <div id="contactList" style="float: right" runat="server">
                    <asp:DropDownList ID="contactDropDown" ViewStateMode="Enabled" class="select-listbox"
                        AutoPostBack="true" OnSelectedIndexChanged="DropDownChange" runat="server">
                    </asp:DropDownList>
                    <br />
                    <br />
                    <div class="profile-detail">
                        <h4>
                            <asp:Label ID="nameLabel" ViewStateMode="Enabled" runat="server"></asp:Label></h4>
                        <p>
                            <asp:Label ID="TitleLabel" ViewStateMode="Enabled" runat="server"></asp:Label>
                            <asp:Label ID="addressLabel" runat="server" ViewStateMode="Enabled"></asp:Label>
                          <%--  <asp:Label ID="address2Label" runat="server" ViewStateMode="Enabled"></asp:Label>--%>
                            <asp:Label ID="cityStateZipLabel" runat="server" ViewStateMode="Enabled"></asp:Label>
                           <%-- <asp:Label ID="stateLabel" runat="server" ViewStateMode="Enabled"></asp:Label>
                            <asp:Label ID="zipCodeLabel" runat="server" ViewStateMode="Enabled"></asp:Label>--%>
                            <asp:Label ID="countryLabel" runat="server" ViewStateMode="Enabled"></asp:Label>
                            </p>
                        <p class="email">
                            <asp:LinkButton ID="emailHyperLink" ViewStateMode="Enabled" OnClientClick="SetHref()" 
                                CssClass="thickbox" runat="server"></asp:LinkButton>
                           <a id="emaillink" runat="server" visible="false"></a>
                           </p>
                        <p class="ph">
                            <asp:Label ID="phoneLabel" ViewStateMode="Enabled" runat="server"></asp:Label>
                        </p>
                        <p class="website">
                            <a ID="webSiteLink" target="_blank" ViewStateMode="Enabled" runat="server"></a></p>
                    </div>
                </div>
                <asp:HiddenField ID="contactIdHiddenValue" ViewStateMode="Enabled" runat="server" />
                <asp:HiddenField ID="selectedLnkEmailHiddenValue" ViewStateMode="Enabled" runat="server" />
                <asp:HiddenField ID="partnerIdHiddenValue" ViewStateMode="Enabled" runat="server" />
                <asp:HiddenField ID="contactTypeIdHiddenValue" ViewStateMode="Enabled" runat="server" />
                <asp:HiddenField ID="regionIdHiddenValue" ViewStateMode="Enabled" runat="server" />
                <asp:HiddenField ID="toFirstNameHiddenValue" ViewStateMode="Enabled" runat="server" />
                <asp:HiddenField ID="toLastNameHiddenValue" ViewStateMode="Enabled" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="resultMessageDiv" runat="server">
    </div>
    <br />
    <div class="main">
        <div class="partner-profile">
            <div class="accordion">
                <h3>
                    <asp:Label ID="headerLabel" CBID="233" runat="server"></asp:Label></h3>
                <asp:UpdatePanel ID="categoryProductsUpdatePanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="search-result">
                            <div class="label">
                                <asp:Label ID="filterResultsLabel" runat="server" CBId="769"></asp:Label></div>
                            <div class="brands">
                                <asp:DropDownList ID="brandDropDown" ViewStateMode="Enabled" runat="server" AutoPostBack="false"
                                    >
                                </asp:DropDownList>&nbsp;&nbsp;</div > 
                           
                            <div class="region-with-margin">
                                <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged">
                                </asp:DropDownList>&nbsp;&nbsp;<asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="go" style="float: right;margin-right:127px;">
                               
                                <asp:LinkButton ID="goLinkButton" OnClick="GoLinkButton_Click" CBID="398" runat="server">Go</asp:LinkButton>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="resultCategoriesDiv" runat="server">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="categoriesResultUpdatePanel" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="goLinkButton" EventName="Click" />
                    </Triggers>
                    <ContentTemplate>
                        <div class="property-list-detail" id="divPropertyListDetails" runat="server">
                            <asp:Repeater ID="categoryProductsRepeater" runat="server" OnItemCommand="CategoryProductsRepeater_ItemCommand"
                             OnItemDataBound="CategoryProductsRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <div id="categoryProductsDivision" class="accordion prod-detail" runat="server">
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:HiddenField ID="userTypeHiddenField" runat="server" />
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="Recommended Partners" CBID="3" CssClass="upper"
                    runat="server"></asp:Label></h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="browsePartnersHyperLink" runat="server" class="active" CBID="32"
                            Text="Browse Partners" NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
                        <ul>
                            <li>
                                <asp:HyperLink ID="searchPartnersHyperLink" class="active" CBID="83" Text="Partner Profile"
                                    runat="server"></asp:HyperLink></li>
                        </ul>
                    </li>
                    <li id="becomePartnerListItem" runat="server">
                        <asp:HyperLink ID="becomePartnerHyperLink" runat="server" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx">Become a Partner</asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="mask">
    </div>
</asp:Content>