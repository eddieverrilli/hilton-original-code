﻿<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/MasterPages/WebMaster.master"
    CodeBehind="PrintApplication.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.PrintApplication" %>

<asp:Content ID="applicationContent" ContentPlaceHolderID="cphContent" runat="Server">
    <link type='text/css' href='../Styles/print.css' rel='stylesheet' />
    <div id="resultMessageDiv" runat="server">
    </div>
    <div class="main">
        <div class="review-application">
            <div class="accordion">
                <div class="print-btn">
                    <p class="go">
                        <asp:LinkButton ID="printHyperLink" Text="PRINT" OnClientClick="javascript:window.print();"
                            runat="server"></asp:LinkButton></p>
                </div>
                <h3>
                    <asp:Label ID="selectedCategoriesLabel" runat="server" CBID="659" Text="Selected Categories"></asp:Label></h3>
                <div class="panel3">
                    <asp:GridView ID="selectedCategoriesGridView" GridLines="None" runat="server" AutoGenerateColumns="false">
                        <RowStyle BorderStyle="None" />
                        <Columns>
                            <asp:TemplateField ItemStyle-CssClass="col1" HeaderStyle-CssClass="col1" HeaderText="CATEGORY">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "CategoryDescription")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col2" HeaderStyle-CssClass="col2" HeaderText="REGIONS">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "RegionName") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col3" HeaderStyle-CssClass="col2" HeaderText="BRAND">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "BrandName") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TYPE" ItemStyle-CssClass="col4">
                                <ItemTemplate>
                                    <itemstyle cssclass="col4"></itemstyle>
                                    <%# DataBinder.Eval(Container.DataItem, "PropertyTypeName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="accordion">
                <h3>
                    <asp:Label ID="partnerInformation" runat="server" CBID="491" Text="Partner Information"></asp:Label></h3>
                <div class="panel1">
                    <h4>
                        <asp:Label ID="accountTypeLabel" runat="server" CBID="164" Text="Account Type"></asp:Label></h4>
                    <table>
                        <tr id="goldBlockTableRow" style="display: none;" runat="server">
                            <td class="col2 wid">
                                <div>
                                    <span class="gold-level"></span>
                                    <div id="gold-partner-img">
                                        <asp:Image ID="goldPartnerImageIcon" runat="server" /></div>
                                    <h6>
                                        <asp:Label ID="goldlevelPartner" runat="server" Text="Gold-Level Partner" CBID="977"></asp:Label></h6>
                                    <p>
                                        <asp:Label ID="goldPartnerDescription" runat="server" CBID="976" Text="Silver-Level benefits, plus access to Hilton’s Construction Report and Lead Generation."></asp:Label></p>
                                </div>
                            </td>
                            <td class="col3 printpad">
                                <h6>
                                    <asp:Label ID="goldPartnershipAmountLabel" runat="server"></asp:Label>
                                </h6>
                                <p>
                                    <asp:Label ID="goldPerYearLabel" runat="server" CBID="509" Text="PER YEAR"></asp:Label></p>
                            </td>
                        </tr>
                        <tr id="silverBlockTableRow" style="display: none;" runat="server">
                            <td class="col2 wid">
                                <div>
                                    <span class="normal-level"></span>
                                    <div id="partner-img">
                                        <asp:Image ID="partnerImageIcon" runat="server" /></div>
                                    <h6>
                                        <asp:Label ID="regularPartner" runat="server" CBID="978" Text="Partner"></asp:Label></h6>
                                    <p>
                                        <asp:Label ID="regularDescription" runat="server" CBID="979" Text="Listing on Suppliers’ Connection"></asp:Label></p>
                                </div>
                            </td>
                            <td class="col3 printpad">
                                <h6>
                                    <asp:Label ID="regularPartnershipAmountLabel" runat="server"></asp:Label>
                                </h6>
                                <p>
                                    <asp:Label ID="regularPerYearLabel" runat="server" CBID="509" Text="PER YEAR"></asp:Label></p>
                            </td>
                        </tr>
                    </table>
                    <div class="clear">
                        &nbsp;</div>
                    <h4>
                        <asp:Label ID="companyInformationLabel" runat="server" Text="Company Information"
                            CBID="137"></asp:Label></h4>
                    <div class="row">
                        <div class="label">
                            <asp:Label ID="companyNameStaticLabel" runat="server" CBID="134" Text="Company Name"></asp:Label></div>
                        <div class="input">
                            <asp:Label ID="companyNameLabel" runat="server"></asp:Label>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="companyLogoStaticLabel" runat="server" CBID="980" Text="company logo"></asp:Label></div>
                            <div class="input">
                                <asp:Image ID="companyLogoImage" CssClass="pics imagestyle" ImageUrl="~/Images/company-logo.gif"
                                    runat="server" /></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                Company Address</div>
                            <div class="input">
                                <p>
                                    <asp:Label ID="companyAddressLabel" runat="server"></asp:Label></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="webSiteLinkStaticLabel" runat="server" Text="WEB SITE" CBID="0"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="webSiteLinkLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="countryStaticLabel" runat="server" CBID="307" Text="Country"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="countryLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="cityStaticLabel" runat="server" Text="City" CBID="306"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="cityLabel" runat="server"></asp:Label>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="stateStaticLabel" runat="server" Text="State" CBID="311"></asp:Label></div>
                                <div class="input">
                                    <asp:Label ID="stateLabel" runat="server"></asp:Label></div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="zipCodeStaticLabel" runat="server" CBID="312" Text="Zip Code"></asp:Label></div>
                                <div class="input">
                                    <asp:Label ID="zipCodeLabel" runat="server"></asp:Label>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="companyDescriptionStaticLabel" runat="server" Text="Company description"
                                            CBID="981"></asp:Label></div>
                                    <div class="input">
                                        <asp:Label ID="companyDescriptionLabel" runat="server"></asp:Label>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="panel1">
                                    <h4>
                                        <asp:Label ID="contactInformationLabel" runat="server" Text="Contact Information"
                                            CBID="324"></asp:Label></h4>
                                    <h6 class="pad20">
                                        <asp:Label ID="primaryContactLabel" runat="server" CBID="519" Text="Primary Contact"></asp:Label></h6>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="firstNameLabel" runat="server" CBID="99" Text="FIRST NAME"></asp:Label></div>
                                        <div class="input">
                                            <asp:HiddenField ID="primaryContactIdHiddenField" runat="server" />
                                            <asp:Label ID="firstNamePrimaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="lastNameLabel" runat="server" Text="LAST NAME" CBID="111"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="lastNamePrimaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="title" runat="server" Text="TITLE" CBID="710"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="titlePrimaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="phoneLabel" runat="server" Text="PHONE" CBID="124"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="phonePrimaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="faxLabel" runat="server" CBID="141" Text="Fax"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="faxPrimaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="emailAddressLabel" runat="server" CBID="384" Text="EMAIL ADDRESS"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="emailAddressPrimaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row border">
                                        <img width="638" height="1" src="../Images/dotted-bg1.gif" alt="" />
                                    </div>
                                    <h6 class="pad20">
                                        <asp:Label ID="secondaryContactLabel" runat="server" CBID="982" Text="Secondary Contact"></asp:Label></h6>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="firstNameSecondaryContactStaticLabel" runat="server" Text="FIRST NAME"
                                                CBID="99"></asp:Label></div>
                                        <div class="input">
                                            <asp:HiddenField ID="secondaryContactIdHiddenField" runat="server" />
                                            <asp:Label ID="firstNameSecondaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="lastNameSecondaryContactStaticLabel" runat="server" Text="LAST NAME"
                                                CBID="111"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="lastNameSecondaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="titleSecondaryContactStaticLabel" runat="server" Text="TITLE" CBID="710"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="titleSecondaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="phoneSecondaryContactStaticLabel" runat="server" Text="PHONE" CBID="124"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="phoneSecondaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="faxSecondaryContactStaticLabel" runat="server" Text="Fax" CBID="141"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="faxSecondaryContact" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label">
                                            <asp:Label ID="emailAddressSecondaryContactStaticLabel" runat="server" Text="EMAIL ADDRESS"
                                                CBID="384"></asp:Label></div>
                                        <div class="input">
                                            <asp:Label ID="emailSecondaryContactLabel" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row border">
                                        <img width="638" height="1" src="../Images/dotted-bg1.gif" alt="" /></div>
                                    <asp:PlaceHolder ID="placeHolder1" runat="server"></asp:PlaceHolder>
                                    <asp:Repeater EnableViewState="false" ID="regionContactRepeater" runat="server">
                                        <ItemTemplate>
                                            <h6 class="pad20">
                                                <asp:Label ID="regionNameLabel" Text='<%# DataBinder.Eval(Container.DataItem, "RegionName")%>'
                                                    runat="server"></asp:Label>
                                                <asp:Label ID="regionIdLabel" Text='<%# DataBinder.Eval(Container.DataItem, "RegionId")%>'
                                                    Visible="false" runat="server"></asp:Label>
                                            </h6>
                                            <div class="row">
                                                <div class="label">
                                                    <asp:HiddenField ID="regionalContactIdHiddenField" runat="server" />
                                                    <asp:Label ID="firstNameRegionalContactLabel" Text="FIRST NAME" CBID="99" runat="server"></asp:Label></div>
                                                <div class="input ">
                                                    <asp:Label ID="firstNameInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName")%>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="label">
                                                    <asp:Label ID="lastNameRegionalContactLabel" Text="LAST NAME" CBID="111" runat="server"></asp:Label></div>
                                                <div class="input ">
                                                    <asp:Label ID="lastNameInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastName")%>'></asp:Label></div>
                                            </div>
                                            <div class="row">
                                                <div class="label">
                                                    <asp:Label ID="titleRegionalContactLabel" Text="TITLE" CBID="710" runat="server"></asp:Label></div>
                                                <div class="input">
                                                    <asp:Label ID="titleInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="label">
                                                    <asp:Label ID="phoneRegionalContactLabel" Text="PHONE" CBID="124" runat="server"></asp:Label></div>
                                                <div class="input textbox-four">
                                                    <asp:Label ID="phoneInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Phone")%>'></asp:Label></div>
                                            </div>
                                            <div class="row">
                                                <div class="label">
                                                    <asp:Label ID="faxRegionalContactLabel" Text="FAX" CBID="141" runat="server"></asp:Label></div>
                                                <div class="input textbox-four">
                                                    <asp:Label runat="server" ID="faxInputRegionalContactLabel" Text='<%# DataBinder.Eval(Container.DataItem, "Fax")%>'></asp:Label></div>
                                            </div>
                                            <div class="row">
                                                <div class="label">
                                                    <asp:Label ID="emailRegionalContactLabel" Text="EMAIL ADDRESS" CBID="384" runat="server"></asp:Label></div>
                                                <div class="input textbox-one">
                                                    <asp:Label ID="emailInputRegionalContactLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Email")%>'></asp:Label></div>
                                            </div>
                                            <div class="row editbtn">
                                                <div class="label">
                                                    &nbsp;</div>
                                            </div>
                                            <div class="row border">
                                                <img width="638" height="1" src="../Images/dotted-bg1.gif" alt="" /></div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>
