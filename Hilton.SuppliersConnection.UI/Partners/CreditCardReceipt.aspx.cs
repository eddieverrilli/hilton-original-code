﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class CreditCardReceipt : PageBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Title = ResourceUtility.GetLocalizedString(90, culture, "Credit Card Receipt");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    List<Contact> contactDetails = GetSession<List<Contact>>(SessionConstants.ContactDetails);
                    PaymentGatewayResponse paymentGatewayResponse = GetSession<PaymentGatewayResponse>(SessionConstants.PaymentGatewayResponse);
                    ClearSession(SessionConstants.PaymentGatewayResponse);

                    PartnerPayment partnerPayment = GetSession<PartnerPayment>(SessionConstants.PartnerPayment);
                    ClearSession(SessionConstants.PartnerPayment);
                    //NameValueCollection nvc = Request.Form;
                    //string merchantId = string.Empty;

                    //if (!string.IsNullOrEmpty(nvc["ssl_amount"]))
                    //{
                    //    lblAmt.Text = nvc["ssl_amount"].ToString();
                    //}
                    //if (!string.IsNullOrEmpty(nvc["ssl_txn_id"]))
                    //{
                    //    lblId.Text = nvc["ssl_txn_id"].ToString();
                    //}
                    //if (!string.IsNullOrEmpty(nvc["ssl_txn_time"]))
                    //{
                    //    lblDate.Text = nvc["ssl_txn_time"].ToString();
                    //}

                    //GoldAmt.Text = ;
                    if (partnerPayment != null)
                    {
                        SetLabelText(amountLabel, Convert.ToDecimal(partnerPayment.AmountRequested, CultureInfo.InvariantCulture).ToString("C", new CultureInfo("en-US")));
                        if (partnerPayment.PartnershipType == (int)PartnershipTypeEnum.GoldPartner)
                        {
                            goldPartnerDiv.Visible = true;
                            partnerDiv.Visible = false;
                        }
                        else
                        {
                            goldPartnerDiv.Visible = false;
                            partnerDiv.Visible = true;
                        }
                    }

                    SetLabelText(transactionAmountLabel, Convert.ToDecimal(paymentGatewayResponse.Amount, CultureInfo.InvariantCulture).ToString("C", new CultureInfo("en-US")));
                    SetLabelText(transactionIdLabel, paymentGatewayResponse.TransactionId);
                    SetLabelText(transactionDateLabel, paymentGatewayResponse.TransactionDatetime.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture));

                    Contact contact;
                    contact = contactDetails.Where(x => x.ContactTypeId == (int)ContactTypeEnum.Primary).First();
                    firstNamePrimaryTextBox.Text = contact.FirstName;
                    lastNamePrimaryTextBox.Text = contact.LastName;
                    titlePrimaryTextBox.Text = contact.Title;
                    phonePrimaryTextBox.Text = contact.Phone;
                    faxPrimaryTextBox.Text = contact.Fax;
                    emailPrimaryTextBox.Text = contact.Email;

                    contact = contactDetails.Where(x => x.ContactTypeId == (int)ContactTypeEnum.Billing).First();
                    firstNameBillingTextBox.Text = contact.FirstName;
                    lastNameBillingTextBox.Text = contact.LastName;
                    titleBillingTextBox.Text = contact.Title;
                    phoneBillingTextBox.Text = contact.Phone;
                    faxBillingTextBox.Text = contact.Fax;
                    emailBillingTextBox.Text = contact.Email;
                    SetLabelText(creditCardReceiptBodyLabel, string.Format("{0} {1}", ResourceUtility.GetLocalizedString(1061, culture, "Congratulations, your account has been activated. Please record the information below. A copy has been emaild to"), contact.Email));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                mainDiv.Visible = false;
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}