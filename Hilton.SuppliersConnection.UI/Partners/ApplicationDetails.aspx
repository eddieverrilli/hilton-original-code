﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="ApplicationDetails.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ApplicationDetails" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../Scripts/custominputs.js"></script>
     <script src="../Scripts/jquery.characterlimiter.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function pageLoad() {
            $('textarea#companyDescriptionTextBox').limiter(2000, $('#remainingCharacters'));
        }
        $(document).ready(function () {
            SetFileInputStyles();           
        });

        function SetFileInputStyles() {
            $("input.customInput").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 65
            });

            $("input.customInput1").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 65
            });
        }

        function AjaxFileUpload(control, fileType) {
            if (fileType == 'image') {
                var imageName = $('#companyLogoFileUpload')[0].value.toString().substring($('#companyLogoFileUpload')[0].value.toString().lastIndexOf('\\') + 1, $('#companyLogoFileUpload')[0].value.length);
                $('#<%= imageNameHiddenField.ClientID %>').val(imageName);
            }
            if (fileType == 'image')
                $('#loading').attr('source', fileType);

            $("#loading")
                .ajaxStart(function (obj) {
                    if ($(this).attr('source') == 'image') {
                        $(this).show();
                        $(this).attr('source', '');
                    }

                })
                .ajaxComplete(function (obj) {
                    $(this).hide();
                });

            $.ajaxFileUpload
                    (
                        {
                            url: '../Handlers/FileUpload.ashx?fileType=' + fileType,
                            secureuri: false,
                            fileElementId: control,
                            dataType: 'json',
                            data: { name: 'login', id: 'id' },
                            success: function (data, status) {
                                if (status == 'success' && data.responseText != null && data.responseText == 'uploaded') {
                                    SetFileLink(fileType);

                                    if (fileType == 'image') {
                                        $.ajax({
                                            type: "POST",
                                            url: "../Handlers/FileUpload.ashx?fileType=" + fileType + "&op=show",
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (data, img) {
                                                $('#<%= companyLogoImage.ClientID %>').attr('src', "data:image/png;base64," + data);
                                            },
                                            error: function (data, status, e) {
                                            }
                                        });
                                    }
                                }
                                else {
                                    ValidateFile(fileType, data.responseText);
                                }
                            },
                            error: function (data, status, e) {
                            }
                        }
                    )
            return false;
        }

        function disabler(event) {
            event.preventDefault();
            return false;
        }
        function SetFileLink(fileType) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text('');
                    $('#imageErrSpan').hide();
                    $('#<%= imageLinkButton.ClientID %>').text($('#<%= imageNameHiddenField.ClientID %>').val());
                    $('#<%= imageLinkButton.ClientID %>').bind('click', disabler);
                    if ($('#imageNameHiddenField.ClientID').val() != '') {
                        $('#<%= removeImageLinkButton.ClientID %>').show();
                    }
                    break;
            }
        }

        function ValidateFile(fileType, msg) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text(msg);
                    $('#imageErrSpan').show();
                    $('#<%= removeImageLinkButton.ClientID %>').hide();
                    $('#<%= imageNameHiddenField.ClientID %>').val("");
                    $('#<%= imageLinkButton.ClientID %>').text("");
                    $('#<%= companyLogoImage.ClientID %>').attr('src', '../Images/NoProduct.PNG');
                    break;
            }

        }
    </script>
</asp:Content>
<asp:Content ID="applicationContent" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="breadcrumbs">
        <asp:HyperLink ID="recommendedPartnersHyperLink" runat="server" Text="Recommended Partners"
            CBID="586" NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
        <span>/</span>
        <asp:HyperLink ID="becomePartnerHyperLinkNavigation" runat="server" CBID="37" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"
            Text="Become a Partner"></asp:HyperLink>
        <span>/</span>
        <asp:Label ID="completeApplicationLabel" runat="server" CBID="1054" Text="Complete Application"></asp:Label>
    </div>
    <div id="resultMessageDiv" runat="server">
    </div>
    <br />
    <div class="main">
        <div class="box1">
            <div class="steps">
                <asp:HyperLink ID="becomeRecomendedPartnerStepHyperlink" runat="server" class="done"></asp:HyperLink>
                <asp:HyperLink ID="applicationDeatilStepHyperLink" runat="server" class="active"></asp:HyperLink>
                <asp:HyperLink ID="reviewApplicationStepHyperLink" runat="server" class=""></asp:HyperLink>
                <asp:HyperLink ID="thankYouHyperStepLink" runat="server" class=""></asp:HyperLink>
            </div>
            <h2>
                <asp:Label ID="titleLabel" runat="server" Text="" CBID="85"></asp:Label></h2>
            <p>
                <asp:Label ID="introLable" runat="server" Text="" CBID="987"></asp:Label>
            </p>
        </div>
        <div class="application-detail">
            <div class="accordion">
                <h3>
                    <asp:Label ID="accountTypeHeader" runat="server" CBID="167"></asp:Label></h3>
                <div class="panel3">
                    <table width="100%">
                        <tr>
                            <td class="eligible" colspan="3">
                                <asp:Label ID="choosePartnerLabel" runat="server" Text="Choose the partner type for which you’d like to apply:"
                                    CBID="988"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="col1">
                                <asp:RadioButton ID="goldLevelPartnerRadioButton" GroupName="PartnerShipType" runat="server" />
                            </td>
                            <td class="col2">
                                <div>
                                    <span class="gold-level"></span>
                                    <h6>
                                        <asp:Label ID="goldPartnerLabel" runat="server" CBID="410"></asp:Label></h6>
                                    <p>
                                        <asp:Label ID="goldPartnerDescriptionLabel" runat="server" CBID="989"></asp:Label>
                                    </p>
                                    <p>
                                        <asp:HyperLink ID="sampleExcelLinkButton" CBID="1055" runat="server" NavigateUrl="~/Excel/ConstructionReport.xls"
                                            Text="Download sample Construction Report"></asp:HyperLink>
                                    </p>
                                </div>
                            </td>
                            <td class="col3">
                                <h6>
                                    <asp:Label ID="goldPartnershipAmountLabel" runat="server"></asp:Label>
                                </h6>
                                <p>
                                    <asp:Label ID="perYearGoldLabel" runat="server" CBID="508"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="border">
                            </td>
                        </tr>
                        <tr>
                            <td class="col1">
                                <asp:RadioButton ID="regularPartnerRadioButton" GroupName="PartnerShipType" runat="server" />
                            </td>
                            <td class="col2">
                                <div>
                                    <span class="normal-level"></span>
                                    <h6>
                                        <asp:Label ID="regularPartnerLabel" runat="server" CBID="611"></asp:Label></h6>
                                    <p>
                                        <asp:Label ID="regularPartnerDescription" runat="server" CBID="990"></asp:Label>
                                </div>
                            </td>
                            <td class="col3">
                                <h6>
                                    <asp:Label ID="regularPartnershipAmountLabel" runat="server"></asp:Label>
                                </h6>
                                <p>
                                    <asp:Label ID="perYearRegularLabel" runat="server" CBID="508"></asp:Label></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="accordion">
                <h3>
                    <asp:Label ID="companyInformationLabel" runat="server" CBID="310"></asp:Label></h3>
                <div class="panel3">
                    <div class="form">
                        <div class="form-field">
                            <div class="form-label">
                            </div>
                            <div class="form-input">
                                <asp:Label ID="companyInformationInstructionLabel" Text="* Indicates a Required Field"
                                    runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="companyNameLabel" runat="server" CBId="133"></asp:Label><asp:Label
                                    ID="mandatoryCompanyNameLabel" runat="server" Text=" *"></asp:Label></div>
                            <div class="form-input textbox-one">
                                <asp:HiddenField ID="partnerIdHiddenField" runat="server" />
                                <asp:TextBox ID="companyNameTextBox" AutoComplete="off" runat="server" MaxLength="256"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="companyNameRequiredFieldValidator" VMTI="12" SetFocusOnError="true"
                                    ViewStateMode="Enabled" runat="server" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    ErrorMessage="Company Name is required" ControlToValidate="companyNameTextBox"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="companyLogoLabel" runat="server" Text="" CBID="991"></asp:Label></div>
                            <asp:UpdatePanel ID="imageUploadUpdatePanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="input image">
                                        <p class="imageload">
                                            <img id="loading" src="../Images/async.gif" class="loadingimg-becomepartner" />
                                            <asp:Image ID="companyLogoImage" runat="server" CssClass="pic imagestyle" />
                                        </p>
                                        <div class="form-input fileinputs">
                                            <input type="file" size="5px" id="companyLogoFileUpload" class="customInput customStyle"
                                                name="companyLogoImage1" onchange="return AjaxFileUpload('companyLogoFileUpload','image');" />
                                        </div>
                                        <div class="imgname">
                                            <asp:LinkButton ID="imageLinkButton" runat="server" ViewStateMode="Enabled" CausesValidation="false"></asp:LinkButton>
                                            <asp:LinkButton ID="removeImageLinkButton" OnClick="RemoveImageLinkButton_Click"
                                                runat="server" Style="display: none; margin-left: 15px" CausesValidation="false"
                                                Text="X" CBID="753"></asp:LinkButton>
                                        </div>
                                        <span id="imageErrSpan" class="errorText" style="display: none"></span>
                                        <input type="hidden" id="imageNameHiddenField" runat="server" value="" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="imageLinkButton" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="removeImageLinkButton" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="form-spec">
                            <asp:Label ID="companyLogoSizeDescription" runat="server" Text="Images should be JPG or PNG, resized to 235x235 pixels"
                                CBID="1121"></asp:Label></div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="webSiteLabel" runat="server" Text="WEB SITE" CBID="0"></asp:Label></div>
                            <div class="form-input textbox-one">
                                <asp:TextBox ID="webSiteTextBox" AutoComplete="off" runat="server" MaxLength="256"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="webSiteRegularExpressionValidator" VMTI="20" runat="server"
                                    ViewStateMode="Enabled" SetFocusOnError="true" ControlToValidate="webSiteTextBox"
                                    CssClass="errorText" ValidationGroup="ValidationGp2"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="companyAddressLabel" runat="server" CBID="302"></asp:Label><asp:Label
                                    ID="mandatoryCompanyAddressLabel" runat="server" Text=" *"></asp:Label></div>
                            <div class="form-input textbox-one">
                                <asp:TextBox ID="companyAddressLine1TextBox" AutoComplete="off" runat="server" MaxLength="256"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="companyAddressLine1RequiredFieldValidator" VMTI="13"
                                    ViewStateMode="Enabled" SetFocusOnError="true" runat="server" CssClass="errorText"
                                    ValidationGroup="ValidationGp2" ControlToValidate="companyAddressLine1TextBox"
                                    Display="Dynamic" ErrorMessage="Company Address is required"></asp:RequiredFieldValidator>
                                <div class="space">
                                    &nbsp;</div>
                                <asp:TextBox ID="companyAddressLine2TextBox" AutoComplete="off" runat="server" MaxLength="256"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="countryLabel" runat="server" CBID="344"></asp:Label><asp:Label ID="mandatoryCountryLabel"
                                    runat="server" Text=" *"></asp:Label></div>
                            <asp:UpdatePanel ID="dropDownUpdatePanel" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <div class="form-input textbox-one">
                                        <asp:DropDownList ID="countryDropDownList" runat="server" ViewStateMode="Enabled"
                                            AutoPostBack="true" OnSelectedIndexChanged="CountryDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="countryRequiredFieldValidator" SetFocusOnError="true"
                                            ViewStateMode="Enabled" runat="server" CssClass="errorText" ValidationGroup="ValidationGp2"
                                            VMTI="14" InitialValue="0" ControlToValidate="countryDropDownList" Display="Dynamic"
                                            ErrorMessage="Country is required"></asp:RequiredFieldValidator>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="cityLabel" runat="server" CBID="295"></asp:Label><asp:Label ID="mandatoryCityLabel"
                                    runat="server" Text=" *"></asp:Label></div>
                            <div class="form-input textbox-two">
                                <asp:TextBox ID="cityTextBox" AutoComplete="off" runat="server" MaxLength="128 "></asp:TextBox>
                                <asp:RequiredFieldValidator ID="cityRequiredFieldValidator" SetFocusOnError="true"
                                    ViewStateMode="Enabled" runat="server" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    ControlToValidate="cityTextBox" Display="Dynamic" ErrorMessage="City is required"
                                    VMTI="15"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="stateLabel" runat="server" CBID="687"></asp:Label><asp:Label ID="stateMandatoryLabel"
                                    runat="server" Text=" *"></asp:Label></div>
                            <asp:UpdatePanel ID="stateDropDownUpdatePanel" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <div class="form-input no-pad">
                                        <asp:DropDownList ID="stateDropDownList" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="stateRequiredFieldValidator" VMTI="16" SetFocusOnError="true"
                                            ViewStateMode="Enabled" runat="server" CssClass="errorText" ValidationGroup="ValidationGp2"
                                            InitialValue="0" ControlToValidate="stateDropDownList" Display="Dynamic" ErrorMessage="State is required"></asp:RequiredFieldValidator>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="countryDropDownList" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="zipCodeLabel" runat="server" CBID="762"></asp:Label><asp:Label ID="mandatoryZipCodeLabel"
                                    runat="server" Text=" *"></asp:Label></div>
                            <asp:UpdatePanel ID="zipCodeUpdatePanel" ViewStateMode="Enabled" UpdateMode="Conditional"
                                runat="server">
                                <ContentTemplate>
                                    <div class="form-input textbox-three">
                                        <asp:TextBox ID="zipCodeTextBox" AutoComplete="off" MaxLength="10" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="zipCodeRequiredFieldValidator" VMTI="17" runat="server"
                                            ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                            SetFocusOnError="true" ControlToValidate="zipCodeTextBox" Display="Dynamic" ErrorMessage="Zip Code is required"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="zipCodeRegularExpressionValidator" ViewStateMode="Enabled"
                                            VMTI="18" CssClass="errorText" ControlToValidate="zipCodeTextBox" ValidationGroup="ValidationGp2"
                                            SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="countryDropDownList" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="companyDescription" runat="server" CBID="308"></asp:Label><asp:Label
                                    ID="mandatoryCompanyDescriptionLabel" runat="server" Text=" *"></asp:Label></div>
                            <div class="form-input textbox-three">
                                <asp:TextBox ID="companyDescriptionTextBox" AutoComplete="off" runat="server" TextMode="MultiLine"
                                    MaxLength="2000" ClientIDMode="Static"></asp:TextBox><br />                                                                      
                                <asp:RequiredFieldValidator ID="companyDescriptionRequiredFieldValidator" VMTI="19"
                                    ViewStateMode="Enabled" runat="server" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="companyDescriptionTextBox" Display="Dynamic"
                                    ErrorMessage="Company Description is required"></asp:RequiredFieldValidator>
                            </div>                             
                        </div>
                         <div class="errorText becomPartnerDesc">                                    
                                        <asp:Label runat="server" ID="remainingCharacters" ClientIDMode="Static"></asp:Label>
                                        <asp:Label runat="server" ID="remainingCharacterLabel" Text="characters remaining"></asp:Label>                                        
                                    </div> 
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <h3>
                    <asp:Label ID="contactInformationLabel" runat="server" CBID="324"></asp:Label></h3>
                <div class="panel3">
                    <div class="form">
                        <h6>
                            <asp:Label ID="primaryContactLabel" runat="server" CBID="518"></asp:Label></h6>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="firstNamePrimaryContactLabel" CBID="992" runat="server" Text=""></asp:Label>
                                *
                            </div>
                            <div class="form-input textbox-one">
                                <asp:HiddenField ID="primaryContactIdHiddenField" runat="server" />
                                <asp:TextBox runat="server" AutoComplete="off" ID="firstNamePrimaryContactTextBox"
                                    MaxLength="128"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="firstNameRequiredFieldValidator" VMTI="1" runat="server"
                                    ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="firstNamePrimaryContactTextBox" Display="Dynamic"
                                    ErrorMessage="First Name is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="firstNameRegularExpressionValidator" VMTI="2"
                                    ViewStateMode="Enabled" runat="server" ControlToValidate="firstNamePrimaryContactTextBox"
                                    ErrorMessage="Please enter valid name" CssClass="errorText" SetFocusOnError="true"
                                    ValidationGroup="ValidationGp2" Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="lastNamePrimaryContactLabel" CBID="110" runat="server"></asp:Label>
                                *</div>
                            <div class="form-input textbox-one">
                                <asp:TextBox runat="server" AutoComplete="off" ID="lastNamePrimaryContactTextBox"
                                    MaxLength="128"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="lastNameRequiredFieldValidator" VMTI="3" runat="server"
                                    ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="lastNamePrimaryContactTextBox" Display="Dynamic"
                                    ErrorMessage="Last Name is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="lastNameRegularExpressionValidator" VMTI="4"
                                    ViewStateMode="Enabled" runat="server" ControlToValidate="lastNamePrimaryContactTextBox"
                                    ErrorMessage="Please enter valid name" SetFocusOnError="true" CssClass="errorText"
                                    ValidationGroup="ValidationGp2" Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="titlePrimaryContactLabel" runat="server" CBID="709"></asp:Label></div>
                            <div class="form-input textbox-one">
                                <asp:TextBox runat="server" AutoComplete="off" ID="titlePrimaryContactTextBox" MaxLength="256"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="titlePrimaryRegularExpressionValidator" VMTI="5"
                                    ViewStateMode="Enabled" runat="server" ControlToValidate="titlePrimaryContactTextBox"
                                    ErrorMessage="Please enter valid title" SetFocusOnError="true" CssClass="errorText"
                                    ValidationGroup="ValidationGp2" Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="phonePrimaryContactLabel" runat="server" CBID="123"></asp:Label>
                                *</div>
                            <div class="form-input textbox-four">
                                <asp:TextBox runat="server" AutoComplete="off" MaxLength="32" ID="phonePrimaryContactTextBox"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="phoneRequiredFieldValidator" VMTI="6" runat="server"
                                    ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="phonePrimaryContactTextBox" Display="Dynamic"
                                    ErrorMessage="Phone is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="phonePrimaryContactRegularExpressionValidator"
                                    VMTI="7" CssClass="errorText" ViewStateMode="Enabled" ControlToValidate="phonePrimaryContactTextBox"
                                    ValidationGroup="ValidationGp2" SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="faxPrimaryContactLabel" runat="server" CBID="140"></asp:Label>
                                *</div>
                            <div class="form-input textbox-four">
                                <asp:TextBox runat="server" AutoComplete="off" MaxLength="32" ID="faxPrimaryContactTextBox"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="faxRequiredFieldValidator" VMTI="8" runat="server"
                                    ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="faxPrimaryContactTextBox" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="faxPrimaryRegularExpressionValidator" SetFocusOnError="true"
                                    ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="faxPrimaryContactTextBox"
                                    ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="emailAddressPrimaryContactLabel" runat="server" CBID="376"></asp:Label>
                                *</div>
                            <div class="form-input textbox-one">
                                <asp:TextBox runat="server" AutoComplete="off" ID="emailAddressPrimaryContactTextBox"
                                    MaxLength="128"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="emailRequiredFieldValidator" VMTI="10" runat="server"
                                    ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="emailAddressPrimaryContactTextBox"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" VMTI="11" runat="server"
                                    ViewStateMode="Enabled" SetFocusOnError="true" ControlToValidate="emailAddressPrimaryContactTextBox"
                                    CssClass="errorText" ValidationGroup="ValidationGp2"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-border">
                        </div>
                        <h6>
                            <asp:Label ID="secondaryContactLabel" Text="Secondary Contact" CBID="993" runat="server"></asp:Label>
                        </h6>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="firstNameSecondaryContactLabel" runat="server" Text="" CBID="992"></asp:Label>
                                *</div>
                            <div class="form-input textbox-one">
                                <asp:HiddenField ID="secondaryContactIdHiddenField" runat="server" />
                                <asp:TextBox runat="server" AutoComplete="off" ID="firstNameSecondaryContactTextBox"
                                    MaxLength="128"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="firstNameSecondaryRequiredFieldValidator" VMTI="1"
                                    ViewStateMode="Enabled" runat="server" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="firstNameSecondaryContactTextBox" Display="Dynamic"
                                    ErrorMessage="First Name is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="firstNameSecContactRegularExpressionValidator"
                                    ViewStateMode="Enabled" VMTI="2" runat="server" ControlToValidate="firstNameSecondaryContactTextBox"
                                    ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="lastNameSecondaryContactLabel" runat="server" CBID="110"></asp:Label>
                                *</div>
                            <div class="form-input textbox-one">
                                <asp:TextBox runat="server" AutoComplete="off" ID="lastNameSecondaryContactTextBox"
                                    MaxLength="128"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="lastNameSecondaryRequiredFieldValidator" VMTI="3"
                                    ViewStateMode="Enabled" runat="server" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="lastNameSecondaryContactTextBox" Display="Dynamic"
                                    ErrorMessage="Last Name is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="lastNameSecContactRegularExpressionValidator"
                                    ViewStateMode="Enabled" VMTI="4" runat="server" ControlToValidate="lastNameSecondaryContactTextBox"
                                    ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="titleSecondaryContactLabel" runat="server" CBID="709"></asp:Label></div>
                            <div class="form-input textbox-one">
                                <asp:TextBox runat="server" AutoComplete="off" ID="titleSecondaryContactTextBox"
                                    MaxLength="128"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="titleSecondaryContactRegularExpressionValidator"
                                    ViewStateMode="Enabled" runat="server" ControlToValidate="titleSecondaryContactTextBox"
                                    VMTI="5" ErrorMessage="Please enter valid title" SetFocusOnError="true" CssClass="errorText"
                                    ValidationGroup="ValidationGp2" Display="Dynamic"></asp:RegularExpressionValidator></div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="phoneSecondaryContactLabel" runat="server" CBID="123"></asp:Label>
                                *</div>
                            <div class="form-input textbox-four">
                                <asp:TextBox runat="server" AutoComplete="off" MaxLength="32" ID="phoneSecondaryContactTextBox"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="phoneSecondaryRequiredFieldValidator" VMTI="6" runat="server"
                                    ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="phoneSecondaryContactTextBox" Display="Dynamic"
                                    ErrorMessage="Phone is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="phoneSecondaryRegularExpressionValidator" VMTI="7"
                                    ViewStateMode="Enabled" CssClass="errorText" ControlToValidate="phoneSecondaryContactTextBox"
                                    ValidationGroup="ValidationGp2" SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="faxSecondaryContactLabel" runat="server" CBID="140"></asp:Label>
                                *</div>
                            <div class="form-input textbox-four">
                                <asp:TextBox runat="server" AutoComplete="off" MaxLength="32" ID="faxSecondaryContactTextBox"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="faxSecondaryRequiredFieldValidator" VMTI="8" runat="server"
                                    ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="faxSecondaryContactTextBox" Display="Dynamic"
                                    ErrorMessage="Fax is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="faxSecondaryRegularExpressionValidator" SetFocusOnError="true"
                                    ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="faxSecondaryContactTextBox"
                                    ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="form-label">
                                <asp:Label ID="emailAddressSecondaryContactLabel" runat="server" CBID="376"></asp:Label>
                                *</div>
                            <div class="form-input textbox-one">
                                <asp:TextBox runat="server" AutoComplete="off" ID="emailAddressSecondaryContactTextBox"
                                    MaxLength="128"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="emailSecondaryRequiredFieldValidator" VMTI="10" runat="server"
                                    ViewStateMode="Enabled" CssClass="errorText" ValidationGroup="ValidationGp2"
                                    SetFocusOnError="true" ControlToValidate="emailAddressSecondaryContactTextBox"
                                    Display="Dynamic" ErrorMessage="Email is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="true" VMTI="11" ID="secondaryEmailRegularExpressionValidator"
                                    ViewStateMode="Enabled" runat="server" ErrorMessage="Correct the format" ControlToValidate="emailAddressSecondaryContactTextBox"
                                    CssClass="errorText" ValidationGroup="ValidationGp2"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-border">
                        </div>
                        <asp:PlaceHolder ID="placeHolder1" runat="server"></asp:PlaceHolder>
                        <asp:UpdatePanel ID="regionalContactUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Repeater ID="regionContactRepeater" ViewStateMode="Enabled" OnItemDataBound="RegionContactRepeater_ItemDataBound"
                                    runat="server">
                                    <ItemTemplate>
                                        <h6>
                                            <asp:Label ID="regionNameLabel" Text='<%# DataBinder.Eval(Container.DataItem, "RegionName")%>'
                                              Visible="false"   runat="server"></asp:Label>
                                            <asp:Label ID="regionIdLabel" Text='<%# DataBinder.Eval(Container.DataItem, "RegionId")%>'
                                                Visible="false" runat="server"></asp:Label>
                                                  <asp:Label ID="countryNameLabel" Text='<%# DataBinder.Eval(Container.DataItem, "CountryName")%>'
                                                   runat="server"></asp:Label>
                                            <asp:Label ID="countryIdLabel" Text='<%# DataBinder.Eval(Container.DataItem, "CountryId")%>'
                                                Visible="false" runat="server"></asp:Label>
                                        </h6>
                                        <p>
                                            <asp:CheckBox ID="sameAsPrimaryCheckBox" AutoPostBack="true" ViewStateMode="Enabled"
                                                OnCheckedChanged="SameAsPrimaryCheckBox_CheckedChanged" Text="Same as Primary Contact"
                                                CBID="634" runat="server" />
                                            <div class="form-field">
                                                <div class="form-label">
                                                    <asp:HiddenField ID="regionalContactIdHiddenField" runat="server" />
                                                    <asp:Label ID="firstNameRegionalContactLabel" CBID="992" Text="" runat="server"></asp:Label></div>
                                                <div class="form-input textbox-one">
                                                    <asp:TextBox ID="firstNameRegionalContactTextBox" MaxLength="128" AutoComplete="off"
                                                        Text='<%# DataBinder.Eval(Container.DataItem, "FirstName")%>' runat="server"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="firstNameRegContactRegularExpressionValidator"
                                                        ViewStateMode="Enabled" VMTI="2" runat="server" ControlToValidate="firstNameRegionalContactTextBox"
                                                        ErrorMessage="Please enter valid name" SetFocusOnError="true" CssClass="errorText"
                                                        ValidationGroup="ValidationGp2" Display="Dynamic"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="form-field">
                                                <div class="form-label">
                                                    <asp:Label ID="lastNameRegionalContactLabel" Text="" CBID="110" runat="server"></asp:Label></div>
                                                <div class="form-input textbox-one">
                                                    <asp:TextBox ID="lastNameRegionalContactTextBox" MaxLength="128" AutoComplete="off"
                                                        runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LastName")%>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="lastNameRegContactRegularExpressionValidator"
                                                        ViewStateMode="Enabled" VMTI="4" runat="server" ControlToValidate="lastNameRegionalContactTextBox"
                                                        ErrorMessage="Please enter valid name" SetFocusOnError="true" CssClass="errorText"
                                                        ValidationGroup="ValidationGp2" Display="Dynamic"></asp:RegularExpressionValidator></div>
                                            </div>
                                            <div class="form-field">
                                                <div class="form-label">
                                                    <asp:Label ID="titleRegionalContactLabel" CBID="709" Text="" runat="server"></asp:Label></div>
                                                <div class="form-input textbox-one">
                                                    <asp:TextBox ID="titleRegionalContactTextBox" AutoComplete="off" MaxLength="128"
                                                        runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="titleRegContactRegularExpressionValidator" runat="server"
                                                        ViewStateMode="Enabled" ControlToValidate="titleRegionalContactTextBox" VMTI="5"
                                                        ErrorMessage="Please enter valid title" SetFocusOnError="true" CssClass="errorText"
                                                        ValidationGroup="ValidationGp2" Display="Dynamic"></asp:RegularExpressionValidator></div>
                                            </div>
                                            <div class="form-field">
                                                <div class="form-label">
                                                    <asp:Label ID="phoneRegionalContactLabel" Text="" CBID="123" runat="server"></asp:Label></div>
                                                <div class="form-input textbox-four">
                                                    <asp:TextBox ID="phoneRegionalContactTextBox" AutoComplete="off" MaxLength="32" runat="server"
                                                        Text='<%# DataBinder.Eval(Container.DataItem, "Phone")%>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="phoneRegionalRegularExpressionValidator" VMTI="7"
                                                        CssClass="errorText" ViewStateMode="Enabled" ControlToValidate="phoneRegionalContactTextBox"
                                                        ValidationGroup="ValidationGp2" SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="form-field">
                                                <div class="form-label">
                                                    <asp:Label ID="faxRegionalContactLabel" CBID="140" Text="" runat="server"></asp:Label></div>
                                                <div class="form-input textbox-four">
                                                    <asp:TextBox runat="server" MaxLength="32" AutoComplete="off" ID="faxRegionalContactTextBox"
                                                        Text='<%# DataBinder.Eval(Container.DataItem, "Fax")%>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="faxRegionalRegularExpressionValidator" SetFocusOnError="true"
                                                        ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="faxRegionalContactTextBox"
                                                        ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="ValidationGp2"
                                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="form-field">
                                                <div class="form-label">
                                                    <asp:Label ID="emailRegionalContactLabel" Text="" CBID="376" runat="server"></asp:Label></div>
                                                <div class="form-input textbox-one">
                                                    <asp:TextBox ID="emailRegionalContactTextBox" AutoComplete="off" MaxLength="128"
                                                        runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Email")%>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regionalEmailRegularExpressionValidator" runat="server"
                                                        ViewStateMode="Enabled" ErrorMessage="Correct the format" VMTI="11" ControlToValidate="emailRegionalContactTextBox"
                                                        SetFocusOnError="true" ValidationGroup="ValidationGp2"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="form-border">
                                            </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="box1">
                <div class="go">
                    <asp:Label ID="nextStepLabel" runat="server" CBID="458"></asp:Label>
                    <asp:Button ID="continueButton" CBID="338" runat="server" ValidationGroup="ValidationGp2"
                        Text="CONTINUE" OnClick="ContinueApplicationData" />
                </div>
                <div class="clear">
                    &nbsp;</div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="Recommended Partners" CBID="586" runat="server"></asp:Label></h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="browsePartnersHyperLink" runat="server" CBID="32" Text="Browse Partners"
                            NavigateUrl="~/Partners/RecommendedPartners.aspx"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="becomePartnerHyperLink" class="active" CBID="84" Text="Become a Partner"
                            runat="server"></asp:HyperLink>
                        <ul>
                            <li>
                                <asp:HyperLink ID="chooseCategoriesHyperLink" runat="server" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"
                                    CBID="994" Text="Step 1: Choose Categories"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="completeApplicationHyperLink" runat="server" class="active" NavigateUrl="~/Partners/ApplicationDetails.aspx"
                                    CBID="995" Text="Step 2: Complete Application"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="reviewApplicationHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/ReviewApplication.aspx"
                                    CBID="996" Text="Step 3: Review Application"></asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="thankYouHyperLink" Enabled="false" runat="server" NavigateUrl="~/Partners/ThankYouNextSteps.aspx"
                                    CBID="997" Text="Step 4: Thank You"></asp:HyperLink>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;</div>
</asp:Content>
