﻿using System;
using System.Globalization;
using System.Web.UI;

using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI
{
    public partial class SubmitFeedback : PageBase
    {
        private IFeedbackManager _feedbackManager;
        private User currentUser;

        /// <summary>
        /// Invoked on page initialization
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _feedbackManager = FeedbackManager.Instance;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            currentUser = (User)GetSession<User>(SessionConstants.User);

            try
            {
                if (!Page.IsPostBack)
                {
                    PopulateDropDownData();
                    string companyName = Server.HtmlDecode(Request.QueryString["partnerName"]);
                    string productName = Request.QueryString["productName"];
                    string skuNumber = Request.QueryString["skuNumber"];
                    partnerIdFromQueryStringHidden.Value = Request.QueryString["partnerId"];
                    if (Request.QueryString["productId"] != null)
                    {
                        productIdFromQueryStringHidden.Value = Request.QueryString["productId"];
                    }
                    else
                    {
                        productIdFromQueryStringHidden.Value = "0";
                    }

                    informationUserNameLabel.Text = currentUser.FirstName + " " + currentUser.LastName;
                    feedbackEmailLabel.Text = currentUser.Email;
                    partnerNameLabel.Text = companyName;
                    productNameLabel.Text = productName;
                    toFirstNameHiddenValue.Value = Request.QueryString["toFirstName"];
                    toLastNameHiddenValue.Value = Request.QueryString["toLastName"];
                    toEmailHiddenValue.Value = Request.QueryString["toEmail"];
                }
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Populate dropdown data
        /// </summary>
        private void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.RatingDropDown, ratingDropDown, null, (int)FeedbackRatingTypeEnum.Neutral);
        }

        /// <summary>
        /// Send Email Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Send_Click(object sender, EventArgs e)
        {
            try
            {
                Feedback feedback = new Feedback();
                feedback.PartnerId = Convert.ToInt32(partnerIdFromQueryStringHidden.Value, CultureInfo.InvariantCulture);
                feedback.ProductId = Convert.ToInt32(productIdFromQueryStringHidden.Value, CultureInfo.InvariantCulture);
                feedback.SenderId = currentUser.UserId;
                feedback.SenderHiltonId = currentUser.HiltonUserNumber;
                feedback.SenderFirstName = currentUser.FirstName;
                feedback.SenderLastName = currentUser.LastName;
                feedback.SenderEmail = currentUser.Email;
                feedback.ToEmail = toEmailHiddenValue.Value;
                feedback.ToFirstName = toFirstNameHiddenValue.Value;
                feedback.ToLastName = toLastNameHiddenValue.Value;
                feedback.Details = descriptionTextArea.Text; //commentsTextBox.Text;
                feedback.RatingId = Convert.ToInt32(ratingDropDown.SelectedValue, CultureInfo.InvariantCulture);
                feedback.FeedbackStatusId = (int)FeedbackStatusEnum.New;
                feedback.TemplateId = 6;
                feedback.culture = Convert.ToString(culture, CultureInfo.InvariantCulture);
                if (_feedbackManager.AddFeedback(feedback))
                {
                    Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "showConfirmationMessage", "ShowConfirm()", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}