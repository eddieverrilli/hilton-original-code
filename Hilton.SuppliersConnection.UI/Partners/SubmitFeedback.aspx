﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="SubmitFeedback.aspx.cs"
    EnableViewState="true" ViewStateMode="Disabled" Inherits="Hilton.SuppliersConnection.UI.SubmitFeedback" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../Styles/modal.css" />
    <title></title>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        $(window).ready(function () {
            
            $('.cancel').click(function () {
                window.parent.closeIframe();
                window.parent.CallParent();
                return false;

            });
        })

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }

        function ShowConfirm() {
            $('#email-partner').hide();
            $('#thankyou-email').show();
        }

        $(document).ready(function () {
            $('textarea#descriptionTextArea').limiter(2000, $('#remainingCharacters'));
        });


        function RestrictToSendMaxLimit() {
            var maxWords = 2000;
            var control = document.getElementById('descriptionTextArea');
            var input = '#descriptionTextArea';
            var resultArray = $(input).val().length;
            if (resultArray.length > maxWords)
                return false;
            if ($(input).val().length > 2000) {
                //alert("You cannot send a message with more than 2000 characters.");
                $('#MsgCharLimit').html("Message limited to 2000 characters or less.");
                $(input).focus(); 
                return false;
            }

        }

        (function ($) {
            $.fn.extend({
                limiter: function (limit, elem) {
                    $(this).on("keyup focus keydown ", function () {
                        setCount(this, elem);
                    });
                    function setCount(src, elem) {
                        var chars = src.value.length;
                        if (chars > limit) {
                            src.value = src.value.substr(0, limit);
                            chars = limit;
                        }
                        if (chars == 2000)
                            $('#MsgCharLimit').html("Message limited to 2000 characters or less.");
                        else
                            $('#MsgCharLimit').empty();
                        elem.html(limit - chars);
                    }
                    setCount($(this)[0], elem);
                }
            });
        })(jQuery);

    </script>
    <div id="resultMessageDiv" runat="server">
    </div>
    <div id="email-partner" class="modal">
        <h2>
            <asp:Label ID="submitFeedbackLabel" runat="server" CBID="441" Text="Submit Feedback"></asp:Label></h2>
        <div class="content">
            <div class="box2">
                <h4>
                    <asp:Label ID="yourInformationLabel" runat="server" CBID="759" Text="YOUR INFORMATION"></asp:Label></h4>
                <div class="content">
                    <h5>
                        <asp:Label ID="informationUserNameLabel" runat="server"></asp:Label></h5>
                    <p class="email">
                        <asp:Label ID="feedbackEmailLabel" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
            <h3>
                <asp:Label ID="sendMessageLabel" runat="server" CBID="998" Text="Please Provide feedback On:"></asp:Label></h3>
            <p>
                <asp:Label ID="partnerNameLabel" runat="server"></asp:Label>
                <asp:Label ID="productNameLabel" runat="server"></asp:Label>
                <asp:Label ID="skuNumberLabel" runat="server"></asp:Label>
            </p>
            <div>
                <asp:Label ID="ratingLabel" runat="server" CBID="582"></asp:Label></div>
            <asp:DropDownList ID="ratingDropDown" ViewStateMode="Enabled" runat="server">
            </asp:DropDownList>
            <asp:Label ID="partnerEmail" runat="server"></asp:Label>
            <div class="comment">
                <h3>
                    <asp:Label ID="enterCommentsLabel" runat="server" CBID="298" Text="comments: (Limited to 100 words or less)" />
                </h3>
                  <div class="input textarea">
                                <asp:TextBox ID="descriptionTextArea" runat="server" TextMode="MultiLine" ClientIDMode="Static" Text="Enter Your Feedback" 
                                           onfocus="if(this.value==this.defaultValue)this.value=''"   >
                                </asp:TextBox>
                                  <asp:RequiredFieldValidator ID="feedbackDetailsRequiredFieldValidator" VMTI="0" runat="server"
                                InitialValue="Enter Your Feedback" CssClass="errorText" ValidationGroup="ValidationGp2"
                                ErrorMessage="Please change default text" ForeColor="Red" ControlToValidate="descriptionTextArea"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                                 <asp:RequiredFieldValidator ID="feedbackDetailsEmptyTextRequiredFieldValidator" VMTI="0"
                                runat="server" SetFocusOnError="true" CssClass="errorText" ValidationGroup="ValidationGp2"
                                ErrorMessage="Feedback Detail is required" ForeColor="Red" ControlToValidate="descriptionTextArea"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                                <div class="errorTxtSubfeedBck">
                                    <asp:Label runat="server" ID="remainingCharacterLabel" Text="Characters left: "></asp:Label>
                                    <asp:Label runat="server" ID="remainingCharacters" ClientIDMode="Static"></asp:Label>
                               </div>
                                <div class="errorTxtSubfeedBck">
                                <asp:Label runat="server" ID="MsgCharLimit"  ClientIDMode="Static" ></asp:Label>
                                </div>
                             
                   </div>
                    
                <div id="maxWordLimitMessage" style="display: none;" runat="server">
                    <asp:Label ID="maxWordErrorMessageLabel" runat="server" CssClass="errorText" ForeColor="Red"
                        Text="Message Limited to 100 words or less"></asp:Label>
                </div>
                <div class="form-btn">
                    <p>
                        <asp:Label ID="allMessagesLoggedLabel" runat="server" Text="All messages are logged."
                            CBID="999"></asp:Label></p>
                    <asp:HyperLink ID="cancelLink" runat="server" CBID="273" CssClass="cancel"></asp:HyperLink>
                    <asp:LinkButton ID="sendLink" ValidationGroup="ValidationGp2" ViewStateMode="Enabled"
                        runat="server" CBID="663" OnClientClick="return RestrictToSendMaxLimit()" OnClick="Send_Click"></asp:LinkButton>
                </div>
                <div class="word-count">
                    <%--<div class="word">
                        <asp:Label ID="wordCountLabel" runat="server" CBID="746"></asp:Label>
                        <div id="commentsTextBox_count" style="float: right; display: none">
                        </div>
                        &nbsp;
                        <asp:Label ID="wordCount" class="word_count" runat="server"></asp:Label>
                    </div>--%>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="thankyou-email" class="modal">
        <h2>
            <asp:Label ID="feedbackConfirmationLabel" runat="server" CBID="442" Text="Feedback Received"></asp:Label></h2>
        <div class="content">
            <div class="comment">
                <h3>
                    <asp:Label ID="thankYouLabel" runat="server" Text="Thank You!" CBID="1001"></asp:Label></h3>
                <p>
                    <asp:Label ID="feedbackReceivedLabel" runat="server" Text="Your feedback has been received."
                        CBId="1000"></asp:Label></p>
                <div class="form-btn">
                    <asp:HyperLink ID="closeHyperLink" runat="server" CBID="78" CssClass="cancel"></asp:HyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="partnerIdFromQueryStringHidden" runat="server" />
    <asp:HiddenField ID="productIdFromQueryStringHidden" runat="server" />
    <asp:HiddenField ID="toFirstNameHiddenValue" runat="server" />
    <asp:HiddenField ID="toLastNameHiddenValue" runat="server" />
    <asp:HiddenField ID="toEmailHiddenValue" runat="server" />
    </form>
</body>
</html>