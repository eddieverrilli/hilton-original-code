﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="EmailAlert.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.EmailAlert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../Styles/modal.css" />
    <title></title>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="addCategoryScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        $(window).ready(function () {
            $('.cancel').click(function () {
                window.parent.closeIframe();
                return false;

            });
        })

        function Close() {
            window.parent.closeIframe();
            return false;
        }

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }

        function ShowConfirm() {
            $('#email-partner').hide();
            $('#thankyou-email').show();

        }

        $(document).ready(function () {

            $('.word_count').each(function () {
                var input = '#' + this.id;
                var count = input + '_count';
                $(count).show();
                Word_count(input, count);
                $(this).keyup(function () { Word_count(input, count) });
                $(this).focus(function () { Word_count(input, count) });

            });

        });

        function Word_count(field, count) {

            var number = 0;
            var matches = $(field).val().match(/\b/g);
            if (matches) {
                number = matches.length / 2;
            }
            $(count).text(number);
        }

        var maxwords = 10;

        function check_length(obj) {

            var ary = obj.value.split(" ");
            var len = ary.length;
            if (len > maxwords) {
                ary = ary.slice(0, maxwords - 1);
                obj.value = ary.join(" ");
                return false;
            }
            return true;
        }

        function LimitWords(id) {
            var maxWords = 100;
            var control = document.getElementById(id);
            var input = '#' + id;
            var resultArray = jQuery.trim($(input).val()).split(/\s+/);
            if (resultArray.length > maxWords) {
                maxWordLimitMessage.style.display = 'block';
                return false;
            }
            else {
                maxWordLimitMessage.style.display = 'none';
            }
        }

        function RestrictToSendMaxLimit() {
            var maxWords = 100;
            var control = document.getElementById('commentsTextBox');
            var input = '#commentsTextBox';
            var resultArray = jQuery.trim($(input).val()).split(/\s+/);
            if (resultArray.length > maxWords)
                return false;
        }

        function TrimWords(id) {
            //debugger;
            var maxWords = 100;
            var control = document.getElementById(id);
            var resultArray = control.value.split(' ');
            if (resultArray.length > maxWords) {
                resultArray = resultArray.slice(0, maxWords);
                // textArea = control.value.substring(0, control.value.lastIndexOf(' '));
                control.value = resultArray;
            }
        }

    </script>
    <div id="resultMessageDiv" runat="server">
    </div>
    <div id="email-partner" class="modal">
        <h2>
            <asp:Label ID="emailPartnerLabel" runat="server" CBID="367" Text="Email Partner"></asp:Label></h2>
        <div class="content">
            <div class="box2">
                <h4>
                    <asp:Label ID="yourInformationLabel" runat="server" CBID="760" Text="YOUR INFORMATION"></asp:Label></h4>
                <div class="content">
                    <h5>
                        <asp:Label ID="informationUserNameLabel" runat="server"></asp:Label></h5>
                    <p class="email">
                        <asp:LinkButton ID="feedbackEmailLinkButton" runat="server"></asp:LinkButton></p>
                    <div class="copy">
                        <asp:CheckBox ID="sendCopyToYouCheckBox" runat="server" Text="  Send a copy to yourself" />
                    </div>
                </div>
            </div>
            <h3>
                <asp:Label ID="sendMessageLabel" runat="server" Text="Send message to vendor contact for:"
                    CBID="1067"></asp:Label></h3>
            <p>
                <asp:Label ID="contactRepresentativeLabel" runat="server" CBID="605" Text=":"></asp:Label></p>
            <asp:UpdatePanel ID="contactUpdatePanel" runat="server" UpdateMode="Conditional"
                class="contactdetails">
                <ContentTemplate>
                    <asp:DropDownList ID="contactDropDownList" OnSelectedIndexChanged="DropDownChange"
                        AutoPostBack="true" runat="server">
                    </asp:DropDownList>
                    <asp:Label ID="contactEmailLabel" runat="server"></asp:Label>
                    <asp:HiddenField ID="regionIdHiddenValue" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="comment">
                <h3>
                    <asp:Label ID="enterCommentsLabel" runat="server" CBID="1070" Text="message: (Limited to 100 words or less)" />
                </h3>
                <p>
                    <asp:TextBox ID="commentsTextBox" runat="server" onblur="LimitWords(this.id)" onkeypress="LimitWords(this.id)"
                        onfocus="if(this.value==this.defaultValue)this.value=''" class="word_count" TextMode="MultiLine"
                        Text="Enter Your Message"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="messageDetailsRequiredFieldValidator" VMTI="0" runat="server"
                        InitialValue="Enter Your Message" CssClass="errorText" ValidationGroup="ValidationGp2"
                        ErrorMessage="Please change default text" ForeColor="Red" ControlToValidate="commentsTextBox"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="messageDetailsEmptyTextRequiredFieldValidator" VMTI="0"
                        runat="server" SetFocusOnError="true" CssClass="errorText" ValidationGroup="ValidationGp2"
                        ErrorMessage="Message Detail is required" ForeColor="Red" ControlToValidate="commentsTextBox"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </p>
                <div id="maxWordLimitMessage" style="display: none;" runat="server">
                    <asp:Label ID="maxWordErrorMessageLabel" runat="server" CssClass="errorText" ForeColor="Red"
                        Text="Message Limited to 100 words or less"></asp:Label>
                </div>
                <div class="form-btn">
                    <p>
                        <asp:Label ID="allMessagesAreLoggedLabel" runat="server" CBID="438"></asp:Label></p>
                    <asp:HyperLink ID="cancelEmail" runat="server" CBID="268" CssClass="cancel">CANCEL</asp:HyperLink>
                    <asp:LinkButton ID="sendEmail" ValidationGroup="ValidationGp2" CBID="661" runat="server"
                        OnClientClick="return RestrictToSendMaxLimit()" OnClick="Send_Click">SEND</asp:LinkButton>
                </div>
                <div class="word-count">
                    <div class="word">
                        <asp:Label ID="wordCountLabel" runat="server" CBID="747"></asp:Label>
                        <div id="commentsTextBox_count" style="float: right; display: none">
                        </div>
                        &nbsp;
                        <asp:Label ID="wordCount" class="word_count" runat="server"></asp:Label>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="thankyou-email" class="modal">
        <h2>
            <asp:Label ID="emailVendorLabel" runat="server" CBID="316" Text="Confirmation"></asp:Label></h2>
        <div class="content">
            <div class="comment">
                <p>
                    <asp:Label ID="sentMessageLiteral" Text="Your Message has been submitted." CBID="34"
                        runat="server"></asp:Label>
                </p>
                <div class="form-btn">
                    <asp:HyperLink ID="closeHyperLink" runat="server" CBID="1079" CssClass="cancel">CLOSE</asp:HyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="contactIdHiddenValue" runat="server" />
    <asp:HiddenField ID="contactTypeIdHiddenValue" runat="server" />
    <asp:HiddenField ID="partnerIdFromQueryStringHidden" runat="server" />
    <asp:HiddenField ID="firstNameHiddenValue" runat="server" />
    <asp:HiddenField ID="lastNameHiddenValue" runat="server" />
    </form>
</body>
</html>