﻿using System;
using System.Globalization;
using System.Threading;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Hilton.SuppliersConnection.UI
{
    public partial class PaymentResponse : PageBase
    {
        private IPartnerManager _partnerManager;

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _partnerManager = PartnerManager.Instance;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //For GET and Get Redirect
                //string transactionStatus = Request.QueryString["ssl_result_message"];
                //string transactionId = Request.QueryString["ssl_txn_id"];
                //string transactionApprovalCode = Request.QueryString["ssl_approval_code"];
                //string errorCode = Request.QueryString["errorCode"];
                //string errorName = Request.QueryString["errorName"];
                //string errorMessage = Request.QueryString["errorMsg"];
                //string amount = Request.QueryString["ssl_amount"];


                string result = string.Empty, transactionStatus = string.Empty, transactionId = string.Empty, transactionApprovalCode = string.Empty, errorCode = string.Empty, errorName = string.Empty, errorMessage = string.Empty, amount = string.Empty, cardNumber = string.Empty;

                errorCode = Request.QueryString["errorCode"];
                errorName = Request.QueryString["errorName"];
                errorMessage = Request.QueryString["errorMsg"];

                //used for logging in ErrLogging table for missing/Duplicate transactions
                string ErrorCodeRequestQueryStr = errorCode, ErrorNameRequestQueryStr = errorName, ErrorMessageRequestQueryStr = errorMessage, cardNumberErrLog = string.Empty;

                if (!string.IsNullOrWhiteSpace(errorCode))
                {
                    transactionStatus = "error";
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLResultMessage] != null)
                {
                    transactionStatus = Request.QueryString[PaymentGatewayConstants.SSLResultMessage].ToString(CultureInfo.InvariantCulture);
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLTransactionId] != null)
                {
                    transactionId = Request.QueryString[PaymentGatewayConstants.SSLTransactionId].ToString();
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLApprovalCode] != null)
                {
                    transactionApprovalCode = Request.QueryString[PaymentGatewayConstants.SSLApprovalCode].ToString(CultureInfo.InvariantCulture);
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLErrorCode] != null)
                {
                    errorCode = Request.QueryString[PaymentGatewayConstants.SSLErrorCode].ToString(CultureInfo.InvariantCulture);
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLErrorName] != null)
                {
                    errorName = Request.QueryString[PaymentGatewayConstants.SSLErrorName].ToString(CultureInfo.InvariantCulture);
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLErrorMessage] != null)
                {
                    errorMessage = Request.QueryString[PaymentGatewayConstants.SSLErrorMessage].ToString(CultureInfo.InvariantCulture);
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLAmount] != null)
                {
                    amount = Request.QueryString[PaymentGatewayConstants.SSLAmount].ToString(CultureInfo.InvariantCulture);
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLCardNumber] != null)
                {
                    // Do not store trimmed credit card number in local SC DB
                    //cardNumber = Request.QueryString[PaymentGatewayConstants.SSLCardNumber].ToString(CultureInfo.InvariantCulture);
                    cardNumber = string.Empty;
                }

                if (Request.QueryString[PaymentGatewayConstants.SSLResult] != null)
                {
                    result = Request.QueryString[PaymentGatewayConstants.SSLResult].ToString(CultureInfo.InvariantCulture);
                }

                DateTime transactionTime;
                PaymentGatewayResponse paymentGatewayResponse = new PaymentGatewayResponse();
                if (!string.IsNullOrEmpty(transactionStatus))
                {
                    paymentGatewayResponse.TrasactionStatus = transactionStatus;
                }

                if (!string.IsNullOrEmpty(amount))
                {
                    paymentGatewayResponse.Amount = amount;
                }

                if (transactionStatus == PaymentGatewayConstants.Approval && !string.IsNullOrEmpty(transactionApprovalCode))
                {
                    paymentGatewayResponse.TrasactionApprovalCode = transactionApprovalCode;
                }

                if (!string.IsNullOrEmpty(transactionId))
                {
                    paymentGatewayResponse.TransactionId = transactionId;
                }

                if (!string.IsNullOrEmpty(errorCode))
                {
                    paymentGatewayResponse.ErrorCode = errorCode;
                }

                if (!string.IsNullOrEmpty(errorName))
                {
                    paymentGatewayResponse.ErrorName = errorName;
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    paymentGatewayResponse.ErrorMessage = errorMessage;
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    paymentGatewayResponse.ErrorMessage = errorMessage;
                }

                DateTime? transactionDatetime = null;
                if (Request.QueryString[PaymentGatewayConstants.SSLTransactionTime] != null && (DateTime.TryParse(Request.QueryString[PaymentGatewayConstants.SSLTransactionTime].ToString(CultureInfo.InvariantCulture), out transactionTime)))
                //   if (DateTime.TryParse(Request.QueryString["ssl_txn_time"], out transactionTime))
                {
                    paymentGatewayResponse.TransactionDatetime = transactionTime;
                    transactionDatetime = transactionTime;
                }

                SetSession<PaymentGatewayResponse>(SessionConstants.PaymentGatewayResponse, paymentGatewayResponse);

                //Case to test for error and declined scenario
                //transactionStatus = "Declined";
                //transactionStatus = "error";
                //errorCode = "1234";
                //errorName = "Invalid number";
                //errorMessage = "Please verify the credentials.";

                string paymentRequestId = GetSession<string>(SessionConstants.PaymentRequestId);




                //Make DB Call to update the payment status and then redirect
                PartnerPayment partnerPayment = new PartnerPayment
                {
                    TransactionId = transactionId,
                    TransactionStatus = transactionStatus,
                    TransactionDate = transactionDatetime,
                    RequestType = string.Empty,
                    TransactionApprovalCode = transactionApprovalCode,
                    ErrorCode = errorCode,
                    ErrorMessage = errorMessage,
                    ErrorName = errorName,
                    AmountPaid = amount,
                    Result = result,
                    CardNumber = cardNumber,
                    PaymentRequestId = Convert.ToInt32(paymentRequestId, CultureInfo.InvariantCulture),
                    Culture = Convert.ToString(culture, CultureInfo.InvariantCulture)
                };

                if (result == "1")
                {
                    partnerPayment.TransactionStatus = "declined";
                }

                int partnerPaymentId = _partnerManager.UpdatePartnerPaymentDetails(partnerPayment);

                if (transactionStatus.ToLower() == "approval" && partnerPaymentId > 0)
                {
                    //Based on the transaction status customize the page display
                    Response.Redirect("~/Partners/CreditCardReceipt.aspx", false);
                }
                else if (transactionStatus.ToLower() == "error")
                {
                    // Show error message
                    messageLabel.Text = errorMessage;
                }
                else if (transactionStatus.ToLower() == "declined" || result == "1") // SSL result 1 is declined
                {
                    // Show error message
                    if (transactionStatus.ToLower() == "declined")
                        messageLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.TransactionDeclined, culture);
                    else
                        messageLabel.Text = transactionStatus;
                }


                //Log everything to DB 
                PartnerPaymentLogError partnerPaymentLogError = new PartnerPaymentLogError
                {
                    errorCodeRequestQueryStr = ErrorCodeRequestQueryStr,
                    errorNameRequestQueryStr = ErrorNameRequestQueryStr,
                    errorMessageRequestQueryStr = ErrorMessageRequestQueryStr,
                    transactionStatus = transactionStatus,
                    transactionId = transactionId,
                    transactionApprovalCode = transactionApprovalCode,
                    errorCode = errorCode,
                    errorName = errorName,
                    errorMessage = errorMessage,
                    amount = amount,
                    cardNumber = cardNumberErrLog,
                    result = result,
                    paymentRequestId = Convert.ToInt32(paymentRequestId, CultureInfo.InvariantCulture)
                };

                _partnerManager.LogErrorPartnerPayment(partnerPaymentLogError);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessLogicPolicy");
                //do nothing
            }

        }
    }
}