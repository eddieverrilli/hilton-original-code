﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True" EnableEventValidation="false"
    Inherits="Home" CodeBehind="Home.aspx.cs" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="contentContent" ContentPlaceHolderID="cphContent" runat="server">
    <script src="Scripts/jquery.jcarousel.min.js" type="text/javascript"></script>
    <link type="text/css" href="Styles/skin.css" rel="stylesheet" />
    <script type="text/javascript">
        function mycarousel_initCallback(carousel) {
            //get total item count
            var total = $('div.showarea').length;

            if (total < 5) {
                carousel.options = $.extend(carousel.options, {
                    wrap: 'last'
                });
            }
            else {
                carousel.options = $.extend(carousel.options, {
                    wrap: 'circular'
                });
            }

            // Disable autoscrolling if the user clicks the prev or next button.
            carousel.buttonNext.bind('click', function () {
                carousel.startAuto(0);
            });

            carousel.buttonPrev.bind('click', function () {
                carousel.startAuto(0);
            });

            // Pause autoscrolling if the user moves with the cursor over the clip.
            carousel.clip.hover(function () {
                carousel.stopAuto();
            }, function () {
                carousel.startAuto();
            });
        };

        jQuery(document).ready(function () {
            jQuery('#mycarousel').jcarousel({
                auto: 3,
                initCallback: mycarousel_initCallback
            });
        });

    </script>
    <div class="breadcrumbs">
        <span>
            <asp:Label ID="breadCrumLabel" runat="server" CBID="909" Text="HOME"></asp:Label></span></div>
    <div class="main-content">
        <div id="unauthorizedaccess" visible="false" viewstatemode="Enabled" runat="server">
            <div id="authorizationDiv" viewstatemode="Inherit" runat="server">
            </div>
            <br />
        </div>
        <h2>
            <asp:Label ID="headerLabel" runat="server" CBID="28" Text="Better Partners. Better Properties.">
            </asp:Label>
        </h2>
        <p>
            <asp:Label ID="headerDescriptionLabel" runat="server" CBID="29" Text=""></asp:Label></p>
    </div>
    <div class="main">
        <div class="inner-content">
            <h3>
                <asp:Label ID="SuppConLabel" runat="server" Text="Suppliers’ Connection"></asp:Label></h3>
            <div class="middle-curve">
                <ul class="list noULstyle">
                    <li>
                        <asp:HyperLink ID="browseRecommendedPartnersHyperlink" CBID="1048" runat="server"
                            Text="Browse Hilton’s Recommended Partners" NavigateUrl="~/Partners/RecommendedPartners.aspx">
                        </asp:HyperLink></li>
                    <li id="becomePartnerListItem" runat="server">
                        <asp:HyperLink ID="applyForRecommendedPartnerHyperlink" CBID="1049" NavigateUrl="~/Partners/BecomeRecommendedPartner.aspx"
                            runat="server" Text="Apply for Recommended Partner Status">
                        </asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="outFitHotelPropertyHyperlink" CBID="1050" runat="server" Text="Outfit a Hotel Property using Suppliers’ Connection">
                        </asp:HyperLink></li>
                </ul>
                <div class="line">
                    &nbsp;</div>
                <div class="carousl" id="partnerLogos" runat="server">
                    <h4>
                        <asp:Label ID="recPartnersLabel" runat="server" CBID="908" Text="RECOMMENDED PARTNERS INCLUDE:">
                        </asp:Label></h4>
                    <ul id="mycarousel" class="jcarousel-skin-tango">
                        <asp:ListView ID="datalistRecommendedPartner" runat="server" ViewStateMode="Enabled"
                            RepeatColumns="5" RepeatDirection="Horizontal" OnItemCommand="DatalistRecommendedPartner_OnItemCommand"
                            OnItemDataBound="DatalistRecommendedPartner_OnItemDataBound">
                            <itemtemplate>
                                <li>
                                    <div class="showarea">
                                        <asp:Image ID="partnerImg" ViewStateMode="Disabled" runat="server" Width="106" Height="72" />
                                        <asp:ImageButton ID="partnerImageButton" runat="server" Visible="false" Width="106"
                                            Height="72" /></div>
                                </li>
                            </itemtemplate>
                        </asp:ListView>
                    </ul>
                </div>
                <br />
                <div id="resultMessageDiv" runat="server">
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar" style="display:none" >
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="sidePanelHeaderLabel" runat="server" CBID="909" Text="HOME"></asp:Label></h3>
            <div class="middle">
                <ul>
                    <li>
                        <asp:HyperLink ID="whatIsSuppliersConnectionHyperLink" NavigateUrl="~/About/AboutSupplierConnection.aspx"
                            runat="server" Text="What is Suppliers' Connection?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="howToUseSuppliersConnectionHyperLink" NavigateUrl="~/About/HowToUseSuppliersConnection.aspx"
                            runat="server" Text="How to Use Suppliers' Connection?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="getAnAccountHyperLink" NavigateUrl="~/About/GetAnAccount.aspx"
                            runat="server" Text="How do I Get an Account?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="howToBecomeRecommendedPartnerHyperLink" NavigateUrl="~/About/HowToBecomeRecommendedPartner.aspx"
                            runat="server" Text="How do I Become a Recommended Partner?"></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="contactUsHyperLink" NavigateUrl="~/About/ContactUs.aspx" runat="server"
                            CBID="17" Text="Contact Us"></asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>