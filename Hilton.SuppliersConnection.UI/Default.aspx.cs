﻿using System;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Default : PageBase
    {
        private IUserManager _userManager;

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _userManager = UserManager.Instance;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                User user;

                //Sample cache retrieval
                user = Caching.Get<User>(CacheConstants.UserData);
                if (user == null)
                {
                    user = _userManager.GetUserDetails(1);
                    Caching.Clear(CacheConstants.UserData);
                    Caching.Add<User>(user, CacheConstants.UserData);
                }

                //Sample Session setting
                SetSession<User>(SessionConstants.UserData, user);

                //Sample Session retrieval
                User user1;
                user1 = GetSession<User>(SessionConstants.UserData);

                string timeoutValue = ConfigurationStore.GetAppSetting("P");

                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }
    }
}