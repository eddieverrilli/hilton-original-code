﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SessionExpirationPage.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.SessionExpirationPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Suppliers' Connection</title>
    <link type="text/css" href="../Styles/thickbox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../Styles/main.css" />
    <!--[if IE 8]>
<link type='text/css' href='../Styles/ie8.css' rel='stylesheet' />
<![endif]-->
    <link type='text/css' href='../Styles/basic.css' rel='stylesheet' />
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script src="../Scripts/common.js" type="text/javascript"></script>
   
</head>
<body>
    <form id="MainForm" runat="server">
   <div id="divMaster" runat="server">
        <asp:Panel ID="pnlMaster" runat="server" Style="width: 100%; height: 100%">
            <div class="header">
                <div class="wrapper">
                    <h1>
                        <asp:HyperLink ID="homeHyperlink" runat="server" NavigateUrl="~/">Hilton Worldwide</asp:HyperLink>
                    </h1>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <div class="main-content">
                        <h2>
                            <asp:Label ID="errorHeaderLabel" runat="server" CBID="0" Text="Session Expired!"></asp:Label>
                        </h2>
                        <p>
                            <asp:Label ID="errorDescriptionLabel" runat="server" CBID="0" Text=""></asp:Label></p>
                    </div>
                    <div class="main">
                        <div class="inner-content">
                            <div class="middle-curve">
                                <asp:Label ID="errorMsgPart1" runat="server" CBID="0" Text="Your connection to Suppliers’ Connection has expired. Please "> </asp:Label>
                                <asp:HyperLink ID="errorMsgLink" CssClass="lnkLogin" runat="server" NavigateUrl="~/">login</asp:HyperLink>
                                <asp:Label ID="errorMsgPart2" runat="server" CBID="0" Text="again."> </asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
            </div>
            <div class="footer">
                <div class="wrapper">
                </div>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
