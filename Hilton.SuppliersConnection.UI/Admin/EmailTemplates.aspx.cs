﻿using System;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI
{
    public partial class EmailTemplates : PageBase
    {
        private IEmailTemplateManager _emailTemplateManager;

        /// <summary>
        /// Invoked when add email template button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AddEmailTemplateLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                Response.Redirect("~/Admin/EmailTemplateDetails.aspx", false);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Grid view row command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void EmailTemplateListGridView_RowCommand(object sender, CommandEventArgs args)
        {
            try
            {
                if (args != null && (args.CommandName == "EditEmailTemplate"))
                {
                    Context.Items[UIConstants.Mode] = UIConstants.Edit;
                    Context.Items[UIConstants.EmailTemplateId] = args.CommandArgument.ToString();
                    Server.Transfer("~/Admin/EmailTemplateDetails.aspx", true);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void EmailTemplateListGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Grid view row data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EmailTemplateListGridView_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton editLinkButton = args.Row.FindControl("editLinkButton") as LinkButton;
                    if (editLinkButton != null)
                    {
                        ScriptManager.GetCurrent(this).RegisterPostBackControl(editLinkButton);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void EmailTemplateListGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Invoded on page initialization
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs args)
        {
            base.OnInit(args);
            _emailTemplateManager = EmailTemplateManager.Instance;
        }

        /// <summary>
        /// Invode on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void Page_Load(object sender, EventArgs args)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    if (!Page.IsPostBack)
                    {
                        if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("EmailTemplate.aspx") || Request.UrlReferrer.ToString().Contains("EmailTemplateDetails.aspx")))
                        {
                            MaintainPageState();
                        }
                        else
                        {
                            ClearSession(SessionConstants.EmailTemplateSearch);
                            EmailTemplateSearch emailTemplateSearch = new EmailTemplateSearch();
                            emailTemplateSearch.SortExpression = UIConstants.EmailTemplateId;
                            emailTemplateSearch.SortDirection = UIConstants.AscAbbreviation;
                            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            SetViewState(ViewStateConstants.SortExpression, UIConstants.EmailTemplateId);
                            SearchEmailTemplate(emailTemplateSearch);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will return the sort colum index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in emailTemplateListGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return emailTemplateListGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("EmailTemplate.aspx") || Request.UrlReferrer.ToString().Contains("EmailTemplateDetails.aspx")))
                    {
                        EmailTemplateSearch emailTemplateSearch = GetSession<EmailTemplateSearch>(SessionConstants.EmailTemplateSearch);
                        if (field.SortExpression == emailTemplateSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, emailTemplateSearch.SortExpression);
                            if (emailTemplateSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return emailTemplateListGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// To Maintain the page state
        /// </summary>
        private void MaintainPageState()
        {
            EmailTemplateSearch emailTemplateSearch = GetSession<EmailTemplateSearch>(SessionConstants.EmailTemplateSearch);

            if (emailTemplateSearch != null)
            {
                SearchEmailTemplate(emailTemplateSearch);
            }
        }

        /// <summary>
        /// To Search the email template
        /// </summary>
        /// <param name="emailTemplateSearch"></param>
        private void SearchEmailTemplate(EmailTemplateSearch emailTemplateSearch)
        {
            //Save criteria in session for back button functionality
            ClearSession(SessionConstants.EmailTemplateSearch);
            SetSession<EmailTemplateSearch>(SessionConstants.EmailTemplateSearch, emailTemplateSearch);

            var emailTemplateList = _emailTemplateManager.GetEmailTemplates(emailTemplateSearch);
            if (emailTemplateList.Count == 0)
            {
                searchResultDiv.Attributes.Add("class", "message notification");
                searchResultDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoContentBlockFoundForCriteria, culture);
            }

            emailTemplateListGridView.DataSource = emailTemplateList;
            emailTemplateListGridView.DataBind();
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            EmailTemplateSearch emailTemplateSearch = GetSession<EmailTemplateSearch>(SessionConstants.EmailTemplateSearch);
            emailTemplateSearch.SortExpression = sortExpression;
            emailTemplateSearch.SortDirection = sortDirection;
            SearchEmailTemplate(emailTemplateSearch);
        }
    }
}