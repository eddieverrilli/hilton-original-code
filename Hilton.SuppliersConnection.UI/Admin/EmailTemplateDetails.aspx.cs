﻿using System;
using System.Globalization;
using System.Web.UI;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI.Admin
{
    public partial class EmailTemplateDetails : PageBase
    {
        private IEmailTemplateManager _emailTemplateManager;

        /// <summary>
        /// Invoked on page initialization
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _emailTemplateManager = EmailTemplateManager.Instance;
            //HtmlGenericControl li = new HtmlGenericControl("li");
            //li = (HtmlGenericControl)(this.Master.FindControl("liSettings"));
            //li.Attributes.Add("class", "active");
            //            HttpBrowserCapabilities browser = Request.Browser // COMMENTED BY CODEIT.RIGHT;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //  resultMessageDiv.Visible = false;
                    //Rest result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    if (!Page.IsPostBack)
                    {
                        if (Context.Items[UIConstants.Mode] != null && Context.Items[UIConstants.Mode].ToString() == UIConstants.Edit)
                        {
                            SetPageSettings(UIConstants.Edit);

                            //int contentBlockId = Convert.ToInt32(Context.Items[UIConstants.ContentBlockId], CultureInfo.InvariantCulture);
                            PopulateContentBlockDetails(Convert.ToInt32(Context.Items[UIConstants.EmailTemplateId], CultureInfo.InvariantCulture));
                        }
                        else
                        {
                            SetPageSettings(UIConstants.Add);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.ContentBlockAlreadyExists, culture);
            }
        }

        /// <summary>
        /// Set the page setting for Add/Edit view
        /// </summary>
        private void SetPageSettings(string mode)
        {
            //txtBody.Attributes.Add("class", "mandatory");
            switch (mode)
            {
                case UIConstants.Edit:
                    SetViewState(UIConstants.Mode, UIConstants.Edit);
                    SetLabelText(contentBlockTitleLabel, "Edit Email Template");

                    break;

                case UIConstants.Add:
                    SetViewState(UIConstants.Mode, UIConstants.Add);
                    SetLabelText(contentBlockTitleLabel, "Add Email Template");
                    break;
            }
        }

        private void PopulateContentBlockDetails(int emailTemplateId)
        {
            EmailTemplate emailTemplate = _emailTemplateManager.GetEmailTemplateDetails(emailTemplateId);
            SetLabelText(emailTemplateIDLabel, Convert.ToString(emailTemplate.EmailTemplateId, CultureInfo.InvariantCulture));
            SetTextBoxText(descriptionTextBox, emailTemplate.Description);
            SetTextBoxText(sectionTextBox, emailTemplate.Subject);
            SetTextBoxText(titleTextBox, emailTemplate.EmailTemplateName);
            txtBody.InnerText = emailTemplate.Body.ToString(CultureInfo.InvariantCulture);
            ddlStatus.SelectedValue = Convert.ToString(emailTemplate.Status, CultureInfo.InvariantCulture);
        }

        protected void SaveLinkButton_Click(object sender, EventArgs e)
        {
            //            string body = Server.HtmlDecode(this.txtBody.InnerHtml.ToString()) // COMMENTED BY CODEIT.RIGHT;
            try
            {
                if (GetViewState<string>(UIConstants.Mode) == UIConstants.Add)
                {
                    AddEmailTemplate();
                }
                else if ((GetViewState<string>(UIConstants.Mode) == UIConstants.Edit))
                {
                    UpdateEmailTemplate();
                }
            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
            }
        }

        private void UpdateEmailTemplate()
        {
            EmailTemplate emailTemplate = new EmailTemplate()
            {
                Mode = UIConstants.Edit,
                Body = Server.HtmlDecode(this.txtBody.InnerHtml.ToString()),
                Description = descriptionTextBox.Text.ToString().Trim(),
                EmailTemplateId = Convert.ToInt32(emailTemplateIDLabel.Text.ToString().Trim()),
                EmailTemplateName = titleTextBox.Text.ToString().Trim(),
                Subject = sectionTextBox.Text.ToString().Trim(),
                Status = Convert.ToBoolean(Convert.ToInt16(ddlStatus.SelectedValue.Trim()))
            };
            int result = _emailTemplateManager.UpdateEmailTemplate(emailTemplate);
        }

        private void AddEmailTemplate()
        {
            EmailTemplate emailTemplate = new EmailTemplate()
            {
                Mode = UIConstants.Add,
                Body = Server.HtmlDecode(this.txtBody.InnerHtml.ToString()),
                Description = descriptionTextBox.Text.ToString().Trim(),
                EmailTemplateName = titleTextBox.Text.ToString().Trim(),
                Subject = sectionTextBox.Text.ToString().Trim(),
                Status = Convert.ToBoolean(Convert.ToInt16(ddlStatus.SelectedValue.Trim()))
            };
            int result = _emailTemplateManager.AddEmailTemplate(emailTemplate);
        }
    }
}