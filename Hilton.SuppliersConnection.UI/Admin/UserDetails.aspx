﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="UserDetails.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.UserDetails" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(BeginRequestHandler);
            prm.add_endRequest(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                var request = args.get_request();
                var customData =
                {
                    topScroll: args.get_postBackElement().parentElement.parentElement.scrollTop
                };
                request.set_userContext(customData);

            }
            function EndRequestHandler(sender, args) {
                var response = args.get_response();
                if (response.get_responseAvailable()) {
                    var webRequest = response.get_webRequest();
                    var customData = webRequest.get_userContext();
                    if (customData.topScroll) {
                        if ($('#<%= allProjectDiv.ClientID %>') != null) {
                            if ($('#<%= allProjectDiv.ClientID %>')[0] != null)
                                $('#<%= allProjectDiv.ClientID %>')[0].scrollTop = customData.topScroll;
                        }
                    }
                }
            }
        });
        function fireRowSelection(control, rowIndex) {
            debugger;
            __doPostBack(control, rowIndex);
        }
    </script>
</asp:Content>
<asp:Content ID="detailContent" ContentPlaceHolderID="cphDetails" ViewStateMode="Enabled"
    runat="server">
    <asp:ScriptManager ID="userDetailsScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../Images/search-top-curve.gif" alt="" /></div>
            <div class="middle">
                <asp:HyperLink NavigateUrl="Users.aspx" runat="server" ID="backToUsersHyperLink"
                    Text="‹‹ Back To Users"></asp:HyperLink>
            </div>
            <div class="bottom-curve">
                <img src="../Images/search-bottom-curve.gif" alt="" /></div>
        </div>
    </div>
    <div class="content">
        <div class="wrapper">
            <h2>
                <asp:Literal ID="titleLiteral" runat="server"></asp:Literal>
            </h2>
            <div id="add-edit-partner" class="inner-content">
                <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <br />
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <div class="lft-col">
                    <div class="box1">
                        <h3>
                            <asp:Label ID="basicInformationLabel" runat="server" Text="Basic Information" CBID="248"></asp:Label></h3>
                        <div class="row">
                            <div>
                                <asp:Label ID="idHeadLabel" runat="server" Text="ID" CBID="974" CssClass="label"></asp:Label>
                            </div>
                            <asp:UpdatePanel ID="updateUserIdUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="input textbox3">
                                        <asp:Label ID="idLabel" runat="server"></asp:Label>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="firstNameLabel" CBID="109" Text="First Name"></asp:Label>
                            </div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="firstNameTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="firstNameValidator" runat="server" VMTI="1" SetFocusOnError="true"
                                    ErrorMessage="Please enter first name" ControlToValidate="firstNameTextBox" CssClass="errorText"
                                    Display="Dynamic" ValidationGroup="basicInfoGroup"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="firstNameRegularExpressionValidator" SetFocusOnError="true"
                                    runat="server" ControlToValidate="firstNameTextBox" VMTI="2" ErrorMessage="Please enter valid name"
                                    CssClass="errorText" ValidationGroup="basicInfoGroup" Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="lastnameLabel" CBID="121" Text="Last Name"></asp:Label>
                            </div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="lastNametextBox" runat="server" MaxLength="50"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="lastNameValidator" VMTI="3" runat="server" SetFocusOnError="true"
                                    ControlToValidate="lastNametextBox" ErrorMessage="Please enter last name" CssClass="errorText"
                                    Display="Dynamic" ValidationGroup="basicInfoGroup"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="lastNameRegularExpressionValidator" VMTI="4"
                                    runat="server" ControlToValidate="lastNametextBox" SetFocusOnError="true" ErrorMessage="Please enter valid name"
                                    CssClass="errorText" ValidationGroup="basicInfoGroup" Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="emailLabel" CBID="383" Text="Email Address"></asp:Label>
                            </div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="emailAddressTextBox" runat="server" MaxLength="128"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" VMTI="11" runat="server"
                                    ErrorMessage="Correct the format" CssClass="errorText" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="emailAddressTextBox" ValidationGroup="basicInfoGroup"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="requiredEmailValidator" runat="server" VMTI="10"
                                    ControlToValidate="emailAddressTextBox" ErrorMessage="Please enter email" SetFocusOnError="true"
                                    CssClass="errorText" Display="Dynamic" ValidationGroup="basicInfoGroup"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="hiltonIdLabel" CBID="417" Text="Hilton ID"></asp:Label>
                            </div>
                            <div class="input textbox3">
                                <asp:TextBox ID="hiltonIdTextBox" runat="server" MaxLength="100"></asp:TextBox><br />
                                <asp:RegularExpressionValidator ID="hiltonIdRegularExpression" VMTI="40" ViewStateMode="Enabled"
                                    SetFocusOnError="true" ControlToValidate="hiltonIdTextBox" ValidationGroup="basicInfoGroup"
                                    CssClass="errorText" Display="Dynamic" runat="server" ErrorMessage="Please enter valid Hilton Id"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" CBID="975" ID="statusLabel" Text="User Status"></asp:Label>
                            </div>
                            <div class="input textbox3">
                                <asp:DropDownList ID="userStatusDropDown" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="section alR pad-top">
                        <asp:UpdatePanel ID="buttonsUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <span class="btn-style" id="loginAsThisUserSpan" runat="server">
                                    <asp:LinkButton ID="loginAsThisUserLinkButton" runat="server" Text="Login as this User"
                                        CBID="428" OnClick="LoginAsUserLinkButton_Click">
                                    </asp:LinkButton>
                                </span><span class="btn-style">
                                    <asp:LinkButton ID="saveLinkButton" runat="server" Text="Save" CBID="644" OnClick="SaveLinkButton_Click"
                                        ValidationGroup="basicInfoGroup"></asp:LinkButton>
                                </span>
                                <asp:HiddenField ID="userIdHiddenField" runat="server" />
                                <asp:HiddenField ID="hiltonIdHiddenField" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <asp:CustomValidator ID="adminPermissionValidator" CssClass="errorText" Display="Dynamic"
                        runat="server" ValidationGroup="basicInfoGroup" ControlToValidate="userRoleDropDownList"
                        OnServerValidate="AdminPermissionValidator_ServerValidate"></asp:CustomValidator>
                    <asp:UpdatePanel ID="logUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="accessLogDiv" class="box box2" runat="server">
                                <h3>
                                    <asp:Label ID="accesssLogHeaderLabel" Text="Access Log" runat="server"></asp:Label>
                                </h3>
                                <div class="box-content">
                                    <div class="scroll">
                                        <asp:GridView ID="accessLogGridView" runat="server" AutoGenerateColumns="false" CssClass="col-3"
                                            Width="100%" border="1" rules="all" Style="border-style: None; width: 100%; border-collapse: collapse;">
                                            <Columns>
                                                <asp:BoundField HeaderText="Date" DataField="LogOn" ItemStyle-CssClass="col-1" DataFormatString="{0:dd-MMM-yyyy}" />
                                                <asp:BoundField HeaderText="Time" DataField="LogOn" ItemStyle-CssClass="col-2" DataFormatString="{0:HH:mm:ss}" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div id="activationEmailDiv" class="box box2" runat="server">
                                <h3>
                                    <asp:Label ID="activationEmailLabel" Text="Activation Emails" runat="server"></asp:Label>
                                </h3>
                                <div class="box-content">
                                    <div class="scroll">
                                        <asp:GridView ID="activationEmailGridView" runat="server" AutoGenerateColumns="false"
                                            AllowSorting="true" CssClass="col-3" Width="100%">
                                            <Columns>
                                                <asp:BoundField HeaderText="Email" DataField="Email" ItemStyle-CssClass="col-1" />
                                                <asp:BoundField HeaderText="Date" DataField="RequestSentDateTime" ItemStyle-CssClass="col-2"
                                                    DataFormatString="{0:dd-MMM-yyyy}" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="userTypeUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="rgt-col">
                            <div class="box1 section1">
                                <h3>
                                    <asp:Label ID="linkageLabel" Text="Linkage" CBID="783" runat="server"></asp:Label></h3>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="userTypeLabel" Text="User Type" CBID="734" runat="server"></asp:Label></div>
                                    <div class="input no-pad select2">
                                        <asp:DropDownList ID="userTypeDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="UserTypeDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="userTypeRequiredFieldValidator" VMTI="41" SetFocusOnError="true"
                                            runat="server" CssClass="errorText" Display="Dynamic" ControlToValidate="userTypeDropDownList"
                                            ErrorMessage="Please select a user type" InitialValue="0" ValidationGroup="basicInfoGroup"></asp:RequiredFieldValidator>
                                        <asp:HiddenField ID="isAdminUserHiddenField" runat="server" Value="0" />
                                    </div>
                                </div>
                                <div class="row" id="associatedVendorDiv" runat="server">
                                    <div class="label">
                                        <asp:Label ID="associatedPartnerLabel" Text="Associated Partner" CBID="238" runat="server"></asp:Label></div>
                                    <div class="input no-pad select3">
                                        <asp:DropDownList ID="associatedVendorDropDownList" runat="server">
                                        </asp:DropDownList>
                                        <asp:CustomValidator ID="partnerNameCustomValidator" CssClass="errorText" Display="Dynamic"
                                            runat="server" ValidationGroup="AddPartner" ControlToValidate="associatedVendorDropDownList"
                                            OnServerValidate="partnerNameCustomValidator_ServerValidate"></asp:CustomValidator>
                                    </div>
                                    <div align="right">
                                        <span class="btn-style">
                                            <asp:LinkButton ID="lnkButtonAdd" runat="server" ValidationGroup="AddPartner" CBID="177"
                                                Text="Add" OnClick="LinkButton_Click"></asp:LinkButton>
                                        </span>
                                    </div>
                                    <div class="box-content">
                                        <asp:GridView ID="gridviewPartners" runat="server" OnRowCommand="GridviewPartner_RowCommand"
                                            AutoGenerateColumns="false" class="col-3" Width="100%" DataKeyNames="PartnerId">
                                            <Columns>
                                                <asp:BoundField HeaderText="Partners" DataField="CompanyDescription" ItemStyle-CssClass="col-3"
                                                    ItemStyle-Width="80%" />
                                                <asp:ButtonField ButtonType="Image" ImageUrl="../Images/cross.gif" CommandName="DeletePartner"
                                                    ItemStyle-CssClass="col-6" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="box box3" id="associatedProjectDiv" runat="server">
                                <h3>
                                    <asp:Label ID="associatedProjectsLabel" Text="Associated Projects" runat="server"></asp:Label>
                                </h3>
                                <div class="box-content">
                                    <asp:UpdatePanel ID="userProjectMappingUpdatePanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div id="userProjectMappingDiv" runat="server">
                                            </div>
                                            <br />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div id="associatProjectDiv" runat="server">
                                        <asp:GridView ID="associatedProjectsGridView" runat="server" AutoGenerateColumns="false"
                                            OnRowDataBound="AssociatedProjectsGridView_RowDataBound" class="col-3" Width="100%">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-CssClass="col-1" HeaderText="Type">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ProjectTypeDescription")%>' /><br />
                                                        <asp:Label ID="projectAddedDateLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ProjectAddedDate", "{0:dd-MMM-yyyy}")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="col-2">
                                                    <ItemTemplate>
                                                        <asp:Label ID="brandNameLabel" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.Brand")%>' />
                                                        <br />
                                                        <asp:Label ID="facilityNameLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.FacilityUniqueName")%>' />
                                                        <br />
                                                        <asp:Label ID="propertyAddressOneLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.Address")%>' />
                                                        <br />
                                                        <asp:Label ID="propertyAddressTwoLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.Address2")%>' />
                                                        <asp:Label ID="cityLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Property.City")%>' />,
                                                        <asp:Label ID="stateLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.State")%>' />
                                                        <asp:Label ID="zipCodeLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Property.ZipCode")%>' />
                                                        <br />
                                                        <asp:Label ID="countryNameLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Property.Country")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="col-3">
                                                    <ItemTemplate>
                                                        <asp:Label ID="userRoleLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "User.UserRole")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="cancelImage" ImageUrl="../Images/cross.gif" runat="server" OnClick="CancelImage_Click"
                                                            CommandArgument='<%# Bind("ProjectId") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="field-three">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="col-1">
                                                    <asp:DropDownList ID="projectTypeDropDown" OnSelectedIndexChanged="ProjectTypeDropDown_SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="col-2">
                                                    <asp:DropDownList ID="brandsDropDown" OnSelectedIndexChanged="BrandsDropDown_SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="col-3">
                                                    <asp:DropDownList ID="regionDropDown" OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <span class="btn-style">
                                                        <asp:LinkButton ID="projectsSearchLinkButton" runat="server" Text="Search" CBID="1047"
                                                            OnClick="ProjectsSearchButton_Click"></asp:LinkButton></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="allProjectDiv" runat="server">
                                        <asp:HiddenField ID="selectedRowHiddenField" runat="server" Value="" />
                                        <asp:GridView ID="allProjectsGridView" runat="server" AutoGenerateColumns="false"
                                            DataKeyNames="ProjectId" class="col-3 tr-selected full-tb" OnRowDataBound="AllProjectsGridView_RowDataBound"
                                            OnRowCreated="AllProjectsGridView_RowCreated">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-CssClass="col-1">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ProjectTypeDescription")%>' /><br />
                                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ProjectAddedDate", "{0:dd-MMM-yyyy}")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="col-2">
                                                    <ItemTemplate>
                                                        <asp:Label ID="brandNameLabel" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.Brand")%>' />
                                                        <br />
                                                        <asp:Label ID="facilityNameLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.FacilityUniqueName")%>' />
                                                        <br />
                                                        <asp:Label ID="propertyAddressOneLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.Address")%>' />
                                                        <br />
                                                        <asp:Label ID="propertyAddressTwoLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.Address2")%>' />
                                                        <asp:Label ID="cityLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Property.City")%>' />,
                                                        <asp:Label ID="stateLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Property.State")%>' />
                                                        <asp:Label ID="zipCodeLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Property.ZipCode")%>' />
                                                        <br />
                                                        <asp:Label ID="countryNameLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Property.Country")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="col-3">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ownerLabel" runat="server" Text="Owner:" />
                                                        <asp:Label ID="projectOwnerLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ProjectOwner")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:UpdatePanel ID="allProjectGridViewUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="allProjectGridViewDiv" runat="server">
                                                    <br />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="field-three" id="addRoleDiv" runat="server" visible="false">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="col-1">
                                                    &nbsp;
                                                </td>
                                                <td class="col-2">
                                                    &nbsp;
                                                </td>
                                                <td class="col-3">
                                                    <asp:DropDownList ID="userRoleDropDownList" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <span class="btn-style">
                                                        <asp:LinkButton ID="addRoleLinkButton" Text="Add" CBID="177" runat="server" ValidationGroup="AddRole"
                                                            OnClick="AddRoleLinkButton_Click"></asp:LinkButton></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <asp:CustomValidator ID="validateRoleMapping" CssClass="errorText" Display="Dynamic"
                                        runat="server" ValidationGroup="AddRole" ControlToValidate="userRoleDropDownList"
                                        OnServerValidate="RoleMappingValidator_ServerValidate"></asp:CustomValidator>
                                </div>
                            </div>
                            <div class="box box4" id="adminPanel" runat="server">
                                <h3>
                                    <asp:Label ID="adminPermissionsLabel" runat="server" Text="Administrator Permissions"></asp:Label>
                                </h3>
                                <div class="box-content">
                                    <h4>
                                        <asp:Label ID="regionLabel" CBID="607" runat="server" Text="Regions"></asp:Label>
                                    </h4>
                                    <div id="regionErrorMessageDiv" runat="server" visible="false">
                                        &nbsp;&nbsp;<asp:Label ID="regionErrorMessageLabel" CssClass="errorText" runat="server" />
                                        <br />
                                    </div>
                                    <div class="all-check">
                                        <asp:CheckBox ID="allRegionsCheckBox" CssClass="checkbox" runat="server" Text="All"
                                            AutoPostBack="True" OnCheckedChanged="AllRegionsCheckBox_CheckedChanged" />
                                    </div>
                                    <asp:CheckBoxList ID="regionsCheckBoxList" CssClass="checkboxlist" AutoPostBack="True"
                                        runat="server" RepeatColumns="3" RepeatDirection="Horizontal" OnSelectedIndexChanged="RegionsCheckBox_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                    <br />
                                    <h4>
                                        <asp:Label ID="permissionLabel" CBID="512" runat="server" Text="Permissions"></asp:Label></h4>
                                    <div id="permissionErrorMessageDiv" runat="server" visible="false">
                                        &nbsp;&nbsp;<asp:Label ID="permissionErrorMessageLabel" CssClass="errorText" runat="server" />
                                    </div>
                                    <div class="all-check">
                                        <asp:CheckBox ID="allPermissionsCheckBox" CssClass="checkbox" runat="server" Text="All"
                                            AutoPostBack="True" OnCheckedChanged="AllPermissionsCheckBox_CheckedChanged">
                                        </asp:CheckBox></div>
                                    <asp:CheckBoxList ID="permissionsCheckBoxList" CssClass="checkboxlist" AutoPostBack="True"
                                        runat="server" RepeatColumns="3" RepeatDirection="Horizontal" OnSelectedIndexChanged="PermissionsCheckBoxList_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                    <table cellpadding="0" cellspacing="0" border="0" class="tb8">
                                        <tr>
                                            <td width="240">
                                                <asp:PlaceHolder ID="parentCategoryContainer" ViewStateMode="Enabled" runat="server">
                                                </asp:PlaceHolder>
                                            </td>
                                            <td>
                                                <span class="btn-style" id="addCategoryAccessPermissionSpan" viewstatemode="Enabled"
                                                    runat="server" visible="false">
                                                    <asp:LinkButton ID="addCategoryAccessPermissionLinkButton" Text="Add" CBID="177"
                                                        runat="server" OnClick="AddCategoryAccessPermissionLinkButton_Click"></asp:LinkButton></span>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="categoryListGridView" ValidationGroup="basicInfoGroup" runat="server"
                                        AutoGenerateColumns="false" class="col-3" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="CategoryId" ItemStyle-CssClass="first-col" Visible="false" />
                                            <asp:BoundField DataField="ParentCategoryHierarchy" />
                                            <asp:TemplateField>
                                                <ItemStyle CssClass="third-col" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="cancelImage" ImageUrl="../Images/cross.gif" runat="server" OnCommand="CancelImage_Command"
                                                        CommandArgument='<%# Bind("CategoryId") %>' CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:CustomValidator ID="categoryListValidator" ValidationGroup="basicInfoGroup"
                                        runat="server"></asp:CustomValidator>
                                    <asp:UpdatePanel ID="categoryAdditionResultUpdatePanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div id="categoryAdditionResultDiv" runat="server">
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
