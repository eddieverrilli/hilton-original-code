﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddFeedbakCommentPopup.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Admin.AddFeedbakCommentPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../Styles/iframe-modal.css" />
    <!--[if IE 8]>
    <link type='text/css' href='../Styles/adminie8.css' rel='stylesheet' />
    <![endif]-->
    <link href="../Styles/iewidthfix.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.ie-select-width.js" ></script>
    <script type="text/javascript" src="../Scripts/ie-select-width.js"></script>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="addCommentsScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>    
    <script type="text/javascript">

        $(window).ready(function () {
            $('.cancel').click(function () {
                window.parent.closeIframe();
                return false;

            });


            $("#commentsTextBox").keyup(function () {


                if (this.value.length > 2000) {

                    alert("Comments limited to 2,000 characters only.");
                    this.value = this.value.substring(0, 2000);
                    return false;
                }
                else {
                    var count = 2000 - this.value.length;
                    $(".word_count").text("Characters left: " + count);
                }
            });

            $("#commentsTextBox").focus(function () {


                if (this.value.length > 2000) {

                    alert("Comments limited to 2,000 characters only.");
                    this.value = this.value.substring(0, 2000);
                    return false;
                }
                else {
                    var count = 2000 - this.value.length;
                    $(".word_count").text("Characters left: " + count);
                }
            });


        });


        function Confirm() {

            if ($(".input").val() == "Enter Your Comment" || $(".input").val().length == 0) {
                alert("No comments have been entered.");
                $(".input").focus();
                return false;
            }
        }
            
        

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }

    </script>
    <div class="modalbox" runat="server" >
        <h3>
            <asp:Label ID="titleLabel" runat="server" Text="Add 
            Feedback Comment" ></asp:Label></h3>
        <div class="modal-content" >
            <div class="modal-innercontent">
                
                <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="resultMessageDiv" runat="server" styel="width=400px;">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="copybox">
                    <p>
                    <asp:TextBox ID="commentsTextBox" runat="server"  class ="input" 
                        onfocus="if(this.value==this.defaultValue)this.value=''"  TextMode="MultiLine"
                        Text="Enter Your Comment" MaxLength ="2000" Height="201px" 
                            Width="397px"></asp:TextBox>
                            <br />

                </p>
                </div>
                 
                  <div>
                        <asp:Label ID="Countlbl" class="word_count" runat="server"></asp:Label>
                    
                </div>
                <div class=" alR ">
                    <span class="btn-style btn-stylepad">
                        <asp:LinkButton ID="saveCommentLinkButton" OnClientClick='return Confirm();'
                            Text="Save" ValidationGroup="ValidationGp2" runat="server" 
                        onclick="saveCommentLinkButton_Click" ></asp:LinkButton>
                    </span><span class="btn-style">
                        <asp:LinkButton ID="cancelCommentLinkButton" runat="server" Text="Cancel" 
                            class="cancel simplemodal-overlay" ></asp:LinkButton>
                    </span>
                </div>
            </div>
       </div>
    </div>
    </form>
</body>
</html>