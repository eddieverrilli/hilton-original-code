﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;
using System.Linq;
using System.Data;
using Aspose.Cells;
using System.Text.RegularExpressions;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Partners : PageBase
    {
        private IPartnerManager _partnerManager;

        int countChecked = 0;
        string flagSearch = string.Empty;

        /// <summary>
        /// Binds Data to Category DropDown
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            try
            {
                categoryDropDown.DataSource = categoryList;
                categoryDropDown.DataTextField = "CategoryName";
                categoryDropDown.DataValueField = "CategoryId";
                categoryDropDown.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


        /// <summary>
        /// Invoked at AddPartnerLinkButton Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddPartnerLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                if (partnerSearch != null)
                {
                    partnerSearch.PageSet = pagingControl.PageSet;
                    ClearSession(SessionConstants.PartnerSearch);
                    SetSession<PartnerSearch>(SessionConstants.PartnerSearch, partnerSearch);
                }
                Response.Redirect("~/Admin/PartnerDetails.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Sets GridView HeaderText
        /// </summary>
        private void SetGridViewHeaderText()
        {
            partnerListGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(494, culture, "Partner Name");
            partnerListGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(332, culture, "Contact Name");
            partnerListGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(0, culture, "Partner Type");
            partnerListGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(623, culture, "Request Submitted");
            partnerListGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(391, culture, "Expiration");
            partnerListGridView.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(695, culture, "Status");
        }

        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                partnerSearch.PageIndex = pagingControl.CurrentPageIndex;
                partnerSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                partnerSearch.AllClick = pagingControl.AllPageClick;
                SearchPartners(partnerSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }



        /// <summary>
        /// Category DropDown selected index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void CategoryDropDown_SelectedIndexChanged(object sender, EventArgs args)
        {
            try
            {
                SetSubCategoryAccess();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Sub Category DropDown selected index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void SubCategoryDropDown_SelectedIndexChanged(object sender, EventArgs args)
        {
            try
            {
                SetTerCategoryAccess();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Sets Sub-Category Access
        /// </summary>
        private void SetSubCategoryAccess()
        {
            IList<Category> subcategories = GetChildCategories(categoryDropDown.SelectedValue);
            if (categoryDropDown.SelectedIndex > 0 && subcategories.Count > 0)
            {
                subcategoryDropDown.Enabled = true;
                PopulateCategoryData(subcategories, 2, subcategoryDropDown); // second level categories
            }
            else
            {
                subcategoryDropDown.Items.Clear();
                subcategoryDropDown.Enabled = false;
            }
            tertiarycategoryDropDown.Items.Clear();
            tertiarycategoryDropDown.Enabled = false;
        }

        /// <summary>
        /// Sets Sub-Category Access
        /// </summary>
        private void SetTerCategoryAccess()
        {
            IList<Category> subcategories = GetChildCategories(subcategoryDropDown.SelectedValue);
            if (subcategoryDropDown.SelectedIndex > 0 && subcategories.Count > 0)
            {
                tertiarycategoryDropDown.Enabled = true;
                PopulateCategoryData(subcategories, 3, tertiarycategoryDropDown); // Third level categories
            }
            else
            {
                tertiarycategoryDropDown.Items.Clear();
                tertiarycategoryDropDown.Enabled = false;
            }
        }


        /// <summary>
        /// Gets Child Categories
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            IList<Category> categoryList = (categories.AsEnumerable().
                                            Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture))).ToList());
            return categoryList;
        }


        /// <summary>
        /// Binds DropDown
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        private void BindDropDown(IList<Category> categories, DropDownList sourceDropDown, string defaultText)
        {
            sourceDropDown.DataSource = categories.OrderBy(p => p.CategoryName);
            sourceDropDown.DataTextField = "CategoryName";
            sourceDropDown.DataValueField = "CategoryId";
            sourceDropDown.DataBind();
            sourceDropDown.Items.Insert(0, new ListItem(defaultText, "0"));
        }


        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                partnerSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchPartners(partnerSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at GoButton click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoButton_click(object sender, EventArgs e)
        {
            try
            {
                validateRequestDateCustomValidator.ServerValidate += new ServerValidateEventHandler(ValidateRequestDate);
                validateRequestDateCustomValidator.Validate();
                validateExpirationDateCustomValidator.ServerValidate += new ServerValidateEventHandler(ValidateExpirationDate);
                validateExpirationDateCustomValidator.Validate();
                if (IsValid)
                {
                    if (string.IsNullOrWhiteSpace(selectedBrands.Text))
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", "Please select brand");
                    }
                    else
                    {
                        PartnerSearch partnerSearch = BuildSearchCriteria();
                        partnerSearch.SearchFlag = SearchFlagEnum.Filter.ToString();

                        GetPartnerList(partnerSearch);
                    }
                    if (statusDropDown.SelectedValue == UIConstants.StatusPermanantlyInactive && countChecked != 0)
                    {
                        downloadPartnerReportLinkButton.Style.Add("display", "none");
                        downloadPartnerRptGettingDeleted.Style.Add("display", "block");
                        DeletePartnerLnk.Style.Add("display", "none");
                    }
                    else
                    {
                        downloadPartnerReportLinkButton.Style.Add("display", "block");
                        downloadPartnerRptGettingDeleted.Style.Add("display", "none");
                        DeletePartnerLnk.Style.Add("display", "none");
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


        /// <summary>
        ///  Register for post backs
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (countChecked != 0)
            {
                downloadPartnerReportLinkButton.Style.Add("display", "none");
                downloadPartnerRptGettingDeleted.Style.Add("display", "block");
                DeletePartnerLnk.Style.Add("display", "none");
            }
            else
            {
                downloadPartnerReportLinkButton.Style.Add("display", "block");
                downloadPartnerRptGettingDeleted.Style.Add("display", "none");
                DeletePartnerLnk.Style.Add("display", "none");
            }
            IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            brands.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(optgroup, p.DataValueField)));
            base.Render(writer);
        }


        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                partnerSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchPartners(partnerSearch);
                ClearSession(SessionConstants.PartnerSearch);
                SetSession<PartnerSearch>(SessionConstants.PartnerSearch, partnerSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                partnerSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchPartners(partnerSearch);
                ClearSession(SessionConstants.PartnerSearch);
                SetSession<PartnerSearch>(SessionConstants.PartnerSearch, partnerSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                ValidateUserAccess((int)MenuEnum.Partners);
                _partnerManager = PartnerManager.Instance;
                addPartnerLinkButton.Text = string.Format(CultureInfo.InvariantCulture, "+ {0}", ResourceUtility.GetLocalizedString(185, culture, "Add Partner"));
                SetGridViewHeaderText();
                this.Title = ResourceUtility.GetLocalizedString(1097, culture, "Partner List");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;

                    validateRequestDateCustomValidator.ErrorMessage = string.Empty;
                    validateExpirationDateCustomValidator.ErrorMessage = string.Empty;

                    string IsPartnerDeleted = Request.QueryString["Par"];

                    if (!Page.IsPostBack)
                    {
                        User currentUser = GetSession<User>(SessionConstants.User);

                        PopulateDropDownData();
                        if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("PartnerDetails.aspx") || Request.UrlReferrer.ToString().Contains("Partners.aspx")) && IsPartnerDeleted != "yes")
                        {
                            MaintainPageState();
                        }
                        else
                        {
                            ClearSession(SessionConstants.PartnerSearch);
                        }
                        
                        //downloadPartnerRptGettingDeleted.Visible = false;
                        //DeletePartnerLnk.Visible = false;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(partnerUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                        ScriptManager.RegisterClientScriptBlock(partnerUpdatePanel, this.GetType(), "getItems", "getItems();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                partnerSearch.PageIndex = pagingControl.CurrentPageIndex;
                partnerSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                partnerSearch.AllClick = pagingControl.AllPageClick;
                SearchPartners(partnerSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                partnerSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchPartners(partnerSearch);
                ClearSession(SessionConstants.PartnerSearch);
                SetSession<PartnerSearch>(SessionConstants.PartnerSearch, partnerSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at PartnerListGridView RowCommand
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PartnerListGridView_RowCommand(object sender, CommandEventArgs args)
        {
            try
            {
                if (args != null && (args.CommandName == "EditPartner"))
                {
                    PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                    partnerSearch.PageSet = pagingControl.PageSet;
                    ClearSession(SessionConstants.PartnerSearch);
                    SetSession<PartnerSearch>(SessionConstants.PartnerSearch, partnerSearch);
                    Context.Items[UIConstants.Mode] = UIConstants.Edit;
                    Context.Items[UIConstants.PartnerId] = args.CommandArgument.ToString();
                    Server.Transfer("~/Admin/PartnerDetails.aspx", true);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PartnerListGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PartnerListGridView_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton editLinkButton = args.Row.FindControl("editLinkButton") as LinkButton;
                    if (editLinkButton != null)
                    {
                        ScriptManager.GetCurrent(this).RegisterPostBackControl(editLinkButton);
                    }

                    if (args.Row.Cells[3].Text == "&nbsp;")
                    {
                        args.Row.Cells[3].Text = "-";
                    }

                    if (args.Row.Cells[4].Text == "&nbsp;")
                    {
                        args.Row.Cells[4].Text = "-";
                    }

                    System.Web.UI.WebControls.CheckBox chkDelPartner = (args.Row.FindControl("chkDelPartner") as System.Web.UI.WebControls.CheckBox);
                    chkDelPartner.ID = "chkDelPartner_" + ((Hilton.SuppliersConnection.Entities.Partner)(args.Row.DataItem)).PartnerId;
                    if (((Hilton.SuppliersConnection.Entities.Partner)(args.Row.DataItem)).flagDelPartner == "delete" && statusDropDown.SelectedValue == UIConstants.StatusPermanantlyInactive && flagSearch != "QuickSearch")
                    {
                        chkDelPartner.Visible = true;
                        countChecked = countChecked + 1;

                    }
                    else
                    {
                        chkDelPartner.Visible = false;

                    }
                }
                if (countChecked != 0)
                {
                    downloadPartnerReportLinkButton.Style.Add("display", "none");
                    downloadPartnerRptGettingDeleted.Style.Add("display", "block");
                    DeletePartnerLnk.Style.Add("display", "none");
                }
                else
                {
                    downloadPartnerReportLinkButton.Style.Add("display", "block");
                    downloadPartnerRptGettingDeleted.Style.Add("display", "none");
                    DeletePartnerLnk.Style.Add("display", "none");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PartnerListGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Populates Category Data
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="level"></param>
        /// <param name="categoryDropDown"></param>
        private void PopulateCategoryData(IList<Category> categories, int level, DropDownList categoryDropDown)
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            IList<Category> accessibleCategoryList = FilterCategoryAccess(categories.Where(item => item.Level.Equals(level)).ToList(), currentUser.AccessibleCategoryList);
            BindDropDown(accessibleCategoryList, categoryDropDown, ResourceUtility.GetLocalizedString(1211, culture, "All Categories"));
        }


        /// <summary>
        /// Filters Category Access
        /// </summary>
        /// <param name="sourceCategoryCollection"></param>
        /// <param name="accessibleCategoryCollection"></param>
        /// <returns></returns>
        private IList<Category> FilterCategoryAccess(IList<Category> sourceCategoryCollection, IList<Category> accessibleCategoryCollection)
        {
            try
            {
                IList<Category> resultCategoryCollection = null;
                if (accessibleCategoryCollection.Count > 0)
                {
                    resultCategoryCollection = (IList<Category>)(sourceCategoryCollection.Join(
                                                           accessibleCategoryCollection,
                                                           sourceCategory => sourceCategory.CategoryId,
                                                           accessibleCategory => accessibleCategory.CategoryId, (sourceCategory, accessibleCategory) => sourceCategory)).ToList();
                }
                else
                    resultCategoryCollection = (IList<Category>)sourceCategoryCollection;
                return resultCategoryCollection;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }


        /// <summary>
        /// This method will populate the drop down data
        /// </summary>
        protected void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1201, culture, "All Regions"), 0);
            BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1301, culture, "All Countries"), 0);
            BindStaticDropDown(DropDownConstants.PartnershipStatusDropDown, statusDropDown, ResourceUtility.GetLocalizedString(1226, culture, "All Status"), null, false);
            BindStaticDropDown(DropDownConstants.PartnershipTypeDropDown, partnershipTypeDropDown, ResourceUtility.GetLocalizedString(0, culture, "All Partnerships"), null);

            var brandId = GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            brandCollection.Value = string.Join(",", brandId.ToList().Select(p => p.DataValueField));

            IList<Category> categories = HelperManager.Instance.GetActiveCategories;
            int level = 1; // for root level categories
            SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categories);
            subcategoryDropDown.Enabled = false;
            tertiarycategoryDropDown.Enabled = false;
            PopulateCategoryData(categories, level, categoryDropDown);
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                partnerSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchPartners(partnerSearch);
                ClearSession(SessionConstants.PartnerSearch);
                SetSession<PartnerSearch>(SessionConstants.PartnerSearch, partnerSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


        /// <summary>
        /// This method is invoked with search button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void QuickSearch_Click(object sender, EventArgs args)
        {
            try
            {
                PartnerSearch partnerSearch = BuildSearchCriteria();
                partnerSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                GetPartnerList(partnerSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at RegionDropDown SelectedIndex Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //To reset the country already selected
                countryDropDown.SelectedIndex = -1;
                string[] regionSelected = { regionDropDown.SelectedValue };
                BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1301, culture, "All Countries"), null, true, regionSelected);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Validates Expiration Date
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidateExpirationDate(object source, ServerValidateEventArgs args)
        {
            try
            {
                ValidateDate(args, validateExpirationDateCustomValidator, expirationDateFromTextBox.Text, expirationDateToTextBox.Text);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Validates Request Date
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidateRequestDate(object source, ServerValidateEventArgs args)
        {
            try
            {
                ValidateDate(args, validateRequestDateCustomValidator, requestSubmittedDateFromTextBox.Text, requestSubmittedDateToTextBox.Text);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Binds Partner List
        /// </summary>
        /// <param name="userList"></param>
        private void BindPartnerList(IList<Partner> partnerList)
        {
            partnerListGridView.DataSource = partnerList;
            partnerListGridView.DataBind();
        }

        /// <summary>
        /// This method will populate the search criteria
        /// </summary>
        /// <returns></returns>
        private PartnerSearch BuildSearchCriteria()
        {
            DateTime dateTime;
            User currentUser = (User)GetSession<User>(SessionConstants.User);
            PartnerSearch partnerSearch = new PartnerSearch()
            {
                UserId = currentUser.UserId,
                QuickSearchExpression = searchTextBox.Text.Trim(),
                CategoryId = (categoryDropDown.SelectedIndex == 0) ? string.Empty : categoryDropDown.SelectedValue,
                SubCategoryId = (subcategoryDropDown.SelectedIndex == 0) ? string.Empty : subcategoryDropDown.SelectedValue,
                TertioryCategoryId = (tertiarycategoryDropDown.SelectedIndex == 0) ? string.Empty : tertiarycategoryDropDown.SelectedValue,
                StatusId = (statusDropDown.SelectedIndex == 0) ? string.Empty : statusDropDown.SelectedValue,
                PartnershipTypeId = (partnershipTypeDropDown.SelectedIndex == 0) ? string.Empty : partnershipTypeDropDown.SelectedValue,
                IsSAMPartner = (SAMPartnerDropDown.SelectedIndex == 0) ? string.Empty : SAMPartnerDropDown.SelectedValue,
                BrandId = selectedBrands.Text,
                BrandCollection = brandCollection.Value,
                RegionId = (regionDropDown.SelectedIndex == 0) ? string.Empty : regionDropDown.SelectedValue,
                CountryId = (countryDropDown.SelectedIndex == 0) ? string.Empty : countryDropDown.SelectedValue,
                PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture),
                PageIndex = 0,
            };

            if (DateTime.TryParse(requestSubmittedDateToTextBox.Text, out dateTime))
            {
                partnerSearch.RequestDateTo = Convert.ToDateTime(requestSubmittedDateToTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }
            if (DateTime.TryParse(requestSubmittedDateFromTextBox.Text, out dateTime))
            {
                partnerSearch.RequestDateFrom = Convert.ToDateTime(requestSubmittedDateFromTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }
            if (DateTime.TryParse(expirationDateFromTextBox.Text, out dateTime))
            {
                partnerSearch.ExpirationDateFrom = Convert.ToDateTime(expirationDateFromTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }
            if (DateTime.TryParse(expirationDateToTextBox.Text, out dateTime))
            {
                partnerSearch.ExpirationDateTo = Convert.ToDateTime(expirationDateToTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }

            return partnerSearch;
        }

        /// <summary>
        /// Gets Partner List
        /// </summary>
        /// <param name="partnerSearch"></param>
        private void GetPartnerList(PartnerSearch partnerSearch)
        {
            partnerSearch.SortExpression = UIConstants.CompanyName; ;
            partnerSearch.SortDirection = UIConstants.AscAbbreviation;
            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
            SetViewState(ViewStateConstants.SortExpression, UIConstants.CompanyName);
            SearchPartners(partnerSearch);

            //Pagination Control Setting
            pagingControl.CurrentPageIndex = 0;
            pagingControl.PageSet = 0;
            pagingControl.AllPageClick = false;
        }

        /// <summary>
        /// This method will return the sort colum index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in partnerListGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return partnerListGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("Partners.aspx") || Request.UrlReferrer.ToString().Contains("PartnerDetails.aspx")))
                    {
                        PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
                        if (field.SortExpression == partnerSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, partnerSearch.SortExpression);
                            if (partnerSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return partnerListGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// This method will maintain page state when user redirect from user  details screen
        /// </summary>
        private void MaintainPageState()
        {
            PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);

            if (partnerSearch != null)
            {
                PolulateSearchCriteria(partnerSearch);

                pagingControl.PageSet = partnerSearch.PageSet;

                SearchPartners(partnerSearch);
            }
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        private void PolulateSearchCriteria(PartnerSearch partnerSearch)
        {
            selectedBrands.Text = partnerSearch.BrandId;
            brandCollection.Value = partnerSearch.BrandCollection;

            if (!string.IsNullOrEmpty(partnerSearch.StatusId))
                statusDropDown.SelectedValue = partnerSearch.StatusId;

            if (!string.IsNullOrEmpty(partnerSearch.CategoryId))
            {
                categoryDropDown.SelectedValue = partnerSearch.CategoryId;
                SetSubCategoryAccess();
            }

            if (!string.IsNullOrEmpty(partnerSearch.SubCategoryId))
            {
                subcategoryDropDown.SelectedValue = partnerSearch.SubCategoryId;
                SetTerCategoryAccess();
            }


            if (!string.IsNullOrEmpty(partnerSearch.TertioryCategoryId))
                tertiarycategoryDropDown.SelectedValue = partnerSearch.TertioryCategoryId;

            if (!string.IsNullOrEmpty(partnerSearch.RegionId))
            {
                regionDropDown.SelectedValue = partnerSearch.RegionId;
            }


            if (!string.IsNullOrEmpty(partnerSearch.QuickSearchExpression))
                searchTextBox.Text = partnerSearch.QuickSearchExpression;

            if (partnerSearch.RequestDateFrom.HasValue)
            {
                requestSubmittedDateFromTextBox.Text = ((DateTime)partnerSearch.RequestDateFrom).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (partnerSearch.RequestDateTo.HasValue)
            {
                requestSubmittedDateToTextBox.Text = ((DateTime)partnerSearch.RequestDateTo).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (partnerSearch.ExpirationDateTo.HasValue)
            {
                expirationDateToTextBox.Text = ((DateTime)partnerSearch.ExpirationDateTo).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (partnerSearch.ExpirationDateFrom.HasValue)
            {
                expirationDateFromTextBox.Text = ((DateTime)partnerSearch.ExpirationDateFrom).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(partnerSearch.IsSAMPartner))
                SAMPartnerDropDown.SelectedValue = partnerSearch.IsSAMPartner;

            if (!string.IsNullOrEmpty(partnerSearch.PartnershipTypeId))
                partnershipTypeDropDown.SelectedValue = partnerSearch.PartnershipTypeId;



        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="partnerSearch"></param>
        private void SearchPartners(PartnerSearch partnerSearch)
        {
            int totalRecordCount = 0;
            if (partnerSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                partnerSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                partnerSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (partnerSearch.PageIndex == -1)
                {
                    partnerSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = partnerSearch.PageIndex;
            }

            IList<Partner> partnerList = _partnerManager.GetPartnerList(partnerSearch, ref totalRecordCount);
            if (partnerList.Count == 0)
            {
                searchResultDiv.Attributes.Add("class", "message notification");
                searchResultDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoPartnersFoundForCriteria, culture);
            }

            partnerSearch.TotalRecordCount = totalRecordCount;

            //Save criteria in session for back button functionality
            ClearSession(SessionConstants.PartnerSearch);
            SetSession<PartnerSearch>(SessionConstants.PartnerSearch, partnerSearch);

            flagSearch = partnerSearch.SearchFlag;
            BindPartnerList(partnerList);

            //Pagination Control Setting
            pagingControl.TotalRecordCount = totalRecordCount;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Download Partner report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void DownloadSpreadSheet_ClickLinkButton(object sender, EventArgs args)
        {
            PartnerSearch partnerSearch = BuildSearchCriteria();
            DataSet partnerReportDataSet = null;
            try
            {
                Workbook partnerExportReport = new Workbook();

                partnerReportDataSet = _partnerManager.GetPartnerExportReportDataSet(partnerSearch);

                for (int index = 0; index < partnerReportDataSet.Tables[0].Rows.Count; index++)
                {

                    string[] categories = Regex.Split(partnerReportDataSet.Tables[0].Rows[index]["Category"].ToString(), "::");
                    if (categories.Length > 0)
                    {
                        partnerReportDataSet.Tables[0].Rows[index]["Category"] = categories[0];
                        if (categories.Length > 1)
                        {
                            partnerReportDataSet.Tables[0].Rows[index]["Sub-Category"] = categories[1];
                            if (categories.Length > 2)
                            {
                                partnerReportDataSet.Tables[0].Rows[index]["Tertiary Category"] = categories[2];

                            }
                        }

                    }

                }

                for (int index = 0; index < partnerReportDataSet.Tables[2].Rows.Count; index++)
                {

                    string[] categories = Regex.Split(partnerReportDataSet.Tables[2].Rows[index]["Category"].ToString(), "::");
                    if (categories.Length > 0)
                    {
                        partnerReportDataSet.Tables[2].Rows[index]["Category"] = categories[0];
                        if (categories.Length > 1)
                        {
                            partnerReportDataSet.Tables[2].Rows[index]["Sub-Category"] = categories[1];
                        }
                        if (categories.Length > 2)
                        {
                            partnerReportDataSet.Tables[2].Rows[index]["Tertiary Category"] = categories[2];

                        }
                    }

                }

                License license = new License();
                license.SetLicense(Server.MapPath("~/bin/Aspose.Custom.lic"));
                partnerExportReport.Open(Server.MapPath("~/Excel/PartnerExport.xls"));

                Aspose.Cells.Cell currentDateForSummary = partnerExportReport.Worksheets[0].Cells["A1"];
                Aspose.Cells.Cell currentDateForContacts = partnerExportReport.Worksheets[1].Cells["A1"];
                Aspose.Cells.Cell currentDateForProductDetails = partnerExportReport.Worksheets[2].Cells["A1"];

                String currentDate = String.Format("{0: MM-dd-yyyy}", DateTime.Now);

                partnerExportReport.Worksheets[0].Cells.ImportDataTable(partnerReportDataSet.Tables[0], false, "A3");
                partnerExportReport.Worksheets[0].AutoFitColumn(0);
                partnerExportReport.Worksheets[0].AutoFitColumns();
                currentDateForSummary.PutValue("Suppliers Connection Partner Export - SUMMARY" + " " + currentDate);

                partnerExportReport.Worksheets[1].Cells.ImportDataTable(partnerReportDataSet.Tables[1], false, "A3");
                partnerExportReport.Worksheets[1].AutoFitColumn(0);
                partnerExportReport.Worksheets[1].AutoFitColumns();
                currentDateForContacts.PutValue("Suppliers Connection Partner Export - CONTACTS" + " " + currentDate);

                partnerExportReport.Worksheets[2].Cells.ImportDataTable(partnerReportDataSet.Tables[2], false, "A3");
                partnerExportReport.Worksheets[2].AutoFitColumn(0);
                partnerExportReport.Worksheets[2].AutoFitColumns();
                currentDateForProductDetails.PutValue("Suppliers Connection Partner Export - DETAILS" + " " + currentDate);

                partnerExportReport.Save("PartnerExport.xls", Aspose.Cells.SaveType.OpenInExcel, Aspose.Cells.FileFormatType.Default, Response);

            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        protected void downloadPartnerRptGettingDeleted_Clicked(object sender, EventArgs args)
        {
           try
            {
            string delPartnersList = "";

            delPartnersList = hidDelPartnerIds.Value;

                PartnerSearch partnerSearch = BuildSearchCriteria();
                DataSet partnerReportDataSet = null;

                Workbook partnerExportReport = new Workbook();

                partnerReportDataSet = _partnerManager.GetPartnerExportRptDataSetDelPartners(partnerSearch, delPartnersList);

                for (int index = 0; index < partnerReportDataSet.Tables[0].Rows.Count; index++)
                {

                    string[] categories = Regex.Split(partnerReportDataSet.Tables[0].Rows[index]["Category"].ToString(), "::");
                    if (categories.Length > 0)
                    {
                        partnerReportDataSet.Tables[0].Rows[index]["Category"] = categories[0];
                        if (categories.Length > 1)
                        {
                            partnerReportDataSet.Tables[0].Rows[index]["Sub-Category"] = categories[1];
                            if (categories.Length > 2)
                            {
                                partnerReportDataSet.Tables[0].Rows[index]["Tertiary Category"] = categories[2];

                            }
                        }

                    }

                }

                for (int index = 0; index < partnerReportDataSet.Tables[2].Rows.Count; index++)
                {

                    string[] categories = Regex.Split(partnerReportDataSet.Tables[2].Rows[index]["Category"].ToString(), "::");
                    if (categories.Length > 0)
                    {
                        partnerReportDataSet.Tables[2].Rows[index]["Category"] = categories[0];
                        if (categories.Length > 1)
                        {
                            partnerReportDataSet.Tables[2].Rows[index]["Sub-Category"] = categories[1];
                        }
                        if (categories.Length > 2)
                        {
                            partnerReportDataSet.Tables[2].Rows[index]["Tertiary Category"] = categories[2];

                        }
                    }

                }

                License license = new License();
                license.SetLicense(Server.MapPath("~/bin/Aspose.Custom.lic"));
                partnerExportReport.Open(Server.MapPath("~/Excel/PartnerExport.xls"));

                Aspose.Cells.Cell currentDateForSummary = partnerExportReport.Worksheets[0].Cells["A1"];
                Aspose.Cells.Cell currentDateForContacts = partnerExportReport.Worksheets[1].Cells["A1"];
                Aspose.Cells.Cell currentDateForProductDetails = partnerExportReport.Worksheets[2].Cells["A1"];

                String currentDate = String.Format("{0: MM-dd-yyyy}", DateTime.Now);

                partnerExportReport.Worksheets[0].Cells.ImportDataTable(partnerReportDataSet.Tables[0], false, "A3");
                partnerExportReport.Worksheets[0].AutoFitColumn(0);
                partnerExportReport.Worksheets[0].AutoFitColumns();
                currentDateForSummary.PutValue("Suppliers Connection Partner Export - SUMMARY" + " " + currentDate);

                partnerExportReport.Worksheets[1].Cells.ImportDataTable(partnerReportDataSet.Tables[1], false, "A3");
                partnerExportReport.Worksheets[1].AutoFitColumn(0);
                partnerExportReport.Worksheets[1].AutoFitColumns();
                currentDateForContacts.PutValue("Suppliers Connection Partner Export - CONTACTS" + " " + currentDate);

                partnerExportReport.Worksheets[2].Cells.ImportDataTable(partnerReportDataSet.Tables[2], false, "A3");
                partnerExportReport.Worksheets[2].AutoFitColumn(0);
                partnerExportReport.Worksheets[2].AutoFitColumns();
                currentDateForProductDetails.PutValue("Suppliers Connection Partner Export - DETAILS" + " " + currentDate);

                partnerExportReport.Save("PartnerExport.xls", Aspose.Cells.SaveType.OpenInExcel, Aspose.Cells.FileFormatType.Default, Response);

                downloadPartnerReportLinkButton.Style.Add("display", "none");
                downloadPartnerRptGettingDeleted.Style.Add("display", "none");
                DeletePartnerLnk.Style.Add("display", "block");     
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                downloadPartnerReportLinkButton.Style.Add("display", "none");
                downloadPartnerRptGettingDeleted.Style.Add("display", "block");
                DeletePartnerLnk.Style.Add("display", "none");
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

       

        [System.Web.Services.WebMethod]
        public static string DeletePartners(string partnerIds)
        {
              IPartnerManager _partnerManager = PartnerManager.Instance;
              Hilton.SuppliersConnection.UI.BaseClasses.PageBase ObjPageBase = new Hilton.SuppliersConnection.UI.BaseClasses.PageBase();
              User currentUser = (User)ObjPageBase.GetSession<User>(SessionConstants.User);
              int UserId = currentUser.UserId;
              int resultid = _partnerManager.DeletePartners(partnerIds, UserId);
              if (resultid == 0)
                { return "success"; }
              else
                { return "error"; }
        }


        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            PartnerSearch partnerSearch = GetSession<PartnerSearch>(SessionConstants.PartnerSearch);
            partnerSearch.SortExpression = sortExpression;
            partnerSearch.SortDirection = sortDirection;
            partnerSearch.PageIndex = 0;
            SearchPartners(partnerSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }
    }
}