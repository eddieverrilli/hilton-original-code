﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ViewStateMode="Inherit" AutoEventWireup="True" CodeBehind="Projects.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Projects" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
<script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script type="text/javascript">
        function InitializeToggle() {
            var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleStateValue == "0") {
                document.getElementById('toggleDiv').style.display = "block";
            }
            else {
                $('.toggle-divider a')[0].removeAttribute('class');
                document.getElementById('toggleDiv').style.display = "none";
            }

            $('.col-6 tr, .col-7 tr').hover(function () {

                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            });

        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }

        $(document).ready(function () {
            getItems();
        });

    </script>
</asp:Content>
<asp:Content ID="content1" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="partnerUpdatePanel" runat="server" UpdateMode="Always">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="selectedBrands" EventName="TextChanged" />
    </Triggers>
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="quickSearchLinkButton" CBID="648" Text="Search" runat="server"
                                        OnClick="QuickSearch_Click" ValidationGroup="SearchProjects" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchProjects"
                                    ControlToValidate="searchTextBox" Display="Dynamic" ErrorMessage="Please specify search criteria"
                                    VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server" /></div>
                        </div>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                            <div class="field">
                                <asp:Label ID="projectTypeLabel" runat="server" CssClass="label" CBID="802" Text="Project Type :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="projectTypeDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="ownerLabel" runat="server" CssClass="label" CBID="803" Text="Owner :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="ownerDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="regionLabel" runat="server" CssClass="label" CBID="804" Text="Region :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="field">
                                <asp:Label ID="countrylabel" runat="server" CssClass="label" CBID="1300" Text="Country :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="brandLabel" runat="server" CssClass="label" CBID="805" Text="Brand :"></asp:Label>
                                <div class="textbox-2">
                                     <asp:DropDownList runat="server" ID="optgroup"  ViewStateMode="Enabled" ClientIDMode="Static"  multiple="multiple">         
                                     </asp:DropDownList>
                                     <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                                     <asp:HiddenField ID="isCheckAll" runat="server" ClientIDMode="Static" />
                                     <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display:none;" AutoPostBack="true" OnTextChanged="SelectedBrands_TextChanged"></asp:TextBox>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="o2OStatusLabel1" runat="server" CssClass="label" CBID="806" Text="O2O Status :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="o2oStatusDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="PMLabel" runat="server" CssClass="label" CBID="807" Text="PM :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="pmDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="visibility:hidden"  >
                                <asp:Label ID="propertyTypeLabel" runat="server" Visible="false" CssClass="label" CBID="808" Text="Property Type :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="propertyTypeDropDown" Visible="false" ViewStateMode="Enabled" OnSelectedIndexChanged="PropertyTypeDropDown_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field" >
                                <asp:Label ID="SCStatusLabel" runat="server" CssClass="label" CBID="809" Text="SC Status :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="scStatusDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <div class="alR searchpad-top">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="btnGo" Text="Go" runat="server" CBID="402"  OnClick="GoButton_click" />
                                    </span>
                                </div>
                            </div>
                            <div class="clear">
                                &nbsp;</div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <h2>
                        <asp:Label ID="projectsLabel" runat="server" CBID="557" Text="Projects"></asp:Label></h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                        <div class="hscroll">
                            <asp:GridView ID="projectListGridView" runat="server" ViewStateMode="Enabled" AllowSorting="true"
                                AutoGenerateColumns="False" class="col-6" DataKeyNames="PMName" OnRowCommand="ProjectListGridView_RowCommand"
                                OnRowCreated="ProjectListGridView_RowCreated" OnRowDataBound="ProjectListGridView_RowDataBound"
                                OnSorting="ProjectListGridView_Sorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Fac. ID" ItemStyle-Width="5%" SortExpression="FacilityId">
                                        <ItemTemplate>
                                            <asp:Label ID="facilityIdLabel" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Proj. Type" DataField="ProjectTypeDescription" SortExpression="ProjectTypeDescription"
                                        HeaderStyle-Width="10%" />
                                    <asp:TemplateField HeaderText="Brand" ItemStyle-Width="9%" SortExpression="BrandDescription">
                                        <ItemTemplate>
                                            <asp:Label ID="brandLabel" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BrandSegment" ItemStyle-Width="9%" SortExpression="BrandSegment">
                                        <ItemTemplate>
                                            <asp:Label ID="brandSegmentLabel" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prop. Type" Visible="false" ItemStyle-Width="9%" SortExpression="PropertyTypeDescription">
                                        <ItemTemplate>
                                            <asp:Label ID="propertyTypeDescriptionLabel" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Region" ItemStyle-Width="9%" SortExpression="RegionDescription">
                                        <ItemTemplate>
                                            <asp:Label ID="regionDescriptionLabel" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Country" ItemStyle-Width="9%" SortExpression="CountryDescription">
                                        <ItemTemplate>
                                            <asp:Label ID="countryDescriptionLabel" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Facility Name" ItemStyle-Width="9%" SortExpression="FacilityName">
                                        <ItemTemplate>
                                            <asp:Label ID="facilityNameLabel" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Owner" DataField="OwnerName" Visible="false" SortExpression="Owner"
                                        HeaderStyle-Width="10%" />
                                    <asp:BoundField HeaderText="PM" DataField="PMName" HeaderStyle-Width="9%" SortExpression="PM" />
                                    <asp:BoundField HeaderText="Config %" DataField="ConfigPercent" SortExpression="ConfigPercent"
                                        HeaderStyle-Width="4%" />
                                    <asp:BoundField HeaderText="Other" DataField="Excp" SortExpression="Excp" HeaderStyle-Width="4%" />
                                    <asp:BoundField HeaderText="O2O Status" DataField="O2OStatusDescription" SortExpression="O2OStatusDescription"
                                        HeaderStyle-Width="4%" />
                                    <asp:BoundField HeaderText="Status" DataField="SCStatusDescription" SortExpression="SCStatusDescription"
                                        HeaderStyle-Width="4%" />
                                    <asp:TemplateField ShowHeader="False" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="detailsLinkButton" runat="server" CommandName="ProjectDetails"
                                                CommandArgument='<%# Bind("ProjectId") %>' Text="details"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                                </div>
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>