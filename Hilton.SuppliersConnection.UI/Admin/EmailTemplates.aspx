﻿<%@ Page Title="Content Blocks" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="EmailTemplates.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.EmailTemplates" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc" %>
<asp:Content ID="contentDetails" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="dropDown">
        <div class="wrapper">
            <div class="rgt-bg">
                <ul>
                    <li>
                        <asp:HyperLink ID="generalSettingsHyperlink" runat="server" NavigateUrl="~/Admin/Settings.aspx">General Settings</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="contentBlockHyperLink" runat="server" NavigateUrl="~/Admin/ContentBlocks.aspx">Content Blocks</asp:HyperLink></li>
                    <li>
                        <asp:Label ID="emailTemplateSubMenuLabel" runat="server" CssClass="active" Text="Email Templates"></asp:Label>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="resultMessageDiv" runat="server">
    </div>
    <asp:UpdatePanel ID="emailTemplateUpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="content">
                <div class="wrapper">
                    <asp:LinkButton ID="addEmailTemplateLinkButton" runat="server" OnClick="AddEmailTemplateLinkButton_Click"
                        CssClass="btn"><span>+</span> Add Email Template </asp:LinkButton>
                    <h2>
                        <asp:Label ID="emailTemplatesLabel" runat="server" Text="Email Templates"></asp:Label></h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <asp:GridView ID="emailTemplateListGridView" runat="server" AutoGenerateColumns="False"
                                OnRowCommand="EmailTemplateListGridView_RowCommand" OnRowCreated="EmailTemplateListGridView_RowCreated"
                                OnRowDataBound="EmailTemplateListGridView_RowDataBound" OnSorting="EmailTemplateListGridView_Sorting"
                                AllowSorting="true" class="col-6">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="EmailTemplateId" SortExpression="EmailTemplateId"
                                        HeaderStyle-Width="10%" />
                                    <asp:BoundField HeaderText="Template Name" DataField="EmailTemplateName" SortExpression="EmailTemplateName"
                                        HeaderStyle-Width="30%" />
                                    <asp:BoundField HeaderText="Description" DataField="Description" SortExpression="Description"
                                        HeaderStyle-Width="58%" />
                                    <asp:TemplateField HeaderText="" ShowHeader="False" ItemStyle-Width="2%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="editLinkButton" runat="server" CausesValidation="False" CommandName="EditEmailTemplate"
                                                CommandArgument='<%# Bind("EmailTemplateId") %>' Text="edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>