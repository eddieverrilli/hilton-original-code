﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ViewStateMode="Inherit" AutoEventWireup="True" CodeBehind="Feedbacks.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Admin.Feedbacks" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc1" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../Scripts/thickbox.js"></script>
    <script type="text/javascript">

        function closeIframe() {
            $('.close').click();
        }

        function forceCall() {
            tb_init('a.thickbox');
            imgLoader = new Image();
            imgLoader.src = tb_pathToImage;
        }

       
        

        $(document).ready(function () {
            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });
        });

        function InitializeToggle() {
            if (document.getElementById('<%=toggleStateHiddenField.ClientID %>')) {
                var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
                if (toggleStateValue == "0") {
                    document.getElementById('toggleDiv').style.display = "block";
                }
                else {
                    $('.toggle-divider a')[0].removeAttribute('class');
                    document.getElementById('toggleDiv').style.display = "none";
                }
            }
            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            })

            $('.col-6 tr, .col-7 tr').hover(function () {

                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.arrow').click(function () {

                $(this).toggleClass('active')
                $(this).parent().parent().next().children('.description').children().toggle();
            })

            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });

        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
     <a id="TB_closeWindowButton" class="close" style="position: absolute; z-index: 1000;
        left: -1000em">close</a>
    <asp:UpdatePanel ID="feedbacksUpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="searchButton" runat="server" OnClick="QuickSearch_Click" ValidationGroup="SearchFeedback"
                                        Text="Search" CBID="653" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchFeedback"
                                    ControlToValidate="searchTextBox" Display="Dynamic" ErrorMessage="Please specify search criteria"
                                    VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                            <div class="field">
                                <asp:Label ID="fromDateLabel" runat="server" class="label" Text="Date:" CBID="352"></asp:Label>
                                <div class="textbox-1">
                                    <asp:TextBox ID="fromDateTextBox" class="datepicker" runat="server" />
                                </div>
                                <asp:Label ID="toDateLabel" runat="server" class="label" Text="To:" CBID="722"></asp:Label>
                                <div class="textbox-1">
                                    <asp:TextBox ID="toDateTextBox" class="datepicker" runat="server" />
                                </div>
                                <div class="error-message pad-top10">
                                    <asp:CustomValidator ID="validateDateCustomValidator" CssClass="errorText" OnServerValidate="ValidDate"
                                        runat="server" />
                                </div>
                            </div>
                            <div class="clear">
                                &nbsp;
                            </div>
                            <div class="feedback-list-section">
                                <div class="field">
                                    <asp:Label ID="partnerLabel" runat="server" class="label" Text="Partner:" CBID="487"></asp:Label>
                                    <div class="textbox-2">
                                        <asp:DropDownList ID="partnerDropDown" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="field">
                                    <asp:Label ID="ratingLabel" runat="server" class="label" Text="Rating:" CBID="580"></asp:Label>
                                    <div class="textbox-2">
                                        <asp:DropDownList ID="ratingDropDown" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="field">
                                    <asp:Label ID="statusLabel" runat="server" class="label" Text="Status:" CBID="152"></asp:Label>
                                    <div class="textbox-2">
                                        <asp:DropDownList ID="statusDropDown" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="alR searchpad-top">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="goButton" Text="Go" CBID="404" runat="server" OnClick="GoButton_Click" />
                                    </span>
                                </div>
                            </div>
                            <div class="clear">
                                &nbsp;</div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <asp:LinkButton ID="upateAllLinkButton" runat="server" CBID="910" Visible="false"
                        class="btn" OnClick="UpdateAllLinkButton_Click" Text="Save"></asp:LinkButton>
                    <asp:LinkButton ID="exportCSVLinkButton" runat="server" class="btn" OnClick="ExportCSVLinkButton_Click"
                        Text="Export Excel" CBID="394"></asp:LinkButton>
                    <h2>
                        <asp:Label ID="feedbackSearchResultsLabel" runat="server" class="label" Text="Feedbacks"
                            CBID="150"></asp:Label></h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <asp:Repeater ID="feedbackListRepeater" runat="server" ViewStateMode="Enabled" OnItemDataBound="FeedbackListRepeater_ItemDataBound"
                                OnItemCreated="FeedbackListRepeater_ItemCreated" OnItemCommand="FeedbackListRepeater_ItemCommand">
                                <HeaderTemplate>
                                    <table cellspacing="0" rules="all" border="1" style="border-collapse: collapse;"
                                        class="col-7">
                                        <tr>
                                            <th class="col-one">
                                            </th>
                                            <th class="col-two">
                                                <asp:LinkButton ID="partnerLinkButton" CBID="487" runat="server" Text="Partner" CommandName="CompanyName" />
                                            </th>
                                            <th class="col-three">
                                                <asp:LinkButton ID="productLinkButton" runat="server" Text="Product" CommandName="ProductName"
                                                    CBID="525" />
                                            </th>
                                            <th class="col-four">
                                                <asp:LinkButton ID="ratingRatingLinkButton" runat="server" Text="Rating" CBID="580"
                                                    CommandName="RaitingName" />
                                            </th>
                                            <th class="col-five">
                                                <asp:LinkButton ID="fromLinkButton" runat="server" Text="From" CommandName="From"
                                                    CBID="158" />
                                            </th>
                                            <th class="col-six">
                                                <asp:LinkButton ID="dateSubmittedLinkButton" runat="server" Text="Date Submitted"
                                                    CBID="356" CommandName="SubmittedDate" />
                                            </th>
                                            <th class="col-seven">
                                                <asp:LinkButton ID="statusLinkButton" runat="server" Text="Status" CommandName="Status"
                                                    CBID="699" />
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="col-one">
                                            <span class="arrow"></span>
                                        </td>
                                        <td class="col-two">
                                            <%# DataBinder.Eval(Container.DataItem, "PartnerCompany")%>
                                        </td>
                                        <td class="col-three">
                                            <%# DataBinder.Eval(Container.DataItem, "ProductName")%>
                                        </td>
                                        <td class="col-four">
                                            <%# DataBinder.Eval(Container.DataItem, "RatingDescription")%>
                                        </td>
                                        <td class="col-five">
                                            <%# DataBinder.Eval(Container.DataItem, "From")%>
                                        </td>
                                        <td class="col-six">
                                            <%# DataBinder.Eval(Container.DataItem, "SubmittedDateTime", "{0:dd-MMM-yyyy}")%>
                                        </td>
                                        <td class="col-seven">
                                            <asp:DropDownList class="list-1" ID="feedbackStatusDropDown" runat="server">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="feedbackIdHiddenField" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "FeedbackId")%>' />
                                            <asp:HiddenField ID="feedbackStatusIdHiddenField" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "FeedbackStatusId")%>' />
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="updateLinkButton" runat="server" CausesValidation="False" CommandName="UpdateFeedbackStatus"
                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedbackId")%>' CBID="910"
                                                Text="save"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="description">
                                            <div style="padding:10px 0px 10px 40px;"  >
                                            <table style="border-style:none;float:right;width:100%;">
                                            <tr><td style="width:90%">
                                              <%# DataBinder.Eval(Container.DataItem, "Details")%>
                                              <br />
                                             
                                           </td><td style="width:10%">
                                              <asp:HyperLink ID="hprlnkComment" CssClass="thickbox" runat="server"> Add Comment</asp:HyperLink>
                                            </td></tr>
                                            <tr> <td style="padding:10px 10px 10px 60px;">
                                              <asp:Label  ForeColor ="#878787"  id ="commentsLabel" runat="server" ></asp:Label> 
                                           </td><td></td>
                                           </tr> </table>
                                               </div>
                                         
                                         </td>
                                                              
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <uc1:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>