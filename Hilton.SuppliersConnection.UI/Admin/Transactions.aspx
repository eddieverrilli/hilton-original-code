﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="Transactions.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.TransactionList" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });
        });

        function InitializeToggle() {
            if (document.getElementById('<%=toggleStateHiddenField.ClientID %>') != null) {
                var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
                if (toggleStateValue == "0") {
                    document.getElementById('toggleDiv').style.display = "block";
                }
                else {
                    $('.toggle-divider a')[0].removeAttribute('class');
                    document.getElementById('toggleDiv').style.display = "none";
                }
            }

            $('.col-6 tr, .col-7 tr').hover(function () { 

                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            });

            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });

        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    
    <asp:UpdatePanel ID="tranasctionsUpdatePanel" runat="server" UpdateMode="Always">
    <Triggers>
        <asp:PostBackTrigger ControlID="exportToExcelLinkButton" />
    </Triggers>
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="quickSearchButton" CBID="654" Text="Search" runat="server" OnClick="QuickSearch_Click"
                                        ValidationGroup="SearchTransactions" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchTransactions"
                                    ControlToValidate="searchTextBox" Display="Dynamic" ErrorMessage="Please specify search criteria"
                                    VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server" /></div>
                        </div>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content">
                            <div class="field">
                                <asp:Label ID="dateFromLabel" CssClass="label" CBID="810" runat="server" Text="Date From:"></asp:Label>
                                <div class="textbox-1">
                                    <asp:TextBox ID="fromDateTextBox" runat="server" class="datepicker" ValidationGroup="Date"
                                        CausesValidation="True"></asp:TextBox>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="toLabel" CssClass="label" CBID="811" runat="server" Text="To:"></asp:Label>
                                <div class="textbox-1">
                                    <asp:TextBox ID="toDateTextBox" runat="server" class="datepicker" ValidationGroup="Date"
                                        CausesValidation="True"></asp:TextBox>
                                </div>
                            </div>
                            <div class="error-message pad-top10">
                                <asp:CustomValidator ID="validateDateCustomValidator" runat="server" CssClass="errorText" />
                            </div>
                            <div class="clear">
                            </div>
                            <div class="field">
                                <asp:Label ID="partnerLabel" CssClass="label" CBID="812" runat="server" Text="Partner:"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="partnerDropDownList" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="partnerStatusLabel" CssClass="label" CBID="813" runat="server" Text="Partner Status:"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="partnershipStatusDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="transactionStatusLabel" CssClass="label" CBID="814" runat="server"
                                    Text="Transaction Status:"></asp:Label>
                                <div class="textbox-2 smallwidth">
                                    <asp:DropDownList ID="transactionStatusDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="partnerTypeLabel" CssClass="label" CBID="0" runat="server" Text="Partner Type:"></asp:Label>
                                <div class="textbox-2 smallwidth">
                                    <asp:DropDownList ID="partnerTypeDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <div class="alR searchpad-top">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="goButton" Text="Go" CausesValidation="True" CBID="815" runat="server"
                                            OnClick="GoButton_Click" />
                                    </span>
                                </div>
                            </div>
                            <div class="clear">
                                &nbsp;</div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                 <asp:LinkButton ID="exportToExcelLinkButton" runat="server" class="btn" 
                Text="Export to Excel" CBID="393" onclick="ExportToExcelLinkButton_Click"></asp:LinkButton>
                    <h2>
                        <asp:Label ID="transactionsLabel" runat="server" CBID="729" Text="Transactions"></asp:Label>
                    </h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <div class="detail-section" id="detailSectionDiv" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="col1">
                                            <asp:Label ID="fromLabel" CssClass="label" CBID="816" runat="server" Text="From:"></asp:Label>
                                        </td>
                                        <td class="col2">
                                            <asp:Label ID="dateLabel" runat="server"></asp:Label>
                                        </td>
                                        <td class="col3">
                                            <asp:Label ID="goldLabel" CssClass="label" CBID="0" runat="server" Text="Gold (USD):"></asp:Label>
                                        </td>
                                        <td class="col4">
                                            <asp:Label ID="goldValueLabel" runat="server"></asp:Label>
                                        </td>
                                        <td class="col5">
                                            <asp:Label ID="regularLabel" CssClass="label" CBID="819" runat="server" Text="Regular:"></asp:Label>
                                        </td>
                                        <td class="col6">
                                            <asp:Label ID="regularVAlueLabel" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col1">
                                            <asp:Label ID="totalTextLabel" CssClass="label" CBID="817" runat="server" Text="Total:"></asp:Label>
                                        </td>
                                        <td class="col2">
                                            <asp:Label ID="totalLabel" runat="server"></asp:Label>
                                        </td>
                                        <td class="col3">
                                        </td>
                                        <td class="col4">
                                            <asp:Label ID="goldDataLabel" runat="server"></asp:Label>
                                        </td>
                                        <td class="col5">
                                        </td>
                                        <td class="col6">
                                            <asp:Label ID="regularDataLabel" runat="Server"></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:GridView ID="transactionListGridView" runat="server" AutoGenerateColumns="false"
                                ViewStateMode="Enabled" CssClass="col-6" AllowSorting="true" OnRowDataBound="TransactionResultGridView_RowDataBound"
                                OnRowCommand="TransactionListGridView_RowCommand" OnRowCreated="TransactionListGridView_RowCreated"
                                OnSorting="TransactionListGridView_Sorting">
                                <Columns>
                                    <asp:BoundField HeaderText="Date/Time" DataField="TransactionDateTime" DataFormatString="{0:dd-MMM-yyyy, hh:mm tt}"
                                        SortExpression="TransactionDate" HeaderStyle-Width="13%" />
                                    <asp:BoundField HeaderText="Partner&nbsp;Company" DataField="PartnerCompany" SortExpression="CompanyName"
                                        HeaderStyle-Width="20%" />
                                    <asp:BoundField HeaderText="Contact&nbsp;Name" DataField="ContactName" SortExpression="PartnerFirstName"
                                        HeaderStyle-Width="14%" />
                                    <asp:BoundField HeaderText="Transaction&nbsp;ID" DataField="TransactionId" SortExpression="TransactionId"
                                        HeaderStyle-Width="20%" />
                                    <asp:BoundField HeaderText="P.&nbsp;Type" DataField="PartnerType" SortExpression="PartnershipTypeDesc"
                                        HeaderStyle-Width="7%" />
                                    <asp:TemplateField HeaderText="Amount" ItemStyle-Width="7%" SortExpression="AmountPaid">
                                        <ItemTemplate>
                                            <asp:Label ID="amountLabel" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%" SortExpression="TransactionStatus">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="statusDropDownList" runat="server" ViewStateMode="Enabled"
                                                CssClass="list-1">
                                            </asp:DropDownList>
                                            <asp:Literal ID="statusLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TransactionStatus") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="updateLinkButton" runat="server" CommandName="UpdateTransactionList"
                                                CBID="1046" CommandArgument='<%# Bind("PaymentRecordId") %>' Text="update"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="viewLinkButton" runat="server" CommandName="ViewTransactionList"
                                                CBID="826" Text="view"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>