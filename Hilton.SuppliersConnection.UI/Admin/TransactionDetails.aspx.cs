﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class TransactionDetails : PageBase
    {
        private ITransactionManager _transactionManager;

        /// <summary>
        /// Get the instance of the manager class
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            ValidateUserAccess((int)MenuEnum.Transactions);

            _transactionManager = TransactionManager.Instance;
        }

        /// <summary>
        /// Bind the data to the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    errorRowDiv.Visible = false;
                    if (!IsPostBack)
                    {
                        this.Title = ResourceUtility.GetLocalizedString(922, culture, "Transaction Details");

                        PopulateDropDownData();
                        int paymentRecordId = Convert.ToInt32(Context.Items[UIConstants.PaymentRecordId], CultureInfo.InvariantCulture);
                        var transactionDetails = _transactionManager.GetTransactionDetails(paymentRecordId);

                        SetHiddenFieldValue(paymentRecordIdHiddenField, paymentRecordId.ToString(CultureInfo.InvariantCulture));

                        PopulateTransactionDetails(transactionDetails);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// This method will populate the drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.TransactionStatusDropDown, statusDropDownList, null, null);
            ListItem statusListItem = statusDropDownList.Items.FindByText(TransactionStatusEnum.Error.ToString());
            if (statusListItem != null)
            {
                statusDropDownList.Items.Remove(statusListItem);
            }
        }

        /// <summary>
        /// Update the status of check type transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                string selectedStatusValue;
                if (statusDropDownList.Visible == true)
                {
                    selectedStatusValue = statusDropDownList.SelectedValue;
                }
                else
                {
                    selectedStatusValue = "";
                }
                User currentUser = GetSession<User>(SessionConstants.User);

                TranComment transComment = new TranComment()
                {
                    Comment = commentTextbox.Text.Trim(),
                    Userid = Convert.ToInt32(currentUser.UserId),                    
                };
                
                int paymentRecordId = Convert.ToInt32(paymentRecordIdHiddenField.Value, CultureInfo.InvariantCulture); //Convert.ToInt32(Session[UIConstants.PaymentRecordId], CultureInfo.InvariantCulture);
                //call the business method
                var result = _transactionManager.UpdateTransactionStatus(paymentRecordId, selectedStatusValue, transComment);
                
                if (Convert.ToInt32(result) > 0)
                {
                    ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.TransactionStatusUpdateSuccess, culture));
                    statusLabel.Text = selectedStatusValue;
                }
                else if (Convert.ToInt32(result) == 0)
                {
                    ConfigureResultMessage(resultMessageDiv, "message notification", "Please insert comment");
                    statusLabel.Text = selectedStatusValue;
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.TransactionStatusUpdateFailure, culture));
                }

                var transactionDetails = _transactionManager.GetTransactionDetails(Convert.ToInt32(paymentRecordIdHiddenField.Value));

                PopulateTransactionDetails(transactionDetails);
                PaymentInfoUpdatePanel.Update();
                resultUpdatePanel.Update();
                commentTextbox.Text = "";
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Populate the transaction details
        /// </summary>
        /// <param name="transactionDetails"></param>
        private void PopulateTransactionDetails(PaymentTransactionDetails transactionDetails)
        {
            //Details for partner section
            statusLabel.Text = transactionDetails.PartnerStatus;
            typeLabel.Text = transactionDetails.PartnerType;
            nameLabel.Text = transactionDetails.PartnerCompany;

            foreach (var contact in transactionDetails.Contacts)
            {
                if (contact.ContactTypeId == (int)ContactTypeEnum.Primary)
                {
                    firstNameContactInfoLabel.Text = contact.FirstName;
                    lastNameContactInfoLabel.Text = contact.LastName;
                    companyNameContactInfoLabel.Text = transactionDetails.CompanyName;
                    address1ContactInfoLabel.Text = transactionDetails.CompanyAddress1;
                    address2ContactInfoLabel.Text = transactionDetails.CompanyAddress2;
                    countryContactInfoLabel.Text = transactionDetails.Country;
                    cityContactInfoLabel.Text = transactionDetails.City;
                    stateContactInfoLabel.Text = transactionDetails.State;
                    zipContactInfoLabel.Text = transactionDetails.ZipCode;
                    phoneContactInfoLabel.Text = contact.Phone;
                    faxContactInfoLabel.Text = contact.Fax;
                }
                if (contact.ContactTypeId == (int)ContactTypeEnum.Billing)
                {
                    companyNameBillingInfoLabel.Text = transactionDetails.CompanyName;
                    firstNameBillingInfoLabel.Text = contact.FirstName;
                    lastNameBillingInfoLabel.Text = contact.LastName;
                    address1BillingInfoLabel.Text = transactionDetails.CompanyAddress1;
                    address2BillingInfoLabel.Text = transactionDetails.CompanyAddress2;
                    countryBillingInfoLabel.Text = transactionDetails.Country;
                    cityBillingInfoLabel.Text = transactionDetails.City;
                    stateBillingInfoLabel.Text = transactionDetails.State;
                    zipBillingInfoLabel.Text = transactionDetails.ZipCode;
                    phoneBillingInfoLabel.Text = contact.Phone;
                    faxBillingInfoLabel.Text = contact.Fax;
                }
            }
            transactionIDPaymentInfoLabel.Text = transactionDetails.TransactionId;

            //Details for Payment Info
            if (transactionDetails.TransactionId == "Check")
            {
                statusDropDownList.Items.FindByText(transactionDetails.TransactionStatus).Selected = true;
                statusLiteral.Visible = false;
                //saveButtonDiv.Visible = true;
                errorRowDiv.Visible = false;
            }
            else
            {
                statusLiteral.Visible = true;
                statusLiteral.Text = transactionDetails.TransactionStatus;
                statusDropDownList.Visible = false;
               // saveButtonDiv.Visible = false;
                if (transactionDetails.TransactionStatus == "Error")
                {
                    errorRowDiv.Visible = true;
                    errorMessageLabel.Text = transactionDetails.ErrorMessage;
                }
            }

            datePaymentInfoLabel.Text = transactionDetails.TransactionDateTime.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);

            if (transactionDetails.PaymentAmount.HasValue)
            {
                amountPaymentInfoLabel.Text = transactionDetails.PaymentAmount.Value.ToString("C", CultureInfo.CurrentCulture);
            }
            else
            {
                amountPaymentInfoLabel.Text = string.Empty;
            }
            gridviewCommentHistory.DataSource = transactionDetails.TransComment;
            gridviewCommentHistory.DataBind();
        }
    }
}