﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Settings : PageBase
    {
        private IGeneralSettingsManager _generalSettingsManager;

        /// <summary>
        /// On Selected Index Changed for Terms and Condition Drop down
        /// </summary
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TermsAndConditionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (termsAndConditionDropDown.SelectedValue == "0")
                {
                    saveTermsAndCondionDiv.Visible = true;
                    termAndConditionTextBox.Enabled = true;
                    termAndConditionTextBox.Text = string.Empty;
                }
                else
                {
                    saveTermsAndCondionDiv.Visible = false;
                    termAndConditionTextBox.Enabled = false;
                    IList<Tuple<string, string>> TermsAndConditions = GetViewState<IList<Tuple<string, string>>>(ViewStateConstants.TermsAndconditions);

                    Tuple<string, string> TermsAndConditionTuple = TermsAndConditions.FirstOrDefault(a => a.Item1 == termsAndConditionDropDown.SelectedValue);
                    if (TermsAndConditionTuple != null)
                    {
                        termAndConditionTextBox.Text = TermsAndConditionTuple.Item2;
                    }
                }
              
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

      
        /// <summary>
        ///get the details of general settings entity
        /// </summary>
        protected void GetGeneralSettings()
        {
            try
            {
                GeneralSettings generalSettings;
                generalSettings = _generalSettingsManager.GetGeneralSettings;
                SetTextBoxText(partnerAccountRequestsTextBox, generalSettings.PartnerAccountRequestEmail);
                SetTextBoxText(productRequestsTextBox, generalSettings.ProductRequestEmail);
                SetTextBoxText(feedbackTextBox, generalSettings.FeedbackEmail);

                if (generalSettings.FeaturedProjectVisibility)
                    SetRadioButtonSelected(navigationYesRadioButton, true);
                else
                    SetRadioButtonSelected(navigationNoRadioButton, true);

                SetTextBoxText(goldPriceTextBox, generalSettings.GoldPartnershipAmount);
                SetTextBoxText(regularPriceTextBox, generalSettings.RegularPartnershipAmount);

                IList<Tuple<string, string>> TermsAndConditions = generalSettings.TermsAndConditions;
                SetViewState<IList<Tuple<string, string>>>(ViewStateConstants.TermsAndconditions, TermsAndConditions);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///link button to navigate to content block page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnContentBlock_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ContentBlock.aspx", false);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Link button to navigate to settings page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnGeneralSettings_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("Settings.aspx", false);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///triggers when page initializes
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                ValidateUserAccess((int)MenuEnum.Settings);

                _generalSettingsManager = GeneralSettingsManager.Instance;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///triggers when page loads after server post back.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ResourceUtility.GetLocalizedString(396, culture, "General Settings");
            ScriptManager.RegisterPostBackControl(termsAndConditionDropDown);
            if (!Page.IsPostBack)
            {
                try
                {
                    if (GetSession<User>(SessionConstants.User) != null)
                    {
                        //Pre Populate general Settings
                        GetGeneralSettings();

                        SetRegularExpressions();

                        PopulateDropDownData();
                    }
                }
                catch (Exception exception)
                {
                    UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                }
            }
        }

        /// <summary>
        /// To populate Drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            BindDynamicDropDown(DropDownConstants.TermsAndConditions, termsAndConditionDropDown, ResourceUtility.GetLocalizedString(1115, culture, "Add New"), null);

            int totalItems = termsAndConditionDropDown.Items.Count;
            if (totalItems > 1)
            {
                termsAndConditionDropDown.SelectedValue = termsAndConditionDropDown.Items[termsAndConditionDropDown.Items.Count - 1].Value;
                saveTermsAndCondionDiv.Visible = false;
                termAndConditionTextBox.Enabled = false;
                IList<Tuple<string, string>> TermsAndConditions = GetViewState<IList<Tuple<string, string>>>(ViewStateConstants.TermsAndconditions);

                Tuple<string, string> TermsAndConditionTuple = TermsAndConditions.FirstOrDefault(a => a.Item1 == termsAndConditionDropDown.SelectedValue);
                if (TermsAndConditionTuple != null)
                {
                    termAndConditionTextBox.Text = TermsAndConditionTuple.Item2;
                }
            }
        }

        /// <summary>
        /// This method will set the validation expression for regular expression validation based on culture
        /// </summary>
        private void SetRegularExpressions()
        {
            // Regular Expression for website
            SetValidatorRegularExpressions(partnerAccountRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(productRequestsRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(feedbackRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);

            // Regular Expression for amount
            SetValidatorRegularExpressions(regularPriceRegularExpressionValidator, (int)RegularExpressionTypeEnum.AmountTwoDecimalValidator);
            SetValidatorRegularExpressions(goldPriceRegularExpressionValidator, (int)RegularExpressionTypeEnum.AmountTwoDecimalValidator);
        }

        /// <summary>
        ///triggers when save button is clicked and saves the entity
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveGeneralSettingsLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                GeneralSettings generalSettings = new GeneralSettings()
                {
                    PartnerAccountRequestEmail = partnerAccountRequestsTextBox.Text,
                    ProductRequestEmail = productRequestsTextBox.Text,
                    FeedbackEmail = feedbackTextBox.Text,
                    GoldPartnershipAmount = goldPriceTextBox.Text,
                    RegularPartnershipAmount = regularPriceTextBox.Text,
                };

                if (navigationYesRadioButton.Checked)
                    generalSettings.FeaturedProjectVisibility = true;
                else
                    generalSettings.FeaturedProjectVisibility = false;

                //call the business method
                int result = _generalSettingsManager.UpdateGeneralSettings(generalSettings);

                resultMessageDiv.Visible = true;

                resultMessageDiv.Attributes.Remove("class");
                if (result == 0)
                {
                    resultMessageDiv.Attributes.Add("class", "message error");
                    resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralUpdateFailure, culture);
                }
                else
                {
                    resultMessageDiv.Attributes.Add("class", "message success");
                    resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralSettingsSuccessfullyUpdated, culture);
                }

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Triggers when save terms and condition link button get clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveTermAndConditionsLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid && termAndConditionTextBox.Text.Trim() != string.Empty)
                {
                    User user = GetSession<User>(SessionConstants.User);
                    int versionId = _generalSettingsManager.AddTermsAndConditionsVersion(termAndConditionTextBox.Text.Trim().Replace("em>", "i>"), user.UserId);
                    if (versionId > 0)
                    {
                        UpdateDynamicDropDownList(DropDownConstants.TermsAndConditions);
                        BindDynamicDropDown(DropDownConstants.TermsAndConditions, termsAndConditionDropDown, ResourceUtility.GetLocalizedString(1115, culture, "Add New"), null);
                        termsAndConditionDropDown.SelectedValue = versionId.ToString();
                        saveTermsAndCondionDiv.Visible = false;
                        termAndConditionTextBox.Enabled = false;

                        GeneralSettings generalSettings;
                        generalSettings = _generalSettingsManager.GetGeneralSettings;
                        IList<Tuple<string, string>> TermsAndConditions = generalSettings.TermsAndConditions;
                        SetViewState<IList<Tuple<string, string>>>(ViewStateConstants.TermsAndconditions, TermsAndConditions);

                        resultMessageDiv.Attributes.Add("class", "message success");
                        resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.TermsAndConditionSucessfullyUpdated, culture);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}