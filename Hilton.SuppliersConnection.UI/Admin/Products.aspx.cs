﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Products : PageBase
    {
        private IProductManager _productManager;

        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedBrands_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                var brands = sender as TextBox;
                brandCollection.Value = ProcessDropDownItemChange(DropDownConstants.BrandDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Register for post backs
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            brands.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(optgroup, p.DataValueField)));
            base.Render(writer);
        }
        /// <summary>
        /// Binds Data to Category DropDown
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            try
            {
                categoryDropDown.DataSource = categoryList;
                categoryDropDown.DataTextField = "CategoryName";
                categoryDropDown.DataValueField = "CategoryId";
                categoryDropDown.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Sets GridView Header Text
        /// </summary>
        private void SetGridViewHeaderText()
        {
            productListGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(444, culture, "SKU");
            productListGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(450, culture, "Name");
            productListGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(485, culture, "Partner");
            productListGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(697, culture, "Status");
        }

        /// <summary>
        /// Invoked when add content block button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AddProductLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                if (productSearch != null)
                {
                    productSearch.PageSet = pagingControl.PageSet;
                    productSearch.PageSet = pagingControl.PageSet;
                    ClearSession(SessionConstants.ProductSearch);
                    SetSession<ProductSearch>(SessionConstants.ProductSearch, productSearch);
                }
                Response.Redirect("~/Admin/ProductDetails.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                productSearch.PageIndex = pagingControl.CurrentPageIndex;
                productSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                productSearch.AllClick = pagingControl.AllPageClick;
                SearchProductList(productSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Category DropDown selected index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void CategoryDropDown_SelectedIndexChanged(object sender, EventArgs args)
        {
            try
            {
                SetSubCategoryAccess();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Sets Sub-Category Access
        /// </summary>
        private void SetSubCategoryAccess()
        {
            IList<Category> subcategories = GetChildCategories(categoryDropDown.SelectedValue);
            if (categoryDropDown.SelectedIndex > 0 && subcategories.Count > 0)
            {
                subcategoryDropDown.Enabled = true;
                PopulateCategoryData(subcategories, 2, subcategoryDropDown); // second level categories
            }
            else
            {
                subcategoryDropDown.Items.Clear();
                subcategoryDropDown.Enabled = false;
            }
        }

        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                productSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchProductList(productSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when go gutton is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GoButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(selectedBrands.Text))
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", "Please select brand");
                }
                else
                {
                    SearchProducts();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// It search Products
        /// </summary>
        private void SearchProducts()
        {
            ProductSearch productSearch = PopulateSearchCriteria();
            productSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
            productSearch.SortExpression = UIConstants.Model;
            productSearch.SortDirection = UIConstants.AscAbbreviation;
            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
            SetViewState(ViewStateConstants.SortExpression, UIConstants.Model);
            SearchProductList(productSearch);

            //Pagination Control Setting
            pagingControl.CurrentPageIndex = 0;
            pagingControl.PageSet = 0;
            pagingControl.AllPageClick = false;
        }

        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                productSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchProductList(productSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetSession<ProductSearch>(SessionConstants.ProductSearch, productSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                productSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchProductList(productSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetSession<ProductSearch>(SessionConstants.ProductSearch, productSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoded on page initialization
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs args)
        {
            try
            {
                base.OnInit(args);
                ValidateUserAccess((int)MenuEnum.Products);
                _productManager = ProductManager.Instance;
                SetGridViewHeaderText();

                addProductLinkButton.Text = string.Format(CultureInfo.InvariantCulture, "+ {0}", ResourceUtility.GetLocalizedString(189, culture, "Add Product"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoke on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div

                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;

                    if (!Page.IsPostBack)
                    {
                        ClearSession(SessionConstants.UserId);
                        User currentUser = GetSession<User>(SessionConstants.User);
                        IList<SearchFilters> searchFilterData = _productManager.GetProductSearchFilterItems(currentUser.UserId);
                        SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilterData);

                        PopulateDropDownData();

                        if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("Products.aspx") || Request.UrlReferrer.ToString().Contains("ProductDetails.aspx")))// || Request.UrlReferrer.ToString().Contains("Partners.aspx")))
                        {
                            MaintainPageState();
                        }

                        else if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("Partners.aspx") || Request.UrlReferrer.ToString().Contains("PartnerDetails.aspx")))
                        {
                            ClearSession(SessionConstants.ProductSearch);
                            string partnerId = GetSession<string>(SessionConstants.PartnerId);
                            ClearSession(SessionConstants.PartnerId);
                            if (!string.IsNullOrEmpty(partnerId))
                            {
                                partnerDropDown.SelectedValue = partnerId;
                                SearchProducts();
                            }
                        }
                        else
                        {
                            ClearSession(SessionConstants.ProductSearch);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(productListUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                        ScriptManager.RegisterClientScriptBlock(productListUpdatePanel, this.GetType(), "getItems", "getItems();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                productSearch.PageIndex = pagingControl.CurrentPageIndex;
                productSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                productSearch.AllClick = pagingControl.AllPageClick;
                SearchProductList(productSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                productSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchProductList(productSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetSession<ProductSearch>(SessionConstants.ProductSearch, productSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                productSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchProductList(productSearch);
                ClearSession(SessionConstants.ProductSearch);
                SetSession<ProductSearch>(SessionConstants.ProductSearch, productSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProductListGridView_RowCommand(object sender, CommandEventArgs args)
        {
            try
            {
                if (args != null && (args.CommandName == "EditProductList"))
                {
                    ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                    productSearch.PageSet = pagingControl.PageSet;
                    ClearSession(SessionConstants.ProductSearch);
                    SetSession<ProductSearch>(SessionConstants.ProductSearch, productSearch);
                    Context.Items[UIConstants.Mode] = UIConstants.Edit;
                    Context.Items[UIConstants.ProductListId] = args.CommandArgument.ToString();
                    Server.Transfer("~/Admin/ProductDetails.aspx", false);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProductListGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProductListGridView_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton editLinkButton = args.Row.FindControl("editLinkButton") as LinkButton;
                    if (editLinkButton != null)
                    {
                        ScriptManager.GetCurrent(this).RegisterPostBackControl(editLinkButton);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProductListGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at PropertyType DropDown SelectedIndex is Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PropertyTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                brandCollection.Value = ProcessDropDownItemChange(DropDownConstants.RegionDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method is invoked with search button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void QuickSearch_Click(object sender, EventArgs args)
        {
            try
            {
                ProductSearch productSearch = PopulateSearchCriteria();
                productSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                productSearch.SortExpression = UIConstants.Model;
                productSearch.SortDirection = UIConstants.AscAbbreviation;
                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                SetViewState(ViewStateConstants.SortExpression, UIConstants.Model);
                SearchProductList(productSearch);

                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
                pagingControl.AllPageClick = false;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Fires at RegionDropDown Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //To reset the country already selected
                countryDropDown.SelectedIndex = -1;
                brandCollection.Value = ProcessDropDownItemChangeforCountry(DropDownConstants.RegionDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown, countryDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        
        ///
        /// <summary>
        /// Binds DropDown
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        private void BindDropDown(IList<Category> categories, DropDownList sourceDropDown, string defaultText)
        {
            sourceDropDown.DataSource = categories.OrderBy(p => p.CategoryName); ;
            sourceDropDown.DataTextField = "CategoryName";
            sourceDropDown.DataValueField = "CategoryId";
            sourceDropDown.DataBind();
            sourceDropDown.Items.Insert(0, new ListItem(defaultText, "0"));
        }

        /// <summary>
        ///  This method will bind the content block
        /// </summary>
        /// <param name="contentblocks"></param>
        private void BindProductList(IList<Product> products)
        {
            productListGridView.DataSource = products;
            productListGridView.DataBind();
        }

        /// <summary>
        /// Gets Child Categories
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            IList<Category> categoryList = (categories.AsEnumerable().
                                            Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture))).ToList());
            return categoryList;
        }


        /// <summary>
        /// This method will return the sort colum index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in productListGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return productListGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("Products.aspx") || Request.UrlReferrer.ToString().Contains("ProductDetails.aspx")))
                    {
                        ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
                        if (field.SortExpression == productSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, productSearch.SortExpression);
                            if (productSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return productListGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// This method will maintain page state when user redirect from content block details screen
        /// </summary>
        private void MaintainPageState()
        {
            ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);

            if (productSearch != null)
            {
                PolulateSearchCriteria(productSearch);

                pagingControl.PageSet = productSearch.PageSet;

                SearchProductList(productSearch);
            }
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        private void PolulateSearchCriteria(ProductSearch productSearch)
        {
            selectedBrands.Text = productSearch.BrandId;
            brandCollection.Value = productSearch.BrandCollection;
            if (!string.IsNullOrEmpty(productSearch.PartnerId))
                partnerDropDown.SelectedValue = productSearch.PartnerId;

            if (!string.IsNullOrEmpty(productSearch.CategoryId))
            {
                categoryDropDown.SelectedValue = productSearch.CategoryId;
                SetSubCategoryAccess();
            }

            if (!string.IsNullOrEmpty(productSearch.SubCategoryId))
                subcategoryDropDown.SelectedValue = productSearch.SubCategoryId;

            if (!string.IsNullOrEmpty(productSearch.StatusId))
                statusDropDown.SelectedValue = productSearch.StatusId;

            if (!string.IsNullOrEmpty(productSearch.QuickSearchExpression))
                searchTextBox.Text = productSearch.QuickSearchExpression;

            if (!string.IsNullOrEmpty(productSearch.RegionId))
            {
                regionDropDown.SelectedValue = productSearch.RegionId;
                ProcessDropDownItemChange(DropDownConstants.RegionDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(productSearch.BrandId))
            {
                ProcessDropDownItemChange(DropDownConstants.BrandDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(productSearch.PropertyTypeId))
            {
                propertyTypeDropDown.SelectedValue = productSearch.PropertyTypeId;
                ProcessDropDownItemChange(DropDownConstants.PropertyTypesDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }
        }

        /// <summary>
        /// This method will populate the drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            IList<SearchFilters> searchFilters = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);

            BindDropDown(searchFilters, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1205, culture, "All Regions"), null);
            var brandId = Helper.GetSearchFilterDropDownData(DropDownConstants.BrandDropDown, searchFilters).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false);
            brandCollection.Value = string.Join(",", brandId.ToList().Select(p => p.DataValueField));
            BindDropDown(searchFilters, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(1196, culture, "All Types"), null);
            BindDynamicDropDown(DropDownConstants.PartnerDropDown, partnerDropDown, ResourceUtility.GetLocalizedString(1217, culture, "All Partners"), null);
            BindStaticDropDown(DropDownConstants.ProductStatusDropDown, statusDropDown, ResourceUtility.GetLocalizedString(1228, culture, "All Status"), null);
            
            BindDropDown(searchFilters, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1301, culture, "All Countries"), null);
            IList<Category> categories = HelperManager.Instance.GetActiveCategories;
            int level = 1; // for root level categories
            SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categories);
            subcategoryDropDown.Enabled = false;
            PopulateCategoryData(categories, level, categoryDropDown);
        }

        /// <summary>
        /// Populates Category Data
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="level"></param>
        /// <param name="categoryDropDown"></param>
        private void PopulateCategoryData(IList<Category> categories, int level, DropDownList categoryDropDown)
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            IList<Category> accessibleCategoryList = FilterCategoryAccess(categories.Where(item => item.Level.Equals(level)).ToList(), currentUser.AccessibleCategoryList);
            BindDropDown(accessibleCategoryList, categoryDropDown, ResourceUtility.GetLocalizedString(1211, culture, "All Categories"));
        }

        /// <summary>
        /// Filters Category Access
        /// </summary>
        /// <param name="sourceCategoryCollection"></param>
        /// <param name="accessibleCategoryCollection"></param>
        /// <returns></returns>
        private IList<Category> FilterCategoryAccess(IList<Category> sourceCategoryCollection, IList<Category> accessibleCategoryCollection)
        {
            try
            {
                IList<Category> resultCategoryCollection = null;
                if (accessibleCategoryCollection.Count > 0)
                {
                    resultCategoryCollection = (IList<Category>)(sourceCategoryCollection.Join(
                                                           accessibleCategoryCollection,
                                                           sourceCategory => sourceCategory.CategoryId,
                                                           accessibleCategory => accessibleCategory.CategoryId, (sourceCategory, accessibleCategory) => sourceCategory)).ToList();
                }
                else
                    resultCategoryCollection = (IList<Category>)sourceCategoryCollection;
                return resultCategoryCollection;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }

        /// <summary>
        /// This method will populate the search criteria
        /// </summary>
        /// <returns></returns>
        private ProductSearch PopulateSearchCriteria()
        {
            User currentUser = (User)GetSession<User>(SessionConstants.User);
            ProductSearch contentSearch = new ProductSearch()
            {
                QuickSearchExpression = searchTextBox.Text.Trim(),
                PartnerId = (partnerDropDown.SelectedIndex == 0) ? string.Empty : partnerDropDown.SelectedValue,
                RegionId = (regionDropDown.SelectedIndex == 0) ? string.Empty : regionDropDown.SelectedValue,
                BrandId = selectedBrands.Text,
                //Nishant
                CountryId= (countryDropDown.SelectedIndex ==0) ?String.Empty :countryDropDown.SelectedValue,
                BrandCollection = brandCollection.Value,
                UserId = currentUser.UserId,
                PropertyTypeId = (propertyTypeDropDown.SelectedIndex == 0) ? string.Empty : propertyTypeDropDown.SelectedValue,
                CategoryId = (categoryDropDown.SelectedIndex == 0) ? string.Empty : categoryDropDown.SelectedValue,
                SubCategoryId = (subcategoryDropDown.SelectedIndex == 0) ? string.Empty : subcategoryDropDown.SelectedValue,
                StatusId = (statusDropDown.SelectedIndex == 0) ? string.Empty : statusDropDown.SelectedValue,
                PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture),
                PageIndex = 0,
            };
            return contentSearch;
        }

       
        /// <summary>
        /// This method will populate the search criteria
        /// </summary>
        /// <returns></returns>
        private void SearchProductList(ProductSearch productSearch)
        {
            int totalRecordCount = 0;
            if (productSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                productSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                productSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (productSearch.PageIndex == -1)
                {
                    productSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = productSearch.PageIndex;
            }

            IList<Product> products = _productManager.GetProductList(productSearch, ref totalRecordCount);

            if (products.Count == 0)
            {
                ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoProductsFoundForCriteria, culture));
            }

            productSearch.TotalRecordCount = totalRecordCount;

            //Save criteria in session for back button functionality
            SetSession<ProductSearch>(SessionConstants.ProductSearch, productSearch);

            BindProductList(products);

            //Pagination Control Setting
            pagingControl.TotalRecordCount = totalRecordCount;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            ProductSearch productSearch = GetSession<ProductSearch>(SessionConstants.ProductSearch);
            productSearch.SortExpression = sortExpression;
            productSearch.SortDirection = sortDirection;
            productSearch.PageIndex = 0;
            SearchProductList(productSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }
    }
}