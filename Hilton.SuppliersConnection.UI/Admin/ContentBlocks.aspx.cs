﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ContentBlocks : PageBase
    {
        private IContentBlockManager _contentBlockManager;

        /// <summary>
        /// Invoked when add content block button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AddContentBlockLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                if (contentSearch != null)
                {
                    contentSearch.PageSet = pagingControl.PageSet;
                    SetSession<ContentSearch>(SessionConstants.ContentSearch, contentSearch);
                }
                Response.Redirect("~/Admin/ContentBlockDetails.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                contentSearch.PageIndex = pagingControl.CurrentPageIndex;
                contentSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                contentSearch.AllClick = pagingControl.AllPageClick;
                SearchContentBlock(contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ContentBlockListGridView_RowCommand(object sender, CommandEventArgs args)
        {
            try
            {
                if (args != null && (args.CommandName == "EditContentBlock"))
                {
                    ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                    contentSearch.PageSet = pagingControl.PageSet;
                    SetSession<ContentSearch>(SessionConstants.ContentSearch, contentSearch);
                    Context.Items[UIConstants.Mode] = UIConstants.Edit;
                    Context.Items[UIConstants.ContentBlockId] = args.CommandArgument.ToString();
                    Server.Transfer("~/Admin/ContentBlockDetails.aspx", false);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ContentBlockListGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContentBlockListGridView_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton editLinkButton = args.Row.FindControl("editLinkButton") as LinkButton;
                    if (editLinkButton != null)
                    {
                        ScriptManager.GetCurrent(this).RegisterPostBackControl(editLinkButton);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ContentBlockListGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked with delete link is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void DeleteContentBlock(object sender, CommandEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    int result = _contentBlockManager.DeleteContentBlock(Convert.ToInt32(args.CommandArgument, CultureInfo.InvariantCulture));
                    if (result == 0)
                    {
                        resultMessageDiv.Attributes.Add("class", "message success");
                        resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.ContentBlockDeleteSuccess, culture);

                        ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);

                        int totalRecordCount = contentSearch.TotalRecordCount - 1;
                        int pageCount = totalRecordCount / contentSearch.PageSize;
                        if ((totalRecordCount % contentSearch.PageSize) > 0)
                        {
                            pageCount += 1;
                        }

                        if ((contentSearch.PageIndex + 1) > pageCount)
                        {
                            contentSearch.PageIndex = pageCount - 1;
                            pagingControl.CurrentPageIndex = pageCount - 1;
                        }

                        SearchContentBlock(contentSearch);

                        ResourceUtility.UpdateContentBlocks();
                    }
                    else if (result == -1)
                    {
                        resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralDeleteFailure, culture);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                contentSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchContentBlock(contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when go gutton is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GoButton_Click(object sender, EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = BuildSearchCriteria();
                contentSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                GetContentBlockList(contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                contentSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchContentBlock(contentSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetSession<ContentSearch>(SessionConstants.ContentSearch, contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                contentSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchContentBlock(contentSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetSession<ContentSearch>(SessionConstants.ContentSearch, contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on page initialization
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs args)
        {
            try
            {
                base.OnInit(args);

                ValidateUserAccess((int)MenuEnum.Settings);

                _contentBlockManager = ContentBlockManager.Instance;
                SetGridViewHeaderText();
                addContentBlockLinkButton.Text = string.Format(CultureInfo.InvariantCulture, "+ {0}", ResourceUtility.GetLocalizedString(181, culture, "Add Content Block"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoke on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void Page_Load(object sender, EventArgs args)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;

                    if (!Page.IsPostBack)
                    {
                        PopulateDropDownData();

                        if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("ContentBlocks.aspx") || Request.UrlReferrer.ToString().Contains("ContentBlockDetails.aspx")))
                        {
                            MaintainPageState();
                        }
                        else
                        {
                            ClearSession(SessionConstants.ContentSearch);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(contentBlockUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Set Grid view Header from Content Block
        /// </summary>
        private void SetGridViewHeaderText()
        {
            contentBlockListGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(420, culture, "ID");
            contentBlockListGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(716, culture, "Title");
            contentBlockListGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(656, culture, "Section");
            contentBlockListGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(362, culture, "Description");
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                contentSearch.PageIndex = pagingControl.CurrentPageIndex;
                contentSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                contentSearch.AllClick = pagingControl.AllPageClick;
                SearchContentBlock(contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                contentSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchContentBlock(contentSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetSession<ContentSearch>(SessionConstants.ContentSearch, contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                contentSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchContentBlock(contentSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetSession<ContentSearch>(SessionConstants.ContentSearch, contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method is invoked with search button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void QuickSearch_Click(object sender, EventArgs args)
        {
            try
            {
                ContentSearch contentSearch = BuildSearchCriteria();
                contentSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                GetContentBlockList(contentSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  This method will bind the content block
        /// </summary>
        /// <param name="contentblocks"></param>
        private void BindContentBlock(IList<ContentBlock> contentblocks)
        {
            contentBlockListGridView.DataSource = contentblocks;
            contentBlockListGridView.DataBind();
        }

        /// <summary>
        /// This method will populate the search criteria
        /// </summary>
        /// <returns></returns>
        private ContentSearch BuildSearchCriteria()
        {
            ContentSearch contentSearch = new ContentSearch()
            {
                QuickSearchExpression = searchTextBox.Text.Trim(),
                Section = (sectionDropDown.SelectedIndex == 0) ? string.Empty : sectionDropDown.SelectedValue,
                PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture),
                PageIndex = 0,
            };
            return contentSearch;
        }

        /// <summary>
        /// To pull the content block from DB
        /// </summary>
        /// <param name="contentSearch"></param>
        private void GetContentBlockList(ContentSearch contentSearch)
        {
            contentSearch.SortExpression = UIConstants.ContentBlockId; ;
            contentSearch.SortDirection = UIConstants.AscAbbreviation;
            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
            SetViewState(ViewStateConstants.SortExpression, UIConstants.ContentBlockId);
            SearchContentBlock(contentSearch);

            //Pagination Control Setting
            pagingControl.CurrentPageIndex = 0;
            pagingControl.PageSet = 0;
            pagingControl.AllPageClick = false;
        }

        /// <summary>
        /// This method will return the sort column index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in contentBlockListGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return contentBlockListGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("ContentBlocks.aspx") || Request.UrlReferrer.ToString().Contains("ContentBlockDetails.aspx")))
                    {
                        ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
                        if (field.SortExpression == contentSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, contentSearch.SortExpression);
                            if (contentSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return contentBlockListGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// This method will maintain page state when user redirect from content block details screen
        /// </summary>
        private void MaintainPageState()
        {
            ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);

            if (contentSearch != null)
            {
                PolulateSearchCriteria(contentSearch);

                pagingControl.PageSet = contentSearch.PageSet;

                SearchContentBlock(contentSearch);
            }
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        private void PolulateSearchCriteria(ContentSearch contentSearch)
        {
            if (!string.IsNullOrEmpty(contentSearch.Section))
            {
                if (sectionDropDown.Items.Contains(new ListItem(contentSearch.Section)))
                {
                    sectionDropDown.SelectedValue = contentSearch.Section;
                }
                else
                {
                    if (contentSearch.SearchFlag == SearchFlagEnum.Filter.ToString())
                    {
                        contentSearch.SortExpression = UIConstants.ContentBlockId;
                        contentSearch.SortDirection = UIConstants.AscAbbreviation;
                        contentSearch.PageSet = 0;
                        contentSearch.AllClick = false;
                        contentSearch.PageIndex = 0;
                        contentSearch.Section = string.Empty;
                        SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                        SetViewState(ViewStateConstants.SortExpression, UIConstants.ContentBlockId);

                        //Pagination Control Setting
                        pagingControl.CurrentPageIndex = 0;
                        pagingControl.PageSet = 0;
                        pagingControl.AllPageClick = false;
                    }
                }
            }

            if (!string.IsNullOrEmpty(contentSearch.QuickSearchExpression))
                searchTextBox.Text = contentSearch.QuickSearchExpression;
        }

        /// <summary>
        /// This method will populate the drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            BindDynamicDropDown(DropDownConstants.SectionDropDown, sectionDropDown, ResourceUtility.GetLocalizedString(1091, culture, "All Titles"), null);
        }

        /// <summary>
        /// This method will search the content block based on specified criteria
        /// </summary>
        /// <param name="contentSearch"></param>
        private void SearchContentBlock(ContentSearch contentSearch)
        {
            int totalRecordCount = 0;
            if (contentSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                contentSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                contentSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (contentSearch.PageIndex == -1)
                {
                    contentSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = contentSearch.PageIndex;
            }

            var contentblocks = _contentBlockManager.GetContentBlock(contentSearch, ref totalRecordCount);
            if (contentblocks.Count == 0)
            {
                ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoContentBlockFoundForCriteria, culture));
            }

            contentSearch.TotalRecordCount = totalRecordCount;

            //Save criteria in session for back button functionality
            ClearSession(SessionConstants.ContentSearch);
            SetSession<ContentSearch>(SessionConstants.ContentSearch, contentSearch);

            BindContentBlock(contentblocks);

            //Pagination Control Setting
            pagingControl.TotalRecordCount = totalRecordCount;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            ContentSearch contentSearch = GetSession<ContentSearch>(SessionConstants.ContentSearch);
            contentSearch.SortExpression = sortExpression;
            contentSearch.SortDirection = sortDirection;
            contentSearch.PageIndex = 0;
            SearchContentBlock(contentSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }
    }
}