﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

using System.Data;
using Aspose.Cells;
using System.Drawing;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Dashboard : PageBase
    {
        private IDashboardManager _dashboardManager;

        /// <summary>
        /// Get the instance of the manager class and validate the session
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                if (!IsPostBack)
                {
                    User user = GetSession<User>(SessionConstants.User);
                    if (user == null || user.UserTypeId != (int)UserTypeEnum.Administrator)
                    {
                        Response.Redirect("~/AccessDenied.aspx", false);
                    }

                }

                _dashboardManager = DashboardManager.Instance;
                SetGridViewHeaderText();
            }
            catch (ThreadAbortException)
            { }
        }

        /// <summary>
        /// Redirect to first menu item
        /// </summary>
        private void RedirectToFirstMenu()
        {
            IList<UserPermissions> userMenuPermCollection = ((PageBase)Page).GetSession<IList<UserPermissions>>(SessionConstants.AdminMenu);
            UserPermissions menuPermissions = userMenuPermCollection.Where(p => p.ParentMenuId == 0).OrderBy(x => x.DisplayDirection).First();
            Response.Redirect(menuPermissions.Command);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            bool dashboardAuthorization = ValidateAdminControlAuthorization((int)AdminPermissionEnum.Dashboard);
            if (!dashboardAuthorization && GetSession<User>(SessionConstants.User) != null)
            {
                RedirectToFirstMenu();
            }
        }

        /// <summary>
        /// Bind the data to the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ResourceUtility.GetLocalizedString(349, culture, "Dashboard");

            Entities.Dashboard dashboard;
            try
            {
                bool dashboardAuthorization = ValidateAdminControlAuthorization((int)AdminPermissionEnum.Dashboard);
                if (dashboardAuthorization && GetSession<User>(SessionConstants.User) != null)
                {
                    //call the business method
                    dashboard = DashboardManager.Instance.GetDashboardStatistics;
                    //Set the data
                    SetPartnerData(dashboard.PartnerStatistics);
                    SetProductData(dashboard.ProductStatistics);
                    SetMessagingFeedbackData(dashboard.FeedbackStatistics);
                    projectGridView.DataSource = dashboard.ProjectStatistics;
                    projectGridView.DataBind();
                    userGridView.DataSource = dashboard.UserStatistics;
                    userGridView.DataBind();
                    downloadDashboardReportLinkButton.Attributes.Add("onclick", "UpdateSessionTimeout()");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Set the messaging feedback data
        /// </summary>
        /// <param name="feedbackStatistics"></param>
        private void SetMessagingFeedbackData(FeedbackStatistics feedbackStatistics)
        {
            messagesToOwnersLiteral.Text = feedbackStatistics.MessageToOwners.ToString(CultureInfo.InvariantCulture);
            messagesToPartnersLiterals.Text = feedbackStatistics.MessageToPartners.ToString(CultureInfo.InvariantCulture);
            feedbackSubmittedLiterals.Text = feedbackStatistics.FeedbackSubmitted.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Set the partner statistics data
        /// </summary>
        /// <param name="partnerStatistics"></param>
        private void SetPartnerData(PartnerStatistics partnerStatistics)
        {
            currentGoldLiteral.Text = partnerStatistics.CurrentGoldPartners.ToString(CultureInfo.InvariantCulture);
            currentRegLiteral.Text = partnerStatistics.CurrentRegularPartners.ToString(CultureInfo.InvariantCulture);
            outstandingPaymentGoldLiteral.Text = partnerStatistics.OutstandingGoldPartnersRequest.ToString(CultureInfo.InvariantCulture);
            outstandingPaymentRegularLiteral.Text = partnerStatistics.OutstandingRegularPartnersRequest.ToString(CultureInfo.InvariantCulture);
            outstandingPaymentRequestAmountGoldLiteral.Text = partnerStatistics.OutstandingGoldPartnersReqAmount.ToString("C", culture);
            outstandingPaymentRequestAmountRegularLiteral.Text = partnerStatistics.OutstandingRegularPartnersReqAmount.ToString("C", culture);
            totalCollectedYTDGoldLiteral.Text = partnerStatistics.TotalGoldPartnershipAmtCollected.ToString("C", culture);
            totalCollectedYTDRegularLiteral.Text = partnerStatistics.TotalRegularPartnershipAmtCollected.ToString("C", culture);
            totalNumberActivatedCurrentMonthGoldLiteral.Text = partnerStatistics.ActivatedGoldPartnerInCurrentMonth.ToString(CultureInfo.InvariantCulture);
            totalNumberActivatedCurrentMonthRegularLiteral.Text = partnerStatistics.ActivatedRegularPartnerInCurrentMonth.ToString(CultureInfo.InvariantCulture);
            totalAmountActivatedCurrentMonthGoldLiteral.Text = partnerStatistics.ActivatedGoldPartnerAmountInCurrentMonth.ToString("C", culture);
            totalAmountActivatedCurrentMonthRegularLiteral.Text = partnerStatistics.ActivatedRegualrPartnerAmountInCurrentMonth.ToString("C", culture);
            totalNumberActivatedCurrentYearGoldLiteral.Text = partnerStatistics.ActivatedGoldPartnerInCurrentYear.ToString(CultureInfo.InvariantCulture);
            totalNumberActivatedCurrentYearRegularLiteral.Text = partnerStatistics.ActivatedRegularPartnerInCurrentYear.ToString(CultureInfo.InvariantCulture);
            totalAmountActivatedCurrentYearGoldLiteral.Text = partnerStatistics.ActivatedGoldPartnerAmountInCurrentYear.ToString("C", culture);
            totalAmountActivatedCurrentYearRegularLiteral.Text = partnerStatistics.ActivatedRegularPartnerAmountInCurrentYear.ToString("C", culture);
            pendingPartnerRequestGoldLiteral.Text = partnerStatistics.PendingGoldPartnerRequest.ToString(CultureInfo.InvariantCulture);
            pendingPartnerRequestRegularLiteral.Text = partnerStatistics.PendingRegularPartnerRequest.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Set the product data
        /// </summary>
        /// <param name="productStatistics"></param>
        private void SetProductData(ProductStatistics productStatistics)
        {
            productsLiteral.Text = productStatistics.TotalProducts.ToString(CultureInfo.InvariantCulture);
            pendingRequestLiteral.Text = productStatistics.PendingRequest.ToString(CultureInfo.InvariantCulture);
            otherRequestsLiteral.Text = productStatistics.OtherSelections.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Set the GridView Header Text
        /// </summary>
        private void SetGridViewHeaderText()
        {
            // Project Grid
            projectGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(725, culture, "Total");
            projectGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(461, culture, "# @ 0");
            projectGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(240, culture, "Avg % after 0");
            projectGridView.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(460, culture, "# 100%");

            // User Grid
            userGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(725, culture, "Total");
            userGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(464, culture, "# Logins").ReplaceEmptyString();
            userGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(239, culture, "Avg # Logins").ReplaceEmptyString();
        }




        /// <summary>
        /// Download Dashboard report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void DownloadSpreadSheet_ClickLinkButton(object sender, EventArgs args)
        {
            try
            {

                Workbook dashboardExportReport = new Workbook();
                DataSet dashboardReportDataSet = _dashboardManager.GetDashboardExportReportDataSet;

                License license = new License();
                license.SetLicense(Server.MapPath("~/bin/Aspose.Custom.lic"));
          
                dashboardExportReport.Open(Server.MapPath("~/Excel/DashboardExport.xls"));
                
                Aspose.Cells.Cell currentDateForProducts = dashboardExportReport.Worksheets[0].Cells["A1"];
                Aspose.Cells.Cell currentDateForProjects = dashboardExportReport.Worksheets[1].Cells["A1"];
                Aspose.Cells.Cell currentDateForUsers = dashboardExportReport.Worksheets[2].Cells["A1"];

                String currentDate = String.Format("{0: MM-dd-yyyy}", DateTime.Now);
                dashboardReportDataSet.Tables[1].Columns.RemoveAt(0);

                dashboardExportReport.Worksheets[0].Cells.ImportDataTable(dashboardReportDataSet.Tables[0], false, "A2");
                dashboardExportReport.Worksheets[0].AutoFitColumn(0);
                dashboardExportReport.Worksheets[0].AutoFitColumns();
                currentDateForProducts.PutValue("Suppliers Connection Dashboard-Partners-Products Summary" + " " + currentDate);

                dashboardExportReport.Worksheets[1].Cells.ImportDataTable(dashboardReportDataSet.Tables[1], false, "A3");
                dashboardExportReport.Worksheets[1].AutoFitColumn(0);
                dashboardExportReport.Worksheets[1].AutoFitColumns();
                currentDateForProjects.PutValue("Suppliers Connection Dashboard-Projects Summary" + " " + currentDate);

                dashboardExportReport.Worksheets[2].Cells.ImportDataTable(dashboardReportDataSet.Tables[2], false, "A3");
                dashboardExportReport.Worksheets[2].AutoFitColumn(0);
                dashboardExportReport.Worksheets[2].AutoFitColumns();
                currentDateForUsers.PutValue("Suppliers Connection Dashboard-Users Summary " + " " + currentDate);

                dashboardExportReport.Save("DashboardExport.xls", Aspose.Cells.SaveType.OpenInExcel, Aspose.Cells.FileFormatType.Default, Response);

            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }








        /// <summary>
        /// Get the text from resource utility for user grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UserGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (e.Row.Cells[0].Text == UserTypeEnum.Partner.ToString())
                    {
                        e.Row.Cells[0].Text = ResourceUtility.GetLocalizedString(497, culture, "Partner");
                    }
                    else if (e.Row.Cells[0].Text == UserTypeEnum.Owner.ToString())
                    {
                        e.Row.Cells[0].Text = ResourceUtility.GetLocalizedString(481, culture, "Owner");
                    }
                    else if (e.Row.Cells[0].Text == UserTypeEnum.Consultant.ToString())
                    {
                        e.Row.Cells[0].Text = ResourceUtility.GetLocalizedString(319, culture, "Consultant");
                    }
                    else if (e.Row.Cells[0].Text == UserTypeEnum.Administrator.ToString())
                    {
                        e.Row.Cells[0].Text = ResourceUtility.GetLocalizedString(199, culture, "Administrator");
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// Get the text from resource utility from project grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProjectGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.NewDevelopment))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(788, culture, "New Development").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.Conversion))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(789, culture, "Conversion").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.RoomAddition))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(790, culture, "Room Addition").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.BrandChange))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(791, culture, "Brand Change").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.SpecialProject))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(793, culture, "Special Project").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.AdaptiveReuse))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(796, culture, "Adaptive Re-use").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.Renewal))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(798, culture, "Renewal").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.DCPIP))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(792, culture, "D&C PIP").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.ProposedProject))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(794, culture, "Proposed Project").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.QAPIP))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(796, culture, "QA PIP").ReplaceEmptyString();
                    }
                    else if (((ProjectStatistics)(e.Row.DataItem)).ProjectTypeId == Convert.ToInt32(ProjectTypeEnum.Extension))
                    {
                        e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(797, culture, "Extension").ReplaceEmptyString();
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }
    }
}