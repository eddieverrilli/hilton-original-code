﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class UserDetails : PageBase
    {
        private IUserManager _userManager;

        /// <summary>
        ///  Gets The Category Listf
        /// </summary>
        private IList<Category> CategoryList
        {
            get
            {
                if (GetViewState<IList<Category>>(ViewStateConstants.CategoryListAccessViewSate) == null)
                {
                    SetViewState<IList<Category>>(ViewStateConstants.CategoryListAccessViewSate, new List<Category>());
                }
                return (IList<Category>)GetViewState<IList<Category>>(ViewStateConstants.CategoryListAccessViewSate);
            }
            set
            {
                SetViewState<IList<Category>>(ViewStateConstants.CategoryListAccessViewSate, value);
            }
        }

        /// <summary>
        /// Set the page setting for Add/Edit view
        /// </summary>
        private void SetPageSettings(string mode)
        {
            switch (mode)
            {
                case UIConstants.Edit:
                    SetViewState(UIConstants.Mode, UIConstants.Edit);
                    SetLiteralText(titleLiteral, ResourceUtility.GetLocalizedString(195, culture, "Edit User"));

                    break;

                case UIConstants.Add:
                    SetViewState(UIConstants.Mode, UIConstants.Add);
                    SetLiteralText(titleLiteral, ResourceUtility.GetLocalizedString(194, culture, "Add User"));
                    break;
            }
        }

        /// <summary>
        /// Bind the dynamically created drop down to respective category list
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            try
            {
                categoryDropDown.DataSource = categoryList;
                categoryDropDown.DataTextField = "CategoryName";
                categoryDropDown.DataValueField = "CategoryId";
                categoryDropDown.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Add the role to project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddRoleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.IsValid)
                {
                    int selectedProjectId = Convert.ToInt32(selectedRowHiddenField.Value, CultureInfo.InvariantCulture);
                    var associatedProject = GetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState) ?? new List<AssociatedProject>();
                    int noOfProjects = associatedProject.Count(p => p.ProjectId == selectedProjectId);
                    var project = noOfProjects == 1 ? associatedProject.Single(p => p.ProjectId == selectedProjectId)
                        : associatedProject.SingleOrDefault(p => p.ProjectId == selectedProjectId && p.IsToBeAdded == true);
                    if (project != null)
                    {
                        string message = string.Empty;
                        string messageType = string.Empty;
                        //When admin remaps user to already temporarily mapped project
                        if (project.IsToBeAdded == true)
                        {
                            message = ConfigurationStore.GetApplicationMessages(MessageConstants.TempUserProjectMappingExists, culture);
                        }  //When admin remaps the user to temporarily unmapped project
                        else if (project.IsToBeRemoved == true)
                        {
                            //when admin maps user to same role as of temporarily unmapped project
                            if (project.User.UserRoleId == Convert.ToInt32(userRoleDropDownList.SelectedValue, CultureInfo.InvariantCulture))
                            {
                                message = ConfigurationStore.GetApplicationMessages(MessageConstants.TempMappingRemovalIsRemoved, culture);
                                project.IsToBeRemoved = false;
                            } //When admin maps user to different role as temporarily unmapped project
                            else
                            {
                                message = ConfigurationStore.GetApplicationMessages(MessageConstants.TempUserProjectMappingSuccess, culture);
                                AddTempUserProjectMapping(selectedProjectId);
                            }
                        } //When admin maps the user to already mapped project
                        else
                        {
                            message = ConfigurationStore.GetApplicationMessages(MessageConstants.UserProjectMappingExists, culture);
                        }
                        ConfigureResultMessage(userProjectMappingDiv, "message notification", message);
                    } //when admin maps the user to new project
                    else
                    {
                        AddTempUserProjectMapping(selectedProjectId);
                        //Result div to show the message
                        ConfigureResultMessage(userProjectMappingDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.TempUserAssociationToProject, culture));
                    }
                    associatedProject = GetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState) ?? new List<AssociatedProject>();
                    //Bind the selected projects
                    associatedProjectsGridView.DataSource = associatedProject.Where(p => p.IsToBeRemoved == false);
                    associatedProjectsGridView.DataBind();
                    //To show the result division
                    userProjectMappingUpdatePanel.Update();
                    //To update the associated project
                    userTypeUpdatePanel.Update();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Create category for admin access
        /// </summary>
        /// <returns></returns>
        private Category CreateCategoryForAccess()
        {
            int existingDropDownCount = FindOccurence("ddlDynamic");
            Category accessibleCategory = new Category();
            DropDownList lastSelectedDropDown = (DropDownList)parentCategoryContainer.FindControl("ddlDynamic-" + existingDropDownCount.ToString(CultureInfo.InvariantCulture));
            if (lastSelectedDropDown.SelectedValue != "-1")
            {
                accessibleCategory = ReadCategory(lastSelectedDropDown.SelectedValue);
                accessibleCategory.AccessibleForUser = true;
                accessibleCategory.UnaccessibleForUser = false;
                return accessibleCategory;
            }
            else if (existingDropDownCount > 1)
            {
                DropDownList parentCategorySelectedDropDown = (DropDownList)parentCategoryContainer.FindControl("ddlDynamic-" + (Convert.ToInt32(existingDropDownCount, CultureInfo.InvariantCulture) - 1).ToString(CultureInfo.InvariantCulture));
                accessibleCategory = ReadCategory(parentCategorySelectedDropDown.SelectedValue);
                accessibleCategory.AccessibleForUser = true;
                accessibleCategory.UnaccessibleForUser = false;
                return accessibleCategory;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Add category access permission to admin user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddCategoryAccessPermissionLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Category accessibleCategory = CreateCategoryForAccess();
                if (accessibleCategory != null)
                {
                    int noOfCategories = CategoryList.Count(p => p.CategoryId == accessibleCategory.CategoryId);
                    var category = noOfCategories == 1 ? CategoryList.Single(p => p.CategoryId == accessibleCategory.CategoryId)
                       : CategoryList.SingleOrDefault(p => p.CategoryId == accessibleCategory.CategoryId && p.AccessibleForUser == true);
                    if (category != null)
                    {
                        string message = string.Empty;
                        string messageType = string.Empty;
                        //    //When admin remaps category to already temporarily mapped standard
                        if (category.AccessibleForUser == true)
                        {
                            message = ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateCategoryAdded, culture);
                            messageType = "message error";
                        }
                        else if (category.UnaccessibleForUser == true)
                        {
                            //when admin maps user to same role as of temporarily unmapped project
                            message = ConfigurationStore.GetApplicationMessages(MessageConstants.CategoryTemporarilyAdded, culture);
                            messageType = "message notification";
                            category.UnaccessibleForUser = false;
                        }
                        //When admin maps the user to already mapped project
                        else
                        {
                            message = ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateCategoryAdded, culture);
                            messageType = "message error";
                        }
                        ConfigureResultMessage(categoryAdditionResultDiv, messageType, message);
                    } //when admin maps the category to new standard
                    else
                    {
                        CategoryList.Add(accessibleCategory);
                        //Result div to show the message
                        ConfigureResultMessage(categoryAdditionResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.CategoryTemporarilyAdded, culture));
                    }
                    categoryListGridView.Visible = true;
                    categoryListGridView.DataSource = CategoryList.Where(p => p.UnaccessibleForUser == false);
                    categoryListGridView.DataBind();
                    if (categoryListGridView.HeaderRow != null)
                    {
                        categoryListGridView.HeaderRow.Visible = false;
                    }
                    categoryAdditionResultUpdatePanel.Update();
                    userTypeUpdatePanel.Update();
                }
                else
                {
                    ConfigureResultMessage(categoryAdditionResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.SelectOneCategory, culture));
                }
                categoryAdditionResultUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                categoryAdditionResultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Remove the category association from admin permissions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void CancelImage_Command(object sender, CommandEventArgs args)
        {
            try
            {
                string categoryId = ((ImageButton)(sender)).CommandArgument.ToString(CultureInfo.InvariantCulture);
                var category = CategoryList.SingleOrDefault(p => p.CategoryId.ToString(CultureInfo.CurrentCulture) == categoryId && p.UnaccessibleForUser == false);
                if (category.AccessibleForUser == true)
                {
                    ConfigureResultMessage(categoryAdditionResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.CategoryTemporarilyRemoved, culture));
                    category.AccessibleForUser = false;
                    CategoryList.Remove(category);
                }
                else
                {
                    //Remove the visibility of selected project
                    category.UnaccessibleForUser = true;
                    //Result div to show the message
                    ConfigureResultMessage(categoryAdditionResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.CategoryPermanentlyRemoved, culture));
                }
                //Bind the selected projects
                categoryListGridView.DataSource = CategoryList.Where(p => p.UnaccessibleForUser == false);
                categoryListGridView.DataBind();
                if (categoryListGridView.HeaderRow != null)
                {
                    categoryListGridView.HeaderRow.Visible = false;
                }
                //To show the result division
                categoryAdditionResultUpdatePanel.Update();
                userTypeUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                categoryAdditionResultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Validate the admin permission
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void AdminPermissionValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                int userTypeId = 0;
                bool isRegionSelected = false;
                bool isPermissionSelected = false;
                bool isMenuPermissionSelected = false;
                if (userTypeDropDownList.SelectedIndex != 0)
                {
                    userTypeId = Convert.ToInt32(userTypeDropDownList.SelectedValue, CultureInfo.InvariantCulture);
                }
                if (userTypeId == (int)UserTypeEnum.Administrator)
                {
                    foreach (ListItem item in regionsCheckBoxList.Items)
                    {
                        if (item.Selected == true)
                        {
                            isRegionSelected = true;
                            break;
                        }
                    }
                    foreach (ListItem item in permissionsCheckBoxList.Items)
                    {
                        if (item.Selected == true)
                        {
                            isPermissionSelected = true;

                            switch ((AdminPermissionEnum)Enum.Parse(typeof(AdminPermissionEnum), item.Value, true))
                            {
                                case AdminPermissionEnum.ProjectTemplates:
                                case AdminPermissionEnum.Projects:
                                case AdminPermissionEnum.Partner:
                                case AdminPermissionEnum.Products:
                                case AdminPermissionEnum.Settings:
                                case AdminPermissionEnum.Users:
                                case AdminPermissionEnum.Feedback:
                                case AdminPermissionEnum.Messages:
                                case AdminPermissionEnum.Transactions:
                                case AdminPermissionEnum.Dashboard:
                                    isMenuPermissionSelected = true;
                                    break;
                            }

                            if (isMenuPermissionSelected)
                                break;
                        }
                    }

                    if (isRegionSelected == false)
                    {
                        regionErrorMessageLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectOneRegion, culture);
                        regionErrorMessageDiv.Visible = true;
                        args.IsValid = false;
                    }

                    if (isPermissionSelected == false)
                    {
                        permissionErrorMessageLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectOnePermission, culture);
                        permissionErrorMessageDiv.Visible = true;
                        args.IsValid = false;
                    }
                    else if (isMenuPermissionSelected == false)
                    {
                        permissionErrorMessageLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectOneMenuPermission, culture);
                        permissionErrorMessageDiv.Visible = true;
                        args.IsValid = false;
                    }

                    userTypeUpdatePanel.Update();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Checks the all permissions when all check box is checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AllPermissionsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem item in permissionsCheckBoxList.Items)
                {
                    item.Selected = ((CheckBox)(sender)).Checked;
                }

                permissionErrorMessageLabel.Text = string.Empty;
                permissionErrorMessageDiv.Visible = false;

                if (allPermissionsCheckBox.Checked)
                {
                    if (!parentCategoryContainer.HasControls())
                    {
                        var categories = HelperManager.Instance.GetActiveCategories;
                        SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categories);
                        addCategoryAccessPermissionSpan.Visible = true;
                        int numberOfDropDowns = FindOccurence("ddlDynamic");
                        CreateDropDownList("ddlDynamic-" + Convert.ToString(numberOfDropDowns + 1, CultureInfo.InvariantCulture));
                        FindPopulateDropDowns(string.Empty, "ddlDynamic-" + Convert.ToString(numberOfDropDowns + 1, CultureInfo.InvariantCulture));
                    }
                }
                else
                {
                    RemoveAllCategoryDropDowns();
                    CategoryList = null;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Register the on click event on the click of associated projects grid row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssociatedProjectsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var data = e.Row.DataItem as AssociatedProject;
                    if (data.Property != null && string.IsNullOrWhiteSpace(data.Property.Address2))
                    {
                        var addressTwoLable = e.Row.FindControl("propertyAddressTwoLabel") as Label;
                        addressTwoLable.Visible = false;
                    }
                    else
                    {
                        var addressTwoLable = e.Row.FindControl("propertyAddressTwoLabel") as Label;
                        addressTwoLable.Text += "<br />";
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Register the on click event on the click of projects grid row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AllProjectsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onclick"] = "javascript:__doPostBack('" + allProjectsGridView.ClientID + "','" + e.Row.RowIndex.ToString(CultureInfo.InvariantCulture) + "')";
                    var data = e.Row.DataItem as AssociatedProject;
                    if (data.Property != null && string.IsNullOrWhiteSpace(data.Property.Address2))
                    {
                        var addressTwoLable = e.Row.FindControl("propertyAddressTwoLabel") as Label;
                        addressTwoLable.Visible = false;
                    }
                    else
                    {
                        var addressTwoLable = e.Row.FindControl("propertyAddressTwoLabel") as Label;
                        addressTwoLable.Text += "<br />";
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Set the style on all project grid view if selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AllProjectsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int selectedRowIndex = 0;
                if (Request["__EVENTARGUMENT"] != null && !string.IsNullOrWhiteSpace(Request["__EVENTARGUMENT"]))
                {
                    int number;
                    if (int.TryParse((Convert.ToString(Request["__EVENTARGUMENT"], CultureInfo.InvariantCulture)), out number))
                    {
                        selectedRowIndex = Convert.ToInt32(Convert.ToString(Request["__EVENTARGUMENT"], CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
                        if (e.Row.RowIndex == selectedRowIndex)
                        {
                            foreach (TableCell cell in e.Row.Cells)
                                cell.CssClass = "active";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// When All Region check box is checked/unchecked ,it checks/uncheck all individual regions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AllRegionsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem item in regionsCheckBoxList.Items)
                {
                    item.Selected = ((CheckBox)(sender)).Checked;
                }

                regionErrorMessageLabel.Text = string.Empty;
                regionErrorMessageDiv.Visible = false;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Remove the user project association
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelImage_Click(Object sender, EventArgs e)
        {
            try
            {
                int projectId = Convert.ToInt32(((ImageButton)(sender)).CommandArgument, CultureInfo.InvariantCulture);
                var associatedProject = GetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState) ?? new List<AssociatedProject>();
                var project = associatedProject.Single(p => p.ProjectId == projectId && p.IsToBeRemoved == false);
                if (project.IsToBeAdded == true)
                {
                    ConfigureResultMessage(userProjectMappingDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.TempUserProjectMappingRemoval, culture));
                    project.IsToBeAdded = false;
                    associatedProject.Remove(project);
                }
                else
                {
                    //Remove the visibility of selected project
                    project.IsToBeRemoved = true;
                    //Result div to show the message
                    ConfigureResultMessage(userProjectMappingDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.TempUserRemovalFromProject, culture));
                }
                //Bind the selected projects
                associatedProjectsGridView.DataSource = associatedProject.Where(p => p.IsToBeRemoved == false);
                associatedProjectsGridView.DataBind();
                SetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState, associatedProject);

                //To show the result division
                userProjectMappingUpdatePanel.Update();
                //To update the associated project
                userTypeUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Used for creating the dynamic drop down list
        /// </summary>
        /// <param name="Id"></param>
        protected void CreateDropDownList(string Id)
        {
            try
            {
                DropDownList parentCategoryDropDown = new DropDownList();
                HtmlGenericControl categoryDiv = new HtmlGenericControl("div");
                categoryDiv.Attributes.Add("class", "input no-pad select2");
                parentCategoryDropDown.ID = Id;
                parentCategoryDropDown.AutoPostBack = true;
                parentCategoryDropDown.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);
                categoryDiv.Controls.Add(parentCategoryDropDown);
                parentCategoryContainer.Controls.Add(categoryDiv);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Initialize the use manager instance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                ValidateUserAccess((int)MenuEnum.Users);

                backToUsersHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "‹‹ {0}", ResourceUtility.GetLocalizedString(246, culture, "Back To Users"));
                adminPermissionsLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(198, culture, "Administrator Permissions"));
                accesssLogHeaderLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(162, culture, "Access Log"));
                activationEmailLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(169, culture, "Activation Emails"));
                associatedProjectsLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(237, culture, "Associated Projects"));
                _userManager = UserManager.Instance;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handles the selected index changed of the dynamic created drop downs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = (DropDownList)sender;
                string id = ddl.ID;
                int existingDropDownCount = FindOccurence("ddlDynamic");
                string[] idSequence = id.Split('-');

                string currentSelectedCategoryId = ddl.SelectedValue.ToString(CultureInfo.InvariantCulture);
                // remove all the below dynamic drop down.
                for (int dropdownCount = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1; dropdownCount <= existingDropDownCount; dropdownCount++)
                {
                    parentCategoryContainer.Controls.RemoveAt(dropdownCount - 1);
                }
                if (currentSelectedCategoryId != "-1")
                {
                    Category currentSelectedCategory = ReadCategory(currentSelectedCategoryId);
                    if (!(currentSelectedCategory.IsLeaf))
                    {
                        IList<Category> childCategoryList = GetChildCategories(currentSelectedCategoryId);
                        if (childCategoryList.Count() > 0)
                        {
                            int nextID = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1;
                            CreateDropDownList("ddlDynamic-" + nextID.ToString(CultureInfo.InvariantCulture));
                            FindPopulateDropDowns(currentSelectedCategoryId, "ddlDynamic-" + nextID.ToString(CultureInfo.InvariantCulture));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Recreate the dynamic controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                SetGridViewHeaderText();
                if (parentCategoryContainer == null)
                {
                    parentCategoryContainer = new PlaceHolder();
                }
                RecreateControls("ddlDynamic", "DropDownList");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Bind the data to the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    int selectedRowIndex = 0;
                    if (!string.IsNullOrWhiteSpace(Request["__EVENTARGUMENT"]) && Request["__EVENTTARGET"] != null && Request["__EVENTTARGET"].ToString(CultureInfo.InvariantCulture).Contains(allProjectsGridView.ID))
                    {
                        selectedRowIndex = Convert.ToInt32(Convert.ToString(Request["__EVENTARGUMENT"], CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
                        selectedRowHiddenField.Value = Convert.ToString(allProjectsGridView.DataKeys[selectedRowIndex].Value, CultureInfo.InvariantCulture);
                    }

                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    categoryAdditionResultDiv.Attributes.Remove("class");
                    categoryAdditionResultDiv.InnerHtml = string.Empty;

                    resultUpdatePanel.Update();
                    categoryAdditionResultUpdatePanel.Update();

                    if (!IsPostBack)
                    {
                        this.Title = ResourceUtility.GetLocalizedString(1107, culture, "User Details");

                        User user = GetSession<User>(SessionConstants.User);

                        var isAdministrativeUser = user.Permissions.Any(x => x.PermissionId == (int)AdminPermissionEnum.AdministrativeUsers);// Administrative User permission
                        if (isAdministrativeUser)
                            isAdminUserHiddenField.Value = "1";

                        int userId = Convert.ToInt32(Context.Items["UserId"], CultureInfo.InvariantCulture);
                        userIdHiddenField.Value = userId.ToString(CultureInfo.InvariantCulture);

                        PopulateDropDownData();

                        SetRegularExpressions();

                        Entities.User _user = _userManager.GetUserDetails(userId);
                        if (Context.Items[UIConstants.Mode] != null && Context.Items[UIConstants.Mode].ToString() == UIConstants.Edit)
                        {
                            SetPageSettings(UIConstants.Edit);
                            LoadDataForEditUser(_user);
                        }
                        else
                        {
                            SetPageSettings(UIConstants.Add);
                        }

                        IList<SearchFilters> searchFilterData = UserManager.Instance.GetSearchFilterItems;
                        SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilterData);

                        //Select the userType
                        switch (_user.UserTypeId)
                        {
                            case (int)UserTypeEnum.Partner:
                                userTypeDropDownList.SelectedValue = _user.UserTypeId.ToString(CultureInfo.InvariantCulture);
                                DisplayUserTypeDiv(UserTypeEnum.Partner);
                                var associatedPartners = _user.AssociatedPartners;
                                SetViewState<IList<Partner>>(ViewStateConstants.PartnerDetails, associatedPartners);

                                //Bind the selected projects
                                gridviewPartners.DataSource = associatedPartners;
                                gridviewPartners.DataBind();
                                userTypeUpdatePanel.Update();

                                //Set the associated vendor
                                //if (_user.Company != null)
                                //{
                                //    ListItem itemToSelect = associatedVendorDropDownList.Items.FindByValue(_user.Company);
                                //    if (itemToSelect != null)
                                //    {
                                //        itemToSelect.Selected = true;
                                //    }
                                //}
                                updateUserIdUpdatePanel.Update();
                                activationEmailDiv.Visible = false;
                                logUpdatePanel.Update();
                                break;

                            case (int)UserTypeEnum.Owner:
                            case (int)UserTypeEnum.Consultant:
                                userTypeDropDownList.SelectedValue = _user.UserTypeId.ToString(CultureInfo.InvariantCulture);
                                DisplayUserTypeDiv(UserTypeEnum.Owner);
                                var associatedProject = _user.AssociatedProjects;
                                SetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState, associatedProject);

                                PopulateSearchDropDownData();

                                //Bind the selected projects
                                associatedProjectsGridView.DataSource = associatedProject;
                                associatedProjectsGridView.DataBind();
                                break;

                            case (int)UserTypeEnum.Administrator:
                                userTypeDropDownList.SelectedValue = _user.UserTypeId.ToString(CultureInfo.InvariantCulture);
                                DisplayUserTypeDiv(UserTypeEnum.Administrator);

                                //Populate the selected region
                                foreach (var selectedRegions in _user.AdminPermission.Regions)
                                {
                                    foreach (ListItem region in regionsCheckBoxList.Items)
                                    {
                                        if (region.Value == selectedRegions.RegionId.ToString(CultureInfo.InvariantCulture))
                                        {
                                            region.Selected = true;
                                            break;
                                        }
                                    }
                                }
                                if (_user.AdminPermission.Regions.Count == regionsCheckBoxList.Items.Count)
                                {
                                    allRegionsCheckBox.Checked = true;
                                }

                                //Populates the selected permissions
                                foreach (var selectedPermission in _user.AdminPermission.Permissions)
                                {
                                    foreach (ListItem permission in permissionsCheckBoxList.Items)
                                    {
                                        if (permission.Value == selectedPermission.PermissionId.ToString(CultureInfo.InvariantCulture))
                                        {
                                            permission.Selected = true;
                                            break;
                                        }
                                    }
                                }
                                if (permissionsCheckBoxList.Items.FindByValue(((int)AdminPermissionEnum.CategoryEntry).ToString(CultureInfo.InvariantCulture)).Selected)
                                {
                                    var categories = HelperManager.Instance.GetActiveCategories;
                                    SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categories);
                                    int cnt = FindOccurence("ddlDynamic");
                                    addCategoryAccessPermissionSpan.Visible = true;
                                    CreateDropDownList("ddlDynamic-" + Convert.ToString(cnt + 1, CultureInfo.InvariantCulture));
                                    FindPopulateDropDowns(string.Empty, "ddlDynamic-" + Convert.ToString(cnt + 1, CultureInfo.InvariantCulture));
                                }
                                else
                                {
                                    RemoveAllCategoryDropDowns();
                                }

                                if (_user.AdminPermission.Permissions.Count == permissionsCheckBoxList.Items.Count)
                                {
                                    allPermissionsCheckBox.Checked = true;
                                }
                                if (_user.AccessibleCategoryList.Count > 0)
                                {
                                    CategoryList = _user.AccessibleCategoryList;
                                    categoryListGridView.DataSource = _user.AccessibleCategoryList;
                                    categoryListGridView.DataBind();
                                    if (categoryListGridView.HeaderRow != null)
                                        categoryListGridView.HeaderRow.Visible = false;
                                }
                                activationEmailDiv.Visible = false;
                                logUpdatePanel.Update();
                                break;
                            default:                                        //For new user
                                userTypeDropDownList.SelectedValue = _user.UserTypeId.ToString(CultureInfo.InvariantCulture);
                                associatedProjectDiv.Visible = false;
                                associatedVendorDiv.Visible = false;
                                adminPanel.Visible = false;
                                activationEmailDiv.Visible = false;
                                accessLogDiv.Visible = false;
                                logUpdatePanel.Update();
                                loginAsThisUserSpan.Visible = false;

                                PopulateSearchDropDownData();
                                break;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(userTypeUpdatePanel, this.GetType(), "InitializeScript", "InitializeScript();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// This method with set the validation expression of the validator based on the culture
        /// </summary>
        private void SetRegularExpressions()
        {
            SetValidatorRegularExpressions(firstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(hiltonIdRegularExpression, (int)RegularExpressionTypeEnum.HiltonIdValidator);
        }

        /// <summary>
        /// Populate the static drop downs
        /// </summary>
        private void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.UserTypesDropDown, userTypeDropDownList, ResourceUtility.GetLocalizedString(1247, culture, "Select a User Type"), null);
            BindStaticDropDown(DropDownConstants.UserStatusDropDown, userStatusDropDown, null, null);
            BindDynamicDropDown(DropDownConstants.PartnerDropDown, associatedVendorDropDownList, ResourceUtility.GetLocalizedString(1233, culture, "Select a Partner"), null);
            BindStaticDropDown(DropDownConstants.OwnerAndConsultantRolesDropDown, userRoleDropDownList, ResourceUtility.GetLocalizedString(1253, culture, "Role"), null);

            if (isAdminUserHiddenField.Value != "1")
            {
                userTypeDropDownList.Items.Remove(new ListItem(UserTypeEnum.Administrator.ToString(), ((int)UserTypeEnum.Administrator).ToString(CultureInfo.InvariantCulture)));
            }

            regionsCheckBoxList.DataSource = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
            regionsCheckBoxList.DataValueField = "DataValueField";
            regionsCheckBoxList.DataTextField = "DataTextField";
            regionsCheckBoxList.DataBind();

            //Load all permission check box list
            permissionsCheckBoxList.DataSource = GetStaticDropDownDataSource(DropDownConstants.AdminPermissionsDropDown);
            permissionsCheckBoxList.DataTextField = "DataTextField";
            permissionsCheckBoxList.DataValueField = "DataValueField";
            permissionsCheckBoxList.DataBind();
        }

        /// <summary>
        /// Handles the check box checked event of permission check box list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PermissionsCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var isAllChecked = true;
                foreach (ListItem item in permissionsCheckBoxList.Items)
                {
                    if (item.Selected == false)
                    {
                        allPermissionsCheckBox.Checked = false;
                        isAllChecked = false;
                        break;
                    }
                }
                permissionErrorMessageLabel.Text = string.Empty;
                permissionErrorMessageDiv.Visible = false;

                allPermissionsCheckBox.Checked = isAllChecked;
                //auto select the user when admin user is selected,
                //7 -- admin permission check box, 6 -- user check box
                ListItem masterAdmin = permissionsCheckBoxList.Items.FindByValue(((int)AdminPermissionEnum.AdministrativeUsers).ToString(CultureInfo.InvariantCulture));
                if (masterAdmin.Selected == true)
                {
                    foreach (ListItem item in permissionsCheckBoxList.Items)
                    {
                        if (item.Value == ((int)AdminPermissionEnum.Users).ToString(CultureInfo.InvariantCulture))
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
                //when category is checked create the category dropdown
                CheckBoxList list = (CheckBoxList)sender;
                string[] control = Request.Form.Get("__EVENTTARGET").Split('$');
                int index = control.Length - 1;
                string selectedValue = list.Items[Int32.Parse(control[index], CultureInfo.InvariantCulture)].Value;

                if (selectedValue.Equals(((int)AdminPermissionEnum.CategoryEntry).ToString(CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                {
                    if (list.Items[Int32.Parse(control[index], CultureInfo.InvariantCulture)].Selected)
                    {
                        var categories = HelperManager.Instance.GetActiveCategories;
                        SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categories);
                        int cnt = FindOccurence("ddlDynamic");
                        addCategoryAccessPermissionSpan.Visible = true;
                        CreateDropDownList("ddlDynamic-" + Convert.ToString(cnt + 1, CultureInfo.InvariantCulture));
                        FindPopulateDropDowns(string.Empty, "ddlDynamic-" + Convert.ToString(cnt + 1, CultureInfo.InvariantCulture));

                    }
                    else if (!(list.Items[Int32.Parse(control[index], CultureInfo.InvariantCulture)].Selected))//for category entry
                    {
                        CategoryList = null;
                        RemoveAllCategoryDropDowns();
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Set the associated Project grid view header
        /// </summary>
        private void SetGridViewHeaderText()
        {
            associatedProjectsGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(555, culture, "Project Type");
            associatedProjectsGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(427, culture, "Location");
            associatedProjectsGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(630, culture, "Role");
        }

        /// <summary>
        /// Remove all category dropdowns
        /// </summary>
        private void RemoveAllCategoryDropDowns()
        {
            try
            {
                parentCategoryContainer.Controls.Clear();
                categoryAdditionResultDiv.Style.Clear();
                categoryAdditionResultDiv.Attributes.Clear();
                categoryAdditionResultDiv.InnerHtml = String.Empty;
                categoryAdditionResultUpdatePanel.Update();
                categoryListGridView.Visible = false;
                addCategoryAccessPermissionSpan.Visible = false;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Search the projects for a given project type, brand and region combination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProjectsSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                selectedRowHiddenField.Value = string.Empty;
                int projectTypeId = 0;
                int brandId = 0;
                int regionId = 0;
                IList<AssociatedProject> projects = new List<AssociatedProject>();
                if (projectTypeDropDown.SelectedItem != null)
                {
                    projectTypeId = Convert.ToInt32(projectTypeDropDown.SelectedItem.Value, CultureInfo.InvariantCulture);
                }
                if (brandsDropDown.SelectedItem != null)
                {
                    brandId = Convert.ToInt32(brandsDropDown.SelectedItem.Value, CultureInfo.InvariantCulture);
                }
                if (regionDropDown.SelectedItem != null)
                {
                    regionId = Convert.ToInt32(regionDropDown.SelectedItem.Value, CultureInfo.InvariantCulture);
                }
                projects = _userManager.SearchProject(projectTypeId, brandId, regionId);

                if (projects != null && projects.Count > 0)
                {
                    allProjectDiv.Attributes.Add("class", "scroll");
                }
                else
                {
                    allProjectDiv.Attributes.Remove("class");
                }
                allProjectsGridView.DataSource = projects;
                allProjectsGridView.DataBind();
                if (allProjectsGridView.HeaderRow != null)
                {
                    allProjectsGridView.HeaderRow.Visible = false;
                }
                SetViewState<IList<AssociatedProject>>(ViewStateConstants.AllProjectsViewState, projects);

                //Hide the add role button and role dropdown
                addRoleDiv.Visible = projects.Count > 0 ? true : false;
                if (projects.Count == 0)
                {
                    addRoleDiv.Visible = false;
                    ConfigureResultMessage(allProjectGridViewDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.EmptyProjectListErrorMessage, culture));
                    allProjectGridViewUpdatePanel.Update();
                }
                else
                {
                    addRoleDiv.Visible = true;
                    allProjectGridViewDiv.Visible = false;
                }
                userTypeUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Filter the brand and region drop down when project type drop down is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProjectTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProcessDropDownItemChange(DropDownConstants.ProjectTypesDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Filter the brand and project type drop down when region drop down is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProcessDropDownItemChange(DropDownConstants.RegionDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Filters the project type and region drop down when brand is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BrandsDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProcessDropDownItemChange(DropDownConstants.BrandDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Process the drop down item change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        private void ProcessDropDownItemChange(string sourceDropDown)
        {
            int brandSelectedValue = Convert.ToInt32(brandsDropDown.SelectedValue, CultureInfo.CurrentCulture);
            int projectTypeSelectedValue = Convert.ToInt32(projectTypeDropDown.SelectedValue, CultureInfo.CurrentCulture);
            int regionSelectedValue = Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.CurrentCulture);

            IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
            IList<SearchFilters> filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, projectTypeSelectedValue);

            if (brandSelectedValue == 0 && projectTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.ProjectTypesDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);

                filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, null);
                BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), null);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), null);
            }
            else if (brandSelectedValue == 0 && projectTypeSelectedValue != 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);
                filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, null, projectTypeSelectedValue);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), null);
                BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), null);
            }
            else if (brandSelectedValue != 0 && projectTypeSelectedValue == 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.ProjectTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
            {
                BindDropDown(searchFiltersData, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), null);
                BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), null);
            }
            else
            {
                if (brandSelectedValue != 0 && projectTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && projectTypeSelectedValue != 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.BrandDropDown || sourceDropDown == DropDownConstants.ProjectTypesDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);

                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                        BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.ProjectTypesDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, null, projectTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                    }
                }
                else if (brandSelectedValue == 0 && projectTypeSelectedValue != 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.ProjectTypesDropDown || sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);

                    if (sourceDropDown == DropDownConstants.ProjectTypesDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, null, projectTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);
                    }
                }

                else if (brandSelectedValue == 0 && projectTypeSelectedValue != 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.BrandDropDown))
                {
                    filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, null, projectTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);

                    filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, null);
                    BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);
                }
                else if (brandSelectedValue != 0 && projectTypeSelectedValue != 0 && regionSelectedValue != 0)
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, projectTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);

                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, regionSelectedValue, null);
                        BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);

                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, projectTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.ProjectTypesDropDown)
                    {
                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, projectTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);

                        filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, regionSelectedValue, projectTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                    }
                }
                else if (brandSelectedValue != 0 && projectTypeSelectedValue != 0 && regionSelectedValue == 0 && (sourceDropDown == DropDownConstants.RegionDropDown))
                {
                    filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);

                    filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, null, projectTypeSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                }

                else if (brandSelectedValue != 0 && projectTypeSelectedValue == 0 && regionSelectedValue != 0 && (sourceDropDown == DropDownConstants.ProjectTypesDropDown))
                {
                    filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, brandSelectedValue, null, null);
                    BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);

                    filteredCollection = _userManager.GetFilteredCollection(searchFiltersData, null, null, regionSelectedValue);
                    BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                }
                else
                {
                    if (sourceDropDown == DropDownConstants.BrandDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.ProjectTypesDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), regionSelectedValue);
                    }
                    else if (sourceDropDown == DropDownConstants.RegionDropDown)
                    {
                        BindDropDown(filteredCollection, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), brandSelectedValue);
                        BindDropDown(filteredCollection, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1198, culture, "All Types"), projectTypeSelectedValue);
                    }
                }
            }
        }

        /// <summary>
        /// Checks/Uncheck the All Region checkbox if all the regions are checked or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionsCheckBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var isAllChecked = true;
                foreach (ListItem item in regionsCheckBoxList.Items)
                {
                    if (item.Selected == false)
                    {
                        allRegionsCheckBox.Checked = false;
                        isAllChecked = false;
                        break;
                    }
                }
                allRegionsCheckBox.Checked = isAllChecked;

                regionErrorMessageLabel.Text = string.Empty;
                regionErrorMessageDiv.Visible = false;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// .NET will refuse to accept "unknown" post backs for security reasons.
        /// Because of this we have to register all possible callbacks.
        /// This must be done in Render, hence the override for selecting row on mouse click
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            for (int i = 0; i < allProjectsGridView.Rows.Count; i++)
            {
                Page.ClientScript.RegisterForEventValidation(
                        new System.Web.UI.PostBackOptions(
                            allProjectsGridView, "Select$" + i.ToString(CultureInfo.InvariantCulture)));
            }
            // Do the standard rendering stuff
            base.Render(writer);
        }

        /// <summary>
        /// Validate the controls before Partner mapping
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>

        protected void partnerNameCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (associatedVendorDropDownList.SelectedIndex == 0)
                {
                    args.IsValid = false;
                    partnerNameCustomValidator.ErrorMessage = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectAtleastOnePartner, culture);                   

                }

                IList<Partner> partnerSpecifications = GetViewState<IList<Partner>>(ViewStateConstants.PartnerDetails);
                if (partnerSpecifications == null)
                {
                    partnerSpecifications = new List<Partner>();
                }

                if (partnerSpecifications.Any(x => x.PartnerId == associatedVendorDropDownList.SelectedValue))
                {
                    args.IsValid = false;
                    partnerNameCustomValidator.ErrorMessage = ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicatePartner, culture); 
                }

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }


        /// <summary>
        /// Validate the controls before role mapping
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void RoleMappingValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                //DataKey selectedDataKey = new DataKey;
                if (string.IsNullOrEmpty(selectedRowHiddenField.Value) && userRoleDropDownList.SelectedIndex == 0)
                {
                    args.IsValid = false;
                    validateRoleMapping.ErrorMessage = ConfigurationStore.GetApplicationMessages(MessageConstants.ChooseProjectForRoleMapping, culture)
                        + UIConstants.And + ConfigurationStore.GetApplicationMessages(MessageConstants.ChooseRoleForProjectMapping, culture);
                    userProjectMappingDiv.Visible = false;
                }
                else if (selectedRowHiddenField == null || string.IsNullOrEmpty(selectedRowHiddenField.Value))
                {
                    args.IsValid = false;
                    validateRoleMapping.ErrorMessage = ConfigurationStore.GetApplicationMessages(MessageConstants.ChooseProjectForRoleMapping, culture);
                    userProjectMappingDiv.Visible = false;
                }
                else if (userRoleDropDownList.SelectedIndex == 0)
                {
                    args.IsValid = false;
                    validateRoleMapping.ErrorMessage = ConfigurationStore.GetApplicationMessages(MessageConstants.ChooseRoleForProjectMapping, culture);
                    userProjectMappingDiv.Visible = false;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Login as other user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoginAsUserLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(hiltonIdHiddenField.Value))
                {
                    ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.LoginOnAsUserHiltonIdMissing, culture));
                }
                else
                {
                    string hiltonUserName = hiltonIdHiddenField.Value;

                    CreateLoggedInUserSession(hiltonUserName);
                    ClearSession(SessionConstants.WebMenu);
                    ClearSession(SessionConstants.AdminMenu);                    
                    Response.Redirect("~/", false);
                    //Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority),false);
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Save or update the user info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    bool isCategoryAccessValid = true;
                    bool isPartnersValid = true;
                    //set the common data
                    var _user = new Entities.User()
                     {
                         FirstName = firstNameTextBox.Text,
                         LastName = lastNametextBox.Text,
                         Email = emailAddressTextBox.Text,
                         HiltonUserName = string.IsNullOrWhiteSpace(hiltonIdTextBox.Text) ? string.Empty : hiltonIdTextBox.Text.Trim(),
                         UserStatusId = Convert.ToInt32(userStatusDropDown.SelectedItem.Value, CultureInfo.InvariantCulture),
                         UserId = Convert.ToInt32(userIdHiddenField.Value, CultureInfo.InvariantCulture),
                         Culture = Convert.ToString(culture, CultureInfo.InvariantCulture)
                     };
                    _user.UserTypeId = Convert.ToInt32(userTypeDropDownList.SelectedValue, CultureInfo.InvariantCulture);
                    int userId = -1;
                    //set the user specific data
                    switch (_user.UserTypeId)
                    {
                        case (int)UserTypeEnum.Partner:                           
 
                            _user.AssociatedPartners = GetViewState<IList<Partner>>(ViewStateConstants.PartnerDetails) ?? new List<Partner>();
                            if( _user.AssociatedPartners == null || _user.AssociatedPartners.Count == 0)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.AddAtleastOnePartner, culture));
                                isPartnersValid = false;
                            }
                            //_user.Company = associatedVendorDropDownList.SelectedItem.Value;
                            break;

                        case (int)UserTypeEnum.Owner:
                        case (int)UserTypeEnum.Consultant:
                            _user.AssociatedProjects = GetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState) ?? new List<AssociatedProject>();
                            break;

                        case (int)UserTypeEnum.Administrator:
                            _user.AdminPermission = new AdminPermission();
                            _user.AdminPermission.Permissions = new List<Permission>();
                            _user.AdminPermission.Regions = new List<Region>();
                            foreach (ListItem region in regionsCheckBoxList.Items)
                            {
                                if (region.Selected == true)
                                {
                                    _user.AdminPermission.Regions.Add(
                                        new Region()
                                        {
                                            RegionId = Convert.ToInt32(region.Value, CultureInfo.InvariantCulture)
                                        });
                                }
                            }
                            foreach (ListItem permission in permissionsCheckBoxList.Items)
                            {
                                if (permission.Selected == true)
                                {
                                    _user.AdminPermission.Permissions.Add(
                                        new Permission()
                                        {
                                            PermissionId = Convert.ToInt32(permission.Value, CultureInfo.InvariantCulture)
                                        });
                                }
                            }
                            var categoryList = CategoryList;
                            if (_user.AdminPermission.Permissions.Any(x => x.PermissionId == (int)AdminPermissionEnum.CategoryEntry) && categoryList.Count == 0)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SelectOneCategory, culture));
                                isCategoryAccessValid = false;
                            }

                            _user.AccessibleCategoryList = categoryList;
                            break;
                    }
                    if (isCategoryAccessValid && isPartnersValid)
                    {
                        var user = GetSession<User>(SessionConstants.User);
                        if (GetViewState<string>(UIConstants.Mode) == UIConstants.Add)
                        {
                            userId = _userManager.AddUser(_user, user.UserId);
                            if (userId == -1)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserAdditionFailed, culture));
                            }
                            else if (userId == -2)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateHiltonIdErrorMessage, culture));
                            }
                            else if (userId == -3)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserExists, culture));
                            }
                            else
                            {
                                ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.UserAdditionSuccess, culture));
                                idLabel.Text = userId.ToString(CultureInfo.InvariantCulture);

                                hiltonIdHiddenField.Value = hiltonIdTextBox.Text;
                                //After the successful save get the user id andd update the mode to edit
                                userIdHiddenField.Value = userId.ToString(CultureInfo.InvariantCulture);
                                SetViewState<string>(UIConstants.Mode, UIConstants.Edit);

                                //To set the visibility based on user accessibility
                                loginAsThisUserSpan.Visible = true;

                                _user.UserId = userId;
                                UpdateAssociatedProjects(_user);

                                userTypeDropDownList.Enabled = false;
                                //To update the user id after successful save
                                updateUserIdUpdatePanel.Update();
                            }
                        }
                        else if (GetViewState<string>(UIConstants.Mode) == UIConstants.Edit)
                        {
                            userId = _userManager.UpdateUser(_user, user.UserId);
                            if (userId == -1)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserUpdateFailed, culture));
                            }
                            else if (userId == -2)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateHiltonIdErrorMessage, culture));
                            }
                            else if (userId == -3)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserExists, culture));
                            }
                            else
                            {
                                ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.UserUpdateSuccess, culture));
                                hiltonIdHiddenField.Value = hiltonIdTextBox.Text;
                                UpdateAssociatedProjects(_user);

                                if (_user.UserId == user.UserId)
                                {
                                    UpdateUserMenuAndPermissionAccess(user.UserId);
                                }
                            }
                        }
                        userTypeUpdatePanel.Update();
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Update Activation Email Visibility
        /// </summary>
        /// <param name="p"></param>
        private void UpdateActivationEmail(int userId)
        {
            var activationEmail = _userManager.GetActivationEmails(userId);
            activationEmailGridView.DataSource = activationEmail;
            activationEmailGridView.DataBind();
            if (activationEmailGridView.HeaderRow != null)
            {
                activationEmailGridView.HeaderRow.Visible = false;
            }
        }

        /// <summary>
        /// Updates User Menu Access Permission
        /// </summary>
        /// <param name="p"></param>
        private void UpdateUserMenuAndPermissionAccess(int userId)
        {
            //Update user menu
            var userPermissions = SecurityManager.Instance.GetMenus((int)UserTypeEnum.Administrator, true, userId);
            //set menu permission in session
            SetSession<IList<UserPermissions>>(SessionConstants.AdminMenu, userPermissions);

            //Update User permission access
            User user = GetSession<User>(SessionConstants.User);
            user.MenuAccess = SecurityManager.Instance.UpdateMenuAccess(userId);
        }

        /// <summary>
        /// Update the associated Project details
        /// </summary>
        /// <param name="user"></param>
        private void UpdateAssociatedProjects(User user)
        {
            if (user.UserTypeId == (int)UserTypeEnum.Consultant || user.UserTypeId == (int)UserTypeEnum.Owner)
            {
                var associatedProjects = GetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState) ?? new List<AssociatedProject>();
                var updatedAssociatedProjects = new List<AssociatedProject>();
                foreach (var project in associatedProjects)
                {
                    if (project.IsToBeRemoved == false)
                    {
                        updatedAssociatedProjects.Add(project);
                    }
                }
                updatedAssociatedProjects.ToList().ForEach(p => p.IsToBeAdded = false);
                SetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState, updatedAssociatedProjects);

                associatedProjectsGridView.DataSource = updatedAssociatedProjects;
                associatedProjectsGridView.DataBind();

                UpdateOwnerConsultantDivVisibility();

                UpdateActivationEmail(user.UserId);
            }
            else
            {
                accessLogDiv.Visible = true;
                logUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Update the user module visibility for owner and consultant type user
        /// </summary>
        private void UpdateOwnerConsultantDivVisibility()
        {
            userProjectMappingDiv.Visible = false;
            userProjectMappingUpdatePanel.Update();

            activationEmailDiv.Visible = true;
            accessLogDiv.Visible = true;
            logUpdatePanel.Update();
        }

        /// <summary>
        /// Displays the section as per the selected user type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UserTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Convert the selected value of string type to user type enumerator
                UserTypeEnum userType = (UserTypeEnum)Enum.Parse(typeof(UserTypeEnum), ((DropDownList)sender).SelectedValue);
                DisplayUserTypeDiv(userType);
                allProjectGridViewDiv.Visible = false;
                //When user type is changed than refresh the projects mapping
                if (UserTypeEnum.Consultant == userType || UserTypeEnum.Owner == userType)
                {
                    associatedProjectsGridView.DataSource = null;
                    associatedProjectsGridView.DataBind();

                    allProjectsGridView.DataSource = null;
                    allProjectsGridView.DataBind();

                    SetViewState<IList<AssociatedProject>>(ViewStateConstants.AllProjectsViewState, null);
                    SetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState, null);

                    userProjectMappingDiv.Visible = false;

                    projectTypeDropDown.SelectedIndex = 0;
                    brandsDropDown.SelectedIndex = 0;
                    regionDropDown.SelectedIndex = 0;
                    userRoleDropDownList.SelectedIndex = 0;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Add the temporary user project role mapping
        /// </summary>
        /// <param name="projectId"></param>
        private void AddTempUserProjectMapping(int projectId)
        {
            var allProjects = GetViewState<IList<AssociatedProject>>(ViewStateConstants.AllProjectsViewState) ?? new List<AssociatedProject>();
            var selectedProject = allProjects.Single(p => p.ProjectId == projectId);
            if (selectedProject.User == null)
            {
                selectedProject.User = new User();
            }
            selectedProject.User.UserRoleId = Convert.ToInt32(userRoleDropDownList.SelectedValue, CultureInfo.InvariantCulture);
            selectedProject.User.UserRole = userRoleDropDownList.SelectedItem.Text;
            selectedProject.IsToBeAdded = true;
            var associatedProject = GetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState) ?? new List<AssociatedProject>();
            associatedProject.Add(selectedProject);
            SetViewState<IList<AssociatedProject>>(ViewStateConstants.AssociatedProjectViewState, associatedProject);
        }

        /// <summary>
        /// Display the division depending on the user type selected
        /// </summary>
        /// <param name="userType"></param>
        private void DisplayUserTypeDiv(UserTypeEnum userType)
        {
            switch (userType)
            {
                case UserTypeEnum.Partner:                                   //For Partner
                    associatedProjectDiv.Visible = false;
                    associatedVendorDiv.Visible = true;
                    adminPanel.Visible = false;
                    break;

                case UserTypeEnum.Owner:                                   //For Owner/Consultant
                case UserTypeEnum.Consultant:
                    associatedProjectDiv.Visible = true;
                    associatedVendorDiv.Visible = false;
                    adminPanel.Visible = false;
                    break;

                case UserTypeEnum.Administrator:                                   //For Admin
                    associatedProjectDiv.Visible = false;
                    associatedVendorDiv.Visible = false;
                    adminPanel.Visible = true;
                    break;
                default:
                    associatedProjectDiv.Visible = false;
                    associatedVendorDiv.Visible = false;
                    adminPanel.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Used to find the  occurrence of the dynamic created drop down list
        /// </summary>
        /// <param name="substr"></param>
        /// <returns></returns>
        private int FindOccurence(string substr)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int count = 0;
            for (int i = 0; i < ctrls.Length; i++)
            {
                if (ctrls[i].Contains(substr + "-") && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Find the populated drop down
        /// </summary>
        /// <param name="selectedCategoryId"></param>
        /// <param name="id"></param>
        private void FindPopulateDropDowns(string selectedCategoryId, string id)
        {
            DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, id);
            PopulateDropDown(newDropDown, selectedCategoryId);
        }

        /// <summary>
        /// Get the child category
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            var categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            IList<Category> categoryList = (categories.AsEnumerable().
                                            Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture))).ToList());
            return categoryList;
        }

        /// <summary>
        /// Get the root level category
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetRootLevelCategories()
        {
            var categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            IList<Category> rootCategoryList = (categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
            return rootCategoryList;
        }

        /// <summary>
        /// Load the data for edit user
        /// </summary>
        /// <param name="user"></param>
        private void LoadDataForEditUser(User user)
        {
            //Load the basic information
            idLabel.Text = user.UserId.ToString(CultureInfo.InvariantCulture);
            firstNameTextBox.Text = user.FirstName;
            lastNametextBox.Text = user.LastName;
            emailAddressTextBox.Text = user.Email;
            if (user.HiltonUserName != string.Empty)
            {
                hiltonIdTextBox.Text = user.HiltonUserName.ToString(CultureInfo.InvariantCulture);
                hiltonIdHiddenField.Value = user.HiltonUserName.ToString(CultureInfo.InvariantCulture);
            }
            //Set the selected value for user status
            userStatusDropDown.Items.FindByValue(user.UserStatusId.ToString(CultureInfo.InvariantCulture)).Selected = true;

            //Load the access log
            accessLogGridView.DataSource = user.UserAccessLogs;
            accessLogGridView.DataBind();
            if (accessLogGridView.HeaderRow != null)
            {
                accessLogGridView.HeaderRow.Visible = false;
            }

            //Load the activation emails
            activationEmailGridView.DataSource = user.ActivationEmailDetails;
            activationEmailGridView.DataBind();
            if (activationEmailGridView.HeaderRow != null)
            {
                activationEmailGridView.HeaderRow.Visible = false;
            }
            //Disable the change of user type
            userTypeDropDownList.Enabled = false;
        }

        /// <summary>
        /// Used to populate the dynamically created drop down
        /// </summary>
        /// <param name="parentCategoryDropDown"></param>
        /// <param name="selectedValue"></param>
        private void PopulateDropDown(DropDownList parentCategoryDropDown, string selectedValue)
        {
            parentCategoryDropDown.Items.Clear();
            IList<Category> categoryList = new List<Category>();
            if (String.IsNullOrEmpty(selectedValue))
            {
                categoryList = GetRootLevelCategories();
            }
            else if (selectedValue != "-1")
            {
                categoryList = GetChildCategories(selectedValue);
            }
            if (categoryList != null)
            {
                BindData(parentCategoryDropDown, categoryList);
            }
            parentCategoryDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1243, culture, "Select a Category"), "-1"));
        }

        /// <summary>
        ///  Populate the progressive drop down data
        /// </summary>
        private void PopulateSearchDropDownData()
        {
            IList<SearchFilters> searchFilters = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);

            BindDropDown(searchFilters, DropDownConstants.BrandDropDown, brandsDropDown, ResourceUtility.GetLocalizedString(1188, culture, "All Brands"), null);
            BindDropDown(searchFilters, DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1220, culture, "All Types"), null);
            BindDropDown(searchFilters, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1207, culture, "All Regions"), null);
        }

        /// <summary>
        /// Find the currently selected selected drop down
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(string currentSelectedCategoryId)
        {
            var categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            Category category = (Category)(categories.Where(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)).Single());
            return category;
        }

        /// <summary>
        /// Re create the dynamic drop downs
        /// </summary>
        /// <param name="ctrlPrefix"></param>
        /// <param name="ctrlType"></param>
        private void RecreateControls(string ctrlPrefix, string ctrlType)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int cnt = FindOccurence(ctrlPrefix);
            if (cnt > 0)
            {
                for (int controlCount = 1; controlCount <= cnt; controlCount++)
                {
                    for (int i = 0; i < ctrls.Length; i++)
                    {
                        if (ctrls[i].Contains(ctrlPrefix + "-" + controlCount.ToString(CultureInfo.InvariantCulture)) && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                        {
                            string ctrlID = string.Empty;
                            string[] subCtrls = ctrls[i].Split('-');

                            for (int l = 0; l < subCtrls.Length; l++)
                            {
                                if (subCtrls[l].Contains("ddlDynamic"))
                                {
                                    ctrlID = subCtrls[l + 1].Substring(0, 1);
                                }
                            }

                            ctrlID = "ddlDynamic-" + ctrlID;

                            if (ctrlType == "DropDownList")
                            {
                                CreateDropDownList(ctrlID);
                            }
                            break;
                        }
                    }
                }
            }
        }

        protected void LinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (associatedVendorDropDownList.SelectedIndex == 0)
                {
                    return;
                }
                else
                {
                    GetPartnerDetail();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }

        }

        public void GetPartnerDetail()
        {

            IList<Partner> partnerSpecifications = GetViewState<IList<Partner>>(ViewStateConstants.PartnerDetails);
            if (partnerSpecifications == null)
            {
                partnerSpecifications = new List<Partner>();
            }

            if (!partnerSpecifications.Any(x => x.PartnerId == associatedVendorDropDownList.SelectedValue))
            {
                partnerSpecifications.Add(new Partner()
                {
                    PartnerId = associatedVendorDropDownList.SelectedValue,
                    CompanyDescription = associatedVendorDropDownList.SelectedItem.Text

                });
                SetViewState<IList<Partner>>(ViewStateConstants.PartnerDetails, partnerSpecifications);
                gridviewPartners.DataSource = partnerSpecifications;
                gridviewPartners.DataBind();
            }


        }
        public void GridviewPartner_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeletePartner")
            {
                int index = Convert.ToInt32(e.CommandArgument, CultureInfo.InvariantCulture);

                string PartnerId = gridviewPartners.DataKeys[index].Values[0].ToString();

                IList<Partner> partnerSpecifications = GetViewState<IList<Partner>>(ViewStateConstants.PartnerDetails);
                Partner partnerItem = (Partner)partnerSpecifications.Where(i => i.PartnerId.Equals(PartnerId)).First();

                partnerSpecifications.Remove(partnerItem);

                SetViewState<IList<Partner>>(ViewStateConstants.PartnerDetails, partnerSpecifications);

                gridviewPartners.DataSource = partnerSpecifications;
                gridviewPartners.DataBind();
            }
        }
    }
}