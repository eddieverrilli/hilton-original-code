﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Utilities;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Collections;

namespace Hilton.SuppliersConnection.UI
{
    public partial class CopyBrandTemplate : PageBase
    {
        private IProjectTemplateManager _projectTemplateManager;
        private ArrayList checkboxList;
        /// <summary>
        /// Invoked on page initialization
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs args)
        {
            try
            {
                base.OnInit(args);
                checkboxList = new ArrayList();
                ValidateUserAccess((int)MenuEnum.ProjectTemplates);
                _projectTemplateManager = ProjectTemplateManager.Instance;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    if (!IsPostBack)
                    {
                        mvtemplate.SetActiveView(vwBrandSelection);
                        mvtemplate.ActiveViewIndex = 0;
                        BindCopyFromBrandDropDownData();
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }

        }
        /// <summary>
        /// Bind all the active brands having active templates present in system
        /// </summary>
        private void BindCopyFromBrandDropDownData()
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            IList<Brand> brandCollection = _projectTemplateManager.GetProjectTemplateActiveBrands();
            drpBrandCopyFrom.DataSource = brandCollection;
            drpBrandCopyFrom.DataTextField = "BrandDescription";
            drpBrandCopyFrom.DataValueField = "BrandId";
            drpBrandCopyFrom.DataBind();
            drpBrandCopyFrom.Items.Insert(0, new ListItem("Select a Brand", "0"));
            drpBrandCopyTo.Items.Insert(0, new ListItem("Select a Brand", "0"));
        }
        /// <summary>
        /// Bind all the active brands  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void drpBrandCopyFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int CopyfromBrandId = Convert.ToInt32(drpBrandCopyFrom.SelectedItem.Value);
                drpBrandCopyTo.Items.Clear();
                if (CopyfromBrandId != 0)
                {
                    IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
                    var brandlist = brands.Where(item => item.DataValueField != Convert.ToString(CopyfromBrandId)).Distinct().ToList<DropDownItem>();
                    drpBrandCopyTo.DataSource = brandlist.OrderBy(x => x.DataTextField);
                    drpBrandCopyTo.DataTextField = "DataTextField";
                    drpBrandCopyTo.DataValueField = "DataValueField";
                    drpBrandCopyTo.DataBind();
                    drpBrandCopyTo.Items.Insert(0, new ListItem("Select a Brand", "0"));
                }
                else
                {
                    drpBrandCopyTo.Items.Insert(0, new ListItem("Select a Brand", "0"));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        /// Bind the active templates based on selected copy from brand dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                mvtemplate.SetActiveView(vwtemplates);
                BindProjectTemplates();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        /// Bind the active templates based on selected copy from brand dropdown
        /// </summary>
        private void BindProjectTemplates()
        {
            int selectedBrandId = Convert.ToInt32(drpBrandCopyFrom.SelectedItem.Value);
            IList<ProjectTemplate> ProjectTemplateCollection = _projectTemplateManager.GetProjectTemplatesByBrandId(selectedBrandId);
            if (ProjectTemplateCollection != null && ProjectTemplateCollection.Count > 0)
            {
                ProjectTemplateCollection = ProjectTemplateCollection.OrderBy(x => x.RegionDescription).ThenBy(x => x.CountryName).ToList();
                TemplateListGridView.DataSource = ProjectTemplateCollection;
                TemplateListGridView.DataBind();
            }
        }
        /// <summary>
        /// go back to brand selection page with default values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                drpBrandCopyFrom.SelectedIndex = 0;
                drpBrandCopyTo.Items.Clear();
                drpBrandCopyTo.Items.Insert(0, new ListItem("Select a Brand", "0"));
                hidCheckedIDs.Value = string.Empty;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }


        }
        /// <summary>
        /// go to next view and bind all the active categories present in system
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnContinueTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                bool Istemplatechecked = false;
                List<ProjectTemplate> projectTemplateList = new List<ProjectTemplate>();

                foreach (GridViewRow row in TemplateListGridView.Rows)
                {
                    CheckBox chkRow = (CheckBox)row.FindControl("chkRow");
                    if (chkRow.Checked)
                    {
                        Istemplatechecked = true;
                        break;
                    }
                }

                if (Istemplatechecked)
                {
                    mvtemplate.SetActiveView(vwCategory);
                    BindActiveCategories();
                }
                else
                {
                    ConfigureResultMessage(resultMessageTemplateDiv, "message error", "Please select atleast one template to copy");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        /// Bind active categories
        /// </summary>
        private void BindActiveCategories()
        {
            IList<Category> categories = HelperManager.Instance.GetActiveCategories.ToList().Where(b => b.Level == 1 || b.Level == 2 || b.Level == 3).ToList();
            SetSession<IList<Category>>("TemplateCategory", categories);
            IList<Category> rootCategoryList = (categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).OrderBy(p => p.Level).ThenBy(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture)).ToList();
            categoryDivisionsRepeater.DataSource = rootCategoryList;
            categoryDivisionsRepeater.DataBind();
        }
        /// <summary>
        /// go back to bransd selection view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                mvtemplate.SetActiveView(vwBrandSelection);
                drpBrandCopyFrom.SelectedIndex = 0;
                drpBrandCopyTo.Items.Clear();
                drpBrandCopyTo.Items.Insert(0, new ListItem("Select a Brand", "0"));
                hidCheckedIDs.Value = string.Empty;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// repeatedly creates the divisions to dispaly the hierarchy of the categories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryDivisionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl categoryDivision = (HtmlGenericControl)e.Item.FindControl("categoryListDivision");
                    HtmlTable categoryDetailsTable = new HtmlTable();
                    Panel subcategoryPanel = new Panel();
                    if ((Convert.ToInt32(((Entities.Category)(e.Item.DataItem)).ParentCategoryId)) == -1)
                    {
                        if (((Entities.Category)(e.Item.DataItem)).IsLeaf)
                        {
                            categoryDetailsTable = CreateNewCategoryDivision((Entities.Category)(e.Item.DataItem));
                            categoryDivision.Controls.Add(categoryDetailsTable);
                        }
                        else
                        {   //category with subcategories below it.
                            subcategoryPanel = CreateMidLevelCategoryDivision((Entities.Category)(e.Item.DataItem));
                            categoryDivision.Controls.Add(subcategoryPanel);

                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        ///creates a category division for a newly added category with no products and no sub categories under them
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        private HtmlTable CreateNewCategoryDivision(Category category)
        {
            try
            {
                checkboxList.Clear();
                string categoryName = category.CategoryName;
                string categoryLevel = category.Level.ToString(CultureInfo.InvariantCulture);
                string categoryID = category.CategoryId.ToString(CultureInfo.InvariantCulture);
                string parentCatId = category.ParentCategoryId.ToString(CultureInfo.InvariantCulture);
                string rootParentCatId = category.RootParentId.ToString(CultureInfo.InvariantCulture);

                HtmlTable categoryDetailsTable = new HtmlTable();
                categoryDetailsTable.Attributes.Add("class", "cat-table");

                categoryDetailsTable.CellPadding = 0;
                categoryDetailsTable.CellSpacing = 0;
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell;
                string cssClassName;

                for (int colCount = 1; colCount <= 2; colCount++)
                {
                    cell = new HtmlTableCell();
                    cssClassName = "col-";
                    cssClassName = cssClassName + colCount.ToString(CultureInfo.InvariantCulture);
                    cell.Attributes.Add("class", cssClassName);
                    if (colCount == 1)
                    {
                        HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H5");
                        int marginFromLeft = (Convert.ToInt32(categoryLevel, CultureInfo.InvariantCulture) - 1) * 30;
                        headingCategoryName.Style.Add("margin-left", marginFromLeft.ToString(CultureInfo.InvariantCulture) + "px");
                        headingCategoryName.InnerText = categoryName;
                        cell.Controls.Add(headingCategoryName);
                    }
                    else if (colCount == 2)
                    {
                        CheckBox chkActive = new CheckBox();
                        cell.Controls.Add(chkActive);
                        chkActive.Enabled = true;
                        chkActive.ID = "activeCheck_" + categoryID;
                        chkActive.Attributes.Add("SelfID", "ID_" + categoryID);
                        chkActive.Attributes.Add("ParentCatId", parentCatId);
                        chkActive.Attributes.Add("RootParentCatId", rootParentCatId);
                    }
                    row.Cells.Add(cell);
                }
                categoryDetailsTable.Rows.Add(row);
                categoryDetailsTable.Attributes.Clear();
                categoryDetailsTable.Attributes.Add("class", "cat-table");
                return categoryDetailsTable;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }
        /// <summary>
        /// creates all the category divisions which have sub categories under it
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="categoryName"></param>
        /// <param name="categoryBrandStandardNumber"></param>
        /// <param name="categoryLevel"></param>
        /// <returns></returns>
        private Panel CreateMidLevelCategoryDivision(Category category)
        {
            try
            {
                checkboxList.Clear();
                Panel categoryPanel = new Panel();
                string categoryName = category.CategoryName;
                string categoryLevel = category.Level.ToString(CultureInfo.InvariantCulture);
                string categoryID = category.CategoryId.ToString(CultureInfo.InvariantCulture);
                string parentCatId = category.ParentCategoryId.ToString(CultureInfo.InvariantCulture);
                string rootParentCatId = category.RootParentId.ToString(CultureInfo.InvariantCulture);


                HtmlTable categoryDetailsTable = new HtmlTable();
                categoryDetailsTable.Attributes.Add("class", "cat-table");
                categoryDetailsTable.CellPadding = 0;
                categoryDetailsTable.CellSpacing = 0;
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell;
                string cssClassName;
                categoryPanel.ID = "panel_" + categoryID;

                for (int colCount = 1; colCount <= 2; colCount++)
                {
                    cell = new HtmlTableCell();
                    cssClassName = "col-";
                    cssClassName = cssClassName + colCount.ToString(CultureInfo.InvariantCulture);
                    cell.Attributes.Add("class", cssClassName);
                    if (colCount == 1)
                    {
                        HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H5");
                        int marginFromLeft = (Convert.ToInt32(categoryLevel, CultureInfo.InvariantCulture) - 1) * 30;
                        headingCategoryName.Style.Add("margin-left", marginFromLeft.ToString(CultureInfo.InvariantCulture) + "px");
                        headingCategoryName.InnerText = categoryName;
                        cell.Controls.Add(headingCategoryName);
                    }
                    else if (colCount == 2)
                    {
                        CheckBox chkActive = new CheckBox();
                        chkActive.Enabled = true;
                        cell.Controls.Add(chkActive);
                        chkActive.ID = "activeCheck_" + categoryID;
                        chkActive.Attributes.Add("SelfID", "ID_" + categoryID);
                        chkActive.Attributes.Add("ParentCatId", parentCatId);
                        chkActive.Attributes.Add("RootParentCatId", rootParentCatId);
                    }
                    row.Cells.Add(cell);
                }
                categoryDetailsTable.Rows.Add(row);
                categoryDetailsTable.Attributes.Clear();
                categoryDetailsTable.Attributes.Add("class", "cat-table");
                categoryPanel.Controls.Add(categoryDetailsTable);

                IList<Category> categories = GetSession<List<Category>>("TemplateCategory");
                IEnumerable<Category> subCategoryCollection = categories.Where(p => p.RootParentId == Convert.ToInt32(categoryID, CultureInfo.InvariantCulture)).OrderBy(p => p.Level).ThenBy(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture));
                string subCategories = string.Empty;

                foreach (Category subcategory in subCategoryCollection)
                {
                    if (subcategory.Level.Equals(2))
                    {
                        subCategories = subCategories + subcategory.Level + "_" + subcategory.CategoryId + ",";
                    }

                    Panel subcategoryPanel = new Panel();
                    string subcategoryName = subcategory.CategoryName;
                    string subcategoryLevel = subcategory.Level.ToString(CultureInfo.InvariantCulture);
                    string subcategoryID = subcategory.CategoryId.ToString(CultureInfo.InvariantCulture);

                    HtmlTable subcategoryDetailsTable = new HtmlTable();
                    subcategoryDetailsTable.Attributes.Add("class", "cat-table");
                    subcategoryDetailsTable.CellPadding = 0;
                    subcategoryDetailsTable.CellSpacing = 0;
                    HtmlTableRow subcategoryrow = new HtmlTableRow();
                    HtmlTableCell subcategorycell;
                    subcategoryPanel.ID = "panel_" + subcategoryID;

                    for (int colCount = 1; colCount <= 2; colCount++)
                    {
                        subcategorycell = new HtmlTableCell();
                        cssClassName = "col-";
                        cssClassName = cssClassName + colCount.ToString(CultureInfo.InvariantCulture);
                        subcategorycell.Attributes.Add("class", cssClassName);
                        if (colCount == 1)
                        {
                            HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H5");
                            int marginFromLeft = (Convert.ToInt32(subcategoryLevel, CultureInfo.InvariantCulture) - 1) * 30;
                            headingCategoryName.Style.Add("margin-left", marginFromLeft.ToString(CultureInfo.InvariantCulture) + "px");
                            headingCategoryName.InnerText = subcategoryName;
                            subcategorycell.Controls.Add(headingCategoryName);
                        }
                        else if (colCount == 2)
                        {
                            CheckBox chkActive = new CheckBox();
                            chkActive.Enabled = true;
                            subcategorycell.Controls.Add(chkActive);
                            chkActive.ID = "activeCheck_" + subcategoryID;
                            chkActive.Attributes.Add("SelfID", "ID_" + subcategoryID);
                            chkActive.Attributes.Add("ParentCatId", subcategory.ParentCategoryId.ToString());
                            chkActive.Attributes.Add("RootParentCatId", subcategory.RootParentId.ToString());
                        }
                        subcategoryrow.Cells.Add(subcategorycell);
                    }
                    subcategoryDetailsTable.Rows.Add(subcategoryrow);
                    subcategoryDetailsTable.Attributes.Clear();
                    subcategoryDetailsTable.Attributes.Add("class", "cat-table");
                    subcategoryPanel.Style.Add("display", "none");

                    if (subcategory.Level == 2)
                    {
                        subcategoryPanel.Attributes.Add("class", category.CategoryId.ToString());
                    }
                    else
                    {
                        subcategoryPanel.Attributes.Add("class", subcategory.ParentCategoryId.ToString());
                    }

                    if (subcategory.IsLeaf)
                    {
                        subcategoryDetailsTable = CreateNewCategoryDivision(subcategory);
                        subcategoryPanel.Controls.Add(subcategoryDetailsTable);
                    }
                    else
                    {
                        subcategoryPanel.Controls.Add(subcategoryDetailsTable);
                        subcategoryDetailsTable.Attributes.Add("onClick", "ToggleCategory('" + subcategory.CategoryId.ToString() + "')");
                        subcategoryDetailsTable.Style.Add("cursor", "pointer");
                    }
                    if (categoryPanel.ID.ToString(CultureInfo.InvariantCulture) == "panel_" + subcategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))
                    {
                        categoryPanel.Controls.Add(subcategoryPanel);
                    }
                    else //last;
                    {
                        Panel pnlParent = new Panel();
                        foreach (Control p in categoryPanel.Controls)
                        {
                            if (p is Panel)
                            {
                                pnlParent = (Panel)FindControl(categoryPanel, "panel_" + subcategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                if (pnlParent != null)
                                    pnlParent.Controls.Add(subcategoryPanel);
                            }
                        }

                    }
                }//end for of subcategory

                categoryDetailsTable.Attributes.Add("onClick", "ToggleCategory('" + category.CategoryId.ToString() + "')");
                categoryDetailsTable.Style.Add("cursor", "pointer");
                return categoryPanel;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }
        /// <summary>
        /// Preview section to display selected templates and categories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkCopyTemplete_Click(object sender, EventArgs e)
        {
            try
            {
                //Get the selected Categories value
                IList<Category> categories = GetSession<List<Category>>("TemplateCategory");
                IList<Category> CategoryCollection = new List<Category>();
                string[] strUnCheckedIds = hidCheckedIDs.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                Category cat;
                foreach (var item in strUnCheckedIds)
                {
                    cat = new Category();
                    cat.CategoryId = Convert.ToInt32(item);
                    CategoryCollection.Add(cat);
                }
                if (CategoryCollection != null && CategoryCollection.Count > 0)
                {
                    (CategoryCollection.AsEnumerable().Where(p => p.ParentCategoryId == -1)).OrderBy(p => p.Level).ThenBy(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture)).ToList();
                    List<Category> selectedCategories = (from ac in categories join r in CategoryCollection on ac.CategoryId equals r.CategoryId select ac).Distinct().ToList();

                    IList<Category> sortedCategoryList = (selectedCategories.AsEnumerable()).OrderBy(p => p.ParentCategoryHierarchy).ThenBy(p => p.Level).ThenBy(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture)).ToList();
                    grdCategoryPreview.DataSource = sortedCategoryList;
                    grdCategoryPreview.DataBind();

                    SetSession<List<Category>>("SelectedCategories", selectedCategories);

                    mvtemplate.SetActiveView(vwCopyTemplatePreview);
                    lblCopyFromBrand.Text = Convert.ToString(drpBrandCopyFrom.SelectedItem);
                    lblCopyToBrand.Text = Convert.ToString(drpBrandCopyTo.SelectedItem);
                    List<ProjectTemplate> projectTemplateList = new List<ProjectTemplate>();

                    foreach (GridViewRow row in TemplateListGridView.Rows)
                    {
                        CheckBox chkRow = (CheckBox)row.FindControl("chkRow");
                        if (chkRow.Checked)
                        {
                            projectTemplateList.Add(new ProjectTemplate
                            {
                                TemplateId = Convert.ToInt32(TemplateListGridView.DataKeys[row.RowIndex]["TemplateId"]),
                                RegionId = Convert.ToInt32(TemplateListGridView.DataKeys[row.RowIndex]["RegionId"]),
                                CountryId = Convert.ToInt32(TemplateListGridView.DataKeys[row.RowIndex]["CountryId"]),
                                RegionDescription = row.Cells[2].Text.Trim(),
                                CountryName = row.Cells[3].Text.Trim(),
                            });
                        }
                    }

                    SetSession<List<ProjectTemplate>>("SelectedTemplates", projectTemplateList);

                    grdRegionPeview.DataSource = projectTemplateList;
                    grdRegionPeview.DataBind();
                }
                else
                {
                    BindActiveCategories();
                    ConfigureResultMessage(divMessageCategory, "message error", "Please select atleast one Category to copy");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        /// Go back to previous view of templates selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkCancelCategory_Click(object sender, EventArgs e)
        {
            try
            {
                mvtemplate.SetActiveView(vwtemplates);
                ProjectTemplatescheckboxclear();
                hidCheckedIDs.Value = string.Empty;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        /// clear all the selection on templates selection view
        /// </summary>
        public void ProjectTemplatescheckboxclear()
        {
            foreach (GridViewRow row in TemplateListGridView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkrow = (CheckBox)row.FindControl("chkRow");
                    if (chkrow.Checked)
                        chkrow.Checked = false;
                }
            }
            CheckBox chkHeader = (CheckBox)TemplateListGridView.HeaderRow.FindControl("chkHeader");
            if (chkHeader.Checked)
                chkHeader.Checked = false;
        }



        /// <summary>
        /// Copy all the selected data into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkFinalCopy_Click(object sender, EventArgs e)
        {
            try
            {
                List<ProjectTemplate> ProjectTemplateCollection = new List<ProjectTemplate>();
                ProjectTemplateCollection = GetSession<List<ProjectTemplate>>("SelectedTemplates");

                IList<Category> CategoryCollection = new List<Category>();
                CategoryCollection = GetSession<List<Category>>("SelectedCategories");

                int copyFromBrandId = Convert.ToInt32(drpBrandCopyFrom.SelectedItem.Value);
                int copyToBrandId = Convert.ToInt32(drpBrandCopyTo.SelectedItem.Value);

                User currentUser = GetSession<User>(SessionConstants.User);

                int result = _projectTemplateManager.CopyMultipleProjectTemplate(ProjectTemplateCollection, CategoryCollection, copyFromBrandId, copyToBrandId, currentUser.UserId);

                if (result == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel2, this.GetType(), "showmessage", "ShowDialog()", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));

            }

        }

        protected void lnkPreviewCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mvtemplate.SetActiveView(vwCategory);
                BindActiveCategories();
                hidCheckedIDs.Value = string.Empty;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}