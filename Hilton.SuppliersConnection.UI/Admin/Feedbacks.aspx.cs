﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Admin
{
    public partial class Feedbacks : PageBase
    {
        private FeedbackManager _feedbackManager;

        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                feedbackSearch.PageIndex = pagingControl.CurrentPageIndex;
                feedbackSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                feedbackSearch.AllClick = pagingControl.AllPageClick;
                SearchFeedbacks(feedbackSearch);
                
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked on ItemCommand Event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void FeedbackListRepeater_ItemCommand(object source, RepeaterCommandEventArgs args)
        {
            try
            {
                if (args != null && args.Item.ItemType == ListItemType.Header)
                {
                    string commandSourceId = ((LinkButton)args.CommandSource).ID;
                    string sortExpression = args.CommandName;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);
                    SetViewState<string>(ViewStateConstants.CommandSourceId, commandSourceId);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortRepeaterView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortRepeaterView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
                else if (args != null && (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem))
                {
                    if (args.CommandName == "UpdateFeedbackStatus")
                    {
                        IList<Feedback> feedbackCollection = new List<Feedback>();

                        HiddenField feedbackStatusIdHiddenField = args.Item.FindControl("feedbackStatusIdHiddenField") as HiddenField;
                        DropDownList statusDropDownList = args.Item.FindControl("feedbackStatusDropDown") as DropDownList;
                        string oldFeedbackStatusId = feedbackStatusIdHiddenField.Value;
                        string newFeedbackStatusId = statusDropDownList.SelectedValue;
                        if (oldFeedbackStatusId != newFeedbackStatusId)
                        {
                            feedbackCollection.Add(new Feedback()
                            {
                                FeedbackId = Convert.ToInt32(args.CommandArgument.ToString(), CultureInfo.InvariantCulture),
                                FeedbackStatusId = Convert.ToInt16(newFeedbackStatusId, CultureInfo.InvariantCulture),
                            });
                            feedbackStatusIdHiddenField.Value = newFeedbackStatusId;
                        }

                        UpdateFeedbackStatus(feedbackCollection);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked on ItemCreated Event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void FeedbackListRepeater_ItemCreated(object source, RepeaterItemEventArgs args)
        {
            try
            {
                if (args != null && args.Item.ItemType == ListItemType.Header)
                {
                    if (GetViewState<object>(ViewStateConstants.CommandSourceId) != null)
                    {
                        string commandSourceId = GetViewState<object>(ViewStateConstants.CommandSourceId).ToString();
                        LinkButton lbtn = args.Item.FindControl(commandSourceId) as LinkButton;
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {
                            lbtn.Attributes.Add("class", "sorta");
                        }
                        else
                        {
                            lbtn.Attributes.Add("class", "sortd");
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked on ItemDataBound Event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void FeedbackListRepeater_ItemDataBound(object source, RepeaterItemEventArgs args)
        {
            try
            {
                if (args != null && (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem))
                {
                    DropDownList statusDropDownList = args.Item.FindControl("feedbackStatusDropDown") as DropDownList;
                    BindStaticDropDown(DropDownConstants.FeedbackStatusDropDown, statusDropDownList, null, null);
                    statusDropDownList.SelectedValue = ((Feedback)args.Item.DataItem).FeedbackStatusId.ToString(CultureInfo.InvariantCulture);


                    HyperLink hprlnkComment = new HyperLink();
                    hprlnkComment = (HyperLink)(args.Item.FindControl("hprlnkComment"));
                    if (hprlnkComment != null)
                    {
                        int fid = ((Feedback)args.Item.DataItem).FeedbackId;
                        hprlnkComment.NavigateUrl = "AddFeedbakCommentPopup.aspx?keepThis=true&fid=" + fid.ToString() + "&TB_iframe=true&height=400&width=400";
                    }




                    IList<FeedbackComment> comments = ((Feedback)args.Item.DataItem).FeedbackComments;
                    if (comments != null)
                    {
                        Label commentLbl = (Label)args.Item.FindControl("commentsLabel");
                        if (commentLbl != null)
                        {
                            foreach (FeedbackComment comment in comments)
                            {
                                commentLbl.Text += "<I>By: </I>" + comment.SubmittedbyFullName + "<I> Added: </I>" + comment.SubmittedDate.ToString("dddd, MMMM d, yyyy hh:mm tt") + "<I> Comment: </I>" + comment.Comment + "<br/>";
                            }

                        }
                    }


                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                feedbackSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchFeedbacks(feedbackSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked on Go Button Click to Seacrh Records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    FeedbackSearch feedbackSearch = BuildSearchCriteria();
                    feedbackSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                    feedbackSearch.SortExpression = UIConstants.CompanyName;
                    feedbackSearch.SortDirection = UIConstants.AscAbbreviation;
                    SetViewState(ViewStateConstants.CommandSourceId, UIConstants.FeedbackDefaultCommandSourceId);
                    SetViewState(ViewStateConstants.SortExpression, UIConstants.CompanyName);
                    SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                    SearchFeedbacks(feedbackSearch);

                    //Pagination Control Setting
                    pagingControl.CurrentPageIndex = 0;
                    pagingControl.PageSet = 0;
                    pagingControl.AllPageClick = false;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                feedbackSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchFeedbacks(feedbackSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState, feedbackSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                feedbackSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchFeedbacks(feedbackSearch);
                SetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState, feedbackSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked on OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                ValidateUserAccess((int)MenuEnum.Feedback);
                _feedbackManager = FeedbackManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(1092, culture, "Feedback List");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked on Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;
                    ScriptManager.RegisterPostBackControl(exportCSVLinkButton);
                    if (!Page.IsPostBack)
                    {
                        exportCSVLinkButton.Visible = false;
                        upateAllLinkButton.Visible = false;
                        PopulateDropDownData();
                        
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(feedbacksUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                        ScriptManager.RegisterClientScriptBlock(feedbacksUpdatePanel, this.GetType(), "ForceCall", "forceCall();", true);
                        
                    }

                    if (GetSession<string>(SessionConstants.ReturnFromCommentPopup)!=null )
                    {
                        FeedbackSearch feedbackSearch = GetSession<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                        SearchFeedbacks(feedbackSearch);
                        ClearSession(SessionConstants.ReturnFromCommentPopup);

                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                feedbackSearch.PageIndex = pagingControl.CurrentPageIndex;
                feedbackSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                feedbackSearch.AllClick = pagingControl.AllPageClick;
                SearchFeedbacks(feedbackSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                feedbackSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchFeedbacks(feedbackSearch);
                SetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState, feedbackSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                feedbackSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchFeedbacks(feedbackSearch);
                SetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState, feedbackSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// Invoked on Specific Search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void QuickSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FeedbackSearch feedbackSearch = BuildSearchCriteria();
                feedbackSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                feedbackSearch.SortExpression = UIConstants.CompanyName; ;
                feedbackSearch.SortDirection = UIConstants.AscAbbreviation;
                SetViewState(ViewStateConstants.CommandSourceId, UIConstants.FeedbackDefaultCommandSourceId);
                SetViewState(ViewStateConstants.SortExpression, UIConstants.CompanyName);
                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                SearchFeedbacks(feedbackSearch);

                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
                pagingControl.AllPageClick = false;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// To Invoked on Save Click and save the records.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void UpdateAllLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                IList<Feedback> feedbackCollection = new List<Feedback>();

                foreach (RepeaterItem item in feedbackListRepeater.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        HiddenField feedbackIdHiddenField = item.FindControl("feedbackIdHiddenField") as HiddenField;
                        HiddenField feedbackStatusIdHiddenField = item.FindControl("feedbackStatusIdHiddenField") as HiddenField;
                        DropDownList statusDropDownList = item.FindControl("feedbackStatusDropDown") as DropDownList;
                        string oldFeedbackStatusId = feedbackStatusIdHiddenField.Value;
                        string newFeedbackStatusId = statusDropDownList.SelectedValue;
                        if (oldFeedbackStatusId != newFeedbackStatusId)
                        {
                            feedbackCollection.Add(new Feedback()
                            {
                                FeedbackId = Convert.ToInt32(feedbackIdHiddenField.Value, CultureInfo.InvariantCulture),
                                FeedbackStatusId = Convert.ToInt16(newFeedbackStatusId, CultureInfo.InvariantCulture),
                            });
                            feedbackStatusIdHiddenField.Value = newFeedbackStatusId;
                        }
                    }
                }
                UpdateFeedbackStatus(feedbackCollection);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        ///triggers when export to excel button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ExportCSVLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                feedbackSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.ExportToExcelMaxPageSize), CultureInfo.InvariantCulture); // Max page size to get all records to excel at once
                ExportFeedbacks(feedbackSearch);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// To Validate Date
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidDate(object source, ServerValidateEventArgs args)
        {
            try
            {
                ValidateDate(args, validateDateCustomValidator, fromDateTextBox.Text, toDateTextBox.Text);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// To Bind the feedbacks to repeater
        /// </summary>
        /// <param name="feedbackList"></param>
        private void BindFeedbacks(IList<Feedback> feedbackList)
        {
            //Set the data
            feedbackListRepeater.DataSource = feedbackList;
            feedbackListRepeater.DataBind();
        }

        /// <summary>
        /// To build the search criteria
        /// </summary>
        /// <returns></returns>
        private FeedbackSearch BuildSearchCriteria()
        {
            User currentUser = (User)GetSession<User>(SessionConstants.User);
            FeedbackSearch feedbackSearch = new FeedbackSearch()
            {
                UserId = currentUser.UserId,
                PartnerId = (partnerDropDown.SelectedIndex == 0) ? string.Empty : partnerDropDown.SelectedValue,
                RatingId = (ratingDropDown.SelectedIndex == 0) ? string.Empty : ratingDropDown.SelectedValue,
                StatusId = (statusDropDown.SelectedIndex == 0) ? string.Empty : statusDropDown.SelectedValue,
                QuickSearchExpression = searchTextBox.Text.Trim(),
            };

            if ((!String.IsNullOrEmpty(fromDateTextBox.Text.Trim())) && (!String.IsNullOrEmpty(toDateTextBox.Text.Trim())))
            {
                feedbackSearch.DateFrom = Convert.ToDateTime(fromDateTextBox.Text.Trim(), CultureInfo.InvariantCulture);
                feedbackSearch.DateTo = Convert.ToDateTime(toDateTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }

            return feedbackSearch;
        }

        /// <summary>
        /// To Popultae Drop Downs
        /// </summary>
        private void PopulateDropDownData()
        {
            BindDynamicDropDown(DropDownConstants.FeedbackPartnerDropDown, partnerDropDown, ResourceUtility.GetLocalizedString(1216, culture, "All Partners"), null);
            BindStaticDropDown(DropDownConstants.RatingDropDown, ratingDropDown, ResourceUtility.GetLocalizedString(1221, culture, "All Ratings"), null);
            BindStaticDropDown(DropDownConstants.FeedbackStatusDropDown, statusDropDown, ResourceUtility.GetLocalizedString(1227, culture, "All Status"), null);
        }

        /// <summary>
        /// Tp Search the Feedbacks
        /// </summary>
        /// <param name="feedbackSearch"></param>
        private void SearchFeedbacks(FeedbackSearch feedbackSearch)
        {
            int totalRecordCount = 0;
            if (feedbackSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                feedbackSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                feedbackSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (feedbackSearch.PageIndex == -1)
                {
                    feedbackSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = feedbackSearch.PageIndex;
            }

            //call the business method
            var feedbackList = _feedbackManager.GetFeedbackList(feedbackSearch, ref totalRecordCount);

            if (feedbackList.Count == 0)
            {
                searchResultDiv.Attributes.Add("class", "message notification");
                searchResultDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoFeedbacksFoundForCriteria, culture);
                exportCSVLinkButton.Visible = false;
                upateAllLinkButton.Visible = false;
                feedbackListRepeater.Visible = false;
            }
            else
            {
                exportCSVLinkButton.Visible = true;
                upateAllLinkButton.Visible = false;// Set to true to extend save all option
                feedbackListRepeater.Visible = true;
            }

            feedbackSearch.TotalRecordCount = totalRecordCount;

            SetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState, feedbackSearch);
            SetSession<FeedbackSearch>(ViewStateConstants.FeedbackViewState, feedbackSearch);   
            BindFeedbacks(feedbackList);

            //Pagination Control Setting
            pagingControl.TotalRecordCount = totalRecordCount;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);

            validateDateCustomValidator.ErrorMessage = string.Empty;
        }

        /// <summary>
        ///opens an excel sheet and sets teh content
        /// </summary>
        /// <param name="feedbackSearch"></param>
        private void ExportFeedbacks(FeedbackSearch feedbackSearch)
        {
            try
            {
                //DataSet feedbackCollection = new DataSet();

               
                //call the business method
                int totalRecordCount =0;
                IList<Feedback> feedbackCollection = _feedbackManager.GetFeedbackList(feedbackSearch, ref totalRecordCount);  //.ExportFeedback(feedbackSearch);

                if (feedbackCollection.Count == 0)
                {
                    searchResultDiv.Attributes.Add("class", "message notification");
                    searchResultDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoFeedbackToExport, culture);

                    exportCSVLinkButton.Visible = false;
                    upateAllLinkButton.Visible = false;
                    feedbackListRepeater.Visible = false;
                }
                else
                {
                    exportCSVLinkButton.Visible = true;
                    upateAllLinkButton.Visible = false; //Set to true to extend save all functionality
                    feedbackListRepeater.Visible = true;
                }

                ExportToExcel(feedbackCollection);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        ///exports the feedbacks to excel
        /// </summary>
        /// <param name="feedbackCollection"></param>
        private void ExportToExcel(IList<Feedback> feedbackCollection)
        {
            try
            {
                string contentDisposition = UIConstants.FeedbackExcelFileName;
                Response.ClearContent();
                Response.AddHeader("content-disposition", contentDisposition);
                Response.ContentType = UIConstants.ContentType;
                if (feedbackCollection.Count != 0)
                {
                    WriteFeedbacksToExcel(feedbackCollection);
                    Response.End();
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// To write Feedbacks to Excel
        /// </summary>
        /// <param name="feedbackCollectionTable"></param>
        private void WriteFeedbacksToExcel(IList<Feedback> feedbackCollection)
        {
            //DataTable feedbackCollectionTable = feedbackCollection.Tables[0];
            try
            {
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(488, culture, "Partners") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(525, culture, "Product") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(581, culture, "Rating") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(159, culture, "From") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(357, culture, "Date Submitted") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(1093, culture, "Feedback") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(699, culture, "Status") + "\t");
                Response.Write("Admin\t");
                Response.Write("Date Added\t");
                Response.Write("Comment\t");
                Response.Write(System.Environment.NewLine);

                foreach (Feedback feedback in feedbackCollection)
                {
                    string prodName = string.Empty;
                    //if (row["ProductName"].ToString() == "-")
                    //{
                    prodName = feedback.ProductName;// row["ProductName"].ToString().Replace("-", "");
                    //}

                    Response.Write(feedback.PartnerCompany + "\t");
                    Response.Write(prodName + "\t");
                    Response.Write(feedback.RatingDescription + "\t");
                    Response.Write(feedback.From + "\t");
                    Response.Write(feedback.SubmittedDateTime.ToString("dd-MMM-yyyy") + "\t");
                    Response.Write(feedback.Details.Replace(Environment.NewLine, " ") + "\t");
                    Response.Write(feedback.FeedbackStatusDesc + "\t");
                    if (feedback.FeedbackComments != null)
                    {
                        int commentCount = 0;
                        foreach (FeedbackComment comment in feedback.FeedbackComments)
                        {
                            commentCount++;
                            Response.Write(comment.SubmittedbyFullName + "\t");
                            Response.Write(comment.SubmittedDate.ToString("dd-MMM-yyyy") + "\t");
                            Response.Write(comment.Comment.Replace(Environment.NewLine, " ") + "\t");
                            if (feedback.FeedbackComments.Count != commentCount)
                            {
                                Response.Write(System.Environment.NewLine);
                                Response.Write("\t");
                                Response.Write("\t");
                                Response.Write("\t");
                                Response.Write("\t");
                                Response.Write("\t");
                                Response.Write("\t");
                                Response.Write("\t");

                            }

                        }
                    }
                    Response.Write("\n");
                }
                Response.End();

               
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// This method will sort the result repeater control
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortRepeaterView(string sortExpression, string sortDirection)
        {
            FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
            feedbackSearch.SortExpression = sortExpression;
            feedbackSearch.SortDirection = sortDirection;
            feedbackSearch.PageIndex = 0;
            SearchFeedbacks(feedbackSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }

        /// <summary>
        /// UpdateFeedbackStatus()
        /// </summary>
        /// <param name="feedbackCollection"></param>
        private void UpdateFeedbackStatus(IList<Feedback> feedbackCollection)
        {
            if (feedbackCollection.Count > 0)
            {
                int result = _feedbackManager.UpdateFeedback(feedbackCollection);
                if (result == 0)
                {
                    resultMessageDiv.Attributes.Add("class", "message error");
                    resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralUpdateFailure, culture);
                }
                else
                {
                    resultMessageDiv.Attributes.Add("class", "message success");
                    resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.FeedbackSuccessfullyUpdated, culture);
                    FeedbackSearch feedbackSearch = GetViewState<FeedbackSearch>(ViewStateConstants.FeedbackViewState);
                    SearchFeedbacks(feedbackSearch);
                }
            }
            else
            {
                resultMessageDiv.Attributes.Add("class", "message notification");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.FeedbackStatusNotModified, culture);
            }

            validateDateCustomValidator.ErrorMessage = string.Empty;
        }
    }
}