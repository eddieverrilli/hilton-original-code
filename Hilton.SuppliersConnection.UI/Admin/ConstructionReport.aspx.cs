﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Admin
{
    public partial class ConstructionReport : PageBase
    {
        private IPartnerManager _partnerManager;
        private IHelperManager _helperManager;

        /// <summary>
        /// Invoked at OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                ValidateUserAccess((int)MenuEnum.AdminConstructionReport);
                _partnerManager = PartnerManager.Instance;
                _helperManager = HelperManager.Instance;
                this.Title = ResourceUtility.GetLocalizedString(0, culture, "Construction Report");

            }
            catch (Exception genericException)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref genericException);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        
        /// <summary>
        /// Bind the data to control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    resultMessageDiv.Visible = false;
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Download construction report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void DownloadSpreadSheet_ClickLinkButton(object sender, EventArgs args)
        {
            try
            {
                DataSet constReportDataSet = _partnerManager.GetConstructionReportDataSet(0); // pass 0 when access from Admin

                ExportToExcel(constReportDataSet);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Exports Construction Reports Data into Excel Format
        /// </summary>
        /// <param name="constReportDataSet"></param>
        private void ExportToExcel(DataSet constReportDataSet)
        {
            try
            {
                string attach = UIConstants.ConstructionReportExcelFileName;
                IList<ReportColumns> reportColumnCollection = null;

                Response.ClearContent();
                Response.AddHeader("content-disposition", attach);
                Response.ContentType = UIConstants.ContentType;
                reportColumnCollection = _helperManager.GetReportColumnDetails((int)ReportEnum.ConstructionReport);

                StringBuilder constReportStringBuilder = new StringBuilder();
                constReportStringBuilder.Append("<html><head><title></title></head><body>");
                constReportStringBuilder.AppendFormat("<table border = '1' cellpadding = '2'>");
                constReportStringBuilder.Append("<tr style = 'width: 100%'>");
                constReportStringBuilder.AppendFormat("<td colspan = '{0}'><b>Projects in Design and Under Construction</b></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.AppendFormat("<tr><td colspan = '{0}'></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.AppendFormat("<tr><td colspan = '{1}'>Report Date: {0}</td></tr>", DateTime.Now.ToLongDateString(), reportColumnCollection.Count);
                constReportStringBuilder.AppendFormat("<tr><td colspan = '{0}'></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.Append("<tr>");

                foreach (ReportColumns reportColumn in reportColumnCollection)
                {
                    constReportStringBuilder.AppendFormat("<td style ='width: {1}px; background-color: {2};vertical-align: {3};text-align: {4};height: {5};color: {7}';display:{6}><b>{0}</b></td>"
                                                    , reportColumn.ColumnName, reportColumn.Width, reportColumn.HeaderBgColor, reportColumn.HeaderVAlign, reportColumn.HeaderHAlign, (reportColumn.AutoFit) ? "100%" : reportColumn.Height + "px",
                                                     reportColumn.Visible, reportColumn.HeaderTextColor);
                }

                constReportStringBuilder.Append("</tr>");

                if (constReportDataSet.Tables != null)
                {
                    foreach (DataRow reportRow in constReportDataSet.Tables[0].Rows)
                    {
                        constReportStringBuilder.Append("<tr>");

                        for (int i = 0; i < constReportDataSet.Tables[0].Columns.Count; i++)
                        {
                            ReportColumns reportColumn = reportColumnCollection[i];
                            constReportStringBuilder.AppendFormat("<td style ='width: {1}px; background-color: {2};vertical-align: {3};text-align: {4};height: {5}';display:{6}>{0}</td>",
                                reportRow[i], reportColumn.Width, reportColumn.CellColor, reportColumn.VAlign, reportColumn.HAlign, (reportColumn.AutoFit) ? "100%" : reportColumn.Height + "px",
                                 reportColumn.Visible);

                        }

                        constReportStringBuilder.Append("</tr>");
                    }
                }
                constReportStringBuilder.Append("</table></body></html>");
                Response.Write(constReportStringBuilder.ToString());
                Response.End();
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

    }
}