﻿<%@ Page Title="Email Template" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="EmailTemplateDetails.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Admin.EmailTemplateDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <link rel="stylesheet" type="text/css" href="../Scripts/yui/build/assets/skins/sam/skin.css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/yui/build/assets/skins/sam/editor.css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/yui/build/menu/assets/skins/sam/menu.css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/yui/build/button/assets/skins/sam/button.css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/yui/build/container/assets/skins/sam/container.css" />
    <script type="text/javascript" src="../Scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
    <script type="text/javascript" src="../Scripts/yui/build/element/element-beta-min.js"></script>
    <script type="text/javascript" src="../Scripts/yui/build/container/container_core-min.js"></script>
    <script type="text/javascript" src="../Scripts/yui/build/menu/menu-min.js"></script>
    <script type="text/javascript" src="../Scripts/yui/build/button/button-min.js"></script>
    <script type="text/javascript" src="../Scripts/yui/build/editor/editor-beta-min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../Images/search-top-curve.gif" alt="" />
            </div>
            <div class="middle">
                <%--  javascript:history.back();--%>
                <a href="EmailTemplates.aspx" title="" id="contentBlockLink">‹‹ Back to Email Template</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                    href="Settings.aspx" title="" id="A1">Back to Settings</a>
            </div>
            <div class="bottom-curve">
                <img src="../Images/search-bottom-curve.gif" alt="" />
            </div>
        </div>
    </div>
    <div class="content">
        <div class="wrapper">
            <h2>
                <asp:Label ID="contentBlockTitleLabel" runat="server" Text=""></asp:Label>
            </h2>
            <div class="inner-content" id="editor-page">
                <div id="resultMessageDiv" runat="server">
                </div>
                <br />
                <div class="lft-col" style="width: 300px">
                    <h3>
                        <span>Details</span>
                    </h3>
                    <div class="row">
                        <div>
                            <span class="label">Template ID:</span></div>
                        <div class="input" id="divIDValue" runat="server">
                            <asp:Label ID="emailTemplateIDLabel" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <asp:Label ID="sectionLabel" runat="server" CBID="100" Text="Subject:" class="label"></asp:Label></div>
                        <div id="sectionDiv" runat="server" class="input textbox3 mandatory">
                            <asp:TextBox ID="sectionTextBox" runat="server"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="sectionRequiredFieldValidator" runat="server" CssClass="errorText"
                                ControlToValidate="sectionTextBox" Display="Dynamic" SetFocusOnError="true" VMTI="28"
                                ErrorMessage="Section is required"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <asp:Label ID="titleLabel" CssClass="label" CBID="" Text="Title:" runat="server"></asp:Label>
                        </div>
                        <div id="titleDiv" runat="server" class="input textbox3 mandatory">
                            <asp:TextBox ID="titleTextBox" runat="server"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="titleRequiredFieldValidator" runat="server" CssClass="errorText"
                                ControlToValidate="titleTextBox" Display="Dynamic" ErrorMessage="Title is required"
                                VMTI="29" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <span class="label">Description:</span></div>
                        <div class="input textarea">
                            <asp:TextBox ID="descriptionTextBox" runat="server" TextMode="MultiLine" Style="width: 200px;"></asp:TextBox>
                            <asp:HiddenField ID="languageResourceIdHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                    <div class="section">
                     <span class="btn-style">
                        <asp:LinkButton ID="saveLinkButton" runat="server" Text="Save" OnClick="SaveLinkButton_Click"></asp:LinkButton>
                        </span>
                    </div>
                </div>
                <div class="rgt-col" style="width: 580px">
                    <h3>
                        <span>Content</span></h3>
                    <div class="row">
                        <div>
                            <span class="label">Status:</span></div>
                        <div class="input">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="list-2">
                                <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="yui-skin-sam">
                        <textarea id="txtBody" name="txtBody" style="width: 100%" rows="5" cols="10" runat="server"></textarea>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        (function () {
            var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event;

            var myConfig = {
                height: '500px',
                width: '580px',
                dompath: true,
                focusAtStart: true,
                handleSubmit: true,
                animate: true
            };

            YAHOO.log('Create the Editor..', 'info', 'example');
            var myEditor = new YAHOO.widget.Editor('<%=txtBody.ClientID %>', myConfig);
            YAHOO.util.Event.on('<%= saveLinkButton.ClientID %>', 'click', function () {
                //Put the HTML back into the text area
                myEditor.saveHTML();
            });

            myEditor._defaultToolbar.buttonType = 'advanced';

            myEditor.render();

        })();

    </script>
</asp:Content>