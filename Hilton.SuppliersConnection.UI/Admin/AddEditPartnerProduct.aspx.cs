﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Admin
{
    public partial class AddEditPartnerProduct : PageBase
    {
        private IProjectTemplateManager _projectTemplateManager;
        private Entities.ProjectTemplate projectTemplate;
        private int currentCategoryId, currentPartnerId, currentProductId;
        private int projectTemplateId;
        private string mode;

        /// <summary>
        /// Get the instance of the manager class
        /// </summary>
        /// <param name="e"></param>
        ///
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            _projectTemplateManager = ProjectTemplateManager.Instance;
        }

        /// <summary>
        ///gets teh enitire list of categories for a template Id
        /// </summary>
        /// <param name="projectTemplateID"></param>
        private void PopulateProjectTemplateDetails(int projectTemplateID)
        {
            try
            {
                projectTemplate = _projectTemplateManager.GetProjectTemplateDetail(projectTemplateID);

                SetViewState<ProjectTemplate>(ViewStateConstants.ProjectTemplate, projectTemplate);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///loads the page and populates the details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    currentCategoryId = Convert.ToInt32(Request.QueryString["cat"], CultureInfo.InvariantCulture);
                    projectTemplateId = Convert.ToInt32(Request.QueryString["pt"], CultureInfo.InvariantCulture);
                    mode = Request.QueryString["mode"].ToString(CultureInfo.InvariantCulture);
                    //PopulateProjectTemplateDetails(projectTemplateId);
                    //CreateInitialDropDowns();
                    if (!IsPostBack)
                    {
                        PopulateProjectTemplateDetails(projectTemplateId);
                        CreateInitialDropDowns();
                        if (mode.Equals(UIConstants.Add.ToLower(CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                        {
                            SetLabelText(pageHeader, ResourceUtility.GetLocalizedString(784, culture));
                            BindPartnerDropDown(RetrievePartnerList());
                            partnerDropDownList.SelectedValue = "-1";
                            productDropDownList.Enabled = false;
                            BindProductDropDown(RetrieveProductList(-1));
                            productDropDownList.SelectedValue = "-1";
                        }
                        else
                        {
                            SetLabelText(pageHeader, ResourceUtility.GetLocalizedString(785, culture));

                            currentPartnerId = Convert.ToInt32(Request.QueryString["partner"], CultureInfo.InvariantCulture);
                            currentProductId = Convert.ToInt32(Request.QueryString["product"], CultureInfo.InvariantCulture);
                            CategoryPartnerProduct partnerProductToEdit = GetPartnerProductForEdit(currentPartnerId, currentProductId, currentCategoryId);

                            BindPartnerDropDown(RetrievePartnerList());
                            partnerDropDownList.SelectedValue = partnerProductToEdit.PartnerId.ToString(CultureInfo.InvariantCulture);

                            productDropDownList.Enabled = true;
                            if (partnerDropDownList.SelectedIndex == 0)
                            {
                                productDropDownList.Items.Insert(0, new ListItem(DropDownConstants.None, "-1"));
                                PartnerListUpdatePanel.Update();
                                ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.InactivePartnerProduct, culture));
                            }
                            else
                            {
                                BindProductDropDown(RetrieveProductList(currentPartnerId));
                                productDropDownList.SelectedValue = partnerProductToEdit.ProductId.ToString(CultureInfo.InvariantCulture);
                                if (productDropDownList.SelectedIndex == 0 && currentProductId != 0)
                                {
                                    ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.InactivePartnerProduct, culture));
                                }
                            }
                            statusDropDownList.SelectedValue = partnerProductToEdit.IsActive ? "1" : "0";
                        }
                        SetViewState(SessionConstants.SelectedPartnerId, (int?)Convert.ToInt32(partnerDropDownList.SelectedValue, CultureInfo.InvariantCulture));
                        SetViewState(SessionConstants.SelectedProductId, (int?)Convert.ToInt32(productDropDownList.SelectedValue, CultureInfo.InvariantCulture));
                        SetViewState(SessionConstants.SelectedPartnerProductStatus, (int?)Convert.ToInt32(statusDropDownList.SelectedValue, CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        CreateInitialDropDowns();
                        currentProductId = Convert.ToInt32(Request.QueryString["product"], CultureInfo.InvariantCulture);
                    }
                }
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// returns an entity of CategoryPartnerProduct to be edited with the values in the UI
        /// </summary>
        /// <param name="currentPartnerId"></param>
        /// <param name="currentProductId"></param>
        /// <returns></returns>
        private CategoryPartnerProduct GetPartnerProductForEdit(int currentPartnerId, int currentProductId, int categoryId)
        {
            CategoryPartnerProduct partnerProductToEdit = new CategoryPartnerProduct();
            try
            {
                ProjectTemplate currentProjectTemplate = GetViewState<ProjectTemplate>(ViewStateConstants.ProjectTemplate);
                Category cat = currentProjectTemplate.Categories.SingleOrDefault(p => p.PartnerProducts.Count() > 0 && p.PartnerProducts.Any(q => q.PartnerId > 0 && q.PartnerId == currentPartnerId && q.ProductId == currentProductId && q.CategoryId == categoryId));
                partnerProductToEdit.PartnerId = currentPartnerId;
                partnerProductToEdit.ProductId = currentProductId;
                partnerProductToEdit.IsActive = (cat.PartnerProducts.Single(q => q.PartnerId == currentPartnerId && q.ProductId == currentProductId)).IsActive;
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                if (rethrow)
                {
                    throw exception;
                }
            }
            resultUpdatePanel.Update();
            return partnerProductToEdit;
        }

        /// <summary>
        /// create initial category drop down
        /// </summary>
        private void CreateInitialDropDowns()
        {
            try
            {
                Category selectedCategory = ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture));

                for (int catLevel = 1; catLevel <= Convert.ToInt32(selectedCategory.Level); catLevel++)
                {
                    CreateDropDownList("ddlDynamic-" + Convert.ToString(catLevel, CultureInfo.InvariantCulture));
                }
                PopulateInitialValues(selectedCategory);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// populate the category drop downs with initial values
        /// </summary>
        /// <param name="selectedCategory"></param>
        private void PopulateInitialValues(Category selectedCategory)
        {
            try
            {
                ArrayList catHierarchyList = new ArrayList();
                int parentCatId;
                int selectedCategoryLevel = selectedCategory.Level;
                Category category = new Category();
                for (int initialDropDownCount = selectedCategoryLevel; initialDropDownCount >= 1; initialDropDownCount--)
                {
                    if (initialDropDownCount == selectedCategoryLevel)
                    {
                        category = selectedCategory;
                    }
                    else
                    {
                        parentCatId = category.ParentCategoryId;
                        category = ReadCategory(parentCatId.ToString(CultureInfo.InvariantCulture));
                    }
                    catHierarchyList.Add(category);
                }
                catHierarchyList.Reverse();
                Category currentCategory;
                for (int initialDropDownCount = selectedCategoryLevel; initialDropDownCount >= 1 && catHierarchyList.Count >= 0; initialDropDownCount--)
                {
                   DropDownList currentDropDown = (DropDownList)parentCategoryContainer.FindControl("ddlDynamic-" + Convert.ToString(initialDropDownCount, CultureInfo.InvariantCulture));
                    currentCategory = (Category)(catHierarchyList[initialDropDownCount - 1]);
                    ProjectTemplate currentProjectTemplate = GetViewState<ProjectTemplate>(ViewStateConstants.ProjectTemplate);
                    IList<Category> categoryList = currentProjectTemplate.Categories.AsEnumerable().Where(p => p.RootParentId == currentCategory.RootParentId && p.Level == currentCategory.Level).ToList();
                    BindCategoryDropDowns(currentDropDown, categoryList);
                    currentDropDown.SelectedValue = ((Category)(catHierarchyList[initialDropDownCount - 1])).CategoryId.ToString(CultureInfo.InvariantCulture);
                    catHierarchyList.RemoveAt(initialDropDownCount - 1);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///binds the category list from db to drop downs
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindCategoryDropDowns(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            categoryDropDown.DataSource = categoryList;
            categoryDropDown.DataTextField = "CategoryName";
            categoryDropDown.DataValueField = "CategoryId";
            categoryDropDown.DataBind();
        }

        /// <summary>
        ///binds the drop down of partners with partner list from db
        /// </summary>
        /// <param name="partnerList"></param>
        public void BindPartnerDropDown(IList<Partner> partnerList)
        {
            partnerDropDownList.DataSource = partnerList;
            partnerDropDownList.DataTextField = "CompanyName";
            partnerDropDownList.DataValueField = "PartnerId";
            partnerDropDownList.DataBind();
            partnerDropDownList.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1232, culture, "Select a Partner"), "-1"));
            PartnerListUpdatePanel.Update();
        }

        /// <summary>
        ///binds the drop down of products with products list from db as per selected partner
        /// </summary>
        /// <param name="productList"></param>
        public void BindProductDropDown(IList<Product> productList)
        {
            productDropDownList.DataSource = productList;
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "ProductId";
            productDropDownList.DataBind();
            productDropDownList.Items.Insert(0, new ListItem(DropDownConstants.None, "-1"));
            PartnerListUpdatePanel.Update();
        }

        /// <summary>
        ///creates the drop down list dynamically
        /// </summary>
        /// <param name="ID"></param>
        private void CreateDropDownList(string ID)
        {
            try
            {
                HtmlGenericControl rowDivision = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                HtmlGenericControl labelContainerDivision = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                HtmlGenericControl dropDownContainerDivision = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");

                rowDivision.Attributes.Add("class", "row");
                rowDivision.ID = "rowDivision" + ID;
                Label categoryLabel = new Label();
                categoryLabel.CssClass = "label";
                categoryLabel.Text = ResourceUtility.GetLocalizedString(287, culture);

                labelContainerDivision.Controls.Add(categoryLabel);
                dropDownContainerDivision.Attributes.Add("class", "input no-pad");

                DropDownList parentCategoryDropDown = new DropDownList();
                parentCategoryDropDown.ID = ID;
                parentCategoryDropDown.AutoPostBack = true;
                dropDownContainerDivision.Controls.Add(parentCategoryDropDown);
                parentCategoryDropDown.Enabled = false;
                rowDivision.Controls.Add(labelContainerDivision);
                rowDivision.Controls.Add(dropDownContainerDivision);
                parentCategoryContainer.Controls.Add(rowDivision);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///get all the sub categories for a particular category
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            ProjectTemplate currentProjectTemplate = GetViewState<ProjectTemplate>(ViewStateConstants.ProjectTemplate);
            IList<Category> categoryList = (currentProjectTemplate.Categories.AsEnumerable().
                                            Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture)) && (p.HasProducts == false)).ToList());
            return categoryList;
        }

       
        /// <summary>
        ///gets all the root level categories
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetRootLevelCategories()
        {
            ProjectTemplate currentProjectTemplate = GetViewState<ProjectTemplate>(ViewStateConstants.ProjectTemplate);
            IList<Category> rootCategoryList = (currentProjectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1 && (p.HasProducts == false))).ToList();
            return rootCategoryList;
        }

        /// <summary>
        ///populates the  category drop down as per the levels
        /// </summary>
        /// <param name="parentCategoryDropDown"></param>
        /// <param name="selectedValue"></param>
        /// <param name="selectedCategory"></param>
        private void PopulateDropDown(DropDownList parentCategoryDropDown, string selectedValue, Category selectedCategory)
        {
            try
            {
                parentCategoryDropDown.Items.Clear();

                IList<Category> categoryList = new List<Category>();
                if (string.IsNullOrEmpty(selectedValue))
                {
                    categoryList = GetRootLevelCategories();
                }
                else if (selectedValue != "-1")
                {
                    categoryList = GetChildCategories(selectedValue);
                }
                if (mode.Equals(UIConstants.Edit.ToLower(CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                {
                    if (categoryList.Contains(selectedCategory))
                        categoryList.Remove(selectedCategory);
                }
                if (categoryList != null)
                {
                    BindCategoryDropDowns(parentCategoryDropDown, categoryList);
                }
                parentCategoryDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1232, culture, "Select a Partner"), "-1"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///reads teh details of the category for a given id
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(string currentSelectedCategoryId)
        {
            ProjectTemplate currentProjectTemplate = GetViewState<ProjectTemplate>(ViewStateConstants.ProjectTemplate);
            Category category = (Category)(currentProjectTemplate.Categories.Single(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)));
            if (category != null)
                return category;
            else
                return null;
        }

        /// <summary>
        ///saves the partner / product addition or updation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveProductLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.IsValid)
                {
                    CategoryPartnerProduct partnerProduct = CreatePartnerProduct();
                    bool isProductUpdated;
                    if (mode.Equals(UIConstants.Add.ToLower(CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                    {
                        isProductUpdated = _projectTemplateManager.AddPartnerProduct(partnerProduct);
                        if (isProductUpdated)
                        {
                            SetSession<string>(SessionConstants.TemplateOperationResult, ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerProductSuccessfullyAdded, culture));
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                        }
                        else
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                        }
                    }
                    else
                    {
                        isProductUpdated = _projectTemplateManager.UpdatePartnerProduct(partnerProduct);
                        if (isProductUpdated)
                        {
                            SetSession<string>(SessionConstants.TemplateOperationResult, ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerProductSuccessfullyUpdated, culture));
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                        }
                        else
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            resultUpdatePanel.Update();
        }

        /// <summary>
        ///validates all the values entered for adding / editing
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void PartnerValidator_ServerValidation(object source, ServerValidateEventArgs args)
        {
            try
            {
                Category category = ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture));
                if (mode.Equals(UIConstants.Add.ToLower(CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                {
                    if (partnerDropDownList.SelectedValue != "-1")
                    {
                        if (category.PartnerProducts.Count > 0)
                        {
                            var partnerProduct = category.PartnerProducts.Where(p => (p.PartnerId == Convert.ToInt32(partnerDropDownList.SelectedValue, CultureInfo.InvariantCulture)));
                            if (partnerProduct != null)
                            {
                                foreach (var item in partnerProduct)
                                {
                                    if (item.ProductId == 0 && Convert.ToInt32(productDropDownList.SelectedValue, CultureInfo.InvariantCulture) == -1)
                                    {
                                        args.IsValid = false;
                                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicatePartner, culture));
                                        break;
                                    }
                                    if (Convert.ToInt32(productDropDownList.SelectedValue, CultureInfo.InvariantCulture) == item.ProductId)
                                    {
                                        args.IsValid = false;
                                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicatePartnerProduct, culture));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        args.IsValid = false;
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.MandatoryPartnerSelection, culture)); ;
                    }
                }
                else
                {
                    if ((GetViewState(SessionConstants.SelectedPartnerId).ToString() == partnerDropDownList.SelectedValue) && (GetViewState(SessionConstants.SelectedProductId).ToString() == productDropDownList.SelectedValue)
                        && (currentProductId == 0 || currentProductId == Convert.ToInt32(productDropDownList.SelectedValue, CultureInfo.InvariantCulture)))
                    {
                        if (statusDropDownList.SelectedValue.ToLower() == GetViewState(SessionConstants.SelectedPartnerProductStatus).ToString())
                        {
                            args.IsValid = false;
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicatePartnerProduct, culture));
                        }
                    }
                    else
                    {
                        if (partnerDropDownList.SelectedValue != "-1")
                        {
                            if (category.PartnerProducts.Count > 0)
                            {
                                var partnerProduct = category.PartnerProducts.Where(p => (p.PartnerId == Convert.ToInt32(partnerDropDownList.SelectedValue, CultureInfo.InvariantCulture)));
                                if (partnerProduct != null)
                                {
                                    foreach (var item in partnerProduct)
                                    {
                                        if (item.ProductId == 0 && Convert.ToInt32(productDropDownList.SelectedValue, CultureInfo.InvariantCulture) == -1)
                                        {
                                            args.IsValid = false;
                                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicatePartner, culture));
                                            break;
                                        }
                                        if (Convert.ToInt32(productDropDownList.SelectedValue, CultureInfo.InvariantCulture) == item.ProductId)
                                        {
                                            args.IsValid = false;
                                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicatePartnerProduct, culture));
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            args.IsValid = false;
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.MandatoryPartnerSelection, culture)); ;
                        }
                    }
                }
                resultUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                args.IsValid = false;
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.MandatoryPartnerSelection, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Create Partner Product
        /// </summary>
        /// <returns></returns>
        private CategoryPartnerProduct CreatePartnerProduct()
        {
            CategoryPartnerProduct categoryPartnerProduct = new CategoryPartnerProduct();
            try
            {
                categoryPartnerProduct.CategoryId = currentCategoryId;
                categoryPartnerProduct.TemplateId = projectTemplateId;
                categoryPartnerProduct.PartnerId = Convert.ToInt32(partnerDropDownList.SelectedValue, CultureInfo.InvariantCulture);
                Category category = ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture));
                if (!mode.Equals(UIConstants.Add.ToLower(CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                {
                    categoryPartnerProduct.OldPartnerId = Convert.ToInt32(Request.QueryString["partner"], CultureInfo.InvariantCulture);
                    categoryPartnerProduct.OldProductId = Convert.ToInt32(Request.QueryString["product"], CultureInfo.InvariantCulture);
                }

                categoryPartnerProduct.ProductId = Convert.ToInt32(productDropDownList.SelectedValue, CultureInfo.InvariantCulture);
                if (statusDropDownList.SelectedValue == "1")
                {
                    categoryPartnerProduct.IsActive = true;
                }
                else
                {
                    categoryPartnerProduct.IsActive = false;
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                if (rethrow)
                {
                    throw exception;
                }
            }
            resultUpdatePanel.Update();
            return categoryPartnerProduct;
        }

        /// <summary>
        /// Retrieve Partner List
        /// </summary>
        /// <param name="currentCategoryId"></param>
        /// <param name="projectTemplateId"></param>
        /// <returns></returns>
        private IList<Partner> RetrievePartnerList()
        {
            IList<Partner> partnerList = new List<Partner>();
            try
            {
                partnerList = _projectTemplateManager.GetActivePartnerList();
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                if (rethrow)
                {
                    throw exception;
                }
                resultUpdatePanel.Update();
            }
            return partnerList;
        }

        /// <summary>
        /// Retrieve Product List for partner
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        private IList<Product> RetrieveProductList(int partnerId)
        {
            IList<Product> productList = new List<Product>();
            try
            {
                CategoryPartnerProduct categoryPartnerProduct = new CategoryPartnerProduct();
                categoryPartnerProduct.PartnerId = partnerId;
                productList = _projectTemplateManager.GetProductList(categoryPartnerProduct);
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                if (rethrow)
                {
                    throw exception;
                }
                resultUpdatePanel.Update();
            }
            return productList;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void partnerDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = sender as DropDownList;
                BindProductDropDown(RetrieveProductList(Convert.ToInt32(ddl.SelectedValue, CultureInfo.InvariantCulture)));
                productDropDownList.SelectedValue = "-1";
                productDropDownList.Enabled = Convert.ToInt32(ddl.SelectedValue, CultureInfo.InvariantCulture) == -1 ? false : true;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}