﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="Settings.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Settings" %>

<asp:Content ID="content2" ContentPlaceHolderID="cphHead" runat="server">
    <script src="../Scripts/jquery-tinymce.js" type="text/javascript"></script>
    <script src="../Scripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
    <script type="text/javascript">

        // Initializes all textareas with the tinymce class
        $(document).ready(function () {

            //    var versionValue = $('select#cphDetails_termsAndConditionDropDown option:selected').val();
            //  alert(versionValue)
            //  if (versionValue == "0") {

            tinyMCE.init(
        {
            script_url: '../Scripts/tiny_mce/tiny_mce.js',
            mode: "exact",
            elements: "ctl00$cphDetails$termAndConditionTextBox",
            plugins: "lists,spellchecker,inlinepopups,preview,table,contextmenu,paste,directionality,fullscreen,",

            // Theme options
            theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,sub,sup,|,hr,|,removeformat,|,charmap,",
            theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,|,code,|,insertdate,inserttime,preview,visualaid,|,fullscreen,",
            theme_advanced_buttons3: "tablecontrols,",
            theme_advanced_buttons4: "ltr,rtl,|,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_disable: "code",
            theme_advanced_statusbar_location: "bottom",
            theme_advanced_resizing: false,
            visualblocks_default_state: true,

            // Schema is HTML5 instead of default HTML4
            schema: "html5",

            // End container block element when pressing enter inside an empty block
            end_container_on_empty_block: true,

            // Skin options
            skin: "o2k7",
            skin_variant: "silver",
            oninit: CustomOnInit,
            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "js/template_list.js",
            external_link_list_url: "js/link_list.js",
            external_image_list_url: "js/image_list.js",
            media_external_list_url: "js/media_list.js",

            // HTML5 formats
            style_formats: [
                { title: 'h1', block: 'h1' },
                { title: 'h2', block: 'h2' },
                { title: 'h3', block: 'h3' },
                { title: 'h4', block: 'h4' },
                { title: 'h5', block: 'h5' },
                { title: 'h6', block: 'h6' },
                { title: 'p', block: 'p' },
                { title: 'div', block: 'div' },
                { title: 'pre', block: 'pre' },
                { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
                { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
                { title: 'blockquote', block: 'blockquote', wrapper: true },
                { title: 'hgroup', block: 'hgroup', wrapper: true },
                { title: 'aside', block: 'aside', wrapper: true },
                { title: 'figure', block: 'figure', wrapper: true }
        ]

        });
            //tinyMCE.activeEditor.getBody().setAttribute('contenteditable', false);

            function CustomOnInit() {
                var versionValue = $('select#cphDetails_termsAndConditionDropDown option:selected').val();
                //  tinyMCE.activeEditor.settings.readonly = true;

                //      alert(versionValue)
                if (versionValue == "0") {
                    tinyMCE.activeEditor.getBody().setAttribute('contenteditable', true);

                }
                else {
                    tinyMCE.activeEditor.getBody().setAttribute('contenteditable', false);
                    tinyMCE.activeEditor.getBody().style.backgroundColor = "lightgray";

                }
            }
        });

        function ReInitializeTinyMCE() {

            tinyMCE.idCounter = 0;
            tinyMCE.execCommand('mceRemoveControl', true, 'cphDetails_termAndConditionTextBox');

            tinyMCE.init(
            {
                script_url: '../Scripts/tiny_mce/tiny_mce.js',
                mode: "exact",
                elements: "ctl00$cphDetails$termAndConditionTextBox",
                theme: "advanced",
                plugins: "lists,spellchecker,inlinepopups,preview,table,contextmenu,paste,directionality,fullscreen,",

                // Theme options
                theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,sub,sup,|,hr,removeformat,|,charmap,",
                theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,|,code,|,insertdate,inserttime,preview,visualaid,|,fullscreen,",
                theme_advanced_buttons3: "tablecontrols,",
                theme_advanced_buttons4: "ltr,rtl,|,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_disable: "code",
                theme_advanced_resizing: false,
                visualblocks_default_state: true,

                // Schema is HTML5 instead of default HTML4
                schema: "html5",

                // End container block element when pressing enter inside an empty block
                end_container_on_empty_block: true,

                // Skin options
                skin: "o2k7",
                skin_variant: "silver",

                // Drop lists for link/image/media/template dialogs
                template_external_list_url: "js/template_list.js",
                external_link_list_url: "js/link_list.js",
                external_image_list_url: "js/image_list.js",
                media_external_list_url: "js/media_list.js",

                // HTML5 formats
                style_formats: [
                { title: 'h1', block: 'h1' },
                { title: 'h2', block: 'h2' },
                { title: 'h3', block: 'h3' },
                { title: 'h4', block: 'h4' },
                { title: 'h5', block: 'h5' },
                { title: 'h6', block: 'h6' },
                { title: 'p', block: 'p' },
                { title: 'div', block: 'div' },
                { title: 'pre', block: 'pre' },
                { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
                { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
                { title: 'blockquote', block: 'blockquote', wrapper: true },
                { title: 'hgroup', block: 'hgroup', wrapper: true },
                { title: 'aside', block: 'aside', wrapper: true },
                { title: 'figure', block: 'figure', wrapper: true }
        ]

            });

            var versionValue = $('select#cphDetails_termsAndConditionDropDown option:selected').val();

            if (versionValue == "0") {
                // alert('hello');
                tinyMCE.get(tinyMCE.activeEditor.editorId).getBody().setAttribute('contenteditable', 'true');
            }
            else {
                // alert('disabled by me');
                tinyMCE.get(tinyMCE.activeEditor.editorId).getBody().setAttribute('contenteditable', 'false');
                tinyMCE.activeEditor.getBody().style.backgroundColor = "lightgray";
            }

        }

        function UpdateTextArea() {
            tinyMCE.triggerSave(false, true);
        }

        function ValidateTinyMCE(sender, args) {
            //  alert("blank");
            var isValid = false;
            var value = tinyMCE.get(tinyMCE.activeEditor.editorId).getContent({ format: 'text' });
            if (value.replace(/\s/g, "").length == 0) {
                args.IsValid = false;
                //         alert("blank");
            }
            else {
                //Check for space tag
                if (value == "<p>&nbsp;</p>") {
                    //Clear TinyMCE
                    tinyMCE.get(tinyMCE.activeEditor.editorId).execCommand('mceSetContent', false, "");
                    args.IsValid = false;
                    //           alert("cleaned");
                }
                else {
                    args.IsValid = true;
                    //         alert("default");

                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphDetails" runat="server" ViewStateMode="Enabled">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="dropDown">
        <div class="wrapper">
            <div class="rgt-bg">
                <ul>
                    <li>
                        <asp:Label ID="generalSettingsLabel" runat="server" class="active" CBID="396" Text="General Settings"></asp:Label></li>
                    <li>
                        <asp:HyperLink ID="contentBlockHyperLink" runat="server" CBID="336" NavigateUrl="~/Admin/ContentBlocks.aspx"
                            Text="Content Blocks"></asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="emailTemplateHyperLink" Visible="false" runat="server" CBID="972"
                            NavigateUrl="~/Admin/EmailTemplates.aspx" Text="Email Templates"></asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="search">
        <div class="wrapper">
        </div>
    </div>
    <div class="content">
        <div class="wrapper">
            <h2>
                <asp:Label ID="manageGeneralSettingsLabel" runat="server" class="active" CBID="431"
                    Text="Manage General Settings"></asp:Label></h2>
            <div class="inner-content">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="saveGeneralSettingsLinkButton" EventName="Click" />
                        <%-- <asp:AsyncPostBackTrigger ControlID="saveTermAndConditionLinkButton" EventName="Click" />--%>
                    </Triggers>
                    <ContentTemplate>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                        <br />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="generalSettingsUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="lft-col">
                            <div id="email">
                                <h3>
                                    <asp:Label ID="emailRecipientsLabel" runat="server" class="active" CBID="390" Text="Email Recipients"></asp:Label></h3>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="partnerAccountRequestLabel" runat="server" CBID="490" Text="Partner Account Requests:"></asp:Label></div>
                                    <div class="input textbox3">
                                        <asp:TextBox ID="partnerAccountRequestsTextBox" runat="server" />
                                        <div class="error-message">
                                            <asp:RegularExpressionValidator ID="partnerAccountRegularExpressionValidator" runat="server"
                                                ValidationGroup="SaveSettings" ControlToValidate="partnerAccountRequestsTextBox"
                                                VMTI="11" SetFocusOnError="true" ErrorMessage="Invalid Email ID" CssClass="errorText1"
                                                Display="Dynamic"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="partnerAccountRequiredFieldValidator" VMTI="10" SetFocusOnError="true"
                                                ValidationGroup="SaveSettings" runat="server" ErrorMessage="Required" CssClass="errorText1"
                                                ControlToValidate="partnerAccountRequestsTextBox" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="productRequestsLabel" runat="server" CBID="538" Text="Product Requests:"></asp:Label></div>
                                    <div class="input textbox3">
                                        <asp:TextBox ID="productRequestsTextBox" runat="server" />
                                        <div class="error-message">
                                            <asp:RegularExpressionValidator ID="productRequestsRegularExpressionValidator" VMTI="11"
                                                ValidationGroup="SaveSettings" runat="server" CssClass="errorText1" ControlToValidate="productRequestsTextBox"
                                                ErrorMessage="Invalid Email ID" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="productRequestsRequiredFieldValidator" VMTI="10"
                                                ValidationGroup="SaveSettings" SetFocusOnError="true" runat="server" CssClass="errorText1"
                                                ControlToValidate="productRequestsTextBox" ErrorMessage="Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="feedbackLabel" runat="server" CBID="151" Text="Feedback:"></asp:Label></div>
                                    <div class="input textbox3">
                                        <asp:TextBox ID="feedbackTextBox" runat="server" />
                                        <div class="error-message">
                                            <asp:RegularExpressionValidator ID="feedbackRegularExpressionValidator" runat="server"
                                                ValidationGroup="SaveSettings" ControlToValidate="feedbackTextBox" VMTI="11"
                                                ErrorMessage="Invalid Email ID" CssClass="errorText1" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="FeedbackRequiredFieldValidator" runat="server" ControlToValidate="feedbackTextBox"
                                                ValidationGroup="SaveSettings" CssClass="errorText1" ErrorMessage="Required"
                                                VMTI="10" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="featured">
                                <h3>
                                    <asp:Label ID="featuredProjectsLabel" runat="server" class="active" CBID="149" Text="Featured Projects"></asp:Label></h3>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="navigationVisibleLabel" runat="server" CBID="451" Text="Navigation Visible?"></asp:Label></div>
                                    <div class="input">
                                        <p>
                                            <asp:RadioButton ID="navigationYesRadioButton" runat="server" GroupName="NavigationVisible"
                                                Checked="true" />
                                            <asp:Label ID="yesLabel" runat="server" CBID="757" Text="Yes"></asp:Label></p>
                                        <p>
                                            <asp:RadioButton ID="navigationNoRadioButton" runat="server" GroupName="NavigationVisible" />
                                            <asp:Label ID="noLabel" runat="server" CBID="459" Text="No"></asp:Label></p>
                                    </div>
                                </div>
                            </div>
                            <div class="section alR pad-top">
                                <span class="btn-style">
                                    <asp:LinkButton ID="saveGeneralSettingsLinkButton" runat="server" CBID="642" class="save"
                                        ValidationGroup="SaveSettings" OnClick="SaveGeneralSettingsLinkButton_Click">
                                    </asp:LinkButton>
                                </span>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="rgt-col">
                    <asp:UpdatePanel ID="partnerPriceUpdatePanel" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="saveGeneralSettingsLinkButton" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <div id="partners">
                                <h3>
                                    <asp:Label ID="partnerPricingLabel" runat="server" class="active" CBID="495" Text="Partner Pricing"></asp:Label></h3>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="goldLabel" runat="server" CBID="0" Text="Gold (USD):"></asp:Label></div>
                                    <div class="sign">
                                        $</div>
                                    <div class="input textbox3">
                                        <asp:TextBox ID="goldPriceTextBox" runat="server" />
                                        <div class="error-message">
                                            <asp:RegularExpressionValidator ID="goldPriceRegularExpressionValidator" runat="server"
                                                ValidationGroup="SaveSettings" CssClass="errorText1" ControlToValidate="goldPriceTextBox"
                                                VMTI="36" ErrorMessage="Only Numbers allowed" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="goldPriceRequiredFieldValidator" runat="server" ControlToValidate="goldPriceTextBox"
                                                ValidationGroup="SaveSettings" CssClass="errorText1" ErrorMessage="Amount is Required"
                                                VMTI="35" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="regularLabel" runat="server" CBID="0" Text="Partner (USD):"></asp:Label></div>
                                    <div class="sign">
                                        $</div>
                                    <div class="input textbox3">
                                        <asp:TextBox ID="regularPriceTextBox" runat="server" />
                                        <div class="error-message">
                                            <asp:RegularExpressionValidator ID="regularPriceRegularExpressionValidator" runat="server"
                                                ValidationGroup="SaveSettings" CssClass="errorText1" ControlToValidate="regularPriceTextBox"
                                                VMTI="36" ErrorMessage="Only Numbers Allowed" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="regularPriceRequiredFieldValidator" runat="server"
                                                ValidationGroup="SaveSettings" CssClass="errorText1" ControlToValidate="regularPriceTextBox"
                                                VMTI="35" ErrorMessage="Amount is Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="termsAndConditionUpdatePanel" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="saveTermAndConditionLinkButton" />
                        </Triggers>
                        <ContentTemplate>
                            <div id="termsandcondition">
                                <h3>
                                    <asp:Label ID="termsAndConditionLabel" runat="server" class="active" CBID="0" Text="Terms and Condition"></asp:Label>
                                </h3>
                                <div class="row">
                                    <div class="label textbox3 ">
                                        <asp:DropDownList ID="termsAndConditionDropDown" runat="server" AutoPostBack="true"
                                            CssClass="width150" onchange="UpdateTextArea()" OnSelectedIndexChanged="TermsAndConditionDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="editor1">
                                        <asp:TextBox ID="termAndConditionTextBox" runat="server" Rows="10" CssClass="widthTC"
                                            ValidationGroup="TermAndCondition" TextMode="MultiLine" />
                                        <div class="error-message noindent">
                                            <asp:CustomValidator ID="contentCustomValidator" ClientValidationFunction="ValidateTinyMCE"
                                                VMTI="44" runat="server" CssClass="errorText" ValidationGroup="TermAndCondition"
                                                ErrorMessage="Content is required"></asp:CustomValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section alR pad-top" id="saveTermsAndCondionDiv" runat="server">
                                <span class="btn-style">
                                    <asp:LinkButton ID="saveTermAndConditionLinkButton" runat="server" CBID="642" class="save"
                                        OnClientClick="UpdateTextArea()" ValidationGroup="TermAndCondition" OnClick="SaveTermAndConditionsLinkButton_Click">
                                    </asp:LinkButton>
                                </span>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>