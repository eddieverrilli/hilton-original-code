﻿<%@ Page  Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" EnableViewState="false" CodeBehind="ConstructionReport.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.Admin.ConstructionReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>

    <div class="dropDown">
        <div class="wrapper">
            <div class="rgt-bg"></div>
         </div>
    </div>
   <div class="content">
        <div class="wrapper">
        <asp:LinkButton ID="downloadConstructionReportLinkButton" CBID="0" runat="server" CssClass="btn" OnClick="DownloadSpreadSheet_ClickLinkButton" Text="Download Construction Report"></asp:LinkButton>
            <h2>
                <asp:Label ID="downloadConstuctionLabel" runat="server" CBID="0" Text="Construction Report"></asp:Label>
            </h2>
            <div class="clear">
                            &nbsp;</div>
                <div class="inner-content">
                    <div class="construction">
                        <h4>
                            <asp:Label ID="constructionReportHeaderLabel" runat="server" CBID="0" Text="Download a list of all new builds along with property contact information"></asp:Label>
                        </h4>
                        <div class="clear">
                            &nbsp;</div>
                             <div id="resultMessageDiv" runat="server">
                                </div>
                        <div class="clear">
                            &nbsp;</div>
                        </div>
                </div>
        </div>
    </div>
</asp:Content>