﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="TransactionDetails.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.TransactionDetails" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../Scripts/custominputs.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.ui.datepicker.js"></script>
    <script src="../Scripts/jquery.characterlimiter.js" type="text/javascript"></script>  
</asp:Content>

<asp:Content ID="mainDetailContent" ContentPlaceHolderID="cphDetails" ViewStateMode="Enabled"
    runat="server">
    <asp:ScriptManager ID="transactionDetailsScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript">
        function pageLoad(){  
            $('textarea#commentTextbox').limiter(1000, $('#remainingCharacters'));
            };
        
    </script>
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../Images/search-top-curve.gif" alt="" /></div>
            <div class="middle">
                <asp:HyperLink NavigateUrl="Transactions.aspx" runat="server" ID="backToTransactionsHyperLink"
                    Text="‹‹ Back To Transactions"></asp:HyperLink>
            </div>
            <div class="bottom-curve">
                <img src="../Images/search-bottom-curve.gif" alt="" />
            </div>
        </div>
    </div>
    <div class="content">
        <div class="wrapper">
            <h2>
                <asp:Label ID="transactionDetailsLabel" CBID="922" Text="Transaction Details" runat="server"></asp:Label>
            </h2>
            <div id="transaction" class="inner-content">
                <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                        <br />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="lft-col">
                    <div class="box1">
                        <h3>
                            <asp:Label ID="partnerLabel" CBID="925" Text="Partner" runat="server"></asp:Label>
                        </h3>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="statusStaticLabel" CBID="923" Text="Status:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="statusLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="typeStaticLabel" CBID="924" Text="Type:" runat="server"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:Label ID="typeLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="nameStaticLabel" CBID="926" Text="Name:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="nameLabel" runat="server"></asp:Label></div>
                        </div>
                    </div>
                    <div class="box1">
                        <h3>
                            <asp:Label ID="contactInformationLabel" CBID="927" Text="Contact Information" runat="server"></asp:Label></h3>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="companyNameContactInfoStaticLabel" CBID="928" Text="Company Name:"
                                    runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="companyNameContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="firstNameContactInfoStaticLabel" CBID="929" Text="First Name:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="firstNameContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="lastNameContactInfoStaticLabel" CBID="930" Text="Last Name:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="lastNameContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="address1ContactInfoStaticLabel" CBID="931" Text="Address:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Literal ID="address1ContactInfoLabel" runat="server"></asp:Literal></div>
                        </div>
                        <div class="row">
                            <div class="input">
                                <asp:Label ID="address2ContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="countryContactInfoStaticLabel" CBID="932" Text="Country:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="countryContactInfoLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="cityContactInfoStaticLabel" CBID="933" Text="City:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="cityContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="stateContactInfoSTaticLabel" CBID="934" Text="State:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="stateContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="zipContactInfoStaticLabel" CBID="935" Text="Zip:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="zipContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="phoneContactInfoStaticLabel" CBID="936" Text="Phone:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="phoneContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="faxContactInfoStaticLabel" CBID="937" Text="Fax:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="faxContactInfoLabel" runat="server"></asp:Label></div>
                        </div>
                    </div>
                    <div class="box1">
                        <h3>
                            <asp:Label ID="billingInformationLabel" CBID="938" Text="Billing Information" runat="server"></asp:Label></h3>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="companyNameBillingInfoStaticLabel" CBID="928" Text="Company Name:"
                                    runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="companyNameBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="firstNameBillingInfoStaticLabel" CBID="929" Text="First Name:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="firstNameBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="lastNameBillingInfoStaticLabel" CBID="930" Text="Last Name:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="lastNameBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="address1BillingInfoStaticLabel" CBID="931" Text="Address:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="address1BillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="input">
                                <asp:Label ID="address2BillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="countryBillingInfoStaticLabel" CBID="932" Text="Country:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="countryBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="cityBillingInfoSTaticLabel" CBID="933" Text="City:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="cityBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="stateBillingInfoStaticLabel" CBID="934" Text="State:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="stateBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="zipBillingInfoStaticLabel" CBID="935" Text="Zip:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="zipBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="phoneBillingInfoStaticLabel" CBID="936" Text="Phone:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="phoneBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="faxBillingInfoStaticLabel" CBID="937" Text="Fax:" runat="server"></asp:Label></div>
                            <div class="input">
                                <asp:Label ID="faxBillingInfoLabel" runat="server"></asp:Label></div>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="PaymentInfoUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="rgt-col">
                            <div class="box1">
                                <h3>
                                    <asp:Label ID="paymentInformationLabel" CBID="939" Text="Payment Information" runat="server"></asp:Label></h3>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="paymentStatusLabel" CBID="940" Text="Payment Status:" runat="server"></asp:Label></div>
                                    <div class="input no-pad">
                                        <asp:DropDownList ID="statusDropDownList" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="input">
                                        <asp:Literal ID="statusLiteral" runat="server" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="dateLabel" CBID="941" Text="Date:" runat="server"></asp:Label></div>
                                    <div class="input">
                                        <asp:Label ID="datePaymentInfoLabel" ViewStateMode="Enabled" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="row" id="transactionId" runat="server">
                                    <div class="label">
                                        <asp:Label ID="transactionIdLabel1" CBID="942" Text="Transaction ID:" runat="server"></asp:Label></div>
                                    <div class="input">
                                        <asp:Label ID="transactionIDPaymentInfoLabel" ViewStateMode="Enabled" runat="server"></asp:Label></div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="amountLabel" CBID="943" Text="Amount:" runat="server"></asp:Label></div>
                                    <div class="input">
                                        <asp:Label ID="amountPaymentInfoLabel" ViewStateMode="Enabled" runat="server"></asp:Label></div>
                                </div>
                                 <div class="row">
                                    <div class="label">
                                        <asp:Label ID="commentLable" CBID="0" Text="Comment:" runat="server"></asp:Label></div>
                                    <div class="input no-pad">
                                    <asp:TextBox ID="commentTextbox" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                                     <div class="errorText">
                                        <asp:Label runat="server" ID="remainingCharacters" ClientIDMode="Static"></asp:Label>
                                        <asp:Label runat="server" ID="remainingCharacterLabel" Text="characters remaining" ></asp:Label>    
                                    </div>
                                     </div>
                                     
                                </div>


                                <div class="row" id="errorRowDiv" runat="server">
                                    <div class="label">
                                        <asp:Label ID="errorMessageStaticLabel" CBID="944" Text="Error Message:" runat="server"></asp:Label></div>
                                    <div class="input">
                                        <asp:Label ID="errorMessageLabel" ViewStateMode="Enabled" runat="server"></asp:Label></div>
                                </div>
                                <div class="section alR pad-top" id="saveButtonDiv" runat="server">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="saveLinkButton" ViewStateMode="Enabled" runat="server" class="save"
                                            CBID="1045" Text="Save" OnClick="SaveLinkButton_Click"></asp:LinkButton>
                                    </span>
                                    <asp:HiddenField ID="paymentRecordIdHiddenField" ViewStateMode="Enabled" runat="server" />
                                </div>                                
                        </div>
                        
                    <div id="users" class="box">
                        <h3>
                            <asp:Label ID="usersLabel" runat="server" Text="Comment History" CssClass="label"></asp:Label>
                        </h3>
                        <div class="box-content" style="overflow: scroll;">
                            <div id="usersDiv" runat="server" >
                                <asp:GridView ID="gridviewCommentHistory" runat="server" AutoGenerateColumns="false" class="col-3">
                                    <Columns>                                       
                                        <asp:BoundField HeaderText="Recorded DateTime" DataField="RecordedDatetime" ItemStyle-CssClass="col-2" />
                                        <asp:BoundField HeaderText="User Name" DataField="UserName" ItemStyle-CssClass="col-3" />
                                        <asp:BoundField HeaderText="Comments" DataField="Comment" ItemStyle-CssClass="col-4"/>                                        
                                    </Columns>
                                </asp:GridView>
                            </div>
                            </div>
                            </div>
                            </div>                        
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>