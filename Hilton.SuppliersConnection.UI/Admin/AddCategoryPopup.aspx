﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCategoryPopup.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Admin.AddCategoryPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../Styles/iframe-modal.css" />
    <!--[if IE 8]>
    <link type='text/css' href='../Styles/adminie8.css' rel='stylesheet' />
    <![endif]-->
    <link href="../Styles/iewidthfix.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.ie-select-width.js" ></script>
    <script type="text/javascript" src="../Scripts/ie-select-width.js"></script>
    <title></title>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="addCategoryScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>    
    <script type="text/javascript">
        $(window).ready(function () {
            $('.cancel').click(function () {
                window.parent.closeIframe();
                return false;

            });
        })

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }

    </script>
    <div id="addCategoryModal" class="modalbox">
        <h3>
            <asp:Label ID="pageHeader" runat="server"></asp:Label></h3>
        <div class="modal-content">
            <div class="modal-innercontent">
                <div class="form-field">
                    <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="resultMessageDiv" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="row">
                        <div>
                            <asp:Label ID="categoryNameLabel" runat="server" class="label" Text="Category Name"
                                CBID="290"></asp:Label></div>
                        <div class="input textbox3 mandatory1">
                            <asp:TextBox ID="categoryNameTextBox" runat="server"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="categoryNameValidator" Enabled="true" VMTI="27" SetFocusOnError="true"
                                runat="server" ValidationGroup="standardMapping" ControlToValidate="categoryNameTextBox"
                                CssClass="errorText padleft" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div id="parentCategoryContainer" runat="server">
                    </div>
                    <div class="row">
                        <div>
                            <asp:Label ID="insertBeforeLabel" runat="server" class="label" Text="Insert" CBID="291"></asp:Label>
                        </div>
                        <div class="input no-pad">
                            <asp:DropDownList ID="positionDropDownList" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%--  pending for HBS integration--%>
                    <asp:UpdatePanel ID="standardMappingUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div>
                                    <span class="label">Standard:</span>
                                </div>
                                <div class="input textbox3 add-field">
                                    <asp:TextBox ID="standardTextBox" runat="server" CssClass="textbox-multiline" TextMode="MultiLine"
                                        ValidationGroup="standardTextSelection"></asp:TextBox>
                                    <asp:LinkButton ID="addStandardLinkButton" runat="server" Visible="false" class="add"
                                        ValidationGroup="standardTextSelection" OnCommand="AddStandardLinkButton_Command"></asp:LinkButton>
                                </div>
                                <div class="box-content">
                                    <asp:GridView ID="associatedStandardsGridView" Visible="false" ValidationGroup="standardMapping"
                                        runat="server" AutoGenerateColumns="false" class="col-3">
                                        <Columns>
                                            <asp:BoundField DataField="StandardNumber" ItemStyle-CssClass="first-col" />
                                            <asp:BoundField DataField="StandardDescription" />
                                            <asp:TemplateField>
                                                <ItemStyle CssClass="third-col" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="cancelImage" ImageUrl="../Images/cross.gif" runat="server" OnCommand="CancelImage_Command"
                                                        CommandArgument='<%# Bind("StandardNumber") %>' CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:CustomValidator ID="standardMappingValidator" Enabled="false" ValidationGroup="standardMapping"
                                        runat="server" OnServerValidate="StandardMappingValidator_ServerValidation"></asp:CustomValidator>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="row">
                        <div>
                            <asp:Label ID="descriptionLabel" runat="server" class="label" Text="Description"
                                CBID="361"></asp:Label>
                        </div>
                        <div class="input no-pad">
                            <asp:TextBox ID="descriptionTextBox" CssClass="textbox-multiline" TextMode="MultiLine"
                                runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <asp:Label ID="styleDescription" runat="server" class="label" Text="Style Description"
                                CBID="701"></asp:Label>
                        </div>
                        <div class="input no-pad">
                            <asp:TextBox ID="styleDescriptionTextBox" CssClass="textbox-multiline" TextMode="MultiLine"
                                runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <asp:Label ID="contactInfoLabel" runat="server" class="label" Text="Contact Info"
                                CBID="329"></asp:Label>
                        </div>
                        <div class="input no-pad">
                            <asp:TextBox ID="contactInfoTextBox" CssClass="textbox-multiline" TextMode="MultiLine"
                                runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <asp:Label ID="statusLabel" runat="server" class="label" Text="Status" CBID="692"></asp:Label>
                    </div>
                    <div class="row">
                        <div>
                            <asp:Label ID="isActiveLabel" runat="server" class="label" Text="Active?" CBID="170"></asp:Label>
                        </div>
                        <div class="input no-pad">
                            <asp:DropDownList ID="isActiveDropDownList" CssClass="status-width" runat="server">
                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="leafConfigDivision" runat="server">
                        <div class="row">
                            <div>
                                <asp:Label ID="isRequiredLabel" runat="server" class="label" Text="Required?" CBID="845"></asp:Label>
                            </div>
                            <div class="input no-pad">
                                <asp:DropDownList ID="isRequiredDropDownList" CssClass="status-width" runat="server">
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="isNewPartnersLabel" runat="server" class="label" Text="New Partners?"
                                    CBID="452"></asp:Label>
                            </div>
                            <div class="input no-pad">
                                <asp:DropDownList ID="isNewPartnerRequiredDropDownList" CssClass="status-width" runat="server">
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="isNewProductsLabel" runat="server" class="label" Text="New Products?"
                                    CBID="454"></asp:Label>
                            </div>
                            <div class="input no-pad ">
                                <asp:DropDownList ID="isNewProductRequiredDropDownList" CssClass="status-width" runat="server">
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="section alR pad-top">
                        <span class="btn-style btn-stylepad">
                            <asp:LinkButton ID="saveCategoryLinkButton" ValidationGroup="standardMapping" runat="server"
                                CBID="638" Text="Save" OnCommand="SaveCategoryLinkButton_Command"></asp:LinkButton>
                        </span><span class="btn-style ">
                            <asp:LinkButton ID="TB_closeWindowButton" runat="server" class="cancel simplemodal-overlay"
                                OnClick="CancelCategoryLinkButton_Click" Text="Cancel" CBID="275"></asp:LinkButton>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
