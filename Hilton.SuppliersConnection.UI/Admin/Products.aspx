﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ViewStateMode="Inherit" AutoEventWireup="True" CodeBehind="Products.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Products" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc1" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script type="text/javascript">

        function InitializeToggle() {
            var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleStateValue == "0") {
                document.getElementById('toggleDiv').style.display = "block";
            }
            else {
                $('.toggle-divider a')[0].removeAttribute('class');
                document.getElementById('toggleDiv').style.display = "none";
            }

            $('.col-6 tr, .col-7 tr').hover(function () {

                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            })
        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }
        $(document).ready(function () {
            getItems();
        });
    </script>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="productListUpdatePanel" runat="server" UpdateMode="Always">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="selectedBrands" EventName="TextChanged" />
        </Triggers>
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="quickSearchLinkButton" runat="server" OnClick="QuickSearch_Click"
                                        Text="Search" CBID="650" ValidationGroup="SearchProduct" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchProduct" ControlToValidate="searchTextBox"
                                    Display="Dynamic" ErrorMessage="Please specify search criteria" VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server" />
                            </div>
                        </div>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                            <div class="field">
                                <asp:Label ID="partnerLabel" runat="server" class="label" Text="Partner:" CBID="485"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="partnerDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="regionLabel" runat="server" class="label" Text="Region:" CBID="960"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="countryLabel" runat="server" class="label" Text="Country:" CBID="1300"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled" AutoPostBack="true"
                                        runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field" style="clear:both;">
                                <asp:Label ID="brandLabel" runat="server" class="label" Text="Brand:" CBID="961"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList runat="server" ID="optgroup" ViewStateMode="Enabled" ClientIDMode="Static"
                                        multiple="multiple">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="isCheckAll" runat="server" ClientIDMode="Static" />
                                    <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display: none;"
                                        AutoPostBack="true" OnTextChanged="SelectedBrands_TextChanged"></asp:TextBox>
                                </div>
                            </div>
                            <div style="visibility:hidden">
                                <asp:Label ID="propertyType" runat="server" class="label" Text="Property Type:" CBID="962"
                                    Visible="false"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="propertyTypeDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="PropertyTypeDropDown_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server" Visible="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="categoryLabel" runat="server" class="label" Text="Category:" CBID="963"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="categoryDropDown" ViewStateMode="Enabled" runat="server" OnSelectedIndexChanged="CategoryDropDown_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="subcategoryLabel" runat="server" class="label" Text="Sub Category:"
                                    CBID="964"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="subcategoryDropDown" ViewStateMode="Enabled" runat="server"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="statusLabel" runat="server" class="label" Text="Status:" CBID="697"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="statusDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                           <%-- <div class="field">
                            </div>--%>
                            <div class="field" >
                                <div class="alR searchpad-top">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="goLinkButton" Text="Go" runat="server" CBID="965" OnClick="GoButton_Click" />
                                    </span>
                                </div>
                            </div>
                            <div class="clear">
                                &nbsp;</div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <asp:LinkButton ID="addProductLinkButton" runat="server" Text="Add Product" OnClick="AddProductLinkButton_Click"
                        CssClass="btn"></asp:LinkButton>
                    <h2>
                        <asp:Label ID="productHeadingLabel" runat="server" class="label" Text="Products"
                            CBID="543"></asp:Label></h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <asp:GridView ID="productListGridView" runat="server" ViewStateMode="Enabled" CssClass="col-6"
                                AutoGenerateColumns="False" OnSorting="ProductListGridView_Sorting" OnRowCreated="ProductListGridView_RowCreated"
                                OnRowCommand="ProductListGridView_RowCommand" OnRowDataBound="ProductListGridView_RowDataBound"
                                AllowSorting="true" class="col-6">
                                <Columns>
                                    <asp:BoundField DataField="ModalName" SortExpression="SKUNumber" HeaderStyle-Width="10%" />
                                    <asp:BoundField DataField="ProductName" SortExpression="ProductName" HeaderStyle-Width="42%" />
                                    <asp:BoundField DataField="PartnerName" SortExpression="CompanyName" HeaderStyle-Width="31%" />
                                    <asp:BoundField DataField="StatusDescription" SortExpression="ProductStatusDesc"
                                        HeaderStyle-Width="15%" />
                                    <asp:TemplateField HeaderText="" ShowHeader="False" ItemStyle-Width="2%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="editLinkButton" runat="server" ViewStateMode="Enabled" CausesValidation="False"
                                                CommandName="EditProductList" CommandArgument='<%# Bind("ProductId") %>' Text="Edit"
                                                CBID="777"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc1:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
