﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="Partners.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.Partners" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script src="../Scripts/jquery-ui_popup.js"></script>
    <link href="../Styles/popup.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(document).ready(function () {
            $('#ErrorDelPartner').hide();
            $("#dialog-confirm").hide();
            $("#PartnerDelSuccessMsg").hide();
            if ($('#cphDetails_statusDropDown').val() != "7")
            { $("#cphDetails_downloadPartnerRptGettingDeleted").hide(); }
            $("#cphDetails_DeletePartnerLnk").hide();
            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });
            getItems();
        });

        function InitializeToggle() {
            $('#ErrorDelPartner').hide();
            if (document.getElementById('<%=toggleStateHiddenField.ClientID %>') != null)
            {
            var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleStateValue == "0") {
                document.getElementById('toggleDiv').style.display = "block";
            }
            }
        else {
            if ($('.toggle-divider a')[0] != null)
            { $('.toggle-divider a')[0].removeAttribute('class'); }
                document.getElementById('toggleDiv').style.display = "none";
            }

            $('.col-6 tr, .col-7 tr').hover(function () {

                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            });

            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });

        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }


        function NewExportToExcel_Clicked() {
            var delPartnerIds = '';
            $('input:checkbox[id^="cphDetails_partnerListGridView_chkDelPartner_"]').each(function () {
                if ($(this).is(":checked")) {
                    delPartnerIds = delPartnerIds + this.id.split('_')[3] + ',';
                }
            });
            $('#hidDelPartnerIds').attr('value', delPartnerIds);

            if (delPartnerIds == "") {
                $('#ErrorDelPartner').show();
                return false;
            } else {
                $('#ErrorDelPartner').hide();
                $("#cphDetails_downloadPartnerRptGettingDeleted").hide();
                $("#cphDetails_downloadPartnerReportLinkButton").hide();
                $("#cphDetails_DeletePartnerLnk").show();
                $('input:checkbox[id^="cphDetails_partnerListGridView_chkDelPartner_"]').each(function () {
                    $(this).attr("disabled", true);
                });
                return true;
            }       
        }




        function ShowDialog() {
            $mydialog = $("#dialog-confirm").dialog({
                resizable: false,
                height: 140,
                modal: true,
                closeOnEscape: false,
                buttons: {
                    "Confirm": function () {
                        $(this).dialog("close");
                        CallbackForPartnerDel(true);
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                        CallbackForPartnerDel(false);
                    }
                }
            });
               $('#dialog-confirm').prev().find('button').hide();         
        }



        function  CallbackForPartnerDel(value) {
            if (value) {
                var delPartnerIds = $('#hidDelPartnerIds').val();
                CallServerMethod_DelPartners(delPartnerIds);
            } else {
                $("#cphDetails_downloadPartnerRptGettingDeleted").show();
                $("#cphDetails_downloadPartnerReportLinkButton").hide();
                $("#cphDetails_DeletePartnerLnk").hide();
                $('input:checkbox[id^="cphDetails_partnerListGridView_chkDelPartner_"]').each(function () {
                    $(this).removeAttr("disabled");
                });
            }
            return value;
        }

        function CallServerMethod_DelPartners(delPartnerIds) {
            $.ajax({
                type: "POST",
                url: "Partners.aspx/DeletePartners",
                //data: '{"partnerIds":' + PartnerIds + '}',
                data: "{ 'partnerIds':'" + delPartnerIds + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = data.d;
                    if (result == "success") {
                        ShowPartnerDelSuccessMsg();
                    }
                    else if (result == "error") {
                        $('#cphContent_resultMessageDiv').innerHTML = "An application error occurred. Please try again. If the problem persists, contact Administrator."; 
                    }
                    else {
                        $('#cphContent_resultMessageDiv').innerHTML = "An application error occurred. Please try again. If the problem persists, contact Administrator."; 
                    }
                },
                error: function (e) {
                    $('#cphContent_resultMessageDiv').innerHTML = "An application error occurred. Please try again. If the problem persists, contact Administrator."; 
                }
            });
        }

        function ShowPartnerDelSuccessMsg() {
            $mydialog = $("#PartnerDelSuccessMsg").dialog({
                resizable: false,
                height: 140,
                modal: true,
                closeOnEscape: false,
                buttons: {
                    "Ok": function () {
                        $(this).dialog("close");
                         window.location.href = "../Admin/Partners.aspx?Par=yes";
                    }
                }
            });
            $('#PartnerDelSuccessMsg').prev().find('button').hide();
        }

    </script>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="cphDetails" ViewStateMode="Enabled"
    runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="partnerUpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="quickSearchLinkButton" Text="Search" runat="server" CBID="649"
                                        OnClick="QuickSearch_Click" ValidationGroup="SearchPartners" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchPartners"
                                    ControlToValidate="searchTextBox" Display="Dynamic" ErrorMessage="Please specify search criteria"
                                    VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server" />
                            </div>
                        </div>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                            <div class="field">
                                <asp:Label ID="requestSubmittedDateLabel" runat="server" class="label" Text="Request Submitted Date:"
                                    CBID="351"></asp:Label>
                                <div class="textbox-1">
                                    <asp:TextBox ID="requestSubmittedDateFromTextBox" runat="server" class="datepicker"></asp:TextBox>
                                </div>
                                <asp:Label ID="requestSubmittedDateToLabel" runat="server" CBID="950" class="label"
                                    Text="To:"></asp:Label>
                                <div class="textbox-1">
                                    <asp:TextBox ID="requestSubmittedDateToTextBox" runat="server" class="datepicker"></asp:TextBox>
                                </div>
                                <div class="error-message search-msg">
                                    <asp:CustomValidator ID="validateRequestDateCustomValidator" runat="server" />
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="expirationDateLabel" runat="server" class="label" Text="Expiration:"
                                    CBID="391"></asp:Label>
                                <div class="textbox-1">
                                    <asp:TextBox ID="expirationDateFromTextBox" runat="server" class="datepicker"></asp:TextBox>
                                </div>
                                <asp:Label ID="expirationDateToLabel" runat="server" class="label" CBID="950" Text="To:"></asp:Label>
                                <div class="textbox-1">
                                    <asp:TextBox ID="expirationDateToTextBox" runat="server" class="datepicker"></asp:TextBox>
                                </div>
                                <div class="error-message search-msg">
                                    <asp:CustomValidator ID="validateExpirationDateCustomValidator" runat="server" />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="field">
                                <asp:Label ID="statusLabel" runat="server" class="label" Text="Status:" CBID="695"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="statusDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="regionLabel" runat="server" class="label" Text="Region:" CBID="951"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="countryLabel" runat="server" class="label" Text="Country :" CBID="1300"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="brandLabel" runat="server" class="label" Text="Brand:" CBID="952"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList runat="server" ID="optgroup" ViewStateMode="Enabled" ClientIDMode="Static"
                                        multiple="multiple">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="isCheckAll" runat="server" ClientIDMode="Static" />
                                    <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display: none;"></asp:TextBox>
                                </div>
                            </div>
                            <div class="field" style="clear: both;">
                                <asp:Label ID="categoryLabel" runat="server" class="label" Text="Category:" CBID="0"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="categoryDropDown" ViewStateMode="Enabled" runat="server" OnSelectedIndexChanged="CategoryDropDown_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="subcategoryLabel" runat="server" class="label" Text="Sub Category:"
                                    CBID="0"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="subcategoryDropDown" OnSelectedIndexChanged="SubCategoryDropDown_SelectedIndexChanged"
                                        AutoPostBack="true" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="tertiarycategoryLabel" runat="server" class="label" Text="Sub Category:"
                                    CBID="0"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="tertiarycategoryDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="SAMPartnerLabel" runat="server" class="label" Text="SAM Partner:"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="SAMPartnerDropDown" ViewStateMode="Enabled" runat="server">
                                        <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="partnershipType" runat="server" class="label" Text="Partner Type:"
                                    CBID="0"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="partnershipTypeDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <div class="alR searchpad-top">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="goLinkButton" Text="Go" runat="server" CBID="402" OnClick="GoButton_click" />
                                    </span>
                                </div>
                            </div>
                            <div class="clear">
                                &nbsp;
                            </div>
                        </div>
                        <div class="clear">
                            &nbsp;
                        </div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                         <div id="ErrorDelPartner" class="message error"  >
                         Please select partner(s) to delete.
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <asp:LinkButton ID="downloadPartnerReportLinkButton" CBID="0" runat="server" CssClass="btn"
                        OnClick="DownloadSpreadSheet_ClickLinkButton" Text="Export to Excel"></asp:LinkButton>
                     <asp:LinkButton ID="downloadPartnerRptGettingDeleted" CBID="0" runat="server" CssClass="btn"
                        OnClick="downloadPartnerRptGettingDeleted_Clicked" OnClientClick="return NewExportToExcel_Clicked();" Text="Export to Excel" ></asp:LinkButton>
                     <asp:LinkButton ID="DeletePartnerLnk" runat="server" 
                        CssClass="btn" Text="Delete Selected Partners" OnClientClick="if ( ! ShowDialog()) return false;" ></asp:LinkButton>
                    <asp:LinkButton ID="addPartnerLinkButton" runat="server" OnClick="AddPartnerLinkButton_Click"
                        CssClass="btn" Text="Add Partner"></asp:LinkButton>
                    <h2>
                        <asp:Label ID="partnerHeadingLabel" runat="server" class="label" Text="Partners"
                            CBID="954"></asp:Label></h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <asp:GridView ID="partnerListGridView" runat="server" ViewStateMode="Enabled" AllowSorting="true"
                                AutoGenerateColumns="False" class="col-6" OnRowCommand="PartnerListGridView_RowCommand"
                                OnRowCreated="PartnerListGridView_RowCreated" OnRowDataBound="PartnerListGridView_RowDataBound"
                                OnSorting="PartnerListGridView_Sorting">
                                <Columns>
                                    <asp:BoundField DataField="CompanyName" SortExpression="CompanyName" HeaderStyle-Width="20%" />
                                    <asp:BoundField DataField="PrimaryContactName" SortExpression="PrimaryContactName"
                                        HeaderStyle-Width="20%" />
                                    <asp:BoundField DataField="PartnershipTypeDesc" SortExpression="PartnershipTypeDesc"
                                        HeaderStyle-Width="14%" />
                                    <asp:BoundField DataField="PartnershipReqSendDate" HeaderStyle-Width="15%" SortExpression="PartnershipRequestDate"
                                        DataFormatString="{0:dd-MMM-yyyy}" />
                                    <asp:BoundField DataField="PartnershipExpirationDate" DataFormatString="{0:dd-MMM-yyyy}"
                                        HeaderStyle-Width="15%" SortExpression="PartnershipExpirationDate" />
                                    <asp:BoundField DataField="PartnershipStatusDesc" SortExpression="PartnershipStatusDesc"
                                        HeaderStyle-Width="15%" />
                                    <asp:TemplateField ShowHeader="False" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="editLinkButton" runat="server" CommandName="EditPartner" CommandArgument='<%# Bind("PartnerId") %>'
                                                Text="edit" CBID="776"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   <asp:TemplateField ShowHeader="False" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                        <asp:CheckBox ID="chkDelPartner" runat="server" Visible="false"  />
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                    <asp:HiddenField runat="server" ID="hidDelPartnerIds" ClientIDMode="Static" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="downloadPartnerReportLinkButton" />
            <asp:PostBackTrigger ControlID="downloadPartnerRptGettingDeleted" />
            
        </Triggers>
    </asp:UpdatePanel>
            <div id="dialog-confirm" >
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to delete the selected partners? All related data will be permanently deleted.</p>
            </div>
            <div  id="PartnerDelSuccessMsg" >
                 <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Selected Partners have been deleted.</p>
            </div>
</asp:Content>
