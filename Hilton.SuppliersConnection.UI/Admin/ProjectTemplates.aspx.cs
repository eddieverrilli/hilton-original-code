﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;
using System.Web.Services;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ProjectTemplates : PageBase
    {
        private IProjectTemplateManager _projectTemplateManager;

        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedBrands_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                var brands = sender as TextBox;
                brandCollection.Value = ProcessDropDownItemChange(DropDownConstants.BrandDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                templateSearch.PageIndex = pagingControl.CurrentPageIndex;
                templateSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                templateSearch.AllClick = pagingControl.AllPageClick;
                SearchTemplateList(templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                templateSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchTemplateList(templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when go button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GoButton_Click(object sender, EventArgs args)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(selectedBrands.Text))
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", "Please select brand");
                }
                else
                {
                    TemplateSearch templateSearch = PopulateSearchCriteria();
                    templateSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                    templateSearch.SortExpression = UIConstants.BrandName;
                    templateSearch.SortDirection = UIConstants.AscAbbreviation;
                    SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                    SetViewState(ViewStateConstants.SortExpression, UIConstants.BrandName);
                    SearchTemplateList(templateSearch);

                    //Pagination Control Setting
                    pagingControl.CurrentPageIndex = 0;
                    pagingControl.PageSet = 0;
                    pagingControl.AllPageClick = false;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Perform the quick search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void QuickSearch_Click(object sender, EventArgs args)
        {
            try
            {
                TemplateSearch templateSearch = PopulateSearchCriteria();
                templateSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                SearchTemplateList(templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Set the project list grid header text
        /// </summary>
        private void SetGridViewHeaderText()
        {
            projectListGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(259, culture, "Brand");
          //  projectListGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(859, culture, "Property Type");
            projectListGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(596, culture, "Region");
            projectListGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(307, culture, "Country");
            projectListGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(462, culture, "# Categories");
            projectListGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(470, culture, "# Projects");
            projectListGridView.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(691, culture, "Status");
        }

        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                templateSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchTemplateList(templateSearch);
                ClearSession(SessionConstants.TemplateSearch);
                SetSession<TemplateSearch>(SessionConstants.TemplateSearch, templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                templateSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchTemplateList(templateSearch);
                ClearSession(SessionConstants.TemplateSearch);
                SetSession<TemplateSearch>(SessionConstants.TemplateSearch, templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on page initialization
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs args)
        {
            try
            {
                base.OnInit(args);
                ValidateUserAccess((int)MenuEnum.ProjectTemplates);
                _projectTemplateManager = ProjectTemplateManager.Instance;
                SetGridViewHeaderText();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            brands.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(optgroup,  p.DataValueField)));
            base.Render(writer);
        }

        /// <summary>
        /// Bind the data to the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void Page_Load(object sender, EventArgs args)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;

                    if (!Page.IsPostBack)
                    {
                        upateAllLinkButton.Visible = false;

                        PopulateDropDownData();

                        if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("ProjectTemplateDetails.aspx")))
                        {
                            MaintainPageState();
                        }
                        else
                        {
                            ClearSession(SessionConstants.TemplateSearch);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(projectTemplateUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                        ScriptManager.RegisterClientScriptBlock(projectTemplateUpdatePanel, this.GetType(), "getItems", "getItems();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                templateSearch.PageIndex = pagingControl.CurrentPageIndex;
                templateSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                templateSearch.AllClick = pagingControl.AllPageClick;
                SearchTemplateList(templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                templateSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchTemplateList(templateSearch);
                ClearSession(SessionConstants.TemplateSearch);
                SetSession<TemplateSearch>(SessionConstants.TemplateSearch, templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                templateSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchTemplateList(templateSearch);
                ClearSession(SessionConstants.TemplateSearch);
                SetSession<TemplateSearch>(SessionConstants.TemplateSearch, templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProjectListGridView_RowCommand(object sender, GridViewCommandEventArgs args)
        {
            try
            {
                if (args != null && (args.CommandName == "ViewCategories"))
                {
                    TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                    templateSearch.PageSet = pagingControl.PageSet;
                    SetSession<TemplateSearch>(SessionConstants.TemplateSearch, templateSearch);
                    SetSession<string>(SessionConstants.ProjectTemplateId, args.CommandArgument.ToString());
                    Response.Redirect("~/Admin/ProjectTemplateDetails.aspx", true);
                }
                else if (args != null && (args.CommandName == "UpdateTemplate"))
                {
                    Control control = args.CommandSource as Control;

                    if (control != null)
                    {
                        IList<ProjectTemplate> projectCollection = new List<ProjectTemplate>();

                        GridViewRow _currenrtRow = control.Parent.NamingContainer as GridViewRow;

                        string oldTemplateStatusId = projectListGridView.DataKeys[_currenrtRow.RowIndex].Values[1].ToString();
                        DropDownList statusDropDownList = _currenrtRow.FindControl("projectTemplateStatusDropDown") as DropDownList;

                        string newTemplateStatusId = statusDropDownList.SelectedValue;
                        if (oldTemplateStatusId != newTemplateStatusId)
                        {
                            projectCollection.Add(new ProjectTemplate()
                            {
                                TemplateId = Convert.ToInt32(args.CommandArgument.ToString(), CultureInfo.InvariantCulture),
                                StatusId = Convert.ToInt32(newTemplateStatusId, CultureInfo.InvariantCulture)
                            });
                        }

                        UpdateTemplateStatus(projectCollection);
                    }
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProjectListGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProjectListGridView_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton viewCategoriesLinkButton = args.Row.FindControl("viewCategoriesLinkButton") as LinkButton;
                    if (viewCategoriesLinkButton != null)
                    {
                        viewCategoriesLinkButton.Text = ResourceUtility.GetLocalizedString(739, culture, "view categories").ReplaceEmptyString();
                        ScriptManager.GetCurrent(this).RegisterPostBackControl(viewCategoriesLinkButton);
                    }

                    DropDownList templatStatusDropDown = args.Row.FindControl("projectTemplateStatusDropDown") as DropDownList;
                    PopulateStatusDropDownData(templatStatusDropDown);
                    templatStatusDropDown.SelectedValue = ((ProjectTemplate)args.Row.DataItem).StatusId.ToString(CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProjectListGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


        /// <summary>
        /// Refresh the drop down lists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                countryDropDown.SelectedIndex = -1; 
                brandCollection.Value = ProcessDropDownItemChangeforCountry(DropDownConstants.RegionDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown, countryDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Update the template status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void UpdateAllLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                IList<ProjectTemplate> projectCollection = new List<ProjectTemplate>();

                foreach (GridViewRow row in projectListGridView.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        string tenplateId = projectListGridView.DataKeys[row.RowIndex].Values[0].ToString();
                        string oldTemplateStatusId = projectListGridView.DataKeys[row.RowIndex].Values[1].ToString();
                        DropDownList statusDropDownList = row.FindControl("projectTemplateStatusDropDown") as DropDownList;

                        string newTemplateStatusId = statusDropDownList.SelectedValue;
                        if (oldTemplateStatusId != newTemplateStatusId)
                        {
                            projectCollection.Add(new ProjectTemplate()
                            {
                                TemplateId = Convert.ToInt32(tenplateId, CultureInfo.InvariantCulture),
                                StatusId = Convert.ToInt32(newTemplateStatusId, CultureInfo.InvariantCulture)
                            });
                        }
                    }
                }
                UpdateTemplateStatus(projectCollection);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  This method will bind the content block
        /// </summary>
        /// <param name="contentblocks"></param>
        private void BindTemplateList(IList<ProjectTemplate> projectTemplates)
        {
            projectListGridView.DataSource = projectTemplates;
            projectListGridView.DataBind();
        }

        /// <summary>
        /// This method will return the sort column index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in projectListGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return projectListGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("ProjectTemplates.aspx") || Request.UrlReferrer.ToString().Contains("ProjectTemplateDetails.aspx")))
                    {
                        TemplateSearch tenplateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                        if (field.SortExpression == tenplateSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, tenplateSearch.SortExpression);
                            if (tenplateSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return projectListGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// This method will maintain page state when user redirect from content block details screen
        /// </summary>
        private void MaintainPageState()
        {
            TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);

            if (templateSearch != null)
            {
                PolulateSearchCriteria(templateSearch);

                pagingControl.PageSet = templateSearch.PageSet;

                SearchTemplateList(templateSearch);
            }
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        private void PolulateSearchCriteria(TemplateSearch templateSearch)
        {
            selectedBrands.Text = templateSearch.BrandId;
            brandCollection.Value = templateSearch.BrandCollection;
            if (!string.IsNullOrEmpty(templateSearch.RegionId))
            {
                regionDropDown.SelectedValue = templateSearch.RegionId;
                ProcessDropDownItemChange(DropDownConstants.RegionDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(templateSearch.BrandId))
            {
                ProcessDropDownItemChange(DropDownConstants.BrandDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(templateSearch.PropertyTypeId))
            {
                propertyTypeDropDown.SelectedValue = templateSearch.PropertyTypeId;
                ProcessDropDownItemChange(DropDownConstants.PropertyTypesDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(templateSearch.StatusId))
                statusDropDown.SelectedValue = templateSearch.StatusId;

            if (!string.IsNullOrEmpty(templateSearch.QuickSearchExpression))
                searchTextBox.Text = templateSearch.QuickSearchExpression;
        }

        /// <summary>
        /// Populate the drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            IList<SearchFilters> searchFilters = _projectTemplateManager.GetProjectTemplateSearchFilterItems(currentUser.UserId);
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilters);
            var brandId = Helper.GetSearchFilterDropDownData(DropDownConstants.BrandDropDown, searchFilters).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false);
            brandCollection.Value = string.Join(",", brandId.ToList().Select(p => p.DataValueField));
            BindDropDown(searchFilters, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(1194, culture, "All Types"), null);
            BindDropDown(searchFilters, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1200, culture, "All Regions"), null);
            BindDropDown(searchFilters, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1301, culture, "All Countries"), null);
            BindStaticDropDown(DropDownConstants.ProjectTemplateStatusDropDown, statusDropDown, ResourceUtility.GetLocalizedString(1225, culture, "All Status"), null);
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        /// <returns></returns>
        private TemplateSearch PopulateSearchCriteria()
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            TemplateSearch templateSearch = new TemplateSearch()
            {
                UserId = currentUser.UserId,
                QuickSearchExpression = searchTextBox.Text.Trim(),
                RegionId = (regionDropDown.SelectedIndex == 0) ? string.Empty : regionDropDown.SelectedValue,
                CountryId = (countryDropDown.SelectedIndex == 0) ? string.Empty : countryDropDown.SelectedValue,
                BrandId = selectedBrands.Text,
                BrandCollection = brandCollection.Value,
                PropertyTypeId = (propertyTypeDropDown.SelectedIndex == 0) ? string.Empty : propertyTypeDropDown.SelectedValue,
                StatusId = (statusDropDown.SelectedIndex == 0) ? string.Empty : statusDropDown.SelectedValue,
                PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture),
                PageIndex = 0,
            };
            return templateSearch;
        }

        /// <summary>
        /// Populate the status drop down
        /// </summary>
        /// <param name="statusDropDownList"></param>
        private void PopulateStatusDropDownData(DropDownList statusDropDownList)
        {
            // Populate Section dropdown list
            statusDropDownList.DataSource = GetStaticDropDownDataSource(DropDownConstants.ProjectTemplateStatusDropDown);
            statusDropDownList.DataTextField = "DataTextField";
            statusDropDownList.DataValueField = "DataValueField";
            statusDropDownList.DataBind();
        }

        /// <summary>
        /// This method will search the content block based on specified criteria
        /// </summary>
        /// <param name="contentSearch"></param>
        private void SearchTemplateList(TemplateSearch templateSearch)
        {
            int totalRecordCount = 0;
            if (templateSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                templateSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                templateSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (templateSearch.PageIndex == -1)
                {
                    templateSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = templateSearch.PageIndex;
            }

            IList<ProjectTemplate> templateList = _projectTemplateManager.GetProjectTemplateList(templateSearch, ref totalRecordCount);

            if (templateList.Count == 0)
            {
                ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoProjectTemplateFoundForCriteria, culture));
                upateAllLinkButton.Visible = false;
                projectListGridView.Visible = false;
            }
            else
            {
                upateAllLinkButton.Visible = false;// setting it to false. Can be set to true for update all functionality as future requirement
                projectListGridView.Visible = true;
            }

            templateSearch.TotalRecordCount = totalRecordCount;

            //Save criteria in session for back button functionality
            ClearSession(SessionConstants.TemplateSearch);
            SetSession<TemplateSearch>(SessionConstants.TemplateSearch, templateSearch);

            BindTemplateList(templateList);

            //Pagination Control Setting
            pagingControl.TotalRecordCount = totalRecordCount;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
            templateSearch.SortExpression = sortExpression;
            templateSearch.SortDirection = sortDirection;
            templateSearch.PageIndex = 0;
            SearchTemplateList(templateSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }

        /// <summary>
        /// Update the template status
        /// </summary>
        /// <param name="projectCollection"></param>
        private void UpdateTemplateStatus(IList<ProjectTemplate> projectCollection)
        {
            if (projectCollection.Count > 0)
            {
                int result = _projectTemplateManager.UpdateProjectTemplate(projectCollection);
                if (result == 0)
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralUpdateFailure, culture));
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.ProjectTemplateSuccessfullyUpdated, culture));
                    TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateSearch);
                    SearchTemplateList(templateSearch);
                }
            }
            else
            {
                ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.ProjectTemplateStatusNotModified, culture));
            }
        }

        //protected void copyTemplateLinkButton_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("CopyBrandTemplate.aspx", true);
        //}       
    }
}