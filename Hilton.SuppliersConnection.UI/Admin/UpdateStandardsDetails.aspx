﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="UpdateStandardsDetails.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.UpdateStandard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" runat="server">
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../../Images/search-top-curve.gif" alt="" /></div>
            <div class="middle">
                <div class="search-section">
                    <asp:Button ID="btnButton" runat="server" Text="" class="search-btn" />
                    <div class="textbox">
                        <asp:TextBox ID="searchTextBox" runat="server"></asp:TextBox>
                    </div>
                </div>
                <asp:HyperLink ID="BackToStandardsHyperLink" NavigateUrl="~/Admin/Standards.aspx"
                    runat="server">‹‹ Back to Standards</asp:HyperLink>
                <div class="clear">
                </div>
            </div>
            <div class="bottom-curve">
                <img src="../../Images/search-bottom-curve.gif" alt="" /></div>
        </div>
    </div>
    <div class="content">
        <div class="wrapper">
            <h2>
                Update Standard</h2>
            <div id="update-standard" class="inner-content">
                <div class="lft-col">
                    <div class="box1">
                        <h3>
                            <span>Standard Information</span></h3>
                        <div class="row">
                            <div class="label">
                                Date Updated:</div>
                            <div class="input">
                                <asp:Label ID="DateUpdatedLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                Brand:</div>
                            <div class="input">
                                <asp:Label ID="BrandLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                Region:</div>
                            <div class="input">
                                <asp:Label ID="RegionLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                Standard #:</div>
                            <div class="input">
                                <asp:Label ID="StandardLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                Category:</div>
                            <div class="input">
                                <asp:Label ID="CategoryLabel" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                New Publish Date:</div>
                            <div class="input">
                                <asp:Label ID="NewPublishDateLabel" runat="server"></asp:Label></div>
                        </div>
                    </div>
                    <div class="box box2">
                        <h3>
                            <span>Previous Standard</span></h3>
                        <div class="box-content">
                            <div class="scroll">
                                <div>
                                    <p>
                                        <asp:Label runat="server" ID="PreviousStandardLabel"></asp:Label></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rgt-col">
                    <div class="box1 section1">
                        <div class="row">
                            <div class="label">
                                Status:</div>
                            <div class="input no-pad select2">
                                <asp:DropDownList runat="server" ID="StatusDropDownList">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="box box2">
                        <h3>
                            <span>Affected Partners</span></h3>
                        <div class="box-content">
                            <div class="scroll">
                                <asp:GridView ID="AffectedPartnersGridView" runat="server" Width="100%" AutoGenerateColumns="false"
                                    CssClass="col-3">
                                    <Columns>
                                        <asp:BoundField DataField="AffectedPartners" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                Partners Notified:</div>
                            <div class="input">
                                <asp:Label ID="PartnersNotifiedLabel" runat="server"></asp:Label>
                                <div class="send-notification">
                                    <asp:HyperLink ID="SendNotificationHyperlink" runat="server"></asp:HyperLink>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box2">
                        <h3>
                            <span>New Standard</span></h3>
                        <div class="box-content">
                            <div class="scroll">
                                <div>
                                    <p>
                                        <asp:Label ID="NewStandardLabel" runat="server"></asp:Label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>