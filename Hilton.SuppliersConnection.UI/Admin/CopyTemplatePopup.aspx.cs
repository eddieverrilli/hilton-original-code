﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Admin
{
    public partial class CopyTemplatePopup : PageBase
    {
        private IProjectTemplateManager _projectTemplateManager;
        private int projectTemplateId;

        /// <summary>
        /// Get the instance of the manager class
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _projectTemplateManager = ProjectTemplateManager.Instance;
            projectTemplateId = Convert.ToInt32(Request.QueryString["pt"], CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// event when page is loaded after a server hit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    if (!IsPostBack)
                    {
                        PopulateBrandRegionTypeList();
                    }
                }
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// binds the data to region drop down
        /// </summary>
        /// <param name="userId"></param>
        private void PopulateBrandRegionTypeList()
        {
            try
            {
                User currentUser = GetSession<User>(SessionConstants.User);
                IList<BrandRegionType> regionCollection = new List<BrandRegionType>();
                regionCollection = _projectTemplateManager.GetBrandRegionTypeList(currentUser.UserId, projectTemplateId);
                regionSelectionDropDown.Items.Clear();
                regionSelectionDropDown.DataSource = regionCollection;
                regionSelectionDropDown.DataTextField = "BrandRegionPropertyTypeDesc";
                regionSelectionDropDown.DataValueField = "BrandRegionPropertyTypeId";
                regionSelectionDropDown.DataBind();
                regionSelectionDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1250, culture, "Please Select"), "-1"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// event for region drop down selection changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionSelectionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (regionSelectionDropDown.SelectedIndex != 0)
                {
                    string selectedValues = regionSelectionDropDown.SelectedValue;
                    SetViewState(ViewStateConstants.SelectedValues, (object)selectedValues);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///saves the changes . Copies the source to target template
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveCopyTemplateLinkButton_Click(object sender, EventArgs e)
        {
            string[] selectedValues = (GetViewState<string>(ViewStateConstants.SelectedValues)).ToString().Split(',');
            var destBrandId = Convert.ToInt32(selectedValues[0], CultureInfo.InvariantCulture);
            var destRegionId = Convert.ToInt32(selectedValues[1], CultureInfo.InvariantCulture);
            var destCountryId = Convert.ToInt32(selectedValues[2], CultureInfo.InvariantCulture);
            //var destPropertyTypeId= Convert.ToInt32(selectedValues[3], CultureInfo.InvariantCulture);
            ProjectTemplate sourceTemplate, destinationTemplate;
            sourceTemplate = new ProjectTemplate();
            destinationTemplate = new ProjectTemplate();
            sourceTemplate.TemplateId = projectTemplateId;
            sourceTemplate.UserId = GetLoggedinUserId();
            destinationTemplate.RegionId = destRegionId;
            destinationTemplate.BrandId = destBrandId;
            destinationTemplate.PropertyTypeId = 1;   //--- Propertytyperemoved ---destPropertyTypeId;
            destinationTemplate.CountryId = destCountryId;
            
            bool copyCategoriesWithProducts = false;
            bool isDataCopied;

            try
            {
                if ((!(categoriesPartnerProductsRadioButton.Checked)) && (categoriesRadioButton.Checked))
                {
                    copyCategoriesWithProducts = false;
                }
                else if ((categoriesPartnerProductsRadioButton.Checked) && (!(categoriesRadioButton.Checked)))
                {
                    copyCategoriesWithProducts = true;
                }

                isDataCopied = _projectTemplateManager.CopyTemplate(sourceTemplate, destinationTemplate, copyCategoriesWithProducts);
                if (isDataCopied)
                {
                    SetSession<string>(SessionConstants.TemplateOperationResult, ConfigurationStore.GetApplicationMessages(MessageConstants.TemplateSuccessfullyCopied, culture));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                if (rethrow)
                {
                    throw exception;
                }
            }
        }
    }
}