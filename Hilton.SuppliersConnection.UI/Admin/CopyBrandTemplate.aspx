﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CopyBrandTemplate.aspx.cs"
    ViewStateMode="Enabled" MasterPageFile="~/MasterPages/AdminMaster.master" Inherits="Hilton.SuppliersConnection.UI.CopyBrandTemplate" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server" ViewStateMode="Enabled">
    <script src="../Scripts/jquery-ui_popup.js" type="text/javascript"></script>
    <style type="text/css">
        .ui-dialog-buttonpane.ui-widget-content
        {
            border: none;
            text-align: center;
            margin: 0 auto;
        }
        .ui-dialog-buttonset button
        {
            background: url("../Images/buttons.gif") repeat-x scroll 0 -76px transparent !important;
            border: 1px solid #CECECE !important;
            color: #827672 !important;
            margin-left: 10px;
            margin-top: 0;
            padding: 5px 15px !important;
            text-transform: uppercase;
            border-radius: .3em;
            box-shadow: 0 1px 2px rgba(0,0,0,0.2);
            line-height: 16px;
            cursor: pointer;
            width: 80px !important;
        }
        .ui-dialog
        {
            z-index: 200;
            width: 465px !important;
            position: fixed;
            padding: 0 0 5px 0;
            border: none;
            border-radius: 0;
        }
        .ui-icon-alert
        {
            display: none;
        }
        .ui-dialog-content.ui-widget-content
        {
            display: block;
            width: auto;
            min-height: 50px !important; /* max-height: none; */ /* height: 0px; */
            border: none;
            height: 50px;
            padding: 20px;
        }
        .ui-widget-overlay
        {
            z-index: 99;
            background: #000;
            position: fixed;
        }
        .ui-dialog-titlebar .ui-button-text
        {
            display: none;
        }
        .ui-widget-header button
        {
            width: 29px !important;
            float: right;
            margin-bottom: 0;
            background: url(../Images/cross-img.gif) no-repeat 100% 0 !important;
            border: none !important;
            height: 20px;
            margin-top: 6px;
        }
        .ui-dialog-titlebar-close
        {
            visibility: hidden;
        }
        .ui-icon-closethick
        {
            visibility: hidden;
        }
        
        .content h3
        {
            /* background: #462f30; */
            color: #462f30;
            font-size: 14px;
            padding: 10px 15px;
        }
        
        .buttondiv
        {
            text-align: right;
            padding-bottom: 8px;
            padding-right: 10px;
            background-color: rgb(209, 209, 209);
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_"]').live("change", function () {
                var Checked = this.checked;
                if ($(this).attr("id") == "cphDetails_categoryDivisionsRepeater_AllCatCheck") {
                    $('input[type=checkbox]').each(function () { this.checked = Checked });
                    return;
                }
                $("span[ParentCatId='" + $(this).parent().attr("SelfID").replace("ID_", "") + "']").each(function () {
                    $(this).find("input:checkbox:eq(0)")[0].checked = Checked;
                }
                                    );
                $("span[RootParentCatId='" + $(this).parent().attr("SelfID").replace("ID_", "") + "']").each(function () {
                    $(this).find("input:checkbox:eq(0)")[0].checked = Checked;
                }
                                    );

                if (Checked == false) {
                    $('#cphDetails_categoryDivisionsRepeater_AllCatCheck')[0].checked = false;
                }

                try {

                    if (Checked == true) {
                        $("span[SelfID=ID_" + $(this).parent().attr("ParentCatId")).find("input:checkbox:eq(0)")[0].checked = this.checked;
                        $("span[SelfID=ID_" + $(this).parent().attr("RootParentCatId")).find("input:checkbox:eq(0)")[0].checked = this.checked
                    }
                }

                catch (e) { }

                //alert("requiredChkClicked");
                var countAllrequiredChk = 0;
                var countCheckedrequiredChk = 0;
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_activeCheck_"]').each(function () {
                    countAllrequiredChk = countAllrequiredChk + 1;
                    if ($(this).is(":checked")) {
                        countCheckedrequiredChk = countCheckedrequiredChk + 1;
                    }
                });

                if (countAllrequiredChk == countCheckedrequiredChk) {

                    $("#cphDetails_categoryDivisionsRepeater_AllCatCheck").prop('checked', true);
                }
                else {
                    $("#cphDetails_categoryDivisionsRepeater_AllCatCheck").prop('checked', false);
                }


            });
        });


        function ToggleCategory(str) {
            if ($("." + str).css('display') == 'none') {
                if ($.browser.mozilla == true && $("." + str).hasClass("cat-level")) {
                    $("." + str).css('display', 'inline-block');
                    $("." + str).css('left-margin', '60px');
                } else {
                    $("." + str).css('display', 'block');
                }
            } else { $("." + str).css('display', 'none'); }
        }

        $("[id*=chkHeader]").live("click", function () {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function () {
                if (chkHeader.is(":checked")) {
                    $(this).attr("checked", "checked");
                    $("td", $(this).closest("tr")).addClass("selected");
                } else {
                    $(this).removeAttr("checked");
                    $("td", $(this).closest("tr")).removeClass("selected");
                }
            });
        });
        $("[id*=chkRow]").live("click", function () {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            if (!$(this).is(":checked")) {
                $("td", $(this).closest("tr")).removeClass("selected");
                chkHeader.removeAttr("checked");
            } else {
                $("td", $(this).closest("tr")).addClass("selected");
                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });



        function ShowDialog() {
            $mydialog = $("#dialog-confirm").dialog({
                resizable: false,
                height: 140,
                modal: true,
                closeOnEscape: false

            });
        }

        function GetCheckedCategories() {
            var CheckedIds = '';
            $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_activeCheck_"]').each(function () {
                if ($(this).is(":checked")) {
                    CheckedIds = CheckedIds + this.id.split('_')[3] + ',';
                }
            });
            $('#hidCheckedIDs').val(CheckedIds);
            if (CheckedIds == '') {
                alert('Please select at least one Category to copy');
                return false;
            }
            else {
                return true;
            }
        }

        function ValidateCheckedTemplates() {
            var ischecked = false;
            $('input:checkbox[id^="cphDetails_TemplateListGridView_chkRow"]').each(function () {
                if ($(this).is(":checked")) {
                    ischecked = true;
                    return false;
                }
            });
            if (ischecked) {
                return true;
            }
            else {
                alert('Please select at least one template to copy');
                return false;
            }
        } 

        


    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" runat="server" ViewStateMode="Enabled">
    <asp:ScriptManager ID="projectTemplateScriptManager" runat="server" EnablePageMethods="true"
        AsyncPostBackTimeout="6000">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="projectTemplateUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="middle">
                        <asp:HyperLink ID="backToProjectTemplateHyperLink" runat="server" NavigateUrl="~/Admin/ProjectTemplates.aspx">‹‹ Back to Project Templates</asp:HyperLink>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="content">
                <div class="wrapper">
                    <div id="resultMessageDiv" runat="server">
                    </div>
                </div>
            </div>
            <asp:MultiView ID="mvtemplate" runat="server">
                <asp:View ID="vwBrandSelection" runat="server">
                    <div class="content">
                        <div class="wrapper">
                            <h2>
                                <asp:Label ID="categoriesHeadingLabel" runat="server" class="label" Text="Copy Brand Template"></asp:Label></h2>
                            <div class="search">
                                <div class="fieldset toggle-content">
                                    <div class="field" style="margin-left: 50px">
                                        <asp:Label ID="lblBrandCopyFrom" runat="server" class="label" Text="Select a Brand to Copy From:"></asp:Label>
                                        <div class="textbox-2">
                                            <asp:DropDownList runat="server" ID="drpBrandCopyFrom" AutoPostBack="true" OnSelectedIndexChanged="drpBrandCopyFrom_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="margin-left: 150px">
                                            <asp:RequiredFieldValidator ID="brandRequiredFieldValidator" SetFocusOnError="true"
                                                VMTI="0" runat="server" ErrorMessage="Please select a brand to Copy From" ValidationGroup="BrandContinue"
                                                ControlToValidate="drpBrandCopyFrom" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="field" style="margin-left: 50px">
                                        <asp:Label ID="lblBrandCopyTo" runat="server" class="label" Text="Select a Brand to Copy To:"></asp:Label>
                                        <div class="textbox-2">
                                            <asp:DropDownList runat="server" ID="drpBrandCopyTo">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="margin-left: 135px">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" VMTI="0"
                                                runat="server" ErrorMessage="Please select a brand to Copy To" ValidationGroup="BrandContinue"
                                                ControlToValidate="drpBrandCopyTo" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="field" style="margin-left: 50px">
                                        <span class="btn-style">
                                            <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                        </span>
                                    </div>
                                    <div class="field" style="margin-left: 10px">
                                        <span class="btn-style">
                                            <asp:LinkButton ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click"
                                                ValidationGroup="BrandContinue" />
                                        </span>
                                    </div>
                                    <div class="clear">
                                        &nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="vwtemplates" runat="server">
                    <div id="resultContentDiv" class="content">
                        <div class="wrapper">
                            <div id="resultMessageTemplateDiv" runat="server">
                            </div>
                            <h2>
                                <asp:Label ID="projectTemplateHeadingLabel" runat="server" class="label" Text="Select Region(s) and Country(s) to Copy"></asp:Label></h2>
                            <div class="inner-content">
                                <div class="no-space">
                                    <asp:GridView ID="TemplateListGridView" runat="server" ViewStateMode="Enabled" CssClass="col-6"
                                        AutoGenerateColumns="False" DataKeyNames="TemplateId,RegionId,CountryId" class="col-6">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="12%">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkHeader" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkRow" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="BrandName" HeaderText="Brand" HeaderStyle-Width="25%" />
                                            <asp:BoundField DataField="RegionDescription" HeaderText="Region" HeaderStyle-Width="25%"
                                                ConvertEmptyStringToNull="true" HtmlEncode="false" />
                                            <asp:BoundField DataField="CountryName" HeaderText="Country" HeaderStyle-Width="25%"
                                                ConvertEmptyStringToNull="true" HtmlEncode="false" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="field">
                                <div class="buttondiv">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="btnCancelTemplate" runat="server" Text="Cancel" OnClick="btnCancelTemplate_Click" />
                                    </span><span class="btn-style">
                                        <asp:LinkButton ID="btnContinueTemplate" runat="server" Text="Continue" OnClientClick="return ValidateCheckedTemplates();"
                                            OnClick="btnContinueTemplate_Click" /></span>
                                    <div class="clear">
                                        &nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="vwCategory" runat="server">
                    <div id="Div1" class="content">
                        <div class="wrapper">
                            <div id="divMessageCategory" runat="server">
                            </div>
                            <h2>
                                <asp:Label ID="Label1" runat="server" class="label" Text="Categories" CBID="278"></asp:Label></h2>
                            <div class="inner-content">
                                <div class="no-space">
                                    <div id="Div2" runat="server" class="categories" style="overflow: hidden">
                                        <asp:Repeater ID="categoryDivisionsRepeater" runat="server" OnItemDataBound="CategoryDivisionsRepeater_ItemDataBound">
                                            <HeaderTemplate>
                                                <div id="categoriesDivision" runat="server" style="width: 963px">
                                                    <table border="0" cellpadding="0" cellspacing="0" class="cat-table cat-table-heading">
                                                        <tr style="background: #f2f2f2; color: #878787; font-size: 12px; font-weight: 400;
                                                            vertical-align: top">
                                                            <td class="col-1">
                                                            </td>
                                                            <td class="col-2">
                                                                <asp:CheckBox ID="AllCatCheck" CssClass="checkbox" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div id="categoryListDivision" runat="server">
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:HiddenField ID="hidCheckedIDs" runat="server" ClientIDMode="Static" />
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="buttondiv">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="lnkCancelCategory" runat="server" Text="Cancel" OnClick="lnkCancelCategory_Click" />
                                    </span><span class="btn-style">
                                        <asp:LinkButton ID="lnkCopyTemplete" runat="server" Text="Continue" OnClientClick="return GetCheckedCategories();"
                                            OnClick="lnkCopyTemplete_Click" />
                                    </span>
                                    <div class="clear">
                                        &nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="vwCopyTemplatePreview" runat="server">
                    <div id="Div3" class="content">
                        <div class="wrapper">
                            <h3>
                                Are you sure you want to copy the following Categories, Regions, and Countries from
                                <asp:Label ID="lblCopyFromBrand" runat="server" class="label"></asp:Label>
                                to
                                <asp:Label ID="lblCopyToBrand" runat="server" class="label"></asp:Label>?
                            </h3>
                            <div class="lft-col" style="padding-top: 30px">
                                <asp:GridView ID="grdCategoryPreview" runat="server" ViewStateMode="Enabled" CssClass="col-6"
                                    AutoGenerateColumns="False" DataKeyNames="CategoryId" AllowSorting="true" class="col-6">
                                    <Columns>
                                        <asp:BoundField DataField="ParentCategoryHierarchy" HeaderText="Category(s)" HeaderStyle-Width="25%" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="rgt-col" style="padding-top: 30px">
                                <asp:GridView ID="grdRegionPeview" runat="server" ViewStateMode="Enabled" CssClass="col-6"
                                    AutoGenerateColumns="False" DataKeyNames="TemplateId" AllowSorting="true" class="col-6">
                                    <Columns>
                                        <asp:BoundField DataField="RegionDescription" HeaderText="Region" HeaderStyle-Width="25%" />
                                        <asp:BoundField DataField="CountryName" HeaderText="Country" HeaderStyle-Width="25%" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="clear">
                                &nbsp;</div>
                        </div>
                        <div class="wrapper">
                            <div class="alR buttondiv">
                                <span class="btn-style">
                                    <asp:LinkButton ID="lnkPreviewCancel" runat="server" Text="Cancel" OnClick="lnkPreviewCancel_Click" />
                                </span><span class="btn-style">
                                    <asp:LinkButton ID="lnkFinalCopy" runat="server" Text="Copy" OnClick="lnkFinalCopy_Click" />
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="dialog-confirm" style="display: none">
                        <div class="content" style="text-align: center;">
                            <h4 style="text-align: center;">
                                Selected template(s) has been sucessfully copied.</h4>
                            <div style="text-align: center;">
                                <span class="btn-style">
                                    <asp:LinkButton ID="lnkCopyCompleteRedirect" runat="server" Text="Ok" PostBackUrl="~/Admin/ProjectTemplates.aspx"></asp:LinkButton>
                                </span>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
