﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ProductDetails : PageBase
    {
        private IPartnerManager _partnerManager;
        private IProductManager _productManager;

        /// <summary>
        /// To bind the data with Drop Down
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            try
            {
                categoryDropDown.DataSource = categoryList.OrderBy(p => p.CategoryName);
                categoryDropDown.DataTextField = "CategoryName";
                categoryDropDown.DataValueField = "CategoryId";
                categoryDropDown.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// To add specification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddSpecificationLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (selectedRegions.Text == string.Empty)
                {
                    errorMsgLabel.Visible = true;
                    errorMsgLabel.Text = "Please Select atleast one region.";
                    return;
                }
                else if (selectedCountries.Text == string.Empty)
                {
                    errorMsgLabel.Visible = true;
                    errorMsgLabel.Text = "Please Select atleast one country.";
                    return;
                }
                else if (Convert.ToInt32(brandDropDown.SelectedItem.Value, CultureInfo.InvariantCulture) == 0)
                {
                    DropDownList brandDropDownlist = new DropDownList();
                    IList<DropDownItem> brands = GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
                    DropDownList regionDropDownlist = new DropDownList();
                    IList<DropDownItem> regions = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
                    IList<DropDownItem> countries = GetStaticDropDownDataSource(DropDownConstants.CountryDropDown);

                    IList<ProductAssignedSpecifications> productAssignedSpecifications
                                            = new List<ProductAssignedSpecifications>();

                    IList<ProductCategory> productCategoriesList = GetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories);
                    string categoryDisplayName = string.Empty;
                    int leafCategoryIndex = -1;

                    foreach (Control c in categoryPlaceHolder.Controls)
                    {
                        if (c is DropDownList)
                        {
                            leafCategoryIndex++;
                            if (leafCategoryIndex.Equals(categoryPlaceHolder.Controls.Count - 1))
                                categoryDisplayName += ((DropDownList)c).SelectedItem.Text.Trim();
                            else
                                categoryDisplayName += ((DropDownList)c).SelectedItem.Text.Trim() + " :: ";
                        }
                    }

                    foreach (DropDownItem brand in brands)
                    {
                        //case A :: No region selected
                        if (selectedRegions.Text == string.Empty)
                        {
                            //Case 1 Country selected No region selected
                            if (selectedCountries.Text != string.Empty)
                            {
                                errorMsgLabel.Visible = true;
                                errorMsgLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectLeafCategoryErrorMessage, culture);
                                return;
                            }
                            else
                            {
                                foreach (DropDownItem region in regions)
                                {

                                    //Populate Country For each Region
                                    IList<DropDownItem> CountryTypeCollection = new List<DropDownItem>();
                                    CountryTypeCollection = _productManager.GetCountriesForRegion(Convert.ToInt32(region.DataValueField));

                                    foreach (DropDownItem country in CountryTypeCollection)
                                    {
                                        IList<ProductPropertyType> propertyTypeCollection = new List<ProductPropertyType>();
                                        if (((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedIndex == 0)
                                        {
                                            errorMsgLabel.Visible = true;
                                            errorMsgLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectLeafCategoryErrorMessage, culture);
                                            return;
                                        }

                                        IList<ProductCategory> productCategoryTemp = productCategoriesList.Where(item => item.CategoryId.Equals(Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture))).ToList<ProductCategory>();
                                        if (productCategoryTemp.Count > 0)
                                        {
                                            productAssignedSpecifications = productCategoryTemp[0].ProductAssignedSpecifications;
                                            if (productAssignedSpecifications.
                                            Where(i => i.BrandId.Equals(Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture))
                                                   && i.RegionId.Equals(Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture))
                                                    && i.CountryId.Equals(Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture)))
                                                       .Count() == 0)
                                            {
                                                productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                                {
                                                    CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                                    BrandId = Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture),
                                                    BrandDescription = brand.DataTextField,
                                                    RegionId = Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture),
                                                    RegionDescription = region.DataTextField,
                                                    CountryId = Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture),
                                                    CountryDescription = country.DataTextField,

                                                }
                                               );
                                                productCategoryTemp[0].ProductAssignedSpecifications = productAssignedSpecifications;
                                            }
                                            else
                                            {
                                                ProductAssignedSpecifications selectedSpecification =
                                                                    productAssignedSpecifications.
                                                                          Where(i => i.BrandId.Equals(Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture))
                                                                                && i.RegionId.Equals(Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture))
                                                                                  && i.CountryId.Equals(Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture))).First();

                                            }
                                        }
                                        else
                                        {
                                            productAssignedSpecifications = new List<ProductAssignedSpecifications>();
                                            productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                            {
                                                CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                                BrandId = Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture),
                                                BrandDescription = brand.DataTextField,
                                                RegionId = Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture),
                                                RegionDescription = region.DataTextField,
                                                CountryId = Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture),
                                                CountryDescription = country.DataTextField,

                                            });

                                            productCategoriesList.Add(new ProductCategory()
                                            {
                                                CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                                CategoryName = categoryDisplayName,
                                                ProductAssignedSpecifications = productAssignedSpecifications
                                            }
                                         );
                                        }
                                        SetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories, productCategoriesList);
                                    }
                                }
                            }
                        }
                        else
                        {//Countries will Populate from Dropdown
                            IList<Country> CountryTypeCollection = new List<Country>();
                            string countryarr = selectedCountries.Text;
                            CountryTypeCollection = _productManager.GetCountriesForRegionForProduct(countryarr);
                            foreach (Country country in CountryTypeCollection)
                            {
                                IList<ProductPropertyType> propertyTypeCollection = new List<ProductPropertyType>();
                                if (((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedIndex == 0)
                                {
                                    errorMsgLabel.Visible = true;
                                    errorMsgLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectLeafCategoryErrorMessage);
                                    return;
                                }


                                IList<ProductCategory> productCategoryTemp = productCategoriesList.Where(item => item.CategoryId.Equals(Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture))).ToList<ProductCategory>();
                                if (productCategoryTemp.Count > 0)
                                {
                                    productAssignedSpecifications = productCategoryTemp[0].ProductAssignedSpecifications;
                                    if (productAssignedSpecifications.
                                            Where(i => i.BrandId.Equals(Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture))
                                                   && i.RegionId.Equals(country.RegionId)
                                                   && i.CountryId.Equals(country.CountryId))
                                                       .Count() == 0)
                                    {
                                        productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                        {
                                            CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                            BrandId = Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture),
                                            BrandDescription = brand.DataTextField,
                                            RegionId = country.RegionId,
                                            RegionDescription = country.RegionName,
                                            CountryId = country.CountryId,
                                            CountryDescription = country.CountryName,

                                        });
                                        productCategoryTemp[0].ProductAssignedSpecifications = productAssignedSpecifications;
                                    }
                                    else
                                    {
                                        ProductAssignedSpecifications selectedSpecification =
                                                            productAssignedSpecifications.
                                                                  Where(i => i.BrandId.Equals(Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture))
                                                                        && i.RegionId.Equals(country.RegionId)
                                                                         && i.CountryId.Equals(country.CountryId)).First();

                                    }
                                }
                                else
                                {
                                    productAssignedSpecifications = new List<ProductAssignedSpecifications>();
                                    productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                    {
                                        CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                        BrandId = Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture),
                                        BrandDescription = brand.DataTextField,
                                        RegionId = country.RegionId,
                                        RegionDescription = country.RegionName,
                                        CountryId = country.CountryId,
                                        CountryDescription = country.CountryName,

                                    });

                                    productCategoriesList.Add(new ProductCategory()
                                    {
                                        CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                        CategoryName = categoryDisplayName,
                                        ProductAssignedSpecifications = productAssignedSpecifications
                                    });
                                }
                            }
                        }
                    }
                    SetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories, productCategoriesList);

                    productCategoryRepeater.DataSource = productCategoriesList;
                    productCategoryRepeater.DataBind();
                    specificationUpdatePanel.Update();
                }
                else if (selectedRegions.Text == string.Empty && (Convert.ToInt32(brandDropDown.SelectedItem.Value, CultureInfo.InvariantCulture) != 0))
                {
                    if (selectedCountries.Text != string.Empty)
                    {
                        errorMsgLabel.Visible = true;
                        errorMsgLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectLeafCategoryErrorMessage, culture);
                        return;
                    }
                    else
                    {
                        DropDownList brandDropDownlist = new DropDownList();
                        IList<DropDownItem> brands = GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
                        DropDownList regionDropDownlist = new DropDownList();
                        IList<DropDownItem> regions = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);

                        IList<ProductAssignedSpecifications> productAssignedSpecifications
                                                = new List<ProductAssignedSpecifications>();

                        IList<ProductCategory> productCategoriesList = GetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories);
                        string categoryDisplayName = string.Empty;
                        int leafCategoryIndex = -1;

                        foreach (Control c in categoryPlaceHolder.Controls)
                        {
                            if (c is DropDownList)
                            {
                                leafCategoryIndex++;
                                if (leafCategoryIndex.Equals(categoryPlaceHolder.Controls.Count - 1))
                                    categoryDisplayName += ((DropDownList)c).SelectedItem.Text.Trim();
                                else
                                    categoryDisplayName += ((DropDownList)c).SelectedItem.Text.Trim() + " :: ";
                            }
                        }

                        foreach (DropDownItem region in regions)
                        {
                            IList<DropDownItem> CountryTypeCollection = new List<DropDownItem>();
                            CountryTypeCollection = _productManager.GetCountriesForRegion(Convert.ToInt32(region.DataValueField));
                            //Country
                            foreach (DropDownItem country in CountryTypeCollection)
                            {
                                IList<ProductPropertyType> propertyTypeCollection = new List<ProductPropertyType>();
                                if (((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedIndex == 0)
                                {
                                    errorMsgLabel.Visible = true;
                                    errorMsgLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectLeafCategoryErrorMessage, culture);
                                    return;
                                }


                                IList<ProductCategory> productCategoryTemp = productCategoriesList.Where(item => item.CategoryId.Equals(Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture))).ToList<ProductCategory>();
                                if (productCategoryTemp.Count > 0)
                                {
                                    productAssignedSpecifications = productCategoryTemp[0].ProductAssignedSpecifications;

                                    if (productAssignedSpecifications.
                                            Where(i => i.BrandId.Equals(Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                   && i.RegionId.Equals(Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture))
                                                   && i.CountryId.Equals(Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture)))
                                                       .Count() == 0)
                                    {
                                        productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                        {
                                            CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                            BrandId = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture),
                                            BrandDescription = brandDropDown.SelectedItem.Text,
                                            RegionId = Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture),
                                            RegionDescription = region.DataTextField,
                                            CountryId = Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture),
                                            CountryDescription = country.DataTextField,

                                        }
                                                                             );
                                        productCategoryTemp[0].ProductAssignedSpecifications = productAssignedSpecifications;
                                    }
                                    else
                                    {
                                        ProductAssignedSpecifications selectedSpecification =
                                                            productAssignedSpecifications.
                                                                  Where(i => i.BrandId.Equals(Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                                        && i.RegionId.Equals(Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture))
                                                                         && i.CountryId.Equals(Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture))).First();

                                    }
                                }
                                else
                                {
                                    productAssignedSpecifications = new List<ProductAssignedSpecifications>();

                                    productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                    {
                                        CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                        BrandId = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture),
                                        BrandDescription = brandDropDown.SelectedItem.Text,
                                        RegionId = Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture),
                                        RegionDescription = region.DataTextField,
                                        CountryId = Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture),
                                        CountryDescription = country.DataTextField,

                                    }
                                  );

                                    productCategoriesList.Add(new ProductCategory()
                                    {
                                        CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                        CategoryName = categoryDisplayName,
                                        ProductAssignedSpecifications = productAssignedSpecifications
                                    }
                                                           );
                                }
                            }
                            SetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories, productCategoriesList);

                            productCategoryRepeater.DataSource = productCategoriesList;
                            productCategoryRepeater.DataBind();
                            specificationUpdatePanel.Update();
                        }
                    }
                }
                else
                {
                    //if ((Convert.ToInt32(countryDropDown.SelectedItem.Value, CultureInfo.InvariantCulture) == 0))
                    if (selectedCountries.Text.Contains(","))
                    {
                        IList<ProductAssignedSpecifications> productAssignedSpecifications
                                                = new List<ProductAssignedSpecifications>();

                        IList<ProductCategory> productCategoriesList = GetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories);
                        string categoryDisplayName = string.Empty;
                        int leafCategoryIndex = -1;

                        foreach (Control c in categoryPlaceHolder.Controls)
                        {
                            if (c is DropDownList)
                            {
                                leafCategoryIndex++;
                                if (leafCategoryIndex.Equals(categoryPlaceHolder.Controls.Count - 1))
                                    categoryDisplayName += ((DropDownList)c).SelectedItem.Text.Trim();
                                else
                                    categoryDisplayName += ((DropDownList)c).SelectedItem.Text.Trim() + " :: ";
                            }
                        }
                        IList<Country> CountryTypeCollection = new List<Country>();
                        string countryarr = selectedCountries.Text;
                        CountryTypeCollection = _productManager.GetCountriesForRegionForProduct(countryarr);

                        //Country
                        foreach (Country country in CountryTypeCollection)
                        {

                            IList<ProductPropertyType> propertyTypeCollection = new List<ProductPropertyType>();
                            if (((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedIndex == 0)
                            {
                                errorMsgLabel.Visible = true;
                                errorMsgLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectLeafCategoryErrorMessage, culture);
                                return;
                            }

                            else
                            {
                                IList<ProductCategory> productCategoryTemp = productCategoriesList.Where(item => item.CategoryId.Equals(Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture))).ToList<ProductCategory>();
                                if (productCategoryTemp.Count > 0)
                                {
                                    productAssignedSpecifications = productCategoryTemp[0].ProductAssignedSpecifications;

                                    if (productAssignedSpecifications.
                                            Where(i => i.BrandId.Equals(Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                   && i.RegionId.Equals(Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                    && i.CountryId.Equals(country.CountryId))
                                                       .Count() == 0)
                                    {
                                        productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                        {
                                            CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                            BrandId = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture),
                                            BrandDescription = brandDropDown.SelectedItem.Text,
                                            RegionId = country.RegionId,
                                            RegionDescription = country.RegionName,
                                            CountryId = country.CountryId,
                                            CountryDescription = country.CountryName,

                                        }
                                                                          );
                                        productCategoryTemp[0].ProductAssignedSpecifications = productAssignedSpecifications;
                                    }
                                    else
                                    {
                                        ProductAssignedSpecifications selectedSpecification =
                                                            productAssignedSpecifications.
                                                                  Where(i => i.BrandId.Equals(Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                                        && i.RegionId.Equals(Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                                         && i.CountryId.Equals(country.CountryId)).First();

                                    }
                                }
                                else
                                {
                                    productAssignedSpecifications = new List<ProductAssignedSpecifications>();
                                    productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                    {
                                        CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                        BrandId = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture),
                                        BrandDescription = brandDropDown.SelectedItem.Text,
                                        RegionId = country.RegionId,
                                        RegionDescription = country.RegionName,
                                        CountryId = country.CountryId,
                                        CountryDescription = country.CountryName,

                                    }
                                                            );

                                    productCategoriesList.Add(new ProductCategory()
                                    {
                                        CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                        CategoryName = categoryDisplayName,
                                        ProductAssignedSpecifications = productAssignedSpecifications
                                    });
                                }

                                SetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories, productCategoriesList);

                                productCategoryRepeater.DataSource = productCategoriesList;
                                productCategoryRepeater.DataBind();
                                specificationUpdatePanel.Update();
                            }
                        }


                    }
                    else
                    {
                        if ((Convert.ToInt32(countryDropDown.SelectedItem.Value, CultureInfo.InvariantCulture) != 0))
                        {
                            IList<ProductPropertyType> propertyTypeCollection = new List<ProductPropertyType>();
                            IList<ProductAssignedSpecifications> productAssignedSpecifications
                                                    = new List<ProductAssignedSpecifications>();

                            IList<ProductCategory> productCategoriesList = GetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories);
                            string categoryDisplayName = string.Empty;
                            int leafCategoryIndex = -1;

                            foreach (Control c in categoryPlaceHolder.Controls)
                            {
                                if (c is DropDownList)
                                {
                                    leafCategoryIndex++;
                                    if (leafCategoryIndex.Equals(categoryPlaceHolder.Controls.Count - 1))
                                        categoryDisplayName += ((DropDownList)c).SelectedItem.Text.Trim();
                                    else
                                        categoryDisplayName += ((DropDownList)c).SelectedItem.Text.Trim() + " :: ";
                                }
                            }

                            if (((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedIndex == 0)
                            {
                                errorMsgLabel.Visible = true;
                                errorMsgLabel.Text = ConfigurationStore.GetApplicationMessages(MessageConstants.SelectLeafCategoryErrorMessage, culture);
                                return;
                            }

                            else
                            {


                                IList<ProductCategory> productCategoryTemp = productCategoriesList.Where(item => item.CategoryId.Equals(Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture))).ToList<ProductCategory>();
                                if (productCategoryTemp.Count > 0)
                                {
                                    productAssignedSpecifications = productCategoryTemp[0].ProductAssignedSpecifications;
                                    string countryarr = selectedCountries.Text;
                                    IList<Country> CountryTypeCollection = _productManager.GetCountriesForRegionForProduct(countryarr);

                                    if (productAssignedSpecifications.
                                            Where(i => i.BrandId.Equals(Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                   && i.RegionId.Equals(Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                    && i.CountryId.Equals(Convert.ToInt32(countryDropDown.SelectedValue, CultureInfo.InvariantCulture)))
                                                       .Count() == 0)
                                    {

                                        productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                        {
                                            CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                            BrandId = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture),
                                            BrandDescription = brandDropDown.SelectedItem.Text,
                                            RegionId = CountryTypeCollection.First().RegionId,
                                            RegionDescription = CountryTypeCollection.First().RegionName,
                                            CountryId = CountryTypeCollection.First().CountryId,
                                            CountryDescription = CountryTypeCollection.First().CountryName,

                                        }
                                                                          );
                                        productCategoryTemp[0].ProductAssignedSpecifications = productAssignedSpecifications;
                                    }
                                    else
                                    {
                                        ProductAssignedSpecifications selectedSpecification =
                                                            productAssignedSpecifications.
                                                                  Where(i => i.BrandId.Equals(Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                                        && i.RegionId.Equals(Convert.ToInt32(regionDropDown.SelectedValue, CultureInfo.InvariantCulture))
                                                                         && i.CountryId.Equals(Convert.ToInt32(countryDropDown.SelectedValue, CultureInfo.InvariantCulture))).First();

                                    }
                                }
                                else
                                {
                                    string countryarr = selectedCountries.Text;
                                    IList<Country> CountryTypeCollection = _productManager.GetCountriesForRegionForProduct(countryarr);
                                    productAssignedSpecifications = new List<ProductAssignedSpecifications>();
                                    productAssignedSpecifications.Add(new ProductAssignedSpecifications()
                                    {
                                        CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                        BrandId = Convert.ToInt32(brandDropDown.SelectedValue, CultureInfo.InvariantCulture),
                                        BrandDescription = brandDropDown.SelectedItem.Text,
                                        RegionId = CountryTypeCollection.First().RegionId,
                                        RegionDescription = CountryTypeCollection.First().RegionName,
                                        CountryId = CountryTypeCollection.First().CountryId,
                                        CountryDescription = CountryTypeCollection.First().CountryName,

                                    });

                                    productCategoriesList.Add(new ProductCategory()
                                    {
                                        CategoryId = Convert.ToInt32(((DropDownList)categoryPlaceHolder.Controls[leafCategoryIndex]).SelectedValue, CultureInfo.InvariantCulture),
                                        CategoryName = categoryDisplayName,
                                        ProductAssignedSpecifications = productAssignedSpecifications
                                    }
                                                           );
                                }

                                SetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories, productCategoriesList);

                                productCategoryRepeater.DataSource = productCategoriesList;
                                productCategoryRepeater.DataBind();
                                specificationUpdatePanel.Update();
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


        protected void SelectedRegions_TextChanged(object sender, EventArgs e)
        {
            try
            {
                regionCollection.Value = selectedRegions.Text;
                var regionList = (regionCollection.Value == null || regionCollection.Value == "0") ? null : regionCollection.Value.Split(',').Select(item => int.Parse(item));
                IList<DropDownItem> countries = GetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState) ?? new List<DropDownItem>();
                countries.Clear();
                SetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState, countries);
                var selRegion = selectedRegions == null || string.IsNullOrWhiteSpace(selectedRegions.Text) ? regionList : selectedRegions.Text.Split(',').Select(item => int.Parse(item));
                if (selRegion != null && regionList != null)
                {
                    IList<DropDownItem> countriesForSelectedRegions = new List<DropDownItem>();
                    foreach (var item in regionList.Where(p => selRegion.Contains(p)))
                    {
                        var countryForRegion = _partnerManager.GetCountriesForRegion(item);
                        countryForRegion.ToList().ForEach(p =>
                        {
                            p.ParentDataField = item.ToString();
                            countriesForSelectedRegions.Add(p);
                        });
                    }

                    countryDropDown.DataSource = countriesForSelectedRegions;
                    countryDropDown.DataTextField = "DataTextField";
                    countryDropDown.DataValueField = "DataValueField";

                    //countryDropDown.SelectedIndex = 0;
                    countryDropDown.DataBind();
                    SetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState, countriesForSelectedRegions);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                dropDownUpdatePanel.Update();
            }
        }




        /// <summary>
        /// Grid view Item Data Bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AssignedSpecificationsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RepeaterItem item = e.Item;
                //Repeater repeaterDetails;
                if ((item.ItemType == ListItemType.Item) || (item.ItemType == ListItemType.AlternatingItem))
                {
                    ProductAssignedSpecifications row = (ProductAssignedSpecifications)item.DataItem;
                    ((ImageButton)item.FindControl("deletePropertyImageButton")).CommandArgument = row.CategoryId.ToString(CultureInfo.InvariantCulture) + "," + row.BrandId.ToString(CultureInfo.InvariantCulture) + "," + row.RegionId.ToString(CultureInfo.InvariantCulture) + "," + row.CountryId.ToString(CultureInfo.InvariantCulture);
                }


            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// To create Drop Down List
        /// </summary>
        /// <param name="ID"></param>
        protected void CreateDropDownList(string ID)
        {
            try
            {
                DropDownList parentCategoryDropDown = new DropDownList();
                parentCategoryDropDown.ID = ID;
                parentCategoryDropDown.AutoPostBack = true;
                parentCategoryDropDown.SelectedIndexChanged += new EventHandler(DynamicCategoryDropDown_OnSelectedIndexChanged);

                categoryPlaceHolder.Controls.Add(parentCategoryDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// To Delete Property Image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeletePropertyImageButton_Click(object sender, EventArgs e)
        {
            try
            {
                string[] args = ((ImageButton)sender).CommandArgument.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                int categoryId = int.Parse(args[0], CultureInfo.InvariantCulture);
                int brandId = int.Parse(args[1], CultureInfo.InvariantCulture);
                int regionId = int.Parse(args[2], CultureInfo.InvariantCulture);
                int countryId = int.Parse(args[3], CultureInfo.InvariantCulture);

                IList<ProductCategory> productCategories = GetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories);
                ProductCategory productCategoryItem = (ProductCategory)productCategories.Where(i => i.CategoryId.Equals(categoryId)).First();
                IList<ProductAssignedSpecifications> productAssignedSpecs = productCategoryItem.ProductAssignedSpecifications;
                ProductAssignedSpecifications filteredProductCategory = (ProductAssignedSpecifications)productAssignedSpecs.Where(i => i.BrandId.Equals(brandId)
                                                                        && i.RegionId.Equals(regionId) && i.CountryId.Equals(countryId)).First();



                productAssignedSpecs.Remove(filteredProductCategory);

                if (productAssignedSpecs.Count == 0)
                    productCategories.Remove(productCategoryItem);

                SetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories, productCategories);

                productCategoryRepeater.DataSource = productCategories;
                productCategoryRepeater.DataBind();
                specificationUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Selected index Changed on Dynamic Drop Down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DynamicCategoryDropDown_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                errorMsgLabel.Visible = false;
                DropDownList ddl = (DropDownList)sender;
                string id = ddl.ID;
                int existingDropDownCount = FindOccurence("categoriesDropDown");
                if (!id.Contains("-"))
                {
                    id = id + "-1";
                }
                string[] idSequence = id.Split('-');

                string currentSelectedCategoryId = ddl.SelectedValue.ToString(CultureInfo.InvariantCulture);
                // remove all the below ddls.
                for (int dropdownCount = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1; dropdownCount <= existingDropDownCount + 1; dropdownCount++)
                {
                    DropDownList rowDivision = (DropDownList)categoryPlaceHolder.FindControl("categoriesDropDown-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
                    categoryPlaceHolder.Controls.Remove(rowDivision);
                }
                if (currentSelectedCategoryId != "0")
                {
                    Category currentSelectedCategory = ReadCategory(currentSelectedCategoryId);
                    if (!(currentSelectedCategory.IsLeaf))
                    {
                        IList<Category> childCategoryList = GetChildCategories(currentSelectedCategoryId);
                        if (childCategoryList.Count() > 0)
                        {
                            int nextID = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1;

                            CreateDropDownList("categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
                            FindPopulateDropDowns(currentSelectedCategoryId, "categoriesDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To get product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (GetViewState<string>(UIConstants.Mode) == UIConstants.Edit)
                {
                    Product product = _productManager.GetProductImage((int)GetViewState(UIConstants.ProductListId));
                    ProcessFileDownload("image", product.ProductImage, product.ImageName);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "SpecSheetScript1", "SetFileInputStyles();", true);
        }

        /// <summary>
        /// Invoked on OnInit
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs args)
        {
            try
            {
                base.OnInit(args);
                ValidateUserAccess((int)MenuEnum.Products);

                backtoProductsHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "‹‹ {0}", ResourceUtility.GetLocalizedString(243, culture, "Back to Products"));
                assignedCategoriesPanelLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(971, culture, "Assigned Categories"));
                _productManager = ProductManager.Instance;
                _partnerManager = PartnerManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(1098, culture, "Product Details");
                RecreateControls("categoriesDropDown", "DropDownList");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null && !Page.IsPostBack)
                {
                    //Moved from Init
                    IList<Category> categories = HelperManager.Instance.GetActiveCategories;
                    SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categories);

                    ValidateProductReviewAuthorization();

                    BindRootCategoryDropDown();

                    PopulateDropDownData();

                    SetRegularExpressions();

                    IList<ProductCategory> productCategories = new List<ProductCategory>();
                    SetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories, productCategories);

                    SetPageSettings();

                    IList<ProductCategory> productCategoryList = GetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories);
                    productCategoryRepeater.DataSource = productCategoryList;
                    productCategoryRepeater.DataBind();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(dropDownUpdatePanel, this.GetType(), "SetRegionAndCountryDropDown", "SetRegionAndCountryDropDown();", true);
                }



            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will set the validation expression for regular expression validator based on culture
        /// </summary>
        private void SetRegularExpressions()
        {
            // Regular Expression for Name
            SetValidatorRegularExpressions(firstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(phoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(faxRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
            // Regular Expression for website
            SetValidatorRegularExpressions(websiteRegularExpressionValidator, (int)RegularExpressionTypeEnum.WebsiteValidator);

            // Regular Expression for email
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
        }

        /// <summary>
        /// To Validate Product Review Authorization
        /// </summary>
        private void ValidateProductReviewAuthorization()
        {
            bool productReview = ValidateAdminControlAuthorization((int)AdminPermissionEnum.ProductReview);
            if (!productReview)
            {
                personTextBox.Enabled = false;
                commentsTextBox.Enabled = false;
                personTechReviewTextBox.Enabled = false;
                commentsTechReviewTextBox.Enabled = false;
                personAdminReviewTextBox.Enabled = false;
                commentsAdminTextBox.Enabled = false;
                personAdminReviewDiv.Attributes.Remove("class");
                personTechReviewDiv.Attributes.Remove("class");
                personDesignReviewDiv.Attributes.Remove("class");
                personAdminReviewDiv.Attributes.Add("class", "input textbox3 readonly");
                personTechReviewDiv.Attributes.Add("class", "input textbox3 readonly");
                personDesignReviewDiv.Attributes.Add("class", "input textbox3 readonly");
            }
            else
            {
                personTextBox.Enabled = true;
                commentsTextBox.Enabled = true;
                personTechReviewTextBox.Enabled = true;
                commentsTechReviewTextBox.Enabled = true;
                personAdminReviewTextBox.Enabled = true;
                commentsAdminTextBox.Enabled = true;
                personAdminReviewDiv.Attributes.Remove("class");
                personTechReviewDiv.Attributes.Remove("class");
                personDesignReviewDiv.Attributes.Remove("class");
                personAdminReviewDiv.Attributes.Add("class", "input textbox3");
                personTechReviewDiv.Attributes.Add("class", "input textbox3");
                personDesignReviewDiv.Attributes.Add("class", "input textbox3");
            }
        }

        /// <summary>
        /// On Selected Index Changed for Partner Drop down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PartnerDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (partnerDropdown.SelectedValue == DropDownConstants.DefaultValue)
                {
                    contactIdHiddenField.Value = string.Empty;
                    firstNameTextBox.Text = string.Empty;
                    lastNameTextBox.Text = string.Empty;
                    emailAddressTextBox.Text = string.Empty;
                    phoneTextBox.Text = string.Empty;
                    faxTextBox.Text = string.Empty;
                }
                else
                {
                    Contact partnerContact = _partnerManager.GetPartnerContactDetails(Convert.ToInt32(partnerDropdown.SelectedValue, CultureInfo.InvariantCulture));
                    contactIdHiddenField.Value = partnerContact.ContactId;
                    firstNameTextBox.Text = partnerContact.FirstName;
                    lastNameTextBox.Text = partnerContact.LastName;
                    emailAddressTextBox.Text = partnerContact.Email;
                    phoneTextBox.Text = partnerContact.Phone;
                    faxTextBox.Text = partnerContact.Fax;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// On ProductCategory Repeater Item Data Bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProductCategoryRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RepeaterItem item = e.Item;
                Repeater repeaterDetails;
                if (((item.ItemType == ListItemType.Item) ||
                    (item.ItemType == ListItemType.AlternatingItem)) && item.ItemType != ListItemType.Header)
                {
                    repeaterDetails = (Repeater)(item.FindControl("assignedSpecificationsRepeater"));

                    ProductCategory productCategories = (ProductCategory)item.DataItem;
                    repeaterDetails.DataSource = productCategories.ProductAssignedSpecifications;
                    repeaterDetails.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Remove uploaded Image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageLinkButton.Text = string.Empty;
                imageNameHiddenField.Value = string.Empty;
                productImage.ImageUrl = "../Images/NoProduct.PNG";
                ClearSession(SessionConstants.ProductImageBytes);
                removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "SpecSheetScript1", "SetFileInputStyles();", true);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Remove uploaded PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemovePdfLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                pdfNameHiddenField.Value = string.Empty;
                specSheetPdfLinkButton.Text = string.Empty;
                ClearSession(SessionConstants.ProductSpecSheetPdf);
                specSheetPdfLinkButton.Text = "";
                removePdfLinkButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                ScriptManager.RegisterClientScriptBlock(specSheetUploadUpdatePanel, specSheetUploadUpdatePanel.GetType(), "SpecSheetScript", "SetFileInputStyles();", true);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Save the records into DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveLinkButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    if (GetViewState<string>(UIConstants.Mode) == UIConstants.Add)
                    {
                        if (Convert.ToInt32(productIdHiddenField.Value) > 0)
                            UpdateProduct();
                        else
                            AddProduct();
                    }
                    else if ((GetViewState<string>(UIConstants.Mode) == UIConstants.Edit))
                    {
                        UpdateProduct();
                    }
                }
                catch (Exception exception)
                {
                    UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }
            }
        }

        /// <summary>
        /// To get the product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SpecSheetPdfLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (GetViewState<string>(UIConstants.Mode) == UIConstants.Edit)
                {
                    Product product = _productManager.GetProductDetails((int)GetViewState(UIConstants.ProductListId));
                    ProcessFileDownload("pdf", product.SpecSheetPdf, product.SpecSheetPdfName);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            ScriptManager.RegisterClientScriptBlock(specSheetUploadUpdatePanel, specSheetUploadUpdatePanel.GetType(), "SpecSheetScript", "SetFileInputStyles();", true);
        }

        /// <summary>
        /// To add products
        /// </summary>
        private void AddProduct()
        {
            try
            {
                IList<ProductCategory> productCategories = GetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories);
                if (productCategories.Where(i => i.ProductAssignedSpecifications.Count > 0).Count() > 0)
                {
                    Product product = new Product()
                    {
                        ProductName = productNameTextBox.Text.Trim(),
                        ProductSku = skuTextBox.Text.Trim(),
                        ProductImage = Session[SessionConstants.ProductImageBytes] != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? (byte[])Session[SessionConstants.ProductImageBytes] : null,
                        ProductThumbnailImage = Session[SessionConstants.ProductImageBytes] != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? ((byte[])Session[SessionConstants.ProductImageBytes]).ConvertToThumbnail() : null,
                        SpecSheetPdf = Session[SessionConstants.ProductSpecSheetPdf] != null ? (byte[])Session[SessionConstants.ProductSpecSheetPdf] : null,
                        PartnerId = Convert.ToInt32(partnerDropdown.SelectedValue, CultureInfo.InvariantCulture),
                        SpecSheetPdfName = pdfNameHiddenField.Value,
                        ImageName = imageNameHiddenField.Value,
                        Description = descriptionTextArea.Text.Trim(),
                        Website = websiteTextBox.Text.Trim(),
                        ContactId = string.IsNullOrEmpty(contactIdHiddenField.Value) ? 0 : Convert.ToInt32(contactIdHiddenField.Value, CultureInfo.InvariantCulture),
                        Contact = new Contact()
                        {
                            FirstName = firstNameTextBox.Text.Trim(),
                            LastName = lastNameTextBox.Text.Trim(),
                            Email = emailAddressTextBox.Text.Trim(),
                            Phone = phoneTextBox.Text.Trim(),
                            Fax = faxTextBox.Text.Trim()
                        },
                        StatusId = Convert.ToInt32(statusDropDown.SelectedValue, CultureInfo.InvariantCulture),
                        RequestedById = Convert.ToInt32(requestedByIdHiddenField.Value, CultureInfo.InvariantCulture),
                        DesignReviewBy = personTextBox.Text.Trim(),
                        DesignReviewComments = commentsTextBox.Text.Trim(),
                        TechReviewBy = personTechReviewTextBox.Text.Trim(),
                        TechReviewComments = commentsTechReviewTextBox.Text.Trim(),
                        AdminReviewBy = personAdminReviewTextBox.Text.Trim(),
                        AdminReviewComments = commentsAdminTextBox.Text.Trim(),
                        UserId = GetLoggedinUserId(),
                    };
                    int result = _productManager.AddProduct(product, productCategories);
                    if (result == 0)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                    }
                    else if (result > 0)
                    {
                        PopulateProductDetails(Convert.ToInt32(result));
                        productIdHiddenField.Value = result.ToString(CultureInfo.InvariantCulture);
                        ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.ProductSuccessfullyAdded, culture));
                    }
                }

                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.ProvideProductAssignedSpec, culture));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To bind root category drop down
        /// </summary>
        private void BindRootCategoryDropDown()
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            categoriesDropDown.DataSource = categories.Where(item => item.ParentCategoryId.Equals(-1)).OrderBy(p => p.CategoryName);
            categoriesDropDown.DataTextField = "CategoryName";
            categoriesDropDown.DataValueField = "CategoryId";
            categoriesDropDown.DataBind();
            categoriesDropDown.Items.Insert(0, new ListItem("Select a Category", "0"));
        }

        /// <summary>
        /// To Find the occurrance of controls
        /// </summary>
        /// <param name="substr"></param>
        /// <returns></returns>
        private int FindOccurence(string substr)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int count = 0;
            for (int i = 0; i < ctrls.Length; i++)
            {
                if (ctrls[i].Contains(substr + "-") && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// To find the populataed drop down
        /// </summary>
        /// <param name="selectedCategoryId"></param>
        /// <param name="id"></param>
        private void FindPopulateDropDowns(string selectedCategoryId, string id)
        {
            DropDownList newDropDown = (DropDownList)FindControl(categoryPlaceHolder, id);
            PopulateDropDown(newDropDown, selectedCategoryId);
        }

        /// <summary>
        /// To get the child categories
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

            IList<Category> categoryList = (categories.AsEnumerable().
                                            Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture))).ToList());
            return categoryList;
        }

        /// <summary>
        /// To get root level categories
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetRootLevelCategories()
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

            IList<Category> rootCategoryList = (categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
            return rootCategoryList;
        }


        /// <summary>
        /// To Populate Drop down
        /// </summary>
        /// <param name="parentCategoryDropDown"></param>
        /// <param name="selectedValue"></param>
        private void PopulateDropDown(DropDownList parentCategoryDropDown, string selectedValue)
        {
            parentCategoryDropDown.Items.Clear();

            IList<Category> categoryList = new List<Category>();
            if (String.IsNullOrEmpty(selectedValue))
            {
                categoryList = GetRootLevelCategories();
            }
            else if (selectedValue != "0")
            {
                categoryList = GetChildCategories(selectedValue);
            }
            if (categoryList != null)
            {
                BindData(parentCategoryDropDown, categoryList);
            }
            parentCategoryDropDown.Items.Insert(0, new ListItem("Select a Subcategories", "0"));
        }

        /// <summary>
        /// To populate Drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            BindDynamicDropDown(DropDownConstants.PartnerDropDown, partnerDropdown, ResourceUtility.GetLocalizedString(1234, culture, "Select a Partner"), null);
            BindStaticDropDown(DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(1192, culture, "All Brands"), null);
            BindStaticDropDown(DropDownConstants.RegionDropDown, regionDropDown, string.Empty, null);
            BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1300, culture, "All Countries"), null);
            BindStaticDropDown(DropDownConstants.ProductStatusDropDown, statusDropDown, ResourceUtility.GetLocalizedString(1235, culture, "Select a Status"), null);
        }

        /// <summary>
        /// To Populate Product Details
        /// </summary>
        /// <param name="productId"></param>
        private void PopulateProductDetails(int? productId)
        {
            Product product = _productManager.GetProductDetails((int)productId);

            productDetailIdLabel.Text = product.ProductId.ToString(CultureInfo.InvariantCulture);
            productNameTextBox.Text = product.ProductName;
            skuTextBox.Text = product.ProductSku;
            partnerDropdown.SelectedValue = product.PartnerId.ToString(CultureInfo.InvariantCulture);
            specSheetPdfLinkButton.Text = product.SpecSheetPdfName;
            imageLinkButton.Text = product.ImageName;
            productImage.ImageUrl = !string.IsNullOrEmpty(product.ImageName) && product.ProductThumbnailImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(product.ProductThumbnailImage) : "../Images/NoProduct.PNG";
            descriptionTextArea.Text = product.Description;
            websiteTextBox.Text = product.Website;
            contactIdHiddenField.Value = product.ContactId.ToString(CultureInfo.InvariantCulture);
            firstNameTextBox.Text = product.Contact.FirstName;
            lastNameTextBox.Text = product.Contact.LastName;
            emailAddressTextBox.Text = product.Contact.Email;
            phoneTextBox.Text = product.Contact.Phone;
            faxTextBox.Text = product.Contact.Fax;
            statusDropDown.SelectedValue = product.StatusId.ToString(CultureInfo.InvariantCulture);
            requestedByIdHiddenField.Value = product.RequestedById.ToString(CultureInfo.InvariantCulture);
            requestedByLinkButton.Text = product.RequestedBy;
            bool userPermissionAuthorization = ValidateAdminControlAuthorization((int)AdminPermissionEnum.Users);
            if (!userPermissionAuthorization)
            {
                requestedByLinkButton.Attributes.Add("href", "javascript:void(0);");
            }

            partnerNameLabel.Text = ";" + product.PartnerName;
            requestDateTextBox.Text = Convert.ToDateTime(product.RequestedDate, CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            personTextBox.Text = product.DesignReviewBy;
            commentsTextBox.Text = product.DesignReviewComments;
            personTechReviewTextBox.Text = product.TechReviewBy;
            commentsTechReviewTextBox.Text = product.TechReviewComments;
            personAdminReviewTextBox.Text = product.AdminReviewBy;
            commentsAdminTextBox.Text = product.AdminReviewComments;

            pdfNameHiddenField.Value = product.SpecSheetPdfName;
            imageNameHiddenField.Value = product.ImageName;

            if (!string.IsNullOrEmpty(product.ImageName))
                removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "inline");
            if (product.SpecSheetPdf != null && !string.IsNullOrEmpty(product.SpecSheetPdfName))
                removePdfLinkButton.Style.Add(HtmlTextWriterStyle.Display, "inline");

            SetSession<byte[]>(SessionConstants.ProductImageBytes, product.ProductImage);
            IList<ProductCategory> productCategoryCollection = _productManager.GetProductAssignedSpecifications((int)productId);
            productCategoryRepeater.DataSource = productCategoryCollection;
            productCategoryRepeater.DataBind();
            SetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories, productCategoryCollection);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void RequestedByLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                bool userPermissionAuthorization = ValidateAdminControlAuthorization((int)AdminPermissionEnum.Users);
                if (userPermissionAuthorization)
                {
                    SetSession<string>(SessionConstants.UserId, requestedByIdHiddenField.Value);
                    Response.Redirect("~/Admin/Users.aspx", false);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Process file download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            string extension = string.Empty;

            switch (fileType)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "image":
                    string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    int upperBound = tempString.GetUpperBound(0);
                    contentType = "image/" + tempString[upperBound];
                    break;
                default:
                    contentType = "";
                    break;
            };

            Response.Buffer = true;
            Response.Charset = "";
            Response.ClearHeaders();
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
            Response.BinaryWrite(fileBytes);
            Response.Flush();
            Response.Clear();
            Response.End();
        }

        /// <summary>
        /// To get the categories from View state
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(string currentSelectedCategoryId)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

            Category category = (Category)(categories.Where(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)).Single());
            return category;
        }

        /// <summary>
        /// To Recreate Controls
        /// </summary>
        /// <param name="ctrlPrefix"></param>
        /// <param name="ctrlType"></param>
        private void RecreateControls(string ctrlPrefix, string ctrlType)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int cnt = FindOccurence(ctrlPrefix);
            if (cnt > 0)
            {
                for (int k = 1; k <= cnt; k++)
                {
                    for (int i = 0; i < ctrls.Length; i++)
                    {
                        if (ctrls[i].Contains(ctrlPrefix + "-" + (k + 1).ToString(CultureInfo.InvariantCulture)) && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                        {
                            string ctrlID = string.Empty;
                            string[] subCtrls = ctrls[i].Split('-');

                            for (int l = 0; l < subCtrls.Length; l++)
                            {
                                if (subCtrls[l].Contains("categoriesDropDown"))
                                {
                                    ctrlID = subCtrls[l + 1].Substring(0, 1);
                                }
                            }

                            ctrlID = "categoriesDropDown-" + ctrlID;

                            if (ctrlType == "DropDownList")
                            {
                                CreateDropDownList(ctrlID);
                            }
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// To set the page settings
        /// </summary>
        private void SetPageSettings()
        {
            User user = GetSession<User>(SessionConstants.User);
            if (Context.Items[UIConstants.Mode] != null && Context.Items[UIConstants.Mode].ToString() == UIConstants.Edit)
            {
                SetViewState(UIConstants.Mode, UIConstants.Edit);
                int? productId = Convert.ToInt32(Context.Items[UIConstants.ProductListId], CultureInfo.InvariantCulture);
                SetViewState(UIConstants.ProductListId, productId);

                SetLabelText(productDetailTitleLabel, ResourceUtility.GetLocalizedString(191, culture, "Edit Product"));

                productNameDiv.Attributes.Add("class", "input textbox3 mandatory");

                PopulateProductDetails(productId);
            }
            else
            {
                ClearSession(SessionConstants.ProductImageBytes);
                SetViewState(UIConstants.Mode, UIConstants.Add);
                SetLabelText(productDetailTitleLabel, ResourceUtility.GetLocalizedString(190, culture, "Add Product"));
                requestedByIdHiddenField.Value = user.UserId.ToString(CultureInfo.InvariantCulture);
                productNameDiv.Attributes.Add("class", "input textbox3 mandatory");
                productImage.ImageUrl = "../Images/NoProduct.PNG";
            }
        }

        /// <summary>
        /// To update the product
        /// </summary>
        private void UpdateProduct()
        {
            try
            {
                IList<ProductCategory> productCategories = GetViewState<IList<ProductCategory>>(ViewStateConstants.ProductCategories);
                if (productCategories.Where(i => i.ProductAssignedSpecifications.Count > 0).Count() > 0)
                {
                    int result = 0;
                    Product product = new Product()
                    {
                        ProductId = Convert.ToInt32(productDetailIdLabel.Text, CultureInfo.InvariantCulture),
                        ProductName = productNameTextBox.Text.Trim(),
                        ProductSku = skuTextBox.Text.Trim(),
                        ProductImage = GetSession<byte[]>(SessionConstants.ProductImageBytes) != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? GetSession<byte[]>(SessionConstants.ProductImageBytes) : null,
                        ProductThumbnailImage = GetSession<byte[]>(SessionConstants.ProductImageBytes) != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? (GetSession<byte[]>(SessionConstants.ProductImageBytes).ConvertToThumbnail()) : null,
                        SpecSheetPdf = Session[SessionConstants.ProductSpecSheetPdf] != null ? (byte[])Session[SessionConstants.ProductSpecSheetPdf] : null,
                        PartnerId = Convert.ToInt32(partnerDropdown.SelectedValue, CultureInfo.InvariantCulture),
                        SpecSheetPdfName = pdfNameHiddenField.Value,
                        ImageName = imageNameHiddenField.Value,
                        Description = descriptionTextArea.Text.Trim(),
                        Website = websiteTextBox.Text.Trim(),
                        ContactId = Convert.ToInt32(contactIdHiddenField.Value, CultureInfo.InvariantCulture),
                        Culture = Convert.ToString(culture, CultureInfo.InvariantCulture),
                        Contact = new Contact()
                        {
                            FirstName = firstNameTextBox.Text.Trim(),
                            LastName = lastNameTextBox.Text.Trim(),
                            Email = emailAddressTextBox.Text.Trim(),
                            Phone = phoneTextBox.Text.Trim(),
                            Fax = faxTextBox.Text.Trim()
                        },
                        StatusId = Convert.ToInt32(statusDropDown.SelectedValue, CultureInfo.InvariantCulture),
                        DesignReviewBy = personTextBox.Text.Trim(),
                        DesignReviewComments = commentsTextBox.Text.Trim(),
                        TechReviewBy = personTechReviewTextBox.Text.Trim(),
                        TechReviewComments = commentsTechReviewTextBox.Text.Trim(),
                        AdminReviewBy = personAdminReviewTextBox.Text.Trim(),
                        AdminReviewComments = commentsAdminTextBox.Text.Trim(),
                        UserId = GetLoggedinUserId(),
                    };

                    if (Session[SessionConstants.ProductSpecSheetPdfName] != null)  //If the spect sheet is selected 
                    {
                        if (Session[SessionConstants.ProductSpecSheetPdfName].ToString().Contains(pdfNameHiddenField.Value))  //If the selected spect sheet name is same in session and in spect sheet name hidden filed i.e no tab-session issue then save normally
                        {
                            result = _productManager.UpdateProduct(product, productCategories);
                            if (result <= 0)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                            }
                            else if (result > 0)
                            {
                                PopulateProductDetails(Convert.ToInt32(result));
                                ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.ProductSuccessfullyUpdated, culture));
                            }
                        }
                        else
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SpecSheetErrorMessage, culture)); // Show error in case session and spect sheet name hidden filed name are different i.e a different sheet is browsed from other browse tab
                        }
                    }
                    else          //If the spect sheet is not selected in this transaction then save normally
                    {
                        result = _productManager.UpdateProduct(product, productCategories);
                        if (result <= 0)
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                        }
                        else if (result > 0)
                        {
                            PopulateProductDetails(Convert.ToInt32(result));
                            ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.ProductSuccessfullyUpdated, culture));
                        }
                    }
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.ProvideProductAssignedSpec, culture));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }
    }
}