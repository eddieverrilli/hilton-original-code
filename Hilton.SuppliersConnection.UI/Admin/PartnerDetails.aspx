﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="PartnerDetails.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.PartnerDetails" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../Scripts/custominputs.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.ui.datepicker.js"></script>
    <script src="../Scripts/jquery.characterlimiter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script type="text/javascript" src="../Scripts/mutiselect.category.dropdowns.js"></script>
    <script type="text/javascript" src="../Scripts/multiselect.admin.region.and.country.js"></script>
    <script type="text/javascript" language="javascript">
        function pageLoad() {
            $('textarea#companyDesriptionTextBox').limiter(2000, $('#remainingCharacters'));
        }
        $(document).ready(function () {
            SetFileInputStyles();
            SetCategoryMappingDropDowns();

            $("#activeDateTextbox").datepicker(
            { yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy',
                onSelect: function (selectedDate) {
                    var date = $(this).datepicker("getDate");
                    $('#expireDateTextbox').datepicker().datepicker('setDate', new Date(date.getFullYear() + 1, date.getMonth(), date.getDate() - 1));
                }
            }

            );

            $("#expireDateTextbox").datepicker(
            { yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' }

            );
        });

        function ForceCall() {
            $("#activeDateTextbox").datepicker(
            { yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy',
                onSelect: function (selectedDate) {
                    var date = $(this).datepicker("getDate");
                    $('#expireDateTextbox').datepicker().datepicker('setDate', new Date(date.getFullYear() + 1, date.getMonth(), date.getDate() - 1));
                }
            }

            );

            $("#expireDateTextbox").datepicker(
            { yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' }

            );

        }
        function SetFileInputStyles() {
            $("input.customInput").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 65
            });

        }

        function ajaxFileUpload(control, fileType) {
            if (fileType == 'image') {
                var imageName = $('#imageUpload')[0].value.toString().substring($('#imageUpload')[0].value.toString().lastIndexOf('\\') + 1, $('#imageUpload')[0].value.length);
                $('#<%= imageNameHiddenField.ClientID %>').val(imageName);
            }

            if (fileType == 'image')
                $('#loading').attr('source', fileType);

            $("#loading")
                .ajaxStart(function (obj) {
                    if ($(this).attr('source') == 'image') {
                        $(this).show();
                        $(this).attr('source', '');
                    }

                })
                .ajaxComplete(function (obj) {
                    $(this).hide();
                });

            $.ajaxFileUpload
                    (
                        {
                            url: '../Handlers/FileUpload.ashx?fileType=' + fileType,
                            secureuri: false,
                            fileElementId: control,
                            dataType: 'json',
                            data: { name: 'login', id: 'id' },
                            success: function (data, status) {
                                if (status == 'success' && data.responseText != null && data.responseText == 'uploaded') {
                                    SetFileLink(fileType);
                                    if (fileType == 'image') {
                                        $.ajax({
                                            type: "POST",
                                            url: "../Handlers/FileUpload.ashx?fileType=" + fileType + "&op=show",
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (data, img) {
                                                $('#<%= companyLogo.ClientID %>').attr('src', "data:image/png;base64," + data);
                                            },
                                            error: function (data, status, e) {
                                            }
                                        });
                                    }
                                }
                                else {
                                    ValidateFile(fileType, data.responseText);
                                }
                            },
                            error: function (data, status, e) {
                            }
                        }
                    )
            return false;
        }

        function SetFileLink(fileType) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text('');
                    $('#imageErrSpan').css({ 'display': 'none' });
                    $('#<%= imageLinkButton.ClientID %>').text($('#<%= imageNameHiddenField.ClientID %>').val());
                    if ($('#imageNameHiddenField').val() != '') {
                        $('#<%= removeImageLinkButton.ClientID %>').css({ 'display': 'inline' });
                    }
                    break;
            }
        }

        function ValidateFile(fileType, msg) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text(msg);
                    $('#imageErrSpan').css({ 'display': 'inline' });
                    $('#<%= removeImageLinkButton.ClientID %>').css({ 'display': 'none' });
                    $('#<%= imageNameHiddenField.ClientID %>').val("");
                    $('#<%= imageLinkButton.ClientID %>').text("");
                    $('#<%= companyLogo.ClientID %>').attr('src', '../Images/NoProduct.PNG');
                    break;
            }
        }
        function InitializeScript() {
            $('.accordion-1 h3').click(function () {
                $(this).toggleClass('active');
                $(this).next().toggle();
            });
            $('.scroll tr').click(function () {
                $('.scroll tr').removeClass('active');
                $(this).addClass('active');
            });
        }

        function inputFocus(i) {
            if (i.value == 'Name' || i.value == 'Hilton ID' || i.value == 'Email address') {
                i.value = ""; i.style.color = "#000";
            }

        }
        function inputBlur(i) {
            if (i.value == "") {
                if ($(i)[0].id.indexOf('addNameTextBox') != -1) {
                    $(i).val("Name");
                    i.defaultValue = "Name";
                }
                else if ($(i)[0].id.indexOf('addHiltonIdTextBox') != -1) {
                    $(i).val("Hilton ID");
                    i.defaultValue = "Hilton ID";
                }
                else if ($(i)[0].id.indexOf('addEmailTextBox') != -1) {
                    $(i).val("Email address");
                    i.defaultValue = "Email address";
                }
                i.style.color = "#888";
            }

        }
        function SetCategoryMappingDropDowns() {
            getItems();
            //SetCategoriesDropDown();
            SetCategoriesDropDownPartnerDetail();
            // SetRegionAndCountryDropDown($('#categoryRegionDropDown'), $('#categoryCountryDropDown'), $('#selectedRegions'), $('#selectedCountries'));
            SetRegionAndCountryDropDownPartnerDetail($('#categoryRegionDropDown'), $('#categoryCountryDropDown'), $('#selectedRegions'), $('#selectedCountries'));
            $("#optgroup").multiselect({
                position: {
                    my: 'left bottom',
                    at: 'left top'
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="partnerDetailsScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../Images/search-top-curve.gif" alt="" /></div>
            <div class="middle">
                <asp:HyperLink ID="backToPartnersHyperLink" runat="server" Text="‹‹ Back to Partners"
                    NavigateUrl="~/Admin/Partners.aspx">
                </asp:HyperLink>
            </div>
            <div class="bottom-curve">
                <img src="../Images/search-bottom-curve.gif" alt="" />
            </div>
        </div>
    </div>
    <div class="content">
        <div class="wrapper">
            <h2>
                <asp:Literal ID="titleLiteral" runat="server"></asp:Literal>
            </h2>
            <div id="add-edit-partner" class="inner-content">
                <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                        <br />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="lft-col">
                    <div class="box1">
                        <asp:UpdatePanel ID="companyInfoUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <h3>
                                    <asp:Label ID="companyInfoLabel" runat="server" CBID="139" Text="Company Information"></asp:Label>
                                </h3>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="companyNameLabel" runat="server" CBID="136" Text="Company Name:"></asp:Label></div>
                                    <div class="input textbox3 mandatory">
                                        <asp:TextBox ID="companyNameTextBox" MaxLength="256" runat="server"></asp:TextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="companyNameRequiredField" VMTI="12" SetFocusOnError="true"
                                            runat="server" ErrorMessage="Please provide company name" ValidationGroup="SavePartner"
                                            ControlToValidate="companyNameTextBox" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <asp:HiddenField ID="partnerIdHiddenField" runat="server" />
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="companyAddressLabel" runat="server" CBID="959" Text="Company Address:"></asp:Label>
                                    </div>
                                    <div class="input textbox3 mandatory">
                                        <asp:TextBox ID="companyAddress1TextBox" MaxLength="256" runat="server" />
                                        <br />
                                        <asp:RequiredFieldValidator ID="companyAddressRequiredField" SetFocusOnError="true"
                                            VMTI="13" runat="server" ErrorMessage="Please provide company address" ValidationGroup="SavePartner"
                                            ControlToValidate="companyAddress1TextBox" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        &nbsp</div>
                                    <div class="input textbox3">
                                        <asp:TextBox ID="companyAddress2TextBox" MaxLength="256" runat="server" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="countryLabel" runat="server" CBID="346" Text="Country:"></asp:Label></div>
                                    <div class="input no-pad ">
                                        <asp:DropDownList ID="countryDropDown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CountryDropDown_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="countryRequiredField" SetFocusOnError="true" VMTI="14"
                                            runat="server" ErrorMessage="Please select a country" ValidationGroup="SavePartner"
                                            ControlToValidate="countryDropDown" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="cityLabel" runat="server" CBID="297" Text="City:"></asp:Label></div>
                                    <div class="input textbox3 mandatory">
                                        <asp:TextBox ID="cityTextBox" runat="server" MaxLength="128"></asp:TextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="cityRequiredField" SetFocusOnError="true" VMTI="15"
                                            runat="server" ErrorMessage="Please provide city" ValidationGroup="SavePartner"
                                            ControlToValidate="cityTextBox" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="stateLabel" runat="server" CBID="690" Text="State:"></asp:Label></div>
                                    <div class="input no-pad ">
                                        <asp:DropDownList ID="stateDropDown" runat="server">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="stateRequiredFieldValidator" SetFocusOnError="true"
                                            VMTI="16" runat="server" ErrorMessage="Please select state" ValidationGroup="SavePartner"
                                            ControlToValidate="stateDropDown" CssClass="errorText" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="zipLabel" runat="server" CBID="762" Text="Postal Code:"></asp:Label></div>
                                    <div id="zipTextBoxDiv" runat="server">
                                        <asp:TextBox ID="zipTextBox" MaxLength="10" runat="server" Enabled="true"></asp:TextBox>
                                        <asp:Label ID="zipErrorMessage" CssClass="errorText" runat="server" />
                                        <asp:RequiredFieldValidator ID="zipRequiredFieldValidator" VMTI="17" runat="server"
                                            SetFocusOnError="true" ValidationGroup="SavePartner" ControlToValidate="zipTextBox"
                                            CssClass="errorText" Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="zipCodeRegularExpressionValidator" VMTI="18"
                                            CssClass="errorText" ControlToValidate="zipTextBox" ValidationGroup="SavePartner"
                                            SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="companyDescriptionLabel" runat="server" CBID="309" Text="Company Description:"></asp:Label></div>
                                    <div class="input no-pad">
                                        <asp:TextBox TextMode="MultiLine" ID="companyDesriptionTextBox" ClientIDMode="Static"
                                            runat="server"></asp:TextBox>
                                    </div>
                                    <div class="errorText partnercomdesc">
                                        <asp:Label runat="server" ID="remainingCharacters" ClientIDMode="Static"></asp:Label>
                                        <asp:Label runat="server" ID="remainingCharacterLabel" Text="characters remaining"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="samPartnerLabel" runat="server" CBID="632" Text="SAM Partner?:"></asp:Label></div>
                                    <div class="input no-pad small">
                                        <asp:DropDownList ID="samDropDown" runat="server">
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="webSiteLinkLabel" runat="server" CBID="0" Text="Web Site:"></asp:Label>
                                    </div>
                                    <div class="input textbox3">
                                        <asp:TextBox ID="webSiteLinkTextBox" MaxLength="256" runat="server" />
                                        <br />
                                        <asp:RegularExpressionValidator ID="webSiteRegularExpressionValidator" VMTI="20"
                                            SetFocusOnError="true" runat="server" ControlToValidate="webSiteLinkTextBox"
                                            ErrorMessage="Please enter valid website" CssClass="errorText" ValidationGroup="SavePartner"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="partnerLogoLabel" runat="server" CBID="429" Text="Partner Logo:"></asp:Label></div>
                            <asp:UpdatePanel ID="imageUploadUpdatePanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="input image">
                                        <p class="imageload">
                                            <img id="loading" src="../Images/async.gif" class="loadingimg" />
                                            <asp:Image ID="companyLogo" runat="server" CssClass="pic imagestyle" /></p>
                                        <div class="fileinputs">
                                            <input type="file" size="5px" id="imageUpload" class="customInput customStyle" name="companyLogo1"
                                                onchange="return ajaxFileUpload('imageUpload','image');" />
                                        </div>
                                        <div class="imgsrc">
                                            <asp:LinkButton ID="imageLinkButton" CssClass="errorText" runat="server" ViewStateMode="Enabled"
                                                CausesValidation="false" OnClick="ImageLinkButton_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="removeImageLinkButton" OnClick="RemoveImageLinkButton_Click"
                                                runat="server" Style="display: none; margin-left: 15px" CausesValidation="false"
                                                CssClass="errorText" Text="X" CBID="753"></asp:LinkButton>
                                        </div>
                                        <span id="imageErrSpan" class="errorText" style="display: none"></span>
                                        <input type="hidden" id="imageNameHiddenField" runat="server" value="" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="imageLinkButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="clear">
                            </div>
                            <div class="form-spec">
                                <asp:Label ID="companyLogoSizeDescription" runat="server" Text="Images should be JPG or PNG, resized to 235x235 pixels"
                                    CBID="1129"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="includeRecPartnerLabel" runat="server" CBID="955" Text="Is Recommended Partner?:">
                                </asp:Label></div>
                            <div class="input no-pad small">
                                <asp:DropDownList ID="incRecPartnerDropDown" runat="server">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="box1">
                        <h3>
                            <asp:Label ID="primaryContactLabel" runat="server" CBID="520" Text="Primary Contact">
                            </asp:Label></h3>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="firstNameLabel" runat="server" CBID="106" Text="First Name:"></asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="firstNameTextBox" runat="server" MaxLength="128">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="firstNameRequiredField" SetFocusOnError="true" VMTI="1"
                                    runat="server" ErrorMessage="Please provide first name" ValidationGroup="SavePartner"
                                    ControlToValidate="firstNameTextBox" CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="firstNameRegularExpressionValidator" VMTI="2"
                                    SetFocusOnError="true" runat="server" ControlToValidate="firstNameTextBox" ErrorMessage="Please enter valid name"
                                    CssClass="errorText" ValidationGroup="SavePartner" Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="lastNameLabel" runat="server" CBID="118" Text="Last Name:"></asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="lastNameTextBox" runat="server" MaxLength="128">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="lastNameRequiredField" VMTI="3" SetFocusOnError="true"
                                    runat="server" ErrorMessage="Please provide last name" ValidationGroup="SavePartner"
                                    ControlToValidate="lastNameTextBox" CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="lastNameRegularExpressionValidator" SetFocusOnError="true"
                                    VMTI="4" runat="server" ControlToValidate="lastNameTextBox" ErrorMessage="Please enter valid name"
                                    CssClass="errorText" ValidationGroup="SavePartner" Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="titleLabel" runat="server" CBID="715" Text="Title:"></asp:Label></div>
                            <div class="input textbox3">
                                <asp:TextBox ID="titleTextBox" runat="server" MaxLength="255">
                                </asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="titleRegularExpressionValidator" VMTI="5" SetFocusOnError="true"
                                    runat="server" ControlToValidate="titleTextBox" ErrorMessage="Please enter valid title"
                                    CssClass="errorText" ValidationGroup="SavePartner" Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="phoneNumberLabel" runat="server" CBID="131" Text="Phone Number:"></asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="phoneNumberTextBox" runat="server" MaxLength="32">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="phoneNumberRequiredField" runat="server" VMTI="6"
                                    SetFocusOnError="true" ErrorMessage="Please provide phone number" ValidationGroup="SavePartner"
                                    ControlToValidate="phoneNumberTextBox" CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="phoneRegularExpressionValidator" VMTI="7" CssClass="errorText"
                                    ControlToValidate="phoneNumberTextBox" ValidationGroup="SavePartner" SetFocusOnError="true"
                                    runat="server">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="faxLabel" runat="server" CBID="148" Text="Fax Number:"></asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="faxTextBox" runat="server" MaxLength="32">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="faxRequiredField" VMTI="8" SetFocusOnError="true"
                                    runat="server" ErrorMessage="Please provide fax number" ValidationGroup="SavePartner"
                                    ControlToValidate="faxTextBox" CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="faxRegularExpressionValidator" SetFocusOnError="true"
                                    ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="faxTextBox"
                                    ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="SavePartner"
                                    Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="emailAddressLabel" runat="server" CBID="388" Text="Email Address:"></asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="emailTextBox" runat="server" MaxLength="128">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="emailRequiredField" VMTI="10" SetFocusOnError="true"
                                    runat="server" ErrorMessage="Please provide email" ValidationGroup="SavePartner"
                                    ControlToValidate="emailTextBox" CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" SetFocusOnError="true"
                                    ViewStateMode="Enabled" VMTI="11" runat="server" ErrorMessage="Correct the format"
                                    ControlToValidate="emailTextBox" CssClass="errorText" ValidationGroup="SavePartner">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                    <div class="box1">
                        <h3>
                            <asp:Label ID="secondayContactLabel" runat="server" CBID="0" Text="Secondary Contact">
                            </asp:Label></h3>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="secondaryContactFirstNameLabel" runat="server" CBID="106" Text="First Name:">
                                </asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="secondaryContactFirstNameTextbox" runat="server" MaxLength="128">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="secondaryContactFirstNameRequiredFieldValidator"
                                    SetFocusOnError="true" VMTI="1" runat="server" ErrorMessage="Please provide first name"
                                    ValidationGroup="SavePartner" ControlToValidate="secondaryContactFirstNameTextbox"
                                    CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="secondaryContactFirstNamerRegularExpressionValidator"
                                    VMTI="2" SetFocusOnError="true" runat="server" ControlToValidate="secondaryContactFirstNameTextbox"
                                    ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="SavePartner"
                                    Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="secondaryContactLastNameLabel" runat="server" CBID="118" Text="Last Name:">
                                </asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="secondaryContactLastNameTextbox" runat="server" MaxLength="128">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="secondaryContactLastNameRequiredFieldValidator" VMTI="3"
                                    SetFocusOnError="true" runat="server" ErrorMessage="Please provide last name"
                                    ValidationGroup="SavePartner" ControlToValidate="secondaryContactLastNameTextbox"
                                    CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="secondaryContactLastNameRegularExpressionValidator"
                                    SetFocusOnError="true" VMTI="4" runat="server" ControlToValidate="secondaryContactLastNameTextbox"
                                    ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="SavePartner"
                                    Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="secondaryContactTitleLabel" runat="server" CBID="715" Text="Title:"></asp:Label></div>
                            <div class="input textbox3">
                                <asp:TextBox ID="secondaryContactTitleTextbox" runat="server" MaxLength="255">
                                </asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="secondaryContactTitleRegularExpressionValidator"
                                    VMTI="5" SetFocusOnError="true" runat="server" ControlToValidate="secondaryContactTitleTextbox"
                                    ErrorMessage="Please enter valid title" CssClass="errorText" ValidationGroup="SavePartner"
                                    Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="secondaryContactPhoneLabel" runat="server" CBID="131" Text="Phone Number:">
                                </asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="secondaryContactPhoneTextbox" runat="server" MaxLength="32">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="secondaryContactPhoneRequiredFieldValidator" runat="server"
                                    VMTI="6" SetFocusOnError="true" ErrorMessage="Please provide phone number" ValidationGroup="SavePartner"
                                    ControlToValidate="secondaryContactPhoneTextbox" CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="secondaryContactPhoneRegularExpressionValidator"
                                    VMTI="7" CssClass="errorText" ControlToValidate="secondaryContactPhoneTextbox"
                                    ValidationGroup="SavePartner" SetFocusOnError="true" runat="server">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="secondaryContactFaxLabel" runat="server" CBID="148" Text="Fax Number:">
                                </asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="secondaryContactFaxTextbox" runat="server" MaxLength="32">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="secondaryContactFaxRequiredFieldValidator" VMTI="8"
                                    SetFocusOnError="true" runat="server" ErrorMessage="Please provide fax number"
                                    ValidationGroup="SavePartner" ControlToValidate="secondaryContactFaxTextbox"
                                    CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="secondaryContactFaxRegularExpressionValidator"
                                    SetFocusOnError="true" ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="secondaryContactFaxTextbox"
                                    ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="SavePartner"
                                    Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="secondaryContactEmailLabel" runat="server" CBID="388" Text="Email Address:">
                                </asp:Label></div>
                            <div class="input textbox3 mandatory">
                                <asp:TextBox ID="secondaryContactEmailTextbox" runat="server" MaxLength="128">
                                </asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="secondaryContactEmailRequiredFieldValidator" VMTI="10"
                                    SetFocusOnError="true" runat="server" ErrorMessage="Please provide email" ValidationGroup="SavePartner"
                                    ControlToValidate="secondaryContactEmailTextbox" CssClass="errorText" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="secondaryContactEmailRegularExpressionValidator"
                                    SetFocusOnError="true" ViewStateMode="Enabled" VMTI="11" runat="server" ErrorMessage="Correct the format"
                                    ControlToValidate="secondaryContactEmailTextbox" CssClass="errorText" ValidationGroup="SavePartner">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="regionDropDownUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="box1">
                                <h3>
                                    <asp:Label ID="regionalContactLabel" runat="server" CBID="604" Text="Country Contact"></asp:Label></h3>
                                <div class="row">
                                    <div class="label">
                                        Country:</div>
                                    <div class="input textbox3">
                                        <asp:DropDownList ID="regionDropDown" runat="server" OnSelectedIndexChanged="RegiondropDown_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="firstNameLabel1" runat="server" CBID="106" Text="First Name:"></asp:Label></div>
                                    <div runat="server" id="firstNameDiv">
                                        <asp:TextBox ID="regionalFirstNameTextBox" runat="server" MaxLength="128"></asp:TextBox>
                                        <br />
                                        <asp:RegularExpressionValidator ID="regionalFirstNameRegularExpressionValidator"
                                            runat="server" ControlToValidate="regionalFirstNameTextBox" VMTI="2" SetFocusOnError="true"
                                            ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="SavePartner"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="lastNameLabel1" runat="server" CBID="118" Text="Last Name:"></asp:Label></div>
                                    <div runat="server" id="lastNameDiv">
                                        <asp:TextBox ID="regionalLastNameTextBox" runat="server" MaxLength="128"></asp:TextBox>
                                        <br />
                                        <asp:RegularExpressionValidator ID="regionalLastNameRegularExpressionValidator" VMTI="4"
                                            SetFocusOnError="true" runat="server" ControlToValidate="regionalLastNameTextBox"
                                            ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="SavePartner"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="titleLabel1" runat="server" CBID="715" Text="Title:"></asp:Label></div>
                                    <div runat="server" id="titleDiv">
                                        <asp:TextBox ID="regionalTitleTextBox" runat="server" MaxLength="255"></asp:TextBox>
                                        <br />
                                        <asp:RegularExpressionValidator ID="regionalTitleRegularExpressionValidator" VMTI="5"
                                            SetFocusOnError="true" runat="server" ControlToValidate="regionalTitleTextBox"
                                            ErrorMessage="Please enter valid title" CssClass="errorText" ValidationGroup="SavePartner"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="phoneNumberLabel1" runat="server" CBID="131" Text="Phone Number:"></asp:Label></div>
                                    <div runat="server" id="phoneDiv">
                                        <asp:TextBox ID="regionalPhoneTextBox" MaxLength="32" runat="server"></asp:TextBox>
                                        <br />
                                        <asp:RegularExpressionValidator ID="phoneRegionalRegularExpressionValidator" VMTI="7"
                                            CssClass="errorText" ControlToValidate="regionalPhoneTextBox" ValidationGroup="SavePartner"
                                            SetFocusOnError="true" runat="server"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="faxNumberLabel1" runat="server" CBID="148" Text="Fax Number:"></asp:Label></div>
                                    <div runat="server" id="faxDiv">
                                        <asp:TextBox ID="regionalFaxTextBox" MaxLength="32" runat="server"></asp:TextBox>
                                        <br />
                                        <asp:RegularExpressionValidator ID="faxRegionalRegularExpressionValidator" SetFocusOnError="true"
                                            VMTI="9" runat="server" ControlToValidate="regionalFaxTextBox" ErrorMessage="Please enter valid fax"
                                            CssClass="errorText" ValidationGroup="SavePartner" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="emailAddressLabel1" runat="server" CBID="388" Text="Email Address:"></asp:Label></div>
                                    <div runat="server" id="emailDiv">
                                        <asp:TextBox ID="regionalEmailTextBox" runat="server" MaxLength="128"></asp:TextBox>
                                        <br />
                                        <asp:RegularExpressionValidator ID="regionalEmailRegularExpressionValidator" SetFocusOnError="true"
                                            VMTI="11" runat="server" ErrorMessage="Correct the format" CssClass="errorText"
                                            ControlToValidate="regionalEmailTextBox" ValidationGroup="SavePartner"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="section alR pad-top">
                        <span class="btn-style">
                            <asp:LinkButton ID="saveLinkButton" CBID="644" Text="Save" OnClick="SaveLinkButton_Click"
                                runat="server" ValidationGroup="SavePartner">
                            </asp:LinkButton>
                        </span>&nbsp;
                        <asp:CustomValidator ID="savePartnerCustomValidator" runat="server" ValidationGroup="SavePartner"
                            CssClass="errorText" Display="Dynamic" OnServerValidate="SavePartnerValidator_ServerValidate">
                        </asp:CustomValidator>
                    </div>
                    <div id="validationErrorMessageDiv" runat="server">
                    </div>
                </div>
                <div class="rgt-col">
                    <div id="partners">
                        <asp:UpdatePanel ID="partnershipStatusUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="statusLabel" runat="server" CBID="696" Text="Status:"></asp:Label></div>
                                    <div class="input">
                                        <asp:DropDownList ID="partnershipStatusDropDown" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="partnershipStatusRequiredField" VMTI="34" SetFocusOnError="true"
                                            runat="server" ErrorMessage="Please select status" ValidationGroup="SavePartner"
                                            ControlToValidate="partnershipStatusDropDown" InitialValue="0" CssClass="errorText"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="activeDateLabel" runat="server" CBID="956" Text="Active Date:"></asp:Label>
                                    </div>
                                    <div class="input textbox3 small1">
                                        <asp:TextBox ID="activeDateTextbox" ClientIDMode="Static" runat="server" class="datepicker"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="expirationDateLabel" runat="server" CBID="957" Text="Expiration Date:"></asp:Label>
                                    </div>
                                    <div class="input textbox3 small1">
                                        <asp:TextBox ID="expireDateTextbox" runat="server" ClientIDMode="Static" class="datepicker"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label ID="partnershipTypeLabel" CBID="958" runat="server" Text="Partnership Type:"></asp:Label></div>
                                    <div class="input">
                                        <asp:DropDownList ID="partnershipTypeDropDown" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="partnershipTypeRequiredFieldValidator" VMTI="33"
                                            SetFocusOnError="true" runat="server" ErrorMessage="Please select Partnership Type"
                                            ValidationGroup="SavePartner" ControlToValidate="PartnershipTypeDropDown" InitialValue="0"
                                            CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div id="Div1" class="row" runat="server" visible="false">
                                    <div class="label">
                                        <asp:Label ID="submitProductsLabel" CBID="1113" runat="server" Text="Submit Products:"></asp:Label></div>
                                    <div class="input">
                                        <asp:CheckBox ID="submitProductsCheckBox" runat="server" ViewStateMode="Enabled"
                                            Checked="true" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        &nbsp;</div>
                                    <div class="errorText  ">
                                        <asp:CustomValidator ID="validateActivationDateCustomValidator" runat="server" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="recommended">
                            <strong>
                                <asp:Label ID="requestReceivedLabel" runat="server" CBID="622" Text="Request Received">
                                </asp:Label></strong>
                            <asp:Label ID="dateTimeLabel" runat="server"></asp:Label>
                        </div>
                        <div class="checkbox">
                            <asp:CheckBox ID="automatedRenewalEmailCheckBox" runat="server" ViewStateMode="Enabled"
                                CBID="1112" Text="Send automated renewal emails at 60, 30, 2 days prior to expiration" /></div>
                        <div id="paymentDiv" class="box" runat="server">
                            <asp:UpdatePanel ID="sentPaymentRequestUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <h3>
                                        <asp:Label ID="paymentRequestLabel" runat="server" Text="Payment Request"></asp:Label>
                                    </h3>
                                    <div class="box-content">
                                        <table cellpadding="0" cellspacing="0" id="sentPaymentRequestTable" runat="server"
                                            class="send-detail">
                                            <tr>
                                                <td class="col1">
                                                    <asp:Label ID="sentLabel" runat="server" CBID="668" Text="Sent:"></asp:Label>
                                                </td>
                                                <td class="col2">
                                                    <asp:Label ID="sentDateLabel" runat="server"></asp:Label>
                                                </td>
                                                <td class="col3">
                                                    <asp:Label ID="sentToEmailLabel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <h4>
                                            <asp:Label ID="sendPayRequestLabel" runat="server" CBID="664" Text="Send a Payment Request"></asp:Label></h4>
                                        <div class="row">
                                            <div class="label labelpartner">
                                                <asp:Label ID="toLabel" runat="server" CBID="720" Text="To:"></asp:Label></div>
                                            <div class="input textbox3  mandatory">
                                                <asp:TextBox ID="emailToTextBox" runat="server" MaxLength="128"></asp:TextBox><br />
                                                <asp:RequiredFieldValidator ID="emailToRequiredField" VMTI="10" SetFocusOnError="true"
                                                    runat="server" ErrorMessage="Please provide email address" ValidationGroup="SendPaymentRequest"
                                                    ControlToValidate="emailToTextBox" CssClass="errorText errorPadding" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="emailToRegularExpression" SetFocusOnError="true"
                                                    VMTI="11" runat="server" ErrorMessage="Correct the format" ControlToValidate="emailToTextBox"
                                                    CssClass="errorText errorPadding" Display="Dynamic" ValidationGroup="SendPaymentRequest"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="label labelpartner">
                                                <asp:Label ID="regularAmountLabel" runat="server" CBID="610" Text="Regular Amount:"></asp:Label></div>
                                            <div class="input textbox3 mandatory">
                                                <asp:TextBox ID="regAmountTextBox" runat="server" CausesValidation="true" MaxLength="20"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="regAmountRequiredField" VMTI="35" SetFocusOnError="true"
                                                    runat="server" ErrorMessage="Please provide regular amount" ValidationGroup="SendPaymentRequest"
                                                    ControlToValidate="regAmountTextBox" CssClass="errorText errorPadding" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regularAmountRegularExpression" VMTI="36" SetFocusOnError="true"
                                                    ValidationGroup="SendPaymentRequest" ControlToValidate="regAmountTextBox" CssClass="errorText errorPadding"
                                                    Display="Dynamic" runat="server" ErrorMessage="Please enter amount upto 2 decimals"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="label labelpartner">
                                                <asp:Label ID="goldAmountLabel" runat="server" CBID="409" Text="Gold Amount:"></asp:Label></div>
                                            <div class="input textbox3 mandatory">
                                                <asp:TextBox ID="goldAmountTextBox" runat="server" CausesValidation="true" MaxLength="20"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="goldAmountRequiredField" VMTI="35" SetFocusOnError="true"
                                                    runat="server" ErrorMessage="Please provide gold amount" ValidationGroup="SendPaymentRequest"
                                                    ControlToValidate="goldAmountTextBox" CssClass="errorText errorPadding" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="goldAmountRegularExpression" VMTI="36" SetFocusOnError="true"
                                                    ValidationGroup="SendPaymentRequest" ControlToValidate="goldAmountTextBox" CssClass="errorText errorPadding"
                                                    Display="Dynamic" runat="server" ErrorMessage="Please enter amount upto 2 decimals"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="alR pad-top">
                                            <span class="btn-style">
                                                <asp:LinkButton CBID="644" ValidationGroup="SendPaymentRequest" ID="savePaymentDetailsLinkButton"
                                                    OnClick="SendPaymentRequestLinkButton_Click" runat="server"> </asp:LinkButton></span>
                                        </div>
                                        <div class="clear">
                                            &nbsp;
                                        </div>
                                        <br />
                                        <div id="sentPaymentRequestResultDiv" runat="server">
                                        </div>
                                        <div class="clear">
                                            &nbsp;
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div id="linked" class="box" runat="server">
                            <asp:UpdatePanel ID="linkedUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <h3>
                                        <asp:Label ID="linkWebAccountLabel" runat="server" Text="Linked Web Accounts"></asp:Label>
                                    </h3>
                                    <div class="box-content">
                                        <asp:GridView ID="linkWebAccountGrid" OnRowDataBound="LinkWebAccountGrid_RowDataBound"
                                            runat="server" AutoGenerateColumns="false" class="col-3" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="userNameLabel" runat="server" CssClass="col-1" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderStyle-CssClass="col-2" ItemStyle-CssClass="col-2" HeaderText="Hilton ID"
                                                    DataField="HiltonId" />
                                                <asp:BoundField HeaderStyle-CssClass="col-3" ItemStyle-CssClass="col-3" HeaderText="Email"
                                                    DataField="Email" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="sendEmailLinkButton" runat="server" CssClass="errorText col-4"
                                                            Text="send activation email" OnClick="SendEmailLinkButton_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="cancelImage" ImageUrl="../Images/cross.gif" runat="server" OnClick="CancelImage_Click"
                                                            CommandArgument='<%# Bind("UserId") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <div class="add-field">
                                            <div class="mandatory">
                                                <asp:TextBox ID="addNameTextBox" onfocus="inputFocus(this)" onblur="inputBlur(this)"
                                                    Text="Name" CBID="449" runat="server" CssClass="add-field1" MaxLength="128"></asp:TextBox>
                                            </div>
                                            <asp:TextBox ID="addHiltonIdTextBox" runat="server" CssClass="add-field2" MaxLength="128"
                                                onfocus="inputFocus(this)" onblur="inputBlur(this)" CBID="415" Text="Hilton ID"></asp:TextBox>
                                            <div class="mandatory">
                                                <asp:TextBox ID="addEmailTextBox" runat="server" CssClass="add-field3" MaxLength="128"
                                                    onfocus="inputFocus(this)" onblur="inputBlur(this)" CBID="382" Text="Email address"></asp:TextBox>
                                            </div>
                                            <span class="btn-style">
                                                <asp:LinkButton ID="addLinkButton" ValidationGroup="linkWebAccount" runat="server"
                                                    OnClick="AddWebAccountLinkButton_Click" CBID="175"></asp:LinkButton></span>
                                        </div>
                                        <div class="add-field">
                                            <asp:RequiredFieldValidator ID="addNameRequiredFieldValidator" VMTI="37" SetFocusOnError="true"
                                                runat="server" ErrorMessage="Please enter name" ValidationGroup="linkWebAccount"
                                                ControlToValidate="addNameTextBox" InitialValue="Name" CssClass="errorText nopad-top"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="nameRegularExpressionValidator" SetFocusOnError="true"
                                                VMTI="4" runat="server" ControlToValidate="addNameTextBox" ErrorMessage="Please enter valid name"
                                                CssClass="errorText nopad-top" ValidationGroup="linkWebAccount" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <div class="clear">
                                                &nbsp;
                                            </div>
                                            <asp:Label ID="lastNameErrorLabel" AssociatedControlID="addNameTextBox" runat="server"
                                                CssClass="errorText nopad-top" Text="Enter last name" Visible="false"></asp:Label>
                                            <div class="clear">
                                            </div>
                                            <asp:RegularExpressionValidator ID="addEmailRegularExpressionValidator" runat="server"
                                                VMTI="11" SetFocusOnError="true" ErrorMessage=" Enter email Or Correct the format"
                                                ControlToValidate="addEmailTextBox" CssClass="errorText nopad-top" Display="Dynamic"
                                                ValidationGroup="linkWebAccount"></asp:RegularExpressionValidator>
                                            <asp:Label ID="hiltonIdFormatErrorLabel" runat="server" CssClass="errorText" Text="Please enter valid Hilton Id"
                                                Visible="false"></asp:Label>
                                        </div>
                                        <div>
                                            <div id="linkWebAccountErrorMessageDiv" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div id="viewNavigationDiv" runat="server">
                            <span class="btn-style">
                                <asp:LinkButton ID="viewPartnerProductLinkButton" runat="server" OnClick="ViewPartnerProductLinkButton_Click"
                                    CBID="740">
                                </asp:LinkButton></span>
                            <br />
                            <br />
                            <span class="btn-style">
                                <asp:LinkButton ID="viewPartnerTransHistoryLinkButton" runat="server" OnClick="ViewPartnerTransHistoryLinkButton_Click"
                                    CBID="741">
                                </asp:LinkButton></span>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div>
                </div>
            </div>
        </div>
        <div id="no-bg" class="wrapper">
            <asp:UpdatePanel ID="mappedCategoryUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="panels">
                        <h2>
                            <asp:Label ID="categoriesLabel" runat="server" CBID="787" Text="Categories"></asp:Label></h2>
                    </div>
                    <%--<asp:UpdatePanel ID="categoriesMappingUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="categoryMappingResultDiv" runat="server">
                        </div>
                        <br />
                    </ContentTemplate>
                </asp:UpdatePanel>--%>
                    <asp:Repeater ID="categoryRepater" runat="server" OnItemDataBound="CategoryRepeater_ItemDataBound">
                        <ItemTemplate>
                            <div class="accordion-1">
                                <h3 class="active">
                                    <%# DataBinder.Eval(Container.DataItem, "CategoryDisplayName")%></h3>
                                <div class="acc-content">
                                    <asp:Repeater ID="detailsRepeater" runat="server" OnItemDataBound="DetailsRepeater_ItemDataBound">
                                        <HeaderTemplate>
                                            <table width="100%" cellspacing="0" cellpadding="0" class="field-one">
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="col3">
                                                    <asp:Label ID="brandLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BrandDescription")%>'></asp:Label>
                                                </td>
                                                <td class="col3">
                                                    <asp:Label ID="regionLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RegionDescription") %>'></asp:Label>
                                                    <asp:GridView ID="mappedCountriesGridView" BorderColor="0" runat="server" AutoGenerateColumns="false"
                                                        ShowHeader="false" Width="80%" GridLines="None" OnRowDataBound="MappedCountryGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="CountryName" ItemStyle-Width="81%" />
                                                            <asp:TemplateField ItemStyle-Width="19%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="removeCategoryCountryLinkButton" runat="server" OnCommand="RemoveCategoryCountryLinkButton_Command"><img src="../Images/cross.gif" alt=""/></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                                <%-- <td class="col3">
                                                    <asp:GridView ID="propertyTypeGridView" BorderColor="0" runat="server" AutoGenerateColumns="false"
                                                        ShowHeader="false" Width="70%" GridLines="None" OnRowDataBound="PropertyGridView_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="PropertyTypeDescription" ItemStyle-Width="81%" />
                                                            <asp:TemplateField ItemStyle-Width="19%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="deletePropertyLinkButton" runat="server" OnCommand="RemovePropertyTypeLinkButton_Command"><img src="../Images/cross.gif" alt=""></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>--%>
                                                <td class="col3">
                                                    <asp:UpdatePanel ID="mappedCategoryUpdatePanel1" runat="server" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="availableDropDown" AutoPostBack="true" CssClass="list-4" runat="server"
                                                                OnSelectedIndexChanged="AvailableDropDown_SelectedIndexChanged">
                                                                <asp:ListItem Value="1">Available</asp:ListItem>
                                                                <asp:ListItem Value="2">Not Available</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="availableDropDownHiddenField" runat="server" />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="availableDropDown" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td class="col1">
                                                    <asp:LinkButton ID="removeCategoryLinkButton" OnCommand="RemoveCategoryLinkButton_Command"
                                                        runat="server" Text="Remove Category" CBID="615" CssClass="btn"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody> </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="chooseCategoryUpdatePanel" ViewStateMode="Enabled" runat="server"
                UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="selectedParentCategories" EventName="TextChanged" />
                    <%--<asp:AsyncPostBackTrigger ControlID="selectedBrands" EventName="TextChanged" />--%>
                    <asp:AsyncPostBackTrigger ControlID="selectedRegions" EventName="TextChanged" />
                    <asp:AsyncPostBackTrigger ControlID="selectedCountries" EventName="TextChanged" />
                </Triggers>
                <ContentTemplate>
                    <table class="field-two add-shadow">
                        <tbody>
                            <tr>
                                <td class="col3">
                                    <div>
                                        <asp:DropDownList ID="optgroup" runat="server" class="list-5" multiple="multiple"
                                            ClientIDMode="Static">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                                        <asp:HiddenField ID="isCheckAll" runat="server" ClientIDMode="Static" />
                                        <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display: none;"
                                            AutoPostBack="false"></asp:TextBox>
                                        <div class="warning">
                                            <asp:RequiredFieldValidator ID="brandValidator" Enabled="false" VMTI="31" SetFocusOnError="true"
                                                runat="server" ErrorMessage="Please select a brand" ValidationGroup="AddCategory"
                                                ControlToValidate="optgroup" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="categoryDropDown" ClientIDMode="Static" runat="server" class="list-5"
                                            ViewStateMode="Enabled" multiple="multiple">
                                        </asp:DropDownList>
                                        <asp:TextBox runat="server" ID="selectedParentCategories" ClientIDMode="Static" Style="display: none;"
                                            AutoPostBack="true" OnTextChanged="SelectedParentCategories_TextChanged"></asp:TextBox>
                                        <div class="warning">
                                            <asp:RequiredFieldValidator ID="categoryValidator" VMTI="27" SetFocusOnError="true"
                                                runat="server" ErrorMessage="Please select a category" ValidationGroup="AddCategory"
                                                ControlToValidate="categoryDropDown" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        <asp:PlaceHolder ID="parentCategoryContainer" ClientIDMode="Static" runat="server">
                                        </asp:PlaceHolder>
                                        <div id="DynamicCategoryDiv" runat="server" clientidmode="Static" />
                                    </div>
                                </td>
                                <td class="col3">
                                    <div>
                                        <asp:DropDownList ID="categoryRegionDropDown" runat="server" class="list-6" multiple="multiple"
                                            ClientIDMode="Static">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="isAllRegionChecked" runat="server" ClientIDMode="Static" Value="1" />
                                        <asp:TextBox runat="server" ID="selectedRegions" ClientIDMode="Static" Style="display: none;"
                                            AutoPostBack="true" OnTextChanged="SelectedRegions_TextChanged"></asp:TextBox>
                                        <br />
                                        <br />
                                        <asp:RequiredFieldValidator ID="regionRequiredFieldValidator" Enabled="false" runat="server"
                                            VMTI="30" SetFocusOnError="true" ErrorMessage="Please select a region" ValidationGroup="AddCategory"
                                            ControlToValidate="categoryRegionDropDown" InitialValue="0" CssClass="errorText"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                   
                                    <div>
                                        <asp:GridView ID="tobeMappedCountriesGridView" runat="server" AutoGenerateColumns="false"
                                            ShowHeader="false" BorderStyle="None" GridLines="None">
                                            <Columns>
                                                <asp:BoundField DataField="DataTextField" />
                                                <asp:TemplateField>
                                                    <ItemStyle CssClass="third-col" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="cancelImage" ImageUrl="../Images/cross.gif" CommandArgument='<%# Convert.ToString(DataBinder.Eval(Container.DataItem, "DataValueField")) %>'
                                                            runat="server" OnCommand="RemoveToBeMappedCountryLinkButton_Command" CausesValidation="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="area-dropdown list-c">
                                        <asp:DropDownList ID="categoryCountryDropDown" runat="server" multiple="multiple"
                                            ClientIDMode="Static">
                                        </asp:DropDownList>
                                        <asp:TextBox runat="server" ID="selectedCountries" ClientIDMode="Static" Style="display: none;"
                                            AutoPostBack="true" OnTextChanged="SelectedCountries_TextChanged"></asp:TextBox>
                                    </div>
                                </td>
                                <%-- <td class="col3">
                                    <div>
                                        <asp:CheckBox Text="All Types" runat="server" ID="allTypesCheckBox" AutoPostBack="true" visible="false"
                                            OnCheckedChanged="AllPropertiesCheckBox_CheckedChanged" />
                                        <asp:CheckBoxList CssClass="types width-full" ID="propertyTypeCheckList" OnSelectedIndexChanged="PropertyCheckBox_SelectedIndexChanged"
                                            AutoPostBack="true" runat="server" RepeatColumns="2" visible= "false" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                    </div>
                                    <asp:Label ID="propertyErrorMessageLabel" CssClass="errorText" runat="server" />
                                </td>--%>
                                <td class="add-btn">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="addCategoryLinkButton" Text = "Add" runat="server" CssClass="add" ValidationGroup="AddCategory"
                                            OnClick="AddCategoryLinkButton_Click" CBID="175"></asp:LinkButton></span>
                                 &nbsp;
                                    <span class="btn-style" runat="server">
                                        <asp:LinkButton ID="deletCategoryLinkButton1"  runat="server" Text="Delete" ValidationGroup="AddCategory"
                                            OnClick="DeleteCategoryLinkButton_Click" CBID="0"></asp:LinkButton>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CustomValidator ID="categoryAdditionValidator" runat="server" ValidationGroup="AddCategory"
                                        ControlToValidate="categoryRegionDropDown" CssClass="errorText" Display="Dynamic"
                                        OnServerValidate="CategoryAdditionValidator_ServerValidate"></asp:CustomValidator>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <asp:UpdatePanel ID="categoriesMappingUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="categoryMappingResultDiv" runat="server" visible="false">
                            </div>
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="text-align: right; padding-bottom: 8px; padding-right: 10px; background-color: rgb(209, 209, 209);">
                        <span class="btn-style">
                            <asp:LinkButton ID="SaveButton2" CBID="644" Text="Save" OnClick="SaveLinkButton_Click"
                                runat="server" ValidationGroup="SavePartner"></asp:LinkButton>
                    </div>
                    </span>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
