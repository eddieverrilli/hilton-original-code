﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="ProjectDetails.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ProjectDetails"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Globalization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" ViewStateMode="Enabled"
    runat="server">
    <asp:ScriptManager ID="userDetailsScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript" src="../Scripts/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../Scripts/custominputs.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            SetFileInputStyles();
        });

        function SetFileInputStyles() {
            $("input.customInput").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 65
            });

            $("input.customInput1").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 65
            });
        }

        function ajaxFileUpload(control, fileType) {
            // debugger;
            if (fileType == 'image') {
                var imageName = $('#projectImageUpload')[0].value.toString().substring($('#projectImageUpload')[0].value.toString().lastIndexOf('\\') + 1, $('#projectImageUpload')[0].value.length);
                $('#<%= imageNameHiddenField.ClientID %>').val(imageName);
            }

            $("#loading")
    .ajaxStart(function () {
        $(this).show();
    })
    .ajaxComplete(function () {
        $(this).hide();
    });

            $.ajaxFileUpload
                    (
                        {
                            url: '../Handlers/FileUpload.ashx?fileType=' + fileType,
                            secureuri: false,
                            fileElementId: control,
                            dataType: 'json',
                            data: { name: 'login', id: 'id' },
                            success: function (data, status) {
                                //debugger;
                                if (status == 'success' && data.responseText != null && data.responseText == 'uploaded') {
                                    SetFileLink(fileType);

                                    if (fileType == 'image') {
                                        $.ajax({
                                            type: "POST",
                                            url: "../Handlers/FileUpload.ashx?fileType=" + fileType + "&op=show",
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (data, img) {
                                                //debugger;

                                                $('#<%= projectImage.ClientID %>').attr('src', "data:image/png;base64," + data);
                                            },
                                            error: function (data, status, e) {

                                            }
                                        });

                                    }

                                }
                                else {
                                    ValidateFile(fileType, data.responseText);
                                }

                            },
                            error: function (data, status, e) {

                            }
                        }
                    )

            return false;

        }

        function SetFileLink(fileType) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text('');
                    $('#imageErrSpan').css({ 'display': 'none' });
                    $('#<%= imageLinkButton.ClientID %>').text($('#<%= imageNameHiddenField.ClientID %>').val());
                    if ($('#imageNameHiddenField.ClientID').val() != '') {
                        $('#<%= removeImageLinkButton.ClientID %>').css({ 'display': 'inline' });
                    }
                    break;

            }
        }

        function ValidateFile(fileType, msg) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text(msg);
                    $('#imageErrSpan').css({ 'display': 'inline' });
                    $('#<%= removeImageLinkButton.ClientID %>').css({ 'display': 'none' });
                    $('#<%= imageNameHiddenField.ClientID %>').val("");
                    $('#<%= imageLinkButton.ClientID %>').text("");
                    $('#<%= projectImage.ClientID %>').attr('src', '../Images/NoProduct.PNG');
                    break;
            }

        }
        function inputFocus(i) {
            if (i.value == 'Name' || i.value == 'Hilton ID' || i.value == 'Email address') {
                i.value = ""; i.style.color = "#000";
            }
        }
        function inputBlur(i) {
            if (i.value == "") {
                if ($(i)[0].id.indexOf('nameTextBox') != -1) {
                    $(i).val("Name");
                    i.defaultValue = "Name";
                }
                else if ($(i)[0].id.indexOf('hiltonIdTextBox') != -1) {
                    $(i).val("Hilton ID");
                    i.defaultValue = "Hilton ID";
                }
                else if ($(i)[0].id.indexOf('emailTextBox') != -1) {
                    $(i).val("Email address");
                    i.defaultValue = "Email address";
                }
                i.style.color = "#888";
            }

        }

        function ToggleCategory(catid, rootParentId) {


            var parentdiv = $("." + catid + "p");
            var a = parentdiv.find('.collapsetoggle');
            if (!a.hasClass("up") && rootParentId == "-1") {
                var allChildDiv = $("." + catid + "r");
                allChildDiv.hide();
                a.toggleClass('up');
            }
            else {
                var div = $("." + catid);
                div.toggle();

                var a = parentdiv.find('.collapsetoggle');
                a.toggleClass('up');
            }

        }

      
    </script>
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../Images/search-top-curve.gif" alt="" /></div>
            <div class="middle">
                <asp:HyperLink ID="backToProjectsHyperLink" runat="server" NavigateUrl="~/Admin/Projects.aspx"
                    Text="Back to Projects"></asp:HyperLink></div>
            <div class="bottom-curve">
                <img src="../Images/search-bottom-curve.gif" alt="" /></div>
        </div>
    </div>
    <div class="content">
        <asp:HiddenField ID="projectIdHiddenField" runat="server" />
        <asp:HiddenField ID="templateIdHiddenField" runat="server" />
        <div class="wrapper">
            <h2>
                <asp:Label ID="projConfLabel" runat="server" Text="Project Details" CBID="546"></asp:Label></h2>
            <div id="basic-details" class="inner-content">
                <div class="lft-col">
                    <div class="box1">
                        <h3>
                            <asp:Label ID="basicDetailsLabel" runat="server" Text="Basic Details" CBID="247"></asp:Label></h3>
                        <div class="row">
                            <div>
                                <asp:Label ID="facilityIDLabel" runat="server" Text="Facility ID:" CBID="395" CssClass="label"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:Label ID="facilityIdLbl" runat="server"></asp:Label></div>
                        </div>
                        <div class="row">
                            <asp:Label ID="projectTemplateLabel" runat="server" Text="Project Template:" CssClass="label"
                                CBID="966"></asp:Label>
                            <div class="input">
                                <asp:Label ID="projectTemplateDetailsLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <asp:Label ID="brandLabel" runat="server" Text="Brand:" CssClass="label" CBID="262"></asp:Label>
                            <div class="input">
                                <asp:Label ID="brandDescriptionLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <asp:Label ID="brandSegmentLabel" runat="server" Text="BrandSegment:" CssClass="label"
                                CBID="0"></asp:Label>
                            <div class="input">
                                <asp:Label ID="brandSegmentDescriptionLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <asp:Label ID="regionLabel" runat="server" Text="Region:" CssClass="label" CBID="599"></asp:Label>
                            <div class="input">
                                <asp:Label ID="regionDescriptionLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <asp:Label ID="countryLabel" runat="server" Text="Country:" CssClass="label" CBID="1300"></asp:Label>
                            <div class="input">
                                <asp:Label ID="countryDescriptionLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="projTypeLabel" runat="server" Text="Project Type:" CssClass="label"
                                    CBID="554"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:Label ID="projectTypeLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" visible="false">
                            <asp:Label ID="propertyTypeLabel" runat="server" Visible="false" Text="Property Type:"
                                CssClass="label" CBID="571"></asp:Label>
                            <div class="input">
                                <asp:Label ID="propertyTypeDescriptionLabel" Visible="false" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="o2oStatusLabel" runat="server" Text="O2O Status:" CssClass="label"
                                    CBID="472"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="suppConnStatusLabel" runat="server" Text="Supp. Conn. Status:" CssClass="label"
                                    CBID="646"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:DropDownList ID="supplierConnStatusDropDownlist" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="sCCompletionLabel" runat="server" Text="S.C. % Completion:" CssClass="label"
                                    CBID="967"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:Label ID="sCCompletionLbl" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="locationLabel" runat="server" Text="Location:" CssClass="label" CBID="426"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:Label ID="locationLbl" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="ownerLabel" runat="server" Text="Owner:" CssClass="label" CBID="479"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:Label ID="ownerLbl" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="featuredLbl" runat="server" Text="Featured:" CssClass="label" CBID="968"></asp:Label>
                            </div>
                            <div class="input small">
                                <asp:DropDownList ID="featuredDropDownlist" runat="server">
                                    <asp:ListItem Value="True">True</asp:ListItem>
                                    <asp:ListItem Value="False">False</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="featuredImageLabel" runat="server" Text="Featured Image:" CssClass="label"
                                    CBID="161"></asp:Label>
                            </div>
                            <asp:UpdatePanel ID="imageUploadUpdatePanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="input image">
                                        <p class="imageload">
                                            <img id="loading" src="../Images/async.gif" class="loadingimg" />
                                            <asp:Image ID="projectImage" runat="server" CssClass="pic imagestyle" /></p>
                                        <div class="fileinputs">
                                            <input type="file" size="5px" id="projectImageUpload" class="customInput customStyle"
                                                name="projectImage1" onchange="return ajaxFileUpload('projectImageUpload','image');" />
                                        </div>
                                        <div class="imgsrc">
                                            <asp:LinkButton ID="imageLinkButton" CssClass="errorText" runat="server" ViewStateMode="Enabled"
                                                CausesValidation="false" OnClick="ImageLinkButton_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="removeImageLinkButton" OnClick="RemoveImageLinkButton_Click"
                                                runat="server" Style="margin-left: 15px; display: none" CausesValidation="false"
                                                Text="X" CBID="753" CssClass="errorText"></asp:LinkButton>
                                        </div>
                                        <span id="imageErrSpan" class="errorText" style="display: none"></span>
                                        <input type="hidden" id="imageNameHiddenField" runat="server" value="" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="imageLinkButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="clear">
                            </div>
                            <div class="form-spec input">
                                <asp:Label ID="companyLogoSizeDescription" runat="server" Text="Images should be JPG or PNG, resized to 235x235 pixels"
                                    CBID="1128"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div class="label">
                                <asp:Label ID="featuredDescriptionLabel" runat="server" Text="Featured Description:"
                                    CssClass="label" CBID="160"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:TextBox ID="featuredDesciptionTexbox" TextMode="MultiLine" runat="server" MaxLength="256"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="section alR pad-top">
                        <span class="btn-style">
                            <asp:LinkButton ID="saveLinkButton" runat="server" CssClass="save" OnClick="SaveLinkButton_Click"
                                CBID="640"></asp:LinkButton>
                        </span>
                    </div>
                    <asp:UpdatePanel ID="projectDetailsUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="projectDetailsResultDiv" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="rgt-col">
                    <div id="users" class="box">
                        <h3>
                            <asp:Label ID="usersLabel" runat="server" Text="Users" CssClass="label"></asp:Label>
                        </h3>
                        <div class="box-content">
                            <div id="usersDiv" runat="server">
                                <asp:GridView ID="gridviewUsers" runat="server" AutoGenerateColumns="false" class="col-3"
                                    OnRowDataBound="gridviewUsers_RowDataBound" DataKeyNames="UserId" OnRowCommand="GridviewUsers_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="@">
                                            <ItemTemplate>
                                                <asp:Image ID="RecCommCheckbox" runat="server" ImageUrl="~/Images/tick.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Name" DataField="Name" ItemStyle-CssClass="col-2" />
                                        <asp:BoundField HeaderText="Email" DataField="Email" ItemStyle-CssClass="col-3" />
                                        <asp:BoundField HeaderText="HiltonID" DataField="HiltonUserName" ItemStyle-CssClass="col-4" />
                                        <asp:BoundField HeaderText="Role" DataField="UserTypeDescription" ItemStyle-CssClass="col-5" />
                                        <asp:ButtonField ButtonType="Image" ImageUrl="../Images/cross.gif" CommandName="DeleteUser"
                                            ItemStyle-CssClass="col-6" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="add-field">
                                <asp:CheckBox ID="RecCommCheckbox" runat="server" />
                                <div class="mandatory">
                                    <asp:TextBox ID="nameTextBox" runat="server" class="textbox5" onfocus="inputFocus(this)"
                                        onblur="inputBlur(this)" Text="Name" MaxLength="101"></asp:TextBox>
                                </div>
                                <div class="mandatory">
                                    <asp:TextBox ID="emailTextBox" runat="server" class="textbox6" onfocus="inputFocus(this)"
                                        onblur="inputBlur(this)" Text="Email address" InitialValue="Email address" MaxLength="100"></asp:TextBox>
                                </div>
                                <asp:TextBox ID="hiltonIdTextBox" runat="server" class="textbox7" onfocus="inputFocus(this)"
                                    onblur="inputBlur(this)" Text="Hilton ID" InitialValue="Hilton ID" MaxLength="123"></asp:TextBox>
                                <asp:DropDownList ID="userRoleDropDownlist" runat="server" class="list-7">
                                </asp:DropDownList>
                                <span class="btn-style">
                                    <asp:LinkButton ID="addLinkButton" runat="server" OnClick="addLinkButton_Click" Text="Add"
                                        CBID="174" ValidationGroup="addUser"></asp:LinkButton>
                                </span>
                            </div>
                            <div style="padding-left: 22px">
                                <div>
                                    <asp:RequiredFieldValidator ID="nameReqFVal" runat="server" ControlToValidate="nameTextBox"
                                        InitialValue="Name" ValidationGroup="addUser" CssClass="errorText" Display="Dynamic"
                                        ErrorMessage="Please enter first Name" VMTI="37" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <asp:Label ID="lastNameErrorLabel" runat="server" AssociatedControlID="nameTextBox"
                                        CssClass="errorText" Text="Please enter last name" Visible="false"></asp:Label>
                                </div>
                                <div>
                                    <asp:RegularExpressionValidator ID="hiltonIdRegularExpression" VMTI="40" ViewStateMode="Enabled"
                                        ControlToValidate="hiltonIdTextBox" ValidationGroup="addUser" CssClass="errorText"
                                        Display="Dynamic" runat="server" ErrorMessage="Please enter valid Hilton Id"></asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <asp:RegularExpressionValidator ID="regexEmailValid" VMTI="11" runat="server" ControlToValidate="emailTextBox"
                                        ValidationGroup="addUser" CssClass="errorText" Display="Dynamic" ErrorMessage="Please enter valid Email"
                                        SetFocusOnError="true"></asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <asp:CompareValidator ID="userRoleCampVal" runat="server" VMTI="42" ErrorMessage="Please select a Role"
                                        ControlToValidate="userRoleDropDownlist" ValueToCompare="0" Operator="NotEqual"
                                        Display="Dynamic" ValidationGroup="addUser" CssClass="errorText" SetFocusOnError="true"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="categoryAdditionResultUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="userAdditionResultDiv" runat="server">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="box box2" id="history">
                        <h3>
                            <asp:Label ID="updateHistoryLabel" runat="server" Text="Update History" CssClass="label"></asp:Label>
                        </h3>
                        <div class="box-content">
                            <div id="updateHistoryDiv" runat="server">
                                <asp:GridView ID="gridviewUpdateHistory" runat="server" AutoGenerateColumns="false"
                                    class="col-3" ShowHeader="false">
                                    <Columns>
                                        <asp:BoundField HeaderText="Date" DataField="ModifiedDate" DataFormatString="{0:dd-MMM-yyyy}"
                                            ItemStyle-CssClass="col-1" />
                                        <asp:BoundField HeaderText="Time" DataField="ModifiedTime" ItemStyle-CssClass="col-2" />
                                        <asp:BoundField HeaderText="Name" DataField="Name" ItemStyle-CssClass="col-3" />
                                        <asp:BoundField HeaderText="Role" DataField="UserTypeDescription" ItemStyle-CssClass="col-4" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="resultUpdateHistoryDiv" runat="server">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="wrapper">
            <asp:UpdatePanel ID="leafCatPartnerProductResultUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="resultLeafCategoryDiv" runat="server">
                    </div>
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="panels">
                <asp:Button ID="submitButton" runat="server" Text="Save" OnClick="SubmitButton_Click"
                    Style="cursor: pointer" CBID="640" CssClass="btn" />
                <h2>
                    <asp:Label ID="projectDetailsLabel" runat="server" Text="Project Categories" CBID="545"></asp:Label>
                </h2>
            </div>
            <div class="inner-content">
                <div class="no-space">
                    <table border="0" cellpadding="0" cellspacing="0" class="project-detail">
                        <tr>
                            <th class="col-1">
                                <asp:Label ID="featuredLabel" runat="server" Text="Featured?" CBID="968"></asp:Label>
                            </th>
                            <th class="col-2">
                                <asp:Label ID="categoryLabel" runat="server" Text="Category" CBID="288"></asp:Label>
                            </th>
                            <th class="col-3">
                                <asp:Label ID="partnerLabel" runat="server" Text="Partner" CBID="484"></asp:Label>
                            </th>
                            <th class="col-4">
                                <asp:Label ID="productLabel" runat="server" Text="Product" CBID="524"></asp:Label>
                            </th>
                            <th class="col-6">
                                <asp:Label ID="requiredLabel" runat="server" Text="Required?" CBID="626"></asp:Label>
                            </th>
                        </tr>
                    </table>
                    <asp:Repeater ID="categoryRepeator" runat="server" OnItemDataBound="CategoryRepeator_ItemDataBound">
                        <ItemTemplate>
                            <div class="project-level1" runat="server" id="CatDiv">
                                <table border="0" cellpadding="0" cellspacing="0" class="project-detail">
                                    <tr>
                                        <td class="col-1">
                                            <asp:Label class='sprite down collapsetoggle up' Width="25px" Height="25px" runat="Server"
                                                ID="ToggleImage" />
                                            <asp:CheckBox ID="seletedChk" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Isfeatured")%>'
                                                Visible="false" />
                                        </td>
                                        <td class="col-2">
                                            <asp:Label ID="catLabel" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "CategoryName")%>'
                                                Width="250px"></asp:Label>
                                            <asp:Label ID="categoryIdLabel" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "CategoryId")%>'
                                                Width="250px" Style="display: none"></asp:Label>
                                        </td>
                                        <td class="col-3">
                                            <asp:Label ID="projectConfigDetailsIdLabel" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ProjectConfigDetailsId")%>'
                                                Width="250px" Style="display: none"></asp:Label>
                                        </td>
                                        <td class="col-4">
                                            <asp:Label ID="projectConfigurationIdLabel" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ProjectConfigurationId")%>'
                                                Width="250px" Style="display: none"></asp:Label>
                                            <asp:Label ID="isLeafLabel" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "IsLeaf")%>'
                                                Width="250px" Style="display: none"></asp:Label>
                                        </td>
                                        <%--<td class="col-5">
                                            &nbsp;
                                        </td>--%>
                                        <td class="col-6">
                                            <span class="col-3">
                                                <asp:DropDownList runat="server" ID="requiredDropDownlist" CssClass="list-9" Visible="false">
                                                    <asp:ListItem Value="False">No</asp:ListItem>
                                                    <asp:ListItem Value="True">Yes</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Repeater ID="productPartnerRepeator" runat="server" OnItemDataBound="ProductPartnerRepeator_ItemDataBound">
                                    <HeaderTemplate>
                                        <div class="project-level3">
                                            <table border="0" cellpadding="0" cellspacing="0" class="project-detail">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="col-2">
                                                <asp:Label ID="catLabel" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "ProjectLeafCategoryDetailsId")%>'
                                                    Width="250px" Style="display: none"></asp:Label>
                                                <asp:Label ID="catIdLabel" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "CategoryId")%>'
                                                    Width="250px" Style="display: none"></asp:Label>
                                            </td>
                                            <td class="col-1">
                                                <%--<asp:CheckBox ID="seletedChk" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Isfeatured")%>' />--%>
                                            </td>
                                            <td class="col-3">
                                                <asp:DropDownList ID="partnerDropDownList" runat="server" class="list-8" AutoPostBack="true"
                                                    OnSelectedIndexChanged="PartnerDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="col-4">
                                                <span class="col-3">
                                                    <asp:DropDownList ID="productDropDownList" runat="server" class="list-8">
                                                    </asp:DropDownList>
                                                </span>
                                            </td>
                                            <%--  <td class="col-5">
                                                            &nbsp;
                                                        </td>--%>
                                            <td class="col-6">
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table></div></FooterTemplate>
                                </asp:Repeater>
                                </td></tr> </table>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
