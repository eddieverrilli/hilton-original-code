﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ProjectTemplateDetails : PageBase
    {
        private IProjectTemplateManager _projectTemplateManager;
        private ArrayList checkboxList;
        private Entities.ProjectTemplate projectTemplate;

        /// <summary>
        /// Refresh the drop down lists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               
               // ProcessDropDownItemChange(DropDownConstants.RegionDropDown, brandDropDown, regionDropDown, propertyTypeDropDown);
                countryDropDown.SelectedIndex = 0;
                ProcessDropDownItemChangeforCountry(DropDownConstants.RegionDropDown, brandDropDown.SelectedValue, regionDropDown, countryDropDown, propertyTypeDropDown);
                brandDropDown.Items[0].Text = "Select a Brand";
                regionDropDown.Items[0].Text = "Select a Region";
                propertyTypeDropDown.Items[0].Text = "Select a Property Type";
                countryDropDown.Items[0].Text = "Select a Country";
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

      
        /// <summary>
        /// Refresh the drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BrandDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProcessDropDownItemChange(DropDownConstants.BrandDropDown, brandDropDown, regionDropDown, propertyTypeDropDown);
                brandDropDown.Items[0].Text = "Select a Brand";
                regionDropDown.Items[0].Text = "Select a Region";
                propertyTypeDropDown.Items[0].Text = "Select a Property Type"; 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Creates a division of products
        /// </summary>
        /// <param name="dtProducts"></param>
        /// <returns></returns>
        private HtmlGenericControl CreateProductGrid(string parentId)
        {
            try
            {
                IEnumerable<CategoryPartnerProduct> productCollection = new List<CategoryPartnerProduct>();
                foreach (Category category in projectTemplate.Categories)
                {
                    productCollection = category.PartnerProducts.Where(p => p.CategoryId.ToString(CultureInfo.InvariantCulture) == parentId);
                    if (productCollection.Count() > 0)
                    {
                        break;
                    }
                }
                HtmlGenericControl productsDivision = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                productsDivision.Attributes.Add("class", "cat-level " + parentId);
                productsDivision.Style.Add("display", "none");
               // productsDivision.Attributes.Add("class", parentId);

                GridView productGrid = new GridView();
                productGrid.DataSource = productCollection;
                productGrid.AutoGenerateColumns = false;
                productGrid.CssClass = "sub-cat";
                productGrid.BorderStyle = 0;
                productGrid.DataKeyNames = new string[] { "PartnerId", "ProductId" };

                BoundField partnerNameBoundField = new BoundField();
                partnerNameBoundField.DataField = "PartnerName";
                productGrid.Columns.Add(partnerNameBoundField);
                productGrid.Columns[0].ItemStyle.CssClass = "sub-col-1";

                BoundField productNameBoundField = new BoundField();
                productNameBoundField.DataField = "ProductName";
                productGrid.Columns.Add(productNameBoundField);
                productGrid.Columns[1].ItemStyle.CssClass = "sub-col-2";

                BoundField productStatusBoundField = new BoundField();
                productStatusBoundField.DataField = "IsActive";
                productGrid.Columns.Add(productStatusBoundField);
                productGrid.Columns[2].ItemStyle.CssClass = "sub-col-3";

                HyperLinkField editProductsHyperLink = new HyperLinkField();
                editProductsHyperLink.Text = "edit";
                productGrid.Columns.Add(editProductsHyperLink);
                productGrid.Columns[3].ItemStyle.CssClass = "edit-btns";
                productGrid.Columns[3].ControlStyle.CssClass = "thickbox";
                productGrid.RowDataBound += new GridViewRowEventHandler(this.ProductGrid_RowDataBound);
                productGrid.DataBind();
                if (productGrid.HeaderRow != null)
                {
                    productGrid.HeaderRow.Visible = false;
                }
                // Check for the status of products, if InActive then apply the required css
                for (int nRowCount = 0; nRowCount < productGrid.Rows.Count; nRowCount++)
                {
                    if (Convert.ToBoolean(productGrid.Rows[nRowCount].Cells[2].Text, CultureInfo.InvariantCulture))
                    {
                        productGrid.Rows[nRowCount].Cells[2].Text = "Active";
                    }
                    else
                    {
                        productGrid.Rows[nRowCount].Cells[2].Text = "InActive";
                        productGrid.Rows[nRowCount].CssClass = "cat-level inactive";
                    }
                }
                productsDivision.Controls.Add(productGrid);
                return productsDivision;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }

        /// <summary>
        ///event when status of a category is changed by check/uncheck of a category check box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ActiveCheckedChanged(object sender, EventArgs args)
        {
            try
            {
                if (hidWhichChkBoxClicked.Value != "all")
                {
                    CheckBox activeCheck = (CheckBox)sender;
                    string dirtyCategoryId = activeCheck.ID.Split('_')[1];

                    bool isSuccess = false;
                    int projectTemplateId = Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture);
                    isSuccess = _projectTemplateManager.UpdateCategoryStatus(projectTemplateId, Convert.ToInt32(dirtyCategoryId, CultureInfo.InvariantCulture), activeCheck.Checked);
                    if (isSuccess)
                    {
                        checkboxList.Clear();
                        IList<Category> categoryList = GetCategoryConfigurationList();

                        if (categoryList != null)
                        {
                            isSuccess = _projectTemplateManager.UpdateCategoryListConfig(projectTemplateId, categoryList);
                            if (isSuccess)
                            {
                                 SetPageSettings();
                            }
                            else
                            {
                                ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.CategorySuccessfullyConfigured, culture));
                            }
                        }
                        else
                        {
                            SetPageSettings();
                        }
                        ScriptManager.RegisterClientScriptBlock(categoryListUpdatePanel, this.GetType(), "ActiveChkClicked", "ActiveChkClicked();", true);
                    }
                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        protected void isActiveAllCheck_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (hidWhichChkBoxClicked.Value == "all")
                {
                    CheckBox activeCheck = (CheckBox)sender;
                        if (activeCheck.Checked)
                        {
                            String[] strUnCheckedIds = hidactiveCheckUnCheckedIds.Value.Split(',');
                            bool isSuccess = false;
                            int projectTemplateId = Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture);
                            isSuccess = _projectTemplateManager.UpdateCategoryStatusForMultipleCategories(projectTemplateId, hidactiveCheckUnCheckedIds.Value, activeCheck.Checked);
                            if (isSuccess)
                            {
                                checkboxList.Clear();
                                IList<Category> categoryList = GetCategoryConfigurationList();

                                if (categoryList != null)
                                {
                                    isSuccess = _projectTemplateManager.UpdateCategoryListConfig(projectTemplateId, categoryList);
                                    if (isSuccess)
                                    {
                                        SetPageSettings();
                                    }
                                }
                            }
                            //activeCheck.Checked = true;
                            ScriptManager.RegisterClientScriptBlock(categoryListUpdatePanel, this.GetType(), "Check_OnCheckAllClick", "Check_OnCheckAllClick('check')", true);
                        }
                        else
                        {
                            String[] strCheckedIds = hidactiveCheckCheckedIds.Value.Split(',');
                            bool isSuccess = false;
                            int projectTemplateId = Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture);
                            isSuccess = _projectTemplateManager.UpdateCategoryStatusForMultipleCategories(projectTemplateId, hidactiveCheckCheckedIds.Value, activeCheck.Checked);
                            if (isSuccess)
                            {
                                checkboxList.Clear();
                                IList<Category> categoryList = GetCategoryConfigurationList();

                                if (categoryList != null)
                                {
                                    isSuccess = _projectTemplateManager.UpdateCategoryListConfig(projectTemplateId, categoryList);
                                    if (isSuccess)
                                    {
                                        SetPageSettings();
                                    }
                                }
                            }
                            //activeCheck.Checked = false;
                            ScriptManager.RegisterClientScriptBlock(categoryListUpdatePanel, this.GetType(), "Check_OnCheckAllClick", "Check_OnCheckAllClick('uncheck')", true);
                        }
                }
                hidWhichChkBoxClicked.Value = "";
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        
     
        /// <summary>
        /// repeatedly creates the divisions to dispaly the hierarchy of the categories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryDivisionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HtmlGenericControl categoryDivision = (HtmlGenericControl)e.Item.FindControl("categoryListDivision");
                    HtmlTable categoryDetailsTable = new HtmlTable();
                    Panel subcategoryPanel = new Panel();
                    if ((Convert.ToInt32(((Entities.Category)(e.Item.DataItem)).ParentCategoryId)) == -1)
                    {
                        if (((Entities.Category)(e.Item.DataItem)).IsLeaf)
                        {
                            if (((Entities.Category)(e.Item.DataItem)).HasProducts) // leaf category with products
                            {
                                categoryDetailsTable = CreateCategoryWithProductsDivision((Entities.Category)(e.Item.DataItem));
                                categoryDivision.Controls.Add(categoryDetailsTable);

                                HtmlGenericControl productDivision = CreateProductGrid(((Entities.Category)(e.Item.DataItem)).CategoryId.ToString(CultureInfo.InvariantCulture));
                                if (productDivision != null)
                                    categoryDivision.Controls.Add(productDivision);
                            }
                            else // absolutely a new category with no products
                            {
                                categoryDetailsTable = CreateNewCategoryDivision((Entities.Category)(e.Item.DataItem));
                                categoryDivision.Controls.Add(categoryDetailsTable);
                            }
                            

                        }
                        else
                        {
                            //category with subcategories below it.
                            subcategoryPanel = CreateMidLevelCategoryDivision((Entities.Category)(e.Item.DataItem));
                            categoryDivision.Controls.Add(subcategoryPanel);
                            
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///triggers export to excel functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExportToExcelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                int projectTemplateId = Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture);
                DataSet projectTemplateDataSet = _projectTemplateManager.ExportProjectTemplate(projectTemplateId);
                int maxLevlel = Convert.ToInt32(projectTemplateDataSet.Tables[1].AsEnumerable().Max(p => p["Level"]));
                ExportToExcel(projectTemplateDataSet, maxLevlel);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Get the instance of the manager class
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ValidateUserAccess((int)MenuEnum.ProjectTemplates);

            checkboxList = new ArrayList();
            _projectTemplateManager = ProjectTemplateManager.Instance;
            this.Title = ResourceUtility.GetLocalizedString(1101, culture, "Project Template Details");
        }

        /// <summary>
        /// Populate the drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            IList<SearchFilters> searchFilters = _projectTemplateManager.GetProjectTemplateSearchFilterItems(currentUser.UserId);
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilters);

            BindDropDown(searchFilters, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(1237, culture, "Select a Brand"), null);
            BindDropDown(searchFilters, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(1236, culture, "Select a Property Type"), null);
            BindDropDown(searchFilters, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(0, culture, "Select a Region"), null);
            BindDropDown(searchFilters, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(0, culture, "Select a Country"), null);
        }

        /// <summary>
        ///loads and populates the page details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;
                  

                    if (!Page.IsPostBack)
                    {
                        PopulateDropDownData();
                        SetPageSettings();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(projectTemplateUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                    }
                    if (GetSession<string>(SessionConstants.TemplateOperationResult) != null)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message success", GetSession<string>(SessionConstants.TemplateOperationResult));
                        ClearSession(SessionConstants.TemplateOperationResult);

                        MaintainPageState();
                    }

                    if (Request["__EVENTTARGET"] != null && !(Request["__EVENTTARGET"].ToString(CultureInfo.InvariantCulture).Contains("brandDropDown") || Request["__EVENTTARGET"].ToString(CultureInfo.InvariantCulture).Contains("regionDropDown") || Request["__EVENTTARGET"].ToString(CultureInfo.InvariantCulture).Contains("propertyTypeDropDown") || Request["__EVENTTARGET"].ToString(CultureInfo.InvariantCulture).Contains("btnGo")
                        || Request["__EVENTTARGET"].ToString(CultureInfo.InvariantCulture).Contains("exportToExcelLinkButton"))  )
                    {
                        SetPageSettings();
                    }
                    
                    templateInfoUpdatePanel.Update();

                    ScriptManager.RegisterClientScriptBlock(templateInfoUpdatePanel, this.GetType(), "ForceCall", "forceCall();", true);
                    ScriptManager.RegisterClientScriptBlock(categoryListUpdatePanel, this.GetType(), "moreLess", "more_lessItems()", true);
                }
               // isActiveAllCheck.Attributes.Add("onclick", "javascript:return isActiveAllCheckClicked()");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            //}
        }

        /// <summary>
        /// .NET will refuse to accept "unknown" post backs for security reasons.
        /// Because of this we have to register all possible callbacks.
        /// This must be done in Render, hence the override for selecting row on mouse click
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            Page.ClientScript.RegisterForEventValidation(
                    new System.Web.UI.PostBackOptions(
                        propertyTypeDropDown));

            // Do the standard rendering stuff
            base.Render(writer);
        }

        /// <summary>
        /// This method will maintain page state when user redirect from content block details screen
        /// </summary>
        private void MaintainPageState()
        {
            TemplateSearch templateSearch = GetSession<TemplateSearch>(SessionConstants.TemplateDetailSearch);

            if (templateSearch != null)
            {
                PolulateSearchCriteria(templateSearch);
            }
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        private void PolulateSearchCriteria(TemplateSearch templateSearch)
        {
            if (!string.IsNullOrEmpty(templateSearch.RegionId))
            {
                regionDropDown.SelectedValue = templateSearch.RegionId;
                ProcessDropDownItemChange(DropDownConstants.RegionDropDown, brandDropDown, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(templateSearch.BrandId))
            {
                brandDropDown.SelectedValue = templateSearch.BrandId;
                ProcessDropDownItemChange(DropDownConstants.BrandDropDown, brandDropDown, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(templateSearch.PropertyTypeId))
            {
                propertyTypeDropDown.SelectedValue = templateSearch.PropertyTypeId;
                ProcessDropDownItemChange(DropDownConstants.PropertyTypesDropDown, brandDropDown, regionDropDown, propertyTypeDropDown);
            }
        }

        /// <summary>
        /// Invoked when go button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void GoButton_Click(object sender, EventArgs args)
        {
            try
            {
                TemplateSearch templateSearch = PopulateSearchCriteria();
                templateSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                SearchTemplateList(templateSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will search the content block based on specified criteria
        /// </summary>
        /// <param name="contentSearch"></param>
        private void SearchTemplateList(TemplateSearch templateSearch)
        {
            int totalRecordCount = 0;

            templateSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            if (templateSearch.PageIndex == -1)
            {
                templateSearch.PageIndex = 0;
            }

            IList<ProjectTemplate> templateList = _projectTemplateManager.GetProjectTemplateList(templateSearch, ref totalRecordCount);

            //Save criteria in session for back button functionality
            ClearSession(SessionConstants.TemplateDetailSearch);
            SetSession<TemplateSearch>(SessionConstants.TemplateDetailSearch, templateSearch);

            if (templateList.Count == 0)
            {
               
                ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoProjectTemplateFoundForCriteria));
              
            }
            else
            {
                int projectTemplateId = templateList.First().TemplateId;
                SetSession<string>(SessionConstants.ProjectTemplateId, projectTemplateId.ToString());
                SetHiddenFieldValue(selectedProjectTemplateHiddenValue, projectTemplateId.ToString(CultureInfo.InvariantCulture));

                PopulateTemplateDetails();
                templateInfoUpdatePanel.Update();
            }
            //   BindTemplateList(templateList);
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        /// <returns></returns>
        private TemplateSearch PopulateSearchCriteria()
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            TemplateSearch templateSearch = new TemplateSearch()
            {
                UserId = currentUser.UserId,
                RegionId = (regionDropDown.SelectedIndex == 0) ? string.Empty : regionDropDown.SelectedValue,
                CountryId = (countryDropDown.SelectedIndex == 0) ? string.Empty : countryDropDown.SelectedValue,
                BrandId = (brandDropDown.SelectedIndex == 0) ? string.Empty : brandDropDown.SelectedValue,
                PropertyTypeId = (propertyTypeDropDown.SelectedIndex == 0) ? string.Empty : propertyTypeDropDown.SelectedValue,
                PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture),
                PageIndex = 0,
            };
            return templateSearch;
        }

        /// <summary>
        ///sets the navigation to edit partner / product page on click of edit button of a product
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProductGrid_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                string categoryId, templateId, partnerId, productId = string.Empty;
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    if (((CategoryPartnerProduct)(args.Row.DataItem)).ProductId == -1)
                    {
                        args.Row.Cells[1].Text = "-";
                    }
                    templateId = selectedProjectTemplateHiddenValue.Value;
                    categoryId = ((CategoryPartnerProduct)(args.Row.DataItem)).CategoryId.ToString(CultureInfo.InvariantCulture);
                    partnerId = ((CategoryPartnerProduct)(args.Row.DataItem)).PartnerId.ToString(CultureInfo.InvariantCulture);
                    productId = ((CategoryPartnerProduct)(args.Row.DataItem)).ProductId.ToString(CultureInfo.InvariantCulture);
                    ((HyperLink)(args.Row.Cells[3].Controls[0])).NavigateUrl = "~/Admin/AddEditPartnerProduct.aspx?keepThis=true&cat=" + categoryId + "&mode=edit&pt=" + templateId.ToString(CultureInfo.InvariantCulture) + "&partner=" + partnerId + "&product=" + productId + "&TB_iframe=true&height=600&width=400";
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///saves the changes of a project template list of categories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void SaveProjectTemplateLinkButton_Command(object sender, CommandEventArgs args)
        {
            try
            {
                checkboxList.Clear();
                IList<Category> categoryList = GetCategoryConfigurationList();
                int projectTemplateId = Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture);
                if (categoryList != null)
                {
                    bool isSuccess = _projectTemplateManager.UpdateCategoryListConfig(projectTemplateId, categoryList);
                    if (isSuccess)
                    {
                        SetPageSettings();
                        ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.CategorySuccessfullyConfigured, culture));
                        projectTemplateUpdatePanel.Update();
                        ScriptManager.RegisterClientScriptBlock(templateInfoUpdatePanel, this.GetType(), "UpdateAllFourAllChk", "UpdateAllFourAllChk();", true);
                    }
                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///sets the initial data for page load
        /// </summary>
        /// <param name="projectTemplate"></param>
        private void BindData(ProjectTemplate projectTemplate)
        {
            try
            {
                //propertyTypeLiteral.Text = projectTemplate.PropertyTypeDescription;
                brandLiteral.Text = projectTemplate.BrandName;
                regionLiteral.Text = projectTemplate.RegionDescription;
                countryLiteral.Text = projectTemplate.CountryName;
                IList<Category> rootCategoryList = (projectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).OrderBy(p => p.Level).ThenBy(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture)).ToList();
                categoryDivisionsRepeater.DataSource = rootCategoryList;
                categoryDivisionsRepeater.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// </summary>
        /// creates the division of leaf level categories with the product grid
        /// <param name="category"></param>
        /// <returns></returns>
        /// <summary>
        private HtmlTable CreateCategoryWithProductsDivision(Category category)
        {
            try
            {
                checkboxList.Clear();
                string categoryName = category.CategoryName;
                string categoryLevel = category.Level.ToString(CultureInfo.InvariantCulture);
                string categoryID = category.CategoryId.ToString(CultureInfo.InvariantCulture);
                bool isActive = category.IsActive;
                bool? isRequired = category.IsRequired;
                bool? isProductRequired = category.IsNewProductsReq;
                bool? isPartnerRequired = category.IsNewPartnersRequired;

                HtmlTable categoryDetailsTable = new HtmlTable();
                categoryDetailsTable.Attributes.Add("class", "cat-table");

               

                
                categoryDetailsTable.CellPadding = 0;
                categoryDetailsTable.CellSpacing = 0;
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell;
                string cssClassName;

                for (int colCount = 1; colCount <= 5; colCount++)
                {
                    cell = new HtmlTableCell();
                    cssClassName = "col-";
                    cssClassName = cssClassName + colCount.ToString(CultureInfo.InvariantCulture);
                    cell.Attributes.Add("class", cssClassName);
                    if (colCount == 1)
                    {
                        HtmlGenericControl divEditButtons = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        divEditButtons.Attributes.Add("class", "edit-btns");
                        HyperLink addProductsLinkButton = CreateProductLinkButton(categoryID);
                        HyperLink editLinkButton = CreateEditLinkButton(categoryID);
                        divEditButtons.Controls.Add(addProductsLinkButton);
                        divEditButtons.Controls.Add(editLinkButton);
                        HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H5");
                        int marginFromLeft = (Convert.ToInt32(categoryLevel, CultureInfo.InvariantCulture) - 1) * 30;
                        headingCategoryName.Style.Add("margin-left", marginFromLeft.ToString(CultureInfo.InvariantCulture) + "px");

                        // To display the standards against a category
                        var standardData = category.CategoryStandards.Where(p => p.CategoryId == category.CategoryId);
                        Repeater standardReperater = new Repeater();
                        standardReperater.DataSource = standardData.Where(p => !string.IsNullOrWhiteSpace(p.StandardNumber));
                        standardReperater.DataBind();

                        foreach (RepeaterItem repeatItem in standardReperater.Items)
                        {
                            RepeaterItem repeaterItem = new RepeaterItem(repeatItem.ItemIndex, ListItemType.Item);
                            Label standardLabel = new Label();
                            if (repeatItem.ItemIndex == 0)
                            {
                                standardLabel.Text = " ( ";
                            }
                            standardLabel.Text += standardData.ToList()[repeatItem.ItemIndex].StandardNumber;
                            if (standardReperater.Items.Count == 1)
                            {
                                standardLabel.Text += " ) ";
                            }
                            if (repeaterItem.ItemIndex != standardReperater.Items.Count - 1)
                            {
                                standardLabel.Text += ", ";
                            }
                            standardLabel.ToolTip = standardData.ToList()[repeatItem.ItemIndex].StandardDescription;
                            repeatItem.Controls.Add(standardLabel);
                        }

                        headingCategoryName.InnerText = categoryName;
                        headingCategoryName.Controls.Add(standardReperater);

                        cell.Controls.Add(divEditButtons);
                        cell.Controls.Add(headingCategoryName);
                    }
                    else if (colCount == 2)
                    {
                        CheckBox chkActive = new CheckBox();
                        cell.Controls.Add(chkActive);
                        chkActive.Enabled = false;
                        chkActive.AutoPostBack = true;
                        chkActive.ID = "activeCheck_" + categoryID;
                        chkActive.CheckedChanged += new EventHandler(ActiveCheckedChanged);
                        if (isActive)
                            chkActive.Checked = true;
                        if (category.IsEnabled)
                            chkActive.Enabled = true;
                    }
                    else if (colCount == 3)
                    {
                        CheckBox chkRequired = new CheckBox();
                        cell.Controls.Add(chkRequired);
                        chkRequired.ID = "requiredCheck_" + categoryID;
                        chkRequired.Attributes.Add("onclick", "return requiredChkClicked();");
                        if (isRequired.HasValue && isRequired.Value == true)
                            chkRequired.Checked = true;
                    }
                    else if (colCount == 4)
                    {
                        CheckBox chkNewPartnerRequired = new CheckBox();
                        cell.Controls.Add(chkNewPartnerRequired);
                        chkNewPartnerRequired.ID = "newPartnerCheck_" + categoryID;
                        chkNewPartnerRequired.Attributes.Add("onclick", "return newPartnerChkClicked();");
                        if (isPartnerRequired.HasValue && isPartnerRequired.Value == true)
                            chkNewPartnerRequired.Checked = true;
                    }
                    else if (colCount == 5)
                    {
                        CheckBox chkNewProductRequired = new CheckBox();
                        cell.Controls.Add(chkNewProductRequired);
                        chkNewProductRequired.ID = "newProductCheck_" + categoryID;
                        chkNewProductRequired.Attributes.Add("onclick", "return newProductChkClicked();");
                        if (isProductRequired.HasValue && isProductRequired.Value == true)
                            chkNewProductRequired.Checked = true;
                    }

                    row.Cells.Add(cell);
                }
                categoryDetailsTable.Rows.Add(row);
                if (!isActive)
                {
                    categoryDetailsTable.Attributes.Clear();
                    categoryDetailsTable.Attributes.Add("class", "cat-table inactive");
                }
                else
                {
                    categoryDetailsTable.Attributes.Clear();
                    categoryDetailsTable.Attributes.Add("class", "cat-table");
                }

                categoryDetailsTable.Attributes.Add("onClick", "ToggleCategory('" + category.CategoryId + "')");
                categoryDetailsTable.Style.Add("cursor", "pointer");
                return categoryDetailsTable;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }

        /// <summary>
        ///creates edit link button dynamically for a category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        private HyperLink CreateEditLinkButton(string categoryId)
        {
            HyperLink editHyperLink = new HyperLink();
            int projectTemplateId = Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture);
            editHyperLink.Text = Utilities.ResourceUtility.GetLocalizedString(775, culture, "Edit");
            editHyperLink.CssClass = "thickbox";
            editHyperLink.NavigateUrl = "~/Admin/AddCategoryPopup.aspx?keepThis=true&cat=" + categoryId + "&mode=edit&pt=" + projectTemplateId.ToString(CultureInfo.InvariantCulture) + "&TB_iframe=true&height=580&width=400";
            return editHyperLink;
        }

        /// <summary>
        /// creates all the category divisions which have sub categories under it
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="categoryName"></param>
        /// <param name="categoryBrandStandardNumber"></param>
        /// <param name="categoryLevel"></param>
        /// <returns></returns>
        private Panel CreateMidLevelCategoryDivision(Category category)
        {
            try
            {
                checkboxList.Clear();
                Panel categoryPanel = new Panel();
                string categoryName = category.CategoryName;
                string categoryLevel = category.Level.ToString(CultureInfo.InvariantCulture);
                string categoryID = category.CategoryId.ToString(CultureInfo.InvariantCulture);
                bool isActive = category.IsActive;

                HtmlTable categoryDetailsTable = new HtmlTable();
                categoryDetailsTable.Attributes.Add("class", "cat-table");
                categoryDetailsTable.CellPadding = 0;
                categoryDetailsTable.CellSpacing = 0;
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell;
                string cssClassName;
                categoryPanel.ID = "panel_" + categoryID;

                for (int colCount = 1; colCount <= 5; colCount++)
                {
                    cell = new HtmlTableCell();
                    cssClassName = "col-";
                    cssClassName = cssClassName + colCount.ToString(CultureInfo.InvariantCulture);
                    cell.Attributes.Add("class", cssClassName);
                    if (colCount == 1)
                    {
                        HtmlGenericControl divEditButtons = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        divEditButtons.Attributes.Add("class", "edit-btns");
                        HyperLink addSubCatLinkButton = CreateSubCategoryLinkButton(categoryID);
                        HyperLink editLinkButton = CreateEditLinkButton(categoryID);
                        divEditButtons.Controls.Add(addSubCatLinkButton);
                        divEditButtons.Controls.Add(editLinkButton);
                        HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H5");

                        int marginFromLeft = (Convert.ToInt32(categoryLevel, CultureInfo.InvariantCulture) - 1) * 30;
                        headingCategoryName.Style.Add("margin-left", marginFromLeft.ToString(CultureInfo.InvariantCulture) + "px");

                        // To display the standards against a category
                        var standardData = category.CategoryStandards.Where(p => p.CategoryId == category.CategoryId);
                        Repeater standardReperater = new Repeater();
                        standardReperater.DataSource = standardData.Where(p => !string.IsNullOrWhiteSpace(p.StandardNumber));
                        standardReperater.DataBind();

                        foreach (RepeaterItem repeatItem in standardReperater.Items)
                        {
                            RepeaterItem repeaterItem = new RepeaterItem(repeatItem.ItemIndex, ListItemType.Item);
                            Label standardLabel = new Label();
                            if (repeatItem.ItemIndex == 0)
                            {
                                standardLabel.Text = " ( ";
                            }
                            standardLabel.Text += standardData.ToList()[repeatItem.ItemIndex].StandardNumber;
                            if (standardReperater.Items.Count == 1)
                            {
                                standardLabel.Text += " ) ";
                            }
                            if (repeaterItem.ItemIndex != standardReperater.Items.Count - 1)
                            {
                                standardLabel.Text += ", ";
                            }

                            standardLabel.ToolTip = standardData.ToList()[repeatItem.ItemIndex].StandardDescription;
                            repeatItem.Controls.Add(standardLabel);
                        }

                        headingCategoryName.InnerText = categoryName;
                        headingCategoryName.Controls.Add(standardReperater);

                        cell.Controls.Add(divEditButtons);
                        cell.Controls.Add(headingCategoryName);
                    }
                    else if (colCount == 2)
                    {
                        CheckBox chkActive = new CheckBox();
                        chkActive.Enabled = false;
                        cell.Controls.Add(chkActive);
                        chkActive.ID = "activeCheck_" + categoryID;
                        chkActive.AutoPostBack = true;
                        chkActive.CheckedChanged += new EventHandler(ActiveCheckedChanged);
                        if (isActive)
                            chkActive.Checked = true;
                        if (category.IsEnabled)
                            chkActive.Enabled = true;
                    }
                    row.Cells.Add(cell);
                }
                categoryDetailsTable.Rows.Add(row);
                if (!isActive)
                {
                    categoryDetailsTable.Attributes.Clear();
                    categoryDetailsTable.Attributes.Add("class", "cat-table inactive");
                }
                else
                {
                    categoryDetailsTable.Attributes.Clear();
                    categoryDetailsTable.Attributes.Add("class", "cat-table");
                }

                //categoryDetailsTable.Attributes.Add("style", "background-color: red");//monia
                categoryDetailsTable.Attributes.Add("class", "cat-table category-level1");
                categoryPanel.Controls.Add(categoryDetailsTable);

              

                IEnumerable<Category> subCategoryCollection = projectTemplate.Categories.Where(p => p.RootParentId == Convert.ToInt32(categoryID, CultureInfo.InvariantCulture)).OrderBy(p => p.Level).ThenBy(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture));
               string subCategories = string.Empty ;
             
                foreach (Category subcategory in subCategoryCollection)
                {
                    if (subcategory.Level.Equals(2))
                    {
                        subCategories = subCategories + subcategory.Level + "_" + subcategory.CategoryId + ",";
                    }
                                          
                    Panel subcategoryPanel = new Panel();
                    string subcategoryName = subcategory.CategoryName;
                    string subcategoryLevel = subcategory.Level.ToString(CultureInfo.InvariantCulture);
                    string subcategoryID = subcategory.CategoryId.ToString(CultureInfo.InvariantCulture);
                    bool subcategoryIsActive = subcategory.IsActive;

                    HtmlTable subcategoryDetailsTable = new HtmlTable();
                    subcategoryDetailsTable.Attributes.Add("class", "cat-table");
                    subcategoryDetailsTable.CellPadding = 0;
                    subcategoryDetailsTable.CellSpacing = 0;
                    HtmlTableRow subcategoryrow = new HtmlTableRow();
                    HtmlTableCell subcategorycell;
                    subcategoryPanel.ID = "panel_" + subcategoryID;

                    for (int colCount = 1; colCount <= 5; colCount++)
                    {
                        subcategorycell = new HtmlTableCell();
                        cssClassName = "col-";
                        cssClassName = cssClassName + colCount.ToString(CultureInfo.InvariantCulture);
                        subcategorycell.Attributes.Add("class", cssClassName);
                        if (colCount == 1)
                        {
                            HtmlGenericControl divEditButtons = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                            divEditButtons.Attributes.Add("class", "edit-btns");
                            HyperLink addSubCatLinkButton = CreateSubCategoryLinkButton(subcategoryID);
                            HyperLink editLinkButton = CreateEditLinkButton(subcategoryID);
                            divEditButtons.Controls.Add(addSubCatLinkButton);
                            divEditButtons.Controls.Add(editLinkButton);
                            HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H5");

                            int marginFromLeft = (Convert.ToInt32(subcategoryLevel, CultureInfo.InvariantCulture) - 1) * 30;
                            headingCategoryName.Style.Add("margin-left", marginFromLeft.ToString(CultureInfo.InvariantCulture) + "px");

                            // To display the standards against a category
                            var standardData = category.CategoryStandards.Where(p => p.CategoryId == category.CategoryId);
                            Repeater standardReperater = new Repeater();
                            standardReperater.DataSource = standardData.Where(p => !string.IsNullOrWhiteSpace(p.StandardNumber));
                            standardReperater.DataBind();

                            foreach (RepeaterItem repeatItem in standardReperater.Items)
                            {
                                RepeaterItem repeaterItem = new RepeaterItem(repeatItem.ItemIndex, ListItemType.Item);
                                Label standardLabel = new Label();
                                if (repeatItem.ItemIndex == 0)
                                {
                                    standardLabel.Text = " ( ";
                                }
                                standardLabel.Text += standardData.ToList()[repeatItem.ItemIndex].StandardNumber;
                                if (standardReperater.Items.Count == 1)
                                {
                                    standardLabel.Text += " ) ";
                                }
                                if (repeaterItem.ItemIndex != standardReperater.Items.Count - 1)
                                {
                                    standardLabel.Text += ", ";
                                }
                                standardLabel.ToolTip = standardData.ToList()[repeatItem.ItemIndex].StandardDescription;
                                repeatItem.Controls.Add(standardLabel);
                            }

                            headingCategoryName.InnerText = subcategoryName;
                            headingCategoryName.Controls.Add(standardReperater);

                            subcategorycell.Controls.Add(divEditButtons);
                            subcategorycell.Controls.Add(headingCategoryName);
                        }
                        else if (colCount == 2)
                        {
                            CheckBox chkActive = new CheckBox();
                            chkActive.Enabled = false;
                            subcategorycell.Controls.Add(chkActive);
                            chkActive.ID = "activeCheck_" + subcategoryID;
                            chkActive.AutoPostBack = true;
                            chkActive.CheckedChanged += new EventHandler(ActiveCheckedChanged);
                            if (subcategoryIsActive)
                                chkActive.Checked = true;
                            if (subcategory.IsEnabled)
                                chkActive.Enabled = true;
                        }
                        subcategoryrow.Cells.Add(subcategorycell);
                    }
                    subcategoryDetailsTable.Rows.Add(subcategoryrow);
                    if (!subcategoryIsActive)
                    {
                        subcategoryDetailsTable.Attributes.Clear();
                        subcategoryDetailsTable.Attributes.Add("class", "cat-table inactive");
                    }
                    else
                    {
                        subcategoryDetailsTable.Attributes.Clear();
                        subcategoryDetailsTable.Attributes.Add("class", "cat-table");
                    }
                   
                    subcategoryPanel.Style.Add("display", "none");
                    if (subcategory.Level ==2)
                    subcategoryPanel.Attributes.Add("class", category.CategoryId.ToString());
                    else
                        subcategoryPanel.Attributes.Add("class", subcategory.ParentCategoryId.ToString());

                    if (subcategory.IsLeaf)
                    {
                        if (subcategory.HasProducts) // leaf category with products
                        {
                            subcategoryDetailsTable = CreateCategoryWithProductsDivision(subcategory);
                            subcategoryPanel.Controls.Add(subcategoryDetailsTable);
                            HtmlGenericControl productDivision = CreateProductGrid(subcategoryID.ToString(CultureInfo.InvariantCulture));
                            if (productDivision != null)
                                subcategoryPanel.Controls.Add(productDivision);
                        }
                        else // absolutely a new category with no products
                        {
                            subcategoryDetailsTable = CreateNewCategoryDivision(subcategory);
                            subcategoryPanel.Controls.Add(subcategoryDetailsTable);
                        }
                    }
                    else
                    {
                        subcategoryPanel.Controls.Add(subcategoryDetailsTable);
                        subcategoryDetailsTable.Attributes.Add("onClick", "ToggleCategory('" + subcategory.CategoryId.ToString() + "')");
                        subcategoryDetailsTable.Style.Add("cursor", "pointer");
                    }
                    if (categoryPanel.ID.ToString(CultureInfo.InvariantCulture) == "panel_" + subcategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))
                    {
                        categoryPanel.Controls.Add(subcategoryPanel);
                    }
                    else //last;
                    {
                        Panel pnlParent = new Panel();
                        foreach (Control p in categoryPanel.Controls)
                        {
                            if (p is Panel)
                            {
                                pnlParent = (Panel)FindControl(categoryPanel, "panel_" + subcategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                if (pnlParent != null)
                                    pnlParent.Controls.Add(subcategoryPanel);
                            }
                        }

                   }
                }//end for of subcategory
              
                categoryDetailsTable.Attributes.Add("onClick", "ToggleCategory('" + category.CategoryId.ToString() + "')");
                categoryDetailsTable.Style.Add("cursor", "pointer");
               
                
                return categoryPanel;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }

        /// <summary>
        ///creates a category division for a newly added category with no products and no sub categories under them
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        private HtmlTable CreateNewCategoryDivision(Category category)
        {
            try
            {
                checkboxList.Clear();
                string categoryName = category.CategoryName;
                string categoryLevel = category.Level.ToString(CultureInfo.InvariantCulture);
                string categoryID = category.CategoryId.ToString(CultureInfo.InvariantCulture);
                bool isActive = category.IsActive;
                bool? isRequired = category.IsRequired;
                bool? isProductRequired = category.IsNewProductsReq;
                bool? isPartnerRequired = category.IsNewPartnersRequired;
                HtmlTable categoryDetailsTable = new HtmlTable();
                categoryDetailsTable.Attributes.Add("class", "cat-table");
                categoryDetailsTable.CellPadding = 0;
                categoryDetailsTable.CellSpacing = 0;
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell;
                string cssClassName;

                for (int colCount = 1; colCount <= 5; colCount++)
                {
                    cell = new HtmlTableCell();
                    cssClassName = "col-";
                    cssClassName = cssClassName + colCount.ToString(CultureInfo.InvariantCulture);
                    cell.Attributes.Add("class", cssClassName);
                    if (colCount == 1)
                    {
                        HtmlGenericControl divEditButtons = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        divEditButtons.Attributes.Add("class", "edit-btns");
                        HyperLink addSubCatLinkButton = CreateSubCategoryLinkButton(categoryID);
                        HyperLink addProductsLinkButton = CreateProductLinkButton(categoryID);
                        HyperLink editLinkButton = CreateEditLinkButton(categoryID);
                        divEditButtons.Controls.Add(addSubCatLinkButton);
                        divEditButtons.Controls.Add(addProductsLinkButton);
                        divEditButtons.Controls.Add(editLinkButton);
                        HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H5");
                        int marginFromLeft = (Convert.ToInt32(categoryLevel, CultureInfo.InvariantCulture) - 1) * 30;
                        headingCategoryName.Style.Add("margin-left", marginFromLeft.ToString(CultureInfo.InvariantCulture) + "px");

                        // To display the standards against a category
                        var standardData = category.CategoryStandards.Where(p => p.CategoryId == category.CategoryId);
                        Repeater standardReperater = new Repeater();
                        standardReperater.DataSource = standardData.Where(p => !string.IsNullOrWhiteSpace(p.StandardNumber));
                        standardReperater.DataBind();

                        foreach (RepeaterItem repeatItem in standardReperater.Items)
                        {
                            RepeaterItem repeaterItem = new RepeaterItem(repeatItem.ItemIndex, ListItemType.Item);
                            Label standardLabel = new Label();
                            if (repeatItem.ItemIndex == 0)
                            {
                                standardLabel.Text = " ( ";
                            }
                            standardLabel.Text += standardData.ToList()[repeatItem.ItemIndex].StandardNumber;
                            if (standardReperater.Items.Count == 1)
                            {
                                standardLabel.Text += " ) ";
                            }
                            if (repeaterItem.ItemIndex != standardReperater.Items.Count - 1)
                            {
                                standardLabel.Text += ", ";
                            }
                            standardLabel.ToolTip = standardData.ToList()[repeatItem.ItemIndex].StandardDescription;
                            repeatItem.Controls.Add(standardLabel);
                        }

                        headingCategoryName.InnerText = categoryName;
                        headingCategoryName.Controls.Add(standardReperater);

                        cell.Controls.Add(divEditButtons);
                        cell.Controls.Add(headingCategoryName);
                    }
                    else if (colCount == 2)
                    {
                        CheckBox chkActive = new CheckBox();
                        cell.Controls.Add(chkActive);
                        chkActive.Enabled = false;
                        chkActive.ID = "activeCheck_" + categoryID;
                        chkActive.AutoPostBack = true;
                        chkActive.CheckedChanged += new EventHandler(ActiveCheckedChanged);
                        if (isActive)
                            chkActive.Checked = true;
                        if (category.IsEnabled)
                            chkActive.Enabled = true;
                    }
                    else if (colCount == 3)
                    {
                        CheckBox chkRequired = new CheckBox();
                        cell.Controls.Add(chkRequired);
                        chkRequired.ID = "requiredCheck_" + categoryID;
                        chkRequired.Attributes.Add("onclick", "return requiredChkClicked();");
                        if (isRequired.HasValue && isRequired.Value == true)
                            chkRequired.Checked = true;
                    }
                    else if (colCount == 4)
                    {
                        CheckBox chkNewPartnerRequired = new CheckBox();
                        cell.Controls.Add(chkNewPartnerRequired);
                        chkNewPartnerRequired.ID = "newPartnerCheck_" + categoryID;
                        chkNewPartnerRequired.Attributes.Add("onclick", "return newPartnerChkClicked();");
                        if (isPartnerRequired.HasValue && isPartnerRequired.Value == true)
                            chkNewPartnerRequired.Checked = true;
                    }
                    else if (colCount == 5)
                    {
                        CheckBox chkNewProductRequired = new CheckBox();
                        cell.Controls.Add(chkNewProductRequired);
                        chkNewProductRequired.ID = "newProductCheck_" + categoryID;
                        chkNewProductRequired.Attributes.Add("onclick", "return newProductChkClicked();");
                        if (isProductRequired.HasValue && isProductRequired.Value == true)
                            chkNewProductRequired.Checked = true;
                    }

                    row.Cells.Add(cell);
                }
                categoryDetailsTable.Rows.Add(row);
                if (!isActive)
                {
                    categoryDetailsTable.Attributes.Clear();
                    categoryDetailsTable.Attributes.Add("class", "cat-table inactive");
                }
                else
                {
                    categoryDetailsTable.Attributes.Clear();
                    categoryDetailsTable.Attributes.Add("class", "cat-table");
                }

                 
                 //categoryDetailsTable.Attributes.Add("style", "background-color: green");//monia
                 categoryDetailsTable.Attributes.Add("class", "cat-table category-level2"); //monia
                return categoryDetailsTable;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }

        /// <summary>
        ///create 'Add partner product ' link button dynamically
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        private HyperLink CreateProductLinkButton(string categoryId)
        {
            HyperLink addProductsLinkButton = new HyperLink();
            int projectTemplateId = Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture);
            addProductsLinkButton.Text = ResourceUtility.GetLocalizedString(188, culture, "Add partner/product").ReplaceEmptyString();
            addProductsLinkButton.CssClass = "thickbox";
            addProductsLinkButton.NavigateUrl = "~/Admin/AddEditPartnerProduct.aspx?keepThis=true&cat=" + categoryId + "&mode=add&pt=" + projectTemplateId.ToString(CultureInfo.InvariantCulture) + "&TB_iframe=true&height=600&width=400";
            return addProductsLinkButton;
        }

        /// <summary>
        ///create 'add sub category' link button
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        private HyperLink CreateSubCategoryLinkButton(string categoryId)
        {
            HyperLink addSubCatLinkButton = new HyperLink();
            int projectTemplateId = Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture);
            addSubCatLinkButton.Text = Utilities.ResourceUtility.GetLocalizedString(192, culture, "Add Sub-Category").ReplaceEmptyString();
            addSubCatLinkButton.CssClass = "thickbox addcategory";
            addSubCatLinkButton.NavigateUrl = "~/Admin/AddCategoryPopup.aspx?keepThis=true&cat=" + categoryId + "&mode=add&pt=" + projectTemplateId.ToString(CultureInfo.InvariantCulture) + "&TB_iframe=true&height=580&width=400";
            return addSubCatLinkButton;
        }

        /// <summary>
        ///creates an excel to export the existing category list to excel
        /// </summary>
        /// <param name="projectTemplateDetailsDataSet"></param>
        private void ExportToExcel(DataSet projectTemplateDetailsDataSet, int MaxLevel)
        {
            try
            {
                string attach = UIConstants.ProjectTemplateDetailsExcelFileName;
                Response.ClearContent();
                Response.AddHeader("content-disposition", attach);
                Response.ContentType = UIConstants.ContentType;
                if (projectTemplateDetailsDataSet.Tables != null)
                {
                    GetLevelSpecificCategories(projectTemplateDetailsDataSet, MaxLevel);
                    Response.End();
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///writes the category list to excel
        /// </summary>
        /// <param name="categoryCollection"></param>
        /// <param name="categoryId"></param>
        private void ExportToExcel(DataSet categoryCollection, string categoryId, int MaxLevel)
        {
            try
            {
                DataTable levelSpecificCategoryTable = null;
                DataRow[] levelSpecificCategoryRowCollection = categoryCollection.Tables[1].Select("parentCategory=" + "'" + categoryId + "'").OrderBy(p => p["level"]).ThenBy(p => Convert.ToInt32(p["sequenceId"], CultureInfo.InvariantCulture)).ToArray(); ;
                if (levelSpecificCategoryRowCollection.GetLength(0) > 0)
                {
                    levelSpecificCategoryTable = levelSpecificCategoryRowCollection.CopyToDataTable();
                    foreach (DataRow categoryDetailsRow in levelSpecificCategoryTable.Rows)
                    {
                        int i = 1;
                        while (i < Convert.ToInt32(categoryDetailsRow["Level"], CultureInfo.InvariantCulture))
                        {
                            Response.Write("\t");
                            i = i + 1;
                        }
                        Response.Write(categoryDetailsRow["CategoryName"].ToString() + "\t");
                        int intTab = 0;

                        int k = 1;
                        while (k < ((MaxLevel - Convert.ToInt32(categoryDetailsRow["Level"], CultureInfo.InvariantCulture)) - intTab) + 3)
                        {
                            Response.Write("\t");
                            k = k + 1;
                        }
                        if (categoryDetailsRow["IsActive"].ToString().ToUpper() == "TRUE")
                            Response.Write("YES" + "\t");
                        else
                            Response.Write("NO" + "\t");

                        if (categoryDetailsRow["IsRequired"].ToString().ToUpper() == "TRUE")
                            Response.Write("YES" + "\t");
                        else
                            Response.Write("NO" + "\t");

                        if (categoryDetailsRow["IsNewPartnersReq"].ToString().ToUpper() == "TRUE")
                            Response.Write("YES" + "\t");
                        else
                            Response.Write("NO" + "\t");

                        if (categoryDetailsRow["IsNewProductsReq"].ToString().ToUpper() == "TRUE")
                            Response.Write("YES" + "\t");
                        else
                            Response.Write("NO" + "\t");

                        Response.Write("\n");
                        if (categoryDetailsRow["HasProducts"].ToString() == "True")
                            WriteProducts(categoryCollection.Tables[2], categoryDetailsRow["CategoryId"].ToString(), MaxLevel);
                        ExportToExcel(categoryCollection, categoryDetailsRow["CategoryId"].ToString(), MaxLevel);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///finds the occurance of all the check boxes
        /// </summary>
        /// <param name="control"></param>
        private void FindCheckboxes(Control control)
        {
            if (control is CheckBox)
            {
                CheckBox checkBox = control as CheckBox;
                checkboxList.Add(checkBox);
            }
            // Process the child controls
            if (control.HasControls())
            {
                foreach (Control child in control.Controls)
                {
                    FindCheckboxes(child);
                }
            }
        }

        /// <summary>
        ///stores the configuration of all categories and returns the list
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetCategoryConfigurationList()
        {
            try
            {
                IList<Category> categoryList = new List<Category>();
                Category category;
                NavigateControls(this.Page.Controls);
                for (int checkboxCount = 0; checkboxCount < checkboxList.Count; checkboxCount++)
                {
                    CheckBox checkbox = (CheckBox)(checkboxList[checkboxCount]);
                    if(checkbox.ID.Contains("_"))
                    {
                            string checkboxId = checkbox.ID;
                            string checkboxPrefix = ((string)checkbox.ID).Split('_')[0].ToString(CultureInfo.InvariantCulture);
                            string categoryId = ((string)checkbox.ID).Split('_')[1].ToString(CultureInfo.InvariantCulture);
                            if (checkboxPrefix.Equals("activeCheck", StringComparison.InvariantCulture))
                            {
                                category = new Category();
                                category.CategoryId = Convert.ToInt32(categoryId, CultureInfo.InvariantCulture);
                                category.IsActive = checkbox.Checked;
                                category.IsLeaf = ReadCategory(category.CategoryId.ToString(CultureInfo.InvariantCulture)).IsLeaf;

                                if (category.IsLeaf)
                                {
                                    int leafCount = 1;
                                    while (leafCount < 4)
                                    {
                                        checkbox = (CheckBox)(checkboxList[checkboxCount + leafCount]);
                                        checkboxId = checkbox.ID;
                                        checkboxPrefix = ((string)checkbox.ID).Split('_')[0].ToString(CultureInfo.InvariantCulture);
                                        switch (checkboxPrefix)
                                        {
                                            case "newPartnerCheck":
                                                category.IsNewPartnersRequired = checkbox.Checked;
                                                break;

                                            case "newProductCheck":
                                                category.IsNewProductsReq = checkbox.Checked;
                                                break;

                                            case "requiredCheck":
                                                category.IsRequired = checkbox.Checked;
                                                break;
                                        }
                                        leafCount++;
                                    }
                                    checkboxCount = checkboxCount + 3;
                                }
                                categoryList.Add(category);
                            }
                    }
                }
                return categoryList;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return null;
            }
        }

        /// <summary>
        ///gets the categories at given level and writes it to excel
        /// </summary>
        /// <param name="projectTemplateDetailsDataSet"></param>
        /// <param name="MaxLevel"></param>
        private void GetLevelSpecificCategories(DataSet projectTemplateDetailsDataSet, int MaxLevel)
        {
            try
            {
                WriteCategoryDetails(projectTemplateDetailsDataSet.Tables[0]);
                WriteHeaders(MaxLevel);
                DataRow[] projectTemplateDetailsDataRow = projectTemplateDetailsDataSet.Tables[1].Select("parentCategory=-1").OrderBy(p => p["level"]).ThenBy(p => p["sequenceId"]).ToArray();
                DataTable levelSpecificCategoryDetails = null;
                if (projectTemplateDetailsDataRow.GetLength(0) > 0)
                {
                    levelSpecificCategoryDetails = projectTemplateDetailsDataRow.CopyToDataTable();
                }
                foreach (DataRow categoryDetailsRow in levelSpecificCategoryDetails.Rows)
                {
                    Response.Write(categoryDetailsRow["CategoryName"].ToString() + "\t");
                    int k = 1;
                    while (k < (MaxLevel - Convert.ToInt32(categoryDetailsRow["Level"].ToString(), CultureInfo.InvariantCulture)) + 3)
                    {
                        Response.Write("\t");
                        k = k + 1;
                    }
                    if (categoryDetailsRow["IsActive"].ToString().ToUpper() == "TRUE")
                        Response.Write("YES" + "\t");
                    else
                        Response.Write("NO" + "\t");
                    Response.Write("\n");
                    if ((bool)categoryDetailsRow["HasProducts"])
                    {
                        WriteProducts(projectTemplateDetailsDataSet.Tables[2], categoryDetailsRow["CategoryId"].ToString(), MaxLevel);
                    }
                    ExportToExcel(projectTemplateDetailsDataSet, categoryDetailsRow["CategoryId"].ToString(), MaxLevel);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

       
        /// <summary>
        ///navigates all the controls generated dynamically in the page
        /// </summary>
        /// <param name="controls"></param>
        private void NavigateControls(ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                FindCheckboxes(control);
            }
        }

        /// <summary>
        ///returns the list of categories for a particular project template Id
        /// </summary>
        /// <param name="projectTemplateID"></param>
        private void PopulateProjectTemplateDetails(int projectTemplateID)
        {
            try
            {
                projectTemplate = _projectTemplateManager.GetProjectTemplateDetail(projectTemplateID);
                BindData(projectTemplate);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///reads the category details for a given category Id
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(string currentSelectedCategoryId)
        {
            Category category = (Category)(projectTemplate.Categories.SingleOrDefault(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)));
            if (category != null)
                return category;
            else
                return null;
        }

        /// <summary>
        /// sets teh page for teh first time
        /// </summary>
        private void SetPageSettings()
        {
            try
            {
                SetViewState(UIConstants.Mode, UIConstants.Edit);
                int projectTemplateId = -1;
                if (!(Page.IsPostBack))
                {
                    projectTemplateId = Convert.ToInt32(GetSession<string>(SessionConstants.ProjectTemplateId), CultureInfo.InvariantCulture);
                    SetHiddenFieldValue(selectedProjectTemplateHiddenValue, projectTemplateId.ToString(CultureInfo.InvariantCulture));
                }
                PopulateTemplateDetails();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        private void PopulateTemplateDetails()
        {
            PopulateProjectTemplateDetails(Convert.ToInt32(selectedProjectTemplateHiddenValue.Value, CultureInfo.InvariantCulture));

            categoryListUpdatePanel.Update();
        }

        /// <summary>
        ///writes thecategories in an excel
        /// </summary>
        /// <param name="dt"></param>
        private void WriteCategoryDetails(DataTable levelSpecificCategoryTable)
        {
            try
            {
                foreach (DataRow categoryDetailsRow in levelSpecificCategoryTable.Rows)
                {
                    for (int categoryDetailCount = 0; categoryDetailCount < levelSpecificCategoryTable.Columns.Count; categoryDetailCount++)
                    {
                        Response.Write(categoryDetailsRow[categoryDetailCount].ToString() + "\n");
                    }
                    Response.Write("\n");
                    Response.Write("\n");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///write the excel headers
        /// </summary>
        /// <param name="Maxlevel"></param>
        private void WriteHeaders(int MaxLevel)
        {
            try
            {
                Response.Write("Category" + "\t");
                int i = 1;
                while (i < MaxLevel)
                {
                    Response.Write("\t");
                    i = i + 1;
                }
         
                Response.Write("Partners" + "\t");

                Response.Write("Products" + "\t");

                Response.Write("Active?" + "\t");

                Response.Write("Required?" + "\t");

                Response.Write("New Partners?" + "\t");

                Response.Write("New Products?" + "\t");
                Response.Write("\n");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///write products for a category
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="catId"></param>
        private void WriteProducts(DataTable dt, string catId, int MaxLevel)
        {
            try
            {
                DataTable productCollection = null;
                DataRow[] d123 = dt.Select("catId=" + "'" + catId + "'");
                if (d123.GetLength(0) > 0)
                {
                    productCollection = d123.CopyToDataTable();
                    foreach (DataRow dr in productCollection.Rows)
                    {
                        int k = 1;
                        while (k <= MaxLevel)
                        {
                            Response.Write("\t");
                            k = k + 1;
                        }
                        Response.Write(dr["PartnerName"].ToString() + "\t");
                        Response.Write(dr["ProductName"].ToString() + "\t");
                        if (dr["ActiveStatus"].ToString().ToUpper() == "TRUE")
                            Response.Write("YES" + "\t");
                        else
                            Response.Write("NO" + "\t");
                        //}
                        Response.Write("\n");
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///writes the standards in the excel
        /// </summary>
        ///  <param name="dt"></param>
        ///  <param name="MaxLevel"></param>
        /// <param name="catId"></param>
        /// <param name="level"></param>
        private int WriteStandards(DataTable dt, string catId, int MaxLevel, int level)
        {
            try
            {
                DataTable productCollection = null;
                int intTab = 0;
                DataRow[] d123 = dt.Select("Category=" + "'" + catId + "'");
                if (d123.GetLength(0) > 0)
                {
                    productCollection = d123.CopyToDataTable();

                    int i = 0;
                    foreach (DataRow dr in productCollection.Rows)
                    {
                        i = i + 1;

                        int k = 1;
                        while (k < (MaxLevel - level) + 1)
                        {
                            Response.Write("\t");
                            k = k + 1;
                        }
                        if (i == 1)
                            Response.Write(dr["StandardDescription"].ToString() + "\t");

                        intTab = k;
                    }
                }
                return intTab;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                return -1;
            }
        } 
    }
}