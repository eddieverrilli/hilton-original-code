﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class TransactionList : PageBase
    {
        private ITransactionManager _transactionManager;
        private IHelperManager _helperManager;
        /// <summary>
        /// Search the transactions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoButton_Click(object sender, EventArgs e)
        {
            try
            {
                SearchTransactions();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Search the transactions
        /// </summary>
        private void SearchTransactions()
        {
            User user = GetSession<User>(SessionConstants.User);
            if (user != null)
            {
                validateDateCustomValidator.ServerValidate += new ServerValidateEventHandler(ValidateTransactionDate);
                validateDateCustomValidator.Validate();
                if (IsValid)
                {
                    TransactionSearch transactionSearch = BuildSearchCriteria(user);
                    transactionSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                    transactionSearch.SearchSource = UIConstants.ActivationGo;

                    GetTransactionList(transactionSearch);
                }
                else
                {
                    transactionListGridView.DataSource = null;
                    transactionListGridView.DataBind();

                    //Pagination Control Setting
                    pagingControl.TotalRecordCount = 0;
                    pagingControl.CurrentPageIndex = 0;
                    pagingControl.PageSet = 0;
                    pagingControl.AllPageClick = false;
                }
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                transactionSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchTransactions(transactionSearch);
                ClearSession(SessionConstants.UserSearch);
                SetSession<TransactionSearch>(SessionConstants.TransactionSearch, transactionSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                transactionSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchTransactions(transactionSearch);
                ClearSession(SessionConstants.UserSearch);
                SetSession<TransactionSearch>(SessionConstants.TransactionSearch, transactionSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method is invoked with search button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void QuickSearch_Click(object sender, EventArgs args)
        {
            try
            {
                User user = GetSession<User>(SessionConstants.User);
                if (user != null)
                {
                    TransactionSearch transactionSearch = BuildSearchCriteria(user);
                    transactionSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                    GetTransactionList(transactionSearch);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Validate the transaction date
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidateTransactionDate(object source, ServerValidateEventArgs args)
        {
            try
            {
                ValidateDate(args, validateDateCustomValidator, fromDateTextBox.Text, toDateTextBox.Text);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Get the instance of the manager class
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);            
            ValidateUserAccess((int)MenuEnum.Transactions);            
            _transactionManager = TransactionManager.Instance;
            _helperManager = HelperManager.Instance;
            SetGridViewHeaderText();
        }

        /// <summary>
        ///
        /// </summary>
        private void SetGridViewHeaderText()
        {
            transactionListGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(820, culture, "Date/Time");
            transactionListGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(821, culture, "Partner");
            transactionListGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(822, culture, "Contact Name");
            transactionListGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(823, culture, "Transaction ID");
            transactionListGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(824, culture, "Partner Type");
            transactionListGridView.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(825, culture, "Amount");
        }

        /// <summary>
        /// Bind the data to control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    this.Title = ResourceUtility.GetLocalizedString(1106, culture, "Transactions List");
                    resultMessageDiv.Visible = false;
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;
                    detailSectionDiv.Visible = false;

                    if (!Page.IsPostBack)
                    {
                        PopulateDropDownData();

                        if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("TransactionDetails.aspx") || Request.UrlReferrer.ToString().Contains("Transactions.aspx")))
                        {
                            MaintainPageState();
                        }
                        else if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("Partners.aspx") || Request.UrlReferrer.ToString().Contains("PartnerDetails.aspx")))
                        {
                            ClearSession(SessionConstants.TransactionSearch);
                            string partnerId = GetSession<string>(SessionConstants.PartnerId);
                            ClearSession(SessionConstants.PartnerId);
                            if (!string.IsNullOrEmpty(partnerId))
                            {
                                User user = GetSession<User>(SessionConstants.User);
                                if (user != null)
                                {
                                    partnerDropDownList.SelectedValue = partnerId;
                                    TransactionSearch transactionSearch = BuildSearchCriteria(user);
                                    transactionSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                                    transactionSearch.SearchSource = UIConstants.ActivationGo;

                                    GetTransactionList(transactionSearch);
                                }
                            }
                        }
                        else
                        {
                            ClearSession(SessionConstants.TransactionSearch);
                        }
                    }
                    else if (Request["__EVENTTARGET"] != null && Request["__EVENTTARGET"].ToString(CultureInfo.InvariantCulture).Contains("exportToExcelLinkButton"))
                    {
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "InitializeToggle", "InitializeToggle()",true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(tranasctionsUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will populate the drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            BindDynamicDropDown(DropDownConstants.PartnerDropDown, partnerDropDownList, ResourceUtility.GetLocalizedString(1215, culture, "All Partners"), null);
            BindStaticDropDown(DropDownConstants.PartnershipStatusDropDown, partnershipStatusDropDown, ResourceUtility.GetLocalizedString(1230, culture, "All Status"), null);
            BindStaticDropDown(DropDownConstants.TransactionStatusDropDown, transactionStatusDropDown, ResourceUtility.GetLocalizedString(1230, culture, "All Status"), null);
            BindStaticDropDown(DropDownConstants.PartnershipTypeDropDown, partnerTypeDropDown, ResourceUtility.GetLocalizedString(1230, culture, "All Status"), null);
        }

        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                transactionSearch.PageIndex = pagingControl.CurrentPageIndex;
                transactionSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                transactionSearch.AllClick = pagingControl.AllPageClick;
                SearchTransactions(transactionSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                transactionSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchTransactions(transactionSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                transactionSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchTransactions(transactionSearch);
                ClearSession(SessionConstants.UserSearch);
                SetSession<TransactionSearch>(SessionConstants.TransactionSearch, transactionSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                transactionSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchTransactions(transactionSearch);
                SetSession<TransactionSearch>(SessionConstants.TransactionSearch, transactionSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        private void PolulateSearchCriteria(TransactionSearch transactionSearch)
        {
            if (!string.IsNullOrEmpty(transactionSearch.TransactionStatusId))
                transactionStatusDropDown.SelectedValue = transactionSearch.TransactionStatusId;

            if (!string.IsNullOrEmpty(transactionSearch.PartnerId))
                partnerDropDownList.SelectedValue = transactionSearch.PartnerId;

            if (!string.IsNullOrEmpty(transactionSearch.PartnerStatusId))
                partnershipStatusDropDown.SelectedValue = transactionSearch.PartnerStatusId;

            if (!string.IsNullOrEmpty(transactionSearch.QuickSearchExpression))
                searchTextBox.Text = transactionSearch.QuickSearchExpression;

            if (transactionSearch.DateTo.HasValue)
            {
                toDateTextBox.Text = ((DateTime)transactionSearch.DateTo).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (transactionSearch.DateFrom.HasValue)
            {
                fromDateTextBox.Text = ((DateTime)transactionSearch.DateFrom).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void TransactionListGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void TransactionListGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Called when user clicks on view or update link button of transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TransactionListGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e != null && e.CommandName == "ViewTransactionList")
            {
                TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                transactionSearch.PageSet = pagingControl.PageSet;
                SetSession<TransactionSearch>(SessionConstants.TransactionSearch, transactionSearch);

                Context.Items[UIConstants.PaymentRecordId] = e.CommandArgument.ToString();
                GridViewRow rowItem = (GridViewRow)((LinkButton)((e.CommandSource))).Parent.Parent;
                DropDownList statusDropDown = (DropDownList)rowItem.FindControl("statusDropDownList");
                //If status drop down is visible than transaction is of type check else online
                Context.Items[UIConstants.TransactionType] = statusDropDown.Visible == true ? (int)PaymentTypeEnum.MailCheck : (int)PaymentTypeEnum.CreditCard;
                Server.Transfer("~/Admin/TransactionDetails.aspx", false);
            }
            else if (e != null && e.CommandName == "UpdateTransactionList")
            {
                try
                {
                    GridViewRow rowItem = (GridViewRow)((LinkButton)((e.CommandSource))).Parent.Parent;
                    DropDownList status = (DropDownList)rowItem.FindControl("statusDropDownList");
                    string selectedValue = status.SelectedItem.Value;
                    int paymentRecordId = Convert.ToInt32(e.CommandArgument, CultureInfo.InvariantCulture);
                    //Call the business method
                    int result = _transactionManager.UpdateTransactionStatus(paymentRecordId, selectedValue);

                    //Result will be 1 meaning that one row is updated
                    if (result == 1)
                    {
                        ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.TransactionStatusUpdateSuccess, culture));
                        TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                        SearchTransactions(transactionSearch);
                    }
                    else
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.TransactionStatusUpdateFailure, culture));
                    }
                }
                catch (Exception exception)
                {
                    UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                }
            }
        }

        /// <summary>
        /// Sets the status column to drop down list if transaction is of type "Cheque".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TransactionResultGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e != null && e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow rowItem = e.Row;
                    TransactionLineItem dataItem = ((Entities.TransactionLineItem)(e.Row.DataItem));
                    DropDownList statusDropDown = (DropDownList)rowItem.FindControl("statusDropDownList");
                    Literal status = (Literal)rowItem.FindControl("statusLiteral");
                    LinkButton updateLinkButton = (LinkButton)rowItem.FindControl("updateLinkButton");
                    LinkButton viewLinkButton = (LinkButton)rowItem.FindControl("viewLinkButton");
                    Label amountPaidLabel = (Label)rowItem.FindControl("amountLabel");

                    if (dataItem.PaymentAmount.HasValue)
                        SetLabelText(amountPaidLabel, dataItem.PaymentAmount.Value.ToString("C", culture));
                    else
                        SetLabelText(amountPaidLabel, "-");

                    ScriptManager.GetCurrent(this).RegisterPostBackControl(viewLinkButton);
                    if (((TransactionLineItem)(rowItem.DataItem)).TransactionId == UIConstants.Check)
                    {
                        if (((TransactionLineItem)(rowItem.DataItem)).TransactionStatus == "Complete")
                        {
                            statusDropDown.Visible = false;
                            status.Visible = true;
                            updateLinkButton.Visible = false;
                        }
                        else
                        {
                            statusDropDown.Visible = true;
                            status.Visible = false;
                            updateLinkButton.Visible = true;
                        }                        
                        BindStaticDropDown(DropDownConstants.TransactionStatusDropDown, statusDropDown, null, null);
                        ListItem statusListItem = statusDropDown.Items.FindByText(TransactionStatusEnum.Error.ToString());
                        if (statusListItem != null)
                        {
                            statusDropDown.Items.Remove(statusListItem);
                        }
                        statusDropDown.Items.FindByText(dataItem.TransactionStatus).Selected = true;
                        viewLinkButton.CommandArgument = dataItem.PaymentRecordId.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        statusDropDown.Visible = false;
                        status.Visible = true;
                        updateLinkButton.Visible = false;
                        viewLinkButton.CommandArgument = dataItem.PaymentRecordId.ToString(CultureInfo.InvariantCulture);
                    }
                    viewLinkButton.Text = ResourceUtility.GetLocalizedString(826, culture, "view");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                transactionSearch.PageIndex = pagingControl.CurrentPageIndex;
                transactionSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                transactionSearch.AllClick = pagingControl.AllPageClick;
                SearchTransactions(transactionSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Set the search parameter
        /// </summary>
        /// <returns></returns>
        private TransactionSearch BuildSearchCriteria(User user)
        {
            var transactionSearch = new TransactionSearch()
            {
                UserId = user.UserId,
                PartnerId = (partnerDropDownList.SelectedIndex == 0) ? string.Empty : partnerDropDownList.SelectedValue,
                PartnerStatusId = (partnershipStatusDropDown.SelectedIndex == 0) ? string.Empty : partnershipStatusDropDown.SelectedValue,
                TransactionStatusId = (transactionStatusDropDown.SelectedIndex == 0) ? string.Empty : transactionStatusDropDown.SelectedValue,
                PartnerShipTypeId = (partnerTypeDropDown.SelectedIndex == 0) ? string.Empty : partnerTypeDropDown.SelectedValue,
                QuickSearchExpression = searchTextBox.Text.Trim(),
            };

            if (!string.IsNullOrWhiteSpace(fromDateTextBox.Text))
            {
                transactionSearch.DateFrom = Convert.ToDateTime(fromDateTextBox.Text, CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrWhiteSpace(toDateTextBox.Text))
            {
                transactionSearch.DateTo = Convert.ToDateTime(toDateTextBox.Text, CultureInfo.InvariantCulture);
            }

            return transactionSearch;
        }

        /// <summary>
        /// Populate the transaction summary displayed at top of transaction grid
        /// </summary>
        /// <param name="transaction"></param>
        private void PopulateTransactionStatistics(Transaction transaction)
        {
            dateLabel.Text = transaction.FromDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) + " to " + transaction.ToDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            regularVAlueLabel.Text = transaction.RegularTransactionsAmount.HasValue ?
                transaction.RegularTransactionsAmount.Value.ToString("C", new CultureInfo("en-US")) : 0.ToString("C", new CultureInfo("en-US"));
            goldValueLabel.Text = transaction.GoldTransactionsAmount.HasValue ?
                transaction.GoldTransactionsAmount.Value.ToString("C", new CultureInfo("en-US")) : 0.ToString("C", new CultureInfo("en-US"));
            totalLabel.Text = transaction.Total.HasValue ? transaction.Total.Value.ToString("C", new CultureInfo("en-US")) : null;
            goldDataLabel.Text = transaction.GoldTransactionsNo.ToString(CultureInfo.InvariantCulture);
            regularDataLabel.Text = transaction.RegularTransactionsNo.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
            transactionSearch.SortExpression = sortExpression;
            transactionSearch.SortDirection = sortDirection;
            transactionSearch.PageIndex = 0;
            SearchTransactions(transactionSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }

        /// <summary>
        /// This method will return the sort column index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in transactionListGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return transactionListGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("Transactions.aspx") || Request.UrlReferrer.ToString().Contains("TransactionDetails.aspx")))
                    {
                        TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);
                        if (field.SortExpression == transactionSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, transactionSearch.SortExpression);
                            if (transactionSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return transactionListGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// Get the transaction list
        /// </summary>
        /// <param name="userSearch"></param>
        private void GetTransactionList(TransactionSearch transactionSearch)
        {
            transactionSearch.SortExpression = UIConstants.TransactionDate; ;
            transactionSearch.SortDirection = UIConstants.AscAbbreviation;
            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
            SetViewState(ViewStateConstants.SortExpression, UIConstants.TransactionDate);
            SearchTransactions(transactionSearch);

            //Pagination Control Setting
            pagingControl.CurrentPageIndex = 0;
            pagingControl.PageSet = 0;
            pagingControl.AllPageClick = false;
        }

        /// <summary>
        /// Search the transaction
        /// </summary>
        /// <param name="userSearch"></param>
        private void SearchTransactions(TransactionSearch transactionSearch)
        {
            if (transactionSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                transactionSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                transactionSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (transactionSearch.PageIndex == -1)
                {
                    transactionSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = transactionSearch.PageIndex;
            }

            var transactionList = _transactionManager.GetTransactionList(transactionSearch);
            if (transactionList == null || transactionList.Transactions.Count == 0)
            {
                ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoTransactionsFoundForCriteria, culture));
                detailSectionDiv.Visible = false;
            }
            else
            {
                transactionSearch.TotalRecordCount = transactionList.TotalRecords;
                detailSectionDiv.Visible = true;
                PopulateTransactionStatistics(transactionList);
            }

            //Save criteria in session for back button functionality
            SetSession<TransactionSearch>(SessionConstants.TransactionSearch, transactionSearch);

            transactionListGridView.DataSource = transactionList != null ? transactionList.Transactions : null;
            transactionListGridView.DataBind();

            //Pagination Control Setting
            pagingControl.TotalRecordCount = transactionList != null ? transactionList.TotalRecords : 0;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// This method will maintain page state when user redirect from user  details screen
        /// </summary>
        private void MaintainPageState()
        {
            TransactionSearch transactionSearch = GetSession<TransactionSearch>(SessionConstants.TransactionSearch);

            if (transactionSearch != null)
            {
                PolulateSearchCriteria(transactionSearch);

                pagingControl.PageSet = transactionSearch.PageSet;

                SearchTransactions(transactionSearch);
            }
        }
        protected void ExportToExcelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                ExportTransactions();

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
        /// <summary>
        /// Search the transactions
        /// </summary>
        private void ExportTransactions()
        {
            User user = GetSession<User>(SessionConstants.User);
            if (user != null)
            {
                validateDateCustomValidator.ServerValidate += new ServerValidateEventHandler(ValidateTransactionDate);
                validateDateCustomValidator.Validate();
                if (IsValid)
                {
                    TransactionSearch transactionSearch = BuildSearchCriteria(user);
                    transactionSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                    transactionSearch.SearchSource = UIConstants.ActivationGo;

                    ExportTransactionList(transactionSearch);
                }
                //else
                //{
                //    transactionListGridView.DataSource = null;
                //    transactionListGridView.DataBind();

                //    //Pagination Control Setting
                //    pagingControl.TotalRecordCount = 0;
                //    pagingControl.CurrentPageIndex = 0;
                //    pagingControl.PageSet = 0;
                //    pagingControl.AllPageClick = false;
                // }
            }
        }
        private void ExportTransactionList(TransactionSearch transactionSearch)
        {
            transactionSearch.SortExpression = UIConstants.TransactionDate; ;
            transactionSearch.SortDirection = UIConstants.AscAbbreviation;
            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
            SetViewState(ViewStateConstants.SortExpression, UIConstants.TransactionDate);
            ExportTransactions(transactionSearch);

            ////Pagination Control Setting
            //pagingControl.CurrentPageIndex = 0;
            //pagingControl.PageSet = 0;
            //pagingControl.AllPageClick = false;
        }
        /// <summary>
        /// Search the transaction
        /// </summary>
        /// <param name="userSearch"></param>
        private void ExportTransactions(TransactionSearch transactionSearch)
        {
            pagingControl.AllPageClick = true;
            transactionSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            var transactionList = _transactionManager.ExportTransactions(transactionSearch);
            if (transactionList == null || transactionList.Tables.Count == 0)
            {
                ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoTransactionsFoundForCriteria, culture));
                detailSectionDiv.Visible = false;
            }
            else
            {
                ExportToExcel(transactionList);
            }
        }
        /// <summary>
        /// Exports Transaction Reports Data into Excel Format
        /// </summary>
        /// <param name="constReportDataSet"></param>
        private void ExportToExcel(DataSet constReportDataSet)
        {
            try
            {
                string attach = UIConstants.TransactionSummaryExcelFileName;
                IList<ReportColumns> reportColumnCollection = null;
                HttpResponse response = HttpContext.Current.Response;
                Response.ClearContent();
                Response.AddHeader("content-disposition", attach);
                Response.ContentType = UIConstants.ContentType;
                reportColumnCollection = _helperManager.GetReportColumnDetails((int)ReportEnum.Transaction);

                StringBuilder constReportStringBuilder = new StringBuilder();
                constReportStringBuilder.Append("<html><head><title></title></head><body>");
                constReportStringBuilder.AppendFormat("<table border = '1' cellpadding = '2'>");
                constReportStringBuilder.Append("<tr style = 'width: 100%'>");
                constReportStringBuilder.AppendFormat("<td colspan = '{0}'><b>Transaction Summary Report</b></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.AppendFormat("<tr><td colspan = '{0}'></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.AppendFormat("<tr><td colspan = 2>From: {0}&nbsp; To: {2}</td><td>Gold: &nbsp${3}</td><td colspan = {1}> Regular: &nbsp;${4}</td></tr>", Convert.ToDateTime(constReportDataSet.Tables[0].Rows[0][1]).ToShortDateString(), reportColumnCollection.Count - 3, Convert.ToDateTime(constReportDataSet.Tables[0].Rows[0][0]).ToShortDateString(), constReportDataSet.Tables[0].Rows[0][4], constReportDataSet.Tables[0].Rows[0][6]);
                constReportStringBuilder.AppendFormat("<tr><td colspan = 2>Total: &nbsp;${0}</td><td>Gold Trans: {2}</td><td  colspan = {1}> Regular Trans: {3}</td></tr>", constReportDataSet.Tables[0].Rows[0][2], reportColumnCollection.Count - 3, constReportDataSet.Tables[0].Rows[0][3], constReportDataSet.Tables[0].Rows[0][5]);
                constReportStringBuilder.AppendFormat("<tr><td colspan = '{0}'></td></tr>", reportColumnCollection.Count);
                constReportStringBuilder.Append("<tr>");
                foreach (ReportColumns reportColumn in reportColumnCollection)
                {
                    constReportStringBuilder.AppendFormat("<td style ='width: {1}px; background-color: {2};vertical-align: {3};text-align: {4};height: {5};color: {7}';display:{6}><b>{0}</b></td>"
                                                    , reportColumn.ColumnName, reportColumn.Width, reportColumn.HeaderBgColor, reportColumn.HeaderVAlign, reportColumn.HeaderHAlign, (reportColumn.AutoFit) ? "100%" : reportColumn.Height + "px",
                                                     reportColumn.Visible, reportColumn.HeaderTextColor);
                }

                constReportStringBuilder.Append("</tr>");

                if (constReportDataSet.Tables != null)
                {
                    foreach (DataRow reportRow in constReportDataSet.Tables[2].Rows)
                    {
                        constReportStringBuilder.Append("<tr>");
                        constReportStringBuilder.AppendFormat("<td valign='top' align='left'>{0}</td>",
                            Convert.ToDateTime(reportRow["TransactionDate"]).ToString("dd-MMM-yyyy, hh:mm tt"));
                        constReportStringBuilder.AppendFormat("<td valign='top' align='left'>{0}</td>",
                                reportRow["CompanyName"]);
                        constReportStringBuilder.AppendFormat("<td valign='top' align='left'>{0}</td>",
                                reportRow["PartnerFirstName"] + " " + reportRow["PartnerLastName"]);
                        constReportStringBuilder.AppendFormat("<td valign='top' align='left'>{0}</td>",
                                reportRow["TransactionId"]);
                        constReportStringBuilder.AppendFormat("<td valign='top' align='left'>{0}</td>",
                                reportRow["PartnershipTypeDesc"]);
                        constReportStringBuilder.AppendFormat("<td valign='top' align='left'>${0}</td>",
                                reportRow["AmountPaid"]);
                        constReportStringBuilder.AppendFormat("<td valign='top' align='left'>{0}</td>",
                                reportRow["TransactionStatus"]);
                        constReportStringBuilder.Append("</tr>");
                    }
                }
                constReportStringBuilder.Append("</table></body></html>");
                Response.Write(constReportStringBuilder.ToString());
                Response.End();
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }
    }
}