﻿<%@ Page Title="Content Blocks" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ViewStateMode="Inherit" AutoEventWireup="True" CodeBehind="ContentBlocks.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.ContentBlocks" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">

        function ConfirmContentBlockDelete() {
            return confirm("Are you sure you want to delete selected content block?");
        }

        function InitializeToggle() {
            var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleStateValue == "0") {
                document.getElementById('toggleDiv').style.display = "block";
            }
            else {
                $('.toggle-divider a')[0].removeAttribute('class');
                document.getElementById('toggleDiv').style.display = "none";
            }

            $('.col-6 tr, .col-7 tr').hover(function () {

                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            })
        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="contentDetails" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="dropDown">
        <div class="wrapper">
            <div class="rgt-bg">
                <ul>
                    <li>
                        <asp:HyperLink ID="generalSettingsHyperLink" runat="server" NavigateUrl="~/Admin/Settings.aspx"
                            CBID="396" Text="General Settings"></asp:HyperLink></li>
                    <li>
                        <asp:Label ID="contentBlockSubMenuLabel" CBID="336" runat="server" CssClass="active"
                            Text="Content Blocks"></asp:Label>
                    </li>
                    <li>
                        <asp:HyperLink ID="emailTemplateHyperLink" Visible="false" runat="server" Text="Email Templates"
                            NavigateUrl="~/Admin/EmailTemplates.aspx"></asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="contentBlockUpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="quickSearchButton" Text="Search" CBID="651" runat="server" OnClick="QuickSearch_Click"
                                        ValidationGroup="SearchContentBlocks" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchContentBlocks"
                                    ControlToValidate="searchTextBox" Display="Dynamic" ErrorMessage="Please specify search criteria"
                                    VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server" />
                            </div>
                            <div class="toggle-divider">
                                <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                            </div>
                            <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                                <div class="field">
                                    <asp:Label ID="titleLabel" CBID="905" runat="server" CssClass="label" Text="Title:"></asp:Label>
                                    <div class="textbox-2 textbox-2-width">
                                        <asp:DropDownList ID="sectionDropDown" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="alR searchpad-top">
                                        <span class="btn-style">
                                            <asp:LinkButton ID="goLinkButton" Text="Go" runat="server" CBID="906" OnClick="GoButton_Click" />
                                        </span>
                                    </div>
                                </div>
                                <div class="clear">
                                    &nbsp;</div>
                            </div>
                            <div class="clear">
                                &nbsp;</div>
                        </div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <asp:LinkButton ID="addContentBlockLinkButton" runat="server" OnClick="AddContentBlockLinkButton_Click"
                        Text="+ Add Content Block" CssClass="btn"> </asp:LinkButton>
                    <h2>
                        <asp:Label ID="contentBlockTitleLabel" runat="server" CBID="337" Text="Content Blocks"></asp:Label>
                    </h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <asp:GridView ID="contentBlockListGridView" ViewStateMode="Enabled" runat="server"
                                AutoGenerateColumns="False" OnRowCommand="ContentBlockListGridView_RowCommand"
                                OnRowCreated="ContentBlockListGridView_RowCreated" OnRowDataBound="ContentBlockListGridView_RowDataBound"
                                OnSorting="ContentBlockListGridView_Sorting" AllowSorting="true" class="col-6">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="ContentBlockId" SortExpression="ContentBlockId"
                                        HeaderStyle-Width="10%" />
                                    <asp:BoundField HeaderText="Title" DataField="Section" SortExpression="Section" HeaderStyle-Width="20%" />
                                    <asp:BoundField HeaderText="Section" DataField="Title" SortExpression="Title" HeaderStyle-Width="30%" />
                                    <asp:BoundField HeaderText="Description" DataField="Description" SortExpression="Description"
                                        HeaderStyle-Width="35%" />
                                    <asp:TemplateField HeaderText="" ShowHeader="False" ItemStyle-Width="2%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="deleteLinkButton" runat="server" CausesValidation="False" CommandName="DeleteContentBlock"
                                                CommandArgument='<%# Bind("ContentBlockId") %>' CBID="907" Text="delete" CssClass="confirm"
                                                OnClientClick="javascript:return ConfirmContentBlockDelete();" OnCommand="DeleteContentBlock"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False" ItemStyle-Width="2%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="editLinkButton" runat="server" CausesValidation="False" CommandName="EditContentBlock"
                                                CommandArgument='<%# Bind("ContentBlockId") %>' CBID="778" Text="edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>