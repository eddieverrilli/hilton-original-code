﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" EnableViewState="false" CodeBehind="Dashboard.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" runat="server">
 <script type="text/javascript">
     function UpdateSessionTimeout() {
         SessionTimeoutCheck();
     }
 </script>
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="content">
        <div class="wrapper">
            <asp:LinkButton ID="downloadDashboardReportLinkButton" CBID="0" runat="server" CssClass="btn"
                OnClick="DownloadSpreadSheet_ClickLinkButton" Text="Export To Excel"></asp:LinkButton>
            <h2>
                <asp:Label ID="dashboardLabel" runat="server" cbid="349" Text="Dashboard"></asp:Label>
            </h2>
            <div class="inner-content">
                <div class="dashboard">
                    <div id="resultMessageDiv" runat="server">
                    </div>
                    <div id="dashboardContent" runat="server">
                        <div class="lft">
                            <div class="box">
                                <h3>
                                    <asp:Label ID="partnerLabel" runat="server" CBID="501" Text="Partners"></asp:Label></h3>
                                <div class="box-content">
                                    <div>
                                        <table cellspacing="0" cellpadding="0" border="1" style="border-style: None; width: 100%;
                                            border-collapse: collapse;" id="partnersTable" class="col-3" rules="all">
                                            <tbody>
                                                <tr>
                                                    <th scope="col" class="col-1">
                                                        &nbsp;
                                                    </th>
                                                    <th scope="col" class="col-2">
                                                        <asp:Label ID="goldLabel" runat="server" CBID="406" Text="Gold"></asp:Label>
                                                    </th>
                                                    <th scope="col" class="col-3">
                                                        <asp:Label ID="partnerLabel2" runat="server" CBID="497" Text="Partner"></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="currentLabel" runat="server" CBID="347" Text="Current"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="currentGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="currentRegLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="outstandPmtRqstLabel" runat="server" CBID="781" Text="# of Outstanding Payment Requests"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="outstandingPaymentGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="outstandingPaymentRegularLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="dollarOutstandingPmtRquestLabel" runat="server" CBID="782" Text="$ of Outstanding Payment Requests (USD)"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="outstandingPaymentRequestAmountGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="outstandingPaymentRequestAmountRegularLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="totalCollectedYTDLabel" runat="server" CBID="371" Text="Total $ Collected YTD (USD)"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="totalCollectedYTDGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="totalCollectedYTDRegularLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="totalNumberActivatedCurrentMonthLabel" runat="server" CBID="0" Text="Total # Activated - Current Month"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="totalNumberActivatedCurrentMonthGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="totalNumberActivatedCurrentMonthRegularLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="totalAmountActivatedCurrentMonthLabel" runat="server" CBID="0" Text="Total $ Activated - Current Month  (USD)"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="totalAmountActivatedCurrentMonthGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="totalAmountActivatedCurrentMonthRegularLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="totalNumberActivatedCurrentYearLabel" runat="server" CBID="0" Text="Total # Activated - Current Year"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="totalNumberActivatedCurrentYearGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="totalNumberActivatedCurrentYearRegularLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="totalAmountActivatedCurrentYearLabel" runat="server" CBID="0" Text="Total $ Activated - Current Year (USD)"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="totalAmountActivatedCurrentYearGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="totalAmountActivatedCurrentYearRegularLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="pendingPartnerRequestLabel" runat="server" CBID="507" Text="Pending Partner Requests"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="pendingPartnerRequestGoldLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="col-3">
                                                        <asp:Literal ID="pendingPartnerRequestRegularLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="products" class="box">
                                <h3>
                                    <asp:Label ID="productsLabel" runat="server" CBID="544" Text="Products"></asp:Label></h3>
                                <div class="box-content">
                                    <div>
                                        <table cellspacing="0" cellpadding="0" border="1" style="width: 100%; border-collapse: collapse;"
                                            id="productSummaryGridView" class="col-3" rules="all">
                                            <tbody>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="noOfProductsLabel" runat="server" CBID="469" Text="# Products"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="productsLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="noOfPendingParterLabel" runat="server" CBID="468" Text="# Pending Requests"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="pendingRequestLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="noOfOtherRequestLabel" runat="server" CBID="467" Text="# 'Other' Selections"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="otherRequestsLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rgt">
                            <div class="box" id="projects">
                                <h3>
                                    <asp:Label ID="projectsLabel" runat="server" CBID="556" Text="Projects"></asp:Label></h3>
                                <div class="box-content">
                                    <asp:GridView ID="projectGridView" runat="server" Width="100%" CellPadding="0" class="col-3"
                                        AutoGenerateColumns="false" OnRowDataBound="ProjectGridView_RowDataBound" DataKeyNames="ProjectTypeId">
                                        <Columns>
                                            <asp:BoundField DataField="ProjectTypeID" ItemStyle-CssClass="col-1" HeaderStyle-CssClass="col-1"
                                                Visible="false" />
                                            <asp:BoundField DataField="ProjectType" ItemStyle-CssClass="col-1" HeaderStyle-CssClass="col-1" />
                                            <asp:BoundField HeaderText="Total" DataField="TotalProjects" ItemStyle-CssClass="col-2"
                                                HeaderStyle-CssClass="col-2" />
                                            <asp:BoundField HeaderText="# @ 0" DataField="NotConfiguredProjectsPercent" ItemStyle-CssClass="col-3"
                                                HeaderStyle-CssClass="col-2" />
                                            <asp:BoundField HeaderText="Avg % after 0" DataField="PartialConfiguredProjectsPercent"
                                                ItemStyle-CssClass="col-4" HeaderStyle-CssClass="col-4" />
                                            <asp:BoundField HeaderText="# 100%" DataField="FullConfiguredProjectsPercent" ItemStyle-CssClass="col-5"
                                                HeaderStyle-CssClass="col-5" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="users" class="box">
                                <h3>
                                    <asp:Label ID="usersLabel" runat="server" CBID="735" Text="Users"></asp:Label></h3>
                                <div class="box-content">
                                    <asp:GridView ID="userGridView" runat="server" Width="100%" CellPadding="0" class="col-3"
                                        AutoGenerateColumns="False" OnRowDataBound="UserGridView_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="UserType" ItemStyle-CssClass="col-1" HeaderStyle-CssClass="col-1" />
                                            <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-CssClass="col-2" HeaderStyle-CssClass="col-2" />
                                            <asp:TemplateField HeaderText="# Logins" HeaderStyle-CssClass="col-3" ItemStyle-CssClass="col-3">
                                                <ItemTemplate>
                                                    <%# DataBinder.Eval(Container.DataItem, "TotalLogOn", "{0:0}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Avg # Logins" ItemStyle-CssClass="col-4" HeaderStyle-CssClass="col-4">
                                                <ItemTemplate>
                                                    <%# DataBinder.Eval(Container.DataItem, "AverageLogOn", "{0:0.#}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="TotalAssocProj" HeaderText="# Assoc Proj" ItemStyle-CssClass="col-5" HeaderStyle-CssClass="col-5" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="messaging" class="box">
                                <h3>
                                    <asp:Label ID="msgFeedbackLabel" runat="server" CBID="440" Text="Messaging & Feedback"></asp:Label></h3>
                                <div class="box-content">
                                    <div>
                                        <table cellspacing="0" cellpadding="0" border="1" style="width: 100%; border-collapse: collapse;"
                                            id="messagingGridView" class="col-3" rules="all">
                                            <tbody>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="msgToOwnerLabel" runat="server" CBID="465" Text="# Messages To Owners"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="messagesToOwnersLiteral" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="msgtoPartnerLabel" runat="server" CBID="466" Text="# Messages To Partners"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="messagesToPartnersLiterals" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-1">
                                                        <asp:Label ID="feedbackSubmittedLabel" runat="server" CBID="463" Text="# Feedback Submitted"></asp:Label>
                                                    </td>
                                                    <td class="col-2">
                                                        <asp:Literal ID="feedbackSubmittedLiterals" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
