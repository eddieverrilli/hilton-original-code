﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Projects : PageBase
    {
        private IProjectManager _projectManager;


        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedBrands_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                var brands = sender as TextBox;
                brandCollection.Value = ProcessDropDownItemChange(DropDownConstants.BrandDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Register for post backs
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            brands.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(optgroup, p.DataValueField)));
            base.Render(writer);
        }

        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                projectSearch.PageIndex = pagingControl.CurrentPageIndex;
                projectSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                projectSearch.AllClick = pagingControl.AllPageClick;
                SearchProjects(projectSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                projectSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchProjects(projectSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Pull up the projects from DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoButton_click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    
                    if (string.IsNullOrWhiteSpace(selectedBrands.Text))
                    {
                        ConfigureResultMessage(resultMessageDiv, "message error", "Please select brand");
                    }
                    else
                    {
                    ProjectSearch projectSearch = BuildSearchCriteria();
                    projectSearch.SearchFlag = SearchFlagEnum.Filter.ToString();

                    GetProjectList(projectSearch);
                }
                    }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                projectSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchProjects(projectSearch);
                ClearSession(SessionConstants.ProjectSearch);
                SetSession<ProjectSearch>(SessionConstants.ProjectSearch, projectSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                projectSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchProjects(projectSearch);
                ClearSession(SessionConstants.ProjectSearch);
                SetSession<ProjectSearch>(SessionConstants.ProjectSearch, projectSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Set the GridView Header from Content Block
        /// </summary>
        private void SetGridViewHeaderText()
        {
            projectListGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(419, culture, "ID");
            projectListGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(553, culture, "Project Type");
            projectListGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(261, culture, "Brand");
            projectListGridView.Columns[3].HeaderText = "Brand Segment";//ResourceUtility.GetLocalizedString(261, culture, "BrandSegment");
            projectListGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(570, culture, "Property Type");
            projectListGridView.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(598, culture, "Region");
            projectListGridView.Columns[6].HeaderText = ResourceUtility.GetLocalizedString(344, culture, "Country");
            projectListGridView.Columns[7].HeaderText = ResourceUtility.GetLocalizedString(799, culture, "Facility Name");
            projectListGridView.Columns[8].HeaderText = ResourceUtility.GetLocalizedString(478, culture, "Owner");
            projectListGridView.Columns[9].HeaderText = ResourceUtility.GetLocalizedString(515, culture, "PM");
            projectListGridView.Columns[10].HeaderText = ResourceUtility.GetLocalizedString(645, culture, "SC %");
            projectListGridView.Columns[11].HeaderText = ResourceUtility.GetLocalizedString(477, culture, "Other");
            projectListGridView.Columns[12].HeaderText = ResourceUtility.GetLocalizedString(471, culture, "O2O Status");
            projectListGridView.Columns[13].HeaderText = ResourceUtility.GetLocalizedString(694, culture, "Status");
        }

        /// <summary>
        /// Invoked on OnInit
        /// </summary>/
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                ValidateUserAccess((int)MenuEnum.Projects);                
                _projectManager = ProjectManager.Instance;
                this.Title = ResourceUtility.GetLocalizedString(1099, culture, "Project List");

                SetGridViewHeaderText();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;

                    if (!Page.IsPostBack)
                    {
                        PopulateDropDownData();

                        if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("ProjectDetails.aspx") || Request.UrlReferrer.ToString().Contains("Projects.aspx")))
                        {
                            MaintainPageState();
                        }
                        else
                        {
                            ClearSession(SessionConstants.ProjectSearch);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(partnerUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                        ScriptManager.RegisterClientScriptBlock(partnerUpdatePanel, this.GetType(), "getItems", "getItems();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                projectSearch.PageIndex = pagingControl.CurrentPageIndex;
                projectSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                projectSearch.AllClick = pagingControl.AllPageClick;
                SearchProjects(projectSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                projectSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchProjects(projectSearch);
                ClearSession(SessionConstants.ProjectSearch);
                SetSession<ProjectSearch>(SessionConstants.ProjectSearch, projectSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                projectSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchProjects(projectSearch);
                ClearSession(SessionConstants.ProjectSearch);
                SetSession<ProjectSearch>(SessionConstants.ProjectSearch, projectSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on GridView RowCommand
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProjectListGridView_RowCommand(object sender, CommandEventArgs args)
        {
            try
            {
                if (args != null && (args.CommandName == "ProjectDetails"))
                {
                    ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                    projectSearch.PageSet = pagingControl.PageSet;
                    ClearSession(SessionConstants.ProjectSearch);
                    SetSession<ProjectSearch>(SessionConstants.ProjectSearch, projectSearch);
                    Context.Items[UIConstants.ProjectId] = args.CommandArgument.ToString();
                    Server.Transfer("~/Admin/ProjectDetails.aspx", true);
                }
            }
            catch (ThreadAbortException)
            { }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProjectListGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProjectListGridView_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton detailsLinkButton = args.Row.FindControl("detailsLinkButton") as LinkButton;
                    if (detailsLinkButton != null)
                    {
                        detailsLinkButton.Text = ResourceUtility.GetLocalizedString(1100, culture, "details");
                        ScriptManager.GetCurrent(this).RegisterPostBackControl(detailsLinkButton);
                    }

                    if (args.Row.Cells[6].Text == "&nbsp;")
                    {
                        args.Row.Cells[6].Text = "-";
                    }

                    if (args.Row.Cells[7].Text == "&nbsp;")
                    {
                        args.Row.Cells[7].Text = "-";
                    }

                    args.Row.Cells[8].Text += "%";

                    Label brand = args.Row.FindControl("brandLabel") as Label;
                    brand.Text = ((Project)(args.Row.DataItem)).Property.BrandDescription;

                    Label brandSegment = args.Row.FindControl("brandSegmentLabel") as Label;
                    brandSegment.Text = ((Project)(args.Row.DataItem)).Property.BrandSegment;

                    Label propertyTypeDescriptionLabel = args.Row.FindControl("propertyTypeDescriptionLabel") as Label;
                    propertyTypeDescriptionLabel.Text = ((Project)(args.Row.DataItem)).Property.PropertyTypeDescription;

                    Label regionDescriptionLabel = args.Row.FindControl("regionDescriptionLabel") as Label;
                    regionDescriptionLabel.Text = ((Project)(args.Row.DataItem)).Property.RegionDescription;

                    Label countryDescriptionLabel = args.Row.FindControl("countryDescriptionLabel") as Label;
                    countryDescriptionLabel.Text = ((Project)(args.Row.DataItem)).Property.Country;

                    Label facilityNameLabel = args.Row.FindControl("facilityNameLabel") as Label;
                    facilityNameLabel.Text = ((Project)(args.Row.DataItem)).Property.FacilityUniqueName;

                    Label facilityIdLabel = args.Row.FindControl("facilityIdLabel") as Label;
                    facilityIdLabel.Text = ((Project)(args.Row.DataItem)).Property.FacilityId.ToString(CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ProjectListGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        
        /// <summary>
        /// This method is invoded with search button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void QuickSearch_Click(object sender, EventArgs args)
        {
            try
            {
                ProjectSearch projectSearch = BuildSearchCriteria();
                projectSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                GetProjectList(projectSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

       
        /// <summary>
        ///  Refresh the drop down lists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PropertyTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                brandCollection.Value = ProcessDropDownItemChange(DropDownConstants.RegionDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Fires at RegionDropDown Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegionDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //To reset the country already selected
                countryDropDown.SelectedIndex = -1;
                brandCollection.Value = ProcessDropDownItemChangeforCountry(DropDownConstants.RegionDropDown, (isCheckAll.Value == "1" || string.IsNullOrWhiteSpace(selectedBrands.Text)) ? "0" : selectedBrands.Text, regionDropDown,countryDropDown, propertyTypeDropDown);
            }
            catch (Exception exception)
            { 
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);

                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }


      
        /// <summary>
        /// To Bind Projects to Grid View
        /// </summary>
        /// <param name="userList"></param>
        private void BindProjectList(IList<Project> projectList)
        {
            projectListGridView.DataSource = projectList;
            projectListGridView.DataBind();
        }

        /// <summary>
        /// This method will populate the search criteria
        /// </summary>
        /// <returns></returns>
        private ProjectSearch BuildSearchCriteria()
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            ProjectSearch projectSearch = new ProjectSearch()
            {
                UserId = currentUser.UserId,
                QuickSearchExpression = searchTextBox.Text.Trim(),
                PropertyTypeId = (propertyTypeDropDown.SelectedIndex == 0) ? string.Empty : propertyTypeDropDown.SelectedValue,
                ProjectManagerId = (pmDropDown.SelectedIndex == 0) ? string.Empty : pmDropDown.SelectedValue,
                ProjectTypeId = (projectTypeDropDown.SelectedIndex == 0) ? string.Empty : projectTypeDropDown.SelectedValue,
                OwnerId = (ownerDropDown.SelectedIndex == 0) ? string.Empty : ownerDropDown.SelectedValue,
                O2OStatusId = (o2oStatusDropDown.SelectedIndex == 0) ? string.Empty : o2oStatusDropDown.SelectedValue,
                SCStatusId = (scStatusDropDown.SelectedIndex == 0) ? string.Empty : scStatusDropDown.SelectedValue,
                CountryId = (countryDropDown.SelectedIndex == 0) ? string.Empty : countryDropDown.SelectedValue,
                //BrandId = (brandDropDown.SelectedIndex == 0) ? string.Empty : brandDropDown.SelectedValue,
                BrandId = selectedBrands.Text,
                BrandCollection = brandCollection.Value,
                RegionId = (regionDropDown.SelectedIndex == 0) ? string.Empty : regionDropDown.SelectedValue,
                PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture),
                PageIndex = 0,
            };
            return projectSearch;
        }

        /// <summary>
        /// To Get the project list
        /// </summary>
        /// <param name="projectSearch"></param>
        private void GetProjectList(ProjectSearch projectSearch)
        {
            projectSearch.SortExpression = UIConstants.BrandDescription; ;
            projectSearch.SortDirection = UIConstants.AscAbbreviation;
            SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
            SetViewState(ViewStateConstants.SortExpression, UIConstants.BrandDescription);
            SearchProjects(projectSearch);

            //Pagination Control Setting
            pagingControl.CurrentPageIndex = 0;
            pagingControl.PageSet = 0;
            pagingControl.AllPageClick = false;
        }

        /// <summary>
        /// This method will return the sort colum index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in projectListGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return projectListGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("Projects.aspx") || Request.UrlReferrer.ToString().Contains("ProjectDetails.aspx")))
                    {
                        ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
                        if (field.SortExpression == projectSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, projectSearch.SortExpression);
                            if (projectSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return projectListGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// This method will maintain page state when user redirect from user  details screen
        /// </summary>
        private void MaintainPageState()
        {
            ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);

            if (projectSearch != null)
            {
                PolulateSearchCriteria(projectSearch);

                pagingControl.PageSet = projectSearch.PageSet;

                SearchProjects(projectSearch);
            }
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        private void PolulateSearchCriteria(ProjectSearch projectSearch)
        {
            selectedBrands.Text = projectSearch.BrandId;
            brandCollection.Value = projectSearch.BrandCollection;
            if (!string.IsNullOrEmpty(projectSearch.OwnerId))
                ownerDropDown.SelectedValue = projectSearch.OwnerId;

            if (!string.IsNullOrEmpty(projectSearch.RegionId))
            {
                regionDropDown.SelectedValue = projectSearch.RegionId;                
                ProcessDropDownItemChange(DropDownConstants.RegionDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(projectSearch.BrandId))
            {
                ProcessDropDownItemChange(DropDownConstants.BrandDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(projectSearch.ProjectTypeId))
                projectTypeDropDown.SelectedValue = projectSearch.ProjectTypeId;

            if (!string.IsNullOrEmpty(projectSearch.O2OStatusId))
                o2oStatusDropDown.SelectedValue = projectSearch.O2OStatusId;

            if (!string.IsNullOrEmpty(projectSearch.ProjectManagerId))
                pmDropDown.SelectedValue = projectSearch.ProjectManagerId;

            if (!string.IsNullOrEmpty(projectSearch.PropertyTypeId))
            {
                propertyTypeDropDown.SelectedValue = projectSearch.PropertyTypeId;
                ProcessDropDownItemChange(DropDownConstants.PropertyTypesDropDown, selectedBrands.Text, regionDropDown, propertyTypeDropDown);
            }

            if (!string.IsNullOrEmpty(projectSearch.SCStatusId))
                scStatusDropDown.SelectedValue = projectSearch.SCStatusId;

            if (!string.IsNullOrEmpty(projectSearch.QuickSearchExpression))
                searchTextBox.Text = projectSearch.QuickSearchExpression;
        }

        /// <summary>
        /// This method will populate the drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            User currentUser = GetSession<User>(SessionConstants.User);
            IList<SearchFilters> searchFilters = _projectManager.GetSearchFilterItems(currentUser.UserId);
            SetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData, searchFilters);

            var brandId = Helper.GetSearchFilterDropDownData(DropDownConstants.BrandDropDown, searchFilters).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false);
            brandCollection.Value = string.Join(",", brandId.ToList().Select(p => p.DataValueField));

            //BindDropDown(searchFilters, DropDownConstants.BrandDropDown, brandDropDown, ResourceUtility.GetLocalizedString(1190, culture, "All Brands"), null);
            BindDropDown(searchFilters, DropDownConstants.PropertyTypesDropDown, propertyTypeDropDown, ResourceUtility.GetLocalizedString(1195, culture, "All Types"), null);
            BindDropDown(searchFilters, DropDownConstants.RegionDropDown, regionDropDown, ResourceUtility.GetLocalizedString(1204, culture, "All Regions"), null);
            //For Country
            BindDropDown(searchFilters, DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1301, culture, "All Countries"), null);
            BindDropDown(searchFilters, DropDownConstants.OwnerDropDown, ownerDropDown, ResourceUtility.GetLocalizedString(1214, culture, "All Owners"), null);
            BindDropDown(searchFilters, DropDownConstants.ProjectManagerDropDown, pmDropDown, ResourceUtility.GetLocalizedString(1218, culture, "All PM"), null);
            BindDropDown(searchFilters, DropDownConstants.O2OstatusDropDown, o2oStatusDropDown, ResourceUtility.GetLocalizedString(1229, culture, "All Status"), null);

            BindStaticDropDown(DropDownConstants.ProjectStatusDropDown, scStatusDropDown, ResourceUtility.GetLocalizedString(1229, culture, "All Status"), null);
            BindStaticDropDown(DropDownConstants.ProjectTypesDropDown, projectTypeDropDown, ResourceUtility.GetLocalizedString(1219, culture, "All Types"), null);
        }

        /// <summary>
        /// To search the project
        /// </summary>
        /// <param name="projectSearch"></param>
        private void SearchProjects(ProjectSearch projectSearch)
        {
            int totalRecordCount = 0;
            if (projectSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                projectSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                projectSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (projectSearch.PageIndex == -1)
                {
                    projectSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = projectSearch.PageIndex;
            }

            var projectList = _projectManager.GetProjectList(projectSearch, ref totalRecordCount);
            if (projectList.Count == 0)
            {
                searchResultDiv.Attributes.Add("class", "message notification");
                searchResultDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoProjectsFoundForCriteria, culture);
            }

            projectSearch.TotalRecordCount = totalRecordCount;

            //Save criteria in session for back button functionality
            ClearSession(SessionConstants.ProjectSearch);
            SetSession<ProjectSearch>(SessionConstants.ProjectSearch, projectSearch);

            BindProjectList(projectList);

            //Pagination Control Setting
            pagingControl.TotalRecordCount = totalRecordCount;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            ProjectSearch projectSearch = GetSession<ProjectSearch>(SessionConstants.ProjectSearch);
            projectSearch.SortExpression = sortExpression;
            projectSearch.SortDirection = sortDirection;
            projectSearch.PageIndex = 0;
            SearchProjects(projectSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }
    }
}