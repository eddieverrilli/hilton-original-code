﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Admin
{
    public partial class AddFeedbakCommentPopup : PageBase
    {
        
        private int feedbackId;
        private FeedbackManager _feedbackManager;

        /// <summary>
        /// Get the instance of the manager class
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _feedbackManager = FeedbackManager.Instance; 
            feedbackId = Convert.ToInt32(Request.QueryString["fid"], CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// event when page is loaded after a server hit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    if (!IsPostBack)
                    {
                       // commentsTextBox.Text = feedbackId.ToString();  
                    }
                }
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }



        protected void saveCommentLinkButton_Click(object sender, EventArgs e)
        {

            try
            {
                FeedbackComment comment = new FeedbackComment();
                comment.Comment = commentsTextBox.Text;
                comment.FeedbackId = feedbackId;
                User user = GetSession<User>(SessionConstants.User);
                if (user != null)
                {
                    comment.Submittedby = user.UserId;
                }
               
                _feedbackManager.AddFeedbackComment(comment);
                SetSession<string>(SessionConstants.ReturnFromCommentPopup, "Yes");   
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

    }
}