﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="true" CodeBehind="ProjectTemplateDetails.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ProjectTemplateDetails" %>



<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" ViewStateMode="Enabled"
    runat="server">


    <script type="text/javascript" src="../Scripts/thickbox.js"></script>
    <script type="text/javascript">

        function ExpandCollapse() {
            if ($("#hidExpandCollapse").val() == "0") { //Expand happnes
                $('div[id^="cphDetails_categoryDivisionsRepeater_panel_"]').show();
                $('div[id^="cphDetails_categoryDivisionsRepeater_panel_"]').children("div").each(function () {
                    $(this).show();
                });
                $("#hidExpandCollapse").val("1");
            } else { //Collaps Happnes
                $(".category-level1").each(function () {
                    $(this).parent().children("div").each(function ()
                    {
                      $(this).hide();
                  });
                });
                $("#hidExpandCollapse").val("0");
            }
        }


        function closeIframe() {
            $('.close').click();
        }

        function forceCall() {
            tb_init('a.thickbox');
            imgLoader = new Image();
            imgLoader.src = tb_pathToImage;
        }

        function SetHref() {
            var hdnfieldValue = $('#<%=selectedProjectTemplateHiddenValue.ClientID%>').val();
            $('#<%=resultMessageDiv.ClientID %>').css('display', 'none');
            $('#<%=addCategoryLinkButton.ClientID%>').attr('href', 'AddCategoryPopup.aspx?keepThis=true&cat=-1&mode=add&pt=' + hdnfieldValue.toString() + '&TB_iframe=true&height=520&width=400');
            return true;
        }

        function SetCopyTemplateHref() {
            var hdnfieldValue = $('#<%=selectedProjectTemplateHiddenValue.ClientID%>').val();
            $('#<%=resultMessageDiv.ClientID %>').css('display', 'none');
            $('#<%=copyTemplateLinkButton.ClientID%>').attr('href', 'CopyTemplatePopup.aspx?keepThis=true&pt=' + hdnfieldValue.toString() + '&TB_iframe=true&height=400&width=400');
            return true;
        }

        function InitializeToggle() {
            var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleStateValue == "0") {
                document.getElementById('toggleDiv').style.display = "block";
            }
            else {
                $('.toggle-divider a')[0].removeAttribute('class');
                document.getElementById('toggleDiv').style.display = "none";
            }

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            });

        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }
        function more_lessItems() {
            $('div.edit-btns + h5').has('span').each(function () {
                var max = 1
                if ($(this).find("span").length > max) {
                    var count = $(this).find("span").length;
                    var num = count - 1;
                    if (num != 0) {
                        $(this).find('span:gt(0)').hide();
                        var list = $(this).find('span:gt(1)');
                        $(this).append('<p class="more display-inline">+' + num + ' More )</p>');
                        $(this).append('<p class="less display-inline"> - Less )</p>');
                        $('.less').hide();
                        $('.more').click(function () {
                            $(this).next().show();
                            $(this).parent().find('span:gt(0)').show()
                            $(this).hide();
                        });
                        $('.less').click(function () {
                            $(this).prev().show();
                            $(this).parent().find('span:gt(0)').hide()
                            $(this).hide();
                        });
                    }
                }
            });
        }

        $(document).ready(function () {
            more_lessItems();
            ActiveChkClicked();
            requiredChkClicked();
            newPartnerChkClicked();
            newProductChkClicked();
        });

        function Check_OnCheckAllClick(clickevent) {
            var activeCheckCheckedIds = '';
            var activeCheckUnCheckedIds = '';
            if (clickevent == 'check') {
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_activeCheck_"]').each(function () {
                    $(this).prop('checked', true);
                    activeCheckCheckedIds = activeCheckCheckedIds + this.id.split('_')[3] + ',';
                });
            }
            else {
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_activeCheck_"]').each(function () {
                    $(this).prop('checked', false);
                    activeCheckUnCheckedIds = activeCheckUnCheckedIds + this.id.split('_')[3] + ',';
                });
            }

            //update the hidden fields which had the checked and unChecked IDs for Active?
            $('#cphDetails_hidactiveCheckCheckedIds').attr('value', activeCheckCheckedIds);
            $('#cphDetails_hidactiveCheckUnCheckedIds').attr('value', activeCheckUnCheckedIds);
        }


        //Called when Active?
        function ActiveChkClicked() {
            var countAllcatDivisionsCB = 0;
            var countCheckedcatDivisionsCB = 0;
            var activeCheckCheckedIds = '';
            var activeCheckUnCheckedIds = '';
            $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_activeCheck_"]').each(function () {
                countAllcatDivisionsCB = countAllcatDivisionsCB + 1;
                if ($(this).is(":checked")) {
                    countCheckedcatDivisionsCB = countCheckedcatDivisionsCB + 1;
                    activeCheckCheckedIds = activeCheckCheckedIds + this.id.split('_')[3] + ',';
                }
                else
                { activeCheckUnCheckedIds = activeCheckUnCheckedIds + this.id.split('_')[3] + ','; }
            });

            if (countAllcatDivisionsCB == countCheckedcatDivisionsCB) {
                $("#cphDetails_isActiveAllCheck").prop('checked', true);
            } else { $("#cphDetails_isActiveAllCheck").prop('checked', false); }

            $('#cphDetails_hidactiveCheckCheckedIds').attr('value', activeCheckCheckedIds);
            $('#cphDetails_hidactiveCheckUnCheckedIds').attr('value', activeCheckUnCheckedIds);

            UpdateThreeAllChk();
        }


        function isRequiredAllCheck_clicked() {
            var CountRequiredCheckedIds = 0;
            var CountRequiredUnCheckedIds = 0;
            if ($("#isRequiredAllCheck").is(':checked')) {
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_requiredCheck_"]').each(function () {
                    $(this).prop('checked', true);
                    $("#cphDetails_categoryDivisionsRepeater_requiredCheck_341_15").prop('checked', true);
                    if ($(this).is(":checked")) {
                        CountRequiredCheckedIds = CountRequiredCheckedIds + 1;
                    }
                    else {
                        CountRequiredUnCheckedIds = CountRequiredUnCheckedIds + 1;
                    }
                });
            }
            else {
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_requiredCheck_"]').each(function () {
                    $(this).prop('checked', false);
                    if ($(this).is(":checked")) {
                        CountRequiredCheckedIds = CountRequiredCheckedIds + 1;
                    }
                    else {
                        CountRequiredUnCheckedIds = CountRequiredUnCheckedIds + 1;
                    }
                });
            }
        }

        //called when any of the  Req? Individual checkbox is clicked 
        function requiredChkClicked() {
            //alert("requiredChkClicked");
            var countAllrequiredChk = 0;
            var countCheckedrequiredChk = 0;
            $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_requiredCheck_"]').each(function () {
                    countAllrequiredChk = countAllrequiredChk + 1;
                    if ($(this).is(":checked")) {
                        countCheckedrequiredChk = countCheckedrequiredChk + 1;
                    }
                });

                if (countAllrequiredChk == countCheckedrequiredChk) {
                    $("#isRequiredAllCheck").prop('checked', true);
                }
                else {
                    $("#isRequiredAllCheck").prop('checked', false);
                }
        }

        //Called when New Partners? Check All checkbox is clicked
        function newPartnersAllCheck_clicked() {
            if ($("#NewPartnerAllCheck").is(':checked')) {
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_newPartnerCheck_"]').each(function () {
                    $(this).prop('checked', true);
                });
            }
            else {
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_newPartnerCheck_"]').each(function () {
                    $(this).prop('checked', false);
                });
            }
        }

        function newPartnerChkClicked() {
            //alert("newPartnerChkClicked");
            var countAllnewPartners = 0;
            var countCheckednewPartners = 0;
            $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_newPartnerCheck_"]').each(function () {
                countAllnewPartners = countAllnewPartners + 1;
                if ($(this).is(":checked")) {
                    countCheckednewPartners = countCheckednewPartners + 1;
                }
            });
            if (countAllnewPartners == countCheckednewPartners) {
                $("#NewPartnerAllCheck").prop('checked', true);
            }
            else {
                $("#NewPartnerAllCheck").prop('checked', false);
            }
        }

        //Called when New New Products? Check All checkbox is clicked
        function newProductsAllCheck_clicked() {
            if ($("#NewProductsAllCheck").is(':checked')) {
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_newProductCheck_"]').each(function () {
                    $(this).prop('checked', true);
                });
            }
            else {
                $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_newProductCheck_"]').each(function () {
                    $(this).prop('checked', false);
                });
            }
        }

        function newProductChkClicked() {
            //alert("newProductChkClicked");
            var countAllNewProducts = 0;
            var countCheckedNewProducts = 0;
            $('input:checkbox[id^="cphDetails_categoryDivisionsRepeater_newProductCheck_"]').each(function () {
                countAllNewProducts = countAllNewProducts + 1;
                if ($(this).is(":checked")) {
                    countCheckedNewProducts = countCheckedNewProducts + 1;
                }
            });
            if (countAllNewProducts == countCheckedNewProducts) {
                $("#NewProductsAllCheck").prop('checked', true);
            }
            else {
                $("#NewProductsAllCheck").prop('checked', false);
            }
        }

        function isActiveAllCheck() {
            $('#cphDetails_hidWhichChkBoxClicked').attr('value', 'all');
            return true;
        }

        //Update the status of all the four all Checkboxes (isActiveAllCheck, isRequiredAll, NewPartnerAllCheck, NewProductsAllCheck)
        function UpdateAllFourAllChk() {
            //alert("save");
            ActiveChkClicked();
            requiredChkClicked();
            newPartnerChkClicked();
            newProductChkClicked();
        }

        //Update the status of three all Checkboxes ( isRequiredAll, NewPartnerAllCheck, NewProductsAllCheck)
        function UpdateThreeAllChk() {
            requiredChkClicked();
            newPartnerChkClicked();
            newProductChkClicked();
        }

        function ToggleCategory(str) {

          //  var div = $("." + str);
           // div.toggle();
            if ($("." + str).css('display') == 'none') {
                if ($.browser.mozilla == true && $("." + str).hasClass("cat-level")  ) {
                    $("." + str).css('display', 'inline-block');
                    $("." + str).css('left-margin', '60px');
                } else {
                    $("." + str).css('display', 'block');
                }
            } else {$("." + str).css('display', 'none'); }
           
        }


    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" ViewStateMode="Enabled"
    runat="server">
    <asp:ScriptManager ID="projectTemplateScriptManager" runat="server" EnablePageMethods="true" AsyncPostBackTimeout ="6000">
    </asp:ScriptManager>
    <a id="TB_closeWindowButton" class="close" style="position: absolute; z-index: 1000;
        left: -1000em">close</a>
    <div class="search">
        <div class="wrapper">
        </div>
    </div>
    <asp:UpdatePanel ID="projectTemplateUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" />
                    </div>
                    <div class="middle">
                        <asp:HyperLink ID="backToProjectTemplateHyperLink" runat="server" NavigateUrl="~/Admin/ProjectTemplates.aspx">‹‹ Back to Project Templates</asp:HyperLink>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                            <div class="field">
                                <asp:Label ID="brandLabel" runat="server" class="label" Text="Brand:" CBID="1102"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="brandDropDown" runat="server" ViewStateMode="Enabled" AutoPostBack="true"
                                        OnSelectedIndexChanged="BrandDropDown_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <div>
                                        <asp:RequiredFieldValidator ID="brandRequiredFieldValidator" SetFocusOnError="true"
                                            VMTI="0" runat="server" ErrorMessage="Please select a brand" ValidationGroup="GoClick"
                                            ControlToValidate="brandDropDown" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="regionLabel" runat="server" class="label" Text="Region:" CBID="1103"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="regionDropDown" AutoPostBack="true" ViewStateMode="Enabled"
                                        OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged" runat="server">
                                    </asp:DropDownList>
                                    <div>
                                        <asp:RequiredFieldValidator ID="regionRequiredFieldValidator" SetFocusOnError="true"
                                            VMTI="0" runat="server" ErrorMessage="Please select a region" ValidationGroup="GoClick"
                                            ControlToValidate="regionDropDown" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                             <div class="field">
                                <asp:Label ID="countrylabel" runat="server" CssClass="label" CBID="1300" Text="Country :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled" 
                                        AutoPostBack="false" runat="server">
                                    </asp:DropDownList>
                                    <div>
                                        <asp:RequiredFieldValidator ID="countryRequiredFieldValidator" SetFocusOnError="true"
                                            VMTI="0" runat="server" ErrorMessage="Please select a country" ValidationGroup="GoClick"
                                            ControlToValidate="countryDropDown" InitialValue="0" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="visibility:hidden">
                                <asp:Label Visible ="false"  ID="propertyType" runat="server" class="label" Text="Property Type:" CBID="1104"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList Visible="false"  ID="propertyTypeDropDown" runat="server" ViewStateMode="Enabled"
                                        AutoPostBack="true" >
                                    </asp:DropDownList>
                                    <div>
                                        <asp:RequiredFieldValidator Visible="false"  ID="propertyTypeRequiredFieldValidator" SetFocusOnError="true"
                                            VMTI="0" runat="server" ErrorMessage="Please select a property type" ValidationGroup="GoClick"
                                            ControlToValidate="propertyTypeDropDown" InitialValue="0" CssClass="errorText"
                                            Display="Dynamic"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="alR searchpad-top">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="btnGo" ValidationGroup="GoClick" runat="server" CBID="1105" Text="Go"
                                            OnClick="GoButton_Click" />
                                    </span>
                                </div>
                                <div class="clear">
                                    &nbsp;</div>
                            </div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="Div1" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" />
                    </div>
                    <div id="resultMessageDiv" runat="server">
                                    </div>
                </div>
                <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
             
            </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
   
    <div id="resultContentDiv" class="content">
        <div class="wrapper">
            <asp:UpdatePanel ID="templatedetailsSectionUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:LinkButton ID="saveProjectTemplateLinkButton" runat="server" class="btn" OnCommand="SaveProjectTemplateLinkButton_Command">Save</asp:LinkButton>
                    <asp:LinkButton ID="addCategoryLinkButton" runat="server" class="thickbox btn addcategory"
                        CommandArgument="none" CommandName="AddCategory" OnClientClick="SetHref()" Text="Add Category"
                        CBID="178"></asp:LinkButton>
                    <asp:LinkButton ID="copyTemplateLinkButton" runat="server" OnClientClick="SetCopyTemplateHref()"
                        class="thickbox btn copytemplate" Text="copy template" CBID="343"></asp:LinkButton>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:LinkButton ID="exportToExcelLinkButton" runat="server" class="btn" OnClick="ExportToExcelLinkButton_Click"
                Text="Export to Excel" CBID="393"></asp:LinkButton>
                 
            <h2>
            <img id="ExpandCollapseLink" src="../Images/project-icon-transBG.png" onclick="ExpandCollapse()"><input type="hidden" id="hidExpandCollapse" value="0" />
                <asp:Label ID="categoriesHeadingLabel" runat="server" class="label" Text="Categories"
                    CBID="278"></asp:Label></h2>
            <div class="inner-content">
                <div class="no-space">
                    <div runat="server" class="categories">
                        <div id="categoriesDivision" runat="server" style="width: 963px">
                            <asp:UpdatePanel ID="templateInfoUpdatePanel" runat="server" UpdateMode="Conditional">
                             
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" class="cat-table cat-table-heading">
                                        <tr>
                                            <th class="col-1">
                                                <h4>
                                                    <asp:Literal runat="server" ID="brandLiteral"></asp:Literal>
                                                </h4>
                                                <p>
                                                   <%-- <asp:Literal runat="server" ID="propertyTypeLiteral"></asp:Literal>--%>
                                                    
                                                    <asp:Literal runat="server" ID="regionLiteral"></asp:Literal>
                                                    <asp:Literal runat="server" ID="countryLiteral"></asp:Literal></p>
                                            </th>
                                            <th class="col-2">
                                                <asp:Label ID="isActiveLabel" CBID="173" runat="server" Text="Active?"></asp:Label>
                                            </th>
                                            <th class="col-3">
                                                <asp:Label ID="requiredLabel" CBID="626" runat="server" Text="Required?"></asp:Label>
                                            </th>
                                            <th class="col-4">
                                                <asp:Label ID="newPartnersLabel" CBID="453" runat="server" Text="New Partners?"></asp:Label>
                                            </th>
                                            <th class="col-5">
                                                <asp:Label ID="newProductsLabel" CBID="455" runat="server" Text="New Products?"></asp:Label>
                                            </th>
                                        </tr>
                                        <tr >
                                          <th class="col-1"></th>
                                          <th class="col-2">
                                              <asp:CheckBox ID="isActiveAllCheck" 
                                                   runat="server" AutoPostBack="true" oncheckedchanged="isActiveAllCheck_CheckedChanged" 
                                                 OnClick="isActiveAllCheck();" />
                                                  </div></th>
                                          <th class="col-3"><input id="isRequiredAllCheck" type="checkbox" name="isRequiredAll" onclick="isRequiredAllCheck_clicked()"  /></th>
                                          <th class="col-4"><input id="NewPartnerAllCheck" type="checkbox" name="newPartnersAll" onclick="newPartnersAllCheck_clicked()" /></th>
                                          <th class="col-5"><input id="NewProductsAllCheck" type="checkbox" name="newProductsAll" onclick="newProductsAllCheck_clicked()" /></th>
                                        </tr>
                                    </table>
                                    <asp:HiddenField runat="server" ID="hidactiveCheckCheckedIds" />
                                    <asp:HiddenField runat="server" ID="hidactiveCheckUnCheckedIds" />
                                    <asp:HiddenField runat="server" ID="hidWhichChkBoxClicked" />
                                    <asp:HiddenField ID="selectedProjectTemplateHiddenValue" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="categoryListUpdatePanel" runat="server" ViewStateMode="Enabled"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Repeater ID="categoryDivisionsRepeater" runat="server" OnItemDataBound="CategoryDivisionsRepeater_ItemDataBound">
                                        <ItemTemplate>
                                            <div id="categoryListDivision" runat="server">
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                            
</asp:Content>