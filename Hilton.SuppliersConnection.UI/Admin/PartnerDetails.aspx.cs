﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class PartnerDetails : PageBase
    {
        private IPartnerManager _partnerManager;


        /// <summary>
        /// Register the controls for the postback 
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            try
            {
                IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
                brands.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(optgroup, p.DataValueField)));
                IList<Category> categories = HelperManager.Instance.GetActiveCategories;
                categories.ToList().ForEach(p => Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(categoryDropDown, p.CategoryId.ToString(CultureInfo.InvariantCulture))));
                base.Render(writer);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedParentCategories_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                var cats = sender as TextBox;
                var CatNames = cats.Text.Split(',');
                if (CatNames[1] != "null")
                    OnSelectedIndexChanged(CatNames[0], cats.Text);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Bind the data to dynamic created category
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            categoryDropDown.DataSource = categoryList;
            categoryDropDown.DataTextField = "CategoryName";
            categoryDropDown.DataValueField = "CategoryId";
            categoryDropDown.DataBind();
        }

        /// <summary>
        /// To get product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (GetViewState<string>(UIConstants.Mode) == UIConstants.Edit)
                {
                    Partner partner = _partnerManager.GetPartnerImage(Convert.ToInt32(partnerIdHiddenField.Value));
                    ProcessFileDownload("image", partner.CompanyLogoImage, partner.CompanyLogoName);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "SpecSheetScript1", "SetFileInputStyles();", true);
        }

        /// <summary>
        /// To Process file download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            string extension = string.Empty;

            switch (fileType)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "image":
                    string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    int upperBound = tempString.GetUpperBound(0);
                    contentType = "image/" + tempString[upperBound];
                    break;
                default:
                    contentType = "";
                    break;
            };

            Response.Buffer = true;
            Response.Charset = "";
            Response.ClearHeaders();
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
            Response.BinaryWrite(fileBytes);
            Response.Flush();
            Response.Clear();
            Response.End();
        }

        /// <summary>
        /// Add the selected country to the country grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddCategoryCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ListItem selectedRegion = ((ListControl)(sender)).SelectedItem;
                IList<DropDownItem> countries = GetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState) ?? new List<DropDownItem>();

                if (countries.Any(p => p.DataValueField == selectedRegion.Value) == false && selectedRegion.Value != DropDownConstants.DefaultValue)
                {
                    countries.Add(new DropDownItem()
                    {
                        DataTextField = selectedRegion.Text,
                        DataValueField = selectedRegion.Value
                    });
                    SetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState, countries);
                    tobeMappedCountriesGridView.DataSource = countries;
                    tobeMappedCountriesGridView.DataBind();
                }
                if (selectedRegion.Value == DropDownConstants.DefaultValue)
                {
                    countries.Clear();
                    SetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState, countries);
                    tobeMappedCountriesGridView.DataSource = countries;
                    tobeMappedCountriesGridView.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Delete the categories mapping
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteCategoryLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.IsValid)
                {
                    var selBrands = selectedBrands.Text.Split(',').ToList();
                    var selRegions = selectedRegions.Text.Split(',').ToList();
                    int catLevel = FindOccurence("selectedCategories");

                    var selCategories = catLevel == 0 ? selectedParentCategories.Text.Split(',').ToList() :
                        ((TextBox)parentCategoryContainer.FindControl("selectedCategories-" + (catLevel + 1).ToString(CultureInfo.InvariantCulture))).Text == string.Empty
                        ? ((TextBox)parentCategoryContainer.FindControl("selectedCategories-" + catLevel)).Text.Split(',').ToList() :
                         ((TextBox)parentCategoryContainer.FindControl("selectedCategories-" + (catLevel + 1).ToString(CultureInfo.InvariantCulture))).Text.Split(',').ToList();
                    selCategories.RemoveAt(0);
                    if (selCategories.Count == 1)
                    {
                        if (selCategories[0] == "null") return;
                    }
                    var allLeafCategories = string.Empty;
                    foreach (var subCat in selCategories)
                    {
                        allLeafCategories += GetAllLeafCategories(subCat) + ",";
                        allLeafCategories.Replace(",,", ",");
                    }
                    var finalCatList = allLeafCategories.Split(',').ToList();
                    finalCatList.RemoveAll(str => String.IsNullOrEmpty(str));


                    var categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
                    //Get the master data for drop downs
                    DropDownList brandDropDownlist = new DropDownList();
                    IList<DropDownItem> brands = GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
                    DropDownList regionDropDownlist = new DropDownList();
                    IList<DropDownItem> regions = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
                    DropDownList countryDropDownList = new DropDownList();
                    IList<DropDownItem> allCountries = GetStaticDropDownDataSource(DropDownConstants.CountryDropDown);

                    if (String.IsNullOrEmpty(partnerIdHiddenField.Value))
                    {
                        partnerIdHiddenField.Value = "0";
                    }

                    //Get the existing partner categories
                    var partnerCategories = GetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState) ?? new List<PartnerCategories>();
                    string selectedCatList = string.Empty;
                    foreach (string category in finalCatList)
                    {
                        if (partnerCategories.FirstOrDefault(p => p.CategoryId.ToString() == category) != null)
                        {
                            selectedCatList += category + ",";
                        }
                    }

                    var currentCatList = selectedCatList.Split(',').ToList();

                    foreach (var category in categories.Where(p => currentCatList.Contains(p.CategoryId.ToString(CultureInfo.InvariantCulture)) && p.IsLeaf == true))
                    {
                        PartnerCategories partnerCategory = new PartnerCategories()
                        {
                            CategoryDisplayName = category.ParentCategoryHierarchy,
                            CategoryId = category.CategoryId,
                            PartnerCategoriesDetails = new List<PartnerCategoryDetails>()
                        };

                        IList<DropDownItem> toBeMappedcountries = GetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState) ?? new List<DropDownItem>();
                        List<string> adminSelectedRegions = toBeMappedcountries.GroupBy(c => c.ParentDataField).Select(group => group.First().ParentDataField).ToList();

                        foreach (DropDownItem brand in brands.Where(p => selBrands.Contains(p.DataValueField)))
                        {
                            foreach (DropDownItem region in regions.Where(p => adminSelectedRegions.Contains(p.DataValueField)))
                            {
                                partnerCategory.PartnerCategoriesDetails.Add(new PartnerCategoryDetails()
                                {
                                    BrandId = Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture),
                                    BrandDescription = brand.DataTextField,
                                    RegionId = Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture),
                                    RegionDescription = region.DataTextField,
                                    IsAvailable = true,
                                    CategoryId = category.CategoryId,
                                });
                            }
                        }

                        //add the countries

                        foreach (PartnerCategoryDetails region in partnerCategory.PartnerCategoriesDetails)
                        {
                            var countries = region.Countries = new List<Country>();
                            foreach (DropDownItem country in toBeMappedcountries)
                            {
                                if (allCountries.Any(p => p.DataValueField == country.DataValueField && p.ParentDataField == region.RegionId.ToString(CultureInfo.InvariantCulture)))
                                {
                                    countries.Add(new Country()
                                    {
                                        CountryId = Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture),
                                        CountryName = Convert.ToString(country.DataTextField, CultureInfo.InvariantCulture)
                                    });
                                }
                            }
                            region.Countries = countries;
                        }

                        //bool isNewCategory = true;
                        foreach (PartnerCategories item in partnerCategories)
                        {
                            if (category.CategoryId == item.CategoryId)
                            {
                                //isNewCategory = false;
                                //code to update the category
                                foreach (PartnerCategoryDetails details in item.PartnerCategoriesDetails)
                                {
                                    foreach (PartnerCategoryDetails partnerCatDetail in partnerCategory.PartnerCategoriesDetails)
                                    {
                                        if (details.BrandId == partnerCatDetail.BrandId && details.RegionId == partnerCatDetail.RegionId)
                                        {
                                            foreach (var country in partnerCatDetail.Countries)
                                            {
                                                if (details.Countries.Any(p => p.CountryId == country.CountryId))
                                                {
                                                    details.Countries = details.Countries.Except(details.Countries.Where(p => country.CountryId == p.CountryId && country.CountryName == p.CountryName)).ToList();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    var newPartnerCategories = new List<PartnerCategories>();
                    foreach (var cat in partnerCategories)
                    {
                        if (cat.PartnerCategoriesDetails.Any(catdetails => catdetails.Countries.Count > 0))
                        {
                            newPartnerCategories.Add(new PartnerCategories()
                            {
                                CategoryId = cat.CategoryId,
                                PartnerCategoriesDetails = new List<PartnerCategoryDetails>(),
                                PartnerId = cat.PartnerId,
                                CategoryDisplayName = cat.CategoryDisplayName
                            });
                            foreach (var catDetails in cat.PartnerCategoriesDetails)
                            {
                                if (catDetails.Countries.Count() > 0)
                                {
                                    newPartnerCategories.Where(c => c.CategoryId == cat.CategoryId).FirstOrDefault().PartnerCategoriesDetails.Add(catDetails);
                                }
                            }
                        }
                    }
                    //monia
                    if (newPartnerCategories.Count == 0)
                    {
                        ConfigureResultMessage(categoryMappingResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.MapCategoryErrorMessage));
                        PopulateCategoryDetails(Convert.ToInt32(partnerIdHiddenField.Value));
                    }
                    else
                    {
                        categoryRepater.DataSource = newPartnerCategories;
                        categoryRepater.DataBind();
                        ConfigureResultMessage(categoryMappingResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerUnmappedwithCategory));
                        SetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState, newPartnerCategories);
                    }
                    mappedCategoryUpdatePanel.Update();
                    categoriesMappingUpdatePanel.Update();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Add the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddCategoryLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.IsValid)
                {
                    var selBrands = selectedBrands.Text.Split(',').ToList();
                    var selRegions = selectedRegions.Text.Split(',').ToList();

                    int catLevel = FindOccurence("selectedCategories");

                    var selCategories = catLevel == 0 ? selectedParentCategories.Text.Split(',').ToList() :
                        ((TextBox)parentCategoryContainer.FindControl("selectedCategories-" + (catLevel + 1).ToString(CultureInfo.InvariantCulture))).Text == string.Empty
                        ? ((TextBox)parentCategoryContainer.FindControl("selectedCategories-" + catLevel)).Text.Split(',').ToList() :
                         ((TextBox)parentCategoryContainer.FindControl("selectedCategories-" + (catLevel + 1).ToString(CultureInfo.InvariantCulture))).Text.Split(',').ToList();
                    selCategories.RemoveAt(0);
                    if (selCategories.Count == 1)
                    {
                        if (selCategories[0] == "null") return;
                    }
                    var allLeafCategories = string.Empty;
                    foreach (var subCat in selCategories)
                    {
                        allLeafCategories += GetAllLeafCategories(subCat) + ",";
                        allLeafCategories.Replace(",,", ",");
                    }
                    var finalCatList = allLeafCategories.Split(',').ToList();
                    finalCatList.RemoveAll(str => String.IsNullOrEmpty(str));
                    var categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

                    DropDownList brandDropDownlist = new DropDownList();
                    IList<DropDownItem> brands = GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
                    DropDownList regionDropDownlist = new DropDownList();
                    IList<DropDownItem> regions = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
                    DropDownList countryDropDownList = new DropDownList();
                    IList<DropDownItem> allCountries = GetStaticDropDownDataSource(DropDownConstants.CountryDropDown);

                    if (String.IsNullOrEmpty(partnerIdHiddenField.Value))
                    {
                        partnerIdHiddenField.Value = "0";
                    }

                    var partnerCategories = GetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState) ?? new List<PartnerCategories>();
                    // foreach (var category in categories.Where(p => finalCatList.Contains(p.CategoryId.ToString(CultureInfo.InvariantCulture)) && p.IsLeaf == true))
                    foreach (string catid in finalCatList)
                    {
                        var category = categories.FirstOrDefault(p => p.CategoryId.ToString() == catid && p.IsLeaf == true);
                        PartnerCategories partnerCategory = new PartnerCategories();

                        partnerCategory.CategoryDisplayName = category.ParentCategoryHierarchy;
                        partnerCategory.CategoryId = category.CategoryId;
                        partnerCategory.PartnerId = Convert.ToInt32(partnerIdHiddenField.Value);
                        partnerCategory.PartnerCategoriesDetails = new List<PartnerCategoryDetails>();

                        //add the countries
                        IList<DropDownItem> toBeMappedcountries = GetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState) ?? new List<DropDownItem>();
                        List<string> adminSelectedRegions = toBeMappedcountries.GroupBy(c => c.ParentDataField).Select(group => group.First().ParentDataField).ToList();

                        foreach (DropDownItem brand in brands.Where(p => selBrands.Contains(p.DataValueField)))
                        {
                            foreach (DropDownItem region in regions.Where(p => adminSelectedRegions.Contains(p.DataValueField)))
                            {
                                partnerCategory.PartnerCategoriesDetails.Add(new PartnerCategoryDetails()
                                {
                                    BrandId = Convert.ToInt32(brand.DataValueField, CultureInfo.InvariantCulture),
                                    BrandDescription = brand.DataTextField,
                                    RegionId = Convert.ToInt32(region.DataValueField, CultureInfo.InvariantCulture),
                                    RegionDescription = region.DataTextField,
                                    IsAvailable = true,
                                    CategoryId = category.CategoryId,
                                });
                            }
                        }

                        foreach (PartnerCategoryDetails region in partnerCategory.PartnerCategoriesDetails)
                        {
                            var countries = region.Countries = new List<Country>();
                            foreach (DropDownItem country in toBeMappedcountries)//.Where( p => allCountries.Where( x => x.DataValueField == p.DataValueField && x.ParentDataField == region.RegionId.ToString(CultureInfo.InvariantCulture))  ))
                            {
                                if (allCountries.Any(p => p.DataValueField == country.DataValueField && p.ParentDataField == region.RegionId.ToString(CultureInfo.InvariantCulture)))
                                {
                                    countries.Add(new Country()
                                    {
                                        CountryId = Convert.ToInt32(country.DataValueField, CultureInfo.InvariantCulture),
                                        CountryName = Convert.ToString(country.DataTextField, CultureInfo.InvariantCulture)
                                    });
                                }
                            }
                        }

                        bool isNewCategory = true;
                        foreach (PartnerCategories item in partnerCategories)
                        {
                            if (category.CategoryId == item.CategoryId)
                            {
                                isNewCategory = false;
                                //code to update the category
                                foreach (PartnerCategoryDetails details in item.PartnerCategoriesDetails)
                                {
                                    //details.Countries.Clear();
                                    foreach (PartnerCategoryDetails partnerCatDetail in partnerCategory.PartnerCategoriesDetails)
                                    {
                                        if (details.BrandId == partnerCatDetail.BrandId && details.RegionId == partnerCatDetail.RegionId)
                                        {

                                            foreach (var country in partnerCatDetail.Countries)
                                            {
                                                if (!details.Countries.Any(p => p.CountryId == country.CountryId))
                                                {
                                                    details.Countries.Add(country);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                foreach (var cat in partnerCategory.PartnerCategoriesDetails)
                                {
                                    if (!item.PartnerCategoriesDetails.Any(p => p.BrandId == cat.BrandId && p.RegionId == cat.RegionId))
                                        item.PartnerCategoriesDetails.Add(cat);
                                }
                            }
                        }
                        if (isNewCategory)
                        {
                            partnerCategories.Add(partnerCategory);
                        }
                    }
                    categoryRepater.DataSource = partnerCategories;
                    categoryRepater.DataBind();
                    SetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState, partnerCategories);  //#INC000015714254 
                    mappedCategoryUpdatePanel.Update();
                    ConfigureResultMessage(categoryMappingResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerMappedwithCategory));
                    categoriesMappingUpdatePanel.Update();

                    partnerCategories = null;

                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// GetAllLeafCategories
        /// </summary>
        /// <param name="selCategory"></param>
        /// <returns></returns>
        private string GetAllLeafCategories(string selCategory)
        {
            var categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);
            if (categories.Any(p => p.CategoryId.ToString(CultureInfo.InvariantCulture) == selCategory && p.IsLeaf == true))
                return selCategory;
            else
            {
                var subcat = string.Empty;
                var subCategories = categories.Where(p => p.ParentCategoryId.ToString(CultureInfo.InvariantCulture) == selCategory);
                foreach (var subCat in subCategories)
                {
                    subcat += GetAllLeafCategories(subCat.CategoryId.ToString(CultureInfo.InvariantCulture)) + ",";
                }
                return subcat;
            }

        }


        /// <summary>
        /// Set the value for category availability
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AvailableDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] args = ((HiddenField)(((ListControl)(sender)).Parent.
                        FindControl("availableDropDownHiddenField"))).Value.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                int categoryId = int.Parse(args[0], CultureInfo.InvariantCulture);
                int brandId = int.Parse(args[1], CultureInfo.InvariantCulture);
                int regionId = int.Parse(args[2], CultureInfo.InvariantCulture);
                var isAvailble = ((DropDownList)sender).SelectedValue == "1" ? true : false;
                var partnerCategories = GetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState) ?? new List<PartnerCategories>();
                partnerCategories.Where(p => p.CategoryId == categoryId).FirstOrDefault().PartnerCategoriesDetails.
                    Where(p => p.BrandId == brandId && p.RegionId == regionId).FirstOrDefault().IsAvailable = isAvailble;
                SetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState, partnerCategories);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Validate the fields required to add category
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void CategoryAdditionValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                categoryAdditionValidator.ErrorMessage = string.Empty;
                int count = FindOccurence("selectedCategories") + 1;//FindOccurence("categoryDropDown") + 1;
                TextBox selCategory = null;
                if (count == 1)
                    selCategory = (TextBox)parentCategoryContainer.FindControl("selectedParentCategories");
                else
                    selCategory = (TextBox)parentCategoryContainer.FindControl("selectedCategories-" + count.ToString(CultureInfo.InvariantCulture));
                if (string.IsNullOrWhiteSpace(selCategory.Text))
                {
                    args.IsValid = false;
                    if (string.IsNullOrWhiteSpace(categoryAdditionValidator.ErrorMessage))
                    {
                        categoryAdditionValidator.ErrorMessage = "Please select category or subcategory.";
                    }
                    else
                    {
                        categoryAdditionValidator.ErrorMessage = "<br />" + "Please select category or subcategory.";
                    }
                }
                if (string.IsNullOrWhiteSpace(selectedBrands.Text))
                {
                    args.IsValid = false;
                    if (string.IsNullOrWhiteSpace(categoryAdditionValidator.ErrorMessage))
                    {
                        categoryAdditionValidator.ErrorMessage = "Please select brand.";
                    }
                    else
                    {
                        categoryAdditionValidator.ErrorMessage = "<br />" + "Please select brand.";
                    }
                }
                if (string.IsNullOrWhiteSpace(selectedRegions.Text))
                {
                    args.IsValid = false;
                    if (string.IsNullOrWhiteSpace(categoryAdditionValidator.ErrorMessage))
                    {
                        categoryAdditionValidator.ErrorMessage = "Please select Region.";
                    }
                    else
                    {
                        categoryAdditionValidator.ErrorMessage = "<br />" + "Please select Region.";
                    }
                }
                if (string.IsNullOrWhiteSpace(selectedCountries.Text))
                {
                    if (string.IsNullOrWhiteSpace(categoryAdditionValidator.ErrorMessage))
                    {
                        categoryAdditionValidator.ErrorMessage = "Please select Country.";
                    }
                    else
                    {
                        categoryAdditionValidator.ErrorMessage = "<br />" + "Please select Country.";
                    }
                    args.IsValid = false;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Remove image link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageLinkButton.Text = string.Empty;
                imageNameHiddenField.Value = string.Empty;
                companyLogo.ImageUrl = "../Images/NoProduct.PNG";
                ClearSession(SessionConstants.ProductImageBytes);
                removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "ImageUpload", "SetFileInputStyles();", true);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedRegions_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                IList<DropDownItem> countries = GetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState) ?? new List<DropDownItem>();
                countries.Clear();

                var selRegion = selectedRegions.Text.Split(',').Select(item => item.ToString(CultureInfo.InvariantCulture));
                var allRegions = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
                //DropDownList region = (DropDownList)sender;
                //If default value is not selected then update the category country drop down
                //when default value is selected then disable the country drop down
                // var regionList = selectedRegions.Text.Split(',').Select(item => int.Parse(item));

                string[] regions = selectedRegions.Text.Split(',');
                //BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1212, culture), 0, true, regions);

                IList<DropDownItem> allCountries = GetStaticDropDownDataSource(DropDownConstants.CountryDropDown);
                IList<DropDownItem> countriesForSelectedRegions = new List<DropDownItem>();
                countriesForSelectedRegions = allCountries.Where(x => regions.Contains(x.ParentDataField)).ToList<DropDownItem>();

                SetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState, countriesForSelectedRegions);



                tobeMappedCountriesGridView.DataSource = countriesForSelectedRegions;
                tobeMappedCountriesGridView.DataBind();
                categoryCountryDropDown.DataSource = countriesForSelectedRegions;
                categoryCountryDropDown.DataTextField = "DataTextField";
                categoryCountryDropDown.DataValueField = "DataValueField";
                categoryCountryDropDown.DataBind();
                selectedCountries.Text = string.Join(",", countriesForSelectedRegions.Select(p => p.DataValueField));
                if (selRegion.Count() != allRegions.Count())
                {
                    categoryCountryDropDown.Enabled = true;
                    chooseCategoryUpdatePanel.Update();
                }
                else
                {
                    categoryCountryDropDown.Enabled = false;
                    chooseCategoryUpdatePanel.Update();
                }

            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Will be fired when the text change event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectedCountries_TextChanged(Object sender, EventArgs e)
        {
            try
            {
                IList<DropDownItem> countries = GetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState) ?? new List<DropDownItem>();
                var allCountries = GetStaticDropDownDataSource(DropDownConstants.CountryDropDown);
                var selCountries = selectedCountries.Text.Split(',').Select(item => item.ToString(CultureInfo.InvariantCulture));

                if (allCountries.Count() != selCountries.Count())
                {
                    foreach (var country in allCountries.Where(p => selCountries.Contains(p.DataValueField)))
                    {
                        if (countries.Any(p => p.DataValueField == country.DataValueField) == false && country.DataValueField != DropDownConstants.DefaultValue)
                        {
                            countries.Add(new DropDownItem()
                            {
                                DataTextField = country.DataTextField,
                                DataValueField = country.DataValueField
                            });
                        }
                    }
                    var dupCountries = new List<DropDownItem>();
                    foreach (var country in countries)
                    {
                        dupCountries.Add(country);
                    }
                    foreach (var country in dupCountries)
                    {
                        if (selCountries.Any(p => p == country.DataValueField) == false)
                        {
                            countries.Remove(country);
                        }
                    }
                }
                else
                {
                    countries.Clear();
                }
                SetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState, countries);
                tobeMappedCountriesGridView.DataSource = countries;
                tobeMappedCountriesGridView.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }


        /// <summary>
        /// Link the web accounts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkWebAccountGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = ResourceUtility.GetLocalizedString(449, culture, "Name");
                    e.Row.Cells[1].Text = ResourceUtility.GetLocalizedString(415, culture, "Hilton ID");
                    e.Row.Cells[2].Text = ResourceUtility.GetLocalizedString(382, culture, "Email address");
                }
                if (e.Row.DataItem != null)
                {
                    Label userName = (Label)e.Row.FindControl("userNameLabel");
                    LinkButton sendEmailLinkButton = (LinkButton)e.Row.FindControl("sendEmailLinkButton");
                    sendEmailLinkButton.Text = ResourceUtility.GetLocalizedString(665, culture, "Send Activation Email");

                    var dataItem = (PartnerLinkedWebAccount)(e.Row.DataItem);
                    if (dataItem != null)
                    {
                        userName.Text = dataItem.FirstName + ' ' + dataItem.LastName;
                        if (dataItem.StatusDesc.ToLower() == "active" || dataItem.StatusDesc.ToLower() == "inactive")
                        {
                            sendEmailLinkButton.Visible = false;
                        }
                        else
                        {
                            sendEmailLinkButton.CommandArgument = dataItem.PartnerId.ToString(CultureInfo.InvariantCulture) + "," + dataItem.UserId.ToString(CultureInfo.InvariantCulture);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Remove the user project association
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelImage_Click(Object sender, EventArgs e)
        {
            try
            {
                int partnerId = Convert.ToInt32(partnerIdHiddenField.Value, CultureInfo.InvariantCulture);
                int userId = Convert.ToInt32(((ImageButton)(sender)).CommandArgument, CultureInfo.InvariantCulture);
                var webAccountsDetails = _partnerManager.DeleteLinkedWebAccount(userId, partnerId);

                if (webAccountsDetails.Item2 == 0)
                {
                    linkWebAccountGrid.DataSource = webAccountsDetails.Item1;
                    linkWebAccountGrid.DataBind();
                    ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.UserDeletedSuccessfully, PageBase.culture));
                }
                else
                {
                    ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SendActivationEmailFailure, PageBase.culture));
                }
                linkedUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// Send the payment request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SendEmailLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    string[] args = ((LinkButton)sender).CommandArgument.Split(',');
                    int partnerId = Convert.ToInt32(args[0], CultureInfo.InvariantCulture);
                    int userId = Convert.ToInt32(args[1], CultureInfo.InvariantCulture);
                    string culture = Convert.ToString(PageBase.culture, CultureInfo.InvariantCulture);
                    int result = -1;
                    var webAccounts = _partnerManager.SendActivationEmail(userId, partnerId, culture, ref result);

                    if (result == 0)
                    {
                        linkWebAccountGrid.DataSource = webAccounts;
                        linkWebAccountGrid.DataBind();
                        ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.SendActivationEmailSuccess, PageBase.culture));
                    }
                    else
                    {
                        ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.SendActivationEmailFailure, PageBase.culture));
                    }
                    linkedUpdatePanel.Update();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Send the payment request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SendPaymentRequestLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    PaymentRequest paymentRequest = new PaymentRequest()
                    {
                        GoldAmount = goldAmountTextBox.Text,
                        RegularAmount = regAmountTextBox.Text,
                        RequestSentTo = emailToTextBox.Text,
                        PartnerId = partnerIdHiddenField.Value,
                        Culture = Convert.ToString(culture, CultureInfo.InvariantCulture)
                    };
                    PaymentRequestDetails paymentRequestDetail = _partnerManager.SendPaymentRequest(paymentRequest);
                    if (paymentRequestDetail != null)
                    {
                        sentPaymentRequestTable.Visible = true;
                        SetLabelText(sentDateLabel, paymentRequestDetail.RequestSentOn.ToString("dd-MMM-yyyy, h:mm tt", CultureInfo.InvariantCulture));
                        SetLabelText(sentToEmailLabel, paymentRequestDetail.RequestSentTo);
                        SetTextBoxText(emailToTextBox, string.Empty);
                        ConfigureResultMessage(sentPaymentRequestResultDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.PaymentRequestSentSuccess, culture));
                    }
                    else
                    {
                        ConfigureResultMessage(sentPaymentRequestResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.PaymentRequestSentError, culture));
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(sentPaymentRequestResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Validate the user addition
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddWebAccountLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    //Get the first name and last name
                    string[] name = addNameTextBox.Text.Split(' ');
                    string firstName = (string)name[0];
                    string lastName = string.Empty;
                    string hiltonIdValidationString = ControlValidatorUtility.GetLocalizedRegularExpression((int)RegularExpressionTypeEnum.HiltonIdValidator, culture);
                    Regex hiltonIdValidator = new Regex(hiltonIdValidationString);
                    for (int length = 1; length < name.Length; length++)
                    {
                        if (length == 1)
                        {
                            lastName = name[1];
                        }
                        else
                        {
                            lastName += " " + name[length];
                        }
                    }
                    if (String.IsNullOrEmpty(lastName.Trim()))
                    {
                        lastNameErrorLabel.Visible = true;
                    }
                    else if (addHiltonIdTextBox.Text == ResourceUtility.GetLocalizedString(415, culture, "Hilton ID") || hiltonIdValidator.IsMatch(addHiltonIdTextBox.Text))
                    {
                        hiltonIdFormatErrorLabel.Visible = false;
                        lastNameErrorLabel.Visible = false;
                        User user = GetSession<User>(SessionConstants.User);

                        var webAccount = new PartnerLinkedWebAccount()
                        {
                            Email = addEmailTextBox.Text.Trim(),
                            HiltonId = addHiltonIdTextBox.Text.Trim() == ResourceUtility.GetLocalizedString(415, culture, "Hilton ID") ? string.Empty : addHiltonIdTextBox.Text.Trim(),
                            FirstName = firstName.Trim(),
                            LastName = lastName.Trim(),
                            PartnerId = Convert.ToInt32(partnerIdHiddenField.Value),
                            CreatedByUserId = user.UserId,
                            Culture = Convert.ToString(culture, CultureInfo.InvariantCulture)
                        };
                        int result = -1;
                        var webAccounts = _partnerManager.LinkedWebAccount(webAccount, ref result);

                        //linkWebAccountErrorMessageLabel

                        if (result == -1)
                        {
                            ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateHiltonIdErrorMessage, culture));
                        }
                        else if (result == -2)
                        {
                            ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.InactiveUser, culture));
                        }
                        else if (result == -3)
                        {
                            ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.UserExists, culture));
                        }
                        else if (result == -4)
                        {
                            ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserAdditionFailed, culture));
                        }
                        else
                        {
                            linkWebAccountGrid.DataSource = webAccounts;
                            linkWebAccountGrid.DataBind();
                            // Reset controls
                            addNameTextBox.Text = ResourceUtility.GetLocalizedString(449, culture, "Name");
                            addHiltonIdTextBox.Text = ResourceUtility.GetLocalizedString(415, culture, "Hilton ID");
                            addEmailTextBox.Text = ResourceUtility.GetLocalizedString(382, culture, "Email address");

                            addNameTextBox.Style.Add("color", "#888");
                            addHiltonIdTextBox.Style.Add("color", "#888");
                            addEmailTextBox.Style.Add("color", "#888");

                            ConfigureResultMessage(linkWebAccountErrorMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.UserAdditionSuccess, culture));
                        }
                    }
                    else
                    {
                        hiltonIdFormatErrorLabel.Visible = true;
                    }
                    linkedUpdatePanel.Update();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Bound the categories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RepeaterItem item = e.Item;
                Repeater repeaterDetails;
                if (((item.ItemType == ListItemType.Item) ||
                    (item.ItemType == ListItemType.AlternatingItem)) && item.ItemType != ListItemType.Header)
                {
                    repeaterDetails = (Repeater)(item.FindControl("detailsRepeater"));

                    repeaterDetails.DataSource = ((PartnerCategories)item.DataItem).PartnerCategoriesDetails;
                    repeaterDetails.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Set the state of state drop down and zip code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList country = (DropDownList)sender;
                if (country.SelectedItem.Value != DropDownConstants.UnitedStates)
                {
                    stateDropDown.SelectedValue = null;
                    if (stateDropDown.Items.Count > 0)
                    {
                        stateDropDown.Items.RemoveAt(0);
                    }
                    stateDropDown.Items.Insert(0, new ListItem(" ", DropDownConstants.DefaultValue));
                    stateDropDown.Enabled = false;
                    //zipLabel.Text = "Postal Code:";
                    stateRequiredFieldValidator.Enabled = false;
                    //zipRequiredFieldValidator.Enabled = true;
                    //zipCodeRegularExpressionValidator.Enabled = true;
                    //zipTextBox.ReadOnly = false;
                    SetTextBoxText(zipTextBox, null);
                    //zipTextBox.Enabled = true;
                    zipTextBoxDiv.Attributes.Remove("class");
                    zipTextBoxDiv.Attributes.Add("class", "input textbox3 small mandatory");
                }
                else
                {
                    if (stateDropDown.Items.Count > 0)
                    {
                        stateDropDown.Items.RemoveAt(0);
                    }
                    stateDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1246, culture, "Select a State"), DropDownConstants.DefaultValue));
                    stateRequiredFieldValidator.Enabled = true;
                    stateDropDown.Enabled = true;
                    zipTextBoxDiv.Attributes.Remove("class");
                    zipTextBoxDiv.Attributes.Add("class", "input textbox3 small mandatory");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Create the drop down list
        /// </summary>
        /// <param name="id"></param>
        protected void CreateDropDownList(int id)
        {
            try
            {
                DropDownList parentCategoryDropDown = new DropDownList();
                parentCategoryDropDown.ID = "categoryDropDown-" + id;
                parentCategoryDropDown.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                parentCategoryDropDown.Attributes.Add("multiple", "multiple");
                parentCategoryDropDown.Attributes.Add("class", "list-5");
                parentCategoryDropDown.ViewStateMode = System.Web.UI.ViewStateMode.Enabled;

                TextBox selectedCategories = new TextBox()
                {
                    ID = "selectedCategories-" + id,
                    ClientIDMode = System.Web.UI.ClientIDMode.Static,
                    ViewStateMode = System.Web.UI.ViewStateMode.Enabled,
                    AutoPostBack = true,
                };

                selectedCategories.TextChanged += new EventHandler(SelectedParentCategories_TextChanged);

                DynamicCategoryDiv.Controls.Add(parentCategoryDropDown);
                DynamicCategoryDiv.Controls.Add(selectedCategories);

                AsyncPostBackTrigger textBoxTrigger = new AsyncPostBackTrigger()
                {
                    ControlID = "selectedCategories-" + id,
                    EventName = "TextChanged"
                };
                chooseCategoryUpdatePanel.Triggers.Add(textBoxTrigger);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Bind the items to detail repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DetailsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RepeaterItem item = e.Item;
                if (((item.ItemType == ListItemType.Item) ||
                    (item.ItemType == ListItemType.AlternatingItem)) && item.ItemType != ListItemType.Header)
                {
                    GridView countryItem = (GridView)(item.FindControl("mappedCountriesGridView"));
                    countryItem.DataSource = ((PartnerCategoryDetails)item.DataItem).Countries;
                    countryItem.DataBind();

                    PopulateRegionalContactIdDropDown(item);

                    //regionDropDownUpdatePanel.Update();
                    //Set the command argument to be used while deleting a property type
                    ((LinkButton)item.FindControl("removeCategoryLinkButton")).CommandArgument = ((PartnerCategories)(((RepeaterItem)((Repeater)(sender)).Parent).DataItem)).CategoryId.ToString(CultureInfo.InvariantCulture) + "," + ((PartnerCategoryDetails)item.DataItem).BrandId.ToString(CultureInfo.InvariantCulture) + "," + ((PartnerCategoryDetails)item.DataItem).RegionId.ToString(CultureInfo.InvariantCulture);

                    //Set the command argument to be used while changing the availability drop down value
                    HiddenField availableDropDownHiddenField = (HiddenField)(item.FindControl("availableDropDownHiddenField"));
                    availableDropDownHiddenField.Value = ((PartnerCategories)(((RepeaterItem)((Repeater)(sender)).Parent).DataItem)).CategoryId.ToString(CultureInfo.InvariantCulture) + "," + ((PartnerCategoryDetails)item.DataItem).BrandId.ToString(CultureInfo.InvariantCulture) + "," + ((PartnerCategoryDetails)item.DataItem).RegionId.ToString(CultureInfo.InvariantCulture);

                    //Populate the availability dropdown
                    DropDownList availability = (DropDownList)(item.FindControl("availableDropDown"));
                    availability.SelectedIndex = ((PartnerCategoryDetails)item.DataItem).IsAvailable == true ? 0 : 1;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Set the command argument for remove country button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MappedCountryGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow item = e.Row;
                if (item.DataItem != null)
                {
                    PartnerCategoryDetails row = (PartnerCategoryDetails)((RepeaterItem)((GridView)(sender)).Parent).DataItem;
                    ((LinkButton)item.FindControl("removeCategoryCountryLinkButton")).CommandArgument = row.CategoryId.ToString(CultureInfo.InvariantCulture) + "," + row.BrandId.ToString(CultureInfo.InvariantCulture) + "," + row.RegionId.ToString(CultureInfo.InvariantCulture) + "," + ((Country)item.DataItem).CountryId;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Initialize the manager instance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                ValidateUserAccess((int)MenuEnum.Partners);
                this.Title = ResourceUtility.GetLocalizedString(1096, culture, "Partner Details");

                _partnerManager = PartnerManager.Instance;

                backToPartnersHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "‹‹ {0}", ResourceUtility.GetLocalizedString(242, culture, "Back to Partners"));
                paymentRequestLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(504, culture, "Payment Request"));
                linkWebAccountLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(786, culture, "Linked Web Accounts"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Dynamic drop down selected index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = (DropDownList)sender;
                string id = ddl.ID;
                int existingDropDownCount = FindOccurence("selectedCategories");
                if (!id.Contains("-"))
                {
                    id = id + "-1";
                }
                string[] idSequence = id.Split('-');

                string currentSelectedCategoryId = ddl.SelectedValue.ToString(CultureInfo.InvariantCulture);
                // remove all the below dynamic drop downs
                for (int dropdownCount = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1; dropdownCount <= existingDropDownCount + 1; dropdownCount++)
                {
                    DropDownList rowDivision = (DropDownList)parentCategoryContainer.FindControl("categoryDropDown-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
                    parentCategoryContainer.Controls.Remove(rowDivision);
                    TextBox selectedCat = (TextBox)parentCategoryContainer.FindControl("selectedCategories-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
                    if (selectedCat != null)
                    {
                        DynamicCategoryDiv.Controls.Remove(selectedCat);
                    }
                }
                if (currentSelectedCategoryId != "0")
                {
                    Category currentSelectedCategory = ReadCategory(currentSelectedCategoryId);
                    if (!(currentSelectedCategory.IsLeaf))
                    {
                        IList<Category> childCategoryList = GetChildCategories(currentSelectedCategoryId);
                        if (childCategoryList.Count() > 0)
                        {
                            int nextID = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1;
                            CreateDropDownList(nextID);
                            FindPopulateDropDowns(currentSelectedCategoryId, "categoryDropDown-" + nextID.ToString(CultureInfo.InvariantCulture));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Dynamic drop down selected index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSelectedIndexChanged(string ddlName, string selectedCategories)
        {
            try
            {
                int existingDropDownCount = FindOccurence("selectedCategories");
                if (!ddlName.Contains("-"))
                {
                    ddlName = ddlName + "-1";
                }
                string[] idSequence = ddlName.Split('-');

                // remove all the below dynamic drop downs
                for (int dropdownCount = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1; dropdownCount <= existingDropDownCount + 1; dropdownCount++)
                {
                    DropDownList rowDivision = (DropDownList)parentCategoryContainer.FindControl("categoryDropDown-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
                    DynamicCategoryDiv.Controls.Remove(rowDivision);
                    TextBox selectedCat = (TextBox)parentCategoryContainer.FindControl("selectedCategories-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
                    if (selectedCat != null)
                    {
                        DynamicCategoryDiv.Controls.Remove(selectedCat);
                    }
                }

                var allCat = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState);

                List<String> selCat = selectedCategories.Split(',').ToList();
                selCat.RemoveAt(0);

                var catLevel = Convert.ToInt32(ddlName.Split('-')[1], CultureInfo.InvariantCulture);
                var count = 0;
                IList<string> selParentCatList = null;
                if (catLevel == 1)
                {
                    count = allCat.Where(item => item.Level.Equals(1)).Count();
                }
                else
                {
                    if (catLevel == 2)
                    {
                        selParentCatList = selectedParentCategories.Text.Split(',').ToList();
                        selParentCatList.RemoveAt(0);
                    }
                    else
                    {
                        TextBox selParentCatTextBox = (TextBox)FindControl(DynamicCategoryDiv, ("selectedCategories-" + (catLevel - 1)).ToString());
                        selParentCatList = selParentCatTextBox.Text.Split(',').ToList();
                        selParentCatList.RemoveAt(0);
                    }
                    count = allCat.Where(item => item.Level.Equals(catLevel) && selParentCatList.Any(p => p == item.ParentCategoryId.ToString(CultureInfo.InvariantCulture))).Count();
                }

                if (count != selCat.Count())
                {
                    var currentSelectedCategories = ReadCategories(selCat.Select(item => int.Parse(item)));
                    FindPopulateDropDowns(currentSelectedCategories, Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// FindPopulateDropDowns
        /// </summary>
        /// <param name="currentSelectedCategories"></param>
        /// <param name="nextID"></param>
        private void FindPopulateDropDowns(IEnumerable<Category> currentSelectedCategories, int nextID)
        {
            IList<Category> childCategoryList = GetChildCategories(currentSelectedCategories);
            if (childCategoryList.Count() > 0)
            {
                CreateDropDownList(nextID);
                DropDownList newDropDown = (DropDownList)FindControl(DynamicCategoryDiv, ("categoryDropDown-" + nextID).ToString());
                BindData(newDropDown, childCategoryList);
            }
        }

        /// <summary>
        /// GetChildCategories
        /// </summary>
        /// <param name="currentSelectedCategories"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(IEnumerable<Category> currentSelectedCategories)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState) ?? new List<Category>();
            IList<Category> categoryList = (categories.AsEnumerable().Where(cat => cat.ParentCategoryId.Equals(currentSelectedCategories.Select(p => p.CategoryId)))).ToList();
            var test = from cat in categories
                       join pCat in currentSelectedCategories on cat.ParentCategoryId equals pCat.CategoryId
                       select cat;
            return test.ToList();

        }

        /// <summary>
        /// Recreate the dynamic controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (parentCategoryContainer == null)
                    parentCategoryContainer = new PlaceHolder();
                RecreateControls("categoryDropDown", "DropDownList");

                GetViewState(ViewStateConstants.CategoriesViewState);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Bind the data to the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                resultMessageDiv.Attributes.Remove("class");
                resultMessageDiv.InnerText = string.Empty;

                if (GetSession<User>(SessionConstants.User) != null)
                {
                    sentPaymentRequestResultDiv.Attributes.Remove("class");
                    sentPaymentRequestResultDiv.InnerHtml = string.Empty;

                    validationErrorMessageDiv.Attributes.Remove("class");
                    validationErrorMessageDiv.InnerHtml = string.Empty;

                    if (addNameTextBox.Text != "Name")
                    {
                        addNameTextBox.Style.Remove("color");
                    }
                    if (addHiltonIdTextBox.Text != "Hilton ID")
                    {
                        addHiltonIdTextBox.Style.Remove("color");
                    }
                    if (addEmailTextBox.Text != "Email address")
                    {
                        addEmailTextBox.Style.Remove("color");
                    }

                    if (!IsPostBack)
                    {
                        addNameTextBox.Style.Add("color", "#888");
                        addHiltonIdTextBox.Style.Add("color", "#888");
                        addEmailTextBox.Style.Add("color", "#888");
                        linkedUpdatePanel.Update();
                        PopulateDropDownData();

                        SetRegularExpressions();

                        // Insert the first item in region id drop down of the regional contact block
                        regionDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1252, culture, "Select"), DropDownConstants.DefaultValue));

                        SetRegionalContactAsReadOnly();

                        if (Context.Items[UIConstants.Mode] != null && Context.Items[UIConstants.Mode].ToString() == UIConstants.Edit)
                        {
                            SetPageSettings(UIConstants.Edit);

                            int partnerId = Convert.ToInt32(Context.Items[UIConstants.PartnerId], CultureInfo.InvariantCulture);
                            SetHiddenFieldValue(partnerIdHiddenField, partnerId.ToString(CultureInfo.InvariantCulture));

                            PopulatePartnerDetails(partnerId);
                            paymentDiv.Visible = true;
                            linked.Visible = true;
                            viewNavigationDiv.Visible = true;
                        }
                        else
                        {
                            SetPageSettings(UIConstants.Add);
                            stateDropDown.Items.Insert(0, new ListItem(" ", DropDownConstants.DefaultValue));
                            stateDropDown.Enabled = false;
                            stateRequiredFieldValidator.Enabled = false;

                            paymentDiv.Visible = false;
                            linked.Visible = false;
                            viewNavigationDiv.Visible = false;
                        }

                        categoryCountryDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1213, culture, "All Countries"), DropDownConstants.DefaultValue));
                        categoryCountryDropDown.Enabled = false;
                        categoryCountryDropDown.Attributes.Add("class", "list-6");
                        chooseCategoryUpdatePanel.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(mappedCategoryUpdatePanel, this.GetType(), "InitializeScript", "InitializeScript();", true);
                        ScriptManager.RegisterClientScriptBlock(partnershipStatusUpdatePanel, this.GetType(), "ForceCall", "ForceCall();", true);
                        ScriptManager.RegisterClientScriptBlock(chooseCategoryUpdatePanel, this.GetType(), "SetCategoryMappingDropDowns", "SetCategoryMappingDropDowns();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// This method will set the validation expression for regular expression validation based on culture
        /// </summary>
        private void SetRegularExpressions()
        {
            // Regular Expression for Name
            SetValidatorRegularExpressions(firstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(lastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(titleRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);
            SetValidatorRegularExpressions(secondaryContactFirstNamerRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(secondaryContactLastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(secondaryContactTitleRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);
            SetValidatorRegularExpressions(secondaryContactPhoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(secondaryContactFaxRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
            SetValidatorRegularExpressions(secondaryContactEmailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);

            SetValidatorRegularExpressions(regionalFirstNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(regionalLastNameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(regionalTitleRegularExpressionValidator, (int)RegularExpressionTypeEnum.TitleValidator);
            SetValidatorRegularExpressions(nameRegularExpressionValidator, (int)RegularExpressionTypeEnum.NameValidator);
            SetValidatorRegularExpressions(phoneRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(phoneRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
            SetValidatorRegularExpressions(faxRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
            SetValidatorRegularExpressions(zipCodeRegularExpressionValidator, (int)RegularExpressionTypeEnum.ZipCodeValidator);
            SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);

            // Regular Expression for email
            SetValidatorRegularExpressions(emailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(regionalEmailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(emailToRegularExpression, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(addEmailRegularExpressionValidator, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(webSiteRegularExpressionValidator, (int)RegularExpressionTypeEnum.WebsiteValidator);

            // Regular Expression for amount
            SetValidatorRegularExpressions(regularAmountRegularExpression, (int)RegularExpressionTypeEnum.AmountTwoDecimalValidator);
            SetValidatorRegularExpressions(goldAmountRegularExpression, (int)RegularExpressionTypeEnum.AmountTwoDecimalValidator);
        }


        /// <summary>
        /// Bind the property type to the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PropertyGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow item = e.Row;
                if (item.DataItem != null)
                {
                    PartnerCategoryDetails row = (PartnerCategoryDetails)((RepeaterItem)((GridView)(sender)).Parent).DataItem;
                    ((LinkButton)item.FindControl("deletePropertyLinkButton")).CommandArgument = row.CategoryId.ToString(CultureInfo.InvariantCulture) + "," + row.BrandId.ToString(CultureInfo.InvariantCulture) + "," + row.RegionId.ToString(CultureInfo.InvariantCulture) + "," + ((PartnerPropertyType)item.DataItem).PropertyTypeId;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Populate the regional contacts as per selected region
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RegiondropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList country = sender as DropDownList;
                int previousSelectedCountry = Convert.ToInt32(GetViewState<string>("previousSelectedCountry"), CultureInfo.InvariantCulture);
                SetViewState<string>("previousSelectedCountry", country.SelectedItem.Value);

                if (previousSelectedCountry != 0)
                {
                    var CountryContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                    var contactToUpdate = CountryContacts.Where(p => p.CountryId == previousSelectedCountry).FirstOrDefault();
                    contactToUpdate.FirstName = regionalFirstNameTextBox.Text;
                    contactToUpdate.LastName = regionalLastNameTextBox.Text;
                    contactToUpdate.Title = regionalTitleTextBox.Text;
                    contactToUpdate.Phone = regionalPhoneTextBox.Text;
                    contactToUpdate.Fax = regionalFaxTextBox.Text;
                    contactToUpdate.Email = regionalEmailTextBox.Text;
                }
                if (country.SelectedItem.Value == DropDownConstants.DefaultValue)
                {
                    SetRegionalContactAsReadOnly();
                }
                else
                {
                    SetRegionalContactAsMandatory();
                    var countryContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                    foreach (Contact contact in countryContacts)
                    {
                        if (country.SelectedItem.Value == contact.CountryId.ToString(CultureInfo.InvariantCulture))
                        {
                            SetTextBoxText(regionalFirstNameTextBox, contact.FirstName);
                            SetTextBoxText(regionalLastNameTextBox, contact.LastName);
                            SetTextBoxText(regionalTitleTextBox, contact.Title);
                            SetTextBoxText(regionalPhoneTextBox, contact.Phone);
                            SetTextBoxText(regionalFaxTextBox, contact.Fax);
                            SetTextBoxText(regionalEmailTextBox, contact.Email);
                            break;
                        }
                    }
                }
                if (country.SelectedItem.Value == DropDownConstants.UnitedStates)
                {
                    regionalFaxTextBox.MaxLength = 32;                                                             //Changed max length to 32(removed 10) TFS#23828
                    regionalPhoneTextBox.MaxLength = 32;
                    SetValidatorRegularExpressions(phoneRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidatorUS);
                    SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidatorUS);

                }
                else
                {
                    regionalFaxTextBox.MaxLength = 32;
                    regionalPhoneTextBox.MaxLength = 32;
                    SetValidatorRegularExpressions(phoneRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.PhoneValidator);
                    SetValidatorRegularExpressions(faxRegionalRegularExpressionValidator, (int)RegularExpressionTypeEnum.FaxValidator);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Remove the category country
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveCategoryCountryLinkButton_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                string[] args = ((LinkButton)sender).CommandArgument.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                int categoryId = int.Parse(args[0], CultureInfo.InvariantCulture);
                int brandId = int.Parse(args[1], CultureInfo.InvariantCulture);
                int regionId = int.Parse(args[2], CultureInfo.InvariantCulture);
                int countryId = int.Parse(args[3], CultureInfo.InvariantCulture);
                IList<Country> countries;
                var partnerCategories = GetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState) ?? new List<PartnerCategories>();

                PartnerCategories partnerCategoryItem = (PartnerCategories)partnerCategories.Where(i => i.CategoryId.Equals(categoryId)).First();
                IList<PartnerCategoryDetails> partnerCategoryDetails = partnerCategoryItem.PartnerCategoriesDetails;
                PartnerCategoryDetails filteredPartnerCategoryDetails
                            = (PartnerCategoryDetails)partnerCategoryDetails.
                                                                        Where(i => i.BrandId.Equals(brandId)
                                                                        && i.RegionId.Equals(regionId)).First();

                countries = filteredPartnerCategoryDetails.Countries;

                if (countries.Where(i => i.CountryId.Equals(countryId)).Count() > 0)
                {
                    var propTypeTemp = (Country)countries.Where(i => i.CountryId.Equals(countryId)).First();
                    countries.Remove(propTypeTemp);
                }
                int matchcount = 0;
                foreach (var partnershipcategory in partnerCategories)
                {
                    matchcount += partnershipcategory.PartnerCategoriesDetails.Where(o => o.Countries.Any(p => p.CountryId.Equals(countryId))).Count();

                }
                var countryContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                var countryContactDropDown = GetViewState<IList<DropDownItem>>(ViewStateConstants.RegionalContactDropDownViewState) ?? new List<DropDownItem>();
                if (matchcount == 0)
                {
                    var removedcontact = countryContacts.Where(i => i.CountryId.Equals(countryId)).First();
                    countryContacts.Remove(removedcontact);
                    var removedcountrydropdown = countryContactDropDown.Where(p => p.DataValueField.Equals(countryId.ToString())).First();
                    countryContactDropDown.Remove(removedcountrydropdown);
                }

                if (countries.Count == 0)
                    partnerCategoryDetails.Remove(filteredPartnerCategoryDetails);

                categoryRepater.DataSource = partnerCategories;
                categoryRepater.DataBind();
                SetViewState<IList<DropDownItem>>(ViewStateConstants.RegionalContactDropDownViewState, countryContactDropDown);
                SetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState, countryContacts);
                SetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState, partnerCategories);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Remove the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveCategoryLinkButton_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                string[] args = ((LinkButton)sender).CommandArgument.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                int categoryId = int.Parse(args[0], CultureInfo.InvariantCulture);
                int brandId = int.Parse(args[1], CultureInfo.InvariantCulture);
                int regionId = int.Parse(args[2], CultureInfo.InvariantCulture);
                var partnerCategory = GetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState) ?? new List<PartnerCategories>();
                var selectedCategory = partnerCategory.Where(p => p.CategoryId == categoryId).FirstOrDefault();
                if (selectedCategory != null)
                {
                    if (selectedCategory.PartnerCategoriesDetails.Count > 1)
                    {
                        PartnerCategoryDetails partnerCategoryDetails = selectedCategory.PartnerCategoriesDetails.Where(p => p.BrandId == brandId && p.RegionId == regionId).FirstOrDefault();

                        partnerCategory.Where(p => p.CategoryId == categoryId).FirstOrDefault().PartnerCategoriesDetails.Remove(partnerCategoryDetails);
                    }
                    else
                    {
                        partnerCategory.Remove(selectedCategory);
                        //empty the contacts text box
                        SetTextBoxText(regionalFirstNameTextBox, null);
                        SetTextBoxText(regionalLastNameTextBox, null);
                        SetTextBoxText(regionalTitleTextBox, null);
                        SetTextBoxText(regionalPhoneTextBox, null);
                        SetTextBoxText(regionalFaxTextBox, null);
                        SetTextBoxText(regionalEmailTextBox, null);
                    }
                }
                //remove the region from region drop down
                var regionalContactDropDown = GetViewState<IList<DropDownItem>>(ViewStateConstants.RegionalContactDropDownViewState) ?? new List<DropDownItem>();
                var removedRegion = regionalContactDropDown.Where(p => p.DataValueField == regionId.ToString(CultureInfo.InvariantCulture)).FirstOrDefault();
                regionalContactDropDown.Remove(removedRegion);
                regionDropDown.DataSource = regionalContactDropDown;
                regionDropDown.DataBind();
                SetViewState<IList<DropDownItem>>(ViewStateConstants.RegionalContactDropDownViewState, regionalContactDropDown);
                regionDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1252, culture, "Select"), DropDownConstants.DefaultValue));
                regionDropDownUpdatePanel.Update();

                if (partnerCategory.Count == 0)
                {
                    ConfigureResultMessage(categoryMappingResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.MapCategoryErrorMessage));
                    categoriesMappingUpdatePanel.Update();
                }
                else
                {
                    categoryRepater.DataSource = partnerCategory;
                    categoryRepater.DataBind();
                    SetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState, partnerCategory);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }


        /// <summary>
        /// Remove to be mapped country
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveToBeMappedCountryLinkButton_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                IList<DropDownItem> countries = GetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState) ?? new List<DropDownItem>();
                DropDownItem country = countries.SingleOrDefault(p => p.DataValueField == ((ImageButton)(sender)).CommandArgument);
                countries.Remove(country);

                SetViewState<IList<DropDownItem>>(ViewStateConstants.CountriesViewState, countries);
                tobeMappedCountriesGridView.DataSource = countries;
                tobeMappedCountriesGridView.DataBind();

                var selCon = selectedCountries.Text.Split(',').ToList();
                selCon.Remove(country.DataValueField);
                selectedCountries.Text = string.Join(",", selCon);

                //When there is no country then show all country in the category country drop down
                if (countries.Count == 0)
                {
                    var regionList = selectedRegions.Text.Split(',').Select(item => int.Parse(item));
                    IList<DropDownItem> countriesForSelectedRegions = new List<DropDownItem>();
                    foreach (var item in regionList)
                    {
                        var countryForRegion = _partnerManager.GetCountriesForRegion(item);
                        countryForRegion.ToList().ForEach(p =>
                        {
                            countriesForSelectedRegions.Add(p);
                        });
                    }

                    categoryCountryDropDown.DataSource = countriesForSelectedRegions;
                    categoryCountryDropDown.DataTextField = "DataTextField";
                    categoryCountryDropDown.DataValueField = "DataValueField";
                    categoryCountryDropDown.DataBind();
                    categoryCountryDropDown.Enabled = true;
                }
                else
                {
                    categoryCountryDropDown.DataSource = countries;
                    categoryCountryDropDown.DataTextField = "DataTextField";
                    categoryCountryDropDown.DataValueField = "DataValueField";
                    categoryCountryDropDown.DataBind();
                    categoryCountryDropDown.Enabled = true;
                }
                chooseCategoryUpdatePanel.Update();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///  Save or update the partner details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                validateActivationDateCustomValidator.ServerValidate += new ServerValidateEventHandler(ValidateActivationDate);
                validateActivationDateCustomValidator.Validate();

                if (IsValid)
                {
                    DateTime dateTime;

                    if (Convert.ToInt32(partnershipStatusDropDown.SelectedItem.Value, CultureInfo.InvariantCulture) == (int)PartnershipStatusEnum.Active && (!DateTime.TryParse(activeDateTextbox.Text, out dateTime) || !DateTime.TryParse(expireDateTextbox.Text, out dateTime)))
                    {
                        ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.RequiredActivationAndExpirationDate, culture));
                    }
                    else
                    {
                        Partner partner = new Partner();

                        partner.UserId = GetLoggedinUserId();

                        if (partnerIdHiddenField.Value != "")
                        {
                            partner.PartnerId = partnerIdHiddenField.Value;
                        }
                        else
                        {
                            partner.PartnerId = GetViewState<string>("partnerId");
                        }
                        partner.CompanyName = companyNameTextBox.Text;
                        partner.WebSiteLink = webSiteLinkTextBox.Text;
                        partner.CompanyAddress1 = companyAddress1TextBox.Text;
                        partner.CompanyAddress2 = companyAddress2TextBox.Text;
                        partner.CountryId = Convert.ToInt32(countryDropDown.SelectedItem.Value, CultureInfo.InvariantCulture);
                        partner.City = cityTextBox.Text;
                        if (stateDropDown.SelectedItem != null)
                        {
                            partner.StateId = Convert.ToInt32(stateDropDown.SelectedItem.Value, CultureInfo.InvariantCulture);
                        }
                        partner.ZipCode = zipTextBox.Text.Trim();
                        partner.CompanyDescription = companyDesriptionTextBox.Text;
                        partner.IsSAM = Convert.ToBoolean(Convert.ToInt32(samDropDown.SelectedItem.Value, CultureInfo.InvariantCulture));
                        partner.IsRecPartner = Convert.ToBoolean(Convert.ToInt32(incRecPartnerDropDown.SelectedItem.Value, CultureInfo.InvariantCulture));
                        partner.Contacts = new List<Contact>();
                        partner.CompanyLogoImage = Session[SessionConstants.ProductImageBytes] != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? (byte[])Session[SessionConstants.ProductImageBytes] : null;
                        partner.CompanyLogoThumbnailImage = Session[SessionConstants.ProductImageBytes] != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? ((byte[])Session[SessionConstants.ProductImageBytes]).ConvertToThumbnail() : null;
                        partner.CompanyLogoName = imageNameHiddenField.Value;
                        partner.Contacts.Add(new Contact()
                        {
                            ContactTypeId = (int)ContactTypeEnum.Primary,
                            ContactId = GetViewState<string>("PrimaryContactId"),
                            FirstName = firstNameTextBox.Text,
                            LastName = lastNameTextBox.Text,
                            Title = titleTextBox.Text,
                            Phone = phoneNumberTextBox.Text,
                            Fax = faxTextBox.Text,
                            Email = emailTextBox.Text
                        });
                        partner.Contacts.Add(new Contact()
                        {
                            ContactTypeId = (int)ContactTypeEnum.Secondary,
                            ContactId = GetViewState<string>("SecondaryContactId"),
                            FirstName = secondaryContactFirstNameTextbox.Text,
                            LastName = secondaryContactLastNameTextbox.Text,
                            Title = secondaryContactTitleTextbox.Text,
                            Phone = secondaryContactPhoneTextbox.Text,
                            Fax = secondaryContactFaxTextbox.Text,
                            Email = secondaryContactEmailTextbox.Text
                        });
                        var regionalContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                        foreach (Contact contact in regionalContacts.Where(p => !(string.IsNullOrWhiteSpace(p.FirstName))))
                        {
                            partner.Contacts.Add(new Contact()
                            {
                                ContactTypeId = (int)ContactTypeEnum.Regional,
                                ContactId = contact.ContactId,
                                FirstName = contact.FirstName,
                                LastName = contact.LastName,
                                Title = contact.Title,
                                Phone = contact.Phone,
                                Fax = contact.Fax,
                                Email = contact.Email,
                                RegionId = contact.RegionId,
                                CountryId = contact.CountryId
                            });
                        }
                        partner.PartnershipStatusId = Convert.ToInt32(partnershipStatusDropDown.SelectedItem.Value, CultureInfo.InvariantCulture);
                        partner.PartnershipTypeId = Convert.ToInt32(partnershipTypeDropDown.SelectedItem.Value, CultureInfo.InvariantCulture);
                        partner.SubmitProducts = true;// commented as this if future CR -- submitProductsCheckBox.Checked;
                        partner.IsAutomatedRenewalEmailReq = automatedRenewalEmailCheckBox.Checked;

                        if (DateTime.TryParse(activeDateTextbox.Text, out dateTime))
                        {
                            partner.ActiveDate = Convert.ToDateTime(activeDateTextbox.Text.Trim(), CultureInfo.InvariantCulture);
                        }

                        if (DateTime.TryParse(expireDateTextbox.Text, out dateTime))
                        {
                            partner.ExpirationDate = Convert.ToDateTime(expireDateTextbox.Text.Trim(), CultureInfo.InvariantCulture);
                        }

                        int result = -1;
                        var partnerCategories = GetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState) ?? new List<PartnerCategories>();
                        if (GetViewState<string>(UIConstants.Mode) == UIConstants.Edit)
                        {
                            result = _partnerManager.UpdatePartner(partner, partnerCategories);
                            if (result == 0)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerUpdateSuccess, culture));
                                PopulatePartnerDetails(Convert.ToInt32(partner.PartnerId));
                                regionDropDown.SelectedValue = GetViewState<string>("previousSelectedCountry");
                                partnerIdHiddenField.Value = partner.PartnerId;
                            }
                            else if (result == -2)
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateCompanyNameErrorMessage, culture));
                            }
                            else
                            {
                                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerUpdateFailure, culture));
                            }
                        }
                        else if (GetViewState<string>(UIConstants.Mode) == UIConstants.Add)
                        {
                            if (String.IsNullOrEmpty(partnerIdHiddenField.Value))
                            {
                                result = _partnerManager.AddPartner(partner, partnerCategories);
                                if (result == -2)
                                {
                                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateCompanyNameErrorMessage, culture));
                                }
                                else if (result == -1)
                                {
                                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerAdditionFailure, culture));
                                }
                                else
                                {
                                    ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerSuccessfullyAdded, culture));
                                    partnerIdHiddenField.Value = result.ToString(CultureInfo.InvariantCulture);
                                    SetViewState<string>("partnerId", result.ToString(CultureInfo.InvariantCulture)); //INC000015714254
                                    UpdateDynamicDropDownList(DropDownConstants.PartnerDropDown);
                                    paymentDiv.Visible = true;
                                    linked.Visible = true;
                                    viewNavigationDiv.Visible = true;
                                    PopulatePartnerDetails(result);
                                    regionDropDown.SelectedValue = GetViewState<string>("previousSelectedCountry");
                                    SetViewState(UIConstants.Mode, UIConstants.Edit); //INC000015714254 
                                }
                            }
                            else
                            {
                                result = _partnerManager.UpdatePartner(partner, partnerCategories);
                                if (result == 0)
                                {
                                    ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerUpdateSuccess, culture));
                                    PopulatePartnerDetails(Convert.ToInt32(partner.PartnerId));
                                    regionDropDown.SelectedValue = GetViewState<string>("previousSelectedCountry");
                                    partnerIdHiddenField.Value = partner.PartnerId;
                                }
                                else if (result == -2)
                                {
                                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateCompanyNameErrorMessage, culture));
                                }
                                else
                                {
                                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.PartnerUpdateFailure, culture));
                                }
                            }
                        }
                    }
                    ClearSession(ViewStateConstants.MultiSearch);
                    resultUpdatePanel.Update();
                    regionDropDownUpdatePanel.Update();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
                regionDropDownUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Validate the activation date
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidateActivationDate(object source, ServerValidateEventArgs args)
        {
            try
            {
                ValidateDate(args, validateActivationDateCustomValidator, activeDateTextbox.Text, expireDateTextbox.Text, "ActivationExpirationDateValidation");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Validate the fields required for saving the partner details
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void SavePartnerValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (regionDropDown.SelectedItem.Value != DropDownConstants.DefaultValue)
                {
                    var regionalContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                    var contactToUpdate = regionalContacts.Where(p => p.CountryId.ToString(CultureInfo.InvariantCulture) == regionDropDown.SelectedItem.Value).FirstOrDefault();
                    contactToUpdate.FirstName = regionalFirstNameTextBox.Text;
                    contactToUpdate.LastName = regionalLastNameTextBox.Text;
                    contactToUpdate.Title = regionalTitleTextBox.Text;
                    contactToUpdate.Phone = regionalPhoneTextBox.Text;
                    contactToUpdate.Fax = regionalFaxTextBox.Text;
                    contactToUpdate.Email = regionalEmailTextBox.Text;
                }
                var partnerCategories = GetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState) ?? new List<PartnerCategories>();
                if (partnerCategories.Count == 0)
                {
                    args.IsValid = false;
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.MapCategoryErrorMessage, culture));
                }
                var regionalContact = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
                string nameValidationString = ControlValidatorUtility.GetLocalizedRegularExpression((int)RegularExpressionTypeEnum.NameValidator, culture);
                var nameValidator = new Regex(nameValidationString);

                foreach (var contact in regionalContact)
                {
                    if ((string.IsNullOrWhiteSpace(contact.FirstName) && string.IsNullOrWhiteSpace(contact.LastName)
                        && string.IsNullOrWhiteSpace(contact.Email) && string.IsNullOrWhiteSpace(contact.Phone)
                        && string.IsNullOrWhiteSpace(contact.Fax)) && string.IsNullOrWhiteSpace(contact.Title))
                    {
                        continue;
                    }
                    else if ((string.IsNullOrWhiteSpace(contact.FirstName) || string.IsNullOrWhiteSpace(contact.LastName)
                        || string.IsNullOrWhiteSpace(contact.Email) || string.IsNullOrWhiteSpace(contact.Phone)
                        || string.IsNullOrWhiteSpace(contact.Fax)))
                    {
                        args.IsValid = false;
                        var regionalContactDropDown = GetViewState<IList<DropDownItem>>(ViewStateConstants.RegionalContactDropDownViewState) ?? new List<DropDownItem>();
                        if (string.IsNullOrWhiteSpace(validationErrorMessageDiv.InnerHtml))
                        {
                            ConfigureResultMessage(validationErrorMessageDiv, "message error", string.Format(CultureInfo.InvariantCulture, ConfigurationStore.GetApplicationMessages(MessageConstants.RegionalContactErrorMessage, culture),
                                regionalContactDropDown.Where(p => p.DataValueField == contact.CountryId.ToString(CultureInfo.InvariantCulture)).FirstOrDefault().DataTextField
                               ));
                        }
                        else
                        {
                            string validationErrorMessage = validationErrorMessageDiv.InnerHtml;
                            validationErrorMessage += "<br />" + string.Format(CultureInfo.InvariantCulture, ConfigurationStore.GetApplicationMessages(MessageConstants.RegionalContactErrorMessage, culture),
                                regionalContactDropDown.Where(p => p.DataValueField == contact.CountryId.ToString(CultureInfo.InvariantCulture)).FirstOrDefault().DataTextField
                                );
                            ConfigureResultMessage(validationErrorMessageDiv, "message error", validationErrorMessage);
                        }
                    }
                    else if (!nameValidator.IsMatch(contact.FirstName) || !nameValidator.IsMatch(contact.LastName))
                    {
                        args.IsValid = false;
                        var regionalContactDropDown = GetViewState<IList<DropDownItem>>(ViewStateConstants.RegionalContactDropDownViewState) ?? new List<DropDownItem>();

                        ConfigureResultMessage(validationErrorMessageDiv, "message error", string.Format(CultureInfo.InvariantCulture, "Please enter valid first and last name for {0}",
                                 regionalContactDropDown.Where(p => p.DataValueField == contact.CountryId.ToString(CultureInfo.InvariantCulture)).FirstOrDefault().DataTextField
                                ));
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Bind the category drop down
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        private void BindDropDown(IList<Category> categories, DropDownList sourceDropDown, string defaultText)
        {
            sourceDropDown.DataSource = categories.OrderBy(p => p.CategoryName);
            sourceDropDown.DataTextField = "CategoryName";
            sourceDropDown.DataValueField = "CategoryId";
            sourceDropDown.DataBind();
            //sourceDropDown.Items.Insert(0, new ListItem(defaultText, DropDownConstants.DefaultValue));
        }

        /// <summary>
        /// Find the occurrence of control
        /// </summary>
        /// <param name="substr"></param>
        /// <returns></returns>
        private int FindOccurence(string substr)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            return ctrls.Where(p => p.Contains(substr + "-") && !p.Contains("EVENTTARGET") && !p.Contains("UpdatePanel")).ToList().Count();
        }

        /// <summary>
        /// Find populate drop down
        /// </summary>
        /// <param name="selectedCategoryId"></param>
        /// <param name="id"></param>
        private void FindPopulateDropDowns(string selectedCategoryId, string id)
        {
            DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, id);
            PopulateDropDown(newDropDown, selectedCategoryId);
        }

        /// <summary>
        ///  Get the child categories
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState) ?? new List<Category>();
            IList<Category> categoryList = (categories.AsEnumerable().
                                            Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture))).ToList());
            return categoryList;
        }

        /// <summary>
        /// Get the root level category
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetRootLevelCategories()
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState) ?? new List<Category>();
            IList<Category> rootCategoryList = (categories.AsEnumerable().Where(p => p.ParentCategoryId == -1)).ToList();
            return rootCategoryList;
        }

        /// <summary>
        /// Populate the category details
        /// </summary>
        /// <param name="partnerId"></param>
        private void PopulateCategoryDetails(int partnerId)
        {
            var partnerCategories = _partnerManager.GetPartnerCategoryDetails(partnerId);
            categoryRepater.DataSource = partnerCategories;
            categoryRepater.DataBind();
            SetViewState<IList<PartnerCategories>>(ViewStateConstants.PartnerCategoriesViewState, partnerCategories);
        }

        /// <summary>
        /// Populate the company info
        /// </summary>
        /// <param name="partner"></param>
        private void PopulateCompanyInfo(Partner partner)
        {
            SetTextBoxText(companyNameTextBox, partner.CompanyName);
            SetTextBoxText(webSiteLinkTextBox, partner.WebSiteLink);
            SetTextBoxText(companyAddress1TextBox, partner.CompanyAddress1);
            SetTextBoxText(companyAddress2TextBox, partner.CompanyAddress2);
            SetTextBoxText(cityTextBox, partner.City);
            SetTextBoxText(zipTextBox, partner.ZipCode);
            SetTextBoxText(companyDesriptionTextBox, partner.CompanyDescription);
            if (countryDropDown.Items.FindByValue(partner.CountryId.ToString(CultureInfo.InvariantCulture)) != null)
            {
                countryDropDown.SelectedIndex = countryDropDown.Items.IndexOf(countryDropDown.Items.FindByValue(partner.CountryId.ToString(CultureInfo.InvariantCulture)));
                if (partner.CountryId == Convert.ToInt16(DropDownConstants.UnitedStates))
                {
                    stateRequiredFieldValidator.Enabled = true;
                    zipRequiredFieldValidator.Enabled = true;
                    //zipCompareValidator.Enabled = true;
                    zipCodeRegularExpressionValidator.Enabled = true;
                }
                else
                {
                    stateRequiredFieldValidator.Enabled = false;
                    zipRequiredFieldValidator.Enabled = false;
                    //zipCompareValidator.Enabled = false;
                    zipCodeRegularExpressionValidator.Enabled = false;
                }
            }
            if (stateDropDown.Items.FindByValue(partner.StateId.ToString(CultureInfo.InvariantCulture)) != null)
            {
                stateDropDown.SelectedIndex = stateDropDown.Items.IndexOf(stateDropDown.Items.FindByValue(partner.StateId.ToString(CultureInfo.InvariantCulture)));
            }
            samDropDown.SelectedIndex = partner.IsSAM ? 1 : 0;
            incRecPartnerDropDown.SelectedIndex = partner.IsRecPartner ? 1 : 0;
        }

        /// <summary>
        /// Populate the contact info
        /// </summary>
        /// <param name="contacts"></param>
        private void PopulateContactInfo(IList<Contact> contacts)
        {
            IList<Contact> regionalContacts = new List<Contact>();
            foreach (var contact in contacts)
            {
                if (contact.ContactTypeId == (int)ContactTypeEnum.Primary)
                {
                    SetViewState<string>("PrimaryContactId", contact.ContactId);
                    SetTextBoxText(firstNameTextBox, contact.FirstName);
                    SetTextBoxText(lastNameTextBox, contact.LastName);
                    SetTextBoxText(titleTextBox, contact.Title);
                    SetTextBoxText(phoneNumberTextBox, contact.Phone);
                    SetTextBoxText(faxTextBox, contact.Fax);
                    SetTextBoxText(emailTextBox, contact.Email);
                }
                else if (contact.ContactTypeId == (int)ContactTypeEnum.Secondary)
                {
                    SetViewState<string>("SecondaryContactId", contact.ContactId);
                    SetTextBoxText(secondaryContactFirstNameTextbox, contact.FirstName);
                    SetTextBoxText(secondaryContactLastNameTextbox, contact.LastName);
                    SetTextBoxText(secondaryContactTitleTextbox, contact.Title);
                    SetTextBoxText(secondaryContactPhoneTextbox, contact.Phone);
                    SetTextBoxText(secondaryContactFaxTextbox, contact.Fax);
                    SetTextBoxText(secondaryContactEmailTextbox, contact.Email);
                }
                else if (contact.ContactTypeId == (int)ContactTypeEnum.Regional)
                {
                    regionalContacts.Add(contact);
                }
            }
            SetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState, regionalContacts);
        }

        /// <summary>
        /// Populate the drop down
        /// </summary>
        /// <param name="parentCategoryDropDown"></param>
        /// <param name="selectedValue"></param>
        private void PopulateDropDown(DropDownList parentCategoryDropDown, string selectedValue)
        {
            parentCategoryDropDown.Items.Clear();
            IList<Category> categoryList = new List<Category>();
            if (string.IsNullOrWhiteSpace(selectedValue))
            {
                categoryList = GetRootLevelCategories();
            }
            else if (selectedValue != "0")
            {
                categoryList = GetChildCategories(selectedValue);
            }
            if (categoryList != null)
            {
                BindData(parentCategoryDropDown, categoryList);
            }
            parentCategoryDropDown.Items.Insert(0, new ListItem("Select a Subcategory", "0"));
        }

        /// <summary>
        /// Populate the static drop downs
        /// </summary>
        private void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.CountryDropDown, countryDropDown, ResourceUtility.GetLocalizedString(1241, culture, "Select a Country"), null);
            BindStaticDropDown(DropDownConstants.PartnerStatusDropDown, partnershipStatusDropDown, null, null, false);
            BindStaticDropDown(DropDownConstants.StateDropDown, stateDropDown, null, null);
            BindStaticDropDown(DropDownConstants.PartnershipTypeDropDown, partnershipTypeDropDown, null, null);
            PopulateCategoryRegionDropDown();
            brandCollection.Value = string.Join(",", GetStaticDropDownDataSource(DropDownConstants.BrandDropDown).ToList().Select(p => p.DataValueField));

            IList<Category> categories = HelperManager.Instance.GetActiveCategories;
            SetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState, categories);

            BindDropDown(categories.Where(item => item.Level.Equals(1)).ToList(), categoryDropDown, ResourceUtility.GetLocalizedString(1242, culture, "Select a Category"));
        }

        /// <summary>
        /// Populate the region drop down
        /// </summary>
        private void PopulateCategoryRegionDropDown()
        {
            categoryRegionDropDown.DataSource = GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
            categoryRegionDropDown.DataTextField = "DataTextField";
            categoryRegionDropDown.DataValueField = "DataValueField";
            categoryRegionDropDown.DataBind();
        }


        /// <summary>
        /// Populate the partner details for editing
        /// </summary>
        /// <param name="partnerId"></param>
        private void PopulatePartnerDetails(int partnerId)
        {
            Partner partner = _partnerManager.GetPartnerDetails(partnerId);

            PopulateCompanyInfo(partner);

            if (stateDropDown.Items[0].Value == string.Empty || stateDropDown.Items[0].Value == DropDownConstants.DefaultValue)
            {
                stateDropDown.Items.RemoveAt(0);
            }

            //state drop down will be enabled only for USA and 1 is code for USA
            if (countryDropDown.SelectedValue != DropDownConstants.UnitedStates)
            {
                stateDropDown.Items.Insert(0, new ListItem(string.Empty, DropDownConstants.DefaultValue));
                stateDropDown.Enabled = false;
                zipTextBox.ReadOnly = false;
                zipTextBox.Enabled = true;
                //zipTextBoxDiv.Attributes.Add("class", "input textbox3 small readonly");
                zipTextBoxDiv.Attributes.Add("class", "input textbox3 small mandatory");
            }
            else
            {
                stateDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1246, culture, "Select a State"), DropDownConstants.DefaultValue));
                stateDropDown.Enabled = true;
                zipTextBox.ReadOnly = false;
                zipTextBox.Enabled = true;
                zipTextBoxDiv.Attributes.Add("class", "input textbox3 small mandatory");
            }

            PopulateContactInfo(partner.Contacts);

            automatedRenewalEmailCheckBox.Checked = partner.IsAutomatedRenewalEmailReq;
            submitProductsCheckBox.Checked = partner.SubmitProducts;

            //Select the partnership status
            if (partnershipStatusDropDown.Items.FindByValue(partner.PartnershipStatusId.ToString(CultureInfo.InvariantCulture)) != null)
            {
                partnershipStatusDropDown.SelectedIndex = partnershipStatusDropDown.Items.IndexOf(partnershipStatusDropDown.Items.FindByValue(partner.PartnershipStatusId.ToString(CultureInfo.InvariantCulture)));
            }

            if (partnershipTypeDropDown.Items.FindByValue(partner.PartnershipTypeId.ToString(CultureInfo.InvariantCulture)) != null)
            {
                partnershipTypeDropDown.SelectedIndex = partnershipTypeDropDown.Items.IndexOf(partnershipTypeDropDown.Items.FindByValue(partner.PartnershipTypeId.ToString(CultureInfo.InvariantCulture)));
            }

            companyLogo.ImageUrl = partner.CompanyLogoThumbnailImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(partner.CompanyLogoThumbnailImage.ConvertToThumbnail()) : "../Images/NoProduct.PNG";
            SetSession<byte[]>(SessionConstants.ProductImageBytes, partner.CompanyLogoThumbnailImage);
            imageLinkButton.Text = partner.CompanyLogoName;
            imageNameHiddenField.Value = partner.CompanyLogoName;
            if (!string.IsNullOrEmpty(partner.CompanyLogoName))
                removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "inline");

            SetLabelText(dateTimeLabel, partner.PartnershipReqSendDate.Value.ToString("dd-MMM-yyyy, h:mm tt"));

            if (partner.ActiveDate != null)
            {
                SetTextBoxText(activeDateTextbox, partner.ActiveDate.Value.ToString("dd-MMM-yyyy"));
            }
            if (partner.ExpirationDate != null)
            {
                SetTextBoxText(expireDateTextbox, partner.ExpirationDate.Value.ToString("dd-MMM-yyyy"));
            }

            if (partner.PaymentRequestDetails.RequestSentOn == DateTime.MinValue)
            {
                sentPaymentRequestTable.Visible = false;
            }
            else
            {
                sentPaymentRequestTable.Visible = true;
                SetLabelText(sentDateLabel, partner.PaymentRequestDetails.RequestSentOn.ToString("dd-MMM-yyyy, h:mm tt", CultureInfo.InvariantCulture));
                SetLabelText(sentToEmailLabel, partner.PaymentRequestDetails.RequestSentTo);
            }
            SetTextBoxText(regAmountTextBox, partner.PaymentRequestDetails.RegularAmount);
            SetTextBoxText(goldAmountTextBox, partner.PaymentRequestDetails.GoldAmount);

            //Bind the associated web link account
            linkWebAccountGrid.DataSource = partner.AssociatedWebAccount;
            linkWebAccountGrid.DataBind();

            PopulateCategoryDetails(partnerId);
        }

        /// <summary>
        /// Populate the regional contact id drop down
        /// </summary>
        /// <param name="item"></param>
        public void PopulateRegionalContactIdDropDown(RepeaterItem item)
        {
            string regionDesc = ((PartnerCategoryDetails)item.DataItem).RegionDescription.ToString(CultureInfo.InvariantCulture);
            string regionId = ((PartnerCategoryDetails)item.DataItem).RegionId.ToString(CultureInfo.InvariantCulture);
            int CountryCount = ((PartnerCategoryDetails)item.DataItem).Countries.Count;

            var countryContactDropDown = GetViewState<IList<DropDownItem>>(ViewStateConstants.RegionalContactDropDownViewState) ?? new List<DropDownItem>();
            List<string> lstcountries = ((PartnerCategoryDetails)item.DataItem).Countries.Select(s => s.CountryId.ToString()).ToList();
            // To be corrected
            //countryContactDropDown.Clear();


            foreach (var country in ((PartnerCategoryDetails)item.DataItem).Countries)
            {
                string CountryId = country.CountryId.ToString();
                if (countryContactDropDown.Count == 0)
                {
                    countryContactDropDown.Add(new DropDownItem()
                    {
                        DataTextField = country.CountryName,
                        DataValueField = country.CountryId.ToString(),
                        ParentDataField = ((PartnerCategoryDetails)item.DataItem).RegionId.ToString()
                    });
                }
                else if (countryContactDropDown.Where(p => p.DataValueField == CountryId).Count() == 0)
                {
                    countryContactDropDown.Add(new DropDownItem()
                    {
                        DataTextField = country.CountryName,
                        DataValueField = country.CountryId.ToString(),
                        ParentDataField = ((PartnerCategoryDetails)item.DataItem).RegionId.ToString()
                    });
                }
            }

            regionDropDown.DataSource = countryContactDropDown;
            regionDropDown.DataTextField = "DataTextField";
            regionDropDown.DataValueField = "DataValueField";
            regionDropDown.DataBind();
            SetViewState<IList<DropDownItem>>(ViewStateConstants.RegionalContactDropDownViewState, countryContactDropDown);
            regionDropDown.Items.Insert(0, new ListItem(ResourceUtility.GetLocalizedString(1252, culture, "Select"), DropDownConstants.DefaultValue));

            var countryContacts = GetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState) ?? new List<Contact>();
            // countryContacts.Clear();
            foreach (var country in ((PartnerCategoryDetails)item.DataItem).Countries)
            {
                if (countryContacts.Count == 0)
                {
                    countryContacts.Add(new Contact()
                    {
                        CountryId = country.CountryId,
                        CountryName = country.CountryName,
                        RegionId = ((PartnerCategoryDetails)item.DataItem).RegionId
                    });
                }
                else if (countryContacts.Where(p => p.CountryId == country.CountryId).Count() == 0)
                {
                    countryContacts.Add(new Contact()
                    {
                        CountryId = country.CountryId,
                        CountryName = country.CountryName,
                        RegionId = ((PartnerCategoryDetails)item.DataItem).RegionId
                    });
                }

            }

            SetViewState<IList<Contact>>(ViewStateConstants.RegionalContactsViewState, countryContacts);
        }

        /// <summary>
        /// Read category
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(string currentSelectedCategoryId)
        {
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState) ?? new List<Category>();
            Category category = (Category)(categories.Where(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)).Single());
            return category;
        }

        /// <summary>
        /// Read category
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private IEnumerable<Category> ReadCategories(IEnumerable<int> currentSelectedCategoryIds)
        {
            //var items = currentSelectedCategoryIds.Select(item => int.Parse(item));
            IList<Category> categories = GetViewState<IList<Category>>(ViewStateConstants.CategoriesViewState) ?? new List<Category>();
            var category = categories.Where(p => currentSelectedCategoryIds.Contains(p.CategoryId));
            return category;
        }

        /// <summary>
        /// recreate the controls
        /// </summary>
        /// <param name="ctrlPrefix"></param>
        /// <param name="ctrlType"></param>
        private void RecreateControls(string ctrlPrefix, string ctrlType)
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            ctrlPrefix = "selectedCategories";
            int cnt = FindOccurence(ctrlPrefix);
            if (cnt > 0)
            {
                for (int k = 1; k <= cnt; k++)
                {
                    for (int i = 0; i < ctrls.Length; i++)
                    {
                        if (ctrls[i].Contains(ctrlPrefix + "-" + (k + 1).ToString(CultureInfo.InvariantCulture)) && !ctrls[i].Contains("EVENTTARGET") && !ctrls[i].Contains("UpdatePanel"))
                        {
                            string ctrlID = string.Empty;
                            string[] subCtrls = ctrls[i].Split('-');

                            for (int l = 0; l < subCtrls.Length; l++)
                            {
                                if (subCtrls[l].Contains("selectedCategories"))
                                {
                                    ctrlID = subCtrls[l + 1].Substring(0, 1);
                                }
                            }
                            if (ctrlType == "DropDownList")
                            {
                                CreateDropDownList(Convert.ToInt32(ctrlID, CultureInfo.InvariantCulture));
                            }
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set the page setting for Add/Edit view
        /// </summary>
        private void SetPageSettings(string mode)
        {
            switch (mode)
            {
                case UIConstants.Edit:
                    SetViewState(UIConstants.Mode, UIConstants.Edit);
                    titleLiteral.Text = ResourceUtility.GetLocalizedString(187, culture, "Edit Partner");

                    User user = GetSession<User>(SessionConstants.User);
                    var isProductPermissionAccess = user.Permissions.Any(x => x.PermissionId == (int)AdminPermissionEnum.Products);
                    if (isProductPermissionAccess)
                        viewPartnerProductLinkButton.Visible = true;
                    else
                        viewPartnerProductLinkButton.Visible = false;

                    var isTransactionPermissionAccess = user.Permissions.Any(x => x.PermissionId == (int)AdminPermissionEnum.Transactions);
                    if (isTransactionPermissionAccess)
                        viewPartnerTransHistoryLinkButton.Visible = true;
                    else
                        viewPartnerTransHistoryLinkButton.Visible = false;
                    break;

                case UIConstants.Add:
                    SetViewState(UIConstants.Mode, UIConstants.Add);
                    titleLiteral.Text = ResourceUtility.GetLocalizedString(186, culture, "Add Partner");
                    partnershipStatusDropDown.SelectedValue = ((int)PartnershipStatusEnum.Inactive).ToString(CultureInfo.InvariantCulture);
                    partnershipTypeDropDown.SelectedValue = ((int)PartnershipTypeEnum.Partner).ToString(CultureInfo.InvariantCulture);
                    zipTextBoxDiv.Attributes.Add("class", "input textbox3 small");
                    companyLogo.ImageUrl = "../Images/NoProduct.PNG";
                    break;
            }
        }

        /// <summary>
        /// Set the state for regional contact controls state as mandatory
        /// </summary>
        private void SetRegionalContactAsMandatory()
        {
            regionalFirstNameTextBox.Enabled = true;
            regionalLastNameTextBox.Enabled = true;
            regionalTitleTextBox.Enabled = true;
            regionalPhoneTextBox.Enabled = true;
            regionalFaxTextBox.Enabled = true;
            regionalEmailTextBox.Enabled = true;

            firstNameDiv.Attributes.Remove("class");
            firstNameDiv.Attributes.Add("class", "input textbox3 mandatory");

            lastNameDiv.Attributes.Remove("class");
            lastNameDiv.Attributes.Add("class", "input textbox3 mandatory");

            titleDiv.Attributes.Remove("class");
            titleDiv.Attributes.Add("class", "input textbox3");

            phoneDiv.Attributes.Remove("class");
            phoneDiv.Attributes.Add("class", "input textbox3 mandatory");

            faxDiv.Attributes.Remove("class");
            faxDiv.Attributes.Add("class", "input textbox3 mandatory");

            emailDiv.Attributes.Remove("class");
            emailDiv.Attributes.Add("class", "input textbox3 mandatory");
        }

        /// <summary>
        /// Set the state for regional contact controls state as read only
        /// </summary>
        private void SetRegionalContactAsReadOnly()
        {
            SetTextBoxText(regionalFirstNameTextBox, null);
            SetTextBoxText(regionalLastNameTextBox, null);
            SetTextBoxText(regionalTitleTextBox, null);
            SetTextBoxText(regionalPhoneTextBox, null);
            SetTextBoxText(regionalFaxTextBox, null);
            SetTextBoxText(regionalEmailTextBox, null);

            regionalFirstNameTextBox.Enabled = false;
            firstNameDiv.Attributes.Add("class", "input textbox3 readonly");

            regionalLastNameTextBox.Enabled = false;
            lastNameDiv.Attributes.Add("class", "input textbox3 readonly");

            regionalTitleTextBox.Enabled = false;
            titleDiv.Attributes.Add("class", "input textbox3 readonly");

            regionalPhoneTextBox.Enabled = false;
            phoneDiv.Attributes.Add("class", "input textbox3 readonly");

            regionalFaxTextBox.Enabled = false;
            faxDiv.Attributes.Add("class", "input textbox3 readonly");

            regionalEmailTextBox.Enabled = false;
            emailDiv.Attributes.Add("class", "input textbox3 readonly");
        }

        /// <summary>
        /// Navigate to the product screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewPartnerProductLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                SetSession<string>(SessionConstants.PartnerId, partnerIdHiddenField.Value);

                Response.Redirect("Products.aspx", false);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Navigate to the transaction history page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewPartnerTransHistoryLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                SetSession<string>(SessionConstants.PartnerId, partnerIdHiddenField.Value);
                Response.Redirect("Transactions.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}