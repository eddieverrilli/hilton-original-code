﻿using System;
using System.Data;

namespace Hilton.SuppliersConnection.UI
{
    public partial class UpdateStandard : System.Web.UI.Page
    {
        public void populateUpdateStandards()
        {
            DateUpdatedLabel.Text = "06/16/2011";
            BrandLabel.Text = "Hilton";
            RegionLabel.Text = "NorthAmerica";
            StandardLabel.Text = "A.1.A.1";
            CategoryLabel.Text = "Vanity Faucet";
            NewPublishDateLabel.Text = "01/01/2012";
            PreviousStandardLabel.Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel sem eget lorem consectetur congue non sodales risus. Mauris neque velit, blandit a porta a, tempus eget mi. Etiam tincidunt est eu lectus convallis in porttitor massa consequat. Aliq  erat volutpat. Morbi iaculis porta ante, vitae varius turpis bibendum sit amet. Donec sed lectus massa, in imperdiet nisi. Curabitur cursus, sem laoreet posuere pellentesque, urna metus lobortis ligula, vel molestie metus arcu quis massa. Aliquam nec mollis orci.";
            NewStandardLabel.Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel sem eget lorem consectetur congue non sodales risus. Mauris neque velit, blandit a porta a, tempus eget mi. Etiam tincidunt est eu lectus convallis in porttitor massa consequat. Aliq  erat volutpat. Morbi iaculis porta ante, vitae varius turpis bibendum sit amet. Donec sed lectus massa, in imperdiet nisi. Curabitur cursus, sem laoreet posuere pellentesque, urna metus lobortis ligula, vel molestie metus arcu quis massa. Aliquam nec mollis orci.";
            StatusDropDownList.Items.Add("New");
            PartnersNotifiedLabel.Text = " 07/15/2011, 16:00:03";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            populateUpdateStandards();

            this.Title = "Update Standards";

            //HtmlGenericControl li = new HtmlGenericControl("li");
            //li = (HtmlGenericControl)(this.Master.FindControl("liStandards"));
            //li.Attributes.Add("class", "active");

            DataSet affectedPartners = new DataSet();
            affectedPartners.ReadXml(MapPath("~\\App_Data\\UpdateStandards.xml"));
            AffectedPartnersGridView.DataSource = affectedPartners.Tables[0];
            AffectedPartnersGridView.DataBind();
            AffectedPartnersGridView.ShowHeader = false;
        }
    }
}