﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ViewStateMode="Inherit" AutoEventWireup="True" CodeBehind="Messages.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Messages" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc1" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });
        });

        function InitializeToggle() {
            var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleStateValue == "0") {
                document.getElementById('toggleDiv').style.display = "block";
            }
            else {
                $('.toggle-divider a')[0].removeAttribute('class');
                document.getElementById('toggleDiv').style.display = "none";
            }

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            })

            $('.col-6 tr, .col-7 tr').hover(function () {

                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.arrow').click(function () {

                $(this).toggleClass('active')
                $(this).parent().parent().next().children('.description').children().toggle();
            })

            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });

        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="messagesUpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="searchButton" runat="server" Text="Search" CBID="1041" OnClick="QuickSearch_Click"
                                        ValidationGroup="SearchMessages" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchMessages"
                                    ControlToValidate="searchTextBox" Display="Dynamic" ErrorMessage="Please specify search criteria"
                                    VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                            <div class="fieldset">
                                <div class="field">
                                    <asp:Label ID="fromDateLabel" runat="server" class="label" Text="Date:" CBID="352"></asp:Label>
                                    <div class="textbox-1">
                                        <asp:TextBox ID="fromDateTextBox" runat="server" class="datepicker"></asp:TextBox>
                                    </div>
                                    <asp:Label ID="toDateLabel" runat="server" class="label" Text="To:" CBID="722"></asp:Label>
                                    <div class="textbox-1">
                                        <asp:TextBox ID="toDateTextBox" runat="server" class="datepicker"></asp:TextBox>
                                    </div>
                                    <div class="error-message pad-top10">
                                        <asp:CustomValidator ID="validateDateCustomValidator" CssClass="errorText" OnServerValidate="ValidDate"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="field">
                                    <asp:Label ID="senderLabel" runat="server" CBID="911" class="label" Text="Sender:"></asp:Label>
                                    <div class="textbox-2">
                                        <asp:DropDownList ID="senderDropDown" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="field">
                                    <asp:Label ID="recipientLabel" runat="server" CBID="912" class="label" Text="Recipient:"></asp:Label>
                                    <div class="textbox-2">
                                        <asp:DropDownList ID="recepientDropDown" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="field">
                                    <asp:Label ID="senderTypeLabel" runat="server" CBID="913" class="label" Text="Sender Type:"></asp:Label>
                                    <div class="textbox-2">
                                        <asp:DropDownList ID="senderTypeDropDown" ViewStateMode="Enabled" runat="server">
                                            <asp:ListItem Value="0">All SenderType</asp:ListItem>
                                            <asp:ListItem Value="P">Partner</asp:ListItem>
                                            <asp:ListItem Value="O">Owner</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="field">
                                    <asp:Label ID="regionLabel" runat="server" CBID="914" class="label" Text="Region:"></asp:Label>
                                    <div class="textbox-2">
                                        <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="alR searchpad-top">
                                        <span class="btn-style">
                                            <asp:LinkButton ID="goButton" Text="Go" CBID="915" runat="server" OnClick="GoButton_Click" />
                                        </span>
                                    </div>
                                </div>
                                <div class="clear">
                                    &nbsp;</div>
                            </div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <asp:LinkButton ID="saveLinkButton" runat="server" CBID="916" Visible="false" class="btn"
                        OnClick="SaveLinkButton_Click" Text="Save"></asp:LinkButton>
                    <asp:LinkButton ID="exportCSVLinkButton" CBID="394" runat="server" class="btn" OnClick="ExportCSVLinkButton_Click"
                        Text="Export Excel"></asp:LinkButton>
                    <h2>
                        <asp:Label ID="messageHeader" runat="server" CBID="917" Text="Message"></asp:Label></h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <asp:Repeater ID="messageListRepeater" runat="server" ViewStateMode="Enabled" OnItemDataBound="MessageListRepeater_ItemDataBound"
                                OnItemCommand="MessageListRepeater_ItemCommand" OnItemCreated="MessageListRepeater_ItemCreated">
                                <HeaderTemplate>
                                    <table cellspacing="0" rules="all" border="1" style="border-collapse: collapse;"
                                        class="col-7">
                                        <tr>
                                            <th class="col-one">
                                            </th>
                                            <th class="col-two">
                                                <asp:LinkButton ID="dateTimeLinkButton" CBID="921" runat="server" Text="Date&nbsp;Sent"
                                                    CommandName="MesssageDate"></asp:LinkButton>
                                            </th>
                                            <th class="col-three">
                                                <asp:LinkButton ID="senderTypeLinkButton" runat="server" CBID="913" Text="Sender&nbsp;Type"
                                                    CommandName="SenderType"></asp:LinkButton>
                                            </th>
                                            <th class="col-four">
                                                <asp:LinkButton ID="senderLinkButton" runat="server" CBID="911" Text="Sender" CommandName="Sender"></asp:LinkButton>
                                            </th>
                                            <th class="col-five">
                                                <asp:LinkButton ID="recipientLinkButton" runat="server" CBID="912" Text="Recipient"
                                                    CommandName="From"></asp:LinkButton>
                                            </th>
                                            <th class="col-six">
                                                <asp:LinkButton ID="categoryLinkButton" runat="server" CBID="919" Text="Category"
                                                    CommandName="CatName"></asp:LinkButton>
                                            </th>
                                            <th class="col-seven">
                                                <asp:LinkButton ID="statusLinkButton" runat="server" CBID="920" Text="Status" CommandName="MessageStatusDesc"></asp:LinkButton>
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="col-one">
                                            <span class="arrow"></span>
                                        </td>
                                        <td class="col-two">
                                            <%# DataBinder.Eval(Container.DataItem, "MessageDateTime","{0:dd-MMM-yyyy}")%>
                                        </td>
                                        <td class="col-three">
                                            <%# DataBinder.Eval(Container.DataItem, "SenderType") %>
                                        </td>
                                        <td class="col-four">
                                            <%# DataBinder.Eval(Container.DataItem, "SenderName")%>
                                        </td>
                                        <td class="col-five">
                                            <%# DataBinder.Eval(Container.DataItem, "RecipientName")%>
                                        </td>
                                        <td class="col-six">
                                            <%# DataBinder.Eval(Container.DataItem, "CategoryName")%>
                                        </td>
                                        <td class="col-seven">
                                            <asp:DropDownList class="list-1" ID="messageStatusDropDown" runat="server">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="messageIdHiddenField" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "MessageId")%>' />
                                            <asp:HiddenField ID="messageStatusIdHiddenField" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "MessageStatusId")%>' />
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="updateLinkButton" runat="server" CausesValidation="False" CommandName="UpdateMessageStatus"
                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MessageId")%>' CBID="916"
                                                Text="update"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="description">
                                            <div>
                                                <%# DataBinder.Eval(Container.DataItem, "Details")%>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <uc1:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>