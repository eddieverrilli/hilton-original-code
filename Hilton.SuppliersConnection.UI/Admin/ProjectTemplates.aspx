﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ViewStateMode="Inherit" AutoEventWireup="True" CodeBehind="ProjectTemplates.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.ProjectTemplates" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc1" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/SetBrandsDropDown.js"></script>
    <script type="text/javascript">
        function InitializeToggle() {
            var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleStateValue == "0") {
                document.getElementById('toggleDiv').style.display = "block";
            }
            else {
                $('.toggle-divider a')[0].removeAttribute('class');
                document.getElementById('toggleDiv').style.display = "none";
            }

            $('.col-6 tr, .col-7 tr').hover(function () {
                7
                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            });
            $('#optgroup').multiselect('refresh');

        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }



        $(document).ready(function () {
            getItems();
        });

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="projectTemplateUpdatePanel" runat="server" UpdateMode="Always">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="selectedBrands" EventName="TextChanged" />
        </Triggers>
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="quickSearchButton" runat="server" OnClick="QuickSearch_Click"
                                        ValidationGroup="SearchProjectTemplates" Text="Search" CBID="647" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchProjectTemplates"
                                    ControlToValidate="searchTextBox" Display="Dynamic" ErrorMessage="Please specify search criteria"
                                    VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server" />
                            </div>
                        </div>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                            <div class="field">
                                <asp:Label ID="brandLabel" runat="server" class="label" Text="Brand:" CBID="858"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList runat="server" ID="optgroup" ViewStateMode="Enabled" ClientIDMode="Static"
                                        multiple="multiple">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="brandCollection" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="isCheckAll" runat="server" ClientIDMode="Static" />
                                    <asp:TextBox runat="server" ID="selectedBrands" ClientIDMode="Static" Style="display: none;"
                                        ViewStateMode="Enabled" AutoPostBack="false" OnTextChanged="SelectedBrands_TextChanged"></asp:TextBox>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="regionLabel" runat="server" class="label" Text="Region:" CBID="857"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="regionDropDown" AutoPostBack="true" ViewStateMode="Enabled"
                                        OnSelectedIndexChanged="RegionDropDown_SelectedIndexChanged" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="countrylabel" runat="server" CssClass="label" CBID="1300" Text="Country :"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="countryDropDown" ViewStateMode="Enabled" AutoPostBack="true"
                                        runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="visibility: hidden">
                                <asp:Label Visible="false" ID="propertyType" runat="server" class="label" Text="Property Type:"
                                    CBID="859"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="propertyTypeDropDown" runat="server" ViewStateMode="Enabled"
                                        Visible="false" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <asp:Label ID="statusLabel" runat="server" class="label" Text="Status:" CBID="691"></asp:Label>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="statusDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <div class="alR searchpad-top">
                                    <span class="btn-style">
                                        <asp:LinkButton ID="btnGo" runat="server" CBID="1044" Text="Go" OnClick="GoButton_Click"
                                            ValidationGroup="Click" />
                                    </span>
                                </div>
                                <div class="clear">
                                    &nbsp;</div>
                            </div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <asp:LinkButton ID="upateAllLinkButton" runat="server" Text="Update All" class="btn"
                        OnClick="UpdateAllLinkButton_Click"></asp:LinkButton>
                    <asp:LinkButton ID="copyTemplateLinkButton" runat="server" PostBackUrl="~/Admin/CopyBrandTemplate.aspx"
                        class="thickbox btn copytemplate" Text="Copy Brand Template"></asp:LinkButton>
                    <h2>
                        <asp:Label ID="projectTemplateHeadingLabel" runat="server" class="label" Text="Project Templates"
                            CBID="860"></asp:Label></h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <asp:GridView ID="projectListGridView" runat="server" ViewStateMode="Enabled" CssClass="col-6"
                                AutoGenerateColumns="False" OnRowCommand="ProjectListGridView_RowCommand" OnRowCreated="ProjectListGridView_RowCreated"
                                OnRowDataBound="ProjectListGridView_RowDataBound" OnSorting="ProjectListGridView_Sorting"
                                DataKeyNames="TemplateId,StatusId" AllowSorting="true" class="col-6">
                                <Columns>
                                    <asp:BoundField DataField="BrandName" SortExpression="BrandName" HeaderStyle-Width="18%" />
                                    <%--<asp:BoundField DataField="PropertyTypeDescription" SortExpression="PropertyTypeName"
                                        HeaderStyle-Width="15%" />--%>
                                    <asp:BoundField DataField="RegionDescription" SortExpression="RegionName" HeaderStyle-Width="25%" />
                                    <asp:BoundField DataField="CountryName" SortExpression="CountryName" HeaderStyle-Width="25%" />
                                    <asp:BoundField DataField="NoOfCategories" SortExpression="TotalCategories" HeaderStyle-Width="14%" />
                                    <asp:BoundField DataField="NoOfProjects" SortExpression="TotalProjects" HeaderStyle-Width="16%" />
                                    <asp:TemplateField SortExpression="StatusDesc" HeaderStyle-Width="12%">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="projectTemplateStatusDropDown" runat="server" CssClass="list-1">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="false" ItemStyle-Width="2%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="updateLinkButton" runat="server" CausesValidation="false" Text="update"
                                                CommandName="UpdateTemplate" CBID="861" CommandArgument='<%# Bind("TemplateId") %>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="viewCategoriesLinkButton" runat="server" CausesValidation="False"
                                                CommandName="ViewCategories" Text="view categories" CommandArgument='<%# Bind("TemplateId") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc1:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
