﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="Users.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.Users" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });
        })

        function InitializeToggle() {
            var toggleStateValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleStateValue == "0") {
                document.getElementById('toggleDiv').style.display = "block";
            }
            else {
                $('.toggle-divider a')[0].removeAttribute('class');
                document.getElementById('toggleDiv').style.display = "none";
            }

            $('.col-6 tr, .col-7 tr').hover(function () {

                $('.col-6 tr, .col-7 tr').children().removeClass('active');
                $(this).children().addClass('active');
            });

            $('.toggle-divider a').click(function () {
                $(this).toggleClass('active');
                $(this).parent().next().slideToggle();
                return false;
            });

            $(".datepicker").datepicker({ yearRange: "-3:+3", changeYear: true, dateFormat: 'dd-M-yy' });

        }

        function ToggleState() {
            var toggleValue = document.getElementById('<%=toggleStateHiddenField.ClientID %>').value;
            if (toggleValue == "0") {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "1";
            }
            else {
                document.getElementById('<%=toggleStateHiddenField.ClientID %>').value = "0";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="detailContent" ContentPlaceHolderID="cphDetails" runat="server">
    <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="usersUpdatePanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="search">
                <div class="wrapper">
                    <div class="top-curve">
                        <img src="../Images/search-top-curve.gif" alt="" /></div>
                    <div class="middle">
                        <div class="search-section">
                            <div class="alR searchpad-top">
                                <span class="btn-style1 search-btn1 ">
                                    <asp:LinkButton ID="quickSearchButton" Text="Search" runat="server" OnClick="QuickSearch_Click"
                                        CBID="652" ValidationGroup="SearchUsers" />
                                </span>
                            </div>
                            <div class="textbox">
                                <asp:RequiredFieldValidator ID="quickSearchRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SearchUsers" ControlToValidate="searchTextBox"
                                    Display="Dynamic" ErrorMessage="Please specify search criteria" VMTI="43"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="searchTextBox" runat="server" /></div>
                        </div>
                        <div class="toggle-divider">
                            <a id="toggleButton" class="active" onclick="ToggleState();"></a>
                        </div>
                        <div id="toggleDiv" class="fieldset toggle-content" style="display: block;">
                            <div class="clear">
                            </div>
                            <div class="right-field">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="field">
                                                <asp:Label ID="lastLoginLabel" runat="server" class="label" Text="Last Login:" CBID="424"></asp:Label>
                                                <div class="textbox-1">
                                                    <asp:TextBox ID="lastLoginFromTextBox" runat="server" class="datepicker"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="field">
                                                <asp:Label ID="toLabel" runat="server" class="label" Text="To:" CBID="721"></asp:Label>
                                                <div class="textbox-1">
                                                    <asp:TextBox ID="lastLoginToTextBox" runat="server" class="datepicker"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="field">
                                                <div class="alR searchpad-top" id="loginGoButtonDiv" runat="server" visible="false">
                                                    <span class="btn-style">
                                                        <asp:LinkButton ID="loginGoButton" Text="Go" Visible="false" runat="server" CBID="403"
                                                            OnClick="LoginGoButton_click" />
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="error-message search-msg">
                                                <asp:CustomValidator ID="validateLoginDateCustomValidator" runat="server" />
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="field">
                                                <asp:Label ID="activationDateLabel" runat="server" class="label" Text="Activation Date:"
                                                    CBID="168"></asp:Label>
                                                <div class="textbox-1">
                                                    <asp:TextBox ID="activationDateFromTextBox" runat="server" class="datepicker"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="field">
                                                <asp:Label ID="toActivationDate" runat="server" class="label" Text="To:" CBID="721"></asp:Label>
                                                <div class="textbox-1">
                                                    <asp:TextBox ID="activationDateToTextBox" runat="server" class="datepicker"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="field">
                                                <div class="alR searchpad-top">
                                                    <span class="btn-style">
                                                        <asp:LinkButton ID="activationGoButton" Text="Go" runat="server" OnClick="ActivationGoButton_click" />
                                                    </span>
                                                </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="error-message search-msg">
                                                <asp:CustomValidator ID="validateActivationDateCustomValidator" runat="server" />
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="field">
                                <span class="label">Type:</span>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="userTypeDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="field">
                                <span class="label">Status:</span>
                                <div class="textbox-2">
                                    <asp:DropDownList ID="userStatusDropDown" ViewStateMode="Enabled" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="clear">
                                &nbsp;</div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </div>
                    <div class="bottom-curve">
                        <img src="../Images/search-bottom-curve.gif" alt="" /></div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <asp:LinkButton ID="addUserLinkButton" runat="server" OnClick="AddUserLinkButton_Click"
                        CssClass="btn" Text="Add User"></asp:LinkButton>
                    <h2>
                        <asp:Label ID="usersHeadingLabel" runat="server" class="label" Text="Users" CBID="737"></asp:Label></h2>
                    <div class="inner-content">
                        <div id="searchResultDiv" runat="server">
                        </div>
                        <div class="no-space">
                            <asp:GridView ID="userListGridView" runat="server" ViewStateMode="Enabled" AllowSorting="true"
                                AutoGenerateColumns="False" class="col-6" OnRowCommand="UserListGridView_RowCommand"
                                OnRowCreated="UserListGridView_RowCreated" OnRowDataBound="UserListGridView_RowDataBound"
                                DataKeyNames="UserTypeId" OnSorting="UserListGridView_Sorting">
                                <Columns>
                                    <asp:BoundField DataField="LastName" SortExpression="LastName" HeaderStyle-Width="13%" />
                                    <asp:BoundField DataField="FirstName" SortExpression="FirstName" HeaderStyle-Width="13%" />
                                    <asp:BoundField DataField="Company" SortExpression="Company" HeaderStyle-Width="12%" />
                                    <asp:BoundField DataField="HiltonUserName" SortExpression="HiltonUserId" HeaderStyle-Width="12%" />
                                    <asp:BoundField DataField="UserTypeDescription" SortExpression="UserTypeDescription"
                                        HeaderStyle-Width="12%" />
                                    <asp:BoundField DataField="UserStatus" SortExpression="UserStatus" HeaderStyle-Width="10%" />
                                    <asp:BoundField DataField="ActivationDate" DataFormatString="{0:dd-MMM-yyyy}" SortExpression="ActivationDate"
                                        HeaderStyle-Width="13%" />
                                    <asp:BoundField DataField="LastLogOn" SortExpression="LastLogOn" DataFormatString="{0:dd-MMM-yyyy}"
                                        HeaderStyle-Width="14%" />
                                    <asp:TemplateField ShowHeader="False" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="editLinkButton" runat="server" CommandName="EditUser" CommandArgument='<%# Bind("UserId") %>'
                                                Text="edit" CBID="779"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc:PagingControl ID="pagingControl" ViewStateMode="Enabled" OnFirstClick="FirstClick"
                                OnPreviousClick="PreviousClick" OnNextClick="NextClick" OnLastClick="LastClick"
                                OnPageNumberClick="PageNumberClick" OnPagedViewClick="PagedViewClick" OnAllClick="AllClick"
                                runat="server" />
                        </div>
                        <div>
                            <br />
                            <asp:HiddenField ID="toggleStateHiddenField" runat="server" Value="0" />
                            <asp:HiddenField ID="isAdminUserHiddenField" runat="server" Value="0" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>