﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Standards : System.Web.UI.Page
    {
        private DataSet ds = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "Updated Standards";

            //HtmlGenericControl li = new HtmlGenericControl("li");
            //li = (HtmlGenericControl)(this.Master.FindControl("liStandards"));
            //li.Attributes.Add("class", "active");
            ds.ReadXml(MapPath("~/App_Data/Standards.xml"));
            UpdatedStandardsGridView.DataSource = ds;
            UpdatedStandardsGridView.DataBind();
        }

        protected void UpdatedStandardsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow rowItem = e.Row;
            for (int dataRowCount = 0; dataRowCount < ds.Tables[0].Rows.Count; dataRowCount++)
            {
                if (dataRowCount == rowItem.DataItemIndex)
                {
                    DropDownList status = (DropDownList)rowItem.FindControl("StatusDropDownList");
                    status.Items.Add(ds.Tables[0].Rows[dataRowCount]["Status"].ToString());
                }
            }
        }
    }
}