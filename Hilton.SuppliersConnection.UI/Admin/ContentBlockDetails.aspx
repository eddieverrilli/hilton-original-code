﻿<%@ Page Title="Content Block" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    ValidateRequest="false" AutoEventWireup="True" CodeBehind="ContentBlockDetails.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.ContentBlockDetails" %>

<asp:Content ID="content2" ContentPlaceHolderID="cphHead" runat="server">
    <script src="../Scripts/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-tinymce.js" type="text/javascript"></script>
    <script src="../Scripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
    <script type="text/javascript">
        // Initializes all textareas with the tinymce class

        tinyMCE.init(
        {
            script_url: '../Scripts/tiny_mce/tiny_mce.js',
            mode: "exact",
            elements: "ctl00$cphDetails$elm1",
            plugins: "lists,spellchecker,inlinepopups,preview,table,contextmenu,paste,directionality,fullscreen,",

            // Theme options
            theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,sub,sup,|,hr,|,removeformat,|,charmap,",
            theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,|,code,|,insertdate,inserttime,preview,visualaid,|,fullscreen,",
            theme_advanced_buttons3: "tablecontrols,",
            theme_advanced_buttons4: "ltr,rtl,|,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_statusbar_location: "bottom",
            theme_advanced_resizing: false,
            visualblocks_default_state: true,

            // Schema is HTML5 instead of default HTML4
            schema: "html5",

            // End container block element when pressing enter inside an empty block
            end_container_on_empty_block: true,

            // Skin options
            skin: "o2k7",
            skin_variant: "silver",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "js/template_list.js",
            external_link_list_url: "js/link_list.js",
            external_image_list_url: "js/image_list.js",
            media_external_list_url: "js/media_list.js",

            // HTML5 formats
            style_formats: [
                { title: 'h1', block: 'h1' },
                { title: 'h2', block: 'h2' },
                { title: 'h3', block: 'h3' },
                { title: 'h4', block: 'h4' },
                { title: 'h5', block: 'h5' },
                { title: 'h6', block: 'h6' },
                { title: 'p', block: 'p' },
                { title: 'div', block: 'div' },
                { title: 'pre', block: 'pre' },
                { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
                { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
                { title: 'blockquote', block: 'blockquote', wrapper: true },
                { title: 'hgroup', block: 'hgroup', wrapper: true },
                { title: 'aside', block: 'aside', wrapper: true },
                { title: 'figure', block: 'figure', wrapper: true }
        ]

        });

        function ReInitializeTinyMCE() {
            tinyMCE.idCounter = 0;
            tinyMCE.execCommand('mceRemoveControl', true, 'cphDetails_elm1');

            tinyMCE.init(
            {
                script_url: '../Scripts/tiny_mce/tiny_mce.js',
                mode: "exact",
                elements: "ctl00$cphDetails$elm1",
                theme: "advanced",
                plugins: "lists,spellchecker,inlinepopups,preview,table,contextmenu,paste,directionality,fullscreen,",

                // Theme options
                theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,sub,sup,|,hr,removeformat,|,charmap,",
                theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,|,code,|,insertdate,inserttime,preview,visualaid,|,fullscreen,",
                theme_advanced_buttons3: "tablecontrols,",
                theme_advanced_buttons4: "ltr,rtl,|,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                visualblocks_default_state: true,

                // Schema is HTML5 instead of default HTML4
                schema: "html5",

                // End container block element when pressing enter inside an empty block
                end_container_on_empty_block: true,

                // Skin options
                skin: "o2k7",
                skin_variant: "silver",

                // Drop lists for link/image/media/template dialogs
                template_external_list_url: "js/template_list.js",
                external_link_list_url: "js/link_list.js",
                external_image_list_url: "js/image_list.js",
                media_external_list_url: "js/media_list.js",

                // HTML5 formats
                style_formats: [
                { title: 'h1', block: 'h1' },
                { title: 'h2', block: 'h2' },
                { title: 'h3', block: 'h3' },
                { title: 'h4', block: 'h4' },
                { title: 'h5', block: 'h5' },
                { title: 'h6', block: 'h6' },
                { title: 'p', block: 'p' },
                { title: 'div', block: 'div' },
                { title: 'pre', block: 'pre' },
                { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
                { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
                { title: 'blockquote', block: 'blockquote', wrapper: true },
                { title: 'hgroup', block: 'hgroup', wrapper: true },
                { title: 'aside', block: 'aside', wrapper: true },
                { title: 'figure', block: 'figure', wrapper: true }
        ]

            });

        }

        function UpdateTextArea() {
            tinyMCE.triggerSave(false, true);
        }

        function ValidateTinyMCE(sender, args) {
            var isValid = false;
            // var value = tinyMCE.get(tinyMCE.activeEditor.editorId).getContent();
            //if (value == "") {
            var value = tinyMCE.get(tinyMCE.activeEditor.editorId).getContent({ format: 'text' });
            if (value.replace(/\s/g, "").length == 0) {
                args.IsValid = false;
            }
            else {
                //Check for space tag
                if (value == "<p>&nbsp;</p>") {
                    //Clear TinyMCE
                    tinyMCE.get(tinyMCE.activeEditor.editorId).execCommand('mceSetContent', false, "");
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphDetails" ViewStateMode="Enabled"
    runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        function pageLoad() {

            $("#<%= titleTextBox.ClientID %>").blur(function () {
                var isReadonly = $("#<%= titleTextBox.ClientID %>").attr('readonly');
                if (!isReadonly) {
                    $.ajax({
                        type: "POST",
                        url: "ContentBlockDetails.aspx/GetDescription",
                        data: "{'title': '" + $("#<%= titleTextBox.ClientID %>").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            // Replace the description TextBox content with the page method's return.
                            $("#<%= descriptionTextBox.ClientID %>").text(msg.d);
                        }
                    });
                }
            });

            $("#<%= titleTextBox.ClientID %>").blur(function () {
                var isReadonly = $("#<%= titleTextBox.ClientID %>").attr('readonly');
                if (!isReadonly) {

                    $.ajax({
                        type: "POST",
                        url: "ContentBlockDetails.aspx/GetLanguageContent",
                        data: "{'title': '" + $("#<%= titleTextBox.ClientID %>").val() + "','selectedLanguageDropDownValue': '" + $("#<%= languageDropDown.ClientID %>").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            // Replace the tinymce editor content with the page method's return.
                            tinyMCE.activeEditor.setContent(msg.d);

                        }
                    });
                }
            });
-

            $("#<%= sectionTextBox.ClientID %>").autocomplete("ContentBlockDetails.aspx/GetSectionAutoComplete", {
                matchContains: true,
                mustMatch: false,
                delay: 200,
                selectOnly: true,
                minChars: 1,
                cacheLength: 20,
                max: 20,
                selectFirst: false,
                isPageMethod: true,
                formatItem: function (item, index, total, query) {
                    return item.Value;
                },
                extraParams: {
                    value: function () {
                        var input = $("#<%= sectionTextBox.ClientID %>").val();
                        var isReadonly = $("#<%= sectionTextBox.ClientID %>").attr('readonly');
                        if (isReadonly) {
                            input = "*";
                        }
                        return input;
                    }

                },
                parse: function (objData) {
                    objTemp = $.evalJSON(objData.d);
                    var arrParsed = [];
                    for (i = 0; i < objTemp.length; i++) {
                        arrParsed[i] = {
                            data: objTemp[i],
                            value: objTemp[i].Value,
                            result: objTemp[i].Value
                        };
                    }
                    return arrParsed;
                }
            });

            $("#<%= titleTextBox.ClientID %>").autocomplete("ContentBlockDetails.aspx/GetTitleAutoComplete", {
                matchContains: true,
                mustMatch: false,
                delay: 200,
                selectOnly: true,
                minChars: 1,
                cacheLength: 20,
                max: 20,
                selectFirst: false,
                isPageMethod: true,
                formatItem: function (item, index, total, query) {
                    return item.Value;
                },
                extraParams: {
                    value: function () {
                        var input = $("#<%= titleTextBox.ClientID %>").val();
                        var isReadonly = $("#<%= titleTextBox.ClientID %>").attr('readonly');
                        if (isReadonly) {
                            input = "*";
                        }
                        return input;
                    }

                },
                parse: function (objData) {
                    objTemp = $.evalJSON(objData.d);
                    var arrParsed = [];
                    for (i = 0; i < objTemp.length; i++) {
                        arrParsed[i] = {
                            data: objTemp[i],
                            value: objTemp[i].Value,
                            result: objTemp[i].Value
                        };
                    }
                    return arrParsed;
                }
            });
        };

    </script>
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../Images/search-top-curve.gif" alt="" />
            </div>
            <div class="middle">
                <asp:HyperLink ID="backToContentBlockHyperLink" runat="server" NavigateUrl="~/Admin/ContentBlocks.aspx"
                    Text="‹‹ Back to Content Blocks"></asp:HyperLink>&nbsp;&nbsp;|&nbsp;&nbsp;
                <asp:HyperLink ID="backToSettingHyperLink" runat="server" NavigateUrl="~/Admin/Settings.aspx"
                    CBID="245" Text="Back to Settings"></asp:HyperLink>
            </div>
            <div class="bottom-curve">
                <img src="../Images/search-bottom-curve.gif" alt="" />
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="contentBlockUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="content">
                <div class="wrapper">
                    <h2>
                        <asp:Label ID="contentBlockTitleLabel" runat="server" Text=""></asp:Label>
                    </h2>
                    <div class="inner-content">
                        <div id="resultMessageDiv" runat="server">
                        </div>
                        <br />
                        <div class="lft-col">
                            <h3>
                                <asp:Label ID="headerLabel" runat="server" Text="Details" CBID="366"></asp:Label>
                            </h3>
                            <div class="row">
                                <div>
                                    <asp:Label ID="idLabel" runat="server" CBID="421" Text="ID:" class="label"></asp:Label></div>
                                <div class="input" id="divIDValue" runat="server">
                                    <asp:Label ID="contentBlockIdLabel" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div>
                                    <asp:Label ID="sectionLabel" runat="server" CBID="717" Text="Title:" class="label"></asp:Label></div>
                                <div id="sectionDiv" runat="server" class="input textbox3 mandatory">
                                    <asp:TextBox ID="sectionTextBox" runat="server"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="sectionRequiredFieldValidator" VMTI="29" runat="server"
                                        SetFocusOnError="true" CssClass="errorText" ControlToValidate="sectionTextBox"
                                        ErrorMessage="Title is required" Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div>
                                    <asp:Label ID="titleLabel" CssClass="label" CBID="657" Text="Section:" runat="server"></asp:Label>
                                </div>
                                <div id="titleDiv" runat="server" class="input textbox3 mandatory">
                                    <asp:TextBox ID="titleTextBox" runat="server"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="titleRequiredFieldValidator" runat="server" CssClass="errorText"
                                        ControlToValidate="titleTextBox" SetFocusOnError="true" ErrorMessage="Section is required"
                                        VMTI="28" Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div>
                                    <asp:Label ID="Label1" CssClass="label" CBID="363" Text="Description:" runat="server"></asp:Label>
                                </div>
                                <div class="input textarea">
                                    <asp:TextBox ID="descriptionTextBox" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    <asp:HiddenField ID="languageResourceIdHiddenField" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="section alR pad-top">
                                <span class="btn-style">
                                    <asp:LinkButton ID="saveLinkButton" runat="server" CBID="643" OnClientClick="UpdateTextArea()"
                                        OnClick="SaveLinkButton_Click"></asp:LinkButton>
                                </span>
                            </div>
                        </div>
                        <div class="rgt-col">
                            <h3>
                                <asp:Label ID="contentLabel" runat="server" Text="Content" CBID="335"></asp:Label></h3>
                            <div class="row">
                                <div>
                                    <asp:Label ID="Label2" runat="server" class="label" Text="Language:" CBID="423"></asp:Label></div>
                                <div class="input">
                                    <asp:DropDownList ID="languageDropDown" runat="server" CssClass="list-2" AutoPostBack="True"
                                        onchange="UpdateTextArea()" OnSelectedIndexChanged="LanguageDropDown_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row">
                                <div>
                                    <asp:Label ID="Label3" runat="server" class="label" Text="Content:" CBID="335"></asp:Label>
                                    <asp:CustomValidator ID="contentCustomValidator" ClientValidationFunction="ValidateTinyMCE"
                                        runat="server" CssClass="errorText" ErrorMessage="Content is required"></asp:CustomValidator>
                                </div>
                            </div>
                            <div class="editor">
                                <textarea id="elm1" name="elm1" rows="25" cols="80" style="width: 100%" runat="server"></textarea>
                                <asp:HiddenField ID="contentBlockIdHiddenField" runat="server" Value="0" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>