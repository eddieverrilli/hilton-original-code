﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;
using Newtonsoft.Json;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ContentBlockDetails : PageBase
    {
        private IContentBlockManager _contentBlockManager;

        /// <summary>
        /// Get the description of content block for specified title
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetDescription(string title)
        {
            IList<LanguageResourceDetail> languageResourceDetailsList = ResourceUtility.GetLanguageResourceDetails();
            LanguageResourceDetail languageResourceDetail = languageResourceDetailsList.Where(x => x.Title.Equals(title, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (languageResourceDetail != null)
            {
                return languageResourceDetail.Description;
            }

            return string.Empty;
        }

        /// <summary>
        /// Get the language specified content
        /// </summary>
        /// <param name="title"></param>
        /// <param name="selectedLanguageDropDownValue"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetLanguageContent(string title, int selectedLanguageDropDownValue)
        {
            IList<LanguageResourceDetail> languageResourceDetailsList = ResourceUtility.GetLanguageResourceDetails();
            LanguageResourceDetail languageResourceDetail = languageResourceDetailsList.Where(x => x.Title.Equals(title, StringComparison.InvariantCultureIgnoreCase)).Where(x => x.CultureTypeId == selectedLanguageDropDownValue).FirstOrDefault();
            if (languageResourceDetail != null)
            {
                return languageResourceDetail.Content;
            }

            return string.Empty;
        }

        /// <summary>
        /// Auto complete the section
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetSectionAutoComplete(string value)
        {
            IList<KeyValuePair<string, string>> lstKeyValuePair = GetAutoCompleteList(AutoCompleteListConstants.SectionAutoCompleteList);
            IList<KeyValuePair<string, string>> lstKeyValuePairFiltered = lstKeyValuePair.Where(x => x.Key.StartsWith(value, StringComparison.OrdinalIgnoreCase)).ToList();
            return JsonConvert.SerializeObject(lstKeyValuePairFiltered);
        }

        /// <summary>
        /// Auto complete the title
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetTitleAutoComplete(string value)
        {
            IList<KeyValuePair<string, string>> lstKeyValuePair = GetAutoCompleteList(AutoCompleteListConstants.TitleAutoCompleteList);
            IList<KeyValuePair<string, string>> lstKeyValuePairFiltered = lstKeyValuePair.Where(x => x.Key.StartsWith(value, StringComparison.OrdinalIgnoreCase)).ToList();
            return JsonConvert.SerializeObject(lstKeyValuePairFiltered);
        }

        /// <summary>
        /// Invoked in language dropdown selected index change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LanguageDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ContentBlock contentBlock = GetViewState<ContentBlock>(ViewStateConstants.ContentBlockViewState);
                int languageResourceId = Convert.ToInt32(languageResourceIdHiddenField.Value, CultureInfo.InvariantCulture);
                int? currentLanguageResourceId = null;
                IList<LanguageResourceDetail> languageResourceDetailsList = ResourceUtility.GetLanguageResourceDetails();
                LanguageResourceDetail languageResourceDetail = languageResourceDetailsList.Where(x => x.Title.Equals(titleTextBox.Text.Trim(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (languageResourceDetail != null)
                {
                    currentLanguageResourceId = languageResourceDetail.LanguageResourceId;
                }
                //Edit Mode
                if (contentBlock != null && languageResourceId == currentLanguageResourceId)
                {
                    LanguageResourceDetail cultureLanguageResourceDetail = contentBlock.LanguageResourceDetails.FirstOrDefault(x => x.CultureTypeId == Convert.ToInt16(languageDropDown.SelectedValue, CultureInfo.InvariantCulture));
                    if (cultureLanguageResourceDetail != null)
                    {
                        this.elm1.Value = cultureLanguageResourceDetail.Content;
                    }
                    else
                    {
                        this.elm1.Value = string.Empty;
                    }
                }
                else
                {
                    //Add Mode
                    if (languageResourceDetail != null)
                    {
                        int lrid = languageResourceDetail.LanguageResourceId;
                        IList<LanguageResourceDetail> languageResourceDetails = languageResourceDetailsList.Where(x => x.LanguageResourceId == lrid).ToList();
                        LanguageResourceDetail selectedCultureResourceDetail = languageResourceDetails.FirstOrDefault(x => x.CultureTypeId == Convert.ToInt16(languageDropDown.SelectedValue, CultureInfo.InvariantCulture));
                        if (selectedCultureResourceDetail != null)
                        {
                            string languageContent = selectedCultureResourceDetail.Content;
                            if (!string.IsNullOrEmpty(languageContent))
                                this.elm1.Value = languageContent;
                        }
                        else
                        {
                            this.elm1.Value = string.Empty;
                        }
                    }
                    else
                    {
                        this.elm1.Value = string.Empty;
                    }
                }

                HttpBrowserCapabilities browser = Request.Browser;
                if (browser.Browser.ToUpper() == "IE" || browser.Browser.ToUpper() == "CHROME")
                {
                    ScriptManager.RegisterClientScriptBlock(contentBlockUpdatePanel, this.GetType(), "init", "ReInitializeTinyMCE();", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on page initialization
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                ValidateUserAccess((int)MenuEnum.Settings);

                _contentBlockManager = ContentBlockManager.Instance;

                backToContentBlockHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "‹‹ {0}", ResourceUtility.GetLocalizedString(241, culture, "Back to Content Blocks"));

                HttpBrowserCapabilities browser = Request.Browser;
                if (!(browser.Browser.ToUpper() == "IE" || browser.Browser.ToUpper() == "CHROME"))
                {
                    ScriptManager.GetCurrent(this).RegisterPostBackControl(saveLinkButton);
                    ScriptManager.GetCurrent(this).RegisterPostBackControl(languageDropDown);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoke on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    if (!Page.IsPostBack)
                    {
                        backToContentBlockHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "‹‹ {0}", ResourceUtility.GetLocalizedString(241, culture));

                        PopulateDropDownData();

                        if (Context.Items[UIConstants.Mode] != null && Context.Items[UIConstants.Mode].ToString() == UIConstants.Edit)
                        {
                            SetPageSettings(UIConstants.Edit);

                            int contentBlockId = Convert.ToInt32(Context.Items[UIConstants.ContentBlockId], CultureInfo.InvariantCulture);
                            contentBlockIdHiddenField.Value = contentBlockId.ToString();
                            PopulateContentBlockDetails(contentBlockId);
                        }
                        else
                        {
                            SetPageSettings(UIConstants.Add);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when save button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (GetViewState<string>(UIConstants.Mode) == UIConstants.Add)
                {
                    AddContentBlock();
                }
                else if ((GetViewState<string>(UIConstants.Mode) == UIConstants.Edit))
                {
                    UpdateContentBlock();
                }

                HttpBrowserCapabilities browser = Request.Browser;
                if (browser.Browser.ToUpper() == "IE" || browser.Browser.ToUpper() == "CHROME")
                {
                    ScriptManager.RegisterClientScriptBlock(contentBlockUpdatePanel, this.GetType(), "init", "ReInitializeTinyMCE();", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Add content block
        /// </summary>
        private void AddContentBlock()
        {
            ContentBlock contentBlock = new ContentBlock()
            {
                Section = sectionTextBox.Text.Trim(),
                Title = titleTextBox.Text.Trim(),
                Description = descriptionTextBox.Text.Trim(),
                LanguageResourceDetail = new LanguageResourceDetail()
                {
                    Content = this.elm1.Value.Trim().Replace("em>", "i>"),
                    CultureTypeId = Convert.ToInt16(languageDropDown.SelectedValue, CultureInfo.InvariantCulture),
                }
            };

            int result = _contentBlockManager.AddContentBlock(contentBlock);
            if (result == 0)
            {
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.ContentBlockAlreadyExists, culture));
            }
            else if (result > 0)
            {
                contentBlockIdLabel.Text = result.ToString(CultureInfo.InvariantCulture);
                contentBlockIdHiddenField.Value = contentBlockIdLabel.Text;
                ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.ContentBlockSuccessfullyAdded, culture));

                UpdateDynamicDropDownList(DropDownConstants.SectionDropDown);

                ResourceUtility.UpdateContentBlocks();
                SetViewState<string>(UIConstants.Mode, UIConstants.Edit);
                PopulateContentBlockDetails(Convert.ToInt32(contentBlockIdLabel.Text));

                //Update Auto complete list for section and title
                if (!IsAutoCompleteListContainsKey(AutoCompleteListConstants.TitleAutoCompleteList, titleTextBox.Text.Trim()))
                    UpdateAutoCompleteList(AutoCompleteListConstants.TitleAutoCompleteList);

                if (!IsAutoCompleteListContainsKey(AutoCompleteListConstants.SectionAutoCompleteList, sectionTextBox.Text.Trim()))
                    UpdateAutoCompleteList(AutoCompleteListConstants.SectionAutoCompleteList);
            }
        }

        /// <summary>
        /// This method will populate content block details
        /// </summary>
        /// <param name="contentBlockId"></param>
        private void PopulateContentBlockDetails(int contentBlockId)
        {
            ContentBlock contentBlock = _contentBlockManager.GetContentBlockDetails(contentBlockId);
            SetLabelText(contentBlockIdLabel, contentBlock.ContentBlockId.ToString(CultureInfo.InvariantCulture));
            SetTextBoxText(sectionTextBox, contentBlock.Section);
            SetTextBoxText(titleTextBox, contentBlock.Title);
            SetTextBoxText(descriptionTextBox, contentBlock.Description);

            SetHiddenFieldValue(languageResourceIdHiddenField, contentBlock.LanguageResourceId.ToString(CultureInfo.InvariantCulture));
            languageDropDown.SelectedIndex = 0;
            LanguageResourceDetail languageResourceDetail = contentBlock.LanguageResourceDetails.FirstOrDefault(x => x.CultureTypeId == (int)CultureTypeEnum.English);

            if (languageResourceDetail != null)
                this.elm1.Value = languageResourceDetail.Content;
            SetViewState<ContentBlock>(ViewStateConstants.ContentBlockViewState, contentBlock);
        }

        /// <summary>
        /// Populate dropdown data
        /// </summary>
        private void PopulateDropDownData()
        {
            //Populate Language Drop Down
            BindStaticDropDown(DropDownConstants.LanguageDropDown, languageDropDown, null, null);
        }

        /// <summary>
        /// Set the page setting for Add/Edit view
        /// </summary>
        private void SetPageSettings(string mode)
        {
            elm1.Attributes.Add("class", "mandatory");
            switch (mode)
            {
                case UIConstants.Edit:
                    SetViewState(UIConstants.Mode, UIConstants.Edit);
                    SetLabelText(contentBlockTitleLabel, ResourceUtility.GetLocalizedString(183, culture, "Edit Content Block"));

                    break;

                case UIConstants.Add:
                    SetViewState(UIConstants.Mode, UIConstants.Add);
                    SetLabelText(contentBlockTitleLabel, ResourceUtility.GetLocalizedString(182, culture, "Add Content Block"));
                    break;
            }
        }

        /// <summary>
        /// Update content block on edit
        /// </summary>
        private void UpdateContentBlock()
        {
            ContentBlock contentBlockData = GetViewState<ContentBlock>(ViewStateConstants.ContentBlockViewState);
            int contentBlockId = contentBlockData.ContentBlockId;
            int languageResourceId = Convert.ToInt32(languageResourceIdHiddenField.Value, CultureInfo.InvariantCulture);
            string title = titleTextBox.Text.Trim();

            int? currentLanguageResourceId = null;
            IList<LanguageResourceDetail> languageResourceDetailsList = ResourceUtility.GetLanguageResourceDetails();
            LanguageResourceDetail languageResourceDetail = languageResourceDetailsList.Where(x => x.Title.Equals(title, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (languageResourceDetail != null)
            {
                currentLanguageResourceId = languageResourceDetail.LanguageResourceId;
            }
            int result = -1;

            ContentBlock contentBlock = new ContentBlock()
            {
                ContentBlockId = contentBlockId,
                Section = sectionTextBox.Text.Trim(),
                Title = titleTextBox.Text.Trim(),
                Description = descriptionTextBox.Text.Trim(),
                LanguageResourceDetail = new LanguageResourceDetail()
                {
                    Content = this.elm1.Value.Trim().Replace("em>", "i>"),
                    CultureTypeId = Convert.ToInt16(languageDropDown.SelectedValue, CultureInfo.InvariantCulture),
                }
            };

            if (currentLanguageResourceId.HasValue && languageResourceId == currentLanguageResourceId)
            {
                contentBlock.Mode = UIConstants.Update;
                contentBlock.LanguageResourceId = Convert.ToInt32(languageResourceIdHiddenField.Value, CultureInfo.InvariantCulture);
            }
            else if (currentLanguageResourceId.HasValue)
            {
                contentBlock.Mode = UIConstants.Change;
                contentBlock.LanguageResourceId = currentLanguageResourceId.Value;
            }
            else
            {
                contentBlock.Mode = UIConstants.New;
            }
            result = _contentBlockManager.UpdateContentBlock(contentBlock);

            if (result == 0)
            {
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.ContentBlockAlreadyExists, culture));
            }
            else
            {
                ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.ContentBlockSuccessfullyUpdated, culture));

                ContentBlock contentBlockUpdate = _contentBlockManager.GetContentBlockDetails(result);
                SetViewState<ContentBlock>(ViewStateConstants.ContentBlockViewState, contentBlockUpdate);

                SetHiddenFieldValue(languageResourceIdHiddenField, contentBlockUpdate.LanguageResourceId.ToString(CultureInfo.InvariantCulture));
                ResourceUtility.UpdateContentBlocks();

                UpdateDynamicDropDownList(DropDownConstants.SectionDropDown);

                ResourceUtility.UpdateLanguageResources();

                //Update Auto complete list for section and title
                if (!IsAutoCompleteListContainsKey(AutoCompleteListConstants.TitleAutoCompleteList, titleTextBox.Text.Trim()))
                    UpdateAutoCompleteList(AutoCompleteListConstants.TitleAutoCompleteList);

                if (!IsAutoCompleteListContainsKey(AutoCompleteListConstants.SectionAutoCompleteList, sectionTextBox.Text.Trim()))
                    UpdateAutoCompleteList(AutoCompleteListConstants.SectionAutoCompleteList);
            }
        }
    }
}