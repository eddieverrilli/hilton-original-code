﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Users : PageBase
    {
        private IUserManager _userManager;

        /// <summary>
        /// Get the data when user clicks on activate go button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ActivationGoButton_click(object sender, EventArgs e)
        {
            try
            {
                validateActivationDateCustomValidator.ServerValidate += new ServerValidateEventHandler(ValidateActivationDate);
                validateActivationDateCustomValidator.Validate();
                validateLoginDateCustomValidator.ServerValidate += new ServerValidateEventHandler(ValidateLoginDate);
                validateLoginDateCustomValidator.Validate();
                if (IsValid)
                {
                    UserSearch userSearch = BuildSearchCriteria();
                    userSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                    userSearch.SearchSource = UIConstants.ActivationGo;

                    GetUserList(userSearch);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Navigate to add user page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddUserLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                if (userSearch != null)
                {
                    userSearch.PageSet = pagingControl.PageSet;
                    ClearSession(SessionConstants.UserSearch);
                    SetSession<UserSearch>(SessionConstants.UserSearch, userSearch);
                }
                Response.Redirect("~/Admin/UserDetails.aspx", false);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                userSearch.PageIndex = pagingControl.CurrentPageIndex;
                userSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                userSearch.AllClick = pagingControl.AllPageClick;
                SearchUsers(userSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                userSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchUsers(userSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                userSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchUsers(userSearch);
                ClearSession(SessionConstants.UserSearch);
                SetSession<UserSearch>(SessionConstants.UserSearch, userSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Get the user list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoginGoButton_click(object sender, EventArgs e)
        {
            try
            {
                validateLoginDateCustomValidator.ServerValidate += new ServerValidateEventHandler(ValidateLoginDate);
                validateLoginDateCustomValidator.Validate();
                if (IsValid)
                {
                    UserSearch userSearch = BuildSearchCriteria();
                    userSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                    userSearch.SearchSource = UIConstants.LoginOnGo;

                    GetUserList(userSearch);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                userSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchUsers(userSearch);
                ClearSession(SessionConstants.UserSearch);
                SetSession<UserSearch>(SessionConstants.UserSearch, userSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Validate the user and get the manager instance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                //Validate the user
                ValidateUserAccess((int)MenuEnum.Users);
                _userManager = UserManager.Instance;
                addUserLinkButton.Text = string.Format(CultureInfo.InvariantCulture, "+ {0}", ResourceUtility.GetLocalizedString(193, culture, "Add User"));
                this.Title = ResourceUtility.GetLocalizedString(1108, culture, "User List");
                SetGridViewHeaderText();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Set the grid view header text
        /// </summary>
        private void SetGridViewHeaderText()
        {
            userListGridView.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(120, culture, "Last Name");
            userListGridView.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(108, culture, "First Name");
            userListGridView.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(301, culture, "Company");
            userListGridView.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(416, culture, "Hilton ID");
            userListGridView.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(733, culture, "User Type");
            userListGridView.Columns[5].HeaderText = ResourceUtility.GetLocalizedString(732, culture, "Status");
            userListGridView.Columns[6].HeaderText = ResourceUtility.GetLocalizedString(168, culture, "Activation Date");
            userListGridView.Columns[7].HeaderText = ResourceUtility.GetLocalizedString(424, culture, "Last Login");
        }

        /// <summary>
        /// Bind the data to control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;

                    validateActivationDateCustomValidator.ErrorMessage = string.Empty;
                    validateLoginDateCustomValidator.ErrorMessage = string.Empty;

                    if (!Page.IsPostBack)
                    {
                        User user = GetSession<User>(SessionConstants.User);

                        var isAdministrativeUser = user.Permissions.Any(x => x.PermissionId == (int)AdminPermissionEnum.AdministrativeUsers);// Administrative User permission
                        if (isAdministrativeUser)
                            isAdminUserHiddenField.Value = "1";

                        PopulateDropDownData();
                        if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("UserDetails.aspx") || Request.UrlReferrer.ToString().Contains("Users.aspx")))
                        {
                            MaintainPageState();
                        }
                        else if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Contains("Products.aspx"))
                        {
                            string userId = GetSession<string>(SessionConstants.UserId);
                            if (!string.IsNullOrWhiteSpace(userId))
                            {
                                UserSearch userSearch = BuildSearchCriteria();
                                userSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                                userSearch.SearchSource = UIConstants.ActivationGo;
                                userSearch.UserId = userId;
                                GetUserList(userSearch);
                                ClearSession(SessionConstants.UserId);
                            }
                        }
                        else
                        {
                            ClearSession(SessionConstants.UserSearch);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(usersUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                userSearch.PageIndex = pagingControl.CurrentPageIndex;
                userSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                userSearch.AllClick = pagingControl.AllPageClick;
                SearchUsers(userSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                userSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchUsers(userSearch);
                ClearSession(SessionConstants.UserSearch);
                SetSession<UserSearch>(SessionConstants.UserSearch, userSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                userSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchUsers(userSearch);
                ClearSession(SessionConstants.UserSearch);
                SetSession<UserSearch>(SessionConstants.UserSearch, userSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method is invoked with search button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void QuickSearch_Click(object sender, EventArgs args)
        {
            try
            {
                UserSearch userSearch = BuildSearchCriteria();
                userSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                GetUserList(userSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Go to the edit user page when user clicks on edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void UserListGridView_RowCommand(object sender, CommandEventArgs args)
        {
            try
            {
                if (args != null && (args.CommandName == "EditUser"))
                {
                    UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                    userSearch.PageSet = pagingControl.PageSet;
                    ClearSession(SessionConstants.UserSearch);
                    SetSession<UserSearch>(SessionConstants.UserSearch, userSearch);
                    Context.Items[UIConstants.Mode] = UIConstants.Edit;
                    Context.Items[UIConstants.UserId] = args.CommandArgument.ToString();
                    Server.Transfer("~/Admin/UserDetails.aspx", true);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        /// Grid view row created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void UserListGridView_RowCreated(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex();
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, args.Row);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Grid view row data bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void UserListGridView_RowDataBound(object sender, GridViewRowEventArgs args)
        {
            try
            {
                if (args != null && args.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton editLinkButton = args.Row.FindControl("editLinkButton") as LinkButton;
                    if (editLinkButton != null)
                    {
                        if (isAdminUserHiddenField.Value != "1")
                        {
                            int userTypeId = Convert.ToInt16(userListGridView.DataKeys[args.Row.RowIndex].Value, CultureInfo.InvariantCulture);
                            if (userTypeId == (int)UserTypeEnum.Administrator)
                            {
                                editLinkButton.Visible = false;
                            }
                        }
                        ScriptManager.GetCurrent(this).RegisterPostBackControl(editLinkButton);
                    }

                    if (args.Row.Cells[6].Text == "&nbsp;")
                    {
                        args.Row.Cells[6].Text = "-";
                    }

                    if (args.Row.Cells[7].Text == "&nbsp;")
                    {
                        args.Row.Cells[7].Text = "-";
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when grid header column is clicked for sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void UserListGridView_Sorting(object sender, GridViewSortEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    string sortExpression = args.SortExpression;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortGridView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortGridView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Validate the activation date
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidateActivationDate(object source, ServerValidateEventArgs args)
        {
            try
            {
                ValidateDate(args, validateActivationDateCustomValidator, activationDateFromTextBox.Text, activationDateToTextBox.Text);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Validate the login date
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidateLoginDate(object source, ServerValidateEventArgs args)
        {
            try
            {
                ValidateDate(args, validateLoginDateCustomValidator, lastLoginFromTextBox.Text, lastLoginToTextBox.Text);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Bind the user list
        /// </summary>
        /// <param name="userList"></param>
        private void BindUserList(IList<User> userList)
        {
            userListGridView.DataSource = userList;
            userListGridView.DataBind();
        }

        /// <summary>
        /// This method will populate the search criteria
        /// </summary>
        /// <returns></returns>
        private UserSearch BuildSearchCriteria()
        {
            DateTime dateTime;
            UserSearch userSearch = new UserSearch()
            {
                QuickSearchExpression = searchTextBox.Text.Trim(),
                TypeId = (userTypeDropDown.SelectedIndex == 0) ? string.Empty : userTypeDropDown.SelectedValue,
                StatusId = (userStatusDropDown.SelectedIndex == 0) ? string.Empty : userStatusDropDown.SelectedValue,
                PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture),
                PageIndex = 0,
                UserId = string.Empty,
            };

            if (DateTime.TryParse(activationDateToTextBox.Text, out dateTime))
            {
                userSearch.ActivateDateTo = Convert.ToDateTime(activationDateToTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }
            if (DateTime.TryParse(activationDateFromTextBox.Text, out dateTime))
            {
                userSearch.ActivationDateFrom = Convert.ToDateTime(activationDateFromTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }
            if (DateTime.TryParse(lastLoginFromTextBox.Text, out dateTime))
            {
                userSearch.LastLoginFrom = Convert.ToDateTime(lastLoginFromTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }
            if (DateTime.TryParse(lastLoginToTextBox.Text, out dateTime))
            {
                userSearch.LastLoginTo = Convert.ToDateTime(lastLoginToTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }
            return userSearch;
        }

        /// <summary>
        /// This method will return the sort column index
        /// </summary>
        /// <returns></returns>
        private int GetSortColumnIndex()
        {
            foreach (DataControlField field in userListGridView.Columns)
            {
                if (field.SortExpression == GetViewState<string>(ViewStateConstants.SortExpression))
                {
                    return userListGridView.Columns.IndexOf(field);
                }
                else if (!Page.IsPostBack)
                {
                    if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("Users.aspx") || Request.UrlReferrer.ToString().Contains("UserDetails.aspx")))
                    {
                        UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
                        if (field.SortExpression == userSearch.SortExpression)
                        {
                            SetViewState<string>(ViewStateConstants.SortExpression, userSearch.SortExpression);
                            if (userSearch.SortDirection == UIConstants.AscAbbreviation)
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                            else
                                SetViewState(ViewStateConstants.SortDirection, SortDirection.Descending);
                            return userListGridView.Columns.IndexOf(field);
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// Get the user list
        /// </summary>
        /// <param name="userSearch"></param>
        private void GetUserList(UserSearch userSearch)
        {
            userSearch.SortExpression = null;
            userSearch.SortDirection = null;
            SetViewState(ViewStateConstants.SortDirection, null);
            SetViewState(ViewStateConstants.SortExpression, null);
            SearchUsers(userSearch);

            //Pagination Control Setting
            pagingControl.CurrentPageIndex = 0;
            pagingControl.PageSet = 0;
            pagingControl.AllPageClick = false;
        }

        /// <summary>
        /// This method will maintain page state when user redirect from user  details screen
        /// </summary>
        private void MaintainPageState()
        {
            UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);

            if (userSearch != null)
            {
                PolulateSearchCriteria(userSearch);
                pagingControl.PageSet = userSearch.PageSet;
                SearchUsers(userSearch);
            }
        }

        /// <summary>
        /// This method will maintain search criteria
        /// </summary>
        private void PolulateSearchCriteria(UserSearch userSearch)
        {
            if (!string.IsNullOrEmpty(userSearch.TypeId))
                userTypeDropDown.SelectedValue = userSearch.TypeId;

            if (!string.IsNullOrEmpty(userSearch.StatusId))
                userStatusDropDown.SelectedValue = userSearch.StatusId;

            if (!string.IsNullOrEmpty(userSearch.QuickSearchExpression))
                searchTextBox.Text = userSearch.QuickSearchExpression;

            if (userSearch.LastLoginTo.HasValue)
            {
                lastLoginToTextBox.Text = ((DateTime)userSearch.LastLoginTo).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (userSearch.LastLoginFrom.HasValue)
            {
                lastLoginFromTextBox.Text = ((DateTime)userSearch.LastLoginFrom).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (userSearch.ActivateDateTo.HasValue)
            {
                activationDateToTextBox.Text = ((DateTime)userSearch.ActivateDateTo).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (userSearch.ActivationDateFrom.HasValue)
            {
                activationDateFromTextBox.Text = ((DateTime)userSearch.ActivationDateFrom).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// This method will populate the drop down data
        /// </summary>
        private void PopulateDropDownData()
        {
            BindStaticDropDown(DropDownConstants.UserTypesDropDown, userTypeDropDown, ResourceUtility.GetLocalizedString(1231, culture, "All Types"), null);
            BindStaticDropDown(DropDownConstants.UserStatusDropDown, userStatusDropDown, ResourceUtility.GetLocalizedString(1224, culture, "All Status"), null);
        }

        /// <summary>
        /// Search the user
        /// </summary>
        /// <param name="userSearch"></param>
        private void SearchUsers(UserSearch userSearch)
        {
            int totalRecordCount = 0;
            if (userSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                userSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                userSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (userSearch.PageIndex == -1)
                {
                    userSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = userSearch.PageIndex;
            }

            var userList = _userManager.GetUserList(userSearch, ref totalRecordCount);
            if (userList.Count == 0)
            {
                ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoUsersFoundForCriteria, culture));
            }

            userSearch.TotalRecordCount = totalRecordCount;

            //Save criteria in session for back button functionality
            ClearSession(SessionConstants.UserSearch);
            SetSession<UserSearch>(SessionConstants.UserSearch, userSearch);

            BindUserList(userList);

            //Pagination Control Setting
            pagingControl.TotalRecordCount = totalRecordCount;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// This method will sort the result grid
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortGridView(string sortExpression, string sortDirection)
        {
            UserSearch userSearch = GetSession<UserSearch>(SessionConstants.UserSearch);
            userSearch.SortExpression = sortExpression;
            userSearch.SortDirection = sortDirection;
            userSearch.PageIndex = 0;
            SearchUsers(userSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }
    }
}