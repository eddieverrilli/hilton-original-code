﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditPartnerProduct.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Admin.AddEditPartnerProduct" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../Styles/iframe-modal.css" />
    <!--[if IE 8]>
    <link type='text/css' href='../Styles/adminie8.css' rel='stylesheet' />
    <![endif]-->
     <link href="../Styles/iewidthfix.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.ie-select-width.js" ></script>
    <script type="text/javascript" src="../Scripts/ie-select-width.js"></script>
    <title></title>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="addCategoryScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>    
    <script type="text/javascript">
        $(window).ready(function () {
            $('.cancel').click(function () {
                window.parent.closeIframe();
                return false;

            });
        })

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }

    </script>
    <div id="addPartnerProductModal" class="modalbox">
        <h3>
            <asp:Label ID="pageHeader" runat="server"></asp:Label></h3>
        <div class="modal-content">
            <div class="modal-innercontent">
                <div class="form-field">
                    <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="resultMessageDiv" runat="server">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div id="parentCategoryContainer" runat="server">
                    </div>
                    <asp:UpdatePanel ID="PartnerListUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="partnerLabel" runat="server" Text="Partner" CBID="483"></asp:Label>
                                    *</div>
                                <div class="input no-pad">
                                    <asp:DropDownList ID="partnerDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="partnerDropDownList_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <br />
                                    <asp:RequiredFieldValidator ID="partnerRequiredFieldValidator" Enabled="false" VMTI="0"
                                        SetFocusOnError="true" runat="server" CssClass="errorText paderror" InitialValue="-1"
                                        ValidationGroup="partnerSelectionValidation" ControlToValidate="partnerDropDownList"
                                        Display="Dynamic" ErrorMessage="Partner is required"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <asp:CustomValidator ID="partnerValidator" ValidationGroup="partnerSelectionValidation"
                                runat="server" OnServerValidate="PartnerValidator_ServerValidation"></asp:CustomValidator>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="productLabel" runat="server" Text="Product" CBID="523"></asp:Label></div>
                                <div class="input no-pad">
                                    <asp:DropDownList ID="productDropDownList" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label">
                                    <asp:Label ID="statusLabel" runat="server" Text="Status" CBID="693"></asp:Label></div>
                                <div class="input no-pad">
                                    <asp:DropDownList ID="statusDropDownList" runat="server">
                                        <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="section alR pad-top">
                        <span class="btn-style btn-stylepad">
                            <asp:LinkButton ID="saveProductLinkButton" runat="server" ValidationGroup="partnerSelectionValidation"
                                OnClick="SaveProductLinkButton_Click" Text="Save" CBID="639"></asp:LinkButton>
                        </span><span class="btn-style ">
                            <asp:LinkButton ID="cancelProductLinkButton" CausesValidation="false" runat="server"
                                class="cancel simplemodal-overlay" Text="Cancel" CBID="276"></asp:LinkButton>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
