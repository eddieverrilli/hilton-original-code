﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI.Admin
{
    public partial class AddCategoryPopup : PageBase
    {
        private IProjectTemplateManager _projectTemplateManager;
        private int currentCategoryId;
        private string mode;
        private Entities.ProjectTemplate projectTemplate;
        private int projectTemplateId;

        // pending for HBS integration
        private IList<CategoryStandard> StandardList
        {
            get
            {
                if (this.ViewState["Standards"] == null)
                {
                    this.ViewState["Standards"] = new List<CategoryStandard>();
                }
                return (IList<CategoryStandard>)this.ViewState["Standards"];
            }
            set
            {
                this.ViewState["Standards"] = value;
            }
        }

        // pending for HBS integration
        /// <summary>
        ///add standard button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void AddStandardLinkButton_Command(object sender, EventArgs args)
        {
            string message = string.Empty;
            string messageType = string.Empty;

            if (string.IsNullOrWhiteSpace(standardTextBox.Text))
            {
                message = ConfigurationStore.GetApplicationMessages(MessageConstants.SpecifyStandard);
                messageType = "message error";
                ConfigureResultMessage(resultMessageDiv, messageType, message);
            }
            else
            {
                string[] standardDetails = standardTextBox.Text.Trim().Split(' ');
                if (standardDetails.Length == 1)
                {
                    message = ConfigurationStore.GetApplicationMessages(MessageConstants.SpecifyStandardFormat);
                    messageType = "message error";
                    ConfigureResultMessage(resultMessageDiv, messageType, message);
                }
                else
                {
                    CategoryStandard standardToBeMapped = CreateStandardEntity(standardTextBox.Text.Trim());
                    int noOfStandards = StandardList.Count(p => p.StandardNumber == standardToBeMapped.StandardNumber);
                    var standard = noOfStandards == 1 ? StandardList.Single(p => p.StandardNumber == standardToBeMapped.StandardNumber)
                        : StandardList.SingleOrDefault(p => p.StandardNumber == standardToBeMapped.StandardNumber && p.IsToBeMapped == true);
                    if (standard != null)
                    {
                        //When admin remaps category to already temporarily mapped standard
                        if (standard.IsToBeMapped == true)
                        {
                            message = ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateStandardMapping);
                            messageType = "message error";
                        }
                        else if (standard.IsToBeRemoved == true)
                        {
                            //when admin maps user to same role as of temporarily unmapped project
                            message = ConfigurationStore.GetApplicationMessages(MessageConstants.StandardTempMapping);
                            messageType = "message notification";
                            standard.IsToBeRemoved = false;
                        }
                        //When admin maps the user to already mapped project
                        else
                        {
                            message = ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateStandardMapping);
                            messageType = "message error";
                        }
                        ConfigureResultMessage(resultMessageDiv, messageType, message);
                    } //when admin maps the category to new standard
                    else
                    {
                        StandardList.Add(standardToBeMapped);
                        standardTextBox.Text = string.Empty;
                        //Result div to show the message
                        ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.StandardTempMapping));
                    }

                    associatedStandardsGridView.DataSource = StandardList.Where(p => p.IsToBeRemoved == false);
                    associatedStandardsGridView.DataBind();
                    associatedStandardsGridView.HeaderRow.Visible = false;
                    standardMappingUpdatePanel.Update();
                }
            }
            resultUpdatePanel.Update();
        }

        /// <summary>
        ///bind the category list to category drop down
        /// </summary>
        /// <param name="categoryDropDown"></param>
        /// <param name="categoryList"></param>
        public void BindData(DropDownList categoryDropDown, IList<Category> categoryList)
        {
            try
            {
                categoryDropDown.DataSource = categoryList;
                categoryDropDown.DataTextField = "CategoryName";
                categoryDropDown.DataValueField = "CategoryId";
                categoryDropDown.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///event for save button click . It saves a new category in the db.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void SaveCategoryLinkButton_Command(object sender, EventArgs args)
        {
            try
            {
                bool isSuccess = false;
                if (this.IsValid)
                {
                    Category categoryToSave = CreateNewCategory();
                    if (mode.Equals(UIConstants.Add.ToLower(CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                    {
                        isSuccess = _projectTemplateManager.AddCategory(categoryToSave, projectTemplateId);
                        if (isSuccess)
                        {
                            SetSession<string>(SessionConstants.TemplateOperationResult, ConfigurationStore.GetApplicationMessages(MessageConstants.CategorySuccessfullyAdded, culture));
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                        }
                        else
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                        }
                    }
                    else
                    {
                        isSuccess = _projectTemplateManager.UpdateCategory(categoryToSave, projectTemplateId);
                        if (isSuccess)
                        {
                            SetSession<string>(SessionConstants.TemplateOperationResult, ConfigurationStore.GetApplicationMessages(MessageConstants.CategorySuccessfullyUpdated, culture));
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "refresh", "RefreshParent();", true);
                        }
                        else
                        {
                            ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                        }
                    }
                }
                // pending for HBS integration
                else
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.MandatoryStandardMapping));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///cancel button click . to cancel the adding of a category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelCategoryLinkButton_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "close", "window.close();", true);
        }

        // pending for HBS integration
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void CancelImage_Command(object sender, CommandEventArgs args)
        {
            string standardNumber = ((ImageButton)(sender)).CommandArgument.ToString(CultureInfo.InvariantCulture);
            var standard = StandardList.SingleOrDefault(p => p.StandardNumber == standardNumber && p.IsToBeRemoved == false);
            if (standard.IsToBeMapped == true)
            {
                ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.StandardMappingTempRemoval));
                standard.IsToBeMapped = false;
                StandardList.Remove(standard);
            }
            else
            {
                //Remove the visibility of selected project
                standard.IsToBeRemoved = true;
                //Result div to show the message
                ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.StandardMappingPermanentRemoval));
            }
            //Bind the selected projects
            associatedStandardsGridView.DataSource = StandardList.Where(p => p.IsToBeRemoved == false);
            associatedStandardsGridView.DataBind();
            if (associatedStandardsGridView.HeaderRow != null)
            {
                associatedStandardsGridView.HeaderRow.Visible = false;
            }
            //To show the result division
            resultUpdatePanel.Update();
            standardMappingUpdatePanel.Update();
        }

        /// <summary>
        /// Get the instance of the manager class
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _projectTemplateManager = ProjectTemplateManager.Instance;
        }

        /// <summary>
        /// drop down selection changed event. creates another level of category drop down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = (DropDownList)sender;
                string id = ddl.ID;
                int existingDropDownCount = FindOccurence("ddlDynamic");
                string[] idSequence = id.Split('-');

                string currentSelectedCategoryId = ddl.SelectedValue.ToString(CultureInfo.InvariantCulture);
                // remove all the below ddls.
                for (int dropdownCount = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1; dropdownCount <= existingDropDownCount; dropdownCount++)
                {
                    HtmlGenericControl rowDivision = (HtmlGenericControl)parentCategoryContainer.FindControl("rowDivisionddlDynamic-" + dropdownCount.ToString(CultureInfo.InvariantCulture));
                    parentCategoryContainer.Controls.Remove(rowDivision);
                }
                if (currentSelectedCategoryId != "-1")
                {
                    Category currentSelectedCategory = ReadCategory(currentSelectedCategoryId);
                    if (!(currentSelectedCategory.IsLeaf))
                    {
                        IList<Category> childCategoryList = GetChildCategories(currentSelectedCategoryId);
                        if (childCategoryList.Count() > 0)
                        {
                            int nextID = Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) + 1;
                            CreateDropDownList("ddlDynamic-" + nextID.ToString(CultureInfo.InvariantCulture));
                            FindPopulateDropDowns(currentSelectedCategoryId, "ddlDynamic-" + nextID.ToString(CultureInfo.InvariantCulture), currentSelectedCategory);
                            PopulatePositionDropDown(Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture), ReadCategory(currentSelectedCategoryId.ToString(CultureInfo.InvariantCulture)));
                        }
                        else
                        {
                            PopulatePositionDropDown(Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture), ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture)));
                        }
                    }
                    else
                    {
                        PopulatePositionDropDown(Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture), ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture)));
                    }
                    SetStatusDropDownAdd(Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture));
                }
                else
                {
                    if (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) > 1)
                    {
                        string parentDropDownId = "ddlDynamic-" + (Convert.ToInt32(idSequence[1], CultureInfo.InvariantCulture) - 1).ToString(CultureInfo.InvariantCulture);
                        DropDownList parentDropDown = (DropDownList)parentCategoryContainer.FindControl(parentDropDownId);
                        if (parentDropDown != null)
                        {
                            PopulatePositionDropDown(Convert.ToInt32(parentDropDown.SelectedValue, CultureInfo.InvariantCulture), ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture)));
                        }
                        SetStatusDropDownAdd(Convert.ToInt32(parentDropDown.SelectedValue, CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        PopulatePositionDropDown(-1, ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture)));
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///event for page load. Shows the initial page configuration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    currentCategoryId = Convert.ToInt32(Request.QueryString["cat"], CultureInfo.InvariantCulture);
                    projectTemplateId = Convert.ToInt32(Request.QueryString["pt"], CultureInfo.InvariantCulture);
                    mode = Request.QueryString["mode"].ToString(CultureInfo.InvariantCulture);
                    if (!IsPostBack)
                    {
                        PopulateProjectTemplateDetails(projectTemplateId);
                        if (mode.Equals(UIConstants.Add.ToLowerInvariant(), StringComparison.InvariantCulture))
                        {
                            SetLabelText(pageHeader, Utilities.ResourceUtility.GetLocalizedString(179, culture));
                            CreateInitialDropDownsAdd();
                            if (currentCategoryId != -1)
                            {
                                SetStatusDropDownAdd(currentCategoryId);
                            }
                        }
                        else
                        {
                            SetLabelText(pageHeader, Utilities.ResourceUtility.GetLocalizedString(180, culture));
                            CreateInitialDropDownsEdit();
                            ConfigurePageForCategoryEdit(currentCategoryId);
                            if (!((ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture))).IsLeaf))
                            {
                                leafConfigDivision.Visible = false;
                            }
                            else
                            {
                                leafConfigDivision.Visible = true;
                            }
                        }
                    }
                }
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// while adding a new category, populates and sets default value.
        /// </summary>
        /// <param name="currentCategoryId"></param>
        private void SetStatusDropDownAdd(int currentCategoryId)
        {
            Category currentCategory = ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture));
            if (!currentCategory.IsActive)
            {
                isActiveDropDownList.SelectedValue = "0";
                isActiveDropDownList.Enabled = false;
            }
            else
            {
                isActiveDropDownList.SelectedValue = "1";
                isActiveDropDownList.Enabled = true;
            }
        }

        /// <summary>
        /// to recreate the drop downs for categories to n level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreInit(object sender, EventArgs e)
        {
            RecreateControls("ddlDynamic", "DropDownList");
        }

        /// <summary>
        ///populates the drop down to display the sequential position of a category being added
        /// </summary>
        /// <param name="parentCategoryId"></param>
        /// <param name="selectedCategory"></param>
        protected void PopulatePositionDropDown(int parentCategoryId, Category selectedCategory)
        {
            try
            {
                positionDropDownList.Items.Clear();
                positionDropDownList.Enabled = true;
                IList<Category> categoryCollection = new List<Category>();
                if (parentCategoryId == -1)
                {
                    ProjectTemplate currentProjectTemplate = (ProjectTemplate)ViewState["projectTemplate"];
                    categoryCollection = currentProjectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1).OrderBy(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture)).ToList();
                }
                else
                {
                    categoryCollection = GetSiblingsForPosition(parentCategoryId.ToString(CultureInfo.InvariantCulture));
                }

                if (mode.Equals(UIConstants.Edit.ToLower(CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                {
                    if (categoryCollection.Contains(selectedCategory))
                        categoryCollection.Remove(selectedCategory);

                    // positionDropDownList.SelectedValue = SetPositionSelectedValue(categoryCollection, selectedCategory);
                }
                IList<Category> childCategoryCollection = new List<Category>();
                foreach (Category category in categoryCollection)
                {
                    childCategoryCollection.Add(new Category()
                    {
                        SequenceId = category.CategoryId.ToString(CultureInfo.InvariantCulture) + "-0",
                        CategoryName = "Before " + category.CategoryName
                    });

                    childCategoryCollection.Add(new Category()
                    {
                        SequenceId = category.CategoryId.ToString(CultureInfo.InvariantCulture) + "-1",
                        CategoryName = "After " + category.CategoryName
                    });
                }
                positionDropDownList.Items.Clear();
                if (categoryCollection.Count == 0)
                {
                    positionDropDownList.Items.Insert(0, new ListItem(DropDownConstants.None, "-1"));
                    positionDropDownList.Enabled = false;
                }
                else
                {
                    positionDropDownList.DataSource = childCategoryCollection;
                    positionDropDownList.DataTextField = "CategoryName";
                    positionDropDownList.DataValueField = "SequenceId";
                    positionDropDownList.DataBind();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        ///// <summary>
        ///// to display the selected value for position drop down in case of editing a category
        ///// </summary>
        ///// <param name="categoryCollection"></param>
        ///// <param name="selectedCategory"></param>
        ///// <returns></returns>
        private void SetPositionSelectedValue(int parentCategoryId, Category selectedCategory)
        {
            try
            {
                IList<Category> categoryCollection = GetSiblingsForPosition(parentCategoryId.ToString(CultureInfo.InvariantCulture));
                string selectedValue = String.Empty;

                if (categoryCollection.Count > 0)
                {
                    int sequence = Convert.ToInt32(selectedCategory.SequenceId, CultureInfo.InvariantCulture);
                    Category selectedPositionCategory;
                    int minSequence = Convert.ToInt32(categoryCollection.Min(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture)));
                    int maxSequence = Convert.ToInt32(categoryCollection.Max(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture)));

                    if (sequence <= minSequence)
                    {
                        if (sequence == minSequence)
                        {
                            selectedPositionCategory = categoryCollection.SingleOrDefault(p => p.SequenceId == (minSequence + 1).ToString(CultureInfo.InvariantCulture));
                            if (selectedPositionCategory != null)
                                selectedValue = selectedPositionCategory.CategoryId.ToString(CultureInfo.InvariantCulture) + "-0";
                        }
                        else
                        {
                            selectedPositionCategory = categoryCollection.SingleOrDefault(p => p.SequenceId == minSequence.ToString(CultureInfo.InvariantCulture));
                            if (selectedPositionCategory != null)
                                selectedValue = selectedPositionCategory.CategoryId.ToString(CultureInfo.InvariantCulture) + "-0";
                        }
                    }
                    else if (sequence >= maxSequence)
                    {
                        if (sequence == maxSequence)
                        {
                            selectedPositionCategory = categoryCollection.SingleOrDefault(p => p.SequenceId == (maxSequence - 1).ToString(CultureInfo.InvariantCulture));
                            if (selectedPositionCategory != null)
                                selectedValue = selectedPositionCategory.CategoryId.ToString(CultureInfo.InvariantCulture) + "-1";
                        }
                        else
                        {
                            selectedPositionCategory = categoryCollection.SingleOrDefault(p => p.SequenceId == maxSequence.ToString(CultureInfo.InvariantCulture));
                            if (selectedPositionCategory != null)
                                selectedValue = selectedPositionCategory.CategoryId.ToString(CultureInfo.InvariantCulture) + "-1";
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(selectedCategory.SequenceId, CultureInfo.InvariantCulture) - 1 == 0 && categoryCollection.Count <= 1)
                        {
                            positionDropDownList.Items.Clear();
                            positionDropDownList.Items.Insert(0, new ListItem(DropDownConstants.None, "-1"));
                            positionDropDownList.Enabled = false;
                        }
                        else
                        {
                            selectedPositionCategory = categoryCollection.SingleOrDefault(p => p.SequenceId == (Convert.ToInt32(selectedCategory.SequenceId, CultureInfo.InvariantCulture) + 1).ToString(CultureInfo.InvariantCulture));
                            if (selectedPositionCategory != null)
                                selectedValue = selectedPositionCategory.CategoryId.ToString(CultureInfo.InvariantCulture) + "-0";
                        }
                    }
                }
                else if (categoryCollection.Count == 0)
                {
                    positionDropDownList.Items.Clear();
                    positionDropDownList.Items.Insert(0, new ListItem(DropDownConstants.None, "-1"));
                    positionDropDownList.Enabled = false;
                }

                if (selectedValue != string.Empty)
                    positionDropDownList.SelectedValue = selectedValue;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        /// // pending for HBS integration
        protected void StandardMappingValidator_ServerValidation(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    if (associatedStandardsGridView.Rows.Count == 0)
                        args.IsValid = false;
                    else
                        args.IsValid = true;
                }
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }

        /// <summary>
        ///populate the values when the page loads for editing a category.
        /// </summary>
        /// <param name="selectedCategoryId"></param>
        private void ConfigurePageForCategoryEdit(int selectedCategoryId)
        {
            try
            {
                Category selectedCategory = ReadCategory(selectedCategoryId.ToString(CultureInfo.InvariantCulture));
                categoryNameTextBox.Text = selectedCategory.CategoryName;
                styleDescriptionTextBox.Text = selectedCategory.StyleDescription;
                descriptionTextBox.Text = selectedCategory.Description;
                contactInfoTextBox.Text = selectedCategory.ContactInfo;
                standardTextBox.Text = selectedCategory.StandardNumbers;
                isActiveDropDownList.SelectedValue = selectedCategory.IsActive == true ? "1" : "0";
                isRequiredDropDownList.SelectedValue = selectedCategory.IsRequired == true ? "1" : "0";
                isNewPartnerRequiredDropDownList.SelectedValue = selectedCategory.IsNewPartnersRequired == true ? "1" : "0";
                isNewProductRequiredDropDownList.SelectedValue = selectedCategory.IsNewProductsReq == true ? "1" : "0";
                if (selectedCategory.ParentCategoryId != -1)
                {
                    isActiveDropDownList.Enabled = ReadCategory(selectedCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture)).IsActive ? true : false;
                }
               
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }

         
        }

        /// <summary>
        ///creates drop down lists dynamically
        /// </summary>
        /// <param name="Id"></param>
        private void CreateDropDownList(string Id)
        {
            try
            {
                HtmlGenericControl rowDivision = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                HtmlGenericControl labelContainerDivision = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                HtmlGenericControl dropDownContainerDivision = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");

                rowDivision.Attributes.Add("class", "row");
                rowDivision.ID = "rowDivision" + Id;
                Label categoryLabel = new Label();
                categoryLabel.CssClass = "label";
                if ((Id.Split('-'))[1].Equals("1", StringComparison.InvariantCulture))
                {
                    categoryLabel.Text = ResourceUtility.GetLocalizedString(482, culture);
                }
                else
                    categoryLabel.Text = ResourceUtility.GetLocalizedString(844, culture, "Sub Category");
             
                labelContainerDivision.Controls.Add(categoryLabel);
                dropDownContainerDivision.Attributes.Add("class", "input no-pad");

                DropDownList parentCategoryDropDown = new DropDownList();
                parentCategoryDropDown.ID = Id;
                parentCategoryDropDown.AutoPostBack = true;
                parentCategoryDropDown.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);

                dropDownContainerDivision.Controls.Add(parentCategoryDropDown);
             
                rowDivision.Controls.Add(labelContainerDivision);
                rowDivision.Controls.Add(dropDownContainerDivision);
                parentCategoryContainer.Controls.Add(rowDivision);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///creates drop down list for the first time when page is loaded for adding a new category
        /// </summary>
        private void CreateInitialDropDownsAdd()
        {
            try
            {
                int currentCatLevel = 1;
                if (currentCategoryId != -1)
                {
                    Category selectedCategory = ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture));
                    for (int catLevel = 1; catLevel <= Convert.ToInt32(selectedCategory.Level); catLevel++)
                    {
                        CreateDropDownList("ddlDynamic-" + Convert.ToString(catLevel, CultureInfo.InvariantCulture));
                        currentCatLevel = catLevel;
                    }
                    PopulateInitialValues(selectedCategory);
                    PopulatePositionDropDown(currentCategoryId, ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture)));

                    if (!(selectedCategory.IsLeaf))
                    {
                        IList<Category> childCategoryList = GetChildCategories(currentCategoryId.ToString(CultureInfo.InvariantCulture));
                        if (childCategoryList.Count() > 0)
                        {
                            int nextID = currentCatLevel + 1;
                            CreateDropDownList("ddlDynamic-" + nextID.ToString(CultureInfo.InvariantCulture));
                            FindPopulateDropDowns(currentCategoryId.ToString(CultureInfo.InvariantCulture), "ddlDynamic-" + nextID.ToString(CultureInfo.InvariantCulture), selectedCategory);
                            PopulatePositionDropDown(Convert.ToInt32(currentCategoryId, CultureInfo.InvariantCulture), ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture)));
                        }
                        else
                        {
                            PopulatePositionDropDown(Convert.ToInt32(currentCategoryId, CultureInfo.InvariantCulture), ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture)));
                        }
                    }
                    else
                    {
                        PopulatePositionDropDown(Convert.ToInt32(currentCategoryId.ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture), ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture)));
                    }
                }
                else
                {
                    int cnt = FindOccurence("ddlDynamic");
                    CreateDropDownList("ddlDynamic-" + Convert.ToString(cnt + 1, CultureInfo.InvariantCulture));
                    FindPopulateDropDowns(String.Empty, "ddlDynamic-" + Convert.ToString(cnt + 1, CultureInfo.InvariantCulture), null);
                    PopulatePositionDropDown(-1, null);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///creates drop down list for the first time when page is loaded for editing a category
        /// </summary>
        private void CreateInitialDropDownsEdit()
        {
            try
            {
                int currentCatLevel = 1;
                Category selectedCategory = ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture));
                int selectedParentCategoryId = selectedCategory.ParentCategoryId;

                if (selectedParentCategoryId != -1)
                {
                    Category selectedParentCategory = ReadCategory(selectedParentCategoryId.ToString(CultureInfo.InvariantCulture));
                    for (int catLevel = 1; catLevel <= Convert.ToInt32(selectedParentCategory.Level); catLevel++)
                    {
                        CreateDropDownList("ddlDynamic-" + Convert.ToString(catLevel, CultureInfo.InvariantCulture));
                        currentCatLevel = catLevel;
                    }
                    PopulateInitialValues(selectedParentCategory);
                    PopulatePositionDropDown(selectedParentCategoryId, selectedCategory);

                    if (!(selectedCategory.IsLeaf))
                    {
                        IList<Category> childCategoryList = GetChildCategories(currentCategoryId.ToString(CultureInfo.InvariantCulture));
                        if (childCategoryList.Count() > 0)
                        {
                            int nextID = currentCatLevel + 1;
                            CreateDropDownList("ddlDynamic-" + nextID.ToString(CultureInfo.InvariantCulture));
                            FindPopulateDropDowns(selectedParentCategoryId.ToString(CultureInfo.InvariantCulture), "ddlDynamic-" + nextID.ToString(CultureInfo.InvariantCulture), selectedCategory);
                            PopulatePositionDropDown(selectedParentCategoryId, selectedCategory);
                            SetPositionSelectedValue(selectedParentCategoryId, selectedCategory);
                        }
                        else
                        {
                            PopulatePositionDropDown(selectedParentCategoryId, selectedCategory);
                            SetPositionSelectedValue(selectedParentCategoryId, selectedCategory);
                        }
                    }
                    else
                    {
                        PopulatePositionDropDown(selectedParentCategoryId, selectedCategory);
                        SetPositionSelectedValue(selectedParentCategoryId, selectedCategory);
                    }
                }
                else
                {
                    int cnt = FindOccurence("ddlDynamic");
                    CreateDropDownList("ddlDynamic-" + Convert.ToString(cnt + 1, CultureInfo.InvariantCulture));
                    FindPopulateDropDowns(string.Empty, "ddlDynamic-" + Convert.ToString(cnt + 1, CultureInfo.InvariantCulture), selectedCategory);
                    PopulatePositionDropDown(-1, selectedCategory);
                    SetPositionSelectedValue(-1, selectedCategory);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// create a category entity from the values displayed in the UI.
        /// </summary>
        /// <returns></returns>
        private Category CreateNewCategory()//tbs standards mapping
        {
            Category newCategory = new Category();
            try
            {
                if (mode.Equals(UIConstants.Edit.ToLowerInvariant(), StringComparison.InvariantCulture))
                {
                    Category existingCategory = ReadCategory(currentCategoryId.ToString(CultureInfo.InvariantCulture));
                    newCategory.CategoryId = Convert.ToInt32(currentCategoryId);
                    newCategory.IsLeaf = existingCategory.IsLeaf;
                    if (!(newCategory.IsLeaf))
                    {
                        newCategory.IsRequired = null;
                        newCategory.IsNewProductsReq = null;
                        newCategory.IsNewPartnersRequired = null;
                    }
                    else
                    {
                        newCategory.IsRequired = (isRequiredDropDownList.SelectedValue == "1") ? true : false;
                        newCategory.IsNewProductsReq = (isNewProductRequiredDropDownList.SelectedValue == "1") ? true : false;
                        newCategory.IsNewPartnersRequired = (isNewPartnerRequiredDropDownList.SelectedValue == "1") ? true : false;
                    }
                }
                else
                {
                    newCategory.IsLeaf = true;
                    newCategory.IsRequired = (isRequiredDropDownList.SelectedValue == "1") ? true : false;
                    newCategory.IsNewProductsReq = (isNewProductRequiredDropDownList.SelectedValue == "1") ? true : false;
                    newCategory.IsNewPartnersRequired = (isNewPartnerRequiredDropDownList.SelectedValue == "1") ? true : false;
                }
                int currentCategoryLevel = FindOccurence("ddlDynamic");
                Category parentCategory;
                DropDownList parentDropDown = (DropDownList)parentCategoryContainer.FindControl("ddlDynamic-" + currentCategoryLevel.ToString(CultureInfo.InvariantCulture));
                DropDownList rootDropDown = (DropDownList)parentCategoryContainer.FindControl("ddlDynamic-" + 1.ToString(CultureInfo.InvariantCulture));

                if (!string.IsNullOrEmpty(categoryNameTextBox.Text.Trim()))
                    newCategory.CategoryName = categoryNameTextBox.Text.Trim();
                if (rootDropDown.SelectedValue == "-1")
                {
                    newCategory.ParentCategoryId = -1;
                    newCategory.Level = 1;
                }
                else
                {
                    if (parentDropDown.SelectedValue == "-1")
                    {
                        DropDownList grandParentDropDown = (DropDownList)parentCategoryContainer.FindControl("ddlDynamic-" + (currentCategoryLevel - 1).ToString(CultureInfo.InvariantCulture));
                        parentCategory = ReadCategory(grandParentDropDown.SelectedValue.ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        parentCategory = ReadCategory(parentDropDown.SelectedValue.ToString(CultureInfo.InvariantCulture));
                    }
                    newCategory.ParentCategoryId = parentCategory.CategoryId;

                    newCategory.Level = parentCategory.Level + 1;
                }
                newCategory.RootParentId = Convert.ToInt32(rootDropDown.SelectedValue, CultureInfo.InvariantCulture);

                //set IsActive for new category
                if (newCategory.ParentCategoryId != -1)
                {
                    if ((ReadCategory(newCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))).IsActive)
                    {
                        newCategory.IsActive = (isActiveDropDownList.SelectedValue == "1") ? true : false;
                    }
                    else
                        newCategory.IsActive = false;
                }
                else
                {
                    newCategory.IsActive = (isActiveDropDownList.SelectedValue == "1") ? true : false;
                }
                newCategory.SequenceId = SetSequenceIdForCategory();
                newCategory.Description = descriptionTextBox.Text.Trim();
                newCategory.StyleDescription = styleDescriptionTextBox.Text.Trim();
                newCategory.ContactInfo = contactInfoTextBox.Text.Trim();
                newCategory.StandardNumbers = standardTextBox.Text.Trim();
                // pending for HBS integration
                newCategory.CategoryStandards = StandardList;
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                if (rethrow)
                {
                    throw exception;
                }
                resultUpdatePanel.Update();
            }
            return newCategory;
        }

        // pending for HBS integration
        /// <summary>
        ///
        /// </summary>
        /// <param name="standardDescription"></param>
        /// <returns></returns>
        ///
        private CategoryStandard CreateStandardEntity(string standardDescription)
        {
            CategoryStandard standardToBeMapped = new CategoryStandard();
            string[] standardDetails = standardTextBox.Text.Trim().Split(' ');

            standardToBeMapped.StandardNumber = standardDetails[0].ToString(CultureInfo.InvariantCulture);
            string standardText = standardDescription.Substring(standardToBeMapped.StandardNumber.Length + 1);
            standardToBeMapped.StandardDescription = standardText.Trim();
            standardToBeMapped.IsToBeMapped = true;
            standardToBeMapped.IsToBeRemoved = false;

            return standardToBeMapped;
        }

        /// <summary>
        ///finds the number of drop down occurances in the page
        /// </summary>
        /// <param name="substr"></param>
        /// <returns></returns>
        private int FindOccurence(string substr)
        {
            string reqstr = Request.Form.ToString();
            return ((reqstr.Length - reqstr.Replace(substr, "").Length) / substr.Length);
        }

        /// <summary>
        ///searches for new drop down
        /// </summary>
        /// <param name="selectedCategoryId"></param>
        /// <param name="id"></param>
        /// <param name="selectedCategory"></param>
        private void FindPopulateDropDowns(string selectedCategoryId, string id, Category selectedCategory)
        {
            DropDownList newDropDown = (DropDownList)FindControl(parentCategoryContainer, id);
            PopulateDropDown(newDropDown, selectedCategoryId, selectedCategory);
        }

        /// <summary>
        ///gets the sub categories for a category
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetChildCategories(string selectedValue)
        {
            ProjectTemplate currentProjectTemplate = (ProjectTemplate)ViewState["projectTemplate"];
           IList<Category> categoryList = (currentProjectTemplate.Categories.AsEnumerable().
                                            Where(p => (p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture)) && (p.HasProducts == false)).ToList());
            return categoryList;
        }

        /// <summary>
        ///gets the list of all root level categories
        /// </summary>
        /// <returns></returns>
        private IList<Category> GetRootLevelCategories()
        {
            ProjectTemplate currentProjectTemplate = (ProjectTemplate)ViewState["projectTemplate"];
            IList<Category> rootCategoryList = (currentProjectTemplate.Categories.AsEnumerable().Where(p => p.ParentCategoryId == -1 && (p.HasProducts == false))).ToList();
            return rootCategoryList;
        }

        /// <summary>
        ///gets the category list under the same parent at the same level
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private IList<Category> GetSiblingsForPosition(string selectedValue)
        {
            ProjectTemplate currentProjectTemplate = (ProjectTemplate)ViewState["projectTemplate"];
            IList<Category> categoryList = (currentProjectTemplate.Categories.AsEnumerable().
                                            Where(p => p.ParentCategoryId == Convert.ToInt32(selectedValue, CultureInfo.InvariantCulture)).OrderBy(p => Convert.ToInt32(p.SequenceId, CultureInfo.InvariantCulture)).ToList());
            return categoryList;
        }

        /// <summary>
        /// populates a dro down dynamically created with a list of categories according to a specific level
        /// </summary>
        /// <param name="parentCategoryDropDown"></param>
        /// <param name="selectedValue"></param>
        /// <param name="selectedCategory"></param>
        private void PopulateDropDown(DropDownList parentCategoryDropDown, string selectedValue, Category selectedCategory)
        {
            try
            {
                parentCategoryDropDown.Items.Clear();

                IList<Category> categoryList = new List<Category>();
                if (string.IsNullOrEmpty(selectedValue))
                {
                    categoryList = GetRootLevelCategories();
                }
                else if (selectedValue != "-1")
                {
                    categoryList = GetChildCategories(selectedValue);
                }
                if (mode.Equals(UIConstants.Edit.ToLower(CultureInfo.InvariantCulture), StringComparison.InvariantCultureIgnoreCase))
                {
                    if (categoryList.Contains(selectedCategory))
                        categoryList.Remove(selectedCategory);
                }
                if (categoryList != null)
                {
                    BindData(parentCategoryDropDown, categoryList);
                }
                parentCategoryDropDown.Items.Insert(0, new ListItem(DropDownConstants.None, "-1"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///populates the drop down with initial values while adding a new category
        /// </summary>
        /// <param name="selectedCategory"></param>
        private void PopulateInitialValues(Category selectedCategory)
        {
            try
            {
                ArrayList catHierarchyList = new ArrayList();
                int parentCatId;
                int selectedCategoryLevel = selectedCategory.Level;
                Category category = new Category();
                for (int initialDropDownCount = selectedCategoryLevel; initialDropDownCount >= 1; initialDropDownCount--)
                {
                    if (initialDropDownCount == selectedCategoryLevel)
                    {
                        category = selectedCategory;
                    }
                    else
                    {
                        parentCatId = category.ParentCategoryId;
                        category = ReadCategory(parentCatId.ToString(CultureInfo.InvariantCulture));
                    }

                    catHierarchyList.Add(category);
                }
                catHierarchyList.Reverse();
                Category currentCategory;
                for (int initialDropDownCount = selectedCategoryLevel; initialDropDownCount >= 1 && catHierarchyList.Count >= 0; initialDropDownCount--)
                {
                    DropDownList currentDropDown = (DropDownList)parentCategoryContainer.FindControl("ddlDynamic-" + Convert.ToString(initialDropDownCount, CultureInfo.InvariantCulture));
                    currentCategory = (Category)(catHierarchyList[initialDropDownCount - 1]);
                    IList<Category> categoryList = projectTemplate.Categories.AsEnumerable().Where(p => p.RootParentId == currentCategory.RootParentId && p.Level == currentCategory.Level).ToList();
                    BindData(currentDropDown, categoryList);
                    currentDropDown.SelectedValue = ((Category)(catHierarchyList[initialDropDownCount - 1])).CategoryId.ToString(CultureInfo.InvariantCulture);
                    catHierarchyList.RemoveAt(initialDropDownCount - 1);
                    currentDropDown.Items.Insert(0, new ListItem(DropDownConstants.None, "-1"));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///gets the list of all categories for a project template ID
        /// </summary>
        /// <param name="projectTemplateID"></param>
        private void PopulateProjectTemplateDetails(int projectTemplateID)
        {
            try
            {
                projectTemplate = _projectTemplateManager.GetProjectTemplateDetail(projectTemplateID);
                ViewState["projectTemplate"] = projectTemplate;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///reads the details for the selected category id
        /// </summary>
        /// <param name="currentSelectedCategoryId"></param>
        /// <returns></returns>
        private Category ReadCategory(string currentSelectedCategoryId)
        {
            ProjectTemplate currentProjectTemplate = (ProjectTemplate)ViewState["projectTemplate"];
            Category category = (Category)(currentProjectTemplate.Categories.SingleOrDefault(p => p.CategoryId == Convert.ToInt32(currentSelectedCategoryId, CultureInfo.InvariantCulture)));
            if (category != null)
                return category;
            else
                return null;
        }

        /// <summary>
        ///recreates teh drop downs
        /// </summary>
        /// <param name="ctrlPrefix"></param>
        /// <param name="ctrlType"></param>
        private void RecreateControls(string ctrlPrefix, string ctrlType)
        {
            try
            {
                string[] ctrls = Request.Form.ToString().Split('&');
                int cnt = FindOccurence(ctrlPrefix);
                if (cnt > 0)
                {
                    for (int k = 1; k <= cnt; k++)
                    {
                        for (int i = 0; i < ctrls.Length; i++)
                        {
                            if (ctrls[i].Contains(ctrlPrefix + "-" + k.ToString(CultureInfo.InvariantCulture)) && !ctrls[i].Contains("EVENTTARGET"))
                            {
                                string ctrlID = ctrls[i].Split('=')[0];

                                if (ctrlType == "DropDownList")
                                {
                                    CreateDropDownList(ctrlID);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                resultUpdatePanel.Update();
            }
        }

        /// <summary>
        ///sets the sequence for a category to be saved
        /// </summary>
        /// <returns></returns>
        private string SetSequenceIdForCategory()
        {
            string sequenceId = string.Empty;
            try
            {
                if (positionDropDownList.SelectedValue != "-1")
                {
                    Category siblingCategory = ReadCategory(positionDropDownList.SelectedValue.Split('-')[0].ToString(CultureInfo.InvariantCulture));
                    if (positionDropDownList.SelectedValue.Split('-')[1].ToString(CultureInfo.InvariantCulture) == "0")
                    {
                        sequenceId = siblingCategory.SequenceId.ToString(CultureInfo.InvariantCulture) + "-0";
                    }
                    else if (positionDropDownList.SelectedValue.Split('-')[1].ToString(CultureInfo.InvariantCulture) == "1")
                    {
                        sequenceId = siblingCategory.SequenceId.ToString(CultureInfo.InvariantCulture) + "-1";
                    }
                }
                else
                {
                    sequenceId = "1" + "-0";
                }
            }
            catch (Exception exception)
            {
                bool rethrow = DataAccessExceptionHandler.HandleException(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                if (rethrow)
                {
                    throw exception;
                }
                resultUpdatePanel.Update();
            }

            return sequenceId;
        }
    }
}