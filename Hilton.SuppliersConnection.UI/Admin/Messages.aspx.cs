﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class Messages : PageBase
    {
        private IMessageManager _messageManager;

        /// <summary>
        /// Invoked when pagination all button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void AllClick(object sender, System.EventArgs args)
        {
            try
            {
                MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                messageSearch.PageIndex = pagingControl.CurrentPageIndex;
                messageSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
                messageSearch.AllClick = pagingControl.AllPageClick;
                SearchMessages(messageSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination first button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void FirstClick(object sender, EventArgs args)
        {
            try
            {
                MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                messageSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchMessages(messageSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Pull up the messages from DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    MessageSearch messageSearch = PopulateSearchCriteria();
                    messageSearch.SearchFlag = SearchFlagEnum.Filter.ToString();
                    messageSearch.SortExpression = UIConstants.SenderName;
                    messageSearch.SortDirection = UIConstants.AscAbbreviation;
                    SetViewState(ViewStateConstants.CommandSourceId, UIConstants.MessageDefaultCommandSourceId);
                    SetViewState(ViewStateConstants.SortExpression, UIConstants.SenderName);
                    SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                    SearchMessages(messageSearch);

                    //Pagination Control Setting
                    pagingControl.CurrentPageIndex = 0;
                    pagingControl.PageSet = 0;
                    pagingControl.AllPageClick = false;
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LastClick(object sender, EventArgs args)
        {
            try
            {
                MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                messageSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchMessages(messageSearch);
                ClearSession(SessionConstants.ContentSearch);
                SetViewState<MessageSearch>(ViewStateConstants.MessageViewState, messageSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///  Invoked on repeater item data bound
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void MessageListRepeater_ItemCommand(object source, RepeaterCommandEventArgs args)
        {
            try
            {
                if (args != null && args.Item.ItemType == ListItemType.Header)
                {
                    string commandSourceId = ((LinkButton)args.CommandSource).ID;
                    string sortExpression = args.CommandName;
                    if (GetViewState<object>(ViewStateConstants.SortExpression) != null && GetViewState<string>(ViewStateConstants.SortExpression) != sortExpression)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }

                    SetViewState<string>(ViewStateConstants.SortExpression, sortExpression);
                    SetViewState<string>(ViewStateConstants.CommandSourceId, commandSourceId);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        SortRepeaterView(sortExpression, UIConstants.DescAbbreviation);
                    }
                    else
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        SortRepeaterView(sortExpression, UIConstants.AscAbbreviation);
                    }
                }
                else if (args != null && (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem))
                {
                    if (args.CommandName == "UpdateMessageStatus")
                    {
                        IList<Message> messageCollection = new List<Message>();

                        HiddenField messageStatusIdHiddenField = args.Item.FindControl("messageStatusIdHiddenField") as HiddenField;
                        DropDownList statusDropDownList = args.Item.FindControl("messageStatusDropDown") as DropDownList;
                        string oldMessageStatusId = messageStatusIdHiddenField.Value;
                        string newMessageStatusId = statusDropDownList.SelectedValue;
                        if (oldMessageStatusId != newMessageStatusId)
                        {
                            messageCollection.Add(new Message()
                            {
                                MessageId = Convert.ToInt32(args.CommandArgument.ToString(), CultureInfo.InvariantCulture),
                                MessageStatusId = Convert.ToInt16(newMessageStatusId, CultureInfo.InvariantCulture),
                            });
                            messageStatusIdHiddenField.Value = newMessageStatusId;
                        }

                        UpdateMessageStatus(messageCollection);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Repeater Item Created.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void MessageListRepeater_ItemCreated(object source, RepeaterItemEventArgs args)
        {
            try
            {
                if (args != null && args.Item.ItemType == ListItemType.Header)
                {
                    if (GetViewState<object>(ViewStateConstants.CommandSourceId) != null)
                    {
                        string commandSourceId = GetViewState<object>(ViewStateConstants.CommandSourceId).ToString();
                        LinkButton lbtn = args.Item.FindControl(commandSourceId) as LinkButton;
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {
                            lbtn.Attributes.Add("class", "sorta");
                        }
                        else
                        {
                            lbtn.Attributes.Add("class", "sortd");
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Repeater Item Data Bound
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void MessageListRepeater_ItemDataBound(object source, RepeaterItemEventArgs args)
        {
            try
            {
                if (args != null && (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem))
                {
                    DropDownList statusDropDownList = args.Item.FindControl("messageStatusDropDown") as DropDownList;
                    PopulateStatusDropDownData(statusDropDownList);
                    statusDropDownList.SelectedValue = ((Message)args.Item.DataItem).MessageStatusId.ToString(CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void NextClick(object sender, EventArgs args)
        {
            try
            {
                MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                messageSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchMessages(messageSearch);
                SetViewState<MessageSearch>(ViewStateConstants.MessageViewState, messageSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked On OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                ValidateUserAccess((int)MenuEnum.Messages);
                _messageManager = MessageManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(1094, culture, "Message List");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    //Clear result div
                    resultMessageDiv.Attributes.Remove("class");
                    resultMessageDiv.InnerText = string.Empty;

                    searchResultDiv.Attributes.Remove("class");
                    searchResultDiv.InnerText = string.Empty;
                    ScriptManager.RegisterPostBackControl(exportCSVLinkButton);
                    if (!Page.IsPostBack)
                    {
                        exportCSVLinkButton.Visible = false;
                        saveLinkButton.Visible = false;
                        PopulateDropDownData();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(messagesUpdatePanel, this.GetType(), "InitializeToggle", "InitializeToggle();", true);
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination paged view button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PagedViewClick(object sender, EventArgs args)
        {
            try
            {
                MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                messageSearch.PageIndex = pagingControl.CurrentPageIndex;
                messageSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                messageSearch.AllClick = pagingControl.AllPageClick;
                SearchMessages(messageSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination page number is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void PageNumberClick(object sender, EventArgs args)
        {
            try
            {
                var pageNumberLinkButton = (LinkButton)sender;
                MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                messageSearch.PageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                pagingControl.CurrentPageIndex = Convert.ToInt32(pageNumberLinkButton.Text, CultureInfo.InvariantCulture) - 1;
                SearchMessages(messageSearch);
                SetViewState<MessageSearch>(ViewStateConstants.MessageViewState, messageSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when pagination previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PreviousClick(object sender, EventArgs args)
        {
            try
            {
                MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                messageSearch.PageIndex = pagingControl.CurrentPageIndex;
                SearchMessages(messageSearch);
                SetViewState<MessageSearch>(ViewStateConstants.MessageViewState, messageSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked on Specific Search Criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void QuickSearch_Click(object sender, EventArgs e)
        {
            try
            {
                MessageSearch messageSearch = PopulateSearchCriteria();
                messageSearch.SearchFlag = SearchFlagEnum.QuickSearch.ToString();
                messageSearch.SortExpression = UIConstants.SenderName; ;
                messageSearch.SortDirection = UIConstants.AscAbbreviation;
                SetViewState(ViewStateConstants.CommandSourceId, UIConstants.MessageDefaultCommandSourceId);
                SetViewState(ViewStateConstants.SortExpression, UIConstants.SenderName);
                SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                SearchMessages(messageSearch);

                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
                pagingControl.AllPageClick = false;
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Save the Messages into DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void SaveLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                IList<Message> messageCollection = new List<Message>();

                foreach (RepeaterItem item in messageListRepeater.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        HiddenField messageIdHiddenField = item.FindControl("messageIdHiddenField") as HiddenField;
                        HiddenField messageStatusIdHiddenField = item.FindControl("messageStatusIdHiddenField") as HiddenField;
                        DropDownList statusDropDownList = item.FindControl("messageStatusDropDown") as DropDownList;
                        string oldMessageStatusId = messageStatusIdHiddenField.Value;
                        string newMessageStatusId = statusDropDownList.SelectedValue;
                        if (oldMessageStatusId != newMessageStatusId)
                        {
                            messageCollection.Add(new Message()
                            {
                                MessageId = Convert.ToInt32(messageIdHiddenField.Value, CultureInfo.InvariantCulture),
                                MessageStatusId = Convert.ToInt16(newMessageStatusId, CultureInfo.InvariantCulture),
                            });
                            messageStatusIdHiddenField.Value = newMessageStatusId;
                        }
                    }
                }

                UpdateMessageStatus(messageCollection);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To update the message Status
        /// </summary>
        /// <param name="messageCollection"></param>
        private void UpdateMessageStatus(IList<Message> messageCollection)
        {
            if (messageCollection.Count > 0)
            {
                int result = _messageManager.UpdateMessage(messageCollection);
                if (result == 0)
                {
                    ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralUpdateFailure, culture));
                }
                else
                {
                    ConfigureResultMessage(resultMessageDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.MessageSuccessfullyUpdated, culture));
                    MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                    SearchMessages(messageSearch);
                }
            }
            else
            {
                ConfigureResultMessage(resultMessageDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.MessageStatusNotModified, culture));
            }

            validateDateCustomValidator.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// triggers when export to excel is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ExportCSVLinkButton_Click(object sender, EventArgs args)
        {
            try
            {
                MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
                messageSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.ExportToExcelMaxPageSize), CultureInfo.InvariantCulture); // Max page size to get all records to excel at once
                User currentUser = GetSession<User>(SessionConstants.User);
                messageSearch.UserId = currentUser.UserId;
                ExportMessages(messageSearch);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        ///Exports a collection of messages
        /// </summary>
        /// <param name="messageSearch"></param>
        private void ExportMessages(MessageSearch messageSearch)
        {
            try
            {
                DataSet messageCollection = new DataSet();

                //call the business method
                messageCollection = _messageManager.ExportMessage(messageSearch);

                if (messageCollection.Tables.Count == 0)
                {
                    searchResultDiv.Attributes.Add("class", "message notification");
                    searchResultDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoContentBlockFoundForCriteria, culture);
                    exportCSVLinkButton.Visible = false;
                    messageListRepeater.Visible = false;
                }
                else
                {
                    exportCSVLinkButton.Visible = true;
                    messageListRepeater.Visible = true;
                }

                ExportToExcel(messageCollection);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        ///Exports a collection of messages
        /// </summary>
        /// <param name="messageCollection"></param>
        private void ExportToExcel(DataSet messageCollection)
        {
            try
            {
                string attach = UIConstants.MessageExcelFileName;
                Response.ClearContent();
                Response.AddHeader("content-disposition", attach);
                Response.ContentType = UIConstants.ContentType;
                if (messageCollection.Tables != null)
                {
                    WriteMessagesToExcel(messageCollection.Tables[0]);
                    Response.End();
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        ///writes records to excel
        /// </summary>
        /// <param name="messageCollectionTable"></param>
        private void WriteMessagesToExcel(DataTable messageCollectionTable)
        {
            try
            {
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(355, culture, "Date Sent") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(913, culture, "Sender Type") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(667, culture, "Sender") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(585, culture, "Recipient") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(919, culture, "Category") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(920, culture, "Status") + "\t");
                Response.Write(Utilities.ResourceUtility.GetLocalizedString(1095, culture, "Message") + "\t");
                Response.Write(System.Environment.NewLine);
                foreach (DataRow row in messageCollectionTable.Rows)
                {
                    string catName = string.Empty;
                    catName = row["CatName"].ToString().Replace("-", "");
                    Response.Write(row["MesssageDate"].ToString() + "\t");
                    Response.Write(row["SenderType"].ToString() + "\t");
                    Response.Write(row["From"].ToString() + "\t");
                    Response.Write(row["Sender"].ToString() + "\t");
                    Response.Write(catName.ToString(CultureInfo.InvariantCulture) + "\t");
                    Response.Write(row["MessageStatusDesc"].ToString() + "\t");
                    Response.Write(row["MessageDetails"].ToString() + "\t");
                    Response.Write("\n");
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                resultMessageDiv.Attributes.Add("class", "message error");
                resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture);
            }
        }

        /// <summary>
        /// To Validate Date
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidDate(object source, ServerValidateEventArgs args)
        {
            try
            {
                ValidateDate(args, validateDateCustomValidator, fromDateTextBox.Text, toDateTextBox.Text);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// To Bind the Messages
        /// </summary>
        /// <param name="messageList"></param>
        private void BindMessages(IList<Message> messageList)
        {
            //Set the data
            messageListRepeater.DataSource = messageList;
            messageListRepeater.DataBind();
        }

        /// <summary>
        /// To Populate Drop Down Data
        /// </summary>
        private void PopulateDropDownData()
        {
            User user = GetSession<User>(SessionConstants.User);
            MessageDropDown messageDropDowns = _messageManager.GetMessageDropDownsList(user.UserId);
            BindMessagesDropDown(senderDropDown, messageDropDowns.SenderDropDown, ResourceUtility.GetLocalizedString(1223, culture, "All Sender"));
            BindMessagesDropDown(recepientDropDown, messageDropDowns.RecipientDropDown, ResourceUtility.GetLocalizedString(1222, culture, "All Recipient"));
            BindMessagesDropDown(regionDropDown, messageDropDowns.RegionDropDown, ResourceUtility.GetLocalizedString(1202, culture, "All Regions"));
       }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dropDownType"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        /// <param name="selectedValue"></param>
        protected void BindMessagesDropDown(DropDownList sourceDropDown, IList<DropDownItem> dropDownData, string defaultText)
        {
            sourceDropDown.DataSource = dropDownData.OrderBy(p=>p.DataTextField);
            sourceDropDown.DataTextField = "DataTextField";
            sourceDropDown.DataValueField = "DataValueField";
            sourceDropDown.DataBind();
            sourceDropDown.Items.Insert(0, new ListItem(defaultText, DropDownConstants.DefaultValue));
        }

        /// <summary>
        /// To Bind the Region Drop Down
        /// </summary>
        /// <param name="regionDropDownList"></param>
        private void FilterAndBindAdminRegion(IList<DropDownItem> regionDropDownList)
        {
            User user = GetSession<User>(SessionConstants.User);
            if (user != null && user.UserRegions != null && user.UserRegions.Count > 0)
            {
                regionDropDownList = (from p in regionDropDownList join pType in user.UserRegions on p.DataValueField equals pType.RegionId.ToString(CultureInfo.InvariantCulture) select new DropDownItem { DataTextField = p.DataTextField, DataValueField = p.DataValueField }).ToList();
            }
            BindMessagesDropDown(regionDropDown, regionDropDownList, ResourceUtility.GetLocalizedString(1202, culture, "All Regions"));
       }

        /// <summary>
        /// To Populate Search Criteria
        /// </summary>
        /// <returns></returns>
        private MessageSearch PopulateSearchCriteria()
        {
            User currentUser = (User)GetSession<User>(SessionConstants.User);
            MessageSearch messageSearch = new MessageSearch()
            {
                UserId = currentUser.UserId,
                SenderId = (senderDropDown.SelectedIndex == 0) ? string.Empty : senderDropDown.SelectedValue,
                RecipientId = (recepientDropDown.SelectedIndex == 0) ? string.Empty : recepientDropDown.SelectedValue,
                SenderType = (senderTypeDropDown.SelectedIndex == 0) ? string.Empty : senderTypeDropDown.SelectedValue,
                RegionId = (regionDropDown.SelectedIndex == 0) ? string.Empty : regionDropDown.SelectedValue,
                QuickSearchExpression = searchTextBox.Text.Trim(),
            };

            if ((!String.IsNullOrEmpty(fromDateTextBox.Text.Trim())) && (!String.IsNullOrEmpty(toDateTextBox.Text.Trim())))
            {
                messageSearch.DateFrom = Convert.ToDateTime(fromDateTextBox.Text.Trim(), CultureInfo.InvariantCulture);
                messageSearch.DateTo = Convert.ToDateTime(toDateTextBox.Text.Trim(), CultureInfo.InvariantCulture);
            }

            return messageSearch;
        }

        /// <summary>
        /// To Populate Status Drop Down Data
        /// </summary>
        /// <param name="statusDropDownList"></param>
        private void PopulateStatusDropDownData(DropDownList statusDropDownList)
        {
            // Populate Section dropdown list
            statusDropDownList.DataSource = GetStaticDropDownDataSource(DropDownConstants.MessageStatusDropDown);
            statusDropDownList.DataTextField = "DataTextField";
            statusDropDownList.DataValueField = "DataValueField";
            statusDropDownList.DataBind();
        }

        /// <summary>
        /// To Search Messages
        /// </summary>
        /// <param name="feedbackSearch"></param>
        private void SearchMessages(MessageSearch messageSearch)
        {
            int totalRecordCount = 0;
            if (messageSearch.AllClick)
            {
                pagingControl.AllPageClick = true;
                messageSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.MaxPageSize), CultureInfo.InvariantCulture); // Max page size for performance optimization
            }
            else
            {
                pagingControl.AllPageClick = false;
                messageSearch.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
                if (messageSearch.PageIndex == -1)
                {
                    messageSearch.PageIndex = 0;
                }
                pagingControl.CurrentPageIndex = messageSearch.PageIndex;
            }

            //call the business method
            var messageList = _messageManager.GetMessageList(messageSearch, ref totalRecordCount);

            if (messageList.Count == 0)
            {
                ConfigureResultMessage(searchResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.NoMessagesFoundForCriteria, culture));

                exportCSVLinkButton.Visible = false;
                saveLinkButton.Visible = false;
                messageListRepeater.Visible = false;
            }
            else
            {
                exportCSVLinkButton.Visible = true;
                saveLinkButton.Visible = false;//Set to true to extend save all functionality
                messageListRepeater.Visible = true;
            }

            messageSearch.TotalRecordCount = totalRecordCount;

            SetViewState<MessageSearch>(ViewStateConstants.MessageViewState, messageSearch);

            BindMessages(messageList);

            //Pagination Control Setting
            pagingControl.TotalRecordCount = totalRecordCount;
            pagingControl.PageSize = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageSize), CultureInfo.InvariantCulture);
            pagingControl.PageNumberDisplayCount = Convert.ToInt16(ConfigurationStore.GetAppSetting(AppSettingConstants.PageNumberDisplayCount), CultureInfo.InvariantCulture);

            validateDateCustomValidator.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// This method will sort the result repeater control
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        private void SortRepeaterView(string sortExpression, string sortDirection)
        {
            MessageSearch messageSearch = GetViewState<MessageSearch>(ViewStateConstants.MessageViewState);
            messageSearch.SortExpression = sortExpression;
            messageSearch.SortDirection = sortDirection;
            messageSearch.PageIndex = 0;
            SearchMessages(messageSearch);

            if (pagingControl.AllPageClick == false)
            {
                //Pagination Control Setting
                pagingControl.CurrentPageIndex = 0;
                pagingControl.PageSet = 0;
            }
        }
    }
}