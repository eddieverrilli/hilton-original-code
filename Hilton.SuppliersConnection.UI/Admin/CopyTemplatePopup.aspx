﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CopyTemplatePopup.aspx.cs"
    Inherits="Hilton.SuppliersConnection.UI.Admin.CopyTemplatePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../Styles/iframe-modal.css" />
    <!--[if IE 8]>
    <link type='text/css' href='../Styles/adminie8.css' rel='stylesheet' />
    <![endif]-->
    <link href="../Styles/iewidthfix.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.ie-select-width.js" ></script>
    <script type="text/javascript" src="../Scripts/ie-select-width.js"></script>
    <script  type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%#GoogleAnalyticsId%>', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="copyTemplateScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>    
    <script type="text/javascript">
        $(window).ready(function () {
            $('.cancel').click(function () {
                window.parent.closeIframe();
                return false;

            });
        });
        function Confirm() {
            var record = confirm('Are you sure to proceed?');
            if (record == 1) {
                return true;
            }
            else if (record == 0) {
                return false;
            }
        }

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }
    </script>
    <div class="modalbox" runat="server">
        <h3>
            <asp:Label ID="titleLabel" runat="server" Text="Copy Template" CBID="370"></asp:Label></h3>
        <div class="modal-content">
            <div class="modal-innercontent">
                <p>
                    <asp:Label ID="descriptionLabel" runat="server" Text="This will make a copy of the current template & overwrite another template. This
                feature is useful if you’d like to quickly create another template that’s similar to this one."
                        CBID="1040"></asp:Label>
                </p>
                <asp:UpdatePanel ID="resultUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="resultMessageDiv" runat="server">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="copybox">
                    <table width="100%" class="mg-bottom">
                        <tr valign="top">
                            <td>
                                <asp:Label ID="copyLabel" runat="server" Text="Copy" CBID="341"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButton ID="categoriesRadioButton" Style="vertical-align: middle" Text="Active Categories Only"
                                    CBID="171" runat="server" GroupName="CategoryStatus" Checked="true" />
                            </td>
                            <td>
                                <asp:RadioButton ID="categoriesPartnerProductsRadioButton" Style="vertical-align: middle"
                                    Text="Active Categories and Partner/Products" runat="server" GroupName="CategoryStatus"
                                    CBID="172" />
                            </td>
                        </tr>
                        <tr style="height: 5px">
                        </tr>
                        <tr valign="top">
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="to template" CBID="724"></asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList CssClass="copytemplate-dropdown" ID="regionSelectionDropDown" runat="server"
                                    AutoPostBack="true" OnSelectedIndexChanged="RegionSelectionDropDown_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="regionRequiredFieldValidator" runat="server" SetFocusOnError="true"
                                    ControlToValidate="regionSelectionDropDown" ErrorMessage="Please select a combination"
                                    InitialValue="-1" CssClass="errorText" Display="Dynamic" ValidationGroup="CopyTemplate"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="section alR pad-top">
                    <span class="btn-style btn-stylepad">
                        <asp:LinkButton ID="saveCopyTemplateLinkButton" OnClientClick='return Confirm();'
                            Text="Copy" CBID="342" ValidationGroup="CopyTemplate" runat="server" OnClick="SaveCopyTemplateLinkButton_Click"></asp:LinkButton>
                    </span><span class="btn-style ">
                        <asp:LinkButton ID="cancelCopyTemplateLinkButton" runat="server" Text="Cancel" CBID="274"
                            class="cancel simplemodal-overlay"></asp:LinkButton>
                    </span>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
