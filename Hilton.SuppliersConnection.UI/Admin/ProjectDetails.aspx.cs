﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ProjectDetails : PageBase
    {
        private IProjectManager _projectManager;
    
        /// <summary>
        /// Invoked on OnInit
        /// </summary>
        /// <param name="args"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                ValidateUserAccess((int)MenuEnum.Projects);
                _projectManager = ProjectManager.Instance;
                SetGridViewHeaderText();
                this.Title = ResourceUtility.GetLocalizedString(546, culture, "Project Details");
                backToProjectsHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "‹‹ {0}", ResourceUtility.GetLocalizedString(244, culture, "Back to Projects"));
                usersLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(736, culture, "Users"));
                updateHistoryLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0}&nbsp;", ResourceUtility.GetLocalizedString(730, culture, "Update History"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// To set the Header of the User GridView
        /// </summary>
        private void SetGridViewHeaderText()
        {
            gridviewUsers.Columns[0].HeaderText = ResourceUtility.GetLocalizedString(300, culture, "@");
            gridviewUsers.Columns[1].HeaderText = ResourceUtility.GetLocalizedString(448, culture, "Name");
            gridviewUsers.Columns[2].HeaderText = ResourceUtility.GetLocalizedString(381, culture, "Email");
            gridviewUsers.Columns[3].HeaderText = ResourceUtility.GetLocalizedString(414, culture, "HiltonID");
            gridviewUsers.Columns[4].HeaderText = ResourceUtility.GetLocalizedString(629, culture, "Role");
        }

        /// <summary>
        /// Invoked on Page Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GetSession<User>(SessionConstants.User) != null)
                {
                    if (nameTextBox.Text != "Name")
                    {
                        nameTextBox.Style.Remove("color");
                    }
                    if (hiltonIdTextBox.Text != "Hilton ID")
                    {
                        hiltonIdTextBox.Style.Remove("color");
                    }
                    if (emailTextBox.Text != "Email address")
                    {
                        emailTextBox.Style.Remove("color");
                    }

                    if (!IsPostBack)
                    {
                        nameTextBox.Style.Add("color", "#888");
                        hiltonIdTextBox.Style.Add("color", "#888");
                        emailTextBox.Style.Add("color", "#888");

                        int projectId = Convert.ToInt32(Context.Items[UIConstants.ProjectId], CultureInfo.InvariantCulture);
                        SetHiddenFieldValue(projectIdHiddenField, projectId.ToString(CultureInfo.InvariantCulture));
                        GetBasicProjectDetails(projectId);
                        GetProjectUsers(projectId);
                        GetProjectUserHistory(projectId);
                        PopulateCategoryList(projectId);
                        SetRegularExpressions();
                        BindStaticDropDown(DropDownConstants.OwnerAndConsultantRolesDropDown, userRoleDropDownlist, ResourceUtility.GetLocalizedString(1251, culture, "Select"), null);
                    }
                    SetDefaultVisibilityForUpdatePanels();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultUpdateHistoryDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// This method will set the validation expression for regular expression validator based on culture
        /// </summary>
        private void SetRegularExpressions()
        {
            // Regular Expression for website
            SetValidatorRegularExpressions(regexEmailValid, (int)RegularExpressionTypeEnum.EmailValidator);
            SetValidatorRegularExpressions(hiltonIdRegularExpression, (int)RegularExpressionTypeEnum.HiltonIdValidator);
        }

        /// <summary>
        /// Set the visibility off of the result div
        /// </summary>
        /// <param name=""></param>
        public void SetDefaultVisibilityForUpdatePanels()
        {
            categoryAdditionResultUpdatePanel.Visible = false;
            leafCatPartnerProductResultUpdatePanel.Visible = false;
            projectDetailsUpdatePanel.Visible = false;
        }

        /// <summary>
        /// Get the Project History List for the project
        /// </summary>
        /// <param name="projectId"></param>
        public void GetProjectUserHistory(int projectId)
        {
            try
            {
                IList<UserProjectHistory> userProjectHistroyList = _projectManager.GetProjectUsersHistory(projectId);
                if (userProjectHistroyList != null && userProjectHistroyList.Count > 0)
                {
                    updateHistoryDiv.Attributes.Add("class", "scroll");
                }
                else
                {
                    updateHistoryDiv.Attributes.Remove("class");
                }
                gridviewUpdateHistory.DataSource = userProjectHistroyList;
                gridviewUpdateHistory.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultUpdateHistoryDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Get the project details for the project
        /// </summary>
        /// <param name=""></param>
        public void GetBasicProjectDetails(int projectId)
        {
            try
            {
                Project projectDetails = new Project();
                projectDetails = _projectManager.GetProjectDetails(projectId);
                SetHiddenFieldValue(templateIdHiddenField, projectDetails.TemplateId.ToString(CultureInfo.InvariantCulture));
                facilityIdLbl.Text = projectDetails.FacilityId.ToString(CultureInfo.InvariantCulture);
                projectTemplateDetailsLabel.Text = projectDetails.BrandDescription.ToString(CultureInfo.InvariantCulture) + ", " + projectDetails.RegionDescription.ToString(CultureInfo.InvariantCulture) + ", " + projectDetails.PropertyTypeDescription.ToString(CultureInfo.InvariantCulture);
                brandDescriptionLabel.Text = projectDetails.BrandDescription.ToString(CultureInfo.InvariantCulture);
                brandSegmentDescriptionLabel.Text = projectDetails.BrandSegment.ToString(CultureInfo.InvariantCulture);
                regionDescriptionLabel.Text = projectDetails.RegionDescription.ToString(CultureInfo.InvariantCulture);
                //Country details
                countryDescriptionLabel.Text = projectDetails.CountryDescription.ToString(CultureInfo.InvariantCulture);
                propertyTypeDescriptionLabel.Text = projectDetails.PropertyTypeDescription.ToString(CultureInfo.InvariantCulture);
                projectTypeLabel.Text = projectDetails.ProjectTypeDescription.ToString(CultureInfo.InvariantCulture);
                lblStatus.Text = projectDetails.O2OStatusDescription.ToString(CultureInfo.InvariantCulture);
                BindStaticDropDown(DropDownConstants.ProjectStatusDropDown, supplierConnStatusDropDownlist, null, null);
                supplierConnStatusDropDownlist.SelectedValue = projectDetails.ProjectStatus.ToString();
                sCCompletionLbl.Text = projectDetails.ConfigPercent.ToString(CultureInfo.InvariantCulture) + "%";
                locationLbl.Text = projectDetails.FacilityLocation.ToString(CultureInfo.InvariantCulture);
                ownerLbl.Text = projectDetails.OwnerName.ToString(CultureInfo.InvariantCulture);
                projectImage.ImageUrl = projectDetails.FeaturedThumbnailImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(projectDetails.FeaturedThumbnailImage.ConvertToThumbnail()) : "../Images/NoProduct.PNG";
                featuredDropDownlist.SelectedValue = projectDetails.IsFeatured.ToString(CultureInfo.InvariantCulture);
                featuredDesciptionTexbox.Text = projectDetails.FeaturedDescription.ToString(CultureInfo.InvariantCulture);
                imageNameHiddenField.Value = projectDetails.ImageName.ToString();
                imageLinkButton.Text = projectDetails.ImageName.ToString();
                if (!string.IsNullOrEmpty(projectDetails.ImageName) && projectDetails.FeaturedThumbnailImage != null)
                    removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "inline");

                SetSession<byte[]>(SessionConstants.ProductImageBytes, projectDetails.FeaturedThumbnailImage);
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(projectDetailsResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Get the userlist mapped to the project
        /// </summary>
        /// <param name="projectId"></param>
        public void GetProjectUsers(int projectId)
        {
            try
            {
                IList<User> userList = _projectManager.GetProjectUsers(projectId);
                if (userList != null && userList.Count > 0)
                {
                    usersDiv.Attributes.Add("class", "scroll");
                }
                else
                {
                    usersDiv.Attributes.Remove("class");
                }
                gridviewUsers.DataSource = userList;
                gridviewUsers.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(userAdditionResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Get the Category list by project Id
        /// </summary>
        /// <param name="projectId"></param>
        public void PopulateCategoryList(int projectId)
        {
            try
            {
                Tuple<IList<Partner>, IList<Product>> partnerProductlist = _projectManager.GetLeafCatPartners(projectId);
                SetViewState<Tuple<IList<Partner>, IList<Product>>>(ViewStateConstants.LeafCategoryPartnerProducts, partnerProductlist);
                categoryRepeator.DataSource = _projectManager.GetCategoryListByProjectId(projectId);
                categoryRepeator.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultLeafCategoryDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when row is added to user gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gridviewUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (((Hilton.SuppliersConnection.Entities.User)(e.Row.DataItem)).ReceivePartnerCommunication == false)
                {
                    Image receivePartnerCommunicationImage = new Image();
                    receivePartnerCommunicationImage = (Image)e.Row.FindControl("RecCommCheckbox");
                    receivePartnerCommunicationImage.Visible = false;
                }
            }
        }

        /// <summary>
        /// Invoked on Add User Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void addLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid)
                {
                    User user = new User()
                    {
                        Name = nameTextBox.Text.Trim(),
                        Email = emailTextBox.Text.Trim(),
                        UserTypeId = Convert.ToInt32(userRoleDropDownlist.SelectedValue, CultureInfo.InvariantCulture),
                        ReceivePartnerCommunication = RecCommCheckbox.Checked,
                        ProjectId = Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture)
                    };
                    if (hiltonIdTextBox.Text.Trim() == "Hilton ID")
                    {
                        user.HiltonUserName = string.Empty;
                    }
                    else
                    {
                        user.HiltonUserName = hiltonIdTextBox.Text.Trim();
                    }
                    string[] name = nameTextBox.Text.Split(' ');
                    string lastName = string.Empty;
                    for (int length = 1; length < name.Length; length++)
                    {
                        if (length == 1)
                        {
                            lastName = name[1];
                        }
                        else
                        {
                            lastName += " " + name[length];
                        }
                    }
                    if (String.IsNullOrEmpty(lastName.Trim()))
                    {
                        lastNameErrorLabel.Visible = true;
                    }
                    else
                    {
                        user.LastName = lastName;
                        user.FirstName = name[0].ToString();
                        var loggedUser = GetSession<User>(SessionConstants.User);
                        user.UpdatedByUserId = loggedUser.UserId;

                        lastNameErrorLabel.Visible = false;
                        int result = _projectManager.AddUserToProject(user);
                        if (result > 0)
                        {
                            GetProjectUsers(Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture));
                            ConfigureResultMessage(userAdditionResultDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.UserProjectMappingSavedSuccessfully, culture));
                            categoryAdditionResultUpdatePanel.Visible = true;
                            categoryAdditionResultUpdatePanel.Update();
                            SetDefaultValuesInTextBox();
                        }
                        else if (result == -1)
                        {
                            ConfigureResultMessage(userAdditionResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.DuplicateHiltonIdErrorMessage, culture));
                            categoryAdditionResultUpdatePanel.Visible = true;
                            categoryAdditionResultUpdatePanel.Update();
                        }
                        else if (result == -2)
                        {
                            ConfigureResultMessage(userAdditionResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserTypeIsNotOwnerOrConsultant, culture));
                            categoryAdditionResultUpdatePanel.Visible = true;
                            categoryAdditionResultUpdatePanel.Update();
                        }
                        else if (result == -3)
                        {
                            ConfigureResultMessage(userAdditionResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.UserProjectMappingExists, culture));
                            categoryAdditionResultUpdatePanel.Visible = true;
                            categoryAdditionResultUpdatePanel.Update();
                        }
                        else if (result == -4)
                        {
                            ConfigureResultMessage(userAdditionResultDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.InactiveUser, culture));
                            categoryAdditionResultUpdatePanel.Visible = true;
                            categoryAdditionResultUpdatePanel.Update();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(userAdditionResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///
        /// </summary>
        private void SetDefaultValuesInTextBox()
        {
            nameTextBox.Text = "Name";
            emailTextBox.Text = "Email address";
            hiltonIdTextBox.Text = "Hilton ID";
            userRoleDropDownlist.SelectedValue = Convert.ToString(0);
            nameTextBox.Style.Add("color", "#888");
            emailTextBox.Style.Add("color", "#888");
            hiltonIdTextBox.Style.Add("color", "#888");
        }

        /// <summary>
        /// Invoked on Submit Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            SaveProjectConfiguration();
            GetProjectUserHistory(Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///
        /// </summary>
        private void SaveProjectConfiguration()
        {
            try
            {
                IList<CategoryPartnerProduct> catPartnerProductList = new List<CategoryPartnerProduct>();
                IList<Category> catList = new List<Category>();
                foreach (RepeaterItem itemCat in categoryRepeator.Items)
                {
                    Label projectConfigDetailsIdLabel = new Label();
                    projectConfigDetailsIdLabel = (Label)itemCat.FindControl("projectConfigDetailsIdLabel");
                    Label projectConfigurationIdLabel = new Label();
                    projectConfigurationIdLabel = (Label)itemCat.FindControl("projectConfigurationIdLabel");
                    Label isLeafLabel = new Label();
                    isLeafLabel = (Label)itemCat.FindControl("isLeafLabel");
                    Label categoryIdLabel = new Label();
                    categoryIdLabel = (Label)itemCat.FindControl("categoryIdLabel");
                    if (Convert.ToBoolean(isLeafLabel.Text))
                    {
                        catList.Add(new Category()
                        {
                            ProjectConfigDetailsId = Convert.ToInt32(((Label)itemCat.FindControl("projectConfigDetailsIdLabel")).Text, CultureInfo.InvariantCulture),
                            IsFeatured = Convert.ToBoolean(((CheckBox)itemCat.FindControl("seletedChk")).Checked),
                            IsRequired = Convert.ToBoolean(((DropDownList)itemCat.FindControl("requiredDropDownlist")).SelectedValue, CultureInfo.InvariantCulture),
                            ProjectConfigurationId = Convert.ToInt32(((Label)itemCat.FindControl("projectConfigurationIdLabel")).Text, CultureInfo.InvariantCulture),
                            CategoryId = Convert.ToInt32(((Label)itemCat.FindControl("categoryIdLabel")).Text, CultureInfo.InvariantCulture),
                        });
                    }
                    Repeater partnerProductRepeater = new Repeater();
                    partnerProductRepeater = (Repeater)itemCat.FindControl("productPartnerRepeator");
                    if (partnerProductRepeater != null && partnerProductRepeater.Items.Count > 0)
                    {
                        foreach (RepeaterItem itemPartnerProduct in partnerProductRepeater.Items)
                        {
                            if ((Convert.ToInt32(((DropDownList)itemPartnerProduct.FindControl("partnerDropDownList")).SelectedValue, CultureInfo.InvariantCulture)) != 0)
                            {
                                catPartnerProductList.Add(new CategoryPartnerProduct()
                                {
                                    ProjectLeafCategoryDetailsId = Convert.ToInt32(((Label)itemPartnerProduct.FindControl("catLabel")).Text),
                                    PartnerId = Convert.ToInt32(((DropDownList)itemPartnerProduct.FindControl("partnerDropDownList")).SelectedValue, CultureInfo.InvariantCulture),
                                    ProductId = Convert.ToInt32(((DropDownList)itemPartnerProduct.FindControl("productDropDownList")).SelectedValue, CultureInfo.InvariantCulture),
                                    CategoryId = Convert.ToInt32(((Label)itemPartnerProduct.FindControl("catIdLabel")).Text)
                                });
                            }
                        }
                    }
                }
                Project projectDetails = new Project()
                {
                    ProjectId = Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture),
                    UserId = GetSession<User>(SessionConstants.User).UserId
                };

                var duplicate = catPartnerProductList.GroupBy(s => new { s.CategoryId, s.PartnerId, s.ProductId }).Where(g => g.Count() > 1).Select(g => g.Key);
                if (duplicate.Count() > 0)
                {
                    ConfigureResultMessage(resultLeafCategoryDiv, "message notification", ConfigurationStore.GetApplicationMessages(MessageConstants.ProjectDetailsDuplicateRecords, culture));
                    leafCatPartnerProductResultUpdatePanel.Visible = true;
                    categoryAdditionResultUpdatePanel.Update();
                }
                else
                {
                    int result = _projectManager.UpdatePartnerProductLeafCategory(catPartnerProductList, catList, projectDetails);
                    if (result > 0)
                    {
                        PopulateCategoryList(Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture));
                        ConfigureResultMessage(resultLeafCategoryDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.ProjectDetailsSavedSuccessfully, culture));
                        leafCatPartnerProductResultUpdatePanel.Visible = true;
                        categoryAdditionResultUpdatePanel.Update();
                    }
                    else
                    {
                        ConfigureResultMessage(resultLeafCategoryDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.ProjectDetailsError, culture));
                        leafCatPartnerProductResultUpdatePanel.Visible = true;
                        categoryAdditionResultUpdatePanel.Update();
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultLeafCategoryDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when save button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Project project = new Project()
               {
                   FeaturedImage = Session[SessionConstants.ProductImageBytes] != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? (byte[])Session[SessionConstants.ProductImageBytes] : null,
                   FeaturedThumbnailImage = Session[SessionConstants.ProductImageBytes] != null && !string.IsNullOrEmpty(imageNameHiddenField.Value) ? ((byte[])Session[SessionConstants.ProductImageBytes]).ConvertToThumbnail() : null,
                   ImageName = imageNameHiddenField.Value,
                   IsFeatured = Convert.ToBoolean(featuredDropDownlist.SelectedValue, CultureInfo.InvariantCulture),
                   FeaturedDescription = featuredDesciptionTexbox.Text.Trim(),
                   ProjectId = Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture),
                   ProjectStatus = Convert.ToInt32(supplierConnStatusDropDownlist.SelectedValue.ToString()),
                   UserId = GetSession<User>(SessionConstants.User).UserId
               };
                bool result = _projectManager.UpdateProjectDetails(project);

                if (result)
                {
                    GetBasicProjectDetails(Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture));
                    GetProjectUserHistory(Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture));
                    ConfigureResultMessage(projectDetailsResultDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.BasicProjectDetailsSavedSuccessfully, culture));
                    projectDetailsUpdatePanel.Visible = true;
                    categoryAdditionResultUpdatePanel.Update();
                }
                else
                {
                    ConfigureResultMessage(projectDetailsResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.BasicProjectDetailsError, culture));
                    projectDetailsUpdatePanel.Visible = true;
                    categoryAdditionResultUpdatePanel.Update();
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(projectDetailsResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
                projectDetailsUpdatePanel.Visible = true;
                categoryAdditionResultUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Invoked when row is added to user Grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void GridviewUsers_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteUser")
            {
                int index = Convert.ToInt32(e.CommandArgument, CultureInfo.InvariantCulture);
                GridViewRow deleteUserRow = gridviewUsers.Rows[index];

                User deleteUserProjMapping = new User()
                {
                    UserId = Convert.ToInt32(gridviewUsers.DataKeys[index].Values[0].ToString()),
                    ProjectId = Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture)
                };

                DeleteUserProjectMapping(deleteUserProjMapping);
            }
        }

        /// <summary>
        /// Invoked when delete button is click
        /// </summary>
        /// <param name="deleteUserProjMapping"></param>
        public void DeleteUserProjectMapping(User deleteUserProjMapping)
        {
            try
            {
                bool result = _projectManager.DeleteUserFromProject(deleteUserProjMapping);
                if (result == true)
                {
                    GetProjectUsers(Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture));
                    ConfigureResultMessage(userAdditionResultDiv, "message success", ConfigurationStore.GetApplicationMessages(MessageConstants.UserProjectMappingDeleted, culture));
                    categoryAdditionResultUpdatePanel.Visible = true;
                    categoryAdditionResultUpdatePanel.Update();
                    GetProjectUserHistory(Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture));
                    GetProjectUsers(Convert.ToInt32(projectIdHiddenField.Value, CultureInfo.InvariantCulture));
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(userAdditionResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when item is bound to CategoryRepeator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryRepeator_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item;
            if (((item.ItemType == ListItemType.Item) ||
                (item.ItemType == ListItemType.AlternatingItem)) && item.ItemType != ListItemType.Header)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl cat_div = (System.Web.UI.HtmlControls.HtmlGenericControl)item.FindControl("CatDiv");
                string CatId = ((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).CategoryId.ToString();
                 string ParentCatId = ((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).ParentCategoryId.ToString();
                 string RootParentCatId = ((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).RootParentId.ToString();

                 if (((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).ParentCategoryId != -1)
                 {
                     cat_div.Style.Add("display", "none");
                 }


                Label categoryLabel = new Label();
                categoryLabel = (Label)item.FindControl("catLabel");
                categoryLabel.Style.Add("margin-left", 30 * (((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).Level - 1) + "px");
                if (((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).IsLeaf)
                {
                    cat_div.Attributes.Add("class", ParentCatId +" "+ RootParentCatId + "r");

                    Label img = (Label)item.FindControl("ToggleImage");
                    if (img != null)
                        img.Visible = false;

                    ((CheckBox)item.FindControl("seletedChk")).Visible = true;
                    ((CheckBox)item.FindControl("seletedChk")).Checked = ((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).IsFeatured;

                    DropDownList requiredDropDownlist = new DropDownList();
                    requiredDropDownlist = (DropDownList)item.FindControl("requiredDropDownlist");
                    requiredDropDownlist.Visible = true;
                    requiredDropDownlist.SelectedValue = ((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).IsRequired.ToString();
                }
                else
                {
                    cat_div.Attributes.Add("onClick", "ToggleCategory('" + CatId +"','" + RootParentCatId +"')");
                   
                        cat_div.Attributes.Add("class", CatId + "p " + ParentCatId +" "+ RootParentCatId +"r");
                    


                    cat_div.Style.Add("cursor", "pointer");

                    Label img = (Label)item.FindControl("ToggleImage");
                    if (img != null)
                        img.Visible = true;
                }

                if (((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).IsLeaf == true && ((((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)))).PartnerProducts.Count > 0)
                {
                    ((CheckBox)item.FindControl("seletedChk")).Visible = true;
                    ((CheckBox)item.FindControl("seletedChk")).Checked = ((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).IsFeatured;

                    DropDownList requiredDropDownlist = new DropDownList();
                    requiredDropDownlist = (DropDownList)item.FindControl("requiredDropDownlist");
                    requiredDropDownlist.Visible = true;
                    requiredDropDownlist.SelectedValue = ((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).IsRequired.ToString();
                }

                if ((((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).IsLeaf == true) && (((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).PartnerProducts.Count > 0))
                {
                    Repeater productPartnerRepeator = new Repeater();
                    productPartnerRepeator = (Repeater)item.FindControl("productPartnerRepeator");

                    productPartnerRepeator.DataSource = ((Hilton.SuppliersConnection.Entities.Category)(item.DataItem)).PartnerProducts;
                    productPartnerRepeator.DataBind();
                }
            }
        }

        /// <summary>
        /// To get product details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Project project = _projectManager.GetProjectImage(Convert.ToInt32(projectIdHiddenField.Value));
                ProcessFileDownload("image", project.FeaturedImage, project.ImageName);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(projectDetailsResultDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "SpecSheetScript1", "SetFileInputStyles();", true);
        }

        /// <summary>
        /// To Process file download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            string extension = string.Empty;

            switch (fileType)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "image":
                    string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    int upperBound = tempString.GetUpperBound(0);
                    contentType = "image/" + tempString[upperBound];
                    break;
                default:
                    contentType = "";
                    break;
            };

            Response.Buffer = true;
            Response.Charset = "";
            Response.ClearHeaders();
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
            Response.BinaryWrite(fileBytes);
            Response.Flush();
            Response.Clear();
            Response.End();
        }

        /// <summary>
        /// Invoked when item is bound to ProductPartnerRepeator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProductPartnerRepeator_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item;

            if (((item.ItemType == ListItemType.Item) ||
                (item.ItemType == ListItemType.AlternatingItem)) && item.ItemType != ListItemType.Header)
            {
                DropDownList partnerDropDown = new DropDownList();
                partnerDropDown = (DropDownList)item.FindControl("partnerDropDownList");
                DropDownList productDropDown = new DropDownList();
                productDropDown = (DropDownList)item.FindControl("productDropDownList");

                if (((Hilton.SuppliersConnection.Entities.CategoryPartnerProduct)(item.DataItem)).IsOther)
                {
                    ListItem partnerli = new ListItem();
                    partnerli.Text = "Others";
                    partnerli.Value = ((Hilton.SuppliersConnection.Entities.CategoryPartnerProduct)(item.DataItem)).PartnerId.ToString(CultureInfo.InvariantCulture);
                    partnerDropDown.Items.Add(partnerli);

                    ListItem productli = new ListItem();
                    productli.Text = ((Hilton.SuppliersConnection.Entities.CategoryPartnerProduct)(item.DataItem)).ProductName.ToString(CultureInfo.InvariantCulture);
                    productli.Value = ((Hilton.SuppliersConnection.Entities.CategoryPartnerProduct)(item.DataItem)).ProductId.ToString(CultureInfo.InvariantCulture);
                    productDropDown.Items.Add(productli);
                }
                else
                {
                    var partnerProductList = GetViewState<Tuple<IList<Partner>, IList<Product>>>(ViewStateConstants.LeafCategoryPartnerProducts);
                    var partnerlist = partnerProductList.Item1;
                    partnerlist = partnerlist.Where(p => p.PartnerCategory.CategoryId == ((CategoryPartnerProduct)(item.DataItem)).CategoryId && p.IsOther == ((CategoryPartnerProduct)(item.DataItem)).IsOther).ToList();
                    BindPartnerListByCatId(partnerlist, partnerDropDown);

                    partnerDropDown.SelectedValue = ((CategoryPartnerProduct)(item.DataItem)).PartnerId.ToString(CultureInfo.InvariantCulture);

                    var productlist = partnerProductList.Item2;
                    BindProductListByCatId(productlist.Where(p => p.PartnerId == ((CategoryPartnerProduct)(item.DataItem)).PartnerId), productDropDown);
                    productDropDown.SelectedValue = ((Hilton.SuppliersConnection.Entities.CategoryPartnerProduct)(item.DataItem)).ProductId.ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// Invoked when item of the partnerdropdown is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PartnerDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList partnerDropDownList = new DropDownList();
            partnerDropDownList = (DropDownList)sender;
            RepeaterItem item = (RepeaterItem)((DropDownList)sender).NamingContainer;

            DropDownList productDropDownList = new DropDownList();
            productDropDownList = (DropDownList)item.FindControl("productDropDownList");

            var partnerProductList = GetViewState<Tuple<IList<Partner>, IList<Product>>>(ViewStateConstants.LeafCategoryPartnerProducts);
            var productlist = partnerProductList.Item2;
            BindProductListByCatId(productlist.Where(p => p.PartnerId == Convert.ToInt32(partnerDropDownList.SelectedValue, CultureInfo.InvariantCulture)), productDropDownList);
        }

        /// <summary>
        /// Invoked when removeImage button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RemoveImageLinkButton_Click(object sender, EventArgs e)
        {
            imageLinkButton.Text = string.Empty;
            imageNameHiddenField.Value = string.Empty;
            projectImage.ImageUrl = "../Images/NoProduct.PNG";
            ClearSession(SessionConstants.ProductImageBytes);
            removeImageLinkButton.Style.Add(HtmlTextWriterStyle.Display, "none");
            ScriptManager.RegisterClientScriptBlock(imageUploadUpdatePanel, imageUploadUpdatePanel.GetType(), "SpecSheetScript1", "SetFileInputStyles();", true);
        }

        /// <summary>
        /// Get the list of the Partner .
        /// </summary>
        /// <param name="partnerList"></param>
        /// <param name="partnerDropDownList"></param>
        public void BindPartnerListByCatId(IEnumerable<Partner> partnerList, DropDownList partnerDropDownList)
        {
            partnerDropDownList.DataSource = partnerList.Select(p => new { p.CompanyName, p.PartnerId }).Distinct();
            partnerDropDownList.DataTextField = "CompanyName";
            partnerDropDownList.DataValueField = "PartnerId";
            partnerDropDownList.DataBind();
        }

        /// <summary>
        /// Get the list of the Product
        /// </summary>
        /// <param name="partnerList"></param>
        /// <param name="partnerDropDownList"></param>
        public void BindProductListByCatId(IEnumerable<Product> productList, DropDownList productDropDownList)
        {
            productDropDownList.Items.Clear();
            productDropDownList.DataSource = productList.Where(p => !string.IsNullOrWhiteSpace(p.ProductName));
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "productId";
            productDropDownList.DataBind();
            productDropDownList.Items.Insert(0, new ListItem("None", DropDownConstants.DefaultValue));
        }
    }
}