﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" CodeBehind="Standards.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.Standards" %>

<%@ Register Src="~/Controls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDetails" runat="server">
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../Images/search-top-curve.gif" alt="" /></div>
            <div class="middle">
                <div class="search-section">
                    <asp:Button ID="SearchButton" runat="server" Text="Button" class="search-btn" />
                    <div class="textbox">
                        <asp:TextBox ID="SearchText" runat="server" /></div>
                </div>
                <img src="../Images/search-division.gif" alt="" />
                <div class="fieldset">
                    <div class="clear">
                    </div>
                    <div class="right-field">
                        <table>
                            <tr>
                                <td>
                                    <div class="field">
                                        <span class="label">Date Updated :</span>
                                        <div class="textbox-1">
                                            <asp:TextBox ID="DateUpdatedTextBox" runat="server" class="datepicker"> </asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="field">
                                        <span class="label">to </span>
                                        <div class="textbox-1">
                                            <asp:TextBox ID="DateUpdatedToTextBox" runat="server" class="datepicker"> </asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="field">
                                        <input type="submit" class="go-btn" name="ctl00$cphDetails$btnGo" value="" id="cphDetails_btnGo">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="field">
                                        <span class="label">Publish Date :</span>
                                        <div class="textbox-1">
                                            <asp:TextBox ID="PublishDateText" runat="server" class="datepicker"> </asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="field">
                                        <span class="label">to </span>
                                        <div class="textbox-1">
                                            <asp:TextBox ID="PublishDateToText" runat="server" class="datepicker"> </asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="field">
                                        <asp:Button ID="GoButton" Text="Go" runat="server" class="go-btn" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="left-field">
                        <div class="field">
                            <span class="label first">Partner :</span>
                            <div class="textbox-2">
                                <asp:DropDownList ID="PartnerTypeDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="field">
                            <span class="label">Region :</span>
                            <div class="textbox-2">
                                <asp:DropDownList ID="RegionDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="field">
                            <span class="label">Porp. Type :</span>
                            <div class="textbox-2">
                                <asp:DropDownList ID="PorpTypeDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="field">
                            <span class="label last">Brand :</span>
                            <div class="textbox-2">
                                <asp:DropDownList ID="BrandDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
            </div>
            <div class="bottom-curve">
                <img src="../Images/search-top-curve.gif" alt="" /></div>
        </div>
    </div>
    <div class="content">
        <div class="wrapper">
            <h2>
                Updated Standards</h2>
            <div class="inner-content">
                <div class="no-space">
                    <asp:GridView ID="UpdatedStandardsGridView" runat="server" AutoGenerateColumns="False"
                        CssClass="col-6" OnRowDataBound="UpdatedStandardsGridView_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderText="Date Updated" DataField="DateUpdated" HeaderStyle-CssClass="active" />
                            <asp:BoundField HeaderText="Brand" DataField="Brand" HeaderStyle-CssClass="active sort" />
                            <asp:BoundField HeaderText="Type" DataField="Type" HeaderStyle-CssClass="active" />
                            <asp:BoundField HeaderText="Region" DataField="Region" HeaderStyle-CssClass="active" />
                            <asp:BoundField HeaderText="Standard #" DataField="StandardNumber" HeaderStyle-CssClass="active" />
                            <asp:BoundField HeaderText="Category" DataField="Category" HeaderStyle-CssClass="active" />
                            <asp:BoundField HeaderText="New Publish Date" DataField="NewPublishDate" HeaderStyle-CssClass="active" />
                            <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="active">
                                <ItemTemplate>
                                    <asp:DropDownList ID="StatusDropDownList" runat="server" CssClass="list-1">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:HyperLinkField Text="edit" NavigateUrl="~/Admin/UpdateStandardsDetails.aspx"
                                HeaderStyle-CssClass="active" />
                        </Columns>
                    </asp:GridView>
                    <uc1:PagingControl ID="PagingControl1" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>