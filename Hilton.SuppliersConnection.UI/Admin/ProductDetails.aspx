﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.master"
    AutoEventWireup="True" ViewStateMode="Inherit" CodeBehind="ProductDetails.aspx.cs"
    EnableEventValidation="false" Inherits="Hilton.SuppliersConnection.UI.ProductDetails" %>

<%@ Import Namespace="System.Globalization" %>
<asp:Content ID="content1" ContentPlaceHolderID="cphDetails" ViewStateMode="Enabled"
    runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script src="../Scripts/jquery-ui.js" type="text/javascript"></script>   
    <script type="text/javascript" src="../Scripts/ajaxfileupload.js"></script>
    <script type="text/javascript" src="../Scripts/custominputs.js"></script>
    <script src="../Scripts/jquery.characterlimiter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/multiselect.region.and.countries.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {

            SetFileInputStyles();
            SetRegionAndCountryDropDown();
            $('textarea#descriptionTextArea').limiter(2000, $('#remainingCharacters'));
        });

        function SetFileInputStyles() {
            $("input.customInput").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 65
            });

            $("input.customInput1").filestyle({
                image: "../Images/choose.gif",
                imageheight: 22,
                imagewidth: 65,
                width: 65
            }); 5
        }

        function AjaxFileUpload(control, fileType) {
            //debugger;
            if (fileType == 'image') {
                var imageName = $('#ImageUpload')[0].value.toString().substring($('#ImageUpload')[0].value.toString().lastIndexOf('\\') + 1, $('#ImageUpload')[0].value.length);
                $('#<%= imageNameHiddenField.ClientID %>').val(imageName);
            }
            else {
                var pdfName = $('#specSheetFileUpload')[0].value.toString().substring($('#specSheetFileUpload')[0].value.toString().lastIndexOf('\\') + 1, $('#specSheetFileUpload')[0].value.length);
                $('#<%= pdfNameHiddenField.ClientID %>').val(pdfName);
            }
            if (fileType == 'image')
                $('#loading').attr('source', fileType);
            else
                $('#loading1').attr('source', fileType);

            $("#loading")
                .ajaxStart(function (obj) {
                    if ($(this).attr('source') == 'image') {
                        $(this).show();
                        $(this).attr('source', '');
                    }

                })
                .ajaxComplete(function (obj) {
                    $(this).hide();
                });

            $("#loading1")
                .ajaxStart(function (obj) {
                    if ($(this).attr('source') == 'pdf') {
                        $(this).show();
                        $(this).attr('source', '');
                    }

                })
                .ajaxComplete(function (obj) {
                    $(this).hide();
                });

            $.ajaxFileUpload
                    (
                        {
                            url: '../Handlers/FileUpload.ashx?fileType=' + fileType,
                            secureuri: false,
                            fileElementId: control,
                            dataType: 'json',
                            data: { name: 'login', id: 'id' },
                            success: function (data, status) {

                                if (status == 'success' && data.responseText != null && data.responseText == 'uploaded') {
                                    SetFileLink(fileType);

                                    if (fileType == 'image') {
                                        $.ajax({
                                            type: "POST",
                                            url: "../Handlers/FileUpload.ashx?fileType=" + fileType + "&op=show",
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (data, img) {
                                                //debugger;

                                                $('#<%= productImage.ClientID %>').attr('src', "data:image/png;base64," + data);
                                            },
                                            error: function (data, status, e) {

                                            }
                                        });

                                    }

                                }
                                else {
                                    ValidateFile(fileType, data.responseText);
                                }

                            },
                            error: function (data, status, e) {

                            }
                        }
                    )

            return false;

        }

        function disabler(event) {
            event.preventDefault();
            return false;
        }

        function SetFileLink(fileType) {
            debugger;
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text('');
                    $('#imageErrSpan').hide();
                    $('#<%= imageLinkButton.ClientID %>').text($('#<%= imageNameHiddenField.ClientID %>').val());
                    $('#<%= imageLinkButton.ClientID %>').bind('click', disabler);
                    if ($('#imageNameHiddenField.ClientID').val() != '') {
                        $('#<%= removeImageLinkButton.ClientID %>').show();
                    }
                    break;
                case 'pdf':
                    $('#pdfErrSpan').text('');
                    $('#pdfErrSpan').hide();
                    $('#<%= specSheetPdfLinkButton.ClientID %>').text($('#<%= pdfNameHiddenField.ClientID %>').val());
                    $('#<%= specSheetPdfLinkButton.ClientID %>').bind('click', disabler);
                    if ($('#pdfNameHiddenField.ClientID').val() != '') {
                        $('#<%= removePdfLinkButton.ClientID %>').show();
                    }
                    break;

            }
        }

        function ValidateFile(fileType, msg) {
            switch (fileType) {
                case 'image':
                    $('#imageErrSpan').text(msg);
                    $('#imageErrSpan').show();
                    $('#<%= removeImageLinkButton.ClientID %>').hide();
                    $('#<%= imageNameHiddenField.ClientID %>').val("");
                    $('#<%= imageLinkButton.ClientID %>').text("");
                    $('#<%= productImage.ClientID %>').attr('src', '../Images/NoProduct.PNG');
                    break;
                case 'pdf':
                    $('#pdfErrSpan').text(msg);
                    $('#pdfErrSpan').show();
                    $('#<%= removePdfLinkButton.ClientID %>').hide();
                    $('#<%= pdfNameHiddenField.ClientID %>').val("");
                    $('#<%= specSheetPdfLinkButton.ClientID %>').text("");
                    break;
            }

        }
    </script>
    <div class="search">
        <div class="wrapper">
            <div class="top-curve">
                <img src="../Images/search-top-curve.gif" alt="" /></div>
            <div class="middle">
                <asp:HyperLink ID="backtoProductsHyperLink" runat="server" NavigateUrl="~/Admin/Products.aspx"
                    Text="Back to Products">‹‹</asp:HyperLink>
            </div>
            <div class="bottom-curve">
                <img src="../Images/search-bottom-curve.gif" alt="" /></div>
        </div>
    </div>
    <div class="content">
        <div class="wrapper">
            <h2>
                <asp:Label ID="productDetailTitleLabel" runat="server" Text=""></asp:Label></h2>
            <div id="add-edit-partner" class="inner-content">
                <div id="resultMessageDiv" runat="server">
                </div>
                <br />
                <div class="lft-col">
                    <div class="box1">
                        <h3>
                            <asp:Label ID="productDetailsLabel" runat="server" Text="Product Details" CBID="528">
                            </asp:Label></h3>
                        <div class="row">
                            <div>
                                <asp:Label ID="idHeadLabel" runat="server" Text="ID" CBID="969" CssClass="label"></asp:Label>
                            </div>
                            <div class="input">
                                <asp:Label ID="productDetailIdLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="productName" runat="server" Text="Product Name" CBID="970" CssClass="label">
                                </asp:Label>
                            </div>
                            <div id="productNameDiv" runat="server">
                                <asp:TextBox ID="productNameTextBox" runat="server" MaxLength="256">
                                </asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="productNameRequiredFieldValidator" runat="server"
                                    SetFocusOnError="true" CssClass="errorText" ValidationGroup="SaveProduct" ControlToValidate="productNameTextBox"
                                    Display="Dynamic" ErrorMessage="Product Name is required" VMTI="21">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="skuLabel" runat="server" Text="SKU" CBID="673" CssClass="label"></asp:Label></div>
                            <div class="input textbox3">
                                <asp:TextBox ID="skuTextBox" runat="server" MaxLength="123">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="partnerNameHeadLabel" runat="server" Text="Partner" CBID="486" CssClass="label">
                                </asp:Label></div>
                            <asp:UpdatePanel ID="partnerDropdownUpdatePanel" runat="server" RenderMode="Inline"
                                UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="input textbox3">
                                        <asp:DropDownList ID="partnerDropdown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PartnerDropdown_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="partnerRequiredFieldValidator" VMTI="38" runat="server"
                                            SetFocusOnError="true" CssClass="errorText" ValidationGroup="SaveProduct" ControlToValidate="partnerDropdown"
                                            Display="Dynamic" Text="Please select a Partner" InitialValue="0"></asp:RequiredFieldValidator>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="specSheetPDFLabel" runat="server" Text="Spec Sheet PDF" CBID="676"
                                    CssClass="label"></asp:Label>
                            </div>
                            <asp:UpdatePanel ID="specSheetUploadUpdatePanel" runat="server" RenderMode="Inline"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="input image">
                                        <div class="fileinputs">
                                            <input type="file" size="5px" id="specSheetFileUpload" class="customInput1 customStyle"
                                                name="productPDF" onchange="return AjaxFileUpload('specSheetFileUpload','pdf');" />
                                        </div>
                                        <div class="imgsrc">
                                            <img id="loading1" src="../Images/async.gif" style="display: none" />
                                            <asp:LinkButton ID="specSheetPdfLinkButton" runat="server" ViewStateMode="Enabled"
                                                OnClick="SpecSheetPdfLinkButton_Click" CssClass="errorText" CausesValidation="false"></asp:LinkButton>
                                            <asp:LinkButton ID="removePdfLinkButton" OnClick="RemovePdfLinkButton_Click" runat="server"
                                                Style="display: none; margin-left: 15px" CssClass="errorText" CausesValidation="false"
                                                Text="X" CBID="752"></asp:LinkButton>
                                        </div>
                                        <span id="pdfErrSpan" class="errorText" style="display: none"></span>
                                        <input type="hidden" id="pdfNameHiddenField" runat="server" value="" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="specSheetPdfLinkButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="productImageLabel" runat="server" Text="Product Image" CBID="532"
                                    CssClass="label"></asp:Label></div>
                            <asp:UpdatePanel ID="imageUploadUpdatePanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="input image">
                                        <p class="imageload">
                                            <img id="loading" src="../Images/async.gif" class="loadingimg" />
                                            <asp:Image ID="productImage" runat="server" CssClass="pic imagestyle" /></p>
                                        <div class="fileinputs">
                                            <input type="file" size="5px" id="ImageUpload" class="customInput customStyle" name="productImage1"
                                                onchange="return AjaxFileUpload('ImageUpload','image');" />
                                        </div>
                                        <div class="imgsrc">
                                            <asp:LinkButton ID="imageLinkButton" CssClass="errorText" runat="server" ViewStateMode="Enabled"
                                                CausesValidation="false" OnClick="ImageLinkButton_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="removeImageLinkButton" OnClick="RemoveImageLinkButton_Click"
                                                runat="server" Style="display: none; margin-left: 15px" CssClass="errorText"
                                                CausesValidation="false" Text="X" CBID="753"></asp:LinkButton>
                                        </div>
                                        <span id="imageErrSpan" class="errorText" style="display: none"></span>
                                        <input type="hidden" id="imageNameHiddenField" runat="server" value="" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="imageLinkButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="clear">
                            </div>
                            <div class="form-spec">
                                <asp:Label ID="companyLogoSizeDescription" runat="server" Text="Images should be JPG or PNG, resized to 235x235 pixels"
                                    CBID="1128"></asp:Label></div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="descriptionLabel" runat="server" Text="Product Description" CBID="533"
                                    CssClass="label"></asp:Label>
                            </div>
                            <div class="input textarea">
                                <asp:TextBox ID="descriptionTextArea" runat="server" TextMode="MultiLine" ClientIDMode="Static">
                                </asp:TextBox>
                                <div class="errorText">
                                    <asp:Label runat="server" ID="remainingCharacters" ClientIDMode="Static"></asp:Label>
                                    <asp:Label runat="server" ID="remainingCharacterLabel" Text="characters remaining"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label ID="websiteLabel" runat="server" Text="Product Website" CBID="541" CssClass="label">
                                </asp:Label>
                            </div>
                            <div class="input textbox3">
                                <asp:TextBox ID="websiteTextBox" runat="server" MaxLength="1000">
                                </asp:TextBox><br />
                                <asp:RegularExpressionValidator ID="websiteRegularExpressionValidator" SetFocusOnError="true"
                                    VMTI="20" runat="server" ErrorMessage="Correct the format" ValidationGroup="SaveProduct"
                                    CssClass="errorText" Display="Dynamic" ControlToValidate="websiteTextBox">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="partnerContactUpdatePanel" runat="server" RenderMode="Inline"
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="box1">
                                <h3>
                                    <asp:Label ID="contactInfoLabel" runat="server" Text="Contact Information" CBID="330"></asp:Label>
                                </h3>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label runat="server" ID="firstNameLabel" CBID="107" Text="First Name"></asp:Label></div>
                                    <div class="input textbox3 mandatory">
                                        <asp:HiddenField ID="contactIdHiddenField" runat="server" />
                                        <asp:TextBox ID="firstNameTextBox" runat="server" MaxLength="256"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="FirstNameRequiredField" Enabled="true" VMTI="1" SetFocusOnError="true"
                                            runat="server" ErrorMessage="Please provide first name" ValidationGroup="SaveProduct"
                                            ControlToValidate="firstNameTextBox" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="firstNameRegularExpressionValidator" runat="server"
                                            ControlToValidate="firstNameTextBox" SetFocusOnError="true" VMTI="2" Enabled="true"
                                            ErrorMessage="Please enter valid name" CssClass="errorText" ValidationGroup="SaveProduct"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label runat="server" ID="lastnameLabel" CBID="119" Text="Last Name"></asp:Label></div>
                                    <div class="input textbox3 mandatory">
                                        <asp:TextBox ID="lastNameTextBox" runat="server" MaxLength="256"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="lastNameRequiredField" VMTI="3" SetFocusOnError="true"
                                            Enabled="true" runat="server" ErrorMessage="Please provide last name" ValidationGroup="SaveProduct"
                                            ControlToValidate="lastNameTextBox" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="lastNameRegularExpressionValidator" SetFocusOnError="true"
                                            VMTI="4" Enabled="true" runat="server" ControlToValidate="lastNameTextBox" ErrorMessage="Please enter valid name"
                                            CssClass="errorText" ValidationGroup="SaveProduct" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label runat="server" ID="emailLabel" CBID="389" Text="Email Address"></asp:Label></div>
                                    <div class="input textbox3 mandatory">
                                        <asp:TextBox ID="emailAddressTextBox" runat="server" MaxLength="123"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="emailRegularExpressionValidator" VMTI="11" SetFocusOnError="true"
                                            Enabled="true" runat="server" CssClass="errorText" ErrorMessage="Correct the format"
                                            ControlToValidate="emailAddressTextBox" ValidationGroup="SaveProduct"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label runat="server" ID="phoneLabel" CBID="132" Text="Phone Number"></asp:Label></div>
                                    <div class="input textbox3 mandatory">
                                        <asp:TextBox ID="phoneTextBox" runat="server" MaxLength="32"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="phoneNumberRequiredField" VMTI="6" SetFocusOnError="true"
                                            Enabled="true" runat="server" ErrorMessage="Please provide phone number" ValidationGroup="SaveProduct"
                                            ControlToValidate="phoneTextBox" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <%--<asp:RangeValidator ID="phoneNumberRangeValidator" VMTI="7" SetFocusOnError="true"
                                            ValidationGroup="SaveProduct" runat="server" ErrorMessage="Please enter numeric value"
                                            ControlToValidate="phoneTextBox" CssClass="errorText" Display="Dynamic" MinimumValue="0"
                                            Type="Double" MaximumValue="999999999999999"></asp:RangeValidator>--%>
                                        <asp:RegularExpressionValidator ID="phoneRegularExpressionValidator" VMTI="7" CssClass="errorText"
                                            ControlToValidate="phoneTextBox" ValidationGroup="SaveProduct" SetFocusOnError="true"
                                            runat="server"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="label">
                                        <asp:Label runat="server" ID="faxLabel" CBID="147" Text="Fax"></asp:Label></div>
                                    <div class="input textbox3 mandatory">
                                        <asp:TextBox ID="faxTextBox" runat="server" MaxLength="32"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="faxRequiredField" VMTI="8" SetFocusOnError="true"
                                            Enabled="true" runat="server" ErrorMessage="Please provide fax number" ValidationGroup="SaveProduct"
                                            ControlToValidate="faxTextBox" CssClass="errorText" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="faxRegularExpressionValidator" SetFocusOnError="true"
                                            ViewStateMode="Enabled" VMTI="9" runat="server" ControlToValidate="faxTextBox"
                                            ErrorMessage="Please enter valid fax" CssClass="errorText" ValidationGroup="SaveProduct"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="partnerDropdown" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="section alR pad-top">
                        <span class="btn-style">
                            <asp:LinkButton ID="saveLinkButton" runat="server" ValidationGroup="SaveProduct"
                                OnClick="SaveLinkButton_Click" Text="Save" CBID="641">
                            </asp:LinkButton>
                        </span>
                    </div>
                </div>
                <div class="rgt-col">
                    <div id="request-details" class="box1 section1">
                        <h3>
                            <asp:Label runat="server" ID="requestDetailsLabel" CBID="621" Text="Request Details">
                            </asp:Label></h3>
                        <div class="row ">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="statusLabel" CBID="698" Text="Status">
                                </asp:Label></div>
                            <div class="input no-pad select">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tb1">
                                    <tr>
                                        <td width="76%">
                                            <asp:DropDownList ID="statusDropDown" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="statusRequiredFieldValidator" VMTI="39" SetFocusOnError="true"
                                                runat="server" ErrorMessage="Please select a Status" ValidationGroup="SaveProduct"
                                                ControlToValidate="statusDropDown" CssClass="errorText" Display="Dynamic" InitialValue="0">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="requestDateLabel" CBID="620" Text="Request Date">
                                </asp:Label></div>
                            <div class="input">
                                <asp:Label CssClass="label" runat="server" ID="requestDateTextBox"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="requestedByHeadLabel" CBID="625" Text="Requested By">
                                </asp:Label>
                            </div>
                            <div class="input">
                                <asp:HiddenField ID="requestedByIdHiddenField" runat="server" />
                                <asp:HiddenField ID="productIdHiddenField" runat="server" Value="0" />
                                <asp:Label ID="requestedByLabel" runat="server" class="fname"></asp:Label>
                                <asp:LinkButton ID="requestedByLinkButton" runat="server" OnClick="RequestedByLinkButton_Click"
                                    class="fname" ViewStateMode="Enabled">
                                </asp:LinkButton>
                                <asp:Label ID="partnerNameLabel" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="box1">
                        <h3>
                            <asp:Label runat="server" ID="designReviewLabel" CBID="364" Text="Design Review"></asp:Label></h3>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="personLabel" CBID="513" Text="Person">
                                </asp:Label></div>
                            <div class="input textbox3" runat="server" id="personDesignReviewDiv">
                                <asp:TextBox ID="personTextBox" runat="server" MaxLength="123">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="commentsLabel" CBID="299" Text="Comments">
                                </asp:Label></div>
                            <div class="input no-pad">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tb1">
                                    <tr>
                                        <td width="76%">
                                            <asp:TextBox ID="commentsTextBox" runat="server" TextMode="MultiLine" MaxLength="512">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box1">
                        <h3>
                            <asp:Label CssClass="label" runat="server" ID="technicalReviewLabel" CBID="708" Text="Technical Review">
                            </asp:Label></h3>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="techReviewPersonLabel" CBID="513"
                                    Text="Person"></asp:Label></div>
                            <div class="input textbox3" runat="server" id="personTechReviewDiv">
                                <asp:TextBox ID="personTechReviewTextBox" runat="server" MaxLength="123">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="techReviewCommentsLabel" CBID="299"
                                    Text="Comments"></asp:Label></div>
                            <div class="input no-pad">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tb1">
                                    <tr>
                                        <td width="76%">
                                            <asp:TextBox ID="commentsTechReviewTextBox" runat="server" TextMode="MultiLine" MaxLength="512">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box1">
                        <h3>
                            <asp:Label CssClass="label" runat="server" ID="adminReviewLabel" CBID="197" Text="Admnin Review">
                            </asp:Label></h3>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="adminReviewPersonLabel" CBID="513"
                                    Text="Person"></asp:Label></div>
                            <div class="input textbox3" runat="server" id="personAdminReviewDiv">
                                <asp:TextBox ID="personAdminReviewTextBox" runat="server" MaxLength="123">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <asp:Label CssClass="label" runat="server" ID="adminReviewCommentsLabel" CBID="299"
                                    Text="Comments"></asp:Label>
                            </div>
                            <div class="input no-pad">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tb1">
                                    <tr>
                                        <td width="76%">
                                            <asp:TextBox ID="commentsAdminTextBox" runat="server" TextMode="MultiLine" MaxLength="512">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="specificationUpdatePanel" runat="server" RenderMode="Inline"
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="box" id="specifications">
                                <h3>
                                    <asp:Label CssClass="label" runat="server" ID="assignedCategoriesPanelLabel" Text="Assigned Categories"></asp:Label>
                                </h3>
                                <div class="box-content">
                                    <asp:Repeater ID="productCategoryRepeater" runat="server" OnItemDataBound="ProductCategoryRepeater_ItemDataBound">
                                        <ItemTemplate>
                                            <table cellspacing="0" cellpadding="0" border="1" style="width: 100%; border-collapse: collapse;"
                                                class="col-3" rules="all">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="4" class="col-1" scope="col">
                                                            <asp:Label ID="categorySubCategoryLabel" Text='<%# DataBinder.Eval(Container.DataItem, "CategoryName") %>'
                                                                runat="server"></asp:Label>
                                                        </th>
                                                    </tr>
                                                    <asp:Repeater ID="assignedSpecificationsRepeater" runat="server" OnItemDataBound="AssignedSpecificationsRepeater_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td class="col-2">
                                                                    <asp:Label ID="propertyLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BrandDescription") %>'></asp:Label>
                                                                </td>
                                                                <td class="col-2">
                                                                    <asp:Label ID="propertyRegionLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RegionDescription") %>'></asp:Label>
                                                                </td>
                                                                <td class="col-2">
                                                                    <asp:Label ID="propertyCountryLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CountryDescription") %>'></asp:Label>
                                                                </td>
                                                                <td class="col-1">
                                                                    <asp:ImageButton ID="deletePropertyImageButton" runat="server" OnClick="DeletePropertyImageButton_Click"
                                                                        ImageUrl="../Images/cross.gif" CausesValidation="false" Width="10" Height="10" />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody> </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <div class="category-fields">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="categoryPlaceHolder" runat="server">
                                                        <asp:DropDownList ID="categoriesDropDown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DynamicCategoryDropDown_OnSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td style="width: 23%">
                                                    <asp:Label ID="Label1" runat="server" Visible="false" CssClass="errorText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="brandDropDown" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RangeValidator ID="categoryDropDownValidator" VMTI="27" ErrorMessage="Please select a Category"
                                                        ControlToValidate="categoriesDropDown" runat="server" ValidationGroup="ValidationGp1"
                                                        MinimumValue="1" MaximumValue="100000000" Type="Integer" Display="Dynamic" CssClass="errorText" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="err-msg">
                                        <asp:Label ID="errorMsgLabel" runat="server" Visible="false" CssClass="errorText"></asp:Label>
                                    </div>
                                    <table border="0" cellpadding="0" cellspacing="0" class="category-table">
                                        <tr>
                                            <td class="col-1">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="regionDropDown" ViewStateMode="Enabled" multiple="multiple"
                                                                runat="server" ClientIDMode="Static">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="regionCollection" runat="server" ClientIDMode="Static" />
                                                            <asp:HiddenField ID="isAllRegionChecked" runat="server" ClientIDMode="Static" Value="0" />
                                                            <asp:TextBox runat="server" ID="selectedRegions" ClientIDMode="Static" Style="display: none;"
                                                                AutoPostBack="true" OnTextChanged="SelectedRegions_TextChanged"></asp:TextBox>
                                                            <br />
                                                        </td>
                                                        <td>
                                                            <asp:UpdatePanel ID="dropDownUpdatePanel" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ViewStateMode="Enabled" ID="countryDropDown" multiple="multiple"
                                                                        ClientIDMode="Static" runat="server">
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox runat="server" ID="selectedCountries" ClientIDMode="Static" Style="display: none;"></asp:TextBox>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="selectedRegions" EventName="TextChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="col-3">
                                                <span class="btn-style">
                                                    <asp:LinkButton ID="addSpecificationLinkButton" ValidationGroup="ValidationGp1" runat="server"
                                                        Text="Add" CBID="174" OnClick="AddSpecificationLinkButton_Click"></asp:LinkButton>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
