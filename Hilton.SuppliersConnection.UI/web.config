<?xml version="1.0"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=169433
  -->
<configuration>
  <configSections>
    <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" requirePermission="true"/>
    <section name="exceptionHandling" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" requirePermission="true"/>
    <section name="dataConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Data.Configuration.DatabaseSettings, Microsoft.Practices.EnterpriseLibrary.Data, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" requirePermission="true"/>
    <sectionGroup name="applicationSettings" type="System.Configuration.ApplicationSettingsGroup, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089">
      <section name="Hilton.SuppliersConnection.UI.Properties.Settings" type="System.Configuration.ClientSettingsSection, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false"/>
    </sectionGroup>
  </configSections>
  <loggingConfiguration name="" tracingEnabled="true" defaultCategory="General">
    <listeners>
      <add name="Event Log Listener" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.FormattedEventLogTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.FormattedEventLogTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" source="Enterprise Library Logging" formatter="Text Formatter" log="SupplierConnectionLog" machineName="." traceOutputOptions="DateTime, ProcessId, Callstack"/>
      <add name="Database Trace Listener" type="Microsoft.Practices.EnterpriseLibrary.Logging.Database.FormattedDatabaseTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging.Database, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Database.Configuration.FormattedDatabaseTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging.Database, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" databaseInstanceName="DatabaseConnectionString" writeLogStoredProcName="WriteLog" addCategoryStoredProcName="AddLoggingCategory" formatter="Text Formatter" traceOutputOptions="LogicalOperationStack, DateTime, Timestamp, ProcessId, ThreadId, Callstack"/>
      <add name="Rolling Flat File Trace Listener" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.RollingFlatFileTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.RollingFlatFileTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" fileName="D:\SuppliersConnection_Deploy\SuppliersConnectionSIT\Log\Trace.log" formatter="Text Formatter" rollSizeKB="500" traceOutputOptions="LogicalOperationStack, DateTime, Timestamp, ProcessId, ThreadId, Callstack"/>
    </listeners>
    <formatters>
      <add type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" template="Timestamp: {timestamp}{newline}
Message: {message}{newline}
Category: {category}{newline}
Priority: {priority}{newline}
EventId: {eventid}{newline}
Severity: {severity}{newline}
Title:{title}{newline}
Machine: {localMachine}{newline}
App Domain: {localAppDomain}{newline}
ProcessId: {localProcessId}{newline}
Process Name: {localProcessName}{newline}
Thread Name: {threadName}{newline}
Win32 ThreadId:{win32ThreadId}{newline}
Extended Properties: {dictionary({key} - {value}{newline})}" name="Text Formatter"/>
    </formatters>
    <categorySources>
      <add switchValue="All" name="General">
        <listeners>
          <add name="Database Trace Listener"/>
          <add name="Rolling Flat File Trace Listener"/>
        </listeners>
      </add>
    </categorySources>
    <specialSources>
      <allEvents switchValue="All" name="All Events"/>
      <notProcessed switchValue="All" name="Unprocessed Category"/>
      <errors switchValue="All" name="Logging Errors &amp; Warnings">
        <listeners>
          <add name="Database Trace Listener"/>
          <add name="Rolling Flat File Trace Listener"/>
        </listeners>
      </errors>
    </specialSources>
  </loggingConfiguration>
  <exceptionHandling>
    <exceptionPolicies>
      <add name="DataAccessPolicy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add name="DataAccessLoggingHandler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" logCategory="General" eventId="100" severity="Error" title="Enterprise Library Exception Handling" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" priority="0"/>
              <add name="Wrap Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WrapHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" exceptionMessage="An unknown error has occurred in Data Access Layer while processing your request." wrapExceptionType="Hilton.SuppliersConnection.Infrastructure.DataAccessException, Hilton.SuppliersConnection.Infrastructure, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
      <add name="DataAccessCustomPolicy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="NotifyRethrow">
            <exceptionHandlers>
              <add name="Wrap Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WrapHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" wrapExceptionType="Hilton.SuppliersConnection.Infrastructure.DataAccessCustomException, Hilton.SuppliersConnection.Infrastructure, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
      <add name="PassThroughPolicy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="NotifyRethrow">
            <exceptionHandlers>
              <add name="PassThroughWrapHandler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WrapHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" wrapExceptionType="Hilton.SuppliersConnection.Infrastructure.PassThroughException, Hilton.SuppliersConnection.Infrastructure, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
      <add name="BusinessLogicPolicy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add name="BusinessLogicLoggingHandler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" logCategory="General" eventId="100" severity="Error" title="Enterprise Library Exception Handling" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" priority="0"/>
              <add name="BusinessLogicWrapHandler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WrapHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" exceptionMessage="An unknown error has occurred in Business Logic Layer while processing your request. Please contract Help Desk Support at X-XXX-XXX-XXXX with Error Token ID {handlingInstanceID}." wrapExceptionType="Hilton.SuppliersConnection.Infrastructure.BusinessLogicException, Hilton.SuppliersConnection.Infrastructure, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
      <add name="BusinessLogicCustomPolicy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="NotifyRethrow">
            <exceptionHandlers>
              <add name="Wrap Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WrapHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" wrapExceptionType="Hilton.SuppliersConnection.Infrastructure.BusinessLogicCustomException, Hilton.SuppliersConnection.Infrastructure, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
      <add name="UserInterfacePolicy">
        <exceptionTypes>
          <add name="All Exceptions" type="System.Exception, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add name="UserInterfaceLoggingHandler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" logCategory="General" eventId="100" severity="Error" title="Enterprise Library Exception Handling" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" priority="0"/>
              <add name="Wrap Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WrapHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" exceptionMessage="An error occord at front end. please check." wrapExceptionType="Hilton.SuppliersConnection.Infrastructure.UserInterfaceException, Hilton.SuppliersConnection.Infrastructure, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
    </exceptionPolicies>
  </exceptionHandling>
  <dataConfiguration defaultDatabase="DatabaseConnectionString"/>
  <connectionStrings>
    <!--<add name="DatabaseConnectionString" connectionString="Data Source=10.81.47.24;Initial Catalog=SuppliersConnectionSit;User Id=SCApp;Password=P@ssW0rd" providerName="System.Data.SqlClient"/>-->
    <!--<add name="DatabaseConnectionString" connectionString="Data Source=SuppliersConnectionDevDB.hilton.com,53103;Initial Catalog=SupplierConnectionTest;User Id=bI+V8MiJSaNAWJ382sJVLw==;Password=EOPW2X96KQFP4YnOBGec8Q==" providerName="System.Data.SqlClient"/>-->
    <add name="DatabaseConnectionString" connectionString="Data Source=localhost;Initial Catalog=SuppliersConnection;User Id=bI+V8MiJSaNAWJ382sJVLw==;Password=EOPW2X96KQFP4YnOBGec8Q==" providerName="System.Data.SqlClient" />
  </connectionStrings>
  <appSettings>
    <add key="aspnet:MaxHttpCollectionKeys" value="5000"/>
    <add key="UseSSL" value="false"/>
    <add key="OnQInsiderHomePage" value="https://onqinsider.hilton.com/Insider/"/>
	<add key="SAMLLogin" value="https://fd.hilton.com/idp/startSSO.ping?PartnerSpId=SuppliersConnection"/>
    <add key="OnQInsiderLoginPage" value="https://onqinsider.hilton.com/Insider/onqlogin/"/>
    <add key="SuppliersConnection_WebServiceAccount" value="svcSuppConn"/>
    <add key="SuppliersConnection_WebServicePwd" value="SuP1i3rc@n"/>
    <add key="ApplicationURL" value="/Authenticate.aspx?SSORedirect="/>
    <add key="ChartImageHandler" value="storage=memory;deleteAfterServicing=true;"/>
    <add key="PaymentGatewayUrl" value="https://api.demo.convergepay.com/VirtualMerchantDemo/process.do"/>
    <add key="SessionTimeoutPopup" value="58"/>
	  <add key="ContentBlockAbsoluteExpiration" value="0"/>
	  <add key="culture" value="en-US"/>
    <!--Set 58 min here-->
    <add key="EncryptionDecryptionKey" value="SCEncrypt"/>
    <!--Dev-->
    <!--<add key="GoogleAnalyticsId" value="UA-66328623-3"/>-->
    <!--Stage-->
    <add key="GoogleAnalyticsId" value="UA-66328623-2"/>
    <!--Prod-->
    <!--<add key="GoogleAnalyticsId" value="UA-66328623-1"/>-->
    <add key="Culture" value="en-US"/>
  </appSettings>
  <system.webServer>
    <handlers>
      <remove name="ChartImageHandler"/>
      <add name="ChartImageHandler" preCondition="integratedMode" verb="GET,HEAD,POST" path="ChartImg.axd" type="System.Web.UI.DataVisualization.Charting.ChartHttpHandler, System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
    </handlers>
    <staticContent>
      <clientCache cacheControlMaxAge="7.00:00:00" cacheControlMode="UseMaxAge"/>
    </staticContent>
    <defaultDocument>
      <files>
        <add value="Home.aspx"/>
      </files>
    </defaultDocument>
    <urlCompression doDynamicCompression="true"/>
	  <httpProtocol>
		  <customHeaders>
			  <remove name="X-Powered-By"/>
		  </customHeaders>
	  </httpProtocol>
  </system.webServer>
  <system.web>
    <compilation targetFramework="4.0" debug="true">
      <assemblies>
        <add assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
      </assemblies>
    </compilation>
    <sessionState allowCustomSqlDatabase="true" mode="SQLServer" timeout="60" compressionEnabled="true" regenerateExpiredSessionId="false" sqlConnectionString="Data Source=localhost;Initial Catalog=SuppliersConnection;User Id=bI+V8MiJSaNAWJ382sJVLw==;Password=EOPW2X96KQFP4YnOBGec8Q==" cookieless="false" partitionResolverType="Hilton.SuppliersConnection.DataAccess.MyPartitionResolver"/>
    <httpRuntime requestValidationMode="2.0" maxRequestLength="20480" enableVersionHeader="false"/>
	  
  </system.web>
  <system.web>
    <httpHandlers>
      <add path="ChartImg.axd" verb="GET,HEAD,POST" type="System.Web.UI.DataVisualization.Charting.ChartHttpHandler, System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" validate="false"/>
    </httpHandlers>
    <pages>
      <controls>
        <add tagPrefix="asp" namespace="System.Web.UI.DataVisualization.Charting" assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
      </controls>
    </pages>
  </system.web>
  <applicationSettings>
    <Hilton.SuppliersConnection.UI.Properties.Settings>
      <setting name="Hilton_SuppliersConnection_UI_PortalSecurityService_clsPortalSecurityService" serializeAs="String">
        <value>https://onqinsiderservices.hilton.com/portalsecurityservice.asmx</value>
      </setting>
    </Hilton.SuppliersConnection.UI.Properties.Settings>
  </applicationSettings>
	<system.web.extensions>
		<scripting>
			<webServices>
				<jsonSerialization maxJsonLength="2147483647"/>
			</webServices>
		</scripting>
	</system.web.extensions>
	<runtime>
		<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
			<dependentAssembly>
				<assemblyIdentity name="Microsoft.Practices.Unity" publicKeyToken="31BF3856AD364E35" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-2.0.414.0" newVersion="2.0.414.0"/>
			</dependentAssembly>
		</assemblyBinding>
	</runtime>
</configuration>
