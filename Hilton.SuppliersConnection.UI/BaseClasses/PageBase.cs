﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.Utilities;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Xml;
using System.Security;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Pkcs;

namespace Hilton.SuppliersConnection.UI.BaseClasses
{
    public class PageBase : Page
    {
        public string GoogleAnalyticsId
        {
            get
            {
                return ConfigurationManager.AppSettings[AppSettingConstants.GoogleAnalyticsId] == null ? string.Empty
                    : ConfigurationManager.AppSettings[AppSettingConstants.GoogleAnalyticsId].ToString();
            }
        }


        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        protected bool IsSessionTimedout()
        {
            if (Context.Session != null)
            {
                if (Session.IsNewSession)
                {
                    string szCookieHeader = Request.Headers["Cookie"];
                    if ((null != szCookieHeader) && (szCookieHeader.IndexOf("ASP.NET_SessionId", StringComparison.InvariantCultureIgnoreCase) >= 0))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private string userDesc = string.Empty;
        protected string userId = string.Empty;
        private string userNumber = string.Empty;
        private string userType = string.Empty;
        private string userEmail = string.Empty;
        private string firstName = string.Empty;
        private string lastName = string.Empty;
        private string employeeId = string.Empty;

        // TO-DO - If culture need to be set
        //protected static CultureInfo culture = new CultureInfo("en-US");
        protected static CultureInfo culture = new CultureInfo(ConfigurationStore.GetAppSetting(AppSettingConstants.Culture));

        // private static CultureInfo culture = System.Threading.Thread.CurrentThread.CurrentUICulture;
        //protected static CultureInfo culture = null;

        //Translation attribute
        private static string translateAttribute = "CBID";

        private static string validationMsgTextAttribute = "VMTI";
        private ISecurityManager _securityManager;

        public static PageBase Handler { get { return HttpContext.Current.Handler as PageBase; } }

        public void OpenDialog(string url, string name, int width, int height)
        {
            OpenDialog(url, name, width, height, false, false, true, true, true);
        }

        /// <summary>
        ///
        /// </summary>
        protected void SupressPageHeader()
        {
            var menu = Page.Master.FindControl("menuBar");
            if (menu != null)
            {
                menu.Visible = false;
            }

            var loginLink = Page.Master.FindControl("loginLinkButton");
            if (loginLink != null)
            {
                loginLink.Visible = false;
            }

            var adminLinkButtion = Page.Master.FindControl("adminLinkButton");
            if (adminLinkButtion != null)
            {
                adminLinkButtion.Visible = false;
            }

            var barImage = Page.Master.FindControl("barImage");
            if (barImage != null)
            {
                barImage.Visible = false;
            }

            var userNameLabel = Page.Master.FindControl("userNameLabel");
            if (userNameLabel != null)
            {
                userNameLabel.Visible = false;
            }
            var lblcompany = Page.Master.FindControl("lblCompany");
            if (lblcompany != null)
            {
                lblcompany.Visible = false;
            }
            var companyDropdown = Page.Master.FindControl("userPartnersDropDown");
            if (companyDropdown != null)
            {
                companyDropdown.Visible = false;
            }
        }

        /// <summary>
        /// This method validate user permission accessibility
        /// </summary>
        protected void ValidateUserAccess(int menuId)
        {
            try
            {
                if (!IsSessionTimedout())
                {
                    ISecurityManager _securityManager = Hilton.SuppliersConnection.Business.SecurityManager.Instance;
                    User user = GetSession<User>(SessionConstants.User);
                    if (user != null)
                    {
                        var isScreenAuthorized = _securityManager.ValidateScreenAuthorization(user.MenuAccess, menuId);
                        if (!isScreenAuthorized)
                        {
                            Response.Redirect("~/AccessDenied.aspx", false);
                        }
                    }
                    else
                    {
                        Response.Redirect("~/AccessDenied.aspx", false);
                    }
                }
                else
                {
                    // Redirect to session timeout page or to home page. Currently redirecting to home page
                    Response.Redirect("~/", false);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        ///
        /// </summary>
        protected bool ValidateAdminControlAuthorization(int permissionId)
        {
            ISecurityManager _securityManager = Hilton.SuppliersConnection.Business.SecurityManager.Instance;
            User user = GetSession<User>(SessionConstants.User);
            if (user != null)
            {
                return _securityManager.ValidateAdminControlAuthorization(user.Permissions, permissionId);
            }
            else
                return false;
        }

        /// <summary>
        ///
        /// </summary>
        protected bool ValidateMenuAuthorization(int menuId)
        {
            ISecurityManager _securityManager = Hilton.SuppliersConnection.Business.SecurityManager.Instance;
            IList<UserPermissions> userMenuCollection = GetSession<IList<UserPermissions>>(SessionConstants.WebMenu);

            if (userMenuCollection != null && userMenuCollection.Count > 0)
            {
                return _securityManager.ValidateMenuAuthorization(userMenuCollection, menuId);
            }
            else
                return false;
        }

        /// <summary>
        ///
        /// </summary>
        protected int GetLoggedinUserId()
        {
            User loggedinuser = GetSession<User>(SessionConstants.User);
            if (loggedinuser != null)
            {
                return loggedinuser.UserId;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="url"></param>
        /// <param name="name"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="showToolbar"></param>
        /// <param name="showMenuBar"></param>
        /// <param name="showScrollbars"></param>
        /// <param name="showStatus"></param>
        /// <param name="resizable"></param>
        public void OpenDialog(string url, string name, int width, int height, bool showToolbar, bool showMenuBar, bool showScrollbars, bool showStatus, bool resizable)
        {
            string jsToolbar = showToolbar ? "Yes" : "No";
            string jsMenubar = showMenuBar ? "Yes" : "No";
            string jsScrollbars = showScrollbars ? "Yes" : "No";
            string jsStatus = showStatus ? "Yes" : "No";
            string jsResizable = resizable ? "Yes" : "No";

            // Convert relative links
            if (url.StartsWith("~", StringComparison.InvariantCultureIgnoreCase))
            {
                url = ResolveUrl(url);
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "os", "OpenPopup(\"" + url + "\",\"" + name + "\",\"" + width.ToString(CultureInfo.InvariantCulture) + "\",\"" + height.ToString(CultureInfo.InvariantCulture) + "\",\"" + jsToolbar + "\",\"" + jsScrollbars + "\",\"" + jsStatus + "\",\"" + jsMenubar + "\",\"" + jsResizable + "\");", true);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoadComplete(EventArgs e)
        {
            User loggedInUser = (User)Session[SessionConstants.User];
            if (loggedInUser != null && Request.RawUrl != "/TermsAndCondition.aspx" && (loggedInUser.UserTypeId == (int)UserTypeEnum.Partner || loggedInUser.UserTypeId == (int)UserTypeEnum.Owner || loggedInUser.UserTypeId == (int)UserTypeEnum.Consultant) && loggedInUser.ShowTermsAndCondition && !string.IsNullOrEmpty(loggedInUser.TermsAndConditionVersion))
            {
                Response.Redirect("~/TermsAndCondition.aspx", false);
            }
            else if (this.Master != null)
            {
                System.Web.UI.WebControls.Menu menu = this.Master.FindControl("menuBar") as System.Web.UI.WebControls.Menu;
                if (menu != null)
                {
                    SelectMenuItem(menu.Items);
                }
                System.Web.UI.WebControls.Menu adminMenu = this.Master.FindControl("adminMenuBar") as System.Web.UI.WebControls.Menu;
                if (adminMenu != null)
                {
                    SelectAdminMenuItem(adminMenu.Items);
                }
            }
            base.OnLoadComplete(e);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="menuItems"></param>
        /// <returns></returns>
        private bool SelectMenuItem(MenuItemCollection menuItems)
        {
            foreach (MenuItem item in menuItems)
            {
                string url = ResolveUrl(item.NavigateUrl);
                string pageUrl = Request.RawUrl;

                pageUrl = SetPageUrl(pageUrl);

                if (pageUrl.Equals(url, StringComparison.InvariantCultureIgnoreCase))
                {
                    MenuItem mp = item.Parent;
                    if (mp != null)
                    {
                        mp.Selected = true;
                    }
                    else
                    {
                        item.Selected = true;
                    }
                    return true;
                }

                if (SelectMenuItem(item.ChildItems))
                    return true;
            }

            return false;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="menuItems"></param>
        /// <returns></returns>
        private bool SelectAdminMenuItem(MenuItemCollection menuItems)
        {
            foreach (MenuItem item in menuItems)
            {
                string url = ResolveUrl(item.Value);
                string pageUrl = Request.RawUrl;

                pageUrl = SetPageUrl(pageUrl);

                if (pageUrl.Equals(url, StringComparison.InvariantCultureIgnoreCase))
                {
                    MenuItem mp = item.Parent;
                    if (mp != null)
                    {
                        mp.Selected = true;
                    }
                    else
                    {
                        item.Selected = true;
                    }
                    return true;
                }
                if (pageUrl.Contains("?"))
                {
                    if (pageUrl.Contains("/Admin/Partners.aspx") && url.Contains("/Admin/Partners.aspx"))
                    {
                        MenuItem mp = item.Parent;
                        if (mp != null)
                        {
                            mp.Selected = true;
                        }
                        else
                        {
                            item.Selected = true;
                        }
                        return true;
                    }
                }

                if (SelectMenuItem(item.ChildItems))
                    return true;
            }

            return false;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="url"></param>
        /// <param name="pageUrl"></param>
        /// <returns></returns>
        public string SetPageUrl(string pageUrl)
        {
            if (pageUrl == "/Admin/ContentBlocks.aspx" || pageUrl == "/Admin/ContentBlockDetails.aspx")
                pageUrl = "/Admin/Settings.aspx";
            else if (pageUrl == "/Admin/ProductDetails.aspx")
                pageUrl = "/Admin/Products.aspx";
            else if (pageUrl == "/Admin/ProjectTemplateDetails.aspx")
                pageUrl = "/Admin/ProjectTemplates.aspx";
            else if (pageUrl == "/Admin/ProjectDetails.aspx")
                pageUrl = "/Admin/Projects.aspx";
            else if (pageUrl == "/Admin/PartnerDetails.aspx")
                pageUrl = "/Admin/Partners.aspx";
            else if (pageUrl == "/Admin/UserDetails.aspx")
                pageUrl = "/Admin/Users.aspx";
            else if (pageUrl == "/Admin/TransactionDetails.aspx")
                pageUrl = "/Admin/Transactions.aspx";
            else if (pageUrl == "/Partners/PartnerProfile.aspx")
                pageUrl = "/Partners/RecommendedPartners.aspx";
            else if (pageUrl == "/Account/AddEditProduct.aspx")
                pageUrl = "/Account/MyProducts.aspx";
            else if (pageUrl == "/Account/ProductSubmission.aspx")
                pageUrl = "/Account/MyProducts.aspx";
            else if (pageUrl == "/Admin/ConstructionReport.aspx")
                pageUrl = "/Admin/ConstructionReport.aspx";

            return pageUrl;
        }

        /// <summary>
        /// This property return the sort direction structure
        /// </summary>
        protected SortDirection GridViewSortDirection
        {
            get
            {
                if (GetViewState(ViewStateConstants.SortDirection) == null)
                    SetViewState(ViewStateConstants.SortDirection, SortDirection.Ascending);
                return (SortDirection)GetViewState(ViewStateConstants.SortDirection);
            }
            set
            {
                SetViewState(ViewStateConstants.SortDirection, value);
            }
        }

        /// To find control in dynamically created controls
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Control FindControl(Control parent, string id)
        {
            foreach (Control control in parent.Controls)
            {
                if (control.ID == id)
                {
                    return control;
                }
                var childResult = FindControl(control, id);
                if (childResult != null)
                {
                    return childResult;
                }
            } return null;
        }

        /// <summary>
        /// This method will return the auto complete list for specified list type ie Section, Title etc
        /// </summary>
        /// <param name="listType"></param>
        /// <returns></returns>
        public static Collection<KeyValuePair<string, string>> GetAutoCompleteList(string listType)
        {
            DynamicDropDownData dynamicData;
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.DynamicDataAbsoluteExpiration);
            if (cacheAbsoluteExpiration == "0")
            {
                Caching.Clear(CacheConstants.DynamicData);
            }

            if (Caching.Exists(CacheConstants.DynamicData))
            {
                dynamicData = Caching.Get<DynamicDropDownData>(CacheConstants.DynamicData);
            }
            else
            {
                dynamicData = new DynamicDropDownData();
                HelperManager.Instance.GetAutoCompleteList(dynamicData);
                Caching.Clear(CacheConstants.DynamicData);
                if (cacheAbsoluteExpiration != "0")
                    Caching.Add<DynamicDropDownData>(dynamicData, CacheConstants.DynamicData, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
            }

            switch (listType)
            {
                case AutoCompleteListConstants.SectionAutoCompleteList:
                    return dynamicData.SectionDataList;
                case AutoCompleteListConstants.TitleAutoCompleteList:
                    return dynamicData.TitleDataList;
                default:
                    return null;
            }
        }

        /// <summary>
        ///  This method will return the dynamic dropdown item collection like Section etc
        /// </summary>
        /// <param name="dropDownType"></param>
        /// <returns></returns>
        public static IList<DropDownItem> GetDynamicDropDownDataSource(string dropDownType)
        {
            DynamicDropDown dropDown;
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.DynamicDropDownAbsoluteExpiration);
            if (cacheAbsoluteExpiration == "0")
            {
                Caching.Clear(CacheConstants.DynamicDropDown);
            }

            if (Caching.Exists(CacheConstants.DynamicDropDown))
            {
                dropDown = Caching.Get<DynamicDropDown>(CacheConstants.DynamicDropDown);
            }
            else
            {
                dropDown = new DynamicDropDown();
                HelperManager.Instance.GetDynamicDropDownList(dropDown);
                Caching.Clear(CacheConstants.DynamicDropDown);
                if (cacheAbsoluteExpiration != "0")
                    Caching.Add<DynamicDropDown>(dropDown, CacheConstants.DynamicDropDown, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
            }

            switch (dropDownType)
            {
                case DropDownConstants.SectionDropDown:
                    return dropDown.SectionDropDown;
                case DropDownConstants.FeedbackPartnerDropDown:
                    return dropDown.FeedbackPartnerDropDown;
                case DropDownConstants.SenderDropDown:
                    return dropDown.SenderDropDown;
                case DropDownConstants.RecipientDropDown:
                    return dropDown.RecipientDropDown;
                case DropDownConstants.MessageRegionDropDown:
                    return dropDown.MessageRegionDropDown;
                case DropDownConstants.PartnerDropDown:
                    return dropDown.PartnerDropDown;
                case DropDownConstants.ActivePartnersDropDown:
                    return dropDown.ActivePartnersDropDown;
                case DropDownConstants.TermsAndConditions:
                    return dropDown.TermsAndConditions;
                default:
                    return null;
            }
        }

        /// <summary>
        /// This method will return the static dropdown item collection like language
        /// </summary>
        /// <param name="dropDownType"></param>
        /// <returns></returns>
        public static IList<DropDownItem> GetStaticDropDownDataSource(string dropDownType)
        {
            StaticDropDown dropDown;
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.StaticDropDownAbsoluteExpiration);
            if (cacheAbsoluteExpiration == "0")
            {
                Caching.Clear(CacheConstants.StaticDropDown);
            }

            if (Caching.Exists(CacheConstants.StaticDropDown))
            {
                dropDown = Caching.Get<StaticDropDown>(CacheConstants.StaticDropDown);
            }
            else
            {
                dropDown = new StaticDropDown();
                HelperManager.Instance.GetStaticDropDownList(dropDown);
                Caching.Clear(CacheConstants.StaticDropDown);
                if (cacheAbsoluteExpiration != "0")
                    Caching.Add<StaticDropDown>(dropDown, CacheConstants.StaticDropDown, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
            }

            switch (dropDownType)
            {
                case DropDownConstants.LanguageDropDown:
                    return dropDown.LanguageDropDown;
                case DropDownConstants.RatingDropDown:
                    return dropDown.RatingDropDown;
                case DropDownConstants.FeedbackStatusDropDown:
                    return dropDown.FeedbackStatusDropDown;
                case DropDownConstants.BrandDropDown:
                    return dropDown.BrandDropDown;
                case DropDownConstants.RegionDropDown:
                    return dropDown.RegionDropDown;
                case DropDownConstants.PropertyTypesDropDown:
                    return dropDown.PropertyTypesDropDown;
                case DropDownConstants.ProductStatusDropDown:
                    return dropDown.ProductStatusDropDown;
                case DropDownConstants.MessageStatusDropDown:
                    return dropDown.MessageStatusDropDown;
                case DropDownConstants.UserTypesDropDown:
                    return dropDown.UserTypesDropDown;
                case DropDownConstants.UserStatusDropDown:
                    return dropDown.UserStatusDropDown;
                case DropDownConstants.PartnerStatusDropDown:
                    return dropDown.PartnerStatusDropDown;
                case DropDownConstants.ProjectStatusDropDown:
                    return dropDown.ProjectStatusDropDown;
                case DropDownConstants.ProjectTypesDropDown:
                    return dropDown.ProjectTypesDropDown;
                case DropDownConstants.CountryDropDown:
                    return dropDown.CountryDropDown;
                case DropDownConstants.ProjectTemplateStatusDropDown:
                    return dropDown.ProjectTemplateStatusDropDown;
                case DropDownConstants.StateDropDown:
                    return dropDown.StateDropDown;
                case DropDownConstants.AdminPermissionsDropDown:
                    return dropDown.AdminPermissionsDropDown;
                case DropDownConstants.OwnerAndConsultantRolesDropDown:
                    return dropDown.OwnerAndConsultantRolesDropDown;
                case DropDownConstants.PartnershipStatusDropDown:
                    return dropDown.PartnershipStatusDropDown;
                case DropDownConstants.PartnershipTypeDropDown:
                    return dropDown.PartnershipTypeDropDown;
                case DropDownConstants.PaymentTypesDropDown:
                    return dropDown.PaymentTypesDropDown;
                case DropDownConstants.TransactionStatusDropDown:
                    return dropDown.TransactionStatusDropDown;
                case DropDownConstants.UserProjectMappingTypeDropDown:
                    return dropDown.UserProjectMappingTypeDropDown;
                case DropDownConstants.ConsultantRoleDropDown:
                    return dropDown.ConsultantRoleDropDown;
                default:
                    return null;
            }
        }

        /// <summary>
        /// This method check for existence of key in auto complete list
        /// </summary>
        /// <param name="listType"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool IsAutoCompleteListContainsKey(string listType, string key)
        {
            if (Caching.Exists(CacheConstants.DynamicData))
            {
                DynamicDropDownData dynamicData = Caching.Get<DynamicDropDownData>(CacheConstants.DynamicData);
                IList<KeyValuePair<string, string>> autoCompleteList = null;
                switch (listType)
                {
                    case AutoCompleteListConstants.SectionAutoCompleteList:
                        autoCompleteList = dynamicData.SectionDataList;
                        break;

                    case AutoCompleteListConstants.TitleAutoCompleteList:
                        autoCompleteList = dynamicData.TitleDataList;
                        break;
                }
                if (autoCompleteList != null)
                {
                    KeyValuePair<string, string>? keyValuePair = autoCompleteList.Where(x => x.Key == key).FirstOrDefault();
                    if (keyValuePair.Value.Equals(default(KeyValuePair<string, string>)))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// This method will update the auto complete list for specified list type
        /// </summary>
        /// <param name="listType"></param>
        public static void UpdateAutoCompleteList(string listType)
        {
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.DynamicDataAbsoluteExpiration);
            if (cacheAbsoluteExpiration == "0")
            {
                Caching.Clear(CacheConstants.DynamicData);
            }

            if (Caching.Exists(CacheConstants.DynamicData))
            {
                DynamicDropDownData dynamicData = Caching.Get<DynamicDropDownData>(CacheConstants.DynamicData);
                HelperManager.Instance.UpdateAutoCompleteList(dynamicData, listType);
                Caching.Clear(CacheConstants.DynamicData);
                if (cacheAbsoluteExpiration != "0")
                    Caching.Add<DynamicDropDownData>(dynamicData, CacheConstants.DynamicData, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        /// This method will update the dynamic dropdowns for specified drop down type
        /// </summary>
        /// <param name="dropDownType"></param>
        public static void UpdateDynamicDropDownList(string dropDownType)
        {
            DynamicDropDown dropDown;
            string cacheAbsoluteExpiration = ConfigurationStore.GetAppSetting(AppSettingConstants.DynamicDropDownAbsoluteExpiration);
            if (cacheAbsoluteExpiration == "0")
            {
                Caching.Clear(CacheConstants.DynamicDropDown);
            }

            if (Caching.Exists(CacheConstants.DynamicDropDown))
            {
                dropDown = Caching.Get<DynamicDropDown>(CacheConstants.DynamicDropDown);
                HelperManager.Instance.UpdateDynamicDropDownList(dropDown, dropDownType);
                Caching.Clear(CacheConstants.DynamicDropDown);
                if (cacheAbsoluteExpiration != "0")
                    Caching.Add<DynamicDropDown>(dropDown, CacheConstants.DynamicDropDown, Convert.ToDouble((cacheAbsoluteExpiration == null) ? AppSettingConstants.DefaultCacheAbsoluteExpiration : cacheAbsoluteExpiration, CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        /// This method will clear the session
        /// </summary>
        /// <param name="key"></param>
        public void ClearSession(string key)
        {
            var sessionData = GetSession(key);
            if (sessionData != null)
                Session[key] = null;
        }

        /// <summary>
        /// This method will get the session object for specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetSession<T>(string key) where T : class
        {
            if (Session[key] != null)
                return (T)Session[key];
            else
                return null;
        }

        /// <summary>
        /// This method will get the session object
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object GetSession(string key)
        {
            if (Session[key] != null)
                return Session[key];
            else
                return null;
        }


        /// <summary>
        /// Check if session exists or not
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsSessionExists(string key)
        {
            return (Session[key] != null) ? true : false;
        }

        /// <summary>
        /// This method will get the view state for specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetViewState<T>(string key) where T : class
        {
            if (ViewState[key] != null)
                return (T)ViewState[key];
            else
                return null;
        }

        /// <summary>
        /// This method will get the view state
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public object GetViewState(string key)
        {
            if (ViewState[key] != null)
                return (int)ViewState[key];
            else
                return null;
        }

        /// <summary>
        /// First check if there exists a valid session.
        /// </summary>
        /// <param name="sOnQInsiderUserId"></param>
        /// <returns></returns>
        public bool OnQInsiderSessionExists(ref string userId, ref string userNumber)
        {
            PortalSecurityService.clsPortalSecurityService portalSecurity;
            System.Net.CredentialCache portalSecurityCred;
            HttpCookie suppCookie = HttpContext.Current.Request.Cookies["SessionID"];

            if (suppCookie == null)
            {
                return false;
            }
            else
            {
                User user = GetSession<User>(SessionConstants.User);
                if (user != null)
                {
                    portalSecurity = new PortalSecurityService.clsPortalSecurityService();
                    portalSecurityCred = new System.Net.CredentialCache();
                    portalSecurityCred.Add(new Uri(portalSecurity.Url), "Basic", new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SuppliersConnection_WebServiceAccount"], System.Configuration.ConfigurationManager.AppSettings["SuppliersConnection_WebServicePwd"], ""));
                    try
                    {
                        portalSecurity.Credentials = portalSecurityCred;
                        userId = portalSecurity.SessionGetValue(suppCookie.Value, AppSettingConstants.UserId);
                        userDesc = portalSecurity.SessionGetValue(suppCookie.Value, AppSettingConstants.UserDescription);
                        userNumber = portalSecurity.SessionGetValue(suppCookie.Value, AppSettingConstants.UserNumber);
                        userType = portalSecurity.SessionGetValue(suppCookie.Value, AppSettingConstants.UserType);
                        userEmail = portalSecurity.SessionGetValue(suppCookie.Value, AppSettingConstants.UserEmail);
                        employeeId = portalSecurity.SessionGetValue(suppCookie.Value, AppSettingConstants.EmployeeId);
                    }

                    catch (Exception)
                    {
                        throw;
                        //Insider will throw exceptions if cookie is incorrectly formed, but the cookie is Insider-generated so if this ever happens log/return false
                        // return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="redirectURL"></param>
        public void OnQLogin(string redirectURL)
        {
            try
            {
                // get the base 64 encoded SAML
                var samlAssertionRaw = GetSamlFromHttpRequest();

                var webRootFolder = Server.MapPath("~");
                // get the key data
                var certificateData = System.IO.File.ReadAllBytes(string.Concat(webRootFolder, "metadata.xml"));

                var dto = AuthenticatePing(samlAssertionRaw, certificateData);

                userId = dto.UserId;
                userDesc = dto.UserName;
                userNumber = dto.UserNumber;
                userType = dto.Usertype;
                userEmail = dto.Email;
                CreateLoggedInUserSession(userId, userNumber);
                User user = GetSession<User>(SessionConstants.User);
                //Response.Redirect("~/learninglounge/home.aspx", false);
            }
            catch (Exception ex)
            {

            }
        }


        private string GetSamlFromHttpRequest()
        {
            var rawSamlData = "";
            if (Session["assersion"] != null)
            {
                rawSamlData = Session["assersion"].ToString();
            }
            else rawSamlData = null;


            if (rawSamlData == null) Response.Redirect(OnqUrl(), false);
            // the sample data sent us may be already encoded, 
            // which results in double encoding
            if (rawSamlData.Contains('%'))
            {
                rawSamlData = HttpUtility.UrlDecode(rawSamlData);
            }
            if (rawSamlData == null) return null;
            var samlData = Convert.FromBase64String(rawSamlData);

            // read back into a UTF string
            var samlAssertion = Encoding.UTF8.GetString(samlData);
            return samlAssertion;
        }

        public LoginDetailDto AuthenticatePing(string samlAssertionRaw, byte[] certificateData)
        {
            // load a new XML document
            var assertion = new XmlDocument { PreserveWhitespace = true };
            assertion.LoadXml(samlAssertionRaw);

            // use a namespace manager to avoid the worst of xpaths
            var ns = new XmlNamespaceManager(assertion.NameTable);
            ns.AddNamespace("samlp", @"urn:oasis:names:tc:SAML:2.0:protocol");
            ns.AddNamespace("saml", @"urn:oasis:names:tc:SAML:2.0:assertion");
            ns.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);

            // get the signature XML node
            var signNode = assertion.SelectSingleNode(
                "/samlp:Response/saml:Assertion/ds:Signature", ns);

            // load the XML signature
            var signedXml = new SignedXml(assertion.DocumentElement);
            signedXml.LoadXml(signNode as XmlElement);

            // get the certificate, basically:
            // signedXml.KeyInfo.OfType<KeyInfoX509Data>().First().
            //     Certificates.OfType<X509Certificate2>().First()
            // but with added checks
            // var xmlCertificate = GetFirstX509Certificate(signedXml);

            // check the key and signature match
            // if (!signedXml.CheckSignature(certificate, true))
            //if (!IsValidCertificate(xmlCertificate, certificateData))
            //{
            //    throw new SecurityException("Signature check failed.");
            //}

            var onqID = assertion.SelectSingleNode("samlp:Response/saml:Assertion/saml:Subject/saml:NameID", ns).InnerText;

            var statusNode = assertion.SelectSingleNode("/samlp:Response/samlp:Status/samlp:StatusCode", ns);
            var statusValue = "Success";//statusNode.Attributes["Value"].Value.Replace(StatusNs, "");
            if (statusValue != "Success")
                throw new InvalidOperationException(
                    "Could not authenticate via PING. Please contact Hilton support for assistance");
            var attributes =
                assertion.SelectNodes("/samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute", ns);
            var samlUserAttributes = new Dictionary<string, string>();
            foreach (XmlNode attribute in attributes)
            {
                //TODO: refactor? attributes is an XmlNodeList (not a List<XmlNode> or anything implementing IEnumerable) -> may be forced to loop
                var attributeName = attribute.Attributes["Name"].Value;
                var attributeValue = attribute.InnerText;
                samlUserAttributes.Add(attributeName, attributeValue);
            }

            var fullname = "";
            fullname = samlUserAttributes.ContainsKey("firstname") ? samlUserAttributes["firstname"] : null;
            fullname += " ";
            fullname += samlUserAttributes.ContainsKey("lastname") ? samlUserAttributes["lastname"] : null;

            //****** Get the list of InnCodes returned from SAML
            string str = string.Empty;
            foreach (XmlNode xmlNodes2 in assertion.SelectNodes("samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute[@Name = 'inncodes']/saml:AttributeValue", ns))
            {
                str = string.Concat(str, xmlNodes2.InnerText, ",");
            }
            if (string.IsNullOrEmpty(str))
            {
                //This must be a corporate user so call routine to get list of properties from PIM;
            }
            //**************************************************

            var dto = new LoginDetailDto()
            {
                Email = samlUserAttributes.ContainsKey("mail") ? samlUserAttributes["mail"] : null,
                UserName = fullname,
                UserId = onqID,
                Usertype = samlUserAttributes.ContainsKey("lobbyusertype") ? samlUserAttributes["lobbyusertype"] : null,
                UserNumber = samlUserAttributes.ContainsKey("usernumber") ? samlUserAttributes["usernumber"] : null,
                IsAuthenticated = true
            };

            //Create cookie
            //var cookie = new HttpCookie("IsAuth", "true");
            //cookie.Expires = DateTime.Now.AddDays(1);
            //Response.Cookies.Add(cookie);
            return dto;
        }


        private X509Certificate2 GetFirstX509Certificate(SignedXml signedXml)
        {
            return signedXml.KeyInfo.OfType<KeyInfoX509Data>().First().
                Certificates.OfType<X509Certificate2>().First();
        }

        private bool IsValidCertificate(X509Certificate2 certificate, byte[] certificateData)
        {
            // decode the keys
            var cms = new SignedCms(SubjectIdentifierType.IssuerAndSerialNumber);
            cms.Decode(certificateData);

            // Placeholder for the certificate to validate

            // Placeholder for the extra collection of certificates to be used
            //var certificates = new X509Certificate2Collection();
            var certificates = cms.Certificates;

            var chain = new X509Chain
            {
                ChainPolicy =
                {
                    RevocationMode = X509RevocationMode.NoCheck,
                    VerificationFlags = X509VerificationFlags.AllowUnknownCertificateAuthority
                }
            };

            //chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
            chain.ChainPolicy.ExtraStore.AddRange(certificates);

            return chain.Build(certificate);
            // we have a keychain of X509Certificate2s, we need a collection of tokens
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="redirectURL"></param>
        public void OnQLogin_old(string redirectURL)
        {
            string loginURL = string.Empty;
            try
            {

                //check if the onq insider session exists.
                if (OnQInsiderSessionExists(ref userId, ref userNumber))
                {
                    CreateLoggedInUserSession(userId, userNumber);
                    User user = GetSession<User>(SessionConstants.User);
                    if (!string.IsNullOrWhiteSpace(redirectURL))
                    {
                        if (user != null && user.UserTypeId == (int)UserTypeEnum.Partner && user.IsPartnershipExpired)
                            Response.Redirect("~/Account/AccountStatus.aspx", false);
                        else
                            //Response.Redirect("~/" + redirectURL, false);
                            Response.Redirect("~/", false);
                    }
                    else
                    {
                        ClearSession(SessionConstants.User);
                        Response.Redirect("~/AccessDenied.aspx", false);
                    }
                }
                else
                {
                    Response.Redirect(OnqUrl(), false);
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>
        /// This method will set the session
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSession"></param>
        /// <param name="key"></param>
        public void SetSession<T>(string key, T objectToSession) where T : class
        {
            ClearSession(key);
            Session[key] = objectToSession;
        }

        /// <summary>
        /// This method will set the view state
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSession"></param>
        /// <param name="key"></param>
        public void SetViewState<T>(string key, T objectToViewState) where T : class
        {
            ViewState[key] = objectToViewState;
        }

        /// <summary>
        /// this method will set the value in view state for primitive type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSession"></param>
        /// <param name="key"></param>
        public void SetViewState(string key, int? objectToViewState)
        {
            ViewState[key] = objectToViewState;
        }

        /// <summary>
        /// This method will set the value in view state for sort direction structure
        /// </summary>
        /// <param name="key"></param>
        /// <param name="objectToViewState"></param>
        public void SetViewState(string key, SortDirection objectToViewState)
        {
            ViewState[key] = objectToViewState;
        }

        /// <summary>
        /// This method will set the value in the hidden field
        /// </summary>
        /// <param name="target"></param>
        /// <param name="value"></param>
        protected static void SetHiddenFieldValue(HiddenField target, string value)
        {
            if (target != null)
            {
                target.Value = value;
            }
        }

        /// <summary>
        /// This method will set the value in the label
        /// </summary>
        /// <param name="target"></param>
        /// <param name="value"></param>
        protected static void SetLiteralText(Literal target, string value)
        {
            if (target != null)
            {
                target.Text = value;
            }
        }

        /// <summary>
        /// This method will set the value in the label
        /// </summary>
        /// <param name="target"></param>
        /// <param name="value"></param>
        protected static void SetLabelText(Label target, string value)
        {
            if (target != null)
            {
                target.Text = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="target"></param>
        /// <param name="value"></param>
        protected static void SetRadioButtonSelected(CheckBox target, bool value)
        {
            if (target != null)
            {
                target.Checked = value;
            }
        }

        /// <summary>
        /// This method will set the value in the textbox
        /// </summary>
        /// <param name="target"></param>
        /// <param name="value"></param>
        protected static void SetTextBoxText(TextBox target, string value)
        {
            if (target != null)
            {
                target.Text = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="searchFilters"></param>
        /// <param name="dropDownType"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        /// <param name="selectedValue"></param>
        protected void BindDropDown(IList<SearchFilters> searchFiltersData, string dropDownType, DropDownList sourceDropDown, string defaultText, int? selectedValue)
        {
            if (string.Compare(dropDownType, DropDownConstants.ContactDropDownpartnerProfile) == 0)
                sourceDropDown.DataSource = Helper.GetSearchFilterDropDownData(dropDownType, searchFiltersData).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false);
            else
                sourceDropDown.DataSource = Helper.GetSearchFilterDropDownData(dropDownType, searchFiltersData).Where(p => string.IsNullOrWhiteSpace(p.DataTextField) == false).OrderBy(p => p.DataTextField);

            sourceDropDown.DataTextField = "DataTextField";
            sourceDropDown.DataValueField = "DataValueField";
            sourceDropDown.DataBind();
            if (!string.IsNullOrWhiteSpace(defaultText))
                sourceDropDown.Items.Insert(0, new ListItem(defaultText, "0"));
            if (selectedValue.HasValue && selectedValue > 0)
                sourceDropDown.SelectedValue = selectedValue.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dropDownType"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        /// <param name="selectedValue"></param>
        protected void BindDynamicDropDown(string dropDownType, DropDownList sourceDropDown, string defaultText, int? selectedValue)
        {
            sourceDropDown.DataSource = GetDynamicDropDownDataSource(dropDownType).OrderBy(p => p.DataTextField); ;
            sourceDropDown.DataTextField = "DataTextField";
            sourceDropDown.DataValueField = "DataValueField";
            sourceDropDown.DataBind();
            sourceDropDown.Items.Insert(0, new ListItem(defaultText, DropDownConstants.DefaultValue));
            if (selectedValue.HasValue && selectedValue > 0)
                sourceDropDown.SelectedValue = selectedValue.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dropDownType"></param>
        /// <param name="sourceDropDown"></param>
        /// <param name="defaultText"></param>
        /// <param name="selectedValue"></param>
        protected void BindStaticDropDown(string dropDownType, DropDownList sourceDropDown, string defaultText, int? selectedValue, bool defaultSort = true, string[] conditionValues = null)
        {

            IEnumerable<DropDownItem> items;

            if (conditionValues != null && conditionValues.ElementAtOrDefault(0) != "0")
            {
                items = GetStaticDropDownDataSource(dropDownType).Where(item => conditionValues.Contains(item.ParentDataField)).ToList<DropDownItem>().OrderBy(item => item.DataTextField);
            }
            else
            {
                items = GetStaticDropDownDataSource(dropDownType).OrderBy(item => item.DataTextField);
            }

            sourceDropDown.DataSource = defaultSort ? items.OrderBy(p => p.DataTextField) : items;

            sourceDropDown.DataTextField = "DataTextField";
            sourceDropDown.DataValueField = "DataValueField";
            sourceDropDown.DataBind();
            if (!string.IsNullOrEmpty(defaultText))
                sourceDropDown.Items.Insert(0, new ListItem(defaultText, DropDownConstants.DefaultValue));
            if (selectedValue.HasValue && selectedValue > 0)
                sourceDropDown.SelectedValue = selectedValue.ToString();
        }

        /// <summary>
        /// Add sorting image on sort column
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="HeaderRow"></param>
        protected void AddSortImage(int columnIndex, TableRow headerRow)
        {
            if (headerRow != null)
            {
                if (GetViewState<object>(ViewStateConstants.SortDirection).ToString() == UIConstants.Ascending)
                {
                    headerRow.Cells[columnIndex].Attributes.Add("class", "sort-a");
                }
                else if (GetViewState<object>(ViewStateConstants.SortDirection).ToString() == UIConstants.Descending)
                {
                    headerRow.Cells[columnIndex].Attributes.Add("class", "sort-d");
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        protected void ConfigureResultMessage(HtmlGenericControl resultDiv, string classToApply, string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                resultDiv.Visible = true;
                resultDiv.Attributes.Remove("class");
                resultDiv.Attributes.Add("class", classToApply);
                resultDiv.InnerText = message;
            }
        }

        /// <summary>
        /// This method is used for localization to iterate controls for translation attribute
        /// </summary>
        /// <param name="e"></param>
        override protected void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            IterateControls(Controls);
        }

        /// <summary>
        /// Process Drop Down Item Change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        /// <param name="brandDropDown"></param>
        /// <param name="regionDropDown"></param>
        /// <param name="propertyTypeDropDown"></param>
        protected void ProcessDropDownItemChange(string sourceDropDown, DropDownList brandDropDown, DropDownList regionDropDown, DropDownList propertyTypeDropDown)
        {
            IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
            Helper.ProcessDropDownItemChange(searchFiltersData, culture, sourceDropDown, brandDropDown, regionDropDown, propertyTypeDropDown);
        }

        /// <summary>
        /// Process Drop Down Item Change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        /// <param name="brandDropDown"></param>
        /// <param name="regionDropDown"></param>
        /// <param name="propertyTypeDropDown"></param>
        protected string ProcessDropDownItemChange(string sourceDropDown, string selectedBrands, DropDownList regionDropDown, DropDownList propertyTypeDropDown)
        {
            IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
            return Helper.ProcessDropDownItemChange(searchFiltersData, culture, sourceDropDown, selectedBrands, regionDropDown, propertyTypeDropDown);
        }
        protected string ProcessDropDownItemChangeforCountry(string sourceDropDown, string selectedBrands, DropDownList regionDropDown, DropDownList CountryDropDown, DropDownList propertyTypeDropDown)
        {
            IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
            return Helper.ProcessDropDownItemChangeForCountry(searchFiltersData, culture, sourceDropDown, selectedBrands, regionDropDown, CountryDropDown, propertyTypeDropDown);
        }
        /// <summary>
        /// Process Drop Down Item Change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        /// <param name="brandDropDown"></param>
        /// <param name="regionDropDown"></param>
        /// <param name="propertyTypeDropDown"></param>
        /// <param name="categoriesDropDown"></param>
        protected void ProcessDropDownItemChange(string sourceDropDown, DropDownList brandDropDown, DropDownList regionDropDown, DropDownList propertyTypeDropDown, DropDownList categoriesDropDown)
        {
            IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
            Helper.ProcessDropDownItemChange(searchFiltersData, culture, sourceDropDown, brandDropDown, regionDropDown, propertyTypeDropDown, categoriesDropDown);
        }
        protected void ProcessDropDownItemChangeForCountry(string sourceDropDown, DropDownList brandDropDown, DropDownList regionDropDown, DropDownList CountryDropDown, DropDownList propertyTypeDropDown, DropDownList categoriesDropDown)
        {
            IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
            Helper.ProcessDropDownItemChangeForCountry(searchFiltersData, culture, sourceDropDown, brandDropDown, regionDropDown, CountryDropDown, propertyTypeDropDown, categoriesDropDown);
        }
        /// <summary>
        /// Process Drop Down Item Change
        /// </summary>
        /// <param name="sourceDropDown"></param>
        /// <param name="brandDropDown"></param>
        /// <param name="regionDropDown"></param>
        /// <param name="propertyTypeDropDown"></param>
        /// <param name="categoriesDropDown"></param>
        protected Tuple<string, string> ProcessDropDownItemChange(string sourceDropDown, string brandDropDown, string regionDropDown, DropDownList propertyTypeDropDown, DropDownList categoriesDropDown)
        {
            IList<SearchFilters> searchFiltersData = GetViewState<IList<SearchFilters>>(ViewStateConstants.FilterSearchData);
            return Helper.ProcessDropDownItemChange(searchFiltersData, culture, sourceDropDown, brandDropDown, regionDropDown, propertyTypeDropDown, categoriesDropDown);
        }

        /// <summary>
        /// Validate Date
        /// </summary>
        /// <param name="args"></param>
        /// <param name="validateDateCustomValidator"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        protected void ValidateDate(ServerValidateEventArgs args, CustomValidator validateDateCustomValidator, string fromDate, string toDate, string mode = "")
        {
            args.IsValid = false;
            DateTime fromDateTime;
            DateTime toDateTime;

            string invalidFromDateMsg = (String.IsNullOrEmpty(mode)) ? ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidFromDate, culture) : "Invalid Activation Date";
            string invalidToDateMsg = (String.IsNullOrEmpty(mode)) ? ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidToDate, culture) : "Invalid Expiration Date";
            string requiredToDateMsg = (String.IsNullOrEmpty(mode)) ? ConfigurationStore.GetApplicationMessages(MessageConstants.ToDataRequiredValidation, culture) : "Expiration Date is required";
            string requiredFromDateMsg = (String.IsNullOrEmpty(mode)) ? ConfigurationStore.GetApplicationMessages(MessageConstants.FromDataRequiredValidation, culture) : "Activation Date is required";
            string invalidFromToDateMsg = (String.IsNullOrEmpty(mode)) ? ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidFromToDate, culture) : "Activation Date and Expiration Date are invalid";
            string toDateGreaterFromDateMsg = (String.IsNullOrEmpty(mode)) ? ConfigurationStore.GetApplicationMessages(MessageConstants.ToDateGreaterFromDate, culture) : "Expiration Date should be greater than Activation Date";

            if (!string.IsNullOrWhiteSpace(fromDate) && string.IsNullOrWhiteSpace(toDate))
            {
                if (!DateTime.TryParse(fromDate, out fromDateTime))
                {
                    validateDateCustomValidator.ErrorMessage = invalidFromDateMsg;
                }
                else
                {
                    validateDateCustomValidator.ErrorMessage = requiredToDateMsg;
                }
            }
            else if (string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                if (!DateTime.TryParse(toDate, out toDateTime))
                {
                    validateDateCustomValidator.ErrorMessage = invalidToDateMsg;
                }
                else
                {
                    validateDateCustomValidator.ErrorMessage = requiredFromDateMsg;
                }
            }
            else if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                if (!DateTime.TryParse(fromDate, out fromDateTime) && !DateTime.TryParse(toDate, out toDateTime))
                {
                    validateDateCustomValidator.ErrorMessage = invalidFromToDateMsg;
                }
                else if (!DateTime.TryParse(fromDate, out fromDateTime))
                {
                    validateDateCustomValidator.ErrorMessage = invalidFromDateMsg;
                }
                else if (!DateTime.TryParse(toDate, out toDateTime))
                {
                    validateDateCustomValidator.ErrorMessage = invalidToDateMsg;
                }
                else if (fromDateTime > toDateTime)
                {
                    validateDateCustomValidator.ErrorMessage = toDateGreaterFromDateMsg;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                args.IsValid = true;
            }
        }

        /// <summary>
        /// This method will return the translation attribute value
        /// </summary>
        /// <param name="control"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        private string GetControlAttribute(Control control, string attributeName)
        {
            AttributeCollection attributeCollection = null;
            string translateAttributeValue = null;

            if (control is WebControl)
            {
                WebControl webControl = control as WebControl;
                attributeCollection = webControl.Attributes;
                translateAttributeValue = attributeCollection[attributeName];
            }

            return translateAttributeValue;
        }

        /// <summary>
        /// This method will process the controls for localization
        /// </summary>
        /// <param name="controls"></param>
        private void IterateControls(ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                ProcessControl(control);
            }
        }

        /// <summary>
        /// Set Validator Regular Expressions
        /// </summary>
        /// <param name="regularExpressionValidator"></param>
        /// <param name="regularExpressionTypeId"></param>
        protected void SetValidatorRegularExpressions(RegularExpressionValidator regularExpressionValidator, int regularExpressionTypeId)
        {
            string emailValidationExpression = ControlValidatorUtility.GetLocalizedRegularExpression(regularExpressionTypeId, culture);
            if (!string.IsNullOrWhiteSpace(emailValidationExpression))
            {
                regularExpressionValidator.ValidationExpression = emailValidationExpression;
            }
            else
            {
                regularExpressionValidator.Enabled = false;
            }
        }

        /// <summary>
        /// Generates the redirect URL.
        /// </summary>
        /// <returns></returns>
        private string OnqUrl()
        {
            System.Text.StringBuilder output = new System.Text.StringBuilder();

            string onqHomePage = string.Empty;
            string applicationURL = string.Empty;
            string redirectURL = string.Empty;
            string directURL = this.Page.Request.RawUrl.ToString();

            if (directURL == "/")
            {
                directURL = "/Home.aspx";
            }

            onqHomePage = ConfigurationManager.AppSettings["SAMLLogin"];
            //onqHomePage = ConfigurationManager.AppSettings["OnQInsiderLoginPage"];


            Uri applicationUri = new Uri(Request.Url.AbsoluteUri.Replace(directURL, ""));

            applicationURL = ConfigurationManager.AppSettings["ApplicationURL"];

            directURL = directURL.Replace(applicationURL, string.Empty);

            redirectURL = string.Format("{0}", onqHomePage); //SAML
            //redirectURL = string.Format("{0}{1}{2}{3}{4}", onqHomePage, "?APPURL=", applicationUri, HttpUtility.UrlEncode(applicationURL), HttpUtility.UrlEncode(directURL));

            return redirectURL;
        }

        /// <summary>
        /// This method will process process the controls
        /// </summary>
        /// <param name="control"></param>
        private void ProcessControl(Control control)
        {
            string CBID = GetControlAttribute(control, translateAttribute);
            string VMTI = GetControlAttribute(control, validationMsgTextAttribute);
            int contentBlockId;
            int validationMessageTypeId;
            string text = null;
            if (!String.IsNullOrEmpty(CBID) && int.TryParse(CBID, out contentBlockId))
            {
                text = ResourceUtility.GetLocalizedString(contentBlockId, culture, string.Empty);
                if (text == "blank")
                {
                    text = string.Empty;
                }
                if (!string.IsNullOrEmpty(text))
                {
                    if (control is Label)
                    {
                        Label label = control as Label;
                        label.Text = (label.Text.EndsWith(":", StringComparison.InvariantCulture) && !text.EndsWith(":", StringComparison.InvariantCulture)) ? text + ":" : text;
                    }
                    else if (control is Button)
                    {
                        Button button = control as Button;
                        button.Text = (button.Text.EndsWith(":", StringComparison.InvariantCulture) && !text.EndsWith(":", StringComparison.InvariantCulture)) ? text + ":" : text;
                    }
                    else if (control is CheckBox)
                    {
                        CheckBox checkBox = control as CheckBox;
                        checkBox.Text = (checkBox.Text.EndsWith(":", StringComparison.InvariantCulture) && !text.EndsWith(":", StringComparison.InvariantCulture)) ? text + ":" : text;
                    }
                    else if (control is RadioButton)
                    {
                        RadioButton radioButton = control as RadioButton;
                        radioButton.Text = (radioButton.Text.EndsWith(":", StringComparison.InvariantCulture) && !text.EndsWith(":", StringComparison.InvariantCulture)) ? text + ":" : text;
                    }
                    else if (control is System.Web.UI.HtmlControls.HtmlGenericControl)
                    {
                        System.Web.UI.HtmlControls.HtmlGenericControl ctrl = control as System.Web.UI.HtmlControls.HtmlGenericControl;
                        ctrl.InnerText = (ctrl.InnerText.EndsWith(":", StringComparison.InvariantCulture) && !text.EndsWith(":", StringComparison.InvariantCulture)) ? text + ":" : text;
                    }
                    else if (control is LinkButton)
                    {
                        LinkButton linkBtn = control as LinkButton;
                        linkBtn.Text = (linkBtn.Text.EndsWith(":", StringComparison.InvariantCulture) && !text.EndsWith(":", StringComparison.InvariantCulture)) ? text + ":" : text;
                    }
                    else if (control is HyperLink)
                    {
                        HyperLink hyperLink = control as HyperLink;
                        hyperLink.Text = (hyperLink.Text.EndsWith(":", StringComparison.InvariantCulture) && !text.EndsWith(":", StringComparison.InvariantCulture)) ? text + ":" : text;
                    }
                    else if (control is Literal)
                    {
                        Literal literal = control as Literal;
                        literal.Text = (literal.Text.EndsWith(":", StringComparison.InvariantCulture) && !text.EndsWith(":", StringComparison.InvariantCulture)) ? text + ":" : text;
                    }
                }
            }
            else if (!String.IsNullOrEmpty(VMTI) && int.TryParse(VMTI, out validationMessageTypeId))
            {
                text = ControlValidatorUtility.GetLocalizedValidationMessage(validationMessageTypeId, culture);
                if (!string.IsNullOrEmpty(text))
                {
                    if (control is RequiredFieldValidator)
                    {
                        RequiredFieldValidator requiredFiedValidator = control as RequiredFieldValidator;
                        requiredFiedValidator.ErrorMessage = text;
                    }
                    else if (control is RegularExpressionValidator)
                    {
                        RegularExpressionValidator regularExpressionValidator = control as RegularExpressionValidator;
                        regularExpressionValidator.ErrorMessage = text;
                    }
                    else if (control is CompareValidator)
                    {
                        CompareValidator compareValidator = control as CompareValidator;
                        compareValidator.ErrorMessage = text;
                    }
                    else if (control is RangeValidator)
                    {
                        RangeValidator rangeValidator = control as RangeValidator;
                        rangeValidator.ErrorMessage = text;
                    }
                    else if (control is CustomValidator)
                    {
                        CustomValidator customValidator = control as CustomValidator;
                        customValidator.ErrorMessage = text;
                    }
                }
            }

            // Process the child controls
            if (control.HasControls())
            {
                foreach (Control child in control.Controls)
                {
                    ProcessControl(child);
                }
            }
        }

        /// <summary>
        /// Create the session for the Supplier Connection application
        /// </summary>
        /// <param name="userNumber"></param>
        /// <returns></returns>
        protected void CreateLoggedInUserSession(string hiltonId, string hiltonNumber = "")
        {
            try
            {
                _securityManager = Hilton.SuppliersConnection.Business.SecurityManager.Instance;
                User loggedInUser = new User();

                //Check if the user exists in the DB then retrieve the details.
                loggedInUser = _securityManager.CreateUserSession(hiltonId, hiltonNumber);

                if (loggedInUser != null)
                {
                    if (loggedInUser.UserStatusId == Convert.ToInt32(UserStatusEnum.Active))
                    {
                        SetSession<User>(SessionConstants.User, loggedInUser);
                        SetSession<string>(SessionConstants.IsAuthorized, "true");
                    }
                    else if (loggedInUser.UserStatusId == Convert.ToInt32(UserStatusEnum.Pending))
                    {
                        Response.Redirect("~/AccessDenied.aspx", false);
                    }
                    else
                    {
                        Response.Redirect("~/AccessDenied.aspx", false);
                    }
                    if (loggedInUser.UserTypeId == (int)UserTypeEnum.Partner && string.IsNullOrEmpty(loggedInUser.Company))
                    {
                        ClearSession(SessionConstants.User);
                        SetSession<string>(SessionConstants.IsAuthorized, "false");
                        Response.Redirect("~/AccessDenied.aspx", false);
                    }
                }
                else if (userType == UIConstants.CorporateUserType)
                {
                    userDesc += " "; //If Only firstname is available then then we need to add space in the end
                    loggedInUser = new User
                    {
                        HiltonUserName = userId,
                        HiltonUserNumber = userNumber,
                        FirstName = string.IsNullOrWhiteSpace(userDesc) ? string.Empty : userDesc.Substring(0, userDesc.IndexOf(" ")).Trim(),
                        LastName = string.IsNullOrWhiteSpace(userDesc) ? string.Empty : userDesc.Substring(userDesc.IndexOf(" ") + 1).Trim(),
                        UserTypeId = (int)UserTypeEnum.Corporate,
                        Email = userEmail
                    };
                    SetSession<User>(SessionConstants.User, loggedInUser);
                    SetSession<string>(SessionConstants.IsAuthorized, "true");
                }

                else
                {
                    ClearSession(SessionConstants.User);
                    SetSession<string>(SessionConstants.IsAuthorized, "false");
                    Response.Redirect("~/", false);
                }

            }
            catch (ThreadAbortException)
            { }
        }

        /// <summary>
        /// Compress
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static byte[] Compress(byte[] data)
        {
            MemoryStream output = new MemoryStream();
            GZipStream gzip = new GZipStream(output,
                              CompressionMode.Compress, true);
            gzip.Write(data, 0, data.Length);
            gzip.Close();
            return output.ToArray();
        }

        /// <summary>
        /// Decompress
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static byte[] Decompress(byte[] data)
        {
            MemoryStream input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            GZipStream gzip = new GZipStream(input,
                              CompressionMode.Decompress, true);
            MemoryStream output = new MemoryStream();
            byte[] buff = new byte[64];
            int read = -1;
            read = gzip.Read(buff, 0, buff.Length);
            while (read > 0)
            {
                output.Write(buff, 0, read);
                read = gzip.Read(buff, 0, buff.Length);
            }
            gzip.Close();
            return output.ToArray();
        }

        /// <summary>
        /// LoadPageStateFromPersistenceMedium
        /// </summary>
        /// <returns></returns>
        protected override object LoadPageStateFromPersistenceMedium()
        {
            string viewState = Request.Form["__COMPRESSEDVIEWSTATE"];
            byte[] bytes = Convert.FromBase64String(viewState);
            bytes = Decompress(bytes);
            LosFormatter formatter = new LosFormatter();
            return formatter.Deserialize(Convert.ToBase64String(bytes));
        }

        /// <summary>
        /// SavePageStateToPersistenceMedium
        /// </summary>
        /// <param name="viewState"></param>
        protected override void SavePageStateToPersistenceMedium(object viewState)
        {
            LosFormatter formatter = new LosFormatter();
            StringWriter writer = new StringWriter();
            formatter.Serialize(writer, viewState);
            string viewStateString = writer.ToString();
            byte[] bytes = Convert.FromBase64String(viewStateString);
            bytes = Compress(bytes);
            ScriptManager.RegisterHiddenField(this, "__COMPRESSEDVIEWSTATE", Convert.ToBase64String(bytes));
        }

    }


}