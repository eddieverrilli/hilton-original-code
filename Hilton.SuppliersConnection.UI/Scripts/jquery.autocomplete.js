﻿(function(a){a.fn.extend({autocomplete:function(d,c){var b=typeof d=="string";
c=a.extend({},a.Autocompleter.defaults,{url:b?d:null,data:b?null:d,delay:b?a.Autocompleter.defaults.delay:10,max:c&&!c.scroll?100:150},c);
c.highlight=c.highlight||function(e){return e
};
c.formatMatch=c.formatMatch||c.formatItem;
return this.each(function(){new a.Autocompleter(this,c)
})
},result:function(b){return this.bind("result",b)
},search:function(b){return this.trigger("search",[b])
},flushCache:function(){return this.trigger("flushCache")
},setOptions:function(b){return this.trigger("setOptions",[b])
},unautocomplete:function(){return this.trigger("unautocomplete")
}});
a.Autocompleter=function(j,o){var k={UP:38,DOWN:40,DEL:46,TAB:9,RETURN:13,ESC:27,COMMA:188,PAGEUP:33,PAGEDOWN:34,BACKSPACE:8};
var b=a(j).attr("autocomplete","off").addClass(o.inputClass);
var w;
var q="";
var e=a.Autocompleter.Cache(o);
var g=0;
var l;
var f={mouseDownOnSelect:false};
var t=a.Autocompleter.Select(o,j,u,f);
var d;
a.browser.opera&&a(j.form).bind("submit.autocomplete",function(){if(d){d=false;
return false
}});
b.bind((a.browser.opera?"keypress":"keydown")+".autocomplete",function(y){g=1;
l=y.keyCode;
switch(y.keyCode){case k.UP:y.preventDefault();
if(t.visible()){t.prev()
}else{n(0,true)
}break;
case k.DOWN:y.preventDefault();
if(t.visible()){t.next()
}else{n(0,true)
}break;
case k.PAGEUP:y.preventDefault();
if(t.visible()){t.pageUp()
}else{n(0,true)
}break;
case k.PAGEDOWN:y.preventDefault();
if(t.visible()){t.pageDown()
}else{n(0,true)
}break;
case o.multiple&&a.trim(o.multipleSeparator)==","&&k.COMMA:case k.TAB:u();
break;
case k.RETURN:if(u()){y.preventDefault();
d=true;
return false
}break;
case k.ESC:t.hide();
break;
default:clearTimeout(w);
w=setTimeout(n,o.delay);
break
}}).focus(function(){g++
}).blur(function(){g=0;
if(!f.mouseDownOnSelect){h()
}}).click(function(){if(g++>1&&!t.visible()){n(0,true)
}}).bind("search",function(){var z=(arguments.length>1)?arguments[1]:null;
function y(C,A){var D;
if(A&&A.length){for(var B=0;
B<A.length;
B++){if(A[B].result.toLowerCase()==C.toLowerCase()){D=A[B];
break
}}}if(typeof z=="function"){z(D)
}else{b.trigger("result",D&&[D.data,D.value])
}}a.each(x(b.val()),function(A,B){s(B,y,y)
})
}).bind("flushCache",function(){e.flush()
}).bind("setOptions",function(){a.extend(o,arguments[1]);
if("data" in arguments[1]){e.populate()
}}).bind("unautocomplete",function(){t.unbind();
b.unbind();
a(j.form).unbind(".autocomplete")
});
function u(){var A=t.selected();
if(!A){return false
}var C=A.result;
q=C;
if(o.multiple){var E=x(b.val());
if(E.length>1){var B=o.multipleSeparator.length;
var y=a(j).selection().start;
var D,z=0;
a.each(E,function(F,G){z+=G.length;
if(y<=z){D=F;
return false
}z+=B
});
E[D]=C;
C=E.join(o.multipleSeparator)
}C+=o.multipleSeparator
}b.val(C);
i();
b.trigger("result",[A.data,A.value]);
return true
}function n(y,A){if(l==k.DEL){t.hide();
return
}var z=b.val();
if(!A&&z==q){return
}q=z;
z=m(z);
if(z.length>=o.minChars){b.addClass(o.loadingClass);
if(!o.matchCase){z=z.toLowerCase()
}s(z,r,i)
}else{v();
t.hide()
}}function x(y){if(!y){return[""]
}if(!o.multiple){return[a.trim(y)]
}return a.map(y.split(o.multipleSeparator),function(z){return a.trim(y).length?a.trim(z):null
})
}function m(z){if(!o.multiple){return z
}var A=x(z);
if(A.length==1){return A[0]
}var y=a(j).selection().start;
if(y==z.length){A=x(z)
}else{A=x(z.replace(z.substring(y),""))
}return A[A.length-1]
}function c(y,z){if(o.autoFill&&(m(b.val()).toLowerCase()==y.toLowerCase())&&l!=k.BACKSPACE){b.val(b.val()+z.substring(m(q).length));
a(j).selection(q.length,q.length+z.length)
}}function h(){clearTimeout(w);
w=setTimeout(i,200)
}function i(){var y=t.visible();
t.hide();
clearTimeout(w);
v();
if(o.mustMatch){b.search(function(z){if(!z){if(o.multiple){var A=x(b.val()).slice(0,-1);
b.val(A.join(o.multipleSeparator)+(A.length?o.multipleSeparator:""))
}else{b.val("");
b.trigger("result",null)
}}})
}}function r(z,y){if(y&&y.length&&g){v();
t.display(y,z);
c(z,y[0].value);
t.show()
}else{i()
}}function s(D,C,A){if(!o.matchCase){D=D.toLowerCase()
}var y=e.load(D);
if(y&&y.length){C(D,y)
}else{if((typeof o.url=="string")&&(o.url.length>0)){var z={timestamp:+new Date()};
a.each(o.extraParams,function(E,F){z[E]=typeof F=="function"?F():F
});
if(o.isPageMethod==false){a.ajax({mode:"abort",port:"autocomplete"+j.name,dataType:o.dataType,url:o.url,data:a.extend({q:m(D),limit:o.max},z),success:function(E){var F=o.parse&&o.parse(E)||p(E);
e.add(D,F);
C(D,F)
},complete:function(F,E){if(E=="error"){document.write(F.responseText);
ajaxValid=false
}}})
}else{var B="";
a.each(z,function(E,F){if(B.length>0){B+=","
}B+='"'+E+'":"'+F+'"'
});
B="{"+B+"}";
a.ajax({type:"POST",port:"autocomplete"+j.name,contentType:"application/json; charset=utf-8",dataType:"json",url:o.url,data:B,success:function(E){var F=o.parse&&o.parse(E)||p(E);
e.add(D,F);
C(D,F)
}})
}}else{t.emptyList();
A(D)
}}}function p(y){var A=[];
var C=y.split("\n");
for(var z=0;
z<C.length;
z++){var B=a.trim(C[z]);
if(B){B=B.split("|");
A[A.length]={data:B,value:B[0],result:o.formatResult&&o.formatResult(B,B[0])||B[0]}
}}return A
}function v(){b.removeClass(o.loadingClass)
}};
a.Autocompleter.defaults={inputClass:"ac_input",resultsClass:"ac_results",loadingClass:"ac_loading",minChars:1,delay:400,matchCase:false,matchSubset:true,matchContains:false,cacheLength:10,max:100,mustMatch:false,extraParams:{},selectFirst:true,formatItem:function(b){return b[0]
},formatMatch:null,autoFill:false,width:0,multiple:false,multipleSeparator:", ",highlight:function(c,b){return c.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+b.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi,"\\$1")+")(?![^<>]*>)(?![^&;]+;)","gi"),"<strong>$1</strong>")
},scroll:true,scrollHeight:180,isPageMethod:false};
a.Autocompleter.Cache=function(g){var c={};
var e=0;
function f(k,l){if(!g.matchCase){k=k.toLowerCase()
}var j=k.indexOf(l);
if(g.matchContains=="word"){j=k.toLowerCase().search("\\b"+l.toLowerCase())
}if(j==-1){return false
}return j==0||g.matchContains
}function b(i,j){if(e>g.cacheLength){d()
}if(!c[i]){e++
}c[i]=j
}function h(){if(!g.data){return false
}var p={},l=0;
if(!g.url){g.cacheLength=1
}p[""]=[];
for(var k=0,m=g.data.length;
k<m;
k++){var n=g.data[k];
n=(typeof n=="string")?[n]:n;
var q=g.formatMatch(n,k+1,g.data.length);
if(q===false){continue
}var j=q.charAt(0).toLowerCase();
if(!p[j]){p[j]=[]
}var o={value:q,data:n,result:g.formatResult&&g.formatResult(n)||q};
p[j].push(o);
if(l++<g.max){p[""].push(o)
}}a.each(p,function(r,s){g.cacheLength++;
b(r,s)
})
}setTimeout(h,25);
function d(){c={};
e=0
}return{flush:d,add:b,populate:h,load:function(o){if(!g.cacheLength||!e){return null
}if(!g.url&&g.matchContains){var l=[];
for(var n in c){if(n.length>0){var j=c[n];
a.each(j,function(k,p){if(f(p.value,o)){l.push(p)
}})
}}return l
}else{if(c[o]){return c[o]
}else{if(g.matchSubset){for(var m=o.length-1;
m>=g.minChars;
m--){var j=c[o.substr(0,m)];
if(j){var l=[];
a.each(j,function(k,p){if(f(p.value,o)){l[l.length]=p
}});
return l
}}}}}return null
}}
};
a.Autocompleter.Select=function(p,i,q,d){var c={ACTIVE:"ac_over"};
var l,b=-1,e,s="",o=true,f,k;
function h(){if(!o){return
}f=a("<div/>").hide().addClass(p.resultsClass).css("position","absolute").appendTo(document.body);
k=a("<ul/>").appendTo(f).mouseover(function(t){if(r(t).nodeName&&r(t).nodeName.toUpperCase()=="LI"){b=a("li",k).removeClass(c.ACTIVE).index(r(t));
a(r(t)).addClass(c.ACTIVE)
}}).click(function(t){a(r(t)).addClass(c.ACTIVE);
q();
i.focus();
return false
}).mousedown(function(){d.mouseDownOnSelect=true
}).mouseup(function(){d.mouseDownOnSelect=false
});
if(p.width>0){f.css("width",p.width)
}o=false
}function r(u){var t=u.target;
while(t&&t.tagName!="LI"){t=t.parentNode
}if(!t){return[]
}return t
}function n(v){l.slice(b,b+1).removeClass(c.ACTIVE);
m(v);
var t=l.slice(b,b+1).addClass(c.ACTIVE);
if(p.scroll){var u=0;
l.slice(0,b).each(function(){u+=this.offsetHeight
});
if((u+t[0].offsetHeight-k.scrollTop())>k[0].clientHeight){k.scrollTop(u+t[0].offsetHeight-k.innerHeight())
}else{if(u<k.scrollTop()){k.scrollTop(u)
}}}}function m(t){b+=t;
if(b<0){b=l.size()-1
}else{if(b>=l.size()){b=0
}}}function j(t){return p.max&&p.max<t?p.max:t
}function g(){k.empty();
var w=j(e.length);
for(var u=0;
u<w;
u++){if(!e[u]){continue
}var t=p.formatItem(e[u].data,u+1,w,e[u].value,s);
if(t===false){continue
}var v=a("<li/>").html(p.highlight(t,s)).addClass(u%2==0?"ac_even":"ac_odd").appendTo(k)[0];
a.data(v,"ac_data",e[u])
}l=k.find("li");
if(p.selectFirst){l.slice(0,1).addClass(c.ACTIVE);
b=0
}if(a.fn.bgiframe){k.bgiframe()
}}return{display:function(t,u){h();
e=t;
s=u;
g()
},next:function(){n(1)
},prev:function(){n(-1)
},pageUp:function(){if(b!=0&&b-8<0){n(-b)
}else{n(-8)
}},pageDown:function(){if(b!=l.size()-1&&b+8>l.size()){n(l.size()-1-b)
}else{n(8)
}},hide:function(){f&&f.hide();
l&&l.removeClass(c.ACTIVE);
b=-1
},visible:function(){return f&&f.is(":visible")
},current:function(){return this.visible()&&(l.filter("."+c.ACTIVE)[0]||p.selectFirst&&l[0])
},show:function(){var u=a(i).offset();
f.css({width:typeof p.width=="string"||p.width>0?p.width:a(i).width(),top:u.top+i.offsetHeight,left:u.left}).show();
if(p.scroll){k.scrollTop(0);
k.css({maxHeight:p.scrollHeight,overflow:"auto"});
if(a.browser.msie&&typeof document.body.style.maxHeight==="undefined"){var t=0;
l.each(function(){t+=this.offsetHeight
});
var v=t>p.scrollHeight;
k.css("height",v?p.scrollHeight:t);
if(!v){l.width(k.width()-parseInt(l.css("padding-left"))-parseInt(l.css("padding-right")))
}}}},selected:function(){var t=l&&l.filter("."+c.ACTIVE).removeClass(c.ACTIVE);
return t&&t.length&&a.data(t[0],"ac_data")
},emptyList:function(){k&&k.empty()
},unbind:function(){f&&f.remove()
}}
};
a.fn.selection=function(g,c){if(g!==undefined){return this.each(function(){if(this.createTextRange){var j=this.createTextRange();
if(c===undefined||g==c){j.move("character",g);
j.select()
}else{j.collapse(true);
j.moveStart("character",g);
j.moveEnd("character",c);
j.select()
}}else{if(this.setSelectionRange){this.setSelectionRange(g,c)
}else{if(this.selectionStart){this.selectionStart=g;
this.selectionEnd=c
}}}})
}var d=this[0];
if(d.createTextRange){var f=document.selection.createRange(),e=d.value,h="<->",i=f.text.length;
f.text=h;
var b=d.value.indexOf(h);
d.value=e;
this.selection(b,b+i);
return{start:b,end:b+i}
}else{if(d.selectionStart!==undefined){return{start:d.selectionStart,end:d.selectionEnd}
}}}
})(jQuery);