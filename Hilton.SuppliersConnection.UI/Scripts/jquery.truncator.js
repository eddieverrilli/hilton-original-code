﻿(function(a){var f=true;
a.fn.truncate=function(i){var j=a.extend({},a.fn.truncate.defaults,i);
a(this).each(function(){var l=a.trim(e(a(this).text())).length;
if(l<=j.max_length){return
}var k=j.max_length-j.more.length-3;
var n=d(this,k);
var m=a(this).hide();
n.insertAfter(m);
c(n).append(' <br/><a href="#show more content">'+j.more+"</a>");
b(m).append(' <br/><a href="#show less content">'+j.less+"</a>");
n.find("a:last").click(function(){n.hide();
m.show();
return false
});
m.find("a:last").click(function(){n.show();
m.hide();
return false
})
})
};
a.fn.truncate.defaults={max_length:100,more:"More...",less:"Hide"};
function d(j,i){return(j.nodeType==3)?h(j,i):g(j,i)
}function g(k,i){var k=a(k);
var j=k.clone().empty();
var l;
k.contents().each(function(){var m=i-j.text().length;
if(m==0){return
}l=d(this,m);
if(l){j.append(l)
}});
return j
}function h(j,i){var k=e(j.data);
if(f){k=k.replace(/^ /,"")
}f=!!k.match(/ $/);
var k=k.slice(0,i);
k=a("<div/>").text(k).html();
return k
}function e(i){return i.replace(/\s+/g," ")
}function c(l){var i=a(l);
var k=i.children(":last");
if(!k){return l
}var j=k.css("display");
if(!j||j=="inline"){return i
}return c(k)
}function b(k){var i=a(k);
var j=i.children(":last");
if(j&&j.is("p")){return j
}return k
}})(jQuery);
$(function(){$(".excerpt").truncate({max_length:180})
});