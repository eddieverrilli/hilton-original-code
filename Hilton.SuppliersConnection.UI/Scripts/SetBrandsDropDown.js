﻿var br = [];
var isAllUnChecked = 0;
//Set the multilevel Brand drop down 
function setupDDL() {
    try {
        var index = -1;
        var $cont = $('#optgroup');
        $('.multiselect').each(function () {
            var v = $(this).attr('BrandSegment');
            if (v != undefined && v != index) {

                $('<optGroup/>').attr('label', v).appendTo($cont);
                $(this).remove();
                index = v;
            }
            else {
                $cont.find('optgroup').last().append($(this));
                $(this).text($(this).text());
            }
        });
        $('#optgroup').multiselect();
        $('#optgroup').multiselect('refresh');
        if($('#selectedBrands').val() != undefined)
        {
            var myArray = $('#selectedBrands').val().split(',');
            if (myArray != null && myArray != '' && myArray != undefined) {

                for (var j = 0; j < myArray.length; j++) {
                    $cont.find('option[value = ' + myArray[j] + ']').attr({ 'selected': 'selected' });
                }
            }
            else if (isAllUnChecked == 0) {
                //To check all the items
                $("#optgroup").multiselect('checkAll');
            }
            $('#optgroup').multiselect('refresh');
        }
        var values = $("#optgroup").val();
        $('#selectedBrands').val(values);

        $('#optgroup').multiselect('refresh');

         //Default text
        $("#optgroup").multiselect({
            selectedText: function (numChecked, numTotal, checkedItems) {
                if (numChecked == numTotal) {
                    $('#isCheckAll').val("1");
                    return "All Brands";
                }
                else {
                    $('#isCheckAll').val("0");
                    return numChecked + ' Brand(s)';
                }
            }
        });
        $("#optgroup").multiselect({
            //height: "auto",
            minWidth: "202",
            noneSelectedText: "Select Brand(s)",
            checkAllText: "All Brands",
            uncheckAllText: "UnCheck All"
        });
        // Calling the close event
        $("#optgroup").multiselect({
            close: function () {
                var values = $("#optgroup").val();
                $('#selectedBrands').val(values).change();
                
                if ($('#selectedBrands').val() == $('#brandCollection').val())
                    $('#isCheckAll').val("1");
                else
                    $('#isCheckAll').val("0");
            }
        });
        $("#optgroup").multiselect({
            checkAll: function () {
                var values = $("#optgroup").val();
                $('#selectedBrands').val(values);
                isAllUnChecked = 0;
            }
        });

        $("#optgroup").multiselect({
            uncheckAll: function () {
                var values = $("#optgroup").val();
                $('#selectedBrands').val(values);
                isAllUnChecked = 1;
            }
        });
    } catch (e) {
        alert(e);
    }
}

function getItems() {
    try {
        var obj = { Parm: 'Brand', BrandIds: $('#brandCollection').val(), selectedBrands: $("#optgroup").val() };
        $.ajax({
            type: "GET",
            url: "../Handlers/BrandData.ashx",
            data: obj,
            success: function (data) {
                ddl = $('#optgroup');
                br = data;
                if (data != undefined) {
                    var count = 1;
                    for (var i = 0; i < data.length; i++) {
                        ddl.append($('<option />').val(data[i].BrandId).text(data[i].BrandDescription).addClass('multiselect').attr({ 'BrandSegment': data[i].BrandSegment }));
                    }
                    setupDDL();
                }
            },
            error: function (a, b) {
                alert(a.response);
            }
        });

    } catch (e) {
        alert(e);
    }

}
