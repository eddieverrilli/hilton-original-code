﻿var isAllRegionUnChecked = 0;
var isAllCountryUnChecked = 0;
function SetRegionAndCountryDropDown($RegionDropDown, $CountryDropDown, $SelectedRegions, $SelectedCountries) {

    $RegionDropDown.multiselect();
    $RegionDropDown.multiselect('refresh');

    //check the selected regions
    var myArray = $SelectedRegions.val().split(',');
    if (myArray != null && myArray != '' && myArray != undefined) {

        for (var j = 0; j < myArray.length; j++) {
            $RegionDropDown.find('option[value = ' + myArray[j] + ']').attr({ 'selected': 'selected' });
        }
    }
    else if (isAllRegionUnChecked == 0) {
        $RegionDropDown.multiselect('checkAll');
        var values = $RegionDropDown.val();
        $SelectedRegions.val(values);
    }
    $RegionDropDown.multiselect('refresh');
    //Set the selected text value
    $RegionDropDown.multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                return "All Regions";
            }
            else {
                return numChecked + ' Region(s)';
            }
        }
    });
    $RegionDropDown.multiselect('refresh');
    //Look and Feel of drop down
    $RegionDropDown.multiselect({
        noneSelectedText: "Select Region(s)",
        checkAllText: "All Region",
        uncheckAllText: "UnCheck All",
        position: {
            my: 'left bottom',
            at: 'left top'
        }
    });

    // Calling the close event
    $RegionDropDown.multiselect({
        close: function () {
            var values = $RegionDropDown.val();
            $SelectedRegions.val(values).change();
        }
    });

    $RegionDropDown.multiselect({
        checkAll: function () {
            var values = $RegionDropDown.val();
            $SelectedRegions.val(values);
            isAllRegionUnChecked = 0;
        }
    });

    $RegionDropDown.multiselect({
        uncheckAll: function () {
            var values = $RegionDropDown.val();
            $SelectedRegions.val(values);
            isAllRegionUnChecked = 1;
        }
    });

    $CountryDropDown.multiselect();
    $CountryDropDown.multiselect('refresh');
    //check the selected Countries
    var myArray = $SelectedCountries.val().split(',');
    if (myArray != null && myArray != '' && myArray != undefined) {

        for (var j = 0; j < myArray.length; j++) {
            $CountryDropDown.find('option[value = ' + myArray[j] + ']').attr({ 'selected': 'selected' });
        }
    }
    else if(isAllCountryUnChecked == 0)  {
        $CountryDropDown.multiselect('checkAll');
        var values = $CountryDropDown.val();
        $SelectedCountries.val(values);
    }
    //Look and Feel of country drop down
    $CountryDropDown.multiselect({
        noneSelectedText: "Select Country(s)",
        checkAllText: "All Countries",
        uncheckAllText: "UnCheck All",
         position: {
            my: 'left bottom',
            at: 'left top'
        }
    });
    //Selected text of country drop down
    $CountryDropDown.multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                return "All Countries";
            }
            else {
                return numChecked + ' Country(ies)';
            }
        }
    });
    $CountryDropDown.multiselect('refresh');
    // Calling the close event
    $CountryDropDown.multiselect({
        close: function () {
            var values = $CountryDropDown.val();
            $SelectedCountries.val(values).change();
        }
    });
    $CountryDropDown.multiselect('refresh');

    $CountryDropDown.multiselect({
        checkAll: function () {
            var values = $CountryDropDown.val();
            $SelectedCountries.val(values);
            isAllCountryUnChecked = 0;
        }
    });

    $CountryDropDown.multiselect({
        uncheckAll: function () {
            var values = $CountryDropDown.val();
            $SelectedCountries.val(values);
            isAllCountryUnChecked = 1;
        }
    });
}
function SetRegionAndCountryDropDownPartnerDetail($RegionDropDown, $CountryDropDown, $SelectedRegions, $SelectedCountries) {    
    $RegionDropDown.multiselect();
    $RegionDropDown.multiselect('refresh');

    //check the selected regions
    var myArray = $SelectedRegions.val().split(',');
    if (myArray != null && myArray != '' && myArray != undefined) {

        for (var j = 0; j < myArray.length; j++) {
            $RegionDropDown.find('option[value = ' + myArray[j] + ']').attr({ 'selected': 'selected' });
        }
    }
    else if (isAllRegionUnChecked == 0) {
        //$RegionDropDown.multiselect('checkAll');
        $RegionDropDown.find('option[value = 16]').attr({ 'selected': 'selected' });
        var values = $RegionDropDown.val();
        $SelectedRegions.val(values);
    }
    $RegionDropDown.multiselect('refresh');
    //Set the selected text value
    $RegionDropDown.multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                return "All Regions";
            }
            else {
                return numChecked + ' Region(s)';
            }
        }
    });
    $RegionDropDown.multiselect('refresh');
    //Look and Feel of drop down
    $RegionDropDown.multiselect({
        noneSelectedText: "Select Region(s)",
        checkAllText: "All Region",
        uncheckAllText: "UnCheck All",
        position: {
            my: 'left bottom',
            at: 'left top'
        }
    });

    // Calling the close event
    $RegionDropDown.multiselect({
        close: function () {
            var values = $RegionDropDown.val();
            $SelectedRegions.val(values).change();
        }
    });

    $RegionDropDown.multiselect({
        checkAll: function () {
            var values = $RegionDropDown.val();
            $SelectedRegions.val(values);
            isAllRegionUnChecked = 0;
        }
    });

    $RegionDropDown.multiselect({
        uncheckAll: function () {
            var values = $RegionDropDown.val();
            $SelectedRegions.val(values);
            isAllRegionUnChecked = 1;
        }
    });

    $CountryDropDown.multiselect();
    $CountryDropDown.multiselect('refresh');
    //check the selected Countries
    var myArray = $SelectedCountries.val().split(',');
    if (myArray != null && myArray != '' && myArray != undefined) {

        for (var j = 0; j < myArray.length; j++) {
            $CountryDropDown.find('option[value = ' + myArray[j] + ']').attr({ 'selected': 'selected' });
        }
    }
    else if (isAllCountryUnChecked == 0) {
        $CountryDropDown.multiselect('checkAll');
        var values = $CountryDropDown.val();
        $SelectedCountries.val(values);
    }
    //Look and Feel of country drop down
    $CountryDropDown.multiselect({
        noneSelectedText: "Select Country(s)",
        checkAllText: "All Countries",
        uncheckAllText: "UnCheck All",
        position: {
            my: 'left bottom',
            at: 'left top'
        }
    });
    //Selected text of country drop down
    $CountryDropDown.multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                return "All Countries";
            }
            else {
                return numChecked + ' Country(ies)';
            }
        }
    });
    $CountryDropDown.multiselect('refresh');
    // Calling the close event
    $CountryDropDown.multiselect({
        close: function () {
            var values = $CountryDropDown.val();
            $SelectedCountries.val(values).change();
        }
    });
    $CountryDropDown.multiselect('refresh');

    $CountryDropDown.multiselect({
        checkAll: function () {
            var values = $CountryDropDown.val();
            $SelectedCountries.val(values);
            isAllCountryUnChecked = 0;
        }
    });

    $CountryDropDown.multiselect({
        uncheckAll: function () {
            var values = $CountryDropDown.val();
            $SelectedCountries.val(values);
            isAllCountryUnChecked = 1;
        }
    });
}

