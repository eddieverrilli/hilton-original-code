﻿var cr = [];
//var isAllUnChecked = 0;
//Set the multilevel Brand drop down 
function setupCountryDDL() {
    try {
        var index = -1;
        var $cont = $('#OptCountryGroup');
        $('.multicountry').each(function () {
            var v = $(this).attr('RegionName');
            if (v != undefined && v != index) {

                $('<optGroup/>').attr('label', v).appendTo($cont);
                $(this).remove();
                index = v;
            }
            else {
                $cont.find('optGroup').last().append($(this));
                $(this).text($(this).text());
            }
        });
        $('#OptCountryGroup').multiselect();
        $('#OptCountryGroup').multiselect('refresh');
        if($('#selectedCountries').val() != undefined)
        {
            var myArray = $('#selectedCountries').val().split(',');
            if (myArray != null && myArray != '' && myArray != undefined) {

                for (var j = 0; j < myArray.length; j++) {
                    $cont.find('option[value = ' + myArray[j] + ']').attr({ 'selected': 'selected' });
                }
            }
            else if (isAllUnChecked == 0) {
                //To check all the items
                $("#OptCountryGroup").multiselect('checkAll');
            }
            $('#OptCountryGroup').multiselect('refresh');
        }
        var values = $("#OptCountryGroup").val();
        $('#selectedCountries').val(values);

        $('#OptCountryGroup').multiselect('refresh');

         //Default text
        $("#OptCountryGroup").multiselect({
            selectedText: function (numChecked, numTotal, checkedItems) {
                if (numChecked == numTotal) {
                    $('#isCheckAll').val("1");
                    return "All Countries";
                }
                else {
                    $('#isCheckAll').val("0");
                    return numChecked + ' Country(s)';
                }
            }
        });
        $("#OptCountryGroup").multiselect({
            //height: "auto",
            minWidth: "202",
            noneSelectedText: "Select Country(s)",
            checkAllText: "All Countries",
            uncheckAllText: "UnCheck All"
        });
        // Calling the close event
        $("#OptCountryGroup").multiselect({
            close: function () {
                var values = $("#OptCountryGroup").val();
                $('#selectedCountries').val(values).change();
                
                if ($('#selectedCountries').val() == $('#brandCollection').val())
                    $('#isCheckAll').val("1");
                else
                    $('#isCheckAll').val("0");
            }
        });
        $("#OptCountryGroup").multiselect({
            checkAll: function () {
                var values = $("#OptCountryGroup").val();
                $('#selectedCountries').val(values);
                isAllUnChecked = 0;
            }
        });

        $("#OptCountryGroup").multiselect({
            uncheckAll: function () {
                var values = $("#OptCountryGroup").val();
                $('#selectedCountries').val(values);
                isAllUnChecked = 1;
            }
        });
    } catch (e) {
        alert(e);
    }
}

function getCountryItems() {
    try {
        var obj = { Parm: 'Country', CountryIds: $('#countryCollection').val(), selectedCountries: $("#OptCountryGroup").val() };
        $.ajax({
            type: "GET",
            url: "../Handlers/CountryData.ashx",
            data: obj,
            success: function (data) {
                ddl = $('#OptCountryGroup');
                cr = data;
                if (data != undefined) {
                    var count = 1;
                    for (var i = 0; i < data.length; i++) {
                        ddl.append($('<option />').val(data[i].CountryId).text(data[i].CountryName).addClass('multicountry').attr({ 'RegionName': data[i].RegionName }));
                    }
                    setupCountryDDL();
                }
            },
            error: function (a, b) {
                alert(a.response);
            }
        });

    } catch (e) {
        alert(e);
    }

}
