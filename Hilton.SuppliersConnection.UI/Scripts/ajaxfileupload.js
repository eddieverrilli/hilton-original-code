﻿jQuery.extend({createUploadIframe:function(b,d){var a="jUploadFrame"+b;
var c='<iframe id="'+a+'" name="'+a+'" style="position:absolute; top:-9999px; left:-9999px"';
if(window.ActiveXObject){if(typeof d=="boolean"){c+=' src="javascript:false"'
}else{if(typeof d=="string"){c+=' src="'+d+'"'
}}}c+=" />";
jQuery(c).appendTo(document.body);
return jQuery("#"+a).get(0)
},createUploadForm:function(g,b,a){var e="jUploadForm"+g;
var c="jUploadFile"+g;
var d=jQuery('<form  action="" method="POST" name="'+e+'" id="'+e+'" enctype="multipart/form-data"></form>');
if(a){for(var f in a){jQuery('<input type="hidden" name="'+f+'" value="'+a[f]+'" />').appendTo(d)
}}var j=jQuery("#"+b);
var h=jQuery(j).clone();
jQuery(j).attr("id",c);
jQuery(j).before(h);
jQuery(j).appendTo(d);
jQuery(d).css("position","absolute");
jQuery(d).css("top","-1200px");
jQuery(d).css("left","-1200px");
jQuery(d).appendTo("body");
return d
},ajaxFileUpload:function(i){i=jQuery.extend({},jQuery.ajaxSettings,i);
var f=new Date().getTime();
var b=jQuery.createUploadForm(f,i.fileElementId,(typeof(i.data)=="undefined"?false:i.data));
var g=jQuery.createUploadIframe(f,i.secureuri);
var d="jUploadFrame"+f;
var c="jUploadForm"+f;
if(i.global&&!jQuery.active++){jQuery.event.trigger("ajaxStart")
}var h=false;
var k={};
if(i.global){jQuery.event.trigger("ajaxSend",[k,i])
}var j=function(n){var m=document.getElementById(d);
try{if(m.contentWindow){k.responseText=m.contentWindow.document.body?m.contentWindow.document.body.innerHTML:null;
k.responseXML=m.contentWindow.document.XMLDocument?m.contentWindow.document.XMLDocument:m.contentWindow.document
}else{if(m.contentDocument){k.responseText=m.contentDocument.document.body?m.contentDocument.document.body.innerHTML:null;
k.responseXML=m.contentDocument.document.XMLDocument?m.contentDocument.document.XMLDocument:m.contentDocument.document
}}}catch(l){jQuery.handleError(i,k,null,l)
}if(k||n=="timeout"){h=true;
var o;
try{o=n!="timeout"?"success":"error";
if(o!="error"){if(i.global){jQuery.event.trigger("ajaxSuccess",[k,i])
}}}catch(l){o="error";
jQuery.handleError(i,k,o,l)
}if(i.global){jQuery.event.trigger("ajaxComplete",[k,i])
}if(i.global&&!--jQuery.active){jQuery.event.trigger("ajaxStop")
}if(i.success){i.success(k,o)
}jQuery(m).unbind();
setTimeout(function(){try{jQuery(m).remove();
jQuery(b).remove()
}catch(p){jQuery.handleError(i,k,null,p)
}},100);
k=null
}};
if(i.timeout>0){setTimeout(function(){if(!h){j("timeout")
}},i.timeout)
}try{var b=jQuery("#"+c);
jQuery(b).attr("action",i.url);
jQuery(b).attr("method","POST");
jQuery(b).attr("target",d);
if(b.encoding){jQuery(b).attr("encoding","multipart/form-data")
}else{jQuery(b).attr("enctype","multipart/form-data")
}jQuery(b).submit()
}catch(a){jQuery.handleError(i,k,null,a)
}jQuery("#"+d).load(j);
return{abort:function(){}}
},uploadHttpData:function(r,type){var data=!type;
data=type=="xml"||data?r.responseXML:r.responseText;
if(type=="script"){jQuery.globalEval(data)
}if(type=="json"){eval("data = "+data)
}if(type=="html"){jQuery("<div>").html(data).evalScripts()
}return data
}});