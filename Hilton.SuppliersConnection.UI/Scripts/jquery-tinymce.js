﻿jQuery.fn.tinymce=jQuery.fn.tinyMCE=jQuery.fn.TinyMCE=function(a,b){return this.each(function(g){var d,e,f=this.id=this.id||this.name||(this.className||"jMCE")+g;
if(a&&Object===a.constructor){b=a;
a=null
}if(!a&&tinyMCE.get(f)){a="remove";
e=true
}switch(a){case"remove":d="mceRemoveControl";
break;
case"toggle":d="mceToggleEditor";
break;
default:d="mceAddControl"
}tinyMCE.settings=b;
tinyMCE.execCommand(d,false,f);
if(e){$(this).tinyMCE(b)
}})
};