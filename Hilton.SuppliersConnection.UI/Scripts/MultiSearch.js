﻿/// <reference path="jquery-1.7.1.min.js" />


function log(message) {
               //var abc = $("#" + message.value).show();
               if($("#div_error").length> 0)
               {
                $("#div_error").remove();
               }

               if($("#div_" + message.value.replace(" ","")).length == 0)
               {
                    $(CreateParentDiv(message)).appendTo($("#log"));
               }
               else
               {
                    var objCurrent = $("#div_" + message.value.replace(" ",""));
                    if($("#Child_" + message.label.replace(" ","")).length == 0)
                        $(GetText(message)).appendTo(objCurrent);
               }

               $(".icon-cancel").unbind("click").click(function fn() {

                    if($(this).closest("div.parentDiv").find("div.abcd").length ==1)
                   {
                        $(this).closest("div.parentDiv").remove();
                   }

                   $(this).closest("div.abcd").remove();
                   if($(this).closest("div.parentDiv").find("div.abcd").length ==0)
                   {
                    $(this).closest("div.parentDiv").remove();
                   }

               });
               $("#search").val("");
           }

           function GetText(message) {
           //if (message.value == "City")
           //{
               return "<div class='abcd' style ='float:left' id='Child_"+message.label.replace(" ","")+"'  ><div style ='float:left'><input type='hidden' id='"+ message.label.replace(" ","_") + "' value='"+ message.id + "'>" + message.label + "</div><div class='icon-cancel'>&nbsp;</div></div>&nbsp;&nbsp;";                
          // }
//           if (message.value == "Country")
//           {
//               return "<div class='abcde' style ='float:left'><div style ='float:left'><input type='hidden' id='"+ message.label + "' value='"+ message.id + "'>" + message.label + "</div><div class='icon-cancel'>&nbsp;</div><div>&nbsp;&nbsp;";                
//           }
           }


           function CreateParentDiv(id)
           {
                return "<div id='div_"+id.value.replace(" ","") +"' class='parentDiv'><div style ='float:left' class='label'> "+id.value +":</div>"+GetText(id)+" </div>  <div style='clear:both'></div>"
           }

function pageLoad()
{
    $("#search").Watermark("Please type to search");
    $( "#search" ).catcomplete({
             delay: 0,
             minLength: 2,
            source:  function (request, response) {
               var parameters = { name_startsWith: request.term };
               var json_data = JSON.stringify(parameters);
               jQuery.support.cors = true;
                   $.ajax({
                       url: "RecommendedPartners.aspx/SearchAutoComplete",
                       dataType: "json",
                       type: "POST",
                       contentType: "application/json; charset=utf-8",
                       data :json_data,
                       async: true,
                       crossDomain : true,
                       error: function (xhr, textStatus, errorThrown) {
                           $('#log').html(textStatus);
                           $("#search").val("");
                       },
                       failure: function (data) {
                           alert(data);
                       },
                       success: function (data) {
                           response($.map(data.d, function (item) {
                               return {
                                   label: item.name,
                                   value: item.type,
                                   id: item.id,
                               }
                           }));
                       }
                   });
               },
               select: function (event, ui) {
                   log(ui.item); 
                   $("#search").val("");
               },
               open: function () {
                   $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
               },
               close: function () {
                   $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                   //$("#search").val("")
                   $("#search").val("");
               }
    });
}
      
       
       function fn_getSelectedvales()
       {
           var str =""; 
            if ($("div.parentDiv").length > 0)
            {
                    $("div.parentDiv").each(function fn(){
                        str = str + this.id.split("_")[1]+",";   
                        $(this).find("input:hidden").each(function fn(){
                            str = str + "," + $(this).val() ;
                        });
                        str = str + "@";
                    });
                    $("#hdCountry").val(str);
                    //$("#Selectedvalue").empty().text(str);
                     return true;
            }
            else
            {
                if($("#div_error").length==0)
                {
                  $("#log").html("<div id='div_error' style='width: 155px;font-size: 11px;' class='message notification'>No Filter Selected</div>");
                }
              return false;
            }
       }


        $.widget( "custom.catcomplete", $.ui.autocomplete, {
            _renderMenu: function( ul, items ) {
            var that = this,
         currentCategory = "";
          $.each( items, function( index, item ) {
            if ( item.value != currentCategory ) {
              ul.append( "<li class='ui-autocomplete-category'>" + item.value + "</li>" );
              currentCategory = item.value;
            }
            that._renderItemData( ul, item );
          });
        }
      });    