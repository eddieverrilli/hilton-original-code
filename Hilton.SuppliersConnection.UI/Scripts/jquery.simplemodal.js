﻿(function(a){var c=a.browser.msie&&parseInt(a.browser.version)===6&&typeof window.XMLHttpRequest!=="object",g=a.browser.msie&&parseInt(a.browser.version)===7,e=null,b=[];
a.modal=function(d,f){return a.modal.impl.init(d,f)
};
a.modal.close=function(){a.modal.impl.close()
};
a.modal.focus=function(d){a.modal.impl.focus(d)
};
a.modal.setContainerDimensions=function(){a.modal.impl.setContainerDimensions()
};
a.modal.setPosition=function(){a.modal.impl.setPosition()
};
a.modal.update=function(d,f){a.modal.impl.update(d,f)
};
a.fn.modal=function(d){return a.modal.impl.init(this,d)
};
a.modal.defaults={appendTo:"body",focus:true,opacity:50,overlayId:"simplemodal-overlay",overlayCss:{},containerId:"simplemodal-container",containerCss:{},dataId:"simplemodal-data",dataCss:{},minHeight:null,minWidth:null,maxHeight:null,maxWidth:null,autoResize:false,autoPosition:true,zIndex:1000,close:true,closeHTML:'<a class="modalCloseImg" title="Close"></a>',closeClass:"simplemodal-overlay",escClose:true,overlayClose:false,position:null,persist:false,modal:true,onOpen:null,onShow:null,onClose:null};
a.modal.impl={d:{},init:function(d,f){var h=this;
if(h.d.data){return false
}e=a.browser.msie&&!a.boxModel;
h.o=a.extend({},a.modal.defaults,f);
h.zIndex=h.o.zIndex;
h.occb=false;
if(typeof d==="object"){d=d instanceof jQuery?d:a(d);
h.d.placeholder=false;
if(d.parent().parent().size()>0){d.before(a("<span></span>").attr("id","simplemodal-placeholder").css({display:"none"}));
h.d.placeholder=true;
h.display=d.css("display");
if(!h.o.persist){h.d.orig=d.clone(true)
}}}else{if(typeof d==="string"||typeof d==="number"){d=a("<div></div>").html(d)
}else{alert("SimpleModal Error: Unsupported data type: "+typeof d);
return h
}}h.create(d);
h.open();
a.isFunction(h.o.onShow)&&h.o.onShow.apply(h,[h.d]);
return h
},create:function(d){var f=this;
b=f.getDimensions();
if(f.o.modal&&c){f.d.iframe=a('<iframe src="javascript:false;"></iframe>').css(a.extend(f.o.iframeCss,{display:"none",opacity:0,position:"fixed",height:b[0],width:b[1],zIndex:f.o.zIndex,top:0,left:0})).appendTo(f.o.appendTo)
}f.d.overlay=a("<div></div>").attr("id",f.o.overlayId).addClass("simplemodal-overlay").css(a.extend(f.o.overlayCss,{display:"none",opacity:f.o.opacity/100,height:f.o.modal?b[0]:0,width:f.o.modal?b[1]:0,position:"fixed",left:0,top:0,zIndex:f.o.zIndex+1})).appendTo(f.o.appendTo);
f.d.container=a("<div></div>").attr("id",f.o.containerId).addClass("simplemodal-container").css(a.extend(f.o.containerCss,{display:"none",position:"absolute",zIndex:f.o.zIndex+2})).append(f.o.close&&f.o.closeHTML?a(f.o.closeHTML).addClass(f.o.closeClass):"").appendTo(f.o.appendTo);
f.d.wrap=a("<div></div>").attr("tabIndex",-1).addClass("simplemodal-wrap").css({height:"100%",outline:0,width:"100%"}).appendTo(f.d.container);
f.d.data=d.attr("id",d.attr("id")||f.o.dataId).addClass("simplemodal-data").css(a.extend(f.o.dataCss,{display:"none"})).appendTo("body");
f.setContainerDimensions();
f.d.data.appendTo(f.d.wrap);
if(c||e){f.fixIE()
}},bindEvents:function(){var d=this;
a("."+d.o.closeClass).bind("click.simplemodal",function(f){f.preventDefault();
d.close()
});
d.o.modal&&d.o.close&&d.o.overlayClose&&d.d.overlay.bind("click.simplemodal",function(f){f.preventDefault();
d.close()
});
a(document).bind("keydown.simplemodal",function(f){if(d.o.modal&&f.keyCode===9){d.watchTab(f)
}else{if(d.o.close&&d.o.escClose&&f.keyCode===27){f.preventDefault();
d.close()
}}});
a(window).bind("resize.simplemodal",function(){b=d.getDimensions();
d.o.autoResize?d.setContainerDimensions():d.o.autoPosition&&d.setPosition();
if(c||e){d.fixIE()
}else{if(d.o.modal){d.d.iframe&&d.d.iframe.css({height:b[0],width:b[1]});
d.d.overlay.css({height:b[0],width:b[1]})
}}})
},unbindEvents:function(){a("."+this.o.closeClass).unbind("click.simplemodal");
a(document).unbind("keydown.simplemodal");
a(window).unbind("resize.simplemodal");
this.d.overlay.unbind("click.simplemodal")
},fixIE:function(){var d=this,f=d.o.position;
a.each([d.d.iframe||null,!d.o.modal?null:d.d.overlay,d.d.container],function(i,l){if(l){var k=l[0].style;
k.position="absolute";
if(i<2){k.removeExpression("height");
k.removeExpression("width");
k.setExpression("height",'document.body.scrollHeight > document.body.clientHeight ? document.body.scrollHeight : document.body.clientHeight + "px"');
k.setExpression("width",'document.body.scrollWidth > document.body.clientWidth ? document.body.scrollWidth : document.body.clientWidth + "px"')
}else{var j;
if(f&&f.constructor===Array){i=f[0]?typeof f[0]==="number"?f[0].toString():f[0].replace(/px/,""):l.css("top").replace(/px/,"");
i=i.indexOf("%")===-1?i+' + (t = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"':parseInt(i.replace(/%/,""))+' * ((document.documentElement.clientHeight || document.body.clientHeight) / 100) + (t = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"';
if(f[1]){j=typeof f[1]==="number"?f[1].toString():f[1].replace(/px/,"");
j=j.indexOf("%")===-1?j+' + (t = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft) + "px"':parseInt(j.replace(/%/,""))+' * ((document.documentElement.clientWidth || document.body.clientWidth) / 100) + (t = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft) + "px"'
}}else{i='(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (t = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"';
j='(document.documentElement.clientWidth || document.body.clientWidth) / 2 - (this.offsetWidth / 2) + (t = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft) + "px"'
}k.removeExpression("top");
k.removeExpression("left");
k.setExpression("top",i);
k.setExpression("left",j)
}}})
},focus:function(d){var f=this;
d=d&&a.inArray(d,["first","last"])!==-1?d:"first";
var h=a(":input:enabled:visible:"+d,f.d.wrap);
setTimeout(function(){h.length>0?h.focus():f.d.wrap.focus()
},10)
},getDimensions:function(){var d=a(window);
return[a.browser.opera&&a.browser.version>"9.5"&&a.fn.jquery<"1.3"||a.browser.opera&&a.browser.version<"9.5"&&a.fn.jquery>"1.2.6"?d[0].innerHeight:d.height(),d.width()]
},getVal:function(d,f){return d?typeof d==="number"?d:d==="auto"?0:d.indexOf("%")>0?parseInt(d.replace(/%/,""))/100*(f==="h"?b[0]:b[1]):parseInt(d.replace(/px/,"")):null
},update:function(d,f){var h=this;
if(!h.d.data){return false
}h.d.origHeight=h.getVal(d,"h");
h.d.origWidth=h.getVal(f,"w");
h.d.data.hide();
d&&h.d.container.css("height",d);
f&&h.d.container.css("width",f);
h.setContainerDimensions();
h.d.data.show();
h.o.focus&&h.focus();
h.unbindEvents();
h.bindEvents()
},setContainerDimensions:function(){var d=this,f=c||g,k=d.d.origHeight?d.d.origHeight:a.browser.opera?d.d.container.height():d.getVal(f?d.d.container[0].currentStyle.height:d.d.container.css("height"),"h");
f=d.d.origWidth?d.d.origWidth:a.browser.opera?d.d.container.width():d.getVal(f?d.d.container[0].currentStyle.width:d.d.container.css("width"),"w");
var n=d.d.data.outerHeight(true),m=d.d.data.outerWidth(true);
d.d.origHeight=d.d.origHeight||k;
d.d.origWidth=d.d.origWidth||f;
var l=d.o.maxHeight?d.getVal(d.o.maxHeight,"h"):null,o=d.o.maxWidth?d.getVal(d.o.maxWidth,"w"):null;
l=l&&l<b[0]?l:b[0];
o=o&&o<b[1]?o:b[1];
var p=d.o.minHeight?d.getVal(d.o.minHeight,"h"):"auto";
k=k?d.o.autoResize&&k>l?l:k<p?p:k:n?n>l?l:d.o.minHeight&&p!=="auto"&&n<p?p:n:p;
l=d.o.minWidth?d.getVal(d.o.minWidth,"w"):"auto";
f=f?d.o.autoResize&&f>o?o:f<l?l:f:m?m>o?o:d.o.minWidth&&l!=="auto"&&m<l?l:m:l;
d.d.container.css({height:k,width:f});
d.d.wrap.css({overflow:n>k||m>f?"auto":"visible"});
d.o.autoPosition&&d.setPosition()
},setPosition:function(){var d=this,f,h;
f=b[0]/2-d.d.container.outerHeight(true)/2;
h=b[1]/2-d.d.container.outerWidth(true)/2;
if(d.o.position&&Object.prototype.toString.call(d.o.position)==="[object Array]"){f=d.o.position[0]||f;
h=d.o.position[1]||h
}else{f=f;
h=h
}d.d.container.css({left:h,top:f})
},watchTab:function(d){var f=this;
if(a(d.target).parents(".simplemodal-container").length>0){f.inputs=a(":input:enabled:visible:first, :input:enabled:visible:last",f.d.data[0]);
if(!d.shiftKey&&d.target===f.inputs[f.inputs.length-1]||d.shiftKey&&d.target===f.inputs[0]||f.inputs.length===0){d.preventDefault();
f.focus(d.shiftKey?"last":"first")
}}else{d.preventDefault();
f.focus()
}},open:function(){var d=this;
d.d.iframe&&d.d.iframe.show();
if(a.isFunction(d.o.onOpen)){d.o.onOpen.apply(d,[d.d])
}else{d.d.overlay.show();
d.d.container.show();
d.d.data.show()
}d.o.focus&&d.focus();
d.bindEvents()
},close:function(){var d=this;
if(!d.d.data){return false
}d.unbindEvents();
if(a.isFunction(d.o.onClose)&&!d.occb){d.occb=true;
d.o.onClose.apply(d,[d.d])
}else{if(d.d.placeholder){var f=a("#simplemodal-placeholder");
if(d.o.persist){f.replaceWith(d.d.data.removeClass("simplemodal-data").css("display",d.display))
}else{d.d.data.hide().remove();
f.replaceWith(d.d.orig)
}}else{d.d.data.hide().remove()
}d.d.container.hide().remove();
d.d.overlay.hide();
d.d.iframe&&d.d.iframe.hide().remove();
setTimeout(function(){d.d.overlay.remove();
d.d={}
},10)
}}}
})(jQuery);