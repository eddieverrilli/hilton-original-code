﻿/*
    This file is used to provide the image logo update and validate functionality.
*/

//Style for the upload button
function SetFileInputStyles() {
    $("input.customInput").filestyle({
        image: "../Images/choose.gif",
        imageheight: 22,
        imagewidth: 65,
        width: 65
    });

}

//file upload
function ajaxFileUpload(control, fileType) {
    if (fileType == 'image') {
        var imageName = $('#imageUpload')[0].value.toString().substring($('#imageUpload')[0].value.toString().lastIndexOf('\\') + 1, $('#imageUpload')[0].value.length);
        $('#imageNameHiddenField').val(imageName);
    }

    if (fileType == 'image')
        $('#loading').attr('source', fileType);

    $("#loading")
                .ajaxStart(function (obj) {
                    if ($(this).attr('source') == 'image') {
                        $(this).show();
                        $(this).attr('source', '');
                    }

                })
                .ajaxComplete(function (obj) {
                    $(this).hide();
                });

                $.ajaxFileUpload
                    (
                        {
                            url: '../Handlers/FileUpload.ashx?fileType=' + fileType,
                            secureuri: false,
                            fileElementId: control,
                            dataType: 'json',
                            data: { name: 'login', id: 'id' },
                            success: function (data, status) {
                                if (status == 'success' && data.responseText != null && data.responseText == 'uploaded') {
                                    SetFileLink(fileType);
                                    if (fileType == 'image') {
                                        $.ajax({
                                            type: "POST",
                                            url: "../Handlers/FileUpload.ashx?fileType=" + fileType + "&op=show",
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (data, img) {
                                                $('#companyLogo').attr('src', "data:image/png;base64," + data);
                                            },
                                            error: function (data, status, e) {
                                            }
                                        });
                                    }
                                }
                                else {
                                    ValidateFile(fileType, data.responseText);
                                }
                            },
                            error: function (data, status, e) {
                            }
                        }
                    )
    return false;
}

//Set the file link type
function SetFileLink(fileType) {
    switch (fileType) {
        case 'image':
            $('#imageErrSpan').text('');
            $('#imageErrSpan').css({ 'display': 'none' });
            $('#imageLinkButton').text($('#imageNameHiddenField').val());
            if ($('#imageNameHiddenField').val() != '') {
                $('#removeImageLinkButton').css({ 'display': 'inline' });
            }
            break;
    }
}

// Validate the uploaded file
function ValidateFile(fileType, msg) {
    switch (fileType) {
        case 'image':
            $('#imageErrSpan').text(msg);
            $('#imageErrSpan').css({ 'display': 'inline' });
            $('#removeImageLinkButton').css({ 'display': 'none' });
            $('#imageNameHiddenField').val("");
            $('#imageLinkButton').text("");
            $('#companyLogo').attr('src', '../Images/NoProduct.PNG');
            break;
    }
}