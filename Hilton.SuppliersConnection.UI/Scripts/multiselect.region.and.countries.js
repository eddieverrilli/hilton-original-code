﻿var isAllUnChecked = 0;
var isCountryClicked = 0;
var isAllCountryChecked = 0;
function SetRegionAndCountryDropDown() {
    $('#regionDropDown').multiselect();
    $('#regionDropDown').multiselect('refresh');

    //check the selected regions
    var myArray = $('#selectedRegions').val().split(',');
    if (myArray != null && myArray != '' && myArray != undefined) {

        for (var j = 0; j < myArray.length; j++) {
            $('#regionDropDown').find('option[value = ' + myArray[j] + ']').attr({ 'selected': 'selected' });
        }
    }
    else if (isAllUnChecked == 0) {
        //To check all the items
        $('#regionDropDown').multiselect('checkAll');
        $('#isAllRegionChecked').val("1");
    }
    $('#regionDropDown').multiselect('refresh');

    //Set the selected region value to hidden variable
    var values = $('#regionDropDown').val();
    $('#selectedRegions').val(values);
    $('#regionDropDown').multiselect('refresh');

    //Set the selected text value
    $('#regionDropDown').multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                $('#isAllRegionChecked').val("1");
                return "All Regions";
            }
            else {
                $('#isAllRegionChecked').val("0");
                return numChecked + ' Region(s)';
            }
        }
    });

    //Look and Feel of drop down
    $('#regionDropDown').multiselect({
        minWidth: "140",
        noneSelectedText: "Select Region(s)",
        checkAllText: "All Region",
        uncheckAllText: "UnCheck All"
    });

    // Calling the close event
    $('#regionDropDown').multiselect({
        close: function () {
            var values = $('#regionDropDown').val();
            $('#selectedRegions').val(values).change();
            if ($('#selectedRegions').val() == $('#regionCollection').val())
                $('#isAllRegionChecked').val("1");
            else
                $('#isAllRegionChecked').val("0");
        }
    });
    // Check all event
    $('#regionDropDown').multiselect({
        checkAll: function () {
            var values = $('#regionDropDown').val();
            $('#selectedRegions').val(values);
            isAllUnChecked = 0;
        }
    });
    //Uncheck all event
    $('#regionDropDown').multiselect({
        uncheckAll: function () {
            var values = $('#regionDropDown').val();
            $('#selectedRegions').val(values);
            isAllUnChecked = 1;
        }
    });

    //Multiselect of country drop down
    $('#countryDropDown').multiselect();
    $('#countryDropDown').multiselect('refresh');

    if ($('#selectedRegions').val() == $('#regionCollection').val() || isAllUnChecked == 1) {
        if (isCountryClicked != 1 &&  document.referrer.indexOf("PartnerProfile.aspx") == -1) {
            $('#countryDropDown').multiselect('checkAll');
            isCountryClicked = 0;
        }
       isAllUnChecked = 0;
    }
    


    $('#countryDropDown').multiselect('refresh');
    if ($('#selectedRegions').val() == '') {
        $('#countryDropDown').multiselect('disable');
    }
    else {
        $('#countryDropDown').multiselect('enable')
    }
    $('#countryDropDown').multiselect('refresh');
    //Look and Feel of country drop down
    $('#countryDropDown').multiselect({
        minWidth:"140",
        noneSelectedText: "Select Country(s)",
        checkAllText: "All Countries",
        uncheckAllText: "UnCheck All"
    });
    //Selected text of country drop down
    $('#countryDropDown').multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                return "All Countries";
            }
            else {
                return numChecked + ' Country(ies)';
            }
        }
    });
    $('#countryDropDown').multiselect('refresh');
    
    // Check the selected countries
    var countries = $('#selectedCountries').val().split(',');
    if (countries != null && countries != '' && countries != undefined) {

        for (var j = 0; j < countries.length; j++) {
            $('#countryDropDown').find('option[value = ' + countries[j] + ']').attr({ 'selected': 'selected' });
        }
        isAllCountryChecked = 0;
    }
    $('#countryDropDown').multiselect('refresh');
   
    $('#selectedCountries').val($("#countryDropDown").val());
    $('#countryDropDown').multiselect('refresh');
           
    //Close event of country drop down
    $('#countryDropDown').multiselect({
        close: function () {
            var values = $('#countryDropDown').val();
            $('#selectedCountries').val(values);
        }
    });
    $('#countryDropDown').multiselect('refresh');

    $('#countryDropDown').multiselect({
        click: function () {
            isCountryClicked = 1;
        }
    });

    if (isAllCountryChecked == 1) {
        $('#countryDropDown').multiselect('checkAll');
        var values = $('#countryDropDown').val();
        $('#selectedCountries').val(values);
    }

}