﻿
function SetCategoriesDropDown() {
    $('#categoryDropDown').multiselect();
    //Set the position to open inside screen
    $('#categoryDropDown').multiselect('refresh');
    //Add the close event to textbox value so that event can fire
    $('#categoryDropDown').multiselect({
        close: function () {
            var values = $('#categoryDropDown').val();
            $('#selectedParentCategories').val($(this).attr("id") + ',' + values).change();
        }
    });
    //Code to retain the selected root categories on the postback
    var rootCats = $('#selectedParentCategories').val().split(',');
    if (rootCats != null && rootCats != '' && rootCats != undefined) {
        for (var j = 1; j < rootCats.length; j++) {
            //alert("in parent category" + parentCats[j]);
            $('#categoryDropDown').find('option[value = ' + rootCats[j] + ']').attr({ 'selected': 'selected' });
        }
    }
    else {
        $('#categoryDropDown').multiselect('checkAll');
        var values = $('#categoryDropDown').val();
        $('#selectedParentCategories').val($(this).attr("id") + ',' + values);
    }
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    $("#categoryDropDown").multiselect({
        noneSelectedText: "Please select category",
        checkAllText: "All Categories",
        uncheckAllText: "UnCheck All",
        position: {
            my: 'left bottom',
            at: 'left top'
        }
    });
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    $("#categoryDropDown").multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                return "All Categories";
            }
            else {
                return numChecked + ' Category(ies)';
            }
        }
    });
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    SetSubCategoriesDDL();

}

function SetSubCategoriesDDL() {

    //Configure the dynamicaaly created sub categories
    $("#DynamicCategoryDiv select").each(function () {

        $(this).multiselect();
        $(this).multiselect('refresh');

        //Code to retain the selected sub categories on the postback
        var selectedCategoryContainer = $(this).attr("id").split('-');
        var subCatArray = $('#selectedCategories-' + selectedCategoryContainer[1]).val().split(',');
        if (subCatArray != null && subCatArray != '' && subCatArray != undefined) {
            for (var j = 1; j < subCatArray.length; j++) {
                //alert("in parent category" + catArray[j]);
                $('#categoryDropDown-' + selectedCategoryContainer[1]).find('option[value = ' + subCatArray[j] + ']').attr({ 'selected': 'selected' });
            }
        }
        else {
            $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect('checkAll');
            var values = $(this).val();
            $('#selectedCategories-' + selectedCategoryContainer[1]).val($(this).attr("id") + ',' + values);
        }
        //Refresh to reflect the change on postback
        $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect('refresh');

        $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect({
            noneSelectedText: "Please select sub category",
            checkAllText: "All Categories",
            uncheckAllText: "UnCheck All",
            position: {
                my: 'left bottom',
                at: 'left top'
            }
        });

        $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect({
            selectedText: function (numChecked, numTotal, checkedItems) {
                if (numChecked == numTotal) {
                    return "All sub categories";
                }
                else {
                    return numChecked + ' sub category(ies)';
                }
            }
        });
        //Refresh to reflect the change on postback
        $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect('refresh');
        //Hide the dynamically created text boxes(used to configure the postback on sub categories
        $("#DynamicCategoryDiv input").each(function () {
            $('#' + $(this).attr("id")).css("display", "none");
        });
        //configure the close of sub categories event
        $(this).multiselect({
            close: function () {
                var values = $(this).val();
                $('#selectedCategories-' + selectedCategoryContainer[1]).val($(this).attr("id") + ',' + values).change();
            }
        });
    });
}
function SetCategoriesDropDownPartnerDetail() {
    $('#categoryDropDown').multiselect();
    //Set the position to open inside screen
    $('#categoryDropDown').multiselect('refresh');
    //Add the close event to textbox value so that event can fire
    $('#categoryDropDown').multiselect({
        close: function () {
            var values = $('#categoryDropDown').val();
            $('#selectedParentCategories').val($(this).attr("id") + ',' + values).change();
        }
    });
    //Code to retain the selected root categories on the postback
    var rootCats = $('#selectedParentCategories').val().split(',');
    if (rootCats != null && rootCats != '' && rootCats != undefined) {
        for (var j = 1; j < rootCats.length; j++) {
            //alert("in parent category" + parentCats[j]);
            $('#categoryDropDown').find('option[value = ' + rootCats[j] + ']').attr({ 'selected': 'selected' });
        }
    }
    else {
        // $('#categoryDropDown').multiselect('checkAll');
        $('#categoryDropDown').multiselect("uncheckAll");
        var values = $('#categoryDropDown').val();
        $('#selectedParentCategories').val($(this).attr("id") + ',' + values);
    }
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    $("#categoryDropDown").multiselect({
        noneSelectedText: "Please select category",
        checkAllText: "All Categories",
        uncheckAllText: "UnCheck All",
        position: {
            my: 'left bottom',
            at: 'left top'
        }
    });
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    $("#categoryDropDown").multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                return "All Categories";
            }
            else {
                return numChecked + ' Category(ies)';
            }
        }
    });
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    SetSubCategoriesDDL();

}

function SetSubCategoriesDDL() {

    //Configure the dynamicaaly created sub categories
    $("#DynamicCategoryDiv select").each(function () {

        $(this).multiselect();
        $(this).multiselect('refresh');

        //Code to retain the selected sub categories on the postback
        var selectedCategoryContainer = $(this).attr("id").split('-');
        var subCatArray = $('#selectedCategories-' + selectedCategoryContainer[1]).val().split(',');
        if (subCatArray != null && subCatArray != '' && subCatArray != undefined) {
            for (var j = 1; j < subCatArray.length; j++) {
                //alert("in parent category" + catArray[j]);
                $('#categoryDropDown-' + selectedCategoryContainer[1]).find('option[value = ' + subCatArray[j] + ']').attr({ 'selected': 'selected' });
            }
        }
        else {
            $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect('checkAll');
            var values = $(this).val();
            $('#selectedCategories-' + selectedCategoryContainer[1]).val($(this).attr("id") + ',' + values);
        }
        //Refresh to reflect the change on postback
        $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect('refresh');

        $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect({
            noneSelectedText: "Please select sub category",
            checkAllText: "All Categories",
            uncheckAllText: "UnCheck All",
            position: {
                my: 'left bottom',
                at: 'left top'
            }
        });

        $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect({
            selectedText: function (numChecked, numTotal, checkedItems) {
                if (numChecked == numTotal) {
                    return "All sub categories";
                }
                else {
                    return numChecked + ' sub category(ies)';
                }
            }
        });
        //Refresh to reflect the change on postback
        $('#categoryDropDown-' + selectedCategoryContainer[1]).multiselect('refresh');
        //Hide the dynamically created text boxes(used to configure the postback on sub categories
        $("#DynamicCategoryDiv input").each(function () {
            $('#' + $(this).attr("id")).css("display", "none");
        });
        //configure the close of sub categories event
        $(this).multiselect({
            close: function () {
                var values = $(this).val();
                $('#selectedCategories-' + selectedCategoryContainer[1]).val($(this).attr("id") + ',' + values).change();
            }
        });
    });
}

function SetcategoryDropDownEditRegionCountry() {
    $('#categoryDropDown').multiselect();
    //Set the position to open inside screen
    $('#categoryDropDown').multiselect('refresh');
    //Add the close event to textbox value so that event can fire
    $('#categoryDropDown').multiselect({
        close: function () {
            var values = $('#categoryDropDown').val();
            $('#selectedParentCategories').val(values).change();
        }
    });
    //Code to retain the selected root categories on the postback
    var rootCats = $('#selectedParentCategories').val().split(',');
    if (rootCats != null && rootCats != '' && rootCats != undefined) {
        for (var j = 1; j < rootCats.length; j++) {
            //alert("in parent category" + parentCats[j]);
            $('#categoryDropDown').find('option[value = ' + rootCats[j] + ']').attr({ 'selected': 'selected' });
        }
    }
    else {
        // $('#categoryDropDown').multiselect('checkAll');
        $('#categoryDropDown').multiselect("uncheckAll");
        var values = $('#categoryDropDown').val();
        $('#selectedParentCategories').val($(this).attr("id") + ',' + values);
    }
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    $("#categoryDropDown").multiselect({
        noneSelectedText: "Select Category(s)",
        checkAllText: "All Categories",
        uncheckAllText: "UnCheck All",
        position: {
            my: 'left bottom',
            at: 'left top'
        }
    });
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    $("#categoryDropDown").multiselect({
        selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
                return "All Categories";
            }
            else {
                return numChecked + ' Category(s)';
            }
        }
    });
    //Refresh to reflect the change on post back
    $('#categoryDropDown').multiselect('refresh');
    // SetSubCategoriesDDL();
}