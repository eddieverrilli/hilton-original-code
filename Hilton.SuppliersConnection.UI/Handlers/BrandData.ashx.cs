﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Utilities;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI.Handlers
{
    /// <summary>
    /// Summary description for BrandData
    /// </summary>
    public class BrandData : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            IList<DropDownItem> brands = PageBase.GetStaticDropDownDataSource(DropDownConstants.BrandDropDown);
            var finalBrands = new List<Brand>();

            var brandIds = context.Request.QueryString["BrandIds"] ?? string.Empty;
            var brandId = brandIds.Split(',');

            brands.Where( b => brandId.Contains(b.DataValueField)).ToList().ForEach(p =>
            {
                if (finalBrands.Any(x => x.BrandSegment == p.ParentDataField))
                {
                    finalBrands.Add(new Brand() { BrandId = Convert.ToInt32(p.DataValueField), BrandDescription = p.DataTextField, BrandSegment = p.ParentDataField });
                }
                else
                {
                    finalBrands.Add(new Brand() { BrandId = -1, BrandDescription = p.DataTextField, BrandSegment = p.ParentDataField });
                    finalBrands.Add(new Brand() { BrandId = Convert.ToInt32(p.DataValueField), BrandDescription = p.DataTextField, BrandSegment = p.ParentDataField });
                }
            });

            if (finalBrands != null)
            {
                context.Response.ContentType = "application/json";
                string json = new JavaScriptSerializer().Serialize(finalBrands);
                context.Response.Write(json);
            }
            else
            {
                context.Response.StatusCode = 404;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}