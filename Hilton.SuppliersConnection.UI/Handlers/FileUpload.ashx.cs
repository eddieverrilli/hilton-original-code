﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.Utilities;
using Newtonsoft.Json;

namespace Hilton.SuppliersConnection.UI.Admin
{
    /// <summary>
    /// Summary description for ImageUpload
    /// </summary>
    public class ImageUpload : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string fileType = context.Request.QueryString["fileType"];
                string operation = context.Request["op"];

                switch (fileType)
                {
                    case UIConstants.Image:
                        if (!string.IsNullOrEmpty(operation) && operation == "show")
                        {
                            if (context.Session[SessionConstants.ProductImageBytes] != null)
                            {
                                byte[] image = (byte[])(context.Session[SessionConstants.ProductImageBytes]);
                                image = image.ConvertToThumbnail();
                                string imgString = JsonConvert.SerializeObject(Convert.ToBase64String(image));
                                context.Response.Write(imgString);
                                context.Response.End();
                            }
                        }
                        if (context.Request.Files.Count > 0)
                        {
                            HttpContext.Current.Session[SessionConstants.ProductImageBytes] = null;
                            string fileName = string.Empty;
                            var file = context.Request.Files[0];
                            string[] allowedExtensions = { "jpg", "jpeg", "png", "gif" };

                            int fileMaxSize = int.Parse(ConfigurationStore.GetAppSetting(AppSettingConstants.ImageFileMaxSize), CultureInfo.InvariantCulture);

                            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                            {
                                string[] files = file.FileName.Split(new char[] { '\\' });
                                fileName = files[files.Length - 1];
                            }
                            else
                            {
                                fileName = file.FileName;
                            }

                            string[] imageExtension = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                            int upperBound = imageExtension.GetUpperBound(0);

                            if (allowedExtensions.Where(i => i.Equals(imageExtension[upperBound].ToLower(), StringComparison.InvariantCulture)).Count() == 0)
                            {
                                context.Response.Write(ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidImageExtension));
                                context.Response.End();
                            }
                            else
                            {
                                if (file.ContentLength < fileMaxSize * 1024 * 1024)
                                {
                                    byte[] imageBytes = ReadToEnd(file.InputStream);
                                    HttpContext.Current.Session[SessionConstants.ProductImageBytes] = imageBytes;
                                    context.Response.Write("uploaded");
                                    context.Response.End();
                                }
                                else
                                {
                                    context.Response.Write(ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidImageSize) + "  " + fileMaxSize.ToString(CultureInfo.InvariantCulture) + "MB");
                                    context.Response.End();
                                }
                            }
                        }
                        break;

                    case UIConstants.Pdf:
                        if (context.Request.Files.Count > 0)
                        {
                            HttpContext.Current.Session[SessionConstants.ProductSpecSheetPdf] = null;
                            string fileName = string.Empty;
                            var file = context.Request.Files[0];
                            string[] allowedExtensions = { "pdf" };

                            int fileMaxSize = int.Parse(ConfigurationStore.GetAppSetting(AppSettingConstants.PdfFileMaxSize), CultureInfo.InvariantCulture);

                            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                            {
                                string[] files = file.FileName.Split(new char[] { '\\' });
                                fileName = files[files.Length - 1];
                            }
                            else
                            {
                                fileName = file.FileName;
                            }

                            string[] fileExtension = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                            int upperBound = fileExtension.GetUpperBound(0);

                            if (allowedExtensions.Where(i => i.Equals(fileExtension[upperBound], StringComparison.InvariantCulture)).Count() == 0)
                            {
                                context.Response.Write(ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidPdfExtension));
                                context.Response.End();
                            }
                            else
                            {
                                if (file.ContentLength < fileMaxSize * 1024 * 1024)
                                {
                                    byte[] fileBytes = ReadToEnd(file.InputStream);
                                    HttpContext.Current.Session[SessionConstants.ProductSpecSheetPdf] = fileBytes;
                                    HttpContext.Current.Session[SessionConstants.ProductSpecSheetPdfName] = fileName;
                                    context.Response.Write("uploaded");
                                    context.Response.End();
                                }
                                else
                                {
                                    context.Response.Write(ConfigurationStore.GetApplicationMessages(MessageConstants.InvalidPDFFileSize) + "  " + fileMaxSize.ToString(CultureInfo.InvariantCulture) + "MB");
                                    context.Response.End();
                                }
                            }
                        }
                        break;
                    default:
                        context.Response.End();
                        break;
                };
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}