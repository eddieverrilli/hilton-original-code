﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.UI.BaseClasses;

namespace Hilton.SuppliersConnection.UI.Handlers
{
    /// <summary>
    /// Summary description for CountryData
    /// </summary>
    public class CountryData : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            IList<DropDownItem> countries = PageBase.GetStaticDropDownDataSource(DropDownConstants.CountryDropDown);
            var finalCountries = new List<Country>();

            var countryIds = context.Request.QueryString["CountryIds"] ?? string.Empty;
            var countryid = countryIds.Split(',');

            countries.Where(b => countryid.Contains(b.DataValueField)).ToList().ForEach(p =>
            {
                if (finalCountries.Any(x => x.RegionName == p.ParentDataField))
                {
                    finalCountries.Add(new Country() { CountryId  = Convert.ToInt32(p.DataValueField), CountryName  = p.DataTextField, RegionName  = p.ParentDataField });
                }
                else
                {
                    finalCountries.Add(new Country() { CountryId = -1, CountryName = p.DataTextField, RegionName = p.ParentDataField });
                    finalCountries.Add(new Country() { CountryId = Convert.ToInt32(p.DataValueField), CountryName = p.DataTextField, RegionName = p.ParentDataField });
                }
            });
            IList<DropDownItem> regions = PageBase.GetStaticDropDownDataSource(DropDownConstants.RegionDropDown);
           
            foreach (Country country in finalCountries)
            {
                country.RegionName = regions.FirstOrDefault(r => r.DataValueField == country.RegionName).DataTextField;     
            }

             
            if (finalCountries != null)
            {
                context.Response.ContentType = "application/json";
                string json = new JavaScriptSerializer().Serialize(finalCountries.OrderBy(c=>c.RegionName));
                context.Response.Write(json);
            }
            else
            {
                context.Response.StatusCode = 404;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}