﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebMaster.master"
    CodeBehind="PaymentResponse.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.PaymentResponse" %>

<asp:Content ID="contentContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="main-content">
        <h2>
            Payment Status
        </h2>
        <p>
            <asp:Label ID="headerDescriptionLabel" runat="server" Text=""></asp:Label></p>
    </div>
    <div class="main">
        <div class="inner-content">
            <h3>
                Payment Status</h3>
            <div class="middle-curve">
                <asp:Label ID="messageLabel" runat="server"></asp:Label>
            </div>
        </div>
</asp:Content>