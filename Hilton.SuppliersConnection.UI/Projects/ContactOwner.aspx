﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ContactOwner.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.ContactOwner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" type="text/css" href="../Styles/modal.css" />
    <title></title>
     <script  type="text/javascript">
         (function (i, s, o, g, r, a, m) {
             i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                 (i[r].q = i[r].q || []).push(arguments)
             }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
         })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

         ga('create', '<%#GoogleAnalyticsId%>', 'auto');
         ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="addCategoryScriptManager" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        $(window).ready(function () {
            $('.cancel').click(function () {
                window.parent.closeIframe();
                return false;

            });
            $('#<%= closeHyperLink.ClientID %>').click(function () {
                RefreshParent();
            });
        })

        function RefreshParent() { window.parent.location.href = window.parent.location.href; }

        function showConfirm() {
            $('#contact-owner').hide();
            $('#thankyou-message').show();

        }

        $(document).ready(function () {

            $('.word_count').each(function () {
                var input = '#' + this.id;
                var count = input + '_count';
                $(count).show();
                word_count(input, count);
                $(this).keyup(function () { word_count(input, count) });
                $(this).focus(function () { word_count(input, count) });
            });

        });

        function word_count(field, count) {

            var number = 0;
            var matches = $(field).val().match(/\b/g);
            if (matches) {
                number = matches.length / 2;
            }
            $(count).text(number);
        }

        function LimitWords(id) {
            var maxWords = 100;
            var control = document.getElementById(id);
            var input = '#' + id;
            var resultArray = jQuery.trim($(input).val()).split(/\s+/);
            if (resultArray.length > maxWords) {
                maxWordLimitMessage.style.display = 'block';
                return false;
            }
            else {
                maxWordLimitMessage.style.display = 'none';
            }
        }

        function RestrictToSendMaxLimit() {
            var maxWords = 100;
            var control = document.getElementById('commentsTextBox');
            var input = '#commentsTextBox';
            var resultArray = jQuery.trim($(input).val()).split(/\s+/);
            if (resultArray.length > maxWords)
                return false;
        }

    </script>
    <div id="contact-owner" class="modal">
        <h2>
            <asp:Label ID="contactOwnerLabel" CBID="368" runat="server" Text="Contact Project Representative"></asp:Label></h2>
        <div class="content">
            <div id="resultMessageDiv" runat="server">
            </div>
            <h3>
                <asp:Label ID="sendMessageLabel" runat="server" Text="CONTACT THE OWNER OR REPRESENTATIVE OF:"
                    CBID="1074"></asp:Label></h3>
            <asp:Label ID="addressLabel" runat="server" Text="Address will be shown here."></asp:Label>
            <h3>
                <asp:Label ID="regardingLabel" runat="server" CBID="587" Text="REGARDING:"></asp:Label></h3>
            <asp:Label ID="categoryLabel" runat="server" Text="Category Details will be shown here."></asp:Label>
            <div class="comment">
                <h3>
                    <asp:Label ID="enterCommentsLabel" runat="server" Text="message: (Limited to 100 words or less)"
                        CBID="1075" />
                </h3>
                <p>
                    <asp:TextBox ID="commentsTextBox" runat="server" onblur="LimitWords(this.id)" onkeypress="LimitWords(this.id)"
                        onfocus="if(this.value==this.defaultValue)this.value=''" class="word_count" TextMode="MultiLine"
                        Text="Enter Your Message"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="messageDetailsRequiredFieldValidator" VMTI="0" runat="server"
                        InitialValue="Enter Your Message" CssClass="errorText" ValidationGroup="ValidationGp2"
                        ErrorMessage="Please change default text" ForeColor="Red" ControlToValidate="commentsTextBox"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="messageDetailsEmptyTextRequiredFieldValidator" VMTI="0"
                        runat="server" SetFocusOnError="true" CssClass="errorText" ValidationGroup="ValidationGp2"
                        ErrorMessage="Message Detail is required" ForeColor="Red" ControlToValidate="commentsTextBox"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </p>
                <div id="maxWordLimitMessage" style="display: none;" runat="server">
                    <asp:Label ID="maxWordErrorMessageLabel" runat="server" CssClass="errorText" ForeColor="Red"
                        Text="Message Limited to 100 words or less"></asp:Label>
                </div>
                <div class="form-btn">
                    <p>
                        <asp:CheckBox ID="sendCopyToYouCheckBox" runat="server" CBID="1076" Text="  Send a copy to yourself" /><br />
                        <asp:Label ID="notificationLabel" runat="server" CBID="439" Text="All messages are logged"></asp:Label>
                    </p>
                    <asp:HyperLink ID="cancelEmailHyperLink" runat="server" CBID="269" CssClass="cancel"
                        Text="CANCEL"></asp:HyperLink>
                    <asp:LinkButton ID="sendEmailLinkButton" runat="server" CBID="662" ValidationGroup="ValidationGp2"
                        OnClientClick="return RestrictToSendMaxLimit()" OnClick="SendEmail_Click" Text="SEND"></asp:LinkButton>
                </div>
                <div class="word-count">
                    <div class="word">
                        <asp:Label ID="countLabel" runat="server"></asp:Label>
                        <div id="commentsTextBox_count" style="float: right; display: none">
                        </div>
                        &nbsp;
                        <asp:Label ID="wordCountLabel" class="word_count" runat="server"></asp:Label>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="thankyou-message" class="modal">
        <h2>
            <asp:Label ID="emailOwnerLabel" runat="server" CBID="436" Text="Message Sent"></asp:Label></h2>
        <div class="content">
            <div class="comment">
                <p>
                    <asp:Label ID="sentMessageLabel" CBID="315" runat="server"></asp:Label>
                </p>
                <div class="form-btn">
                    <asp:HyperLink ID="closeHyperLink" runat="server" CBID="77" CssClass="cancel" Text="CLOSE"></asp:HyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="contactIdHiddenValue" runat="server" />
    <asp:HiddenField ID="contactEmailHiddenValue" runat="server" />
    <asp:HiddenField ID="firstNameHiddenValue" runat="server" />
    <asp:HiddenField ID="lastNameHiddenValue" runat="server" />
    <asp:HiddenField ID="regionIdHiddenValue" runat="server" />
    <asp:HiddenField ID="brandIdHiddenValue" runat="server" />
    <asp:HiddenField ID="propTypeIdHiddenvalue" runat="server" />
    <asp:HiddenField ID="projectIdHiddenField" runat="server" />
    <asp:HiddenField ID="catIdHiddenField" runat="server" />
    <asp:HiddenField ID="prodIdHiddenField" runat="server" />
    </form>
</body>
</html>