﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
	CodeBehind="FeaturedProjectProfile.aspx.cs" ViewStateMode="Inherit" Inherits="Hilton.SuppliersConnection.UI.FeaturedProjectProfile" %>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="cphHeader" runat="server">
	<script src="../Scripts/thickbox.js" type="text/javascript"></script>
	 <script src="../Scripts/jquery.downloader.js" type="text/javascript"></script>
	<script language="javascript" type="text/javascript">
		$('.comment').hover(function () {
			$(this).children('.tooltip').show().animate({ left: '20px' });

		},
		function () {
			$(this).children('.tooltip').animate({ left: '45px' }).fadeOut();
		});
	</script>
</asp:Content>
<asp:Content ID="ContentContent" ContentPlaceHolderID="cphContent" runat="server">
	<div class="breadcrumbs">
		<asp:HyperLink ID="featuredProjectsHyperLink" runat="server" CBID="2" Text="Featured Projects"
			NavigateUrl="~/Projects/FeaturedProjects.aspx"></asp:HyperLink>
		<span>/</span>
		<asp:Label ID="breadCrumLabel" runat="server" CBID="75" Text="Project Profile"></asp:Label>
	</div>
	<div class="main-content">
		<asp:LinkButton ID="downloadSpreadsheetLinkButton" runat="server" ViewStateMode="Enabled"
			class="btn1 flR" OnClick="downloadSpreadsheetLinkButton_Click">
			<asp:Label ID="downlaodAsSpreadsheetLabel" runat="server" CBID="73" Text="Download As Spreadsheet"></asp:Label></asp:LinkButton>
		<h2>
			<asp:Label ID="brandLabel" runat="server"></asp:Label></h2>
		<h5>
			<asp:Label ID="propertyNameLabel" runat="server"></asp:Label></h5>
		<p>
			<asp:Label ID="cityLabel" runat="server"></asp:Label></p>
		<p>
			<asp:Label ID="stateLabel" runat="server"></asp:Label>     
			<asp:Label ID="postalLabel" runat="server"></asp:Label>
            <br />
            <asp:Label ID="contryLabel" runat="server"></asp:Label>
            </p>
            
		<div class="date">
			<p>
				<asp:Label ID="projectLabel" runat="server"></asp:Label>
				&nbsp;
				<asp:Label ID="openDateLabel" runat="server"></asp:Label></p>
		</div>
		<br />
		<div class="hilton-section" id="hiltonImage" runat="server">
			<asp:Label ID="projectDescriptionLabel" runat="server">
			</asp:Label>
		</div>
		<div class="date" runat="server" id="logOnDiv">
			<p>
				<h1>
					<asp:Label ID="wantToSeeMoreLabel" runat="server" CBID="80" Text="Want to see more information?"></asp:Label>
					<asp:LinkButton ID="logOnAccountLinkButton" runat="server" CssClass="alink" OnClick="LogOnAccount_Click"
						Text="Log into your account."></asp:LinkButton></h1>
			</p>
		</div>
	</div>
	<div class="main">
		<div id="resultMessageDiv" runat="server">
		</div>
		<div class="property-list-detail" id="divPropertyListDetails" runat="server">
			<asp:Repeater ID="categoryDivisionsRepeater" ViewStateMode="Enabled" runat="server"
				OnItemCommand="CategoryDivisionsRepeater_ItemCommand" OnItemDataBound="categoryDivisionsRepeater_ItemDataBound">
				<ItemTemplate>
					<div id="categoryListDivision" class="accordion" runat="server">
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</div>
	</div>
	<div class="sidebar">
		<div id="rightsidebar-wrapper">
			<div class="sidebar-box">
				<h3>
					<asp:Label ID="featuredProjectLabel" runat="server" CBID="2" CssClass="upper" Text="Featured Projects"></asp:Label></h3>
				<div class="middle">
					<ul>
						<li>
							<asp:HyperLink ID="browsePartnersHyperLink" runat="server" class="active" CBID="2"
								Text="Featured Projects" NavigateUrl="~/Projects/FeaturedProjects.aspx"></asp:HyperLink>
							<ul>
								<li>
									<asp:HyperLink ID="projectProfileHyperLink" runat="server" CBID="75" Text="Project Profile"
										class="active"></asp:HyperLink>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="box">
				<div class="title">
					<h3>
						<asp:Label ID="propertyProfileLabel" runat="server" CssClass="upper" Text="Property Profile"></asp:Label></h3>
				</div>
				<div class="content">
					<h2>
						<asp:Label ID="propertyBrandLabel" runat="server"></asp:Label></h2>
					<h4>
						<asp:Label ID="propertyNameSidePanelLabel" runat="server"></asp:Label></h4>
					<asp:Label ID="address1Label" runat="server"></asp:Label>
					<br />
					<div id="addressTwoDiv" runat="server" visible="false">
						<asp:Label ID="addressTwoLabel" runat="server"></asp:Label><br />
					</div>
					<asp:Label ID="cityStateLabel" runat="server"></asp:Label>
					<asp:Label ID="propertyZipCodeLabel" runat="server"></asp:Label>
					<br />
					<asp:Label ID="countryLabel" runat="server"></asp:Label>
					<div class="phone">
						<asp:Label ID="phoneLabel" runat="server"></asp:Label></div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('.panel div div').hide();
		$('.prod-div div').show();
		$('.accordion h3').click(function () {
			$(this).toggleClass('active');
			$(this).siblings('.panel').slideToggle();
		});

		$('.panel h4').click(function () {
			$(this).toggleClass('active');
			$(this).siblings().slideToggle();

		});
	</script>
</asp:Content>