﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class FeaturedProjects : PageBase
    {
        private FeatureProjectManager _featuredProjectManager;
        private IList<Project> _project;

        /// <summary>
        ///triggers for binding each item in the data list.
        /// </summary>
        /// <param name="sender"></param
        /// <param name="e"></param>
        protected void datalistFeaturedProperties_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                ListViewItem item = e.Item;
                if (item.ItemType == ListViewItemType.DataItem)
                {
                    LinkButton lnkImage = (LinkButton)(item.FindControl("propertyImageLinkButton"));
                    System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)(lnkImage.FindControl("propertyImage"));
                   
                    img.ImageUrl = ((Project)(e.Item.DataItem)).FeaturedImage != null ? "data:image/jpg;base64," + Convert.ToBase64String(((Project)(e.Item.DataItem)).FeaturedImage) : "../Images/noproduct.PNG";

                    Label brandLabel = item.FindControl("brandLabel") as Label;
                    brandLabel.Text = ((Project)(e.Item.DataItem)).Property.Brand;

                    Label facilityUniqueNameLabel = item.FindControl("facilityUniqueNameLabel") as Label;
                    facilityUniqueNameLabel.Text = ((Project)(e.Item.DataItem)).Property.FacilityUniqueName;

                    Label propertyTypeDescLabel = item.FindControl("propertyTypeDescLabel") as Label;
                    propertyTypeDescLabel.Text = ((Project)(e.Item.DataItem)).Property.PropertyTypeDescription;

                    Label facilityLocationLabel = item.FindControl("facilityLocationLabel") as Label;
                    facilityLocationLabel.Text = ((Project)(e.Item.DataItem)).Property.FacilityLocation.Trim() + ", ";

                    Label addressLabel = item.FindControl("addressLabel") as Label;
                    addressLabel.Text = ((Project)(e.Item.DataItem)).Property.Address;

                    Label stateAbbreviationLabel = item.FindControl("stateAbbreviationLabel") as Label;
                    stateAbbreviationLabel.Text = ((Project)(e.Item.DataItem)).Property.StateAbbreviation;

                    Label countryLabel = item.FindControl("countryLabel") as Label;
                    countryLabel.Text = ((Project)(e.Item.DataItem)).Property.Country;

                    HiddenField projectIdHidden = item.FindControl("projectIdHidden") as HiddenField;
                    projectIdHidden.Value = Convert.ToString(((Project)(e.Item.DataItem)).Property.ProjectId, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// triggers when a command is fired from any item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void datalistFeaturedProperties_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                Context.Items[UIConstants.ProjectId] = Convert.ToInt32(((HiddenField)(e.Item.FindControl("projectIdHidden"))).Value, CultureInfo.InvariantCulture);

                Server.Transfer("~/Projects/FeaturedProjectProfile.aspx", false);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///triggers when page is loaded
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _featuredProjectManager = FeatureProjectManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(1110, culture, "Featured Projects");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///triggers when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _project = _featuredProjectManager.GetFeatureProjects;
              
                rightPanelHeader.Text = ResourceUtility.GetLocalizedString(2, culture, "Featured Projects").ToUpper();

                datalistFeaturedProperties.DataSource = _project;
                datalistFeaturedProperties.DataBind();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}