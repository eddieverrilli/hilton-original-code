﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class FeaturedProjectProfile : PageBase
    {
        private int projectId = 0;
        private IFeatureProjectManager _featureProjectManager;
        private IOwnerManager _ownerManager;
        private IProjectManager _projectManager;
        private IProductManager _productManager;

        private Entities.ProjectDetail projectProfileDetails;

        /// <summary>
        ///triggers while binding each item in the repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void categoryDivisionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e != null)
                {
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {
                        HtmlGenericControl categoryDivision = (HtmlGenericControl)e.Item.FindControl("categoryListDivision");
                        var dataItem = (Entities.ProjectConfigDetails)(e.Item.DataItem);
                        if (dataItem.ParentCategoryId == -1)
                        {
                            Panel categoryPanel = new Panel();
                            HtmlGenericControl headingCategoryName = new System.Web.UI.HtmlControls.HtmlGenericControl("H3");
                            headingCategoryName.InnerText = dataItem.CategoryName;
                            Label spanOpen = new Label();
                            spanOpen.Attributes.Add("class", "acc-open tg");
                            //Need to replace with CBID code
                            spanOpen.Text = ResourceUtility.GetLocalizedString(1184, culture, "Open");
                            //Need to replace with CBID code
                            headingCategoryName.Controls.Add(spanOpen);

                            Label spanClose = new Label();
                            spanClose.Attributes.Add("class", "acc-close tg");
                            //Need to replace with CBID code
                            spanClose.Text = ResourceUtility.GetLocalizedString(1185, culture, "Close");
                            //Need to replace with CBID code
                            headingCategoryName.Controls.Add(spanClose);
                            //Literal parentCategoryName = new Literal() { Text = dataItem.CategoryName };
                            categoryPanel.Controls.Add(headingCategoryName);

                            Panel categoriesExceptRoot = new Panel();
                            categoriesExceptRoot.Attributes.Add("class", "panel");
                            categoriesExceptRoot.ID = "panel_" + dataItem.CategoryId;

                            IEnumerable<ProjectConfigDetails> projCatDetails = projectProfileDetails.ProjectCategoriesDetails.Where(p => p.RootParentId == dataItem.CategoryId).OrderBy(p => p.CategoryLevel).ThenBy(p => p.SequenceId);

                            foreach (var subCategory in projCatDetails)
                            {
                                if (subCategory.IsLeaf == false)
                                {
                                    Panel subCategoryPanel = new Panel();
                                    subCategoryPanel.ID = "panel_" + subCategory.CategoryId;
                                    HtmlGenericControl midCategoryControl = new HtmlGenericControl("H4");
                                    midCategoryControl.Style.Add("padding-left", (25 + subCategory.CategoryLevel * 10).ToString(CultureInfo.InvariantCulture) + "px");
                                    midCategoryControl.InnerHtml = subCategory.CategoryName;

                                    Label midCategoryControlSpanOpen = new Label();
                                    midCategoryControlSpanOpen.Attributes.Add("class", "acc-open tg");
                                     midCategoryControlSpanOpen.Text = ResourceUtility.GetLocalizedString(1184, culture, "Open");
                                     midCategoryControl.Controls.Add(midCategoryControlSpanOpen);

                                    Label midCategoryControlSpanClose = new Label();
                                    midCategoryControlSpanClose.Attributes.Add("class", "acc-close tg");
                                     midCategoryControlSpanClose.Text = ResourceUtility.GetLocalizedString(1185, culture, "Close");
                                      midCategoryControl.Controls.Add(midCategoryControlSpanClose);

                                      subCategoryPanel.Controls.Add(midCategoryControl);

                                    if (categoriesExceptRoot.ID.ToString(CultureInfo.InvariantCulture) == "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))
                                    {
                                        categoriesExceptRoot.Controls.Add(subCategoryPanel);
                                    }
                                    else
                                    {
                                        Panel parentPanel = new Panel();
                                        foreach (Control p in categoriesExceptRoot.Controls)
                                        {
                                            if (p is Panel)
                                            {
                                                parentPanel = (Panel)FindControl(categoriesExceptRoot, "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                                if (parentPanel != null)
                                                    parentPanel.Controls.Add(subCategoryPanel);
                                            }
                                        }
                                    }
                                }
                                else if (subCategory.IsLeaf)
                                {
                                    using (Panel subPanelDivision = new Panel())
                                    {
                                        subPanelDivision.Attributes.Add("class", "tabel2");
                                        subPanelDivision.ID = "panel_" + subCategory.CategoryId;
                                        HtmlGenericControl leafCategoryHeader;

                                        Label leafCategoryHeaderSpanOpen = new Label();
                                        leafCategoryHeaderSpanOpen.Attributes.Add("class", "acc-open tg");
                                        leafCategoryHeaderSpanOpen.Text = ResourceUtility.GetLocalizedString(1184, culture, "Open");
                             
                                        Label leafCategoryHeaderSpanClose = new Label();
                                        leafCategoryHeaderSpanClose.Attributes.Add("class", "acc-close tg");
                                        leafCategoryHeaderSpanClose.Text = ResourceUtility.GetLocalizedString(1185, culture, "Close");
                                 
                                        GridView productsGrid = null;
                                        HtmlGenericControl productGridDiv = new HtmlGenericControl("DIV");
                                        productGridDiv.Style.Add("padding-left", (25 + subCategory.CategoryLevel * 10).ToString(CultureInfo.InvariantCulture) + "px");
                                        if (subCategory.LeafCategoryDetails.PartnerProductDetails.Count > 0)
                                        {
                                            leafCategoryHeader = new HtmlGenericControl("H4");
                                            leafCategoryHeader.Style.Add("padding-left", (25 + subCategory.CategoryLevel * 10).ToString(CultureInfo.InvariantCulture) + "px");
                                            leafCategoryHeader.InnerHtml = subCategory.CategoryName;
                                    
                                            leafCategoryHeader.Controls.Add(leafCategoryHeaderSpanOpen);
                                            leafCategoryHeader.Controls.Add(leafCategoryHeaderSpanClose);

                                            productGridDiv.Attributes.Add("class", "prod-div");
                                            productsGrid = new GridView();
                                             productsGrid = CreateProductGrid(subCategory.LeafCategoryDetails.PartnerProductDetails);

                                            productGridDiv.Controls.Add(productsGrid);
                                        }
                                        else
                                        {
                                            leafCategoryHeader = new HtmlGenericControl("H4");
                                            leafCategoryHeader.Style.Add("padding-left", (25 + subCategory.CategoryLevel * 10).ToString(CultureInfo.InvariantCulture) + "px");
                                            leafCategoryHeader.InnerHtml = subCategory.CategoryName;
 
                                            productGridDiv.Attributes.Add("class", "prod-div");
                                            productsGrid = new GridView();
                                               productsGrid = CreateProductGrid(subCategory.LeafCategoryDetails.PartnerProductDetails);

                                            productGridDiv.Controls.Add(productsGrid);
                                        }

                                        subPanelDivision.Controls.Add(leafCategoryHeader);
                                        if (subCategory.LeafCategoryDetails.PartnerProductDetails.Count > 0)
                                        {
                                            subPanelDivision.Controls.Add(productGridDiv);
                                        }

                                        if (categoriesExceptRoot.ID.ToString(CultureInfo.InvariantCulture) == "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture))
                                        {
                                            categoriesExceptRoot.Controls.Add(subPanelDivision);
                                        }
                                        else
                                        {
                                            Panel pnlParent = new Panel();
                                            pnlParent.Attributes.Add("class", "tb-col3");
                                            foreach (Control p in categoriesExceptRoot.Controls)
                                            {
                                                if (p is Panel)
                                                {
                                                    pnlParent = (Panel)FindControl(categoriesExceptRoot, "panel_" + subCategory.ParentCategoryId.ToString(CultureInfo.InvariantCulture));
                                                    if (pnlParent != null)
                                                        pnlParent.Controls.Add(subPanelDivision);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            categoryPanel.Controls.Add(categoriesExceptRoot);
                            categoryDivision.Controls.Add(categoryPanel);

                            categoryPanel.Dispose();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///create a product grid  corresponding to a category
        /// </summary>
        /// <param name="partnerProduct"></param>
        /// <returns></returns>
        public GridView CreateProductGrid(IEnumerable<ProjectConfigPartnerProduct> partnerProduct)
        {
            GridView productGrid = new GridView();
            try
            {
                productGrid.CssClass = "no-border";
                productGrid.AlternatingRowStyle.CssClass = "alternate";

                productGrid.ShowHeader = false;
                productGrid.DataSource = partnerProduct;
                productGrid.AutoGenerateColumns = false;

                BoundField productNameBoundField = new BoundField();
                productNameBoundField.HeaderText = "Product Name";
                productGrid.Columns.Add(productNameBoundField);
                productGrid.Columns[0].ItemStyle.CssClass = "col1 nowidth";

                TemplateField tf = new TemplateField();
                tf.ItemTemplate = new ProductGridTemplateImage();

                productGrid.Columns.Add(tf);
                productGrid.Columns[1].ItemStyle.CssClass = "col2";

                productGrid.RowDataBound += new GridViewRowEventHandler(this.productGrid_RowDataBound);
                productGrid.DataBind();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
            return productGrid;
        }

        /// <summary>
        /// Row Command event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CategoryDivisionsRepeater_ItemCommand(object source, RepeaterCommandEventArgs args)
        {
            try
            {
                if (args != null)
                {
                    if (args.CommandName == "viewPartnerProfile")
                    {
                        SetSession<string>(SessionConstants.PartnerId, args.CommandArgument.ToString());
                        ClearSession(SessionConstants.FilterCriteria);
                        ClearSession(SessionConstants.AllCascadeCategoryIds);
                        ClearSession(SessionConstants.ProductImageBytes);
                        Response.Redirect("~/Partners/PartnerProfile.aspx");
                    }
                    else if (args.CommandName.Equals("DownloadPDF", StringComparison.InvariantCultureIgnoreCase))
                    {
                        int productId = Convert.ToInt32(args.CommandArgument, CultureInfo.InvariantCulture);

                        PartnerProductOption productPDF = _ownerManager.GetProductPdf(productId, false);
                        string fileName = productPDF.SpecSheetPdfName;
                        byte[] fileBytes = productPDF.SpecSheetPdf;
                        string contentType = "application/pdf";

                        Response.Buffer = true;
                        Response.Charset = "";
                       // Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                        Response.ClearHeaders();
                        Response.ContentType = contentType;
                        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                        Response.BinaryWrite(fileBytes);
                        Response.Flush();
                        Response.Clear();
                        Response.End();
                    }
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// triggers for data bound of each row of the product grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void productGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    User user = GetSession<User>(SessionConstants.User);
                    ProjectConfigPartnerProduct product = (ProjectConfigPartnerProduct)e.Row.DataItem;
                    LinkButton partnerNameLinkButton = (LinkButton)e.Row.FindControl("partnerNameLinkButton");
                    ImageButton imgPartnerLogo = (ImageButton)e.Row.FindControl("imgPartnerLogo");
                    Label partnerNameLabel = (Label)e.Row.FindControl("partnerNameLabel");
                    Image partnerLogoImage = (Image)e.Row.FindControl("partnerLogoImage");

                    if (product.PartnerImage != null)
                    {
                        imgPartnerLogo.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(product.PartnerImage);
                        partnerLogoImage.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(product.PartnerImage);
                        partnerNameLinkButton.Visible = false;
                        partnerNameLabel.Visible = false;
                    }
                    else
                    {
                        imgPartnerLogo.ImageUrl = "../Images/NoProduct.PNG";
                        imgPartnerLogo.Visible = false;
                        partnerLogoImage.Visible = false;
                        partnerNameLinkButton.Text = product.PartnerName.ToString(CultureInfo.InvariantCulture);
                        partnerNameLabel.Text = product.PartnerName.ToString(CultureInfo.InvariantCulture);
                    }

                    if (user != null)
                    {
                        partnerNameLabel.Visible = false;
                        partnerNameLinkButton.CommandName = "viewPartnerProfile";
                        partnerNameLinkButton.CommandArgument = product.PartnerId.ToString();

                        partnerLogoImage.Visible = false;
                        imgPartnerLogo.CommandName = "viewPartnerProfile";
                        imgPartnerLogo.CommandArgument = product.PartnerId.ToString();

                        if (product.ProductThumbnailImage != null)
                        {
                            ImageButton imgProductLogoButton = (ImageButton)e.Row.FindControl("imgProductLogoButton");
                 
                            imgProductLogoButton.OnClientClick = string.Format("$(this).DownloadImage({{productId:'{0}'}});return false;", product.ProductId);
               
                            imgProductLogoButton.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(product.ProductThumbnailImage);
                        }
                        else
                        {
                            ImageButton imgProductLogoButton = (ImageButton)e.Row.FindControl("imgProductLogoButton");
                            imgProductLogoButton.ImageUrl = "../Images/NoProduct.PNG";
                            imgProductLogoButton.OnClientClick = "return false;";
                            imgProductLogoButton.Visible = false;
                        }



                        Literal ltlDescription = (Literal)e.Row.FindControl("ltlDescription");

                        string prodSkuNoDescLabel = string.Empty;
                        if (product.SkuNumber != null || product.ProductDescription != null)
                        {
                            if (!String.IsNullOrWhiteSpace(product.SkuNumber) && !String.IsNullOrWhiteSpace(product.ProductDescription))
                            {
                                prodSkuNoDescLabel = product.SkuNumber.ToString(CultureInfo.InvariantCulture) + ", " + product.PartnerName;
                            }
                            else if (!String.IsNullOrWhiteSpace(product.SkuNumber) && String.IsNullOrWhiteSpace(product.ProductDescription))
                            {
                                prodSkuNoDescLabel = product.SkuNumber;
                            }
                            else 
                            {
                                prodSkuNoDescLabel = product.PartnerName;
                            }

                            ltlDescription.Text = @"<p class='product-name' >" + product.ProductName;
                            if (user.UserTypeId == Convert.ToInt16(UserTypeEnum.Owner) || user.UserTypeId == Convert.ToInt16(UserTypeEnum.Consultant) || user.UserTypeId == Convert.ToInt16(UserTypeEnum.Administrator) || user.UserTypeId == Convert.ToInt16(UserTypeEnum.Corporate) )
                            {
                                ltlDescription.Text += @"<span class='comment'>
                                                                            <a class='thickbox' href='/Partners/SubmitFeedback.aspx?keepThis=true&partnerName=" + product.PartnerName.ToString(CultureInfo.InvariantCulture) + @"&partnerId=" + product.PartnerId.ToString(CultureInfo.InvariantCulture) + @"&productId=" + product.ProductId.ToString(CultureInfo.InvariantCulture) + @"&toEmail=" + product.ContactEmail.ToString(CultureInfo.InvariantCulture) + @"&toFirstName=" + product.ContactFirstName.ToString(CultureInfo.InvariantCulture) + @"&toLastName=" + product.ContactLastName.ToString(CultureInfo.InvariantCulture) + @"&TB_iframe=true&height=440&width=800'>
                                                                                <img width='14' height='13' src='/images/product/comment-icon.png'>

                                                                            </a>
                                                                            <span class='tooltip'>
                                                                                <img width='110' height='19' src='/images/product/submit-feedback.png'>
                                                                            </span>
                                                                        </span>";
                            }

                            ltlDescription.Text += @"<br/>
                                        <p class='product-code'>" + prodSkuNoDescLabel + @"</p></p>";


                            if (!string.IsNullOrWhiteSpace(product.SpecSheetPDFName))
                            {
                                HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("panel");
                                LinkButton downloadLinkButton = (LinkButton)panel.FindControl("downLoadSpecSheetLinkButton");
                                downloadLinkButton.Visible = true;
                                downloadLinkButton.OnClientClick = string.Format("$(this).DownloadImage({{productId:'{0}',mode:'{1}'}});return false;", product.ProductId, "1");
                                Image pdfIconImage = (Image)panel.FindControl("pdfIconImage");
                                pdfIconImage.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        partnerNameLinkButton.Visible = false;
                        imgPartnerLogo.Visible = false;
                        partnerNameLinkButton.OnClientClick = "return false";
                        imgPartnerLogo.OnClientClick = "return false";
                    }
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Fires On click event of imglogo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgProductLogoButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imgButton = (ImageButton)sender;
                Product product = _productManager.GetProductImage(Convert.ToInt32(imgButton.CommandArgument));
                if (!string.IsNullOrEmpty(product.ImageName))
                    ProcessFileDownload("image", product.ProductImage, product.ImageName);
            }
            catch (ThreadAbortException)
            { }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        /// To Process file download
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileName"></param>
        private void ProcessFileDownload(string fileType, byte[] fileBytes, string fileName)
        {
            string contentType = string.Empty;
            string extension = string.Empty;

            switch (fileType)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;

                case "image":
                    string[] tempString = fileName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                    int upperBound = tempString.GetUpperBound(0);
                    contentType = "image/" + tempString[upperBound];
                    break;
                default:
                    contentType = "";
                    break;
            };

            Response.Buffer = true;
            Response.Charset = "";
           // Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.ClearHeaders();
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
            Response.BinaryWrite(fileBytes);
            Response.Flush();
            Response.Clear();
            Response.End();
        }

        /// <summary>
        ///triggers when page is initialized
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                _featureProjectManager = FeatureProjectManager.Instance;
                _ownerManager = OwnerManager.Instance;
                _projectManager = ProjectManager.Instance;
                _productManager = ProductManager.Instance;

                this.Title = ResourceUtility.GetLocalizedString(1109, culture, "Project Profile");
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        ///triggers when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack || (IsPostBack && (!Request["__EVENTTARGET"].ToString().ToLower().Contains("downloadspreadsheetlinkbutton"))))
                {
                    if (Context.Items[UIConstants.ProjectId] != null)
                    {
                        projectId = Convert.ToInt32(Context.Items[UIConstants.ProjectId], CultureInfo.InvariantCulture);
                        SetSession<string>(SessionConstants.ProjectId, projectId.ToString(CultureInfo.InvariantCulture));
                    }
                    else
                        projectId = Convert.ToInt32(GetSession<string>(SessionConstants.ProjectId), CultureInfo.InvariantCulture);

                    if (projectId == 0)
                    {
                        Response.Redirect("~/", false);
                    }
                    else
                    {
                        featuredProjectLabel.Text = ResourceUtility.GetLocalizedString(2, culture, "Featured Projects").ToUpper();
                        propertyProfileLabel.Text = ResourceUtility.GetLocalizedString(74, culture, "Property Profile").ToUpper();

                        User user = GetSession<User>(SessionConstants.User);
                        Project project = _featureProjectManager.GetProjectProfileDetailByProjectId(projectId);
                        if (user != null)
                        {
                            logOnDiv.Visible = false;
                            projectProfileDetails = _projectManager.GetFeaturedProjectProfile(projectId, user.UserId);
                            downloadSpreadsheetLinkButton.Visible = true;
                        }
                        else
                        {
                            projectProfileDetails = _projectManager.GetFeaturedProjectProfile(projectId);
                            downloadSpreadsheetLinkButton.Visible = false;
                        }
                        categoryDivisionsRepeater.DataSource = (projectProfileDetails.ProjectCategoriesDetails.Where(p => p.ParentCategoryId == -1)).OrderBy(p => p.SequenceId);
                        categoryDivisionsRepeater.DataBind();

                        if (projectProfileDetails.ProjectCategoriesDetails.Count == 0)
                        {
                            resultMessageDiv.Attributes.Add("class", "message notification");
                            resultMessageDiv.InnerText = ConfigurationStore.GetApplicationMessages(MessageConstants.NoCategoriesFoundForProject, culture);
                        }

                        brandLabel.Text = project.Property.Brand;
                        propertyNameLabel.Text = project.Property.FacilityUniqueName;
                        cityLabel.Text = project.Property.City;
                        stateLabel.Text = string.IsNullOrWhiteSpace(project.Property.State) ? string.Empty : project.Property.State + " ";
                        postalLabel.Text = project.Property.ZipCode;
                        contryLabel.Text = project.Property.Country;
                        projectLabel.Text = project.ProjectTypeDescription;
                        projectLabel.Font.Italic = true;
                        if (project.ProjectCompletionDate.HasValue && !string.IsNullOrEmpty(project.ProjectCompletionDate.ToString()))
                        {
                            projectLabel.Text += ", completed " + project.ProjectCompletionDate.Value.ToString("dd-MMM-yyyy");
                        }

                        if (project.ProjectTypeDescription == Convert.ToString(ProjectTypeEnum.Completed, CultureInfo.InvariantCulture))
                        {
                            openDateLabel.Text = Convert.ToString(project.ProjectAddedDate, CultureInfo.InvariantCulture);
                        }
                        if (String.IsNullOrWhiteSpace(project.FeaturedDescription))
                        {
                            hiltonImage.Visible = false;
                        }
                        else
                        {
                            projectDescriptionLabel.Text = project.FeaturedDescription;
                        }
                        propertyBrandLabel.Text = project.Property.Brand;
                        propertyNameSidePanelLabel.Text = project.Property.FacilityUniqueName;
                        address1Label.Text = project.Property.Address;
                        if (!string.IsNullOrWhiteSpace(project.Property.Address2))
                        {
                            addressTwoDiv.Visible = true;
                            addressTwoLabel.Text = project.Property.Address2;
                        }

                        cityStateLabel.Text = project.Property.City + ", " + project.Property.State;
                        propertyZipCodeLabel.Text = project.Property.ZipCode;
                        countryLabel.Text = project.Property.Country;
                        phoneLabel.Text = project.Property.PhoneNumber;
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        /// <summary>
        ///triggers when LogonAccount button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LogOnAccount_Click(Object sender, EventArgs e)
        {
            try
            {
                WebMaster webMaster = (WebMaster)Page.Master;
                webMaster.LogOn();
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
            }
        }

        /// <summary>
        ///a class for instantiating the template field for the grid
        /// </summary>
        public class ProductGridTemplateImage : ITemplate
        {
            private IProductManager _productManager;

            public ProductGridTemplateImage()
            {
                _productManager = ProductManager.Instance;
            }

            public void InstantiateIn(Control container)
            {
                try
                {
                    HtmlTable ht = new HtmlTable();
                    ht.Attributes.Add("Class", "table5");
                    HtmlTableRow hr = new HtmlTableRow();
                    HtmlTableCell c1 = new HtmlTableCell();
                    c1.Attributes.Add("Class", "tdPartner");
                    HtmlTableCell c2 = new HtmlTableCell();
                    c2.Attributes.Add("Class", "tdProduct");
                    HtmlTableCell c3 = new HtmlTableCell();
                    c3.Attributes.Add("Class", "tdDescription");

                    ImageButton imgPartnerLogo = new ImageButton();
                    imgPartnerLogo.ID = "imgPartnerLogo";
                    imgPartnerLogo.CssClass = "pic imagestyle1 pic-border";
                    c1.Controls.Add(imgPartnerLogo);

                    Image partnerLogoImage = new Image();
                    partnerLogoImage.ID = "partnerLogoImage";
                    partnerLogoImage.CssClass = "pic imagestyle1 pic-border";
                    c1.Controls.Add(partnerLogoImage);

                    LinkButton partnerNameLinkButton = new LinkButton();
                    partnerNameLinkButton.ID = "partnerNameLinkButton";
                    partnerNameLinkButton.CssClass = "featuredprofile-label";
                    c1.Controls.Add(partnerNameLinkButton);

                    Label partnerNameLabel = new Label();
                    partnerNameLabel.ID = "partnerNameLabel";
                    partnerNameLabel.CssClass = "featuredprofile-label";
                    c1.Controls.Add(partnerNameLabel);

                    //Display extended product information for logged in user
                    PageBase pageBase = new PageBase();
                    User user = pageBase.GetSession<User>(SessionConstants.User);
                    if (user != null)
                    {
                        ImageButton imgProductLogoButton = new ImageButton();
                        imgProductLogoButton.ID = "imgProductLogoButton";
                        imgProductLogoButton.CssClass = "pic imagestyle1 pic-border";
                        c2.Controls.Add(imgProductLogoButton);

                        HtmlGenericControl panel = new HtmlGenericControl("span") { ID = "panel" };

                        Literal ltlDescription = new Literal();
                        ltlDescription.ID = "ltlDescription";

                        Image ImgpdfIcon = new Image()
                        {
                            ID = "pdfIconImage",
                            ImageUrl = @"~/Images/product/Images/pdf-icon.gif",
                            Width = 11,
                            Height = 11,
                            CssClass = "product",
                            Visible = false

                        };

                        LinkButton downLoadSpecSheetLinkButton = new LinkButton()
                        {
                            ID = "downLoadSpecSheetLinkButton",
                       
                            Text = "Download Spec Sheet",
                            CssClass = "downloadPDFButton",
                            Visible = false
                        };
                       
                        panel.Controls.Add(ltlDescription);
                        panel.Controls.Add(ImgpdfIcon);
                        panel.Controls.Add(downLoadSpecSheetLinkButton);

                        c3.Controls.Add(panel);
                    }

                    hr.Controls.Add(c1);
                    hr.Controls.Add(c2);
                    hr.Controls.Add(c3);

                    ht.Controls.Add(hr);
                    container.Controls.Add(ht);
                }
                catch (Exception exception)
                {
                    UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                }
            }
        }

        /// <summary>
        ///triggers when the download link button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void downloadSpreadsheetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                User user = GetSession<User>(SessionConstants.User);
                int projId = Convert.ToInt32(GetSession<string>(SessionConstants.ProjectId), CultureInfo.InvariantCulture);
                if (user != null)
                {
                    DataSet datasetFeaturedProjectDetails = _featureProjectManager.ExportFeaturedProjectDetails(projId, user.UserId);
                    ExportToExcel(datasetFeaturedProjectDetails);
                }
                else
                {
                    DataSet datasetFeaturedProjectDetails = _featureProjectManager.ExportFeaturedProjectDetails(projId);
                    ExportToExcel(datasetFeaturedProjectDetails);
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        /// <summary>
        ///opens an excel attachment and sets up the content
        /// </summary>
        /// <param name="datasetFeaturedProjectDetails"></param>
        private void ExportToExcel(DataSet datasetFeaturedProjectDetails)
        {
            string attach = "attachment;filename=FeaturedProjectDetails.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attach);
            Response.ContentType = UIConstants.ContentType;
            if (datasetFeaturedProjectDetails.Tables != null)
            {
                GetLevelSpecificCategories(datasetFeaturedProjectDetails);
            }
            Response.End();
        }

        /// <summary>
        ///select category and writes into excel
        /// </summary>
        /// <param name="categoryCollection"></param>
        /// <param name="categoryId"></param>
        private void ExportToExcel(DataSet categoryCollection, string categoryId)
        {
            User user = GetSession<User>(SessionConstants.User);

            DataTable levelSpecificCategoryTable = null;
            DataRow[] levelSpecificCategoryRowCollection = categoryCollection.Tables[1].Select("parentCategory=" + "'" + categoryId + "'").OrderBy(p => p["SequenceId"]).ToArray(); ;
            if (levelSpecificCategoryRowCollection.GetLength(0) > 0)
            {
                levelSpecificCategoryTable = levelSpecificCategoryRowCollection.CopyToDataTable();
                foreach (DataRow categoryDetailsRow in levelSpecificCategoryTable.Rows)
                {
                    if (categoryDetailsRow["IsLeaf"].ToString() == "False")
                    {
                        Response.Write(categoryDetailsRow["CategoryName"].ToString() + "\t");
                    }
                    else
                    {
                        Response.Write("\t" + categoryDetailsRow["CategoryName"].ToString() + "\t");
                    }

                    if (categoryDetailsRow["HasProducts"].ToString() == "True" && categoryDetailsRow["IsLeaf"].ToString() == "True")
                        WriteProducts(categoryCollection.Tables[2], categoryDetailsRow["CategoryId"].ToString());
                    else
                        Response.Write("\n");

                    ExportToExcel(categoryCollection, categoryDetailsRow["CategoryId"].ToString());
                }
            }
        }

        /// <summary>
        /// write products into excel
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="catId"></param>
        private void WriteProducts(DataTable dt, string catId)
        {
            DataTable productCollection = null;
            DataRow[] partnerProductDataset = dt.Select("catId=" + "'" + catId + "'");

            if (partnerProductDataset.GetLength(0) > 0)
            {
                int i = 0;
                productCollection = partnerProductDataset.CopyToDataTable();
                foreach (DataRow dr in productCollection.Rows)
                {
                    i = i + 1;
                   Response.Write(dr["PartnerName"].ToString() + "\t");

                    User user = GetSession<User>(SessionConstants.User);
                    if (user != null)
                    {
                        Response.Write(dr["ProductName"].ToString() + "\t");
                    }
                    if (i < productCollection.Rows.Count)
                        Response.Write("\n\t\t");
                    else
                        Response.Write("\n");
                }
            }
            else
            {
                Response.Write("\n");
            }
        }

        /// <summary>
        /// write headers into excel
        /// </summary>
        private void WriteHeaders()
        {
            Response.Write("Categories");
            Response.Write("\t");
            Response.Write("Item");
            Response.Write("\t");
            Response.Write("Brand");
            User user = GetSession<User>(SessionConstants.User);
            if (user != null)
            {
                Response.Write("\t");
                Response.Write("Products");
            }
            Response.Write("\n");
        }

        /// <summary>
        /// get specific level categories to be written in excel
        /// </summary>
        /// <param name="projectTemplateDetailsDataSet"></param>
        private void GetLevelSpecificCategories(DataSet projectTemplateDetailsDataSet)
        {
            User user = GetSession<User>(SessionConstants.User);

            WriteCategoryDetails(projectTemplateDetailsDataSet.Tables[0]);
            WriteHeaders();
            DataRow[] projectTemplateDetailsDataRow = projectTemplateDetailsDataSet.Tables[1].Select("parentCategory=-1").OrderBy(p => p["SequenceId"]).ToArray();
            DataTable levelSpecificCategoryDetails = null;
            if (projectTemplateDetailsDataRow.GetLength(0) > 0)
            {
                levelSpecificCategoryDetails = projectTemplateDetailsDataRow.CopyToDataTable();
                foreach (DataRow categoryDetailsRow in levelSpecificCategoryDetails.Rows)
                {
                    if (categoryDetailsRow["IsLeaf"].ToString() == "False")
                    {
                        Response.Write(categoryDetailsRow["CategoryName"].ToString() + "\t");
                    }
                    else
                    {
                        Response.Write("\t" + categoryDetailsRow["CategoryName"].ToString() + "\t");
                    }
                    if ((bool)categoryDetailsRow["HasProducts"] && (bool)categoryDetailsRow["IsLeaf"])
                    {
                        WriteProducts(projectTemplateDetailsDataSet.Tables[2], categoryDetailsRow["CategoryId"].ToString());
                    }
                    else
                        Response.Write("\n");
                    ExportToExcel(projectTemplateDetailsDataSet, categoryDetailsRow["CategoryId"].ToString());
                }
            }
        }

        /// <summary>
        ///writes category details into excel
        /// </summary>
        /// <param name="dt"></param>
        private void WriteCategoryDetails(DataTable levelSpecificCategoryTable)
        {
            foreach (DataRow categoryDetailsRow in levelSpecificCategoryTable.Rows)
            {
                Response.Write(categoryDetailsRow["Brand"].ToString() + "\n");
                Response.Write(categoryDetailsRow["PropertyTypeName"].ToString() + "," + "  ");
                Response.Write(categoryDetailsRow["ProjectTypeName"].ToString() + "\n");
                Response.Write(categoryDetailsRow["Address"].ToString() + " ");
                Response.Write(categoryDetailsRow["Address2"].ToString() + "\n");
                Response.Write(categoryDetailsRow["City"].ToString() + "\n");
                Response.Write(categoryDetailsRow["StateName"].ToString() + "," + "  "); //CE59,89
                Response.Write(categoryDetailsRow["Zipcode"].ToString() + "\n");//CE59,89
                Response.Write(categoryDetailsRow["Country"].ToString() + "\n");

                Response.Write("\n");
                Response.Write("\n");
            }
        }
    }
}