﻿using System;
using System.Globalization;
using System.Web.UI;
using Hilton.SuppliersConnection.Business;
using Hilton.SuppliersConnection.Constants;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;
using Hilton.SuppliersConnection.UI.BaseClasses;
using Hilton.SuppliersConnection.Utilities;

namespace Hilton.SuppliersConnection.UI
{
    public partial class ContactOwner : PageBase
    {
        private IMessageManager _messageManager;
        private User currentUser;

        /// <summary>
        /// Invoked at OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                // Contact Owner page is accessible to only Gold partner because Construction report is accessible to only Gold partner
                ValidateUserAccess((int)MenuEnum.ConstructionReport);
                User user = GetSession<User>(SessionConstants.User);
                if (user != null && user.PartnershipType != (int)PartnershipTypeEnum.GoldPartner)
                {
                    Response.Redirect("~/AccessDenied.aspx", false);
                }
                _messageManager = MessageManager.Instance;

                countLabel.Text = string.Format(CultureInfo.InvariantCulture, "{0} :", ResourceUtility.GetLocalizedString(745, culture, "Word Count"));
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked at Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                currentUser = GetSession<User>(SessionConstants.User);

                if (!Page.IsPostBack)
                {
                    contactEmailHiddenValue.Value = Convert.ToString(Request.QueryString["ce"], CultureInfo.InvariantCulture);
                    addressLabel.Text = Convert.ToString(Request.QueryString["ad"], CultureInfo.InvariantCulture);
                    categoryLabel.Text = Convert.ToString(Request.QueryString["ca"], CultureInfo.InvariantCulture);
                    string regionId = Convert.ToString(Request.QueryString["r"], CultureInfo.InvariantCulture);
                    regionIdHiddenValue.Value = regionId;
                    string brandId = Convert.ToString(Request.QueryString["b"], CultureInfo.InvariantCulture);
                    brandIdHiddenValue.Value = brandId;
                    string propType = Convert.ToString(Request.QueryString["p"], CultureInfo.InvariantCulture);
                    propTypeIdHiddenvalue.Value = propType;
                    string toFirstName = Convert.ToString(Request.QueryString["f"], CultureInfo.InvariantCulture);
                    firstNameHiddenValue.Value = toFirstName;
                    string toLastName = Convert.ToString(Request.QueryString["l"], CultureInfo.InvariantCulture);
                    lastNameHiddenValue.Value = toLastName;
                    string projectId = Convert.ToString(Request.QueryString["pj"], CultureInfo.InvariantCulture);
                    projectIdHiddenField.Value = projectId;
                    string categoryId = Convert.ToString(Request.QueryString["c"], CultureInfo.InvariantCulture);
                    catIdHiddenField.Value = categoryId;
                    string productId = Convert.ToString(Request.QueryString["pr"], CultureInfo.InvariantCulture);
                    prodIdHiddenField.Value = productId;
                }
                //Bind GoogleAnalyticsId
                Page.Header.DataBind(); 
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }

        /// <summary>
        /// Invoked when sendEmail button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                Message message = new Message();
                message.SenderId = currentUser.UserId;
                message.ToEmail = contactEmailHiddenValue.Value;
                message.ToFirstName = firstNameHiddenValue.Value;
                message.ToLastName = lastNameHiddenValue.Value;

                if (sendCopyToYouCheckBox.Checked)
                {
                    message.IsCopyToSelf = true;
                }
                else
                {
                    message.IsCopyToSelf = false;
                }
                User loggedInUser = GetSession<User>(SessionConstants.User);
                message.PartnerId = loggedInUser.Company;
                message.ProductId = Convert.ToInt32(prodIdHiddenField.Value);
                message.RegionId = Convert.ToInt32(regionIdHiddenValue.Value, CultureInfo.InvariantCulture);
                message.Details = commentsTextBox.Text;
                message.SenderType = "P";
                message.StatusId = 1;
                message.BrandId = Convert.ToInt32(brandIdHiddenValue.Value);
                message.PropertyTypeId = Convert.ToInt32(propTypeIdHiddenvalue.Value, CultureInfo.InvariantCulture); ;
                message.Address = addressLabel.Text.Trim();
                message.CategoryDisplayName = categoryLabel.Text.Trim();
                message.TemplateId = 1;
                message.ProjectId = Convert.ToInt32(projectIdHiddenField.Value);
                message.CatId = Convert.ToInt32(catIdHiddenField.Value);
                message.culture = Convert.ToString(culture, CultureInfo.InvariantCulture);
                if (_messageManager.InsertMessage(message))
                {
                    Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "showConfirmationMessage", "showConfirm()", true);
                }
            }
            catch (Exception exception)
            {
                UserInterfaceExceptionHandler.HandleExcetion(ref exception);
                ConfigureResultMessage(resultMessageDiv, "message error", ConfigurationStore.GetApplicationMessages(MessageConstants.GeneralErrorMessage, culture));
            }
        }
    }
}