﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebMaster.master" AutoEventWireup="True"
    CodeBehind="FeaturedProjects.aspx.cs" Inherits="Hilton.SuppliersConnection.UI.FeaturedProjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="ContentContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="breadcrumbs">
        <asp:Label ID="pageTitleLabel" runat="server" CBID="2" Text="FEATURED PROJECTS"></asp:Label></div>
    <div class="main-content">
        <h2>
            <asp:Label ID="pageHeaderLabel" runat="server" CBID="30" Text="Browse Featured Projects"></asp:Label></h2>
        <p>
            <asp:Label ID="pageIntroLabel" runat="server" CBID="31" Text=""></asp:Label></p>
    </div>
    <div class="main">
        <div id="resultMessageDiv" runat="server">
        </div>
        <div class="property-list">
            <ul>
                <asp:ListView ID="datalistFeaturedProperties" runat="server" RepeatColumns="3" OnItemDataBound="datalistFeaturedProperties_ItemDataBound"
                    OnItemCommand="datalistFeaturedProperties_ItemCommand">
                    <ItemTemplate>
                        <li>
                            <asp:LinkButton ID="propertyImageLinkButton" runat="server">
                                <asp:Image ID="propertyImage" runat="server" Height="235" Width="235" />
                            </asp:LinkButton>
                            <div class="detail">
                                <h3>
                                    <asp:Label ID="brandLabel" runat="server"></asp:Label>
                                </h3>
                                <h4>
                                    <asp:Label ID="propertyTypeDescLabel" runat="server"></asp:Label>
                                </h4>
                                <p>
                                    <asp:Label ID="facilityUniqueNameLabel" runat="server"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="addressLabel" runat="server"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="facilityLocationLabel" runat="server"></asp:Label>
                                    <asp:Label ID="stateAbbreviationLabel" runat="server"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="countryLabel" runat="server"></asp:Label>
                                </p>
                                <asp:HiddenField ID="projectIdHidden" runat="server" />
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
            </ul>
        </div>
    </div>
    <div class="sidebar">
        <div class="sidebar-box" id="rightsidebar-wrapper">
            <h3>
                <asp:Label ID="rightPanelHeader" Text="Recommended Partners" runat="server"></asp:Label></h3>
        </div>
    </div>
</asp:Content>