﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class UserManager : IUserManager
    {
        private static UserManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private UserManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static UserManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserManager();
                }
                return _instance;
            }
        }

        /// <summary>
        ///  AddS User
        /// </summary>
        /// <param name="user"></param>
        /// <param name="createdByUserId"></param>
        /// <returns></returns>
        public int AddUser(User user, int createdByUserId)
        {
            int result = -1;
            try
            {
                result = UserDataAccess.AddUser(user, createdByUserId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets Filtered Collection
        /// </summary>
        /// <param name="searchFilterDataCollection"></param>
        /// <param name="brandSelectedValue"></param>
        /// <param name="regionSelectedValue"></param>
        /// <param name="projectTypeSelectedValue"></param>
        /// <returns></returns>
        public IList<SearchFilters> GetFilteredCollection(IList<SearchFilters> searchFilterDataCollection, int? brandSelectedValue, int? regionSelectedValue, int? projectTypeSelectedValue)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                IEnumerable<SearchFilters> filteredCollection = searchFilterDataCollection.Where(item => ((regionSelectedValue == null || regionSelectedValue == 0) ? true : item.RegionId.Equals(regionSelectedValue))
                                                                                                          && ((brandSelectedValue == null || brandSelectedValue == 0) ? true : item.BrandId.Equals(brandSelectedValue))
                                                                                                          && ((projectTypeSelectedValue == null || projectTypeSelectedValue == 0) ? true : item.ProjectTypeId.Equals(projectTypeSelectedValue)));
                if (filteredCollection != null)
                {
                    searchFilterCollection = filteredCollection.ToList<SearchFilters>();
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }

        /// <summary>
        /// Gets Images for Home page
        /// </summary>
        /// <returns></returns>
        public IList<Partner> GetImages
        {
            get
            {
                IList<Partner> partnerImageCollection = null;
                try
                {
                    partnerImageCollection = UserDataAccess.GetImages();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return partnerImageCollection;
            }
        }

        /// <summary>
        /// Gets Search Filter Items
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IList<SearchFilters> GetSearchFilterItems
        {
            get
            {
                IList<SearchFilters> searchFilterCollection = null;
                try
                {
                    searchFilterCollection = UserDataAccess.GetSearchFilterData();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return searchFilterCollection;
            }
        }

        /// <summary>
        /// Gets the user details for a given used id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public User GetUserDetails(int userId)
        {
            User user = null;
            try
            {
                user = UserDataAccess.GetUserDetails(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return user;
        }

        /// <summary>
        /// Gets the user list as per the user search
        /// </summary>
        /// <param name="userSearch"></param>
        /// <returns></returns>
        public IList<User> GetUserList(UserSearch userSearch, ref int totalRecordCount)
        {
            IList<User> userCollection = null;
            try
            {
                userCollection = UserDataAccess.GetUserList(userSearch, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userCollection;
        }

        /// <summary>
        /// Get the activation emails
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList<UserActivationEmail> GetActivationEmails(int userId)
        {
            IList<UserActivationEmail> activationEmails = null;
            try
            {
                activationEmails = UserDataAccess.GetActivationEmails(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return activationEmails;
        }

        /// <summary>

        ///// <summary>
        ///// Removes User Project Mapping
        ///// </summary>
        ///// <param name="projectId"></param>
        ///// <param name="userId"></param>
        ///// <param name="isUpdated"></param>
        ///// <returns></returns>
        //public IList<AssociatedProject> RemoveUserProjectMapping(int projectId, int userId, ref bool isUpdated)
        //{
        //    IList<AssociatedProject> associatedProjectCollection = null;
        //    try
        //    {
        //        associatedProjectCollection = UserDataAccess.RemoveUserProjectMapping(projectId, userId, ref isUpdated);
        //    }
        //    catch (Exception exception)
        //    {
        //        bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
        //        if (rethrow)
        //        {
        //            throw exception;
        //        }
        //    }
        //    return associatedProjectCollection;
        //}

        /// <summary>
        /// Search the projects for a given project type, brand and region
        /// </summary>
        /// <param name="projectTypeId"></param>
        /// <param name="brandId"></param>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public IList<AssociatedProject> SearchProject(int projectTypeId, int brandId, int regionId)
        {
            IList<AssociatedProject> associatedProjectCollection = null;
            try
            {
                associatedProjectCollection = UserDataAccess.SearchProject(projectTypeId, brandId, regionId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return associatedProjectCollection;
        }

        /// <summary>
        /// Updates User
        /// </summary>
        /// <param name="user"></param>
        /// <param name="modifiedByUserId"></param>
        /// <returns></returns>
        public int UpdateUser(User user, int modifiedByUserId)
        {
            int result = -1;
            try
            {
                result = UserDataAccess.UpdateUser(user, modifiedByUserId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Validate the user project mapping
        /// </summary>
        /// <param name="activationCode"></param>
        /// <returns></returns>
        public int ValidateUserProjectMapping(string activationCode)
        {
            int result = -1;
            try
            {
                result = UserDataAccess.ValidateUserProjectMapping(activationCode);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}