﻿using System.Collections.Generic;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IProductManager
    {
        int AddProduct(Product product, IList<ProductCategory> productCategories);

        int AddProductForReview(Product product, IList<ProductCategory> productCategories);

        IList<DropDownItem> GetCountriesForRegion(int regionId);

        IList<Country> GetCountriesForRegionForProduct(string countryarr);

        IList<ProductCategory> GetProductAssignedSpecifications(int productId);

        Product GetProductDetails(int productId);

        Product GetProductImage(int productId);

        Product GetProductDetailsForReview(int productId);

        IList<ProductCategory> GetApprovedPartnerProducts(SearchCriteria searchCriteria);

        IList<Product> GetProductList(ProductSearch productSearch, ref int totalRecordCount);

        IList<SearchFilters> GetProductSearchFilterItems(int userId);

        IList<SearchFilters> GetSearchFilterItems(int partnerId);

        int RemoveProduct(int productId);

        int UpdateProduct(Product product, IList<ProductCategory> productCategoryCollection);

        int UpdateProductForReview(Product product);
    }
}