﻿using System;
using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class MessageManager : IMessageManager
    {
        private static MessageManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private MessageManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static MessageManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MessageManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Gets Message List
        /// </summary>
        /// <param name="messageTemplate"></param>
        public DataSet ExportMessage(MessageSearch messageSearch)
        {
            DataSet messageDataSet = null;
            try
            {
                messageDataSet = MessageDataAccess.ExportMessage(messageSearch);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return messageDataSet;
        }

        /// </summary>
        /// <param name="messageSearch"></param>
        /// <returns></returns>
        public IList<Message> GetMessageList(MessageSearch messageSearch, ref int totalRecordCount)
        {
            IList<Message> messageCollection = null;
            try
            {
                messageCollection = MessageDataAccess.GetMessageList(messageSearch, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return messageCollection;
        }

        /// <summary>
        /// Updates Message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public int UpdateMessage(IList<Message> messages)
        {
            int result = -1;
            try
            {
                result = MessageDataAccess.UpdateMessage(messages);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Exports Message
        /// </summary>
        /// <param name="messageTemplate"></param>

        /// <summary>
        /// Inserts Message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool InsertMessage(Message message)
        {
            bool flag = false;
            try
            {
                flag = MessageDataAccess.InsertMessage(message);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return flag;
        }

        /// <summary>
        /// This method will get the message dropdown list for sender and recepient
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MessageDropDown GetMessageDropDownsList(int userId)
        {
            MessageDropDown messageDropDowns = null;
            try
            {
                messageDropDowns = MessageDataAccess.GetMessageDropDownsList(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return messageDropDowns;
        }
    }
}