﻿using System;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class GeneralSettingsManager : IGeneralSettingsManager
    {
        private static GeneralSettingsManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private GeneralSettingsManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static GeneralSettingsManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GeneralSettingsManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// This method Gets General Settings
        /// </summary>
        /// <param name="generalSetting"></param>
        /// <returns></returns>
        public GeneralSettings GetGeneralSettings
        {
            get
            {
                GeneralSettings generalSetting = null;
                try
                {
                    generalSetting = GeneralSettingsDataAccess.GetGeneralSettings();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return generalSetting;
            }
        }

        /// <summary>
        /// This method Updates General Settings
        /// </summary>
        /// <param name="generalSetting"></param>
        /// <returns></returns>
        public int UpdateGeneralSettings(GeneralSettings generalSetting)
        {
            int result = -1;
            try
            {
                result = GeneralSettingsDataAccess.UpdateGeneralSettings(generalSetting);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// This method update add terms and conditions version in the database
        /// </summary>
        /// <param name="generalSetting"></param>
        /// <returns></returns>
        public int AddTermsAndConditionsVersion(string termsAndCondition, int addedByUserId)
        {
            int result = 0;
            try
            {
                result = GeneralSettingsDataAccess.AddTermsAndConditionsVersion(termsAndCondition, addedByUserId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}