﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class PartnerManager : IPartnerManager
    {
        private static PartnerManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private PartnerManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static PartnerManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PartnerManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Check Duplicate Partner
        /// </summary>
        /// <param name="companyName"></param>
        /// <returns></returns>
        public int CheckDuplicatePartner(string companyName)
        {
            int result = 0;
            try
            {
                result = PartnerDataAccess.CheckDuplicatePartner(companyName);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Add new partner
        /// </summary>
        /// <param name="partner"></param>
        /// <returns></returns>
        public int AddPartner(Partner partner, IList<PartnerCategories> partnerCategories)
        {
            int result = -1;
            try
            {
                result = PartnerDataAccess.AddPartner(partner, partnerCategories);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }


        public int UpdatePartnerRegionCountries(PartnerCategories partnerCategoriesList, int userId)
        {
            int result = -1;
            try
            {
                result = PartnerDataAccess.UpdatePartnerRegionCountries(partnerCategoriesList, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
        /// <summary>
        /// Gets Approved Category Hierarchy
        /// </summary>
        /// <param name="catIds"></param>
        /// <returns></returns>
        public IList<ApprovedPartnershipSpecification> GetAppCategoryHierarchy(string catIds)
        {
            IList<ApprovedPartnershipSpecification> approvedPartnerShipSpecification = null;
            try
            {
                approvedPartnerShipSpecification = PartnerDataAccess.GetAppCategoryHierarchy(catIds);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return approvedPartnerShipSpecification;
        }

        /// <summary>
        /// Gets Approved Categories
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public DataSet GetApprovedCategories(int partnerId)
        {
            DataSet resultDataSet = null;

            try
            {
                resultDataSet = PartnerDataAccess.GetApprovedCategories(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return resultDataSet;
        }

        /// <summary>
        /// Gets Partnership Amount
        /// </summary>
        /// <returns></returns>
        public DataSet GetPartnershipAmount
        {
            get
            {
                DataSet resultDataSet = null;
                try
                {
                    resultDataSet = PartnerDataAccess.GetPartnershipAmount();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return resultDataSet;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IList<Category> GetRecommendedVendorsActiveCategories
        {
            get
            {
                IList<Category> recommendedVendorCategories = null;
                try
                {
                    recommendedVendorCategories = PartnerDataAccess.GetRecommendedVendorsActiveCategories();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return recommendedVendorCategories;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IList<Category> GetAddEditProductActiveCategories(int partnerId, int mode)
        {
            IList<Category> addEditProductCategories = null;
            try
            {
                addEditProductCategories = PartnerDataAccess.GetAddEditProductActiveCategories(partnerId, mode);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return addEditProductCategories;
        }

        /// <summary>
        /// To get the categories for Become a partner filter
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IList<Category> GetBecomePartnerCategories
        {
            get
            {
                IList<Category> browsePartnerCategories = null;
                try
                {
                    browsePartnerCategories = PartnerDataAccess.GetBecomePartnerCategories();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return browsePartnerCategories;
            }
        }

        /// <summary>
        /// Gets Approved Partner Category
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public IList<PartnerCategories> GetApprovedPartnerCategory(SearchCriteria searchCriteria)
        {
            IList<PartnerCategories> partnerCategories = null;
            try
            {
                IList<PartnerCategories> partnerCategoryDetailsTemp = PartnerDataAccess.GetApprovedPartnerCategory(searchCriteria);
                partnerCategories = new List<PartnerCategories>();

                var categories = (from approvedCategories in partnerCategoryDetailsTemp
                                  select new { approvedCategories.CategoryId }).Distinct();

                foreach (var cat in categories)
                {
                    var approvedCategoryItems = partnerCategoryDetailsTemp.Where(item => item.CategoryId.Equals(cat.CategoryId));

                    partnerCategories.Add(item: new PartnerCategories
                    {
                        CategoryDisplayName = approvedCategoryItems.First().CategoryDisplayName,
                        PartnerCategoriesDetails = GetPartnerCategoryList(approvedCategoryItems)
                    });
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerCategories;
        }

        /// <summary>
        /// Gets Categories
        /// </summary>
        /// <returns></returns>
        public IList<Category> GetCategories
        {
            get
            {
                IList<Category> categoryList = null;

                try
                {
                    categoryList = PartnerDataAccess.GetCategories();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return categoryList;
            }
        }

        /// <summary>
        /// Gets Categories For Become Partner
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="brandId"></param>
        /// <param name="propertyTypeId"></param>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public ProjectTemplate GetCategoriesForBecomePartner(int categoryId, int brandId, int regionId, int CountryId)
        {
            ProjectTemplate prjtTemplate = null;

            try
            {
                prjtTemplate = PartnerDataAccess.GetCategoriesForBecomePartner(categoryId, brandId, regionId, CountryId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return prjtTemplate;
        }

        /// <summary>
        /// Gets Categories For Submit Product
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="brandId"></param>
        /// <param name="propertyTypeId"></param>
        /// <param name="regionId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public ProjectTemplate GetCategoriesForSubmitProduct(int categoryId, int brandId, int propertyTypeId, int regionId, int countryId, int partnerId)
        {
            ProjectTemplate prjtTemplate = null;

            try
            {
                prjtTemplate = PartnerDataAccess.GetCategoriesForSubmitProduct(categoryId, brandId, propertyTypeId, regionId, countryId, partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return prjtTemplate;
        }

        /// <summary>
        /// Gets Categories For Submitted Products
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="brandId"></param>
        /// <param name="propertyTypeId"></param>
        /// <param name="regionId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public ProjectTemplate GetCategoriesForSubmittedProducts(int categoryId, int brandId, int propertyTypeId, int regionId, int countryId, int partnerId, int mode)
        {
            ProjectTemplate prjtTemplate = null;

            try
            {
                prjtTemplate = PartnerDataAccess.GetCategoriesForSubmittedProducts(categoryId, brandId, propertyTypeId, regionId, countryId, partnerId, mode);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return prjtTemplate;
        }

        /// <summary>
        /// Get the partner account status
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public AccountStatus GetPartnerAccountStatus(int partnerId, int userId)
        {
            AccountStatus accountStatus = null;
            try
            {
                accountStatus = PartnerDataAccess.GetPartnerAccountStatus(partnerId, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return accountStatus;
        }

        /// <summary>
        /// Gets Partner Category Details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IList<PartnerCategories> GetPartnerCategoryDetails(int partnerId)
        {
            IList<PartnerCategories> partnerCategoryList = null;
            try
            {
                partnerCategoryList = PartnerDataAccess.GetPartnerCategoryDetails(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerCategoryList;
        }

        /// <summary>
        /// Gets Partner Contact Details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public Contact GetPartnerContactDetails(int partnerId)
        {
            Contact contact = null;

            try
            {
                contact = PartnerDataAccess.GetPartnerContactDetails(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contact;
        }

        /// <summary>
        /// Gets Partner List
        /// </summary>
        /// <param name="partnerSearch"></param>
        /// <returns></returns>
        public IList<Partner> GetPartnerList(PartnerSearch partnerSearch, ref int totalRecordCount)
        {
            IList<Partner> partnerList = null;
            try
            {
                partnerList = PartnerDataAccess.GetPartnerList(partnerSearch, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerList;
        }

        /// <summary>
        /// Gets Partner Hot Leads
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public Tuple<IList<HotLead>, int> GetPartnerHotLeads(ConstructionReportSearch constReportSearch)
        {
            Tuple<IList<HotLead>, int> hotLeadList = null;

            try
            {
                if (constReportSearch != null)
                {
                    hotLeadList = PartnerDataAccess.GetPartnerHotLeads(constReportSearch);
                }
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return hotLeadList;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="constReportSearch"></param>
        /// <returns></returns>
        public Tuple<IList<WarmLead>, int> GetPartnerWarmLeads(ConstructionReportSearch constReportSearch)
        {
            Tuple<IList<WarmLead>, int> warmLeadList = null;

            try
            {
                if (constReportSearch != null)
                    warmLeadList = PartnerDataAccess.GetPartnerWarmLeads(constReportSearch);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return warmLeadList;
        }

        /// <summary>
        /// Gets Partner Search Result
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="totalRecordCount"></param>
        /// <returns></returns>
        public IList<PartnerSearchResult> GetPartnerSearchResult(PartnerSearch searchCriteria, ref int totalRecordCount)
        {
            IList<PartnerSearchResult> partnerSearchList = null;
            try
            {
                partnerSearchList = PartnerDataAccess.GetPartnerSearchResult(searchCriteria, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerSearchList;
        }

        /// <summary>
        /// Gets Payment Gateway Parameters
        /// </summary>
        /// <returns></returns>
        public PaymentGatewayParameters GetPaymentGatewayParameters
        {
            get
            {
                PaymentGatewayParameters paymentGatewayParam = null;
                try
                {
                    paymentGatewayParam = PartnerDataAccess.GetPaymentGatewayParameters();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return paymentGatewayParam;
            }
        }

        /// <summary>
        /// GetsPayment Request Amounts
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public DataSet GetPaymentRequestAmounts(int paymentRequestId)
        {
            DataSet dsPaymentReqAmts = null;
            try
            {
                dsPaymentReqAmts = PartnerDataAccess.GetPaymentRequestAmounts(paymentRequestId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dsPaymentReqAmts;
        }

        /// <summary>
        /// Gets Search Filter Data For Vendor
        /// </summary>
        /// <returns></returns>
        public CategoriesFilterHierarchy GetSearchFilterDataForVendor(int userId = 0)
        {
            CategoriesFilterHierarchy searchFilterDataForVendor = null;
            try
            {
                searchFilterDataForVendor = PartnerDataAccess.GetSearchFilterDataForVendor(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterDataForVendor;
        }

        /// <summary>
        /// Gets Search Filter Items
        /// </summary>
        /// <returns></returns>
        public CategoriesFilterHierarchy GetPartnershipOpportunitiesSearchFilterItems(int userId = 0)
        {
            CategoriesFilterHierarchy searchFilterList = null;
            try
            {
                searchFilterList = PartnerDataAccess.GetPartnershipOpportunitiesSearchFilterData(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterList;
        }

        /// <summary>
        /// GetsPartner Search Filter Items
        /// </summary>
        /// <returns></returns>
        public IList<SearchFilters> GetPartnerSearchFilterItems(int partnerId)
        {
            IList<SearchFilters> searchFilterList = null;
            try
            {
                searchFilterList = PartnerDataAccess.GetPartnerProfileSearchFilterData(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterList;
        }

        /// <summary>
        /// Gets Partner Linked Web Account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public IList<PartnerLinkedWebAccount> LinkedWebAccount(PartnerLinkedWebAccount account, ref int result)
        {
            IList<PartnerLinkedWebAccount> linkedWebAccountList = null;
            try
            {
                linkedWebAccountList = PartnerDataAccess.LinkedWebAccount(account, ref result);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return linkedWebAccountList;
        }

        /// <summary>
        /// Delete Linked Web Account
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Tuple<IList<PartnerLinkedWebAccount>, int> DeleteLinkedWebAccount(int userId, int partnerId)
        {
            Tuple<IList<PartnerLinkedWebAccount>, int> linkedWebAccountList = null;
            try
            {
                linkedWebAccountList = PartnerDataAccess.DeleteLinkedWebAccount(userId, partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return linkedWebAccountList;
        }

        /// <summary>
        /// Send the activation email for a user id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="partnerId"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public IList<PartnerLinkedWebAccount> SendActivationEmail(int userId, int partnerId, string culture, ref int result)
        {
            IList<PartnerLinkedWebAccount> linkedWebAccountList = null;
            try
            {
                linkedWebAccountList = PartnerDataAccess.SendActivationEmail(userId, partnerId, culture, ref result);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return linkedWebAccountList;
        }

        /// <summary>
        /// Sends Payment Request
        /// </summary>
        /// <param name="paymentRequest"></param>
        /// <returns></returns>
        public PaymentRequestDetails SendPaymentRequest(PaymentRequest paymentRequest)
        {
            PaymentRequestDetails paymentRequestDetails = null;
            try
            {
                paymentRequestDetails = PartnerDataAccess.SendPaymentRequest(paymentRequest);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return paymentRequestDetails;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="partnerApplication"></param>
        /// <returns></returns>
        public bool SubmitPartnerApplication(PartnerApplication partnerApplication)
        {
            return PartnerDataAccess.SubmitPartnerApplication(partnerApplication);
        }

        /// <summary>
        /// Updates Partner Payment Details
        /// </summary>
        /// <param name="paymentForm"></param>
        /// <returns></returns>
        public int UpdatePartnerPaymentDetails(PartnerPayment partnerPayment)
        {
            int result = -1;
            try
            {
                result = PartnerDataAccess.UpdatePartnerPaymentDetails(partnerPayment);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
        /// <summary>
        /// Updates Partner Payment Details
        /// </summary>
        /// <param name="paymentForm"></param>
        /// <returns></returns>
        public void LogErrorPartnerPayment(PartnerPaymentLogError partnerPaymentErrorLog)
        {
            // int result = -1;
            try
            {
                //result = PartnerDataAccess.LogErrorPartnerPayment(partnerPaymentErrorLog);
                PartnerDataAccess.LogErrorPartnerPayment(partnerPaymentErrorLog);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            //return result;
        }
        /// <summary>
        /// Update the partner
        /// </summary>
        /// <param name="partner"></param>
        /// <returns></returns>
        public int UpdatePartner(Partner partner, IList<PartnerCategories> partnerCategories)
        {
            int result = -1;
            try
            {
                result = PartnerDataAccess.UpdatePartner(partner, partnerCategories);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets Countries For Region
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public IList<DropDownItem> GetCountriesForRegion(int regionId)
        {
            IList<DropDownItem> regionItemList = null;
            try
            {
                regionItemList = PartnerDataAccess.GetCountriesForRegion(regionId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return PartnerDataAccess.GetCountriesForRegion(regionId);
        }

        /// <summary>
        /// Gets Partner Category List
        /// </summary>
        /// <param name="approvedCategoryItems"></param>
        /// <returns></returns>
        private static IList<PartnerCategoryDetails> GetPartnerCategoryList(IEnumerable<PartnerCategories> approvedCategoryItems)
        {
            IList<PartnerCategoryDetails> partnerCategoryDetails = new List<PartnerCategoryDetails>();

            foreach (var item in approvedCategoryItems)
            {
                if (!partnerCategoryDetails.Any(p => p.BrandDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].BrandDescription
                    && p.RegionDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].RegionDescription
                    && p.PropertyTypeDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].PropertyTypeDescription))
                {
                    partnerCategoryDetails.Add(new PartnerCategoryDetails
                    {
                        BrandImageUrl = ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].BrandImageUrl,
                        BrandDescription = ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].BrandDescription,
                        RegionDescription = ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].RegionDescription,
                        PropertyTypeDescription = ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].PropertyTypeDescription,
                        StyleDescription = "Dummy Text",
                        Countries = new List<Country> { new Country { CountryName = ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].Countries[0].CountryName } },
                        CategoryStandard = new List<CategoryStandard>() { new CategoryStandard() { StandardDescription = ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].StandardDescription } }
                    });
                }
                else
                {
                    var partnercategoryDetails = partnerCategoryDetails.FirstOrDefault(p => p.BrandDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].BrandDescription
                     && p.RegionDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].RegionDescription
                     && p.PropertyTypeDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].PropertyTypeDescription).CategoryStandard;

                    partnercategoryDetails.Add(
                         new CategoryStandard() { StandardDescription = ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].StandardDescription }
                     );

                    var countries = partnerCategoryDetails.FirstOrDefault(p => p.BrandDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].BrandDescription
                     && p.RegionDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].RegionDescription
                     && p.PropertyTypeDescription == ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].PropertyTypeDescription).Countries;

                    countries.Add(
                         new Country() { CountryName = ((List<PartnerCategoryDetails>)item.PartnerCategoriesDetails)[0].Countries[0].CountryName }
                     );

                }
            }

            return partnerCategoryDetails;
        }

        /// <summary>
        /// Gets the partner details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public Partner GetPartnerDetails(int partnerId)
        {
            Partner partner = null;
            try
            {
                partner = PartnerDataAccess.GetPartnerDetails(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partner;
        }

        /// <summary>
        /// Gets the partner details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public Partner GetPartnerImage(int partnerId)
        {
            Partner partner = null;
            try
            {
                partner = PartnerDataAccess.GetPartnerImage(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partner;
        }

        /// <summary>
        /// Gets Partner Profile Details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public Tuple<IList<SearchFilters>, Partner> GetPartnerProfileDetails(int partnerId, string brandId, string propertyTypeId, string regionId, string countryId, string catId)
        {
            Tuple<IList<SearchFilters>, Partner> partnerData = null;
            try
            {
                partnerData = PartnerDataAccess.GetPartnerProfileDetails(partnerId, brandId, propertyTypeId, regionId, countryId, catId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerData;
        }

        ///// <summary>
        ///// Gets Partner Approved Categories And Products
        ///// </summary>
        ///// <param name="partnerId"></param>
        ///// <param name="brandId"></param>
        ///// <param name="propertyTypeId"></param>
        ///// <param name="regionId"></param>
        ///// <returns></returns>
        //public ProjectTemplate GetPartnerAppCatAndProducts(int partnerId, int brandId = 0, int propertyTypeId = 0, int regionId = 0)
        //{
        //    ProjectTemplate projectTemplate = null;
        //    try
        //    {
        //        projectTemplate = PartnerDataAccess.GetPartnerAppCatAndProducts(partnerId, brandId, propertyTypeId, regionId);
        //    }
        //    catch (Exception exception)
        //    {
        //        bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
        //        if (rethrow)
        //        {
        //            throw exception;
        //        }
        //    }
        //    return projectTemplate;
        //}

        /// <summary>
        /// Gets Partner Approved Categories And Products
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="brandId"></param>
        /// <param name="propertyTypeId"></param>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public ProjectTemplate GetPartnerAppCatAndProducts(int partnerId, string brandId, string propertyTypeId, string regionId, string countryId, string catId)
        {
            ProjectTemplate projectTemplate = null;
            try
            {
                projectTemplate = PartnerDataAccess.GetPartnerAppCatAndProducts(partnerId, brandId, propertyTypeId, regionId, countryId, catId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectTemplate;
        }

        /// <summary>
        /// GetsPayment Code Details
        /// </summary>
        /// <param name="paymentRequestCode"></param>
        /// <returns></returns>
        public PaymentRequestDetails GetPaymentCodeDetails(string paymentRequestCode)
        {
            PaymentRequestDetails paymentRequestDetails = null;
            try
            {
                paymentRequestDetails = PartnerDataAccess.GetPaymentCodeDetails(paymentRequestCode);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return paymentRequestDetails;
        }

        /// <summary>
        /// Gets Renew Account Details
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public PartnerApplication GetRenewAccountDetails(int partnerId)
        {
            PartnerApplication partnerApp = null;
            try
            {
                partnerApp = PartnerDataAccess.GetRenewAccountDetails(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerApp;
        }

        /// <summary>
        /// Updates Partner Application
        /// </summary>
        /// <param name="partnerApplication"></param>
        /// <returns></returns>
        public bool UpdatePartnerApplication(PartnerApplication partnerApplication)
        {
            bool flag = false;
            try
            {
                flag = PartnerDataAccess.UpdatePartnerApplication(partnerApplication);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return flag;
        }


        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public DataSet GetPartnerExportReportDataSet(PartnerSearch partnerSearch = null)
        {
            //get
            //{
            DataSet dsPartnerExportReport = null;
            try
            {
                dsPartnerExportReport = PartnerDataAccess.GetPartnerExportReportDataSet(partnerSearch);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dsPartnerExportReport;
            //}
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public DataSet GetPartnerExportRptDataSetDelPartners(PartnerSearch partnerSearch = null, string delPartnersList = null)
        {
            //get
            //{
            DataSet dsPartnerExportReport = null;
            try
            {
                dsPartnerExportReport = PartnerDataAccess.GetPartnerExportRptDataSetDelPartners(partnerSearch, delPartnersList);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dsPartnerExportReport;
            //}
        }


        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public DataSet GetConstructionReportDataSet(int partnerId)
        {

            DataSet dsConstructionReport = null;
            try
            {
                dsConstructionReport = PartnerDataAccess.GetConstructionReportDataSet(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return dsConstructionReport;

        }

        /// <summary>
        /// Gets Partner Accessibility On Submit Product
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public int GetPartnerAccessibilityOnSubmitProduct(int partnerId)
        {
            int accessibilityFlagValue = 0;

            try
            {
                accessibilityFlagValue = PartnerDataAccess.GetPartnerAccessibilityFlagValue(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return accessibilityFlagValue;
        }

        /// <summary>
        /// Update the account partner status details
        /// </summary>
        /// <param name="accountStatus"></param>
        /// <returns></returns>
        public int UpdatePartnerAccountStatus(AccountStatus accountStatus)
        {
            int result = -1;
            try
            {
                result = PartnerDataAccess.UpdatePartnerAccountStatus(accountStatus);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets Submit Product Search Filter Data
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public CategoriesFilterHierarchy GetSubmitProductSearchFilterData(int partnerId, int mode)
        {
            CategoriesFilterHierarchy searchFilterList = null;
            try
            {
                searchFilterList = PartnerDataAccess.GetSubmitProductSearchFilterData(partnerId, mode);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterList;
        }

        /// <summary>
        /// Gets My Products Search Filter Data
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IList<SearchFilters> GetMyProductSearchFilterData(int partnerId)
        {
            IList<SearchFilters> searchFilterList = null;
            try
            {
                searchFilterList = PartnerDataAccess.GetMyProductSearchFilterData(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterList;
        }

        public static IList<Search> SearchAutoComplete(string name_startsWith)
        {
            IList<Search> searchFilterList = null;
            try
            {
                searchFilterList = PartnerDataAccess.SearchAutoComplete(name_startsWith);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterList;
        }

        public IList<PartnerCategoryDetails> GetCategoryBrandMappingForPartner(int partnerId)
        {
            IList<PartnerCategoryDetails> categoryHierarchyList = null;
            try
            {
                categoryHierarchyList = PartnerDataAccess.GetCategoryBrandMappingForPartner(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return categoryHierarchyList;
        }

        public int DeletePartners(string PartnerIds, int UserId)
        {
            int result = 0;

            try
            {
                result = PartnerDataAccess.DeletePartners(PartnerIds, UserId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return result;
        }
    }
}