﻿using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IProjectTemplateManager
    {
        bool AddCategory(Category category, int projectTemplateId);

        bool AddPartnerProduct(CategoryPartnerProduct partnerProduct);

        bool CopyTemplate(ProjectTemplate sourceTemplate, ProjectTemplate destinationTemplate, bool categoriesWithProducts);

        DataSet ExportProjectTemplate(int projectTemplateId);

        IList<Brand> GetBrandList(int regionId);

        IList<Partner> GetActivePartnerList();

        IList<Product> GetProductList(CategoryPartnerProduct categoryPartnerProduct);

        ProjectTemplate GetProjectTemplateDetail(int projectTemplateId);

        IList<ProjectTemplate> GetProjectTemplateList(TemplateSearch projectSearch, ref int totalRecordCount);

        IList<SearchFilters> GetProjectTemplateSearchFilterItems(int userId);

        IList<PropertyType> GetPropertyTypeList(int selectedRegionId, int selectedBrandId, int sourceProjectTemplateId);

        IList<Region> GetRegionList(int userId);

        bool UpdateCategory(Category category, int projectTemplateId);

        bool UpdateCategoryListConfig(int projectTemplateId, IList<Category> categoryList);

        bool UpdateCategoryStatus(int projectTemplateId, int categoryId, bool isActive);

        bool UpdateCategoryStatusForMultipleCategories(int projectTemplateId, string categoryIds, bool isActive);

        bool UpdatePartnerProduct(CategoryPartnerProduct partnerProduct);

        int UpdateProjectTemplate(IList<ProjectTemplate> projectTemplate);

        IList<BrandRegionType> GetBrandRegionTypeList(int userId, int sourceTemplateId);

        IList<Brand> GetProjectTemplateActiveBrands();

        IList<ProjectTemplate> GetProjectTemplatesByBrandId(int selectedBrandId);

        int CopyMultipleProjectTemplate(IList<ProjectTemplate> projectTemplate, IList<Category> Categories, int copyFromBrandId, int copyToBrandId, int userId);
        
    }
}