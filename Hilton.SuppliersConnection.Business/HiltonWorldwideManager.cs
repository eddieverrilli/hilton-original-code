﻿using System;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class HiltonWorldwideManager : IHiltonWorldwideManager
    {
        private static HiltonWorldwideManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private HiltonWorldwideManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static HiltonWorldwideManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new HiltonWorldwideManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Gets HiltonWorldwideData
        /// </summary>
        /// <param name="brandCode"></param>
        /// <param name="regionCode"></param>
        /// <param name="propertyTypeCode"></param>
        /// <returns></returns>
        public HiltonWorldwide GetHiltonWorldwideData(string brandCode, string regionCode, string propertyTypeCode)
        {
            HiltonWorldwide hiltonWorldwideDetails = null;
            try
            {
                hiltonWorldwideDetails = HiltonWorldwideDataAccess.GetHiltonWorldwideData(brandCode, regionCode, propertyTypeCode);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return hiltonWorldwideDetails;
        }
    }
}