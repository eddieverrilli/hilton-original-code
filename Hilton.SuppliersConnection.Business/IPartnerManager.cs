﻿using System;
using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IPartnerManager
    {
        int AddPartner(Partner partner, IList<PartnerCategories> partnerCategories);

        int CheckDuplicatePartner(string companyName);

        DataSet GetConstructionReportDataSet(int partnerId = 0);

        DataSet GetPartnerExportReportDataSet(PartnerSearch partnerSearch=null);

        DataSet GetPartnerExportRptDataSetDelPartners(PartnerSearch partnerSearch = null, string delPartnersList = null);

        int GetPartnerAccessibilityOnSubmitProduct(int partnerId);

        Partner GetPartnerImage(int partnerId);

        IList<ApprovedPartnershipSpecification> GetAppCategoryHierarchy(string catIds);

        IList<Category> GetRecommendedVendorsActiveCategories { get; }

        IList<Category> GetAddEditProductActiveCategories(int partnerId, int mode);

        IList<Category> GetBecomePartnerCategories { get; }

        DataSet GetApprovedCategories(int partnerId);

        Tuple<IList<HotLead>, int> GetPartnerHotLeads(ConstructionReportSearch constReportSearch);

        Tuple<IList<WarmLead>, int> GetPartnerWarmLeads(ConstructionReportSearch constReportSearch);

        IList<PartnerCategories> GetApprovedPartnerCategory(SearchCriteria searchCriteria);

        IList<Category> GetCategories { get; }

        ProjectTemplate GetCategoriesForBecomePartner(int categoryId, int brandId, int regionId,int CountryDropDown);

        AccountStatus GetPartnerAccountStatus(int partnerId, int userId);

        IList<PartnerCategories> GetPartnerCategoryDetails(int partnerId);

        Contact GetPartnerContactDetails(int partnerId);

        IList<Partner> GetPartnerList(PartnerSearch partnerSearch, ref int totalRecordCount);

        IList<PartnerSearchResult> GetPartnerSearchResult(PartnerSearch searchCriteria, ref int totalRecordCount);

        PaymentGatewayParameters GetPaymentGatewayParameters { get; }

        DataSet GetPaymentRequestAmounts(int paymentRequestId);

        IList<SearchFilters> GetMyProductSearchFilterData(int partnerId);

        CategoriesFilterHierarchy GetSearchFilterDataForVendor(int userId = 0);

        CategoriesFilterHierarchy GetPartnershipOpportunitiesSearchFilterItems(int userId = 0);

        CategoriesFilterHierarchy GetSubmitProductSearchFilterData(int partnerId, int mode);

        IList<SearchFilters> GetPartnerSearchFilterItems(int partnerId);

        IList<PartnerLinkedWebAccount> LinkedWebAccount(PartnerLinkedWebAccount account, ref int result);

        PaymentRequestDetails SendPaymentRequest(PaymentRequest paymentRequest);

        PaymentRequestDetails GetPaymentCodeDetails(string paymentRequestCode);

        bool SubmitPartnerApplication(PartnerApplication partnerApplication);

        int UpdatePartnerPaymentDetails(PartnerPayment partnerPayment);

        void LogErrorPartnerPayment(PartnerPaymentLogError partnerPayment);

        Partner GetPartnerDetails(int partnerId);

        int UpdatePartner(Partner partner, IList<PartnerCategories> partnerCategories);

        IList<DropDownItem> GetCountriesForRegion(int regionId);

        DataSet GetPartnershipAmount { get; }

        Tuple<IList<SearchFilters>, Partner> GetPartnerProfileDetails(int partnerId, string brandId, string propertyTypeId, string regionId, string countryId, string catId);

       // ProjectTemplate GetPartnerAppCatAndProducts(int partnerId, int brandId = 0, int propertyTypeId = 0, int regionId = 0);

        ProjectTemplate GetPartnerAppCatAndProducts(int partnerId, string brandId , string propertyTypeId , string regionId ,string countryId, string catId );

        ProjectTemplate GetCategoriesForSubmitProduct(int categoryId, int brandId, int propertyTypeId, int regionId,int countryId, int partnerId);

        ProjectTemplate GetCategoriesForSubmittedProducts(int categoryId, int brandId, int propertyTypeId, int regionId,int countryId, int partnerId, int mode);

        PartnerApplication GetRenewAccountDetails(int partnerId);

        bool UpdatePartnerApplication(PartnerApplication partnerApplication);

        int UpdatePartnerAccountStatus(AccountStatus accountStatus);

        IList<PartnerLinkedWebAccount> SendActivationEmail(int userId, int partnerId, string culture, ref int result);

        Tuple<IList<PartnerLinkedWebAccount>, int> DeleteLinkedWebAccount(int userId, int partnerId);

         IList<PartnerCategoryDetails> GetCategoryBrandMappingForPartner(int partnerId);

         int UpdatePartnerRegionCountries(PartnerCategories partnerCategoriesList, int userId);

         int DeletePartners(string PartnerIds, int UserId);
    }
}