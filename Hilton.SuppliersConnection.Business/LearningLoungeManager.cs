﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class LearningLoungeManager: ILearningLoungeManager
    {
        private static LearningLoungeManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private LearningLoungeManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static LearningLoungeManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LearningLoungeManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// To retrieve the data that supports the gallery control on the Learning Lounge homepage.
        /// </summary>
        /// <returns>A list of Gallery entities used to populate the gallery control.</returns>
        public List<LearningLoungeGallery> GetGalleryDetails(int userTypeId)
        {
            List<LearningLoungeGallery> galleryList = null;
            try
            {
                galleryList = LearningLoungeDataAccess.GetGalleryDetails(userTypeId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return galleryList;
        }

        /// <summary>
        /// To retrieve the data that supports the tile grid control on the Learning Lounge homepage.
        /// </summary>
        /// <returns>A list of LearningLoungeTile entities.</returns>
        public List<LearningLoungeTile> GetTileGridDetails(int userTypeId)
        {
            List<LearningLoungeTile> tileList = null;
            try
            {
                tileList = LearningLoungeDataAccess.GetTileGridDetails(userTypeId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return tileList;
        }
        
        /// <summary>
        /// To retrieve the data that supports the tile grid control on the Learning Lounge homepage.
        /// </summary>
        /// <returns>A list of LearningLoungeTile entities.</returns>
        public List<LearningLoungeFAQ> GetSupportCenterFAQs(int userTypeId)
        {
            List<LearningLoungeFAQ> faqList = null;
            try
            {
                faqList = LearningLoungeDataAccess.GetSupportCenterFAQs(userTypeId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return faqList;
        }

        /// <summary>
        /// To retrieve the calendar events for the learning lounge calendar page.
        /// </summary>
        /// <returns>A list of calendar events used to populate the calendar on the Learning Lounge calendar page.</returns>
        public List<LearningLoungeCalendarEvent> GetCalendarEvents(int userTypeId)
        {
            List<LearningLoungeCalendarEvent> eventList = null;
            try
            {
                eventList = LearningLoungeDataAccess.GetCalendarEvents(userTypeId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return eventList;
        }

        /// <summary>
        /// To insert a new calendar event on the learning lounge calendar page.
        /// </summary>
        /// <returns>int</returns>
        public String UpsertCalendarEvent(LearningLoungeCalendarEvent newEvent)
        {
            String returnValue;

            try
            {
                returnValue = LearningLoungeDataAccess.UpsertCalendarEvent(newEvent);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
                returnValue = "EXCEPTION";
            }

            return returnValue;
        }

        /// <summary>
        /// To retrieve the data that supports the Design Information in the Learning Lounge.
        /// </summary>
        /// <returns>A list of Design Info entities used to populate the Design Information page.</returns>
        public List<LearningLoungeDesignInfo> GetLearningLoungeDesignInfo(int designInfoId)
        {
            List<LearningLoungeDesignInfo> designInfoList = null;
            try
            {
                designInfoList = LearningLoungeDataAccess.GetDesignInfo(designInfoId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return designInfoList;
        }

        /// <summary>
        /// To insert or update an FAQ item on the learning lounge support page.
        /// </summary>
        /// <returns>int</returns>
        public String UpsertFAQ(LearningLoungeFAQ newFAQ)
        {

            try
            {
                LearningLoungeDataAccess.UpsertFAQ(newFAQ);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To delete an FAQ item on the learning lounge support page.
        /// </summary>
        /// <returns>int</returns>
        public String DeleteFAQ(int faqId)
        {
            try
            {
                LearningLoungeDataAccess.DeleteFAQ(faqId);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Add or update a feature content slider image on the Learning Lounge homepage.
        /// </summary>
        /// <returns>int</returns>
        public String UpsertGallerySliderImage(LearningLoungeGallery newItem)
        {
            try
            {
                LearningLoungeDataAccess.UpsertGallerySliderImage(newItem);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To delete an gallery image on the learning lounge home page.
        /// </summary>
        /// <returns>int</returns>
        public String DeleteGalleryImage(int galleryImageId)
        {
            try
            {
                return LearningLoungeDataAccess.DeleteGalleryImage(galleryImageId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To delete a tile image on the learning lounge home page.
        /// </summary>
        /// <returns>int</returns>
        public String DeleteTileImage(int tileImageId)
        {
            try
            {
                return LearningLoungeDataAccess.DeleteTileImage(tileImageId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Add or update a tile on the Learning Lounge homepage.
        /// </summary>
        /// <returns>int</returns>
        public String UpsertTile(LearningLoungeTile newItem)
        {
            try
            {
                LearningLoungeDataAccess.UpsertTile(newItem);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To retrieve the contact information for a map on the AD&C contacts page.
        /// </summary>
        /// <returns>LearningLoungeDirector list</returns>
        public List<LearningLoungeDirector> GetContactInfo(String mapName)
        {
            List<LearningLoungeDirector> contactList = null;
            try
            {
                contactList = LearningLoungeDataAccess.GetContactInfo(mapName);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contactList;
        }

        /// <summary>
        /// Update a contact.
        /// </summary>
        /// <returns>int</returns>
        public String UpdateContact(LearningLoungeContact contact)
        {
            try
            {
                return LearningLoungeDataAccess.UpdateContact(contact);

            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// Add new CEU Credit.
        /// </summary>
        /// <returns>SUCCESS or EXCEPTION</returns>
        public String AddCEU(LearningLoungeCEU newCEU)
        {

            try
            {
                return LearningLoungeDataAccess.AddCEU(newCEU);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }            
        }

        /// <summary>
        /// To retrieve the list of CEU Credits.
        /// </summary>
        /// <returns>A list of LearningLoungeTile entities.</returns>
        public List<LearningLoungeCEU> GetCEUCredits(int userTypeId)
        {
            List<LearningLoungeCEU> ceuList = null;
            try
            {
                ceuList = LearningLoungeDataAccess.GetCEUCredits(userTypeId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return ceuList;
        }

        /// <summary>
        /// To delete an CEU Credit on the learning lounge CEU Credits page.
        /// </summary>
        /// <returns>int</returns>
        public String DeleteCEU(int ceuCreditId)
        {
            try
            {
                LearningLoungeDataAccess.DeleteCEU(ceuCreditId);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To approve a CEU Credit on the learning lounge CEU Credits page.
        /// </summary>
        /// <returns>int</returns>
        public String ApproveCEU(int ceuCreditId)
        {
            try
            {
                LearningLoungeDataAccess.ApproveCEU(ceuCreditId);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        public String AddMeeting(LearningLoungeMeeting newMeeting)
        {
            try
            {
                return LearningLoungeDataAccess.AddMeeting(newMeeting);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }
        }

        /// <summary>
        /// To retrieve the data that supports the media center control on the Learning Lounge media center page.
        /// </summary>
        /// <returns>A list of LearningLoungeMediaCenterItem entities.</returns>
        public List<LearningLoungeMediaCenterItem> GetMediaCenterItems(int userTypeId)
        {
            List<LearningLoungeMediaCenterItem> mediaList = null;
            try
            {
                mediaList = LearningLoungeDataAccess.GetMediaCenterItems(userTypeId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return mediaList;
        }

        /// <summary>
        /// To insert or update a media center item on the learning lounge media center page.
        /// </summary>
        /// <returns>String SUCCESS or EXCEPTION</returns>
        public String UpsertMediaCenterItem(LearningLoungeMediaCenterItem newItem)
        {
            try
            {
                LearningLoungeDataAccess.UpsertMediaCenterItem(newItem);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To delete a media center item on the learning lounge media center page.
        /// </summary>
        /// <returns></returns>
        public String DeleteMediaCenterItem(int mediaCenterId)
        {
            try
            {
                return LearningLoungeDataAccess.DeleteMediaCenterItem(mediaCenterId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// To delete a calendar event on the learning lounge calendar page.
        /// </summary>
        /// <returns>int</returns>
        public String DeleteCalendarEvent(int eventId)
        {
            try
            {
                LearningLoungeDataAccess.DeleteCalendarEvent(eventId);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To retrieve the data that supports the custom templates on the Learning Lounge home page.
        /// </summary>
        /// <returns>A list of LearningLoungeTemplateContent entities.</returns>
        public List<LearningLoungeTemplateContent> GetTemplateContent(int refId)
        {
            List<LearningLoungeTemplateContent> ctList = null;
            try
            {
                ctList = LearningLoungeDataAccess.GetTemplateContent(refId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return ctList;
        }

        public LearningLoungeSupportCenterInfo GetSupportCenterInfo()
        {
            LearningLoungeSupportCenterInfo info;
            try
            {
                info = LearningLoungeDataAccess.GetSupportCenterInfo();
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
                return new LearningLoungeSupportCenterInfo();
            }
            return info;
        }

        /// <summary>
        /// Update support center info.
        /// </summary>
        /// <returns>int</returns>
        public String UpdateSupportCenterInfo(LearningLoungeSupportCenterInfo info)
        {
            try
            {
                return LearningLoungeDataAccess.UpdateSupportCenterInfo(info);

            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }

        /// <summary>
        /// To reorder an item.
        /// </summary>
        /// <returns>String</returns>
        public String UpdateDisplayOrder(int id, String itemType, int increment)
        {

            try
            {
                LearningLoungeDataAccess.UpdateDisplayOrder(id, itemType, increment);

                return "SUCCESS";
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }

                return "EXCEPTION";
            }

        }
    }
}
