﻿using System;
using System.Collections.Generic;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IProjectManager
    {
        IList<Project> GetProjectList(ProjectSearch projectSearch, ref int totalRecordCount);

        IList<SearchFilters> GetSearchFilterItems(int userId);

        IList<User> GetProjectConsultants(int projectId);

        ProjectDetail GetFeaturedProjectProfile(int projectId, int userId = 0);

        IList<User> GetProjectUsers(int projectId);

        Project GetProjectImage(int projectId);

        IList<UserProjectHistory> GetProjectUsersHistory(int projectId);

        Project GetProjectDetails(int projectId);

        IList<Category> GetCategoryListByProjectId(int projectId);

        int AddUserToProject(User user);

        bool DeleteUserFromProject(User user);

        IList<Product> GetProductByPartnerId(int partnerId);

        Tuple<IList<Partner>, IList<Product>> GetLeafCatPartners(int projectId);

        int UpdatePartnerProductLeafCategory(IList<CategoryPartnerProduct> catPartnerProductList, IList<Category> catList, Project projectDetails);

        bool UpdateProjectDetails(Project project);

        IList<Partner> GetPartnerList(CategoryPartnerProduct categoryPartnerProduct);

        IList<Product> GetProductList(CategoryPartnerProduct categoryPartnerProduct);
    }
}