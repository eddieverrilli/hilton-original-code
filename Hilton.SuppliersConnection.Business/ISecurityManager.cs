﻿using System.Collections.Generic;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface ISecurityManager
    {
        User CreateUserSession(string hiltonId, string hiltonNumber);

        IList<UserPermissions> GetMenus(int userTypeId, bool adminMenu, int userId = 0);

        bool ValidateScreenAuthorization(IList<Menu> menuAccessCollection, int menuId);

        bool ValidateAdminControlAuthorization(IList<Permission> permissionCollection, int permissionId);

        bool ValidateMenuAuthorization(IList<UserPermissions> menuCollection, int menuId);

        bool RecordTermsAndConditionAgreement(Acknowledgement termAndcondition);

        IList<Menu> UpdateMenuAccess(int userId);
    }
}