﻿using System;
using System.Collections.Generic;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class ContentBlockManager : IContentBlockManager
    {
        private static ContentBlockManager _instance;

        /// <summary>
        /// Constructor suppressed for singleton design
        /// </summary>
        private ContentBlockManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static ContentBlockManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ContentBlockManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// AddS Content Block
        /// </summary>
        /// <param name="contentBlock"></param>
        /// <returns></returns>
        public int AddContentBlock(ContentBlock contentBlock)
        {
            int result = -1;
            try
            {
                result = ContentBlockDataAccess.AddContentBlock(contentBlock);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// To Delete ContentBlock
        /// </summary>
        /// <param name="contentBlockId"></param>
        /// <returns></returns>
        public int DeleteContentBlock(int contentBlockId)
        {
            int result = -1;
            try
            {
                return ContentBlockDataAccess.DeleteContentBlock(contentBlockId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// To Get List of ContentBlocks
        /// </summary>
        /// <param name="contentSearch"></param>
        /// <returns></returns>
        public IList<ContentBlock> GetContentBlock(ContentSearch contentSearch, ref int totalRecordCount)
        {
            IList<ContentBlock> contentBlocks = null;
            try
            {
                contentBlocks = ContentBlockDataAccess.GetContentBlock(contentSearch, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contentBlocks;
        }

        /// <summary>
        /// This method get the details of content block
        /// </summary>
        /// <param name="contentBlockId"></param>
        /// <returns>ContentBlock</returns>
        public ContentBlock GetContentBlockDetails(int contentBlockId)
        {
            ContentBlock contentBlock = null;
            try
            {
                contentBlock = ContentBlockDataAccess.GetContentBlockDetails(contentBlockId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contentBlock;
        }

        /// <summary>
        /// To Get LanguageResource Details
        /// </summary>
        /// <returns></returns>
        public IList<LanguageResourceDetail> GetLanguageResourceDetails
        {
            get
            {
                IList<LanguageResourceDetail> languageResourceDetails = null;
                try
                {
                    languageResourceDetails = ContentBlockDataAccess.GetLanguageResourceDetails;
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return languageResourceDetails;
            }
        }

        /// <summary>
        /// To Update ContentBlock
        /// </summary>
        /// <param name="contentBlock"></param>
        /// <returns></returns>
        public int UpdateContentBlock(ContentBlock contentBlock)
        {
            int result = -1;
            try
            {
                result = ContentBlockDataAccess.UpdateContentBlock(contentBlock);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// To Update ContentBlocks
        /// </summary>
        /// <returns></returns>
        public static IList<ContentBlock> UpdateContentBlocks()
        {
            IList<ContentBlock> contentBlocks = null;
            try
            {
                contentBlocks = ContentBlockDataAccess.UpdateContentBlocks();
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return contentBlocks;
        }
    }
}