﻿using System.Collections.Generic;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IUserManager
    {
        int AddUser(User user, int createdByUserId);

        IList<Partner> GetImages { get; }

        IList<SearchFilters> GetFilteredCollection(IList<SearchFilters> searchFilterDataCollection, int? brandSelectedValue, int? regionSelectedValue, int? projectTypeSelectedValue);

        IList<SearchFilters> GetSearchFilterItems { get; }

        User GetUserDetails(int userId);

        IList<User> GetUserList(UserSearch userSearch, ref int totalRecordCount);

        IList<AssociatedProject> SearchProject(int projectTypeId, int brandId, int regionId);

        int UpdateUser(User user, int modifiedByUserId);

        int ValidateUserProjectMapping(string activationCode);

        IList<UserActivationEmail> GetActivationEmails(int userId);
    }
}