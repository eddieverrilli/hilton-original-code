﻿using System;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

using System.Data;

namespace Hilton.SuppliersConnection.Business
{
    public class DashboardManager : IDashboardManager
    {
        private static DashboardManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private DashboardManager()
        { }

        /// <summary>
        /// Gets the instance of the dashboard manager class
        /// </summary>
        public static DashboardManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DashboardManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Maps the data access of dashboard to UI
        /// </summary>
        /// <returns></returns>
        public Dashboard GetDashboardStatistics
        {
            get
            {
                Dashboard dashboard = null;
                try
                {
                    dashboard = DashboardDataAccess.GetDashboardStatistics;
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return dashboard;
            }
        }



        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public DataSet GetDashboardExportReportDataSet
        {
            get
            {
                DataSet dsDashboardExportReport = null;
                try
                {
                    dsDashboardExportReport = DashboardDataAccess.GetDashboardExportReportDataSet();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return dsDashboardExportReport;
            }
        }



    }
}