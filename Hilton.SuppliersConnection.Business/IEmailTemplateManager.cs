﻿using System.Collections.Generic;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IEmailTemplateManager
    {
        int AddEmailTemplate(EmailTemplate emailTemplate);

        EmailTemplate GetEmailTemplateDetails(int emailTemplateId);

        IList<EmailTemplate> GetEmailTemplates(EmailTemplateSearch emailTemplateSearch);

        int UpdateEmailTemplate(EmailTemplate emailTemplate);

        bool SendMailtoUser(EmailTemplate emailRecipient);
    }
}