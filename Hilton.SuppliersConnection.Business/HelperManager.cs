﻿using System; 
using System.Collections.Generic;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class HelperManager : IHelperManager
    {
        private static HelperManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private HelperManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static HelperManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new HelperManager();
                }
                return _instance;
            }
        }

        /// <summary>
        ///  Get Categories List for Category filter
        /// </summary>
        /// <returns></returns>
        public IList<Category> GetActiveCategories
        {
            get
            {
                IList<Category> categories = null;
                try
                {
                    categories = HelperDataAccess.GetActiveCategories();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return categories;
            }
        }

        /// <summary>
        /// Gets AutoComplete List
        /// </summary>
        /// <param name="dynamicData"></param>
        public void GetAutoCompleteList(DynamicDropDownData dynamicData)
        {
            try
            {
                HelperDataAccess.GetAutoCompleteList(dynamicData);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Gets Configuration Settings
        /// </summary>
        /// <returns></returns>
        public ConfigurationSetting GetConfigurationSettings
        {
            get
            {
                ConfigurationSetting configurationSettings = null;
                try
                {
                    configurationSettings = HelperDataAccess.GetConfigurationSettings();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return configurationSettings;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public void GetDynamicDropDownList(DynamicDropDown dropDown)
        {
            try
            {
                HelperDataAccess.GetDynamicDropDownList(dropDown);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public void GetStaticDropDownList(StaticDropDown dropDown)
        {
            try
            {
                HelperDataAccess.GetStaticDropDownList(dropDown);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dynamicData"></param>
        /// <param name="listType"></param>
        public void UpdateAutoCompleteList(DynamicDropDownData dynamicData, string listType)
        {
            try
            {
                HelperDataAccess.UpdateAutoCompleteList(dynamicData, listType);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dropDown"></param>
        public void UpdateDynamicDropDownList(DynamicDropDown dropDown, string dropDownType)
        {
            try
            {
                HelperDataAccess.UpdateDynamicDropDownList(dropDown, dropDownType);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Gets Reports Column Details
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public IList<ReportColumns> GetReportColumnDetails(int reportId)
        {
            IList<ReportColumns> reportColumnCollection = null;
            try
            {
                reportColumnCollection = HelperDataAccess.GetReportColumnDetails(reportId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }

            return reportColumnCollection;
        }

        /// <summary>
        /// To Get Regular Expression Details
        /// </summary>
        /// <returns></returns>
        public IList<RegularExpressionTypeDetails> GetRegularExpressionTypes
        {
            get
            {
                IList<RegularExpressionTypeDetails> regularExpressionTypeDetails = null;
                try
                {
                    regularExpressionTypeDetails = HelperDataAccess.GetRegularExpressionTypes();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return regularExpressionTypeDetails;
            }
        }

        /// <summary>
        ///  Gets Pdfs of a Product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public Product GetProductPdf(int productId)
        {
            Product partnerProductOption = null;

            try
            {
                partnerProductOption = HelperDataAccess.GetProductPdf(productId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerProductOption;
        }

        /// <summary>
        /// To Get list of validation messages
        /// </summary>
        /// <returns></returns>
        public IList<ValidationMessageDetails> GetValidationMessages
        {
            get
            {
                IList<ValidationMessageDetails> validationMessageDetails = null;
                try
                {
                    validationMessageDetails = HelperDataAccess.GetValidationMessages();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return validationMessageDetails;
            }
        }
    }
}