﻿using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IGeneralSettingsManager
    {
        GeneralSettings GetGeneralSettings { get; }

        int UpdateGeneralSettings(GeneralSettings generalSetting);

        int AddTermsAndConditionsVersion(string termsAndCondition, int addedByUserId);
    }
}