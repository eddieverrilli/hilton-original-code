﻿using System;
using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class OwnerManager : IOwnerManager
    {
        private static OwnerManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private OwnerManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static OwnerManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new OwnerManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Adds ProjectUser
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int AddProjectUser(User user)
        {
            int result = 0;
            try
            {
                result = OwnerDataAccess.AddProjectUser(user);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return result;
        }

        /// <summary>
        ///  Export all partner option
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public DataSet ExportAllPartnerOption(int projectId)
        {
            DataSet resultDataSet = null;

            try
            {
                resultDataSet = OwnerDataAccess.ExportAllPartnerOption(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return resultDataSet;
        }

        /// <summary>
        /// Export selected partner option
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public DataSet ExportSelectedPartners(int projectId, int userId)
        {
            DataSet resultDataSet = null;
            try
            {
                resultDataSet = OwnerDataAccess.ExportSelectedPartners(projectId, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return resultDataSet;
        }

        /// <summary>
        /// Get the project profile
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public ProjectDetail GetProjectProfile(int projectId, int userId)
        {
            ProjectDetail projectDetails = null;
            try
            {
                projectDetails = OwnerDataAccess.GetProjectProfile(projectId, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectDetails;
        }

        /// <summary>
        /// Get the project list for owner
        /// </summary>
        /// <param name="myProjectSearch"></param>
        /// <returns></returns>
        public IList<Project> GetProjectListForOwner(MyProjectsSearch myProjectSearch)
        {
            IList<Project> resultProjectList = null;

            try
            {
                resultProjectList = OwnerDataAccess.GetProjectListForOwner(myProjectSearch);
            }

            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return resultProjectList;
        }

        /// <summary>
        /// Gets Partner-Product Options
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="totalRecordCount"></param>
        /// <returns></returns>
        public IList<PartnerProductOption> GetPartnerProductOption(PartnerProdOptionSearch searchCriteria, ref int totalRecordCount)
        {
            IList<PartnerProductOption> resultPartnerProductOptionList = null;

            try
            {
                resultPartnerProductOptionList = OwnerDataAccess.GetPartnerProductOption(searchCriteria, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return resultPartnerProductOptionList;
        }

        /// <summary>
        /// Gets PartnerProductOption Details
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public PartnerProductOption GetPartnerProductOptionDetails(int categoryId, int projectId, int userId)
        {
            PartnerProductOption partnerProductOption = null;
            try
            {
                partnerProductOption = OwnerDataAccess.GetPartnerProductOptionDetails(categoryId, projectId, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerProductOption;
        }

        /// <summary>
        ///  Gets Pdfs of a Product
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="isOther"></param>
        /// <returns></returns>
        public PartnerProductOption GetProductPdf(int productId, bool isOther)
        {
            PartnerProductOption partnerProductOption = null;

            try
            {
                partnerProductOption = OwnerDataAccess.GetProductPdf(productId, isOther);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerProductOption;
        }

        /// <summary>
        /// Updates ProductSelection
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="flagSelect"></param>
        /// <param name="isNonApproved"></param>
        /// <param name="partnerId"></param>
        /// <param name="projectId"></param>
        /// <param name="catId"></param>
        public void UpdateProductSelection(int productId, bool isSelected, bool isNonApproved, int partnerId, int projectId, int catId, int updateByUserid)
        {
            try
            {
                OwnerDataAccess.UpdateProductSelection(productId, isSelected, isNonApproved, partnerId, projectId, catId, updateByUserid);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Saves NonApproved Products
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="productName"></param>
        /// <param name="manufacturer"></param>
        /// <param name="modelNumber"></param>
        /// <param name="reason"></param>
        /// <param name="categoryId"></param>
        /// <param name="specSheetName"></param>
        /// <param name="specSheet"></param>
        public void SaveNonApprovedProducts(int projectId, string productName, string manufacturer, string modelNumber, string reason, int categoryId, string specSheetName, byte[] specSheet, int updatedByUserId)
        {
            try
            {
                OwnerDataAccess.SaveNonApprovedProducts(projectId, productName, manufacturer, modelNumber, reason, categoryId, specSheetName, specSheet, updatedByUserId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Gets Supplier Connection Configuration Percentage
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public string GetSCConfigurationPercent(int projectId)
        {
            string result = string.Empty;
            try
            {
                result = OwnerDataAccess.GetSCConfigurationPercent(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Updates Project User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int UpdateProjectUser(User user)
        {
            int result = 0;
            try
            {
                result = OwnerDataAccess.UpdateProjectUser(user);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Removes Project Consultant Association
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int RemoveProjectConsultantAssociation(User user)
        {
            int result = 0;
            try
            {
                result = OwnerDataAccess.RemoveProjectConsultantAssociation(user);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}