﻿using Hilton.SuppliersConnection.Entities;
using System.Data;

namespace Hilton.SuppliersConnection.Business
{
    public interface ITransactionManager
    {
        PaymentTransactionDetails GetTransactionDetails(int paymentRecordId);

        Transaction GetTransactionList(TransactionSearch transactionSearch);

        DataSet ExportTransactions(TransactionSearch transactionSearch);

        int UpdateTransactionStatus(int paymentRecordId, string transactionStatusId, TranComment transcomment = null);

        
    }
}
