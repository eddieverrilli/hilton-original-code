﻿using System.Collections.Generic;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IFeatureProjectManager
    {
        IList<Project> GetFeatureProjects { get; }

        Project GetProjectProfileDetailByProjectId(int projectId);

        System.Data.DataSet ExportFeaturedProjectDetails(int projectId, int userId = 0);
    }
}