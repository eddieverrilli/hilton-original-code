﻿using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IFeedbackManager
    {
        bool AddFeedback(Feedback feedback);

        IList<Feedback> GetFeedbackList(FeedbackSearch feedbackSearch, ref int totalRecordCount);

        int UpdateFeedback(IList<Feedback> feedbacks);

        bool AddFeedbackComment(FeedbackComment comment);

    }
}