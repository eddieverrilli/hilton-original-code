﻿using System;
using System.Collections.Generic;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class EmailTemplateManager : IEmailTemplateManager
    {
        private static EmailTemplateManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private EmailTemplateManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static EmailTemplateManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EmailTemplateManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// It Adds new EmailTemplate
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <returns></returns>
        public int AddEmailTemplate(EmailTemplate emailTemplate)
        {
            int result = -1;
            try
            {
                result = EmailTemplateDataAccess.AddEmailTemplate(emailTemplate);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets Email Template Details
        /// </summary>
        /// <param name="emailTemplateId"></param>
        /// <returns></returns>
        public EmailTemplate GetEmailTemplateDetails(int emailTemplateId)
        {
            EmailTemplate emailTemplate = null;
            try
            {
                emailTemplate = EmailTemplateDataAccess.GetEmailTemplateDetails(emailTemplateId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return emailTemplate;
        }

        /// <summary>
        /// Gets EmailTemplates List
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <param name="totalRecordCount"></param>
        /// <returns></returns>
        public IList<EmailTemplate> GetEmailTemplates(EmailTemplateSearch emailTemplateSearch)
        {
            IList<EmailTemplate> emailTemplates = null;
            try
            {
                emailTemplates = EmailTemplateDataAccess.GetEmailTemplate(emailTemplateSearch);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return emailTemplates;
        }

        /// <summary>
        /// Updates the Email Template
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <returns></returns>
        public int UpdateEmailTemplate(EmailTemplate emailTemplate)
        {
            int result = -1;
            try
            {
                result = EmailTemplateDataAccess.UpdateEmailTemplate(emailTemplate);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Send mail to user based on selected template
        /// </summary>
        /// <param name="emailRecepient"></param>
        /// <returns></returns>
        public bool SendMailtoUser(EmailTemplate emailRecipient)
        {
            return EmailTemplateDataAccess.SendMailtoUsers(emailRecipient);
        }
    }
}