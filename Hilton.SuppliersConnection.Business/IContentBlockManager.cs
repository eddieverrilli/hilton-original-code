﻿using System.Collections.Generic;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IContentBlockManager
    {
        int AddContentBlock(ContentBlock contentBlock);

        int DeleteContentBlock(int contentBlockId);

        IList<ContentBlock> GetContentBlock(ContentSearch contentSearch, ref int totalRecordCount);

        ContentBlock GetContentBlockDetails(int contentBlockId);

        IList<LanguageResourceDetail> GetLanguageResourceDetails { get; }

        int UpdateContentBlock(ContentBlock contentBlock);
    }
}