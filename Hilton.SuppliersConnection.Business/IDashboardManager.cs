﻿using Hilton.SuppliersConnection.Entities;
using System.Data;

namespace Hilton.SuppliersConnection.Business
{
    public interface IDashboardManager
    {
        Dashboard GetDashboardStatistics { get; }

        DataSet GetDashboardExportReportDataSet { get; }
    }
}