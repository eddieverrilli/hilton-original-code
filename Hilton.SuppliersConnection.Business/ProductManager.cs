﻿using System;
using System.Collections.Generic;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class ProductManager : IProductManager
    {
        private static ProductManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private ProductManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static ProductManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ProductManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Adds Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public int AddProduct(Product product, IList<ProductCategory> productCategories)
        {
            int result = -1;
            try
            {
                result = ProductDataAccess.AddProduct(product, productCategories);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Adds Product For Review
        /// </summary>
        /// <param name="product"></param>
        /// <param name="productCategories"></param>
        /// <returns></returns>
        public int AddProductForReview(Product product, IList<ProductCategory> productCategories)
        {
            int result = 0;

            try
            {
                result = ProductDataAccess.AddProductForReview(product, productCategories);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets Active Sub Categories
        /// </summary>
        /// <param name="catId"></param>
        /// <returns></returns>
        //public IList<Category> GetActiveSubcategories(int catId)
        //{
        //    IList<Category> categories = null;
        //    try
        //    {
        //        IList<Category> categoryList;
        //        categoryList = ProductDataAccess.GetCategories();

        //        IEnumerable<Category> subCategory = categoryList.Where(item => item.ParentCategoryId.Equals(catId));
        //        categories = subCategory.ToList();
        //    }
        //    catch (Exception exception)
        //    {
        //        bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

        //        if (rethrow)
        //        {
        //            throw exception;
        //        }
        //    }
        //    return categories;
        //}

        /// <summary>
        ///     Ges tCountries For Region
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public IList<DropDownItem> GetCountriesForRegion(int regionId)
        {
            IList<DropDownItem> regionDropDownList = null;

            try
            {
                regionDropDownList = ProductDataAccess.GetCountriesForRegion(regionId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return regionDropDownList;
        }


        public IList<Country> GetCountriesForRegionForProduct(string countryarr)
        {
            IList<Country> regionDropDownList = null;

            try
            {
                regionDropDownList = ProductDataAccess.GetCountriesForRegionAndProduct(countryarr);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return regionDropDownList;
        }
        /// <summary>
        /// Get  Approved Partner rProducts
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IList<ProductCategory> GetApprovedPartnerProducts(SearchCriteria searchCriteria)
        {
            IList<ProductCategory> productCategoryList = null;

            try
            {
                productCategoryList = ProductDataAccess.GetApprovedPartnerProducts(searchCriteria);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return productCategoryList;
        }

        /// <summary>
        /// Gets Product Assigned Specifications
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public IList<ProductCategory> GetProductAssignedSpecifications(int productId)
        {
            IList<ProductCategory> productCategoryCollection = null;
            try
            {
                productCategoryCollection = ProductDataAccess.GetProductAssignedSpecifications(productId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return productCategoryCollection;
        }

        /// <summary>
        ///  Gets Product Image
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public Product GetProductImage(int productId)
        {
            Product product = null;
            try
            {
                product = ProductDataAccess.GetProductImage(productId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }

            return product;
        }

        /// <summary>
        /// Gets Product Details
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public Product GetProductDetails(int productId)
        {
            Product product = null;
            try
            {
                product = ProductDataAccess.GetProductDetails(productId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return product;
        }

        /// <summary>
        /// Gets Product Details For Review
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public Product GetProductDetailsForReview(int productId)
        {
            Product product = null;

            try
            {
                product = ProductDataAccess.GetProductDetailsForReview(productId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return product;
        }

        /// <summary>
        /// Gets Product List
        /// </summary>
        /// <param name="productSearch"></param>
        /// <returns></returns>
        public IList<Product> GetProductList(ProductSearch productSearch, ref int totalRecordCount)
        {
            IList<Product> productCollection = null;
            try
            {
                productCollection = ProductDataAccess.GetProductList(productSearch, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return productCollection;
        }

        /// <summary>
        /// Gets Product Search Filter Items
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IList<SearchFilters> GetProductSearchFilterItems(int userId = 0)
        {
            IList<SearchFilters> searchFilters = null;
            try
            {
                searchFilters = ProductDataAccess.GetProductSearchFilterData(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        /// GetsSearch Filter Items
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IList<SearchFilters> GetSearchFilterItems(int partnerId)
        {
            IList<SearchFilters> searchFilters = null;
            try
            {
                searchFilters = ProductDataAccess.GetSearchFilterData(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        /// Removes Product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public int RemoveProduct(int productId)
        {
            int result = 0;

            try
            {
                result = ProductDataAccess.RemoveProduct(productId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return result;
        }

        /// <summary>
        /// Updates Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public int UpdateProduct(Product product, IList<ProductCategory> productCategoryCollection)
        {
            int result = -1;
            try
            {
                result = ProductDataAccess.UpdateProduct(product, productCategoryCollection);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Updates Product For Review
        /// </summary>
        /// <param name="product"></param>
        /// <param name="productCategoryCollection"></param>
        /// <returns></returns>
        public int UpdateProductForReview(Product product)
        {
            int result = 0;

            try
            {
                result = ProductDataAccess.UpdateProductForReview(product);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }

            return result;
        }
    }
}