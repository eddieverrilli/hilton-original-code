﻿using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IHiltonWorldwideManager
    {
        HiltonWorldwide GetHiltonWorldwideData(string brandCode, string regionCode, string propertyTypeCode);
    }
}