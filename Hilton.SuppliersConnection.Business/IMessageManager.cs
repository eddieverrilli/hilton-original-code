﻿using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IMessageManager
    {
        DataSet ExportMessage(MessageSearch messageSearch);

        IList<Message> GetMessageList(MessageSearch messageSearch, ref int totalRecordCount);

        int UpdateMessage(IList<Message> messages);

        bool InsertMessage(Message message);

        MessageDropDown GetMessageDropDownsList(int userId);
    }
}