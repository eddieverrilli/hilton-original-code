﻿using System.Collections.Generic;
using Hilton.SuppliersConnection.Entities; 

namespace Hilton.SuppliersConnection.Business
{
    public interface IHelperManager
    {
        IList<Category> GetActiveCategories { get; }

        void GetAutoCompleteList(DynamicDropDownData dynamicData);

        ConfigurationSetting GetConfigurationSettings { get; }

        IList<ReportColumns> GetReportColumnDetails(int reportId);

        void GetDynamicDropDownList(DynamicDropDown dropDown);

        void GetStaticDropDownList(StaticDropDown dropDown);

        void UpdateAutoCompleteList(DynamicDropDownData dynamicData, string listType);

        void UpdateDynamicDropDownList(DynamicDropDown dropDown, string dropDownType);

        IList<RegularExpressionTypeDetails> GetRegularExpressionTypes { get; }

        IList<ValidationMessageDetails> GetValidationMessages { get; }

        Product GetProductPdf(int productId);
    }
}