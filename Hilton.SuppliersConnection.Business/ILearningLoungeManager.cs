﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface ILearningLoungeManager
    {
        List<LearningLoungeGallery> GetGalleryDetails(int userTypeId);
        List<LearningLoungeTile> GetTileGridDetails(int userTypeId);
        List<LearningLoungeFAQ> GetSupportCenterFAQs(int userTypeId);
        List<LearningLoungeCalendarEvent> GetCalendarEvents(int userTypeId);
        String UpsertCalendarEvent(LearningLoungeCalendarEvent newEvent);
        List<LearningLoungeDesignInfo> GetLearningLoungeDesignInfo(int designInfoId);
        String UpsertFAQ(LearningLoungeFAQ newFAQ);
        String DeleteFAQ(int faqId);
        String UpsertGallerySliderImage(LearningLoungeGallery newItem);
        String DeleteGalleryImage(int galleryImageId);
        String DeleteTileImage(int tileImageId);
        String UpsertTile(LearningLoungeTile newItem);
        List<LearningLoungeDirector> GetContactInfo(String mapName);
        String UpdateContact(LearningLoungeContact contact);
        String AddCEU(LearningLoungeCEU newCEU);
        List<LearningLoungeCEU> GetCEUCredits(int userTypeId);
        String ApproveCEU(int ceuCreditId);
        String DeleteCEU(int ceuCreditId);
        String AddMeeting(LearningLoungeMeeting newMeeting);
        String UpsertMediaCenterItem(LearningLoungeMediaCenterItem newItem);
        List<LearningLoungeMediaCenterItem> GetMediaCenterItems(int userTypeId);
        String DeleteMediaCenterItem(int mediaCenterId);
        String DeleteCalendarEvent(int eventId);
        List<LearningLoungeTemplateContent> GetTemplateContent(int tileOrGalleryId);
        LearningLoungeSupportCenterInfo GetSupportCenterInfo();
        String UpdateSupportCenterInfo(LearningLoungeSupportCenterInfo info);
        String UpdateDisplayOrder(int id, String itemType, int increment);
    }

}
