﻿using System;
using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class FeatureProjectManager : IFeatureProjectManager
    {
        private static FeatureProjectManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private FeatureProjectManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static FeatureProjectManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new FeatureProjectManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Retreives FeatureProjects
        /// </summary>
        /// <returns></returns>
        public IList<Project> GetFeatureProjects
        {
            get
            {
                IList<Project> featuredProjectCollection = null;
                try
                {
                    featuredProjectCollection = FeatureProjectDataAccess.GetFeatureProjects();
                }
                catch (Exception exception)
                {
                    bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                    if (rethrow)
                    {
                        throw exception;
                    }
                }
                return featuredProjectCollection;
            }
        }

        /// <summary>
        /// This method Gets Project Profile Detail By ProjectId
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public Project GetProjectProfileDetailByProjectId(int projectId)
        {
            Project project = null;
            try
            {
                project = FeatureProjectDataAccess.GetProjectProfileDetailByProjectId(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return project;
        }

        /// <summary>
        /// Retrieves data for Export to Excel feature in FeaturedProject
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>

        public DataSet ExportFeaturedProjectDetails(int projectId, int userId = 0)
        {
            DataSet datasetFeaturedProjectDetails = null;
            try
            {
                datasetFeaturedProjectDetails = FeatureProjectDataAccess.GetFeaturedProjectDetails(projectId, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }

            return datasetFeaturedProjectDetails;
        }
    }
}