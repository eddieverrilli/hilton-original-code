﻿using System;
using System.Collections.Generic;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class ProjectManager : IProjectManager
    {
        private static ProjectManager _instance;

        /// <summary>
        ///
        /// </summary>
        private ProjectManager()
        { }

        /// <summary>
        ///
        /// </summary>
        public static ProjectManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ProjectManager();
                }
                return _instance;
            }
        }

        /// <summary>
        ///Get Project list for the search
        /// </summary>
        /// <param name="projectSearch"></param>
        /// <param name="totalRecordCount"></param>
        /// <returns name="Project"></returns>
        public IList<Project> GetProjectList(ProjectSearch projectSearch, ref int totalRecordCount)
        {
            IList<Project> projectCollection = null;
            try
            {
                projectCollection = ProjectDataAccess.GetProjectList(projectSearch, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectCollection;
        }

        /// <summary>
        ///To get list of the search Filters
        /// </summary>
        /// <returns name="SearchFilters"></returns>
        public IList<SearchFilters> GetSearchFilterItems(int userId)
        {
            IList<SearchFilters> searchFilters = null;
            try
            {
                searchFilters = ProjectDataAccess.GetSearchFilterData(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilters;
        }

        /// <summary>
        ///Get list of the user by project Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="User"></returns>
        public IList<User> GetProjectUsers(int projectId)
        {
            IList<User> userCollection = null;
            try
            {
                userCollection = ProjectDataAccess.GetProjectUsers(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userCollection;
        }

        /// <summary>
        ///Get the list of the user project mapping history
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns  name="UserProjectHistory"></returns>
        public IList<UserProjectHistory> GetProjectUsersHistory(int projectId)
        {
            IList<UserProjectHistory> userCollection = null;
            try
            {
                userCollection = ProjectDataAccess.GetProjectUsersHistory(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userCollection;
        }

        /// <summary>
        ///Get the project details by project Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="projectDetails"></returns>
        public Project GetProjectDetails(int projectId)
        {
            Project projectDetails = null;
            try
            {
                projectDetails = ProjectDataAccess.GetProjectDetails(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectDetails;
        }

        /// <summary>
        ///Get the project Image by project Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="projectDetails"></returns>
        public Project GetProjectImage(int projectId)
        {
            Project projectDetails = null;
            try
            {
                projectDetails = ProjectDataAccess.GetProjectImage(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectDetails;
        }

        /// <summary>
        ///Get the list of the category by the projectid
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="categoryListByProjectId"></returns>
        public IList<Category> GetCategoryListByProjectId(int projectId)
        {
            IList<Category> categoryListByProjectId = null;
            try
            {
                categoryListByProjectId = ProjectDataAccess.GetCategoryListByProjectId(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return categoryListByProjectId;
        }

        /// <summary>
        ///Get the result when user is mapped to the project
        /// </summary>
        /// <param name="user"></param>
        /// <returns name="result"></returns>
        public int AddUserToProject(User user)
        {
            int result = 0;
            try
            {
                result = ProjectDataAccess.AddUserToProject(user);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///get partner list from db
        /// </summary>
        /// <param name="categoryPartnerProduct"></param>
        /// <returns></returns>
        public IList<Partner> GetPartnerList(CategoryPartnerProduct categoryPartnerProduct)
        {
            IList<Partner> partnerCollection = null;
            try
            {
                partnerCollection = ProjectDataAccess.GetPartnerList(categoryPartnerProduct);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerCollection;
        }

        /// <summary>
        ///gets product list from db for a selected partner
        /// </summary>
        /// <param name="categoryPartnerProduct"></param>
        /// <returns></returns>
        public IList<Product> GetProductList(CategoryPartnerProduct categoryPartnerProduct)
        {
            IList<Product> productCollection = null;
            try
            {
                productCollection = ProjectDataAccess.GetProductList(categoryPartnerProduct);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return productCollection;
        }

        /// <summary>
        ///Get the result when user is unmapped from the project
        /// </summary>
        /// <param name="user"></param>
        /// <returns name="result"></returns>
        public bool DeleteUserFromProject(User user)
        {
            bool result = false;
            try
            {
                result = ProjectDataAccess.DeleteUserFromProject(user);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///Get the partner list of the leaf category
        /// </summary>
        /// <param name="user"></param>
        /// <returns name="result"></returns>
        public Tuple<IList<Partner>, IList<Product>> GetLeafCatPartners(int projectId)
        {
            Tuple<IList<Partner>, IList<Product>> partnerProductList = null;
            try
            {
                partnerProductList = ProjectDataAccess.GetLeafCatPartners(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerProductList;
        }

        /// <summary>
        ///Get the product list by partner Id
        /// </summary>
        /// <param name="user"></param>
        /// <returns name="Product"></returns>
        public IList<Product> GetProductByPartnerId(int partnerId)
        {
            IList<Product> productList = null;
            try
            {
                productList = ProjectDataAccess.GetProductByPartnerId(partnerId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return productList;
        }

        /// <summary>
        /// Get the result after the partner product mapping for the leaf category
        /// </summary>
        /// <param name="catPartnerProductList"></param>
        /// <param name="catList"></param>
        /// <returns name="result"></returns>
        public int UpdatePartnerProductLeafCategory(IList<CategoryPartnerProduct> catPartnerProductList, IList<Category> catList, Project projectDetails)
        {
            int result = 0;
            try
            {
                result = ProjectDataAccess.UpdatePartnerProductLeafCategory(catPartnerProductList, catList, projectDetails);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Get the result when project details is updated
        /// </summary>
        /// <param name="project"></param>
        /// <returns name ="result"></returns>
        public bool UpdateProjectDetails(Project project)
        {
            int result = 0;
            try
            {
                result = ProjectDataAccess.UpdateProjectDetails(project);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            if (result > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        ///Get the user list of type consultants
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns name="User"></returns>

        public IList<User> GetProjectConsultants(int projectId)
        {
            IList<User> userCollection = null;
            try
            {
                userCollection = ProjectDataAccess.GetProjectConsultants(projectId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userCollection;
        }

        /// <summary>
        ///Get the project details by project id and User Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="userId"></param>
        /// <returns name="ProjectDetail"></returns>

        public ProjectDetail GetFeaturedProjectProfile(int projectId, int userId = 0)
        {
            ProjectDetail projDetail = new ProjectDetail();
            try
            {
                projDetail = ProjectDataAccess.GetFeaturedProjectProfile(projectId, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projDetail;
        }
    }
}