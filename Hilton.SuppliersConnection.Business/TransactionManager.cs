﻿using System;
using System.Data;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class TransactionManager : ITransactionManager
    {
        private static TransactionManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private TransactionManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static TransactionManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TransactionManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Gets the transaction details for a transaction id
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public PaymentTransactionDetails GetTransactionDetails(int paymentRecordId)
        {
            PaymentTransactionDetails transactionDetails = null;
            try
            {
                transactionDetails = TransactionDataAccess.GetTransactionDetails(paymentRecordId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return transactionDetails;
        }

        /// <summary>
        /// Gets the transaction list
        /// </summary>
        /// <param name="transactionSearch"></param>
        /// <returns></returns>
        public Transaction GetTransactionList(TransactionSearch transactionSearch)
        {
            Transaction transaction = null;
            try
            {
                transaction = TransactionDataAccess.GetTransactionList(transactionSearch);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return transaction;
        }
        
        /// <summary>
        /// Gets the transaction list
        /// </summary>
        /// <param name="transactionSearch"></param>
        /// <returns></returns>
        public DataSet ExportTransactions(TransactionSearch transactionSearch)
        {
            DataSet transaction = null;
            try
            {
                transaction = TransactionDataAccess.ExportTransactions(transactionSearch);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return transaction;
        }

        /// <summary>
        /// Update the transaction status
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int UpdateTransactionStatus(int paymentRecordId, string transactionStatusId, TranComment transComment)
        {
            int result = -1;
            try
            {
                result = TransactionDataAccess.UpdateTransactionStatus(paymentRecordId, transactionStatusId, transComment);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}
