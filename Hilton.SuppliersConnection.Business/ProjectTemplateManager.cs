﻿using System;
using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class ProjectTemplateManager : IProjectTemplateManager
    {
        private static ProjectTemplateManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private ProjectTemplateManager()
        { }

        /// <summary>
        ///gets an instance of the class
        /// </summary>
        public static ProjectTemplateManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ProjectTemplateManager();
                }
                return _instance;
            }
        }

        /// <summary>
        ///saves a new category to db
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public bool AddCategory(Category category, int projectTemplateId)
        {
            bool result = false;
            try
            {
                result = ProjectTemplateDataAccess.AddCategory(category, projectTemplateId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///saves a new partner/ product to db
        /// </summary>
        /// <param name="partnerProductXML"></param>
        /// <returns></returns>
        public bool AddPartnerProduct(CategoryPartnerProduct partnerProduct)
        {
            bool result = false;
            try
            {
                result = ProjectTemplateDataAccess.AddPartnerProduct(partnerProduct);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///copy template to target template
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public bool CopyTemplate(ProjectTemplate sourceTemplate, ProjectTemplate destinationTemplate, bool categoriesWithProducts)
        {
            bool result = false;
            try
            {
                result = ProjectTemplateDataAccess.CopyTemplate(sourceTemplate, destinationTemplate, categoriesWithProducts);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///export project template to excel
        /// </summary>
        /// <param name="projectTemplate"></param>
        public DataSet ExportProjectTemplate(int projectTemplateId)
        {
            DataSet projectTemplateDataSet = null;
            try
            {
                projectTemplateDataSet = ProjectTemplateDataAccess.ExportProjectTemplate(projectTemplateId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectTemplateDataSet;
        }

        /// <summary>
        ///retrieves brand list from db
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public IList<Brand> GetBrandList(int regionId)
        {
            IList<Brand> brandCollection = null;
            try
            {
                brandCollection = ProjectTemplateDataAccess.GetBrandList(regionId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return brandCollection;
        }

        /// <summary>
        ///get partner list from db
        /// </summary>
        /// <param name="categoryPartnerProduct"></param>
        /// <returns></returns>
        public IList<Partner> GetActivePartnerList()
        {
            IList<Partner> partnerCollection = null;
            try
            {
                partnerCollection = ProjectTemplateDataAccess.GetActivePartnerList();
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return partnerCollection;
        }

        /// <summary>
        ///gets product list from db for a selected partner
        /// </summary>
        /// <param name="categoryPartnerProduct"></param>
        /// <returns></returns>
        public IList<Product> GetProductList(CategoryPartnerProduct categoryPartnerProduct)
        {
            IList<Product> productCollection = null;
            try
            {
                productCollection = ProjectTemplateDataAccess.GetProductList(categoryPartnerProduct);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return productCollection;
        }

        /// <summary>
        ///get the entire project template ( list of categories ) from db for a particular templateId
        /// </summary>
        /// <param name="projectTemplateId"></param>
        /// <returns></returns>
        public ProjectTemplate GetProjectTemplateDetail(int projectTemplateId)
        {
            ProjectTemplate projectTemplate = null;
            try
            {
                projectTemplate = ProjectTemplateDataAccess.GetProjectTemplateDetail(projectTemplateId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectTemplate;
        }

        /// <summary>
        ///get a list of templates for teh given search criteria
        /// </summary>
        /// <param name="projectSearch"></param>
        /// <returns></returns>
        public IList<ProjectTemplate> GetProjectTemplateList(TemplateSearch projectSearch, ref int totalRecordCount)
        {
            IList<ProjectTemplate> projectTemplateCollection = null;
            try
            {
                projectTemplateCollection = ProjectTemplateDataAccess.GetProjectTemplateList(projectSearch, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return projectTemplateCollection;
        }

        /// <summary>
        ///retrieve the search filters for project templates
        /// </summary>
        /// <returns></returns>
        public IList<SearchFilters> GetProjectTemplateSearchFilterItems(int userId)
        {
            IList<SearchFilters> searchFilterCollection = null;
            try
            {
                searchFilterCollection = ProjectTemplateDataAccess.GetProjectTemplateSearchFilterItems(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return searchFilterCollection;
        }

        /// <summary>
        ///retrives property types list
        /// </summary>
        /// <param name="selectedRegionId"></param>
        /// <param name="selectedBrandId"></param>
        /// <param name="sourceProjectTemplateId"></param>
        /// <returns></returns>
        public IList<PropertyType> GetPropertyTypeList(int selectedRegionId, int selectedBrandId, int sourceProjectTemplateId)
        {
            IList<PropertyType> propertyTypeCollection = null;
            try
            {
                propertyTypeCollection = ProjectTemplateDataAccess.GetPropertyTypeList(selectedRegionId, selectedBrandId, sourceProjectTemplateId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return propertyTypeCollection;
        }

        /// <summary>
        /// Retrieves Brand Region Type list
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sourceTemplateId"></param>
        /// <returns></returns>
        public IList<BrandRegionType> GetBrandRegionTypeList(int userId, int sourceTemplateId)
        {
            IList<BrandRegionType> brandRegionTypeCollection = null;
            try
            {
                brandRegionTypeCollection = ProjectTemplateDataAccess.GetBrandRegionTypeList(userId, sourceTemplateId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return brandRegionTypeCollection;
        }

        /// <summary>
        /// Retrieves region list
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList<Region> GetRegionList(int userId)
        {
            IList<Region> regionCollection = null;
            try
            {
                regionCollection = ProjectTemplateDataAccess.GetRegionList(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return regionCollection;
        }

        /// <summary>
        ///Update a particular category in DB
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public bool UpdateCategory(Category category, int projectTemplateId)
        {
            bool result = false;
            try
            {
                result = ProjectTemplateDataAccess.UpdateCategory(category, projectTemplateId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///Update the status of category list
        /// </summary>
        /// <param name="statusUpdateXML"></param>
        /// <returns></returns>
        public bool UpdateCategoryListConfig(int projectTemplateId, IList<Category> categoryList)
        {
            bool result = false;
            try
            {
                result = ProjectTemplateDataAccess.UpdateCategoryListConfig(projectTemplateId, categoryList);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///update category status
        /// </summary>
        /// <param name="projectTemplateId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public bool UpdateCategoryStatus(int projectTemplateId, int categoryId, bool isActive)
        {
            bool result = false;
            try
            {
                result = ProjectTemplateDataAccess.UpdateCategoryStatus(projectTemplateId, categoryId, isActive);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///update category status for multiple categories in one go
        /// </summary>
        /// <param name="projectTemplateId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public bool UpdateCategoryStatusForMultipleCategories(int projectTemplateId, string categoryIds, bool isActive)
        {
            bool result = false;
            try
            {
                result = ProjectTemplateDataAccess.UpdateCategoryStatusForMultipleCategories(projectTemplateId, categoryIds, isActive);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        ///update partner/ product in db.
        /// </summary>
        /// <param name="statusUpdateXML"></param>
        /// <returns></returns>
        public bool UpdatePartnerProduct(CategoryPartnerProduct partnerProduct)
        {
            bool result = false;
            try
            {
                result = ProjectTemplateDataAccess.UpdatePartnerProduct(partnerProduct);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Update project template status
        /// </summary>
        /// <param name="projectTemplate"></param>
        /// <returns></returns>
        public int UpdateProjectTemplate(IList<ProjectTemplate> projectTemplate)
        {
            int result = -1;
            try
            {
                result = ProjectTemplateDataAccess.UpdateProjectTemplate(projectTemplate);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Update project template status
        /// </summary>
        /// <param name="projectTemplate"></param>
        /// <returns></returns>
        public IList<Brand> GetProjectTemplateActiveBrands()
        {
            IList<Brand> brandCollection = null;
            try
            {
                brandCollection = ProjectTemplateDataAccess.GetProjectTemplateActiveBrands();
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return brandCollection;
        }

        /// <summary>
        /// get project templates
        /// </summary>
        /// <param name="projectTemplate"></param>
        /// <returns></returns>



        public IList<ProjectTemplate> GetProjectTemplatesByBrandId(int selectedBrandId)
        {
            IList<ProjectTemplate> ProjectTemplateCollection = null;
            try
            {
                ProjectTemplateCollection = ProjectTemplateDataAccess.GetProjectTemplatesByBrandId(selectedBrandId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return ProjectTemplateCollection;
        }


        public int CopyMultipleProjectTemplate(IList<ProjectTemplate> projectTemplate, IList<Category> Categories, int copyFromBrandId, int copyToBrandId, int userId)
        {

            int result = -1;
            try
            {
                result = ProjectTemplateDataAccess.CopyMultipleProjectTemplate(projectTemplate, Categories, copyFromBrandId, copyToBrandId, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }
    }
}


