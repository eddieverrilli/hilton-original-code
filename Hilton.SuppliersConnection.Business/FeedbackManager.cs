﻿using System;
using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class FeedbackManager : IFeedbackManager
    {
        private static FeedbackManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private FeedbackManager()
        { }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static FeedbackManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new FeedbackManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// This method Gets FeedbackList
        /// </summary>
        /// <param name="feedbackSearch"></param>
        /// <returns></returns>
        public IList<Feedback> GetFeedbackList(FeedbackSearch feedbackSearch, ref int totalRecordCount)
        {
            IList<Feedback> feedbackCollection = null;
            try
            {
                feedbackCollection = FeedbackDataAccess.GetFeedbackList(feedbackSearch, ref totalRecordCount);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);

                if (rethrow)
                {
                    throw exception;
                }
            }
            return feedbackCollection;
        }

        /// <summary>
        /// It Updates Feedback
        /// </summary>
        /// <param name="feedbacks"></param>
        /// <returns></returns>
        public int UpdateFeedback(IList<Feedback> feedbacks)
        {
            int result = -1;
            try
            {
                result = FeedbackDataAccess.UpdateFeedback(feedbacks);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Adds Feedback
        /// </summary>
        /// <param name="feedback"></param>
        /// <returns></returns>
        public bool AddFeedback(Feedback feedback)
        {
            bool flag = false;
            try
            {
                flag = FeedbackDataAccess.AddFeedback(feedback);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return flag;
        }
        /// <summary>
        /// AddFeedbackComment
        /// </summary>
        /// <param name="comment"></param>
        public bool AddFeedbackComment(FeedbackComment comment)
        {
            bool flag = false;
            try
            {
                flag = FeedbackDataAccess.AddFeedbackComment(comment);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return flag;
        }

    }
}