﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hilton.SuppliersConnection.DataAccess;
using Hilton.SuppliersConnection.Entities;
using Hilton.SuppliersConnection.Infrastructure;

namespace Hilton.SuppliersConnection.Business
{
    public class SecurityManager : ISecurityManager
    {
        private static SecurityManager _instance;

        /// <summary>
        /// private constructor
        /// </summary>
        private SecurityManager()
        { }

        /// <summary>
        /// Gets Menus
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="adminMenu"></param>
        /// <returns></returns>
        public IList<UserPermissions> GetMenus(int userTypeId, bool adminMenu, int userId = 0)
        {
            IList<UserPermissions> userPermissionCollection = null;
            try
            {
                userPermissionCollection = SecurityDataAccess.GetMenus(userTypeId, adminMenu, userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userPermissionCollection;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <returns></returns>
        public IList<Menu> UpdateMenuAccess(int userId)
        {
            IList<Menu> userPermissionCollection = null;
            try
            {
                userPermissionCollection = SecurityDataAccess.UpdateMenuAccess(userId);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return userPermissionCollection;
        }

        /// </summary>
        /// <param name="menuAccessCollection"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public bool ValidateScreenAuthorization(IList<Menu> menuAccessCollection, int menuId)
        {
            if (menuAccessCollection != null && menuAccessCollection.Count > 0)
            {
                return menuAccessCollection.Any(x => x.MenuId == menuId);
            }
            else
                return false;
        }

        /// <summary>
        /// Validates Admin Control Authorization
        /// </summary>
        /// <param name="menuAccessCollection"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public bool ValidateAdminControlAuthorization(IList<Permission> permissionCollection, int permissionId)
        {
            if (permissionCollection != null && permissionCollection.Count > 0)
            {
                return permissionCollection.Any(x => x.PermissionId == permissionId);
            }
            else
                return false;
        }

        /// <summary>
        /// Validates Menu Authorization
        /// </summary>
        /// <param name="menuCollection"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public bool ValidateMenuAuthorization(IList<UserPermissions> menuCollection, int menuId)
        {
            if (menuCollection != null && menuCollection.Count > 0)
            {
                return menuCollection.Any(x => x.MenuId == menuId);
            }
            else
                return false;
        }

        /// <summary>
        /// Record the terms and condition agreed by the user
        /// </summary>
        /// <param name="menuCollection"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public bool RecordTermsAndConditionAgreement(Acknowledgement termAndcondition)
        {
            bool result = false;
            try
            {
                result = SecurityDataAccess.RecordTermsAndConditionAgreement(termAndcondition);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return result;
        }

        /// <summary>
        /// Static singleton instance of the class
        /// </summary>
        public static SecurityManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SecurityManager();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Creates User Session
        /// </summary>
        /// <param name="userNUmber"></param>
        /// <returns></returns>
        public User CreateUserSession(string hiltonId, string hiltonNumber)
        {
            User user = null;
            try
            {
                user = SecurityDataAccess.CreateUserSession(hiltonId, hiltonNumber);
            }
            catch (Exception exception)
            {
                bool rethrow = BusinessLogicExceptionHandler.HandleException(ref exception);
                if (rethrow)
                {
                    throw exception;
                }
            }
            return user;
        }
    }
}