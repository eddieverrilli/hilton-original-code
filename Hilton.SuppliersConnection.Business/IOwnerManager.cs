﻿using System.Collections.Generic;
using System.Data;
using Hilton.SuppliersConnection.Entities;

namespace Hilton.SuppliersConnection.Business
{
    public interface IOwnerManager
    {
        int AddProjectUser(User user);

        DataSet ExportAllPartnerOption(int projectId);

        DataSet ExportSelectedPartners(int projectId, int userId);

        ProjectDetail GetProjectProfile(int projectId, int userId);

        IList<Project> GetProjectListForOwner(MyProjectsSearch myProjectSearch);

        IList<PartnerProductOption> GetPartnerProductOption(PartnerProdOptionSearch searchCriteria, ref int totalRecordCount);

        PartnerProductOption GetPartnerProductOptionDetails(int categoryId, int projectId, int userId);

        PartnerProductOption GetProductPdf(int productId, bool isOther);

        void UpdateProductSelection(int productId, bool isSelected, bool isNonApproved, int partnerId, int projectId, int catId, int updateByUserId);

        void SaveNonApprovedProducts(int projectId, string productName, string manufacturer, string modelNumber, string reason, int categoryId, string specSheetName, byte[] specSheet, int updatedByUserId);

        string GetSCConfigurationPercent(int projectId);

        int RemoveProjectConsultantAssociation(User user);

        int UpdateProjectUser(User user);
    }
}