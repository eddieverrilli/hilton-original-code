﻿using System;
using System.Runtime.Caching;

namespace Hilton.SuppliersConnection.Infrastructure
{
    public static class Caching
    {
        private static readonly ObjectCache cache = MemoryCache.Default;

        /// <summary>
        /// Insert value into the cache using appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add<T>(T objectToCache, string key) where T : class
        {
            cache.Add(key, objectToCache, DateTime.Now.AddHours(8));
        }

        /// <summary>
        /// Insert value into the cache using appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add<T>(T objectToCache, string key, double absoluteExpirationHrs) where T : class
        {
            cache.Add(key, objectToCache, DateTime.Now.AddHours(absoluteExpirationHrs));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <param name="objectToStore">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add(object objectToStore, string key)
        {
            cache.Add(key, objectToStore, DateTime.Now.AddHours(8));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <param name="objectToStore">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add(object objectToStore, string key, double absoluteExpirationHrs)
        {
            cache.Add(key, objectToStore, DateTime.Now.AddHours(absoluteExpirationHrs));
        }

        /// <summary>
        /// Remove item from cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public static void Clear(string key)
        {
            cache.Remove(key);
        }

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return cache.Get(key) != null;
        }

        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <returns>Cached item as type</returns>
        public static T Get<T>(string key) where T : class
        {
            if (cache.Get(key) != null)
                return (T)cache[key];
            else
                return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get(string key)
        {
            if (cache.Get(key) != null)
                return cache[key];
            else
                return null;
        }
    }
}