﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Hilton.SuppliersConnection.Infrastructure
{
    public static class BusinessLogicExceptionHandler
    {
        public static bool HandleException(ref System.Exception ex)
        {
            bool rethrow = false;
            if ((ex is DataAccessException) || (ex is DataAccessCustomException))
            {
                rethrow = ExceptionPolicy.HandleException(ex, "PassThroughPolicy");
                ex = new PassThroughException(ex.Message);
            }
            else if (ex is BusinessLogicCustomException)
            {
                rethrow = ExceptionPolicy.HandleException(ex, "BusinessLogicCustomPolicy");
            }
            else
            {
                rethrow = ExceptionPolicy.HandleException(ex, "BusinessLogicPolicy");
            }
            if (rethrow)
            {
                throw ex;
            }
            return rethrow;
        }
    }
}