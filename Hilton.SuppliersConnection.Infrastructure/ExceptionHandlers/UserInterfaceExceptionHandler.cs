﻿using System;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Hilton.SuppliersConnection.Infrastructure
{
    public static class UserInterfaceExceptionHandler
    {
        public static bool HandleExcetion(ref Exception ex)
        {
            bool rethrow = false;
            try
            {
                if (ex is BaseException)
                {
                    // do nothing as Data Access or Business Logic exception has already been logged / handled
                }
                else
                {
                    rethrow = ExceptionPolicy.HandleException(ex, "UserInterfacePolicy");
                }
            }
            catch (Exception exp)
            {
                ex = exp;
            }
            return rethrow;
        }
    }
}