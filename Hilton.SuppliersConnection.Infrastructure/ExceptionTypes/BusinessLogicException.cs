﻿using System;
using System.Runtime.Serialization;

namespace Hilton.SuppliersConnection.Infrastructure
{
    [Serializable]
    public class BusinessLogicException : BaseException, ISerializable
    {
        public BusinessLogicException()
            : base()
        {
        }

        public BusinessLogicException(string message)
            : base(message)
        {
        }

        public BusinessLogicException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}