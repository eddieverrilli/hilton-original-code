﻿using System;
using System.Runtime.Serialization;

namespace Hilton.SuppliersConnection.Infrastructure
{
    [Serializable]
    public class DataAccessException : BaseException, ISerializable
    {
        public DataAccessException()
            : base()
        {
        }

        public DataAccessException(string message)
            : base(message)
        {
        }

        public DataAccessException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}