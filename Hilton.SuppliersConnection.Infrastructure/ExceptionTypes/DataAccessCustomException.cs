﻿using System;
using System.Runtime.Serialization;

namespace Hilton.SuppliersConnection.Infrastructure
{
    [Serializable]
    public class DataAccessCustomException : BaseException, ISerializable
    {
        public DataAccessCustomException()
            : base()
        {
        }

        public DataAccessCustomException(string message)
            : base(message)
        {
        }

        public DataAccessCustomException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}