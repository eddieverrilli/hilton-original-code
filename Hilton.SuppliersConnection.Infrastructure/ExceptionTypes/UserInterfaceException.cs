﻿using System;
using System.Runtime.Serialization;

namespace Hilton.SuppliersConnection.Infrastructure
{
    [Serializable]
    public class UserInterfaceException : Exception, ISerializable
    {
        public UserInterfaceException()
            : base()
        {
            // Add implementation (if required)
        }

        public UserInterfaceException(string message)
            : base(message)
        {
            // Add implemenation (if required)
        }

        public UserInterfaceException(string message, System.Exception inner)
            : base(message, inner)
        {
            // Add implementation
        }

        protected UserInterfaceException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            //Add implemenation
        }
    }
}