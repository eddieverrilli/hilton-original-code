﻿using System;
using System.Runtime.Serialization;

namespace Hilton.SuppliersConnection.Infrastructure
{
    [Serializable]
    public class BaseException : Exception, ISerializable
    {
        public BaseException()
            : base()
        {
        }

        public BaseException(string message)
            : base(message)
        {
        }

        public BaseException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}