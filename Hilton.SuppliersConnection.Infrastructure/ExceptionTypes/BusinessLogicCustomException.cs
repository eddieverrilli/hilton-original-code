﻿using System;
using System.Runtime.Serialization;

namespace Hilton.SuppliersConnection.Infrastructure
{
    [Serializable]
    public class BusinessLogicCustomException : BaseException, ISerializable
    {
        public BusinessLogicCustomException()
            : base()
        {
        }

        public BusinessLogicCustomException(string message)
            : base(message)
        {
        }

        public BusinessLogicCustomException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}