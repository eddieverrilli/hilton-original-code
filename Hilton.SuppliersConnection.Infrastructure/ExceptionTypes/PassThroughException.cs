﻿using System;
using System.Runtime.Serialization;

namespace Hilton.SuppliersConnection.Infrastructure
{
    [Serializable]
    public class PassThroughException : BaseException, ISerializable
    {
        public PassThroughException()
            : base()
        {
        }

        public PassThroughException(string message)
            : base(message)
        {
        }

        public PassThroughException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}